﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Runtime.Serialization;
using CalculatorTS;
using System.Threading;

namespace CommonClassLibrary
{
    #region Types
    public enum CardReaderCommandT { ReadData, WriteData };

    public enum CardReaderWorkT
    {
        NotWork,
        Work
    }
    public enum HardReaderStatusT
    {
        Unknown,
        Exception,
        TimeOut,
        Waiting,
        SomeoneElsesCard,
        CardReaded,
        CardWrited,
        ReadError,
        WriteError,
        CardExists,
        CardNotExists,
        CardIDExists,
        CardIDNotExists,
        ReaderNotExists
    }
    public enum SoftReaderStatusT
    {
        Unknown,
        TimeOut,
        Exception,
        CardNotExists,
        CardReaded,
        CardWrited,
        ReadError,
        WriteError,
        SomeoneElsesCard,
        Reading,
        Writing
    }

    [DataContract]
    public struct CardReaderResponse
    {
        [DataMember]
        public WebAnswerT Status;
        [DataMember]
        public string Now;
        [DataMember]
        public string CardId;
        [DataMember]
        public string ParkingEnterTime;
        [DataMember]
        public string LastRecountTime;
        [DataMember]
        public byte? TSidFC;
        [DataMember]
        public byte? TPidFC;
        [DataMember]
        public byte? ZoneidFC;
        [DataMember]
        public byte? ClientGroupidFC;
        [DataMember]
        public int? SumOnCard;
        [DataMember]
        public string LastPaymentTime;
        [DataMember]
        public string Nulltime1;
        [DataMember]
        public string Nulltime2;
        [DataMember]
        public string Nulltime3;
        [DataMember]
        public string DateSaveCard;
        [DataMember]
        public string TVP;
        [DataMember]
        public byte? TKVP;
        [DataMember]
        public byte? ClientTypidFC;
        [DataMember]
        public bool? IsDababaseExists;
        [DataMember]
        public bool? Blocked;
        [DataMember]
        public string ErrorText;
        [DataMember]
        public List<string> ReaderList;
        [DataMember]
        public bool? IsAuthenticated;
        [DataMember]
        public HardReaderStatusT? CurrentStatus;
        [DataMember]
        public decimal? AmountDue;
        [DataMember]
        public TariffsT Tariff;
        [DataMember]
        public TariffScheduleModelT TariffShedule;
        [DataMember]
        public ZoneModelT Zone;
        [DataMember]
        public List<AbonementT> Abonements;
        [DataMember]
        public string ShiftOpened;
        [DataMember]
        public string TerminalStatus;


    };
    #endregion
    #region HexToBytenByteToHex
    //**************************************************************************
    //class for Hexidecimal to Byte and Byte to Hexidecimal conversion
    //**************************************************************************
    public class HexToBytenByteToHex
    {
        public HexToBytenByteToHex()
        {

            // constructor

        }
        public static int GetByteCount(string hexString)
        {
            int numHexChars = 0;
            char c;
            // remove all none A-F, 0-9, characters
            for (int i = 0; i < hexString.Length; i++)
            {
                c = hexString[i];
                if (IsHexDigit(c))
                    numHexChars++;
            }
            // if odd number of characters, discard last character
            if (numHexChars % 2 != 0)
            {
                numHexChars--;
            }
            return numHexChars / 2; // 2 characters per byte
        }

        public static byte[] GetBytes(string hexString, out int discarded)
        {
            discarded = 0;
            string newString = "";
            char c;
            // remove all none A-F, 0-9, characters
            for (int i = 0; i < hexString.Length; i++)
            {
                c = hexString[i];
                if (IsHexDigit(c))
                    newString += c;
                else
                    discarded++;
            }
            // if odd number of characters, discard last character
            if (newString.Length % 2 != 0)
            {
                discarded++;
                newString = newString.Substring(0, newString.Length - 1);
            }

            int byteLength = newString.Length / 2;
            byte[] bytes = new byte[byteLength];
            string hex;
            int j = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                hex = new string(new Char[] { newString[j], newString[j + 1] });
                bytes[i] = HexToByte(hex);
                j = j + 2;
            }
            return bytes;
        }
        public static string ToString(byte[] bytes)
        {
            string hexString = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                hexString += bytes[i].ToString("X2");
            }
            return hexString;
        }
        public static bool InHexFormat(string hexString)
        {
            bool hexFormat = true;

            foreach (char digit in hexString)
            {
                if (!IsHexDigit(digit))
                {
                    hexFormat = false;
                    break;
                }
            }
            return hexFormat;
        }

        public static bool IsHexDigit(Char c)
        {
            int numChar;
            int numA = Convert.ToInt32('A');
            int num1 = Convert.ToInt32('0');
            c = Char.ToUpper(c);
            numChar = Convert.ToInt32(c);
            if (numChar >= numA && numChar < (numA + 6))
                return true;
            if (numChar >= num1 && numChar < (num1 + 10))
                return true;
            return false;
        }
        private static byte HexToByte(string hex)
        {
            if (hex.Length > 2 || hex.Length <= 0)
                throw new ArgumentException("hex must be 1 or 2 characters in length");
            byte newByte = byte.Parse(hex, System.Globalization.NumberStyles.HexNumber);
            return newByte;
        }


    }
    #endregion

    public abstract class CommonCardReader
    {
        public object locker = new object();
        protected Log log;
        public CommonCardReader(Log Log = null, bool ReopeNeeded = false)
        {
            reonNeeded = ReopeNeeded;
            log = Log;
            Status = HardReaderStatusT.Unknown;
            SoftStatus = SoftReaderStatusT.Unknown;
        }
        public void InitPool()
        {
            if (StatusThread == null)
            {
                StatusThread = new Thread(PoolThread);
                StatusThread.Name = "CardReaderStatusThread";
                StatusThread.IsBackground = true;
                run = true;
                StatusThread.Start();
            }
        }
        public void ClosePool()
        {
                run = false;
        }


        protected internal byte[] tx = new byte[256];              // передача в ридер 
        protected internal byte[] rx = new byte[256];              // прием из ридер 

        protected internal int step = -1;                           // фаза (этап) чтения/записи
        protected internal bool booli;
        public bool CardInserted
        {
            get; set;
        }
        protected bool cardExists = false;
        public bool CardExists
        {
            get { return cardExists; }
        }

        // ????????????????!!!!!!!!!!!!!!!!!! ????????????????? !!!!!!!!!!!!!!
        public bool CardReaded
        {
            get { return cardReaded; }
            set { cardReaded = value; }
        }
        public string ReaderName
        {
            get; set;
        }
        public bool reonNeeded;

        protected List<string> readerList = new List<string>();
        public List<string> ReaderList
        {
            get
            {
                return readerList;
            }
        }
        public int ReaderCount
        {
            get
            {
                return readerList.Count;
            }
        }

        public bool cardReaded
        {
            get; set;
        }
        protected internal SerialPort _serialPort;
        protected internal CardReaderCommandT _CardReaderCommand;
        protected internal CommandStatusT _CommandStatus;
        public CardReaderCommandT CardReaderCommand
        {
            get
            {
                return _CardReaderCommand;
            }
        }
        public CommandStatusT CommandStatus
        {
            get
            {
                return _CommandStatus;
            }
        }

        public Thread StatusThread;

        public bool run = false;

        public HardReaderStatusT Status
        {
            get; set;
        }             // статус ридера
        public SoftReaderStatusT SoftStatus
        {
            get; set;
        }

        protected internal string statusstr;                        // Описание ошибки
        public string StatusStr
        {
            get
            {
                return statusstr;
            }
        }             // статус ридера

        protected internal ToTariffPlan.DateCard cardInfo;
        public ToTariffPlan.DateCard CardInfo
        {
            get
            {
                return cardInfo;
            }
            set
            {
                cardInfo = value;
            }
        }

        public byte SectorNumber
        {
            get;
            set;
        }

        protected internal byte NumerBlock
        {
            get;
            set;
        }      // номер блока

        protected internal byte[] keyA = new byte[6];              // ключ 6 байт
        public string KeyA
        {
            set
            {
                byte[] k = HexToBytenByteToHex.GetBytes(value, out discarded);
                int j1 = k.Length;
                int j2;
                if (j1 < 6)
                {
                    j2 = 6 - j1;
                }
                else
                {
                    j2 = 0;
                    j1 = 6;
                }

                for (int i = 0; i < j1; i++)
                    keyA[i + j2] = k[i];
            }
        }                // ключ 6 байт

        protected internal byte[] cardId = new byte[4];        // номер карты клиента
        public string CardId
        {
            get
            {
                return HexToBytenByteToHex.ToString(cardId);
            }
        }     // номер карты клиента

        public int CardReadErrorNumber
        {
            get;
            set;
        }

        protected internal byte[] wBlock = new byte[48];           // запись 3 блока карты
                                                                   //        public byte[] WBlock { set { wBlock = value; } }            // запись сектора карты

        protected internal byte[] rBlock = new byte[48];           // чтение 3 блока карты
                                                                   //        public byte[] RBlock { get { return rBlock; } }             // чтение сектора карты

        int discarded;                                          //Stores the number of discarded character

        public virtual bool IsOpen
        {
            get
            {
                return _serialPort.IsOpen;
            }
        }
        public virtual bool Open()
        {
            try
            {
                lock (locker)
                {
                    if (!IsOpen)
                    {
                        try
                        {
                            _serialPort.Open();
                            if (_serialPort.IsOpen)
                            {
                                //Status = HardReaderStatusT.Unknown;
                                //SoftStatus = SoftReaderStatusT.Unknown;
                            }
                            return true;
                        }
                        catch (Exception exc)
                        {
                            if (log != null)
                            {

                                log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCardReader Open: " + exc.ToString());
                            }
                            Status = HardReaderStatusT.TimeOut;
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                {

                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCardReader Open: " + exc.ToString());
                }
                Status = HardReaderStatusT.TimeOut;
                return false;
            }
        }
        public bool Open(string Port)
        {
            try
            {
                lock (locker)
                {
                    if (!_serialPort.IsOpen)
                    {
                        cardExists = false;
                        cardReaded = false;
                        try
                        {
                            _serialPort.PortName = Port;
                            _serialPort.Open();
                            if (_serialPort.IsOpen)
                            {
                                //Status = HardReaderStatusT.Unknown;
                                //SoftStatus = SoftReaderStatusT.Unknown;
                            }
                            return true;
                        }
                        catch (Exception exc)
                        {
                            if (log != null)
                            {

                                log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCardReader Open: " + exc.ToString());
                            }
                            Status = HardReaderStatusT.TimeOut;
                            return false;
                        }
                    }
                    else
                    {
                        //GetStatus();
                        return true;
                    }
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                {

                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCardReader Open: " + exc.ToString());
                }
                Status = HardReaderStatusT.TimeOut;
                return false;
            }
        }
        public virtual bool Close()
        {
            try
            {
                lock (locker)
                {
                    if (IsOpen)
                    {
                        try
                        {
                            //run = false;
                            _serialPort.Close();
                            //GetStatus();
                            cardExists = false;
                            return true;
                        }
                        catch (Exception exc)
                        {
                            if (log != null)
                            {

                                log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCardReader Close: " + exc.ToString());
                            }
                            Status = HardReaderStatusT.TimeOut;
                            return false;
                        }
                    }
                    else
                    {
                        //GetStatus();
                        return true;
                    }
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                {

                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCardReader Close: " + exc.ToString());
                }
                Status = HardReaderStatusT.TimeOut;
                return false;
            }
        }

        public virtual bool CheckKeyA()
        {
            return true;
        }

        /*
        protected virtual bool CheckKeyB()
        {
            return true;
        }
        */
        protected virtual bool GetVersion()
        {
            return true;
        }
        void dataToWBlock()
        {
            int CurPos = 0;
            for (int i = 0; i < 4; i++)
            {
                byte a = (byte)((cardInfo.ParkingEnterTime & (255 << (i * 8))) >> (i * 8));  // выбор байта, начиная с младшего
                wBlock[i + CurPos] = a;
            }
            CurPos += 4;
            for (int i = 0; i < 4; i++)
            {
                byte a = (byte)((cardInfo.LastRecountTime & (255 << (i * 8))) >> (i * 8));  // выбор байта, начиная с младшего
                wBlock[i + CurPos] = a;
            }
            CurPos += 4;
            wBlock[CurPos++] = cardInfo.TSidFC;
            wBlock[CurPos++] = cardInfo.TPidFC;
            wBlock[CurPos++] = cardInfo.ZoneidFC;
            wBlock[CurPos++] = cardInfo.ClientGroupidFC;
            for (int i = 0; i < 4; i++)
            {
                byte a = (byte)((cardInfo.SumOnCard & (255 << (i * 8))) >> (i * 8));  // выбор байта, начиная с младшего
                wBlock[i + CurPos] = a;
            }
            CurPos += 4;
            for (int i = 0; i < 4; i++)
            {
                byte a = (byte)((cardInfo.LastPaymentTime & (255 << (i * 8))) >> (i * 8));  // выбор байта, начиная с младшего
                wBlock[i + CurPos] = a;
            }
            CurPos += 4;
            for (int i = 0; i < 4; i++)
            {
                byte a = (byte)((cardInfo.Nulltime1 & (255 << (i * 8))) >> (i * 8));  // выбор байта, начиная с младшего
                wBlock[i + CurPos] = a;
            }
            CurPos += 4;
            for (int i = 0; i < 4; i++)
            {
                byte a = (byte)((cardInfo.Nulltime2 & (255 << (i * 8))) >> (i * 8));  // выбор байта, начиная с младшего
                wBlock[i + CurPos] = a;
            }
            CurPos += 4;
            for (int i = 0; i < 4; i++)
            {
                byte a = (byte)((cardInfo.Nulltime3 & (255 << (i * 8))) >> (i * 8));  // выбор байта, начиная с младшего
                wBlock[i + CurPos] = a;
            }
            CurPos += 4;
            for (int i = 0; i < 4; i++)
            {
                byte a = (byte)((cardInfo.TVP & (255 << (i * 8))) >> (i * 8));  // выбор байта, начиная с младшего
                wBlock[i + CurPos] = a;
            }
            CurPos += 4;
            wBlock[CurPos++] = cardInfo.TKVP;
            wBlock[CurPos++] = cardInfo.ClientTypidFC;
            CurPos += 2; // Параметры для новых условий
            CurPos += 4; //Время начала промежутка времени для нового условия
            for (int i = 0; i < 4; i++) //Время последней записи на карту
            {
                byte a = (byte)((cardInfo.DateSaveCard & (255 << (i * 8))) >> (i * 8));  // выбор байта, начиная с младшего
                wBlock[i + CurPos] = a;
            }
            CurPos += 4;
        }
        void rBlockToData()                       // считанное с карты в Данные
        {
            int CurPos = 0;
            cardInfo.ParkingEnterTime = 0;
            for (int i = 0; i < 4; i++)
            {
                cardInfo.ParkingEnterTime += rBlock[i + CurPos] << (i * 8);
            }
            CurPos += 4;
            cardInfo.LastRecountTime = 0;
            for (int i = 0; i < 4; i++)
            {
                cardInfo.LastRecountTime += rBlock[i + CurPos] << (i * 8);
            }
            CurPos += 4;
            cardInfo.TSidFC = rBlock[CurPos++];
            cardInfo.TPidFC = rBlock[CurPos++];
            cardInfo.ZoneidFC = rBlock[CurPos++];
            cardInfo.ClientGroupidFC = rBlock[CurPos++];
            cardInfo.SumOnCard = 0;
            for (int i = 0; i < 4; i++)
            {
                cardInfo.SumOnCard += rBlock[i + CurPos] << (i * 8);
            }
            CurPos += 4;
            cardInfo.LastPaymentTime = 0;
            for (int i = 0; i < 4; i++)
            {
                cardInfo.LastPaymentTime += rBlock[i + CurPos] << (i * 8);
            }
            CurPos += 4;
            cardInfo.Nulltime1 = 0;
            for (int i = 0; i < 4; i++)
            {
                cardInfo.Nulltime1 += rBlock[i + CurPos] << (i * 8);
            }
            CurPos += 4;
            cardInfo.Nulltime2 = 0;
            for (int i = 0; i < 4; i++)
            {
                cardInfo.Nulltime2 += rBlock[i + CurPos] << (i * 8);
            }
            CurPos += 4;
            cardInfo.Nulltime3 = 0;
            for (int i = 0; i < 4; i++)
            {
                cardInfo.Nulltime3 += rBlock[i + CurPos] << (i * 8);
            }
            CurPos += 4;
            cardInfo.TVP = 0;
            for (int i = 0; i < 4; i++)
            {
                cardInfo.TVP += rBlock[i + CurPos] << (i * 8);
            }
            CurPos += 4;
            cardInfo.TKVP = rBlock[CurPos++];
            cardInfo.ClientTypidFC = (rBlock[CurPos++]);
            CurPos += 2; //Параметры для новых условий
            CurPos += 4; //Время начала промежутка времени для нового условия
            cardInfo.DateSaveCard = 0;
            for (int i = 0; i < 4; i++)//Время последней записи на карту
            {
                cardInfo.DateSaveCard += rBlock[i + CurPos] << (i * 8);
            }
            CurPos += 4;

            cardReaded = true;
        }

        private void ReadCard()
        {
            if (!IsOpen)
                try
                {
                    Open();
                }
                catch
                {
                    SoftStatus = SoftReaderStatusT.Exception;
                    Status = HardReaderStatusT.Exception;
                }
            if (IsOpen)
            {
                lock (locker)
                {
                    if (GetCard())
                    {
                        Wait();
                    }
                    if (Status == HardReaderStatusT.TimeOut)
                    {
                        cardReaded = false;
                        cardExists = false;
                        SoftStatus = SoftReaderStatusT.TimeOut;
                    }
                    else if (Status == HardReaderStatusT.Exception)
                    {
                        cardReaded = false;
                        cardExists = false;
                        SoftStatus = SoftReaderStatusT.Exception;
                    }
                    else
                    {
                        if (Status == HardReaderStatusT.CardExists)
                        {
                            cardExists = true;
                            SoftStatus = SoftReaderStatusT.Reading;
                            string tmpCardId = CardId;
                            if (GetIdCard())
                            {
                                Wait();
                                if (Status != HardReaderStatusT.CardIDExists)
                                {
                                    cardReaded = false;
                                    //cardExists = false;
                                    SoftStatus = SoftReaderStatusT.ReadError;
                                }
                                else
                                {
                                    if (!(SoftStatus == SoftReaderStatusT.SomeoneElsesCard && tmpCardId == CardId))
                                    {
                                        if ((tmpCardId != CardId) || (!cardReaded))
                                        {
                                            _ReadData();
                                        }
                                    }
                                    if (cardReaded)
                                    {
                                        SoftStatus = SoftReaderStatusT.CardReaded;
                                        Status = HardReaderStatusT.CardReaded;
                                    }
                                    else if (Status == HardReaderStatusT.SomeoneElsesCard)
                                    {
                                        SoftStatus = SoftReaderStatusT.SomeoneElsesCard;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (cardExists || cardReaded)
                            {
                                cardId[0] = 0;
                                cardId[1] = 0;
                                cardId[2] = 0;
                                cardId[3] = 0;
                                cardInfo.ClientGroupidFC = 0;
                                cardInfo.ClientGroupidFC = 0;
                                cardInfo.ClientTypidFC = 0;
                                cardInfo.LastPaymentTime = 0;
                                cardInfo.LastRecountTime = 0;
                                cardInfo.Nulltime1 = 0;
                                cardInfo.Nulltime2 = 0;
                                cardInfo.Nulltime3 = 0;
                                cardInfo.ParkingEnterTime = 0;
                                cardInfo.SumOnCard = 0;
                                cardInfo.TKVP = 0;
                                cardInfo.TPidFC = 0;
                                cardInfo.TSidFC = 0;
                                cardInfo.TVP = 0;
                                cardInfo.ZoneidFC = 0;
                            }
                            cardReaded = false;
                            cardExists = false;
                            SoftStatus = SoftReaderStatusT.CardNotExists;
                        }
                    }
                }
            }
        }
        public virtual bool GetReaderNames()
        {
            return true;
        }
        public virtual bool GetCard()
        {
            return true;
        }
        public virtual bool GetIdCard()
        {
            return true;
        }
        protected virtual bool Read()
        {
            return true;
        }
        protected virtual bool Write()
        {
            return true;
        }
        public virtual bool Wait()
        {
            return true;
        }
        protected internal bool _WriteData()
        {
            dataToWBlock();
            if (!Write())
                return false;
            #region Waiting
            while (Status == HardReaderStatusT.Waiting) //"waiting"
            {
                Thread.Sleep(50);
                if (!Wait())
                {
                    cardReaded = false;
                    return false;
                }
            }
            if (Status == HardReaderStatusT.WriteError)
            {
                cardReaded = false;
                return false;
            }
            #endregion Waiting
            return true;
        }
        protected internal bool _ReadData()
        {
            if (!Read())
                return false;
            #region Waiting
            while (Status == HardReaderStatusT.Waiting) //"ожидание"
            {
                Thread.Sleep(50);
                if (!Wait())
                {
                    cardReaded = false;
                    if (reonNeeded)
                    {
                        Close();
                        Open();
                    }
                    return false;
                }
            }
            #endregion Waiting
            if (Status != HardReaderStatusT.CardReaded)
            {
                cardReaded = false;
                if (reonNeeded)
                {
                    Close();
                    Open();
                }
                return false;
            }
            rBlockToData();
            if (reonNeeded)
            {
                Close();
                Open();
            }
            return true;
        }

        public bool ReadData()
        {
            if ((_CommandStatus == CommandStatusT.Completed)
                || (_CommandStatus == CommandStatusT.Undeifned))
            {
                _CardReaderCommand = CardReaderCommandT.ReadData;
                _CommandStatus = CommandStatusT.Run;
                return true;
            }
            else
                return false;
        }
        public bool WriteData()
        {
            if ((_CommandStatus == CommandStatusT.Completed)
                || (_CommandStatus == CommandStatusT.Undeifned))
            {
                _CardReaderCommand = CardReaderCommandT.WriteData;
                _CommandStatus = CommandStatusT.Run;
                return true;
            }
            else
                return false;
        }
        protected internal void PoolThread()
        {
            while (run)
            {
                if (step == -1)
                {
                    if (_CommandStatus == CommandStatusT.Run)
                    {
                        #region CommandStatusT.Run
                        switch (_CardReaderCommand)
                        {
                            case CardReaderCommandT.ReadData:
                                _CommandStatus = CommandStatusT.Running;
                                lock (locker)
                                {
                                    cardReaded = false;
                                    //_ReadData(); // Ошибки обрабатываются внутри
                                    ReadCard();
                                    if (cardReaded)
                                    {
                                        SoftStatus = SoftReaderStatusT.CardReaded;
                                        cardExists = true;
                                        CardReadErrorNumber = 0;
                                    }
                                    else
                                        SoftStatus = SoftReaderStatusT.ReadError;
                                }
                                _CommandStatus = CommandStatusT.Completed;
                                break;
                            case CardReaderCommandT.WriteData:
                                _CommandStatus = CommandStatusT.Running;
                                if (!IsOpen)
                                    try
                                    {
                                        Open();
                                    }
                                    catch
                                    {
                                        SoftStatus = SoftReaderStatusT.WriteError;
                                        Status = HardReaderStatusT.WriteError;
                                    }
                                if (IsOpen)
                                {
                                    lock (locker)
                                    {
                                        _WriteData();
                                        if (Status == HardReaderStatusT.CardWrited)
                                            SoftStatus = SoftReaderStatusT.CardWrited;
                                        else
                                            SoftStatus = SoftReaderStatusT.WriteError;
                                    }
                                }
                                else
                                {
                                    SoftStatus = SoftReaderStatusT.WriteError;
                                }
                                _CommandStatus = CommandStatusT.Completed;
                                break;
                        }
                        #endregion CommandStatusT.Run
                    }
                    else if ((_CommandStatus == CommandStatusT.Completed)
                        || (_CommandStatus == CommandStatusT.Undeifned))
                    {
                        #region CommandStatusT.NotRun
                        ReadCard();
                        if (Status == HardReaderStatusT.CardReaded)
                        {
                            if (SoftStatus != SoftReaderStatusT.CardWrited && SoftStatus != SoftReaderStatusT.WriteError)
                                SoftStatus = SoftReaderStatusT.CardReaded;
                            cardReaded = true;
                            //cardExists = true;
                            CardReadErrorNumber = 0;
                            if (reonNeeded)
                            {
                                Close();
                                Open();
                            }
                        }
                        else if ((CardInserted) && (
                                                    (Status == HardReaderStatusT.ReadError) ||
                                                    (Status == HardReaderStatusT.SomeoneElsesCard) ||
                                                    (Status == HardReaderStatusT.CardNotExists) ||
                                                    (Status == HardReaderStatusT.CardIDNotExists)
                                                 ))
                        {
                            if (CardReadErrorNumber < 3)
                                CardReadErrorNumber++;
                            else
                            {
                                if (Status == HardReaderStatusT.ReadError)
                                    SoftStatus = SoftReaderStatusT.ReadError;
                                else if (Status == HardReaderStatusT.ReadError)
                                    SoftStatus = SoftReaderStatusT.SomeoneElsesCard;
                                else
                                    SoftStatus = SoftReaderStatusT.CardNotExists;
                            }
                        }
                        else
                            CardReadErrorNumber = 0;
                    }
                    #endregion CommandStatusT.NotRun                    
                }
                else
                {
                    if (!Wait())
                        cardReaded = false;
                }

                Thread.Sleep(200);
            }
        }
    }
}