﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading;
using System.IO;
using System.Data.SqlClient;
using Rps.ShareBase;
using System.Data;
using System.Linq;
//using SqlBaseImport.Model;

namespace CommonClassLibrary
{
    #region Types
    public enum TerminalStatusT { Undeifned, Empty, CardInserted, CardReaded, CardReadedPlus, CardReadedMinus, CardPaid, CardPrePaid, PenalCardRequest, PenalCardDisplay, PenalCardPrePaiment, PenalCardPaiment, PenalCardPaid, CardReadedError, CardJam, CardEjected, CardError, CardInStopList, CardWrongCasse, PreCancelled, Cancelled, CardWriteError, /*CardReadError,*/ PaymentError, /*PrintChequePaid, PrintChequeCancelled, PrintChequeError, PrintChequeShift,*/ ShiftSwitchNeed, ExitWithoutPayment, LongExitOperation, CardFree };
    public enum AlarmCode
    {
        DeviceWork = 0,
        ExternalDoorClosed,
        InternalDoorClosed,
        ShiftOpened,
        LicenseIncorrect = 120,
        //TerminalNotWork = 199,
        CashAcceptorNearFull = 200,
        CashAcceptorFull,
        CashAcceptorJam,
        CashAcceptorNotConnect,
        CashAcceptorBoxOpen,
        CashAcceptorError,
        CashDispenserDownNearEmpty = 206,
        CashDispenserDownEmpty,
        CashDispenserDownError,
        CashDispenserUpNearEmpty = 209,
        CashDispenserUpEmpty,
        CashDispenserUpError,
        CashDispenserRejectFull = 213,
        CashDispenserJam,
        CashDispenserNotConnect,
        CashDispenserError,
        CashDispenserRejectAlmosFull,
        CoinHopperRightNearEmpty = 220,
        CoinHopperRightEmpty,
        CoinHopperRightJam,
        CoinHopperRightNotConnect,
        CoinHopperLeftNearEmpty = 225,
        CoinHopperLeftEmpty,
        CoinHopperLeftJam,
        CoinHopperLeftNotConnect,
        CoinAcceptorNearFull = 230,
        CoinAcceptorFull,
        CoinAcceptorJam,
        CoinAcceptorNotConnect,
        CardDispenserNearEmpty = 240,
        CardDispenserEmpty,
        CardDispenserJam,
        CardDispenserNotConnect,
        CardDispenserRejectFull,
        CardReaderNotConnect = 245,
        CardReaderError,
        BancModuleNotConnect = 250,
        BancModuleJam,
        SlaveContrllerNotConnect = 255,
        SlaveControllerDiskretMduleEmpty,
        SlaveControllerVersionError,
        SlaveControllerSignatureError,
        PaperNearEnd = 280,
        PaperEnd,
        KKMNotConnect,
        KKMModeError,
        KKMIncorrectPass,
        KKMIncorrectMode,
        CoverOpen,
        PaperTransporByHand,
        PrinterCutMechError,
        PrinterFatalError,
        PrinterHeadHot,
        PrinterMechError,
        StopByPaperEnd,
        ExternalDoorOpen = 293,
        InternalDoorOpen,
        PowerError = 295,
        CashierNotConnect,
        ShiftClosed,
        ZReprotBufferFull,
        CashAcceptorPaymentError = 299,
        CoinAcceptorPaymentError,
        CashDispenserPaymentError,
        CoinHopperPaymentError,
        KKMPaymentError,
        CardReaderPaymentError,
        CardDispenserPaymentError,
        BankModulePaymentError,
        MaxShiftTime = 310,
        CashAcceptorCheated, //311 New
        CashAcceptorJamInAcceptor, //312 New
        CashAcceptorJamInStacker, //313 New
        CashAcceptorPause, //314 New
        CashAcceptorBoxNotReady, // 315 New
        CashAcceptorStackMotorFailure,  //316
        CashAcceptorTransportSpeedFailure, //317
        CashAcceptorTransportFailure, //318
        CashAcceptorSolenoidFailure, //319
        CashAcceptorPBUnitFailure, //320
        CashAcceptorCashAcceptorHeadRemove, //321
        CashAcceptorBOOTROMFailure, //322
        CashAcceptorExternalROMFailure, //323
        CashAcceptorRAMFailure, //324
        CashAcceptorExternalROMWritingFailure, //325
        CashDispenserMainMotorError, //326
        CashDispenserNoteQualifierError, //327
        CashDispenserNoteDivertererError, //328
        CashDispenserNoteTransportError, //329
        CashDispenserNoteOutputError, //330
        CashDispenserBusy, //331
        CashDispenserDoorOpen, //332
        CashDispenserEEPROMError, //333
        CashDispenserHardwareError, //334
        CashDispenserUnconnected //335
    }
    //    public class CommonClass
    //    {
    //public enum CardDispenserStatus { Undeifned, Empty, CardInserted, CardJam, CardEjected, CardError };
    public enum CommandStatusT { Undeifned, Run, Running, Completed, Null };
    public enum CardDispenserCommandT { SendFromBoxToReadPosition, SendCardToBezelAndHold, SendCardToBezelAndNotHold, SendCardToRecycleBox, RecycleToReadPosition, SetInsert };
    public enum TerminalWorkT { TerminalClosed, TerminalWork, TerminalDontWork, TerminalChangeLanguage };
    public enum PaimentMethodT { PaimentsAll, PaimentsBankModule, PaimentsCash };
    public enum BankModuleStatusT { BankCardEmpty, BankCardInserted, BankCardSuccess, BankCardNotSuccess, BankCardEjected };
    public enum DeliveryStatusT { Undefined, Delivery, NotDelivery };
    public enum PaymentTransactionTypeT { SuccessPaymentCash = 1, SuccessPaymentBankingCard, CancellPaymentBeforeIncome, CancellPaymentAfterIncome, UnclosedPayment, ToAbonement, CancellPaymentAfterIncomeMoneyReturned };
    public struct PaymentErrorInfoT
    {
        public decimal Paid;
        public decimal CardQuery;
        public decimal DownDiff;
        public decimal ResDiff;
        public decimal SumOnCard;
        public bool Delivery;
        public bool DeliveryError;
        public bool CardWriteError;
        public bool CardDispenserError;
        public bool CashAcceptorError;
        public bool ChequeError;
        public bool Error;
    }
    public enum ExecutableDeviceType { enCashDispenser = 1, enCashAcceptor, enHopper, enCoinAcceptor, enBankModule, enBarCodeScaner, enCardDispenser, enCardReader, enDisplay, enCardBox, enKKM, enPrinter, enSlave, enCashRecycler };
    [DataContract]
    public enum WebAnswerT
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Succsess,
        [EnumMember]
        TemporaryError,
        [EnumMember]
        FatalError
    };
    [DataContract]
    public enum WebStatusT
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Empty,
        [EnumMember]
        Busy,
        [EnumMember]
        Paid
    };
    [DataContract]
    public enum TransactionTypeT
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Shift_close,
        [EnumMember]
        Shift_open,
        [EnumMember]
        Payment_aprowed,
        [EnumMember]
        Payment_is_canceled_by_the_client,
        [EnumMember]
        Payment_is_canceled_by_the_client_and_money_should_be_returned,
        [EnumMember]
        Payment_aprowed_with_error,
        [EnumMember]
        ChangeToAbonement,
        [EnumMember]
        Payment_is_canceled_by_the_client_and_money_returned,
    };
    [DataContract]
    public enum WayOfPaymentT
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Cash,
        [EnumMember]
        Banking_card
    };
    [DataContract]
    public enum ConclusionCodeT
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Done,
        [EnumMember]
        Paid_Partly,
        [EnumMember]
        Payment_is_finished_but_change_had_not_been_given_or_given_not_fully
    };
    [DataContract]
    public struct Response
    {
        public bool Responsed;
        [DataMember]
        /// Статус ответа
        public WebAnswerT Status;
        [DataMember]
        public string Now;
        [DataMember]
        public decimal AmountSum;
        [DataMember]
        public string ErrorText;
        [DataMember]
        public WebStatusT CurrentStatus;
        [DataMember(EmitDefaultValue = false)]
        public List<Change> StatusHistory;
    };
    [Serializable]
    public class AbonementT
    {
        public Guid Id;
        public int ConditionTime;
        public int ConditionBackTimeTypeId;
        public string ConditionTimePeriod;
        public int ConditionAbonementPrice;
    }
    public class AbonementsT
    {
        public Guid Id;
        public int ConditionBackTime;
        public int ConditionBackTimeTypeId;
        public string ConditionTimePeriod;
        public int ConditionAbonementPrice;
        public int ConditionBackTimeCount;
        public int TariffPlanNextId;
    }
    [DataContract]
    public struct TransactionT
    {
        [DataMember]
        public long TransactionNumber;
        [DataMember]
        public string TransactionTime, LastShiftOpenTime, PaymentTime, ExitBefore;
        [DataMember]
        public TransactionTypeT TransactionType;
        [DataMember]
        public PaymentTransactionTypeT PaymentTransactionType;
        [DataMember]
        public WayOfPaymentT WayOfPayment;
        [DataMember]
        public ConclusionCodeT ConclusionCode;
        [DataMember]
        public decimal PrepaymentSum, PurchaseSum, ToPay, Paid, Issued, SumStayedInCM;
        [DataMember]
        public bool BillIsued;
        [DataMember]
        public int ErrorCodeTransactiontop;
        [DataMember]
        public AccetpedT[] BanknotesAccepted, CoinsAccepted; //Массив [Номинал, количество]
        [DataMember]
        public int IssuedNotesCount, IssuedCoinsCount, IssuedFromUpper, IssuedFromLower, IssuedFrom1Hopper, IssuedFrom2Hopper, Rejected;
        [DataMember]
        public int DeliveredNotesCount, DeliveredCoinsCount, DeliveredFromUpper, DeliveredFromLower, DeliveredFrom1Hopper, DeliveredFrom2Hopper;

    }

    public class CardsT
    {
        public byte? Blocked;
        public bool? _IsDeleted;
        public DateTime? Sync;
        public long CardId;
        public Guid? ClientId;
        public Guid? CompanyId;
        public DateTime? ParkingEnterTime;
        public DateTime? LastRecountTime;
        public DateTime? LastPaymentTime;
        public byte? TSidFC;
        public byte? TPidFC;
        public byte? ZoneidFC;
        public byte? ClientGroupidFC;
        public int? SumOnCard;
        public DateTime? Nulltime1;
        public DateTime? Nulltime2;
        public DateTime? Nulltime3;
        public DateTime? TVP;
        public int? TKVP;
        public int? ClientTypidFC;
        public DateTime? DateSaveCard;
        public string LastPlate;
    }
    public class CardsTransactionT
    {
        public Guid Id;
        public byte Blocked;
        public bool _IsDeleted;
        public DateTime? Sync;
        public long CardId;
        public DateTime? ParkingEnterTime;
        public DateTime? LastRecountTime;
        public DateTime? LastPaymentTime;
        public byte? TSidFC;
        public byte? TPidFC;
        public byte? ZoneidFC;
        public byte? ClientGroupidFC;
        public int? SumOnCard;
        public DateTime? Nulltime1;
        public DateTime? Nulltime2;
        public DateTime? Nulltime3;
        public DateTime? TVP;
        public int? TKVP;
        public int? ClientTypidFC;
        public DateTime? DateSaveCard;
        public string LastPlate;
    }

    public class TransactionsT
    {
        public Guid Id;
        public Guid DeviceId;
        public DateTime Time;
        public DateTime TimeEntry;
        public DateTime? TimeExit;
        public long ZoneAfterId;
        public long ZoneBeforeId;
        public bool _IsDeleted;
        public DateTime? Sync;
        public int? DeviceTypeID;
        public string Cardnumber;
        public Guid? ClientID, TarifPlanId, TariffScheduleId;
        public DateTime? Nulltime1, Nulltime2, Nulltime3, TVP;
        public int? TKVP;
        public decimal? SumOnCardBefore, Amount, Balance, Paid, SumOnCardAfter, SumAccepted;
        public Guid? ClientGroupId;
        public int? ClientTypidFC;
        public Guid? ZoneId;
        public DateTime? LastRecountTime;
        public DateTime? TimeOplat;
        public int? PassageTransactionType;
        public string PlateNumberEntrance;
        public string PlateNumberExit;
        public int? PaymentTransactionTypeId;
        public decimal? ChangeIssued;
        public int? CachDispenerDown, CachDispenerUp, Hopper1, Hopper2, BanknotesAccepted, CoinsAccepted;
        public decimal? BankCardAccepted;
        public Guid? Tariff;
        public Guid? SheduleSwitchTariff;
        public Guid? SheduleReturnTariff;
        public int? PoradNumber;
    }
    public class TariffScheduleModelT
    {
        public Guid Id;
        public string Name;
        public bool _IsDeleted;
        public DateTime? Sync;
        public byte? IdFC;
    }
    public class TariffsT
    {
        public Guid Id;
        public string Name;
        public int? ChangingTypeId;
        public int TypeId;
        public int Initial;
        public int? InitialTimeTypeId;
        public int InitialAmount;
        public int ProtectedInterval;
        public int? ProtectedIntervalTimeTypeId;
        public int FreeTime;
        public int? FreeTimeTypeId;
        public bool _IsDeleted;
        public DateTime? Sync;
        public byte? IdFC;
    }
    public class ZoneModelT
    {
        public Guid Id;
        public string Name;
        public int? Capacity;
        public int? Reserved;
        public DateTime? Sync;
        public bool? _IsDeleted;
        public byte? IdFC;
        public int? FreeSpace;
        public int? OccupId;
    }
    public class DeviceCMModelT
    {
        public Guid DeviceId;
        public Guid? ZoneId;
        public int? CashDispenserTypeId;
        public int? MaxCountUp;
        public int? MaxCountDown;
        public int? CountUp;
        public int? CountDown;
        public int? CashDispenserComPortId;
        public int CashAcceptorLimit;
        public int CashAcceptorAlarm;
        public int CashAcceptorCurrent;
        public int? CashAcceptorComPortId;
        public int? HopperTypeId;
        public int? MaxCount2;
        public int? MaxCount1;
        public int? Count2;
        public int? Count1;
        public int? Hopper1ComPortId;
        public int? Hopper2ComPortId;
        public int CoinAcceptorLimit;
        public int CoinAcceptorAlarm;
        public int CoinAcceptorCurrent;
        public int? BankModuleTypeId;
        public bool? Reader;
        public bool? PayPass;
        public int? BarcodeTypeId;
        public int? BarcodeComPortId;
        public int? VAT;
        public int? MaximalChange;
        public int? PrinterTypeId;
        public int? PrinterComPortId;
        public int? CardReaderComPortId;
        public int? CardReader2ComPortId;
        public int? CardTimeOutToBasket;
        public int? PenalCardTimeOut;
        public bool? CancelRefund;
        public bool? ShiftOpened;
        public bool? _IsDeleted;
        public string CardKey;
        public int? CashierNumber;
        public bool? CashDispenserExist;
        public int? NominalUpId;
        public int? NominalDownId;
        public int? RejectCount;
        public bool? CashAcceptorExist;
        public bool? HopperExist;
        public int? NominalRightId;
        public int? NominalLeftId;
        public bool? CoinAcceptorExist;
        public int? CoinAcceptorTypeId;
        public bool? BankModuleExist;
        public bool? CardDispenserExists;
        public int? CardDispenserTypeId;
        public int? CardDispenserComPortId;
        public bool? BarcodeExist;
        public bool? KKMExists;
        public int? KKMTypeId;
        public bool? ShiftAutoClose;
        public DateTime? ShiftNewShiftTime;
        public string CommandPassword;
        public string RegistartionPass;
        public string WithOutCleaningPass;
        public string WithCleaningPass;
        public int? KKMComPortId;
        public bool? PrinterExists;
        public bool? CardReaderExist;
        public bool? CardReader2Exist;
        public int? CardReaderTypeId;
        public int? CardReader2TypeId;
        public bool? SlaveExist;
        public int? SlaveTypeId;
        public int? SlaveComPortId;
        public int? PenaltyCardTPId;
        public int? PenaltyCardTSId;
        public bool? SyncNominals;
        public int? ServerExchangeProtocolId;
        public string ServerURL;
        public int? CurrentTransactionNumber;
        public int? LanguageTimeOut;
        public string DefaultLanguage;
        public string WebCameraIP;
        public string WebCameraLogin;
        public string WebCameraPassword;
        public DateTime? LastShiftOpenTime;
        public int? CashAcceptorTypeId;
        public int? CoinAcceptorComPortId;
        public DateTime? Sync;
        public string WebCameraUrl;
        public Guid? GroupId;
        public string CoinAcceptorPinCode;
        public byte? CoinAcceptorAddress;
        public bool? SingleClient0;
        public byte? CashierType;
        public byte LogLevel;
        public byte? SectorNumber;
        public byte? ChequeTimeOut;
        public int? ShiftNumber;
        public byte? ChequeWork;
    }
    public struct CashAcceptorAllowModelT
    {
        public Guid DeviceId;
        public int BanknoteId;
        public DateTime? Sync;
        public bool Allow;
        public Guid Id;
    }
    public struct CoinAcceptorAllowModelT
    {
        public Guid DeviceId;
        public int CoinId;
        public DateTime? Sync;
        public bool Allow;
        public Guid Id;
    }
    public class DevicesLanguagesT
    {
        public Guid Id;
        public Guid DeviceId;
        public string Language;
        public bool Checked;
        public DateTime? Sync;
    }
    public class DeviceModelT
    {
        public Guid Id;
        public int Type;
        public string Name;
        public string Host;
        public string User;
        public string Password;
        public string Advanced;
        public bool? _IsDeleted;
        public string DataBase;
        public DateTime? Sync;
        public string SlaveCode;
    }
    public class TariffPlanTariffScheduleModelT
    {
        public Guid Id;
        public Guid TariffScheduleId;
        public Guid TariffPlanId;
        public Guid? TariffPlanNextId;
        public int TypeId;
        public bool _IsDeleted;
        public DateTime? Sync;
        public Guid? ConditionZoneAfter;
        public int? ConditionParkingTime;
        public int? ConditionParkingTimeTypeId;
        public int? ConditionEC;
        public int? ConditionECProtectInterval;
        public int? ConditionECProtectIntervalTimeTypId;
        public int? ConditionAbonementPrice;
        public int? ConditionBackTime;
        public int? ConditionBackTimeTypeId;
    }
    public class GroupT
    {
        public Guid Id;
        public DateTime? Sync;
        public byte? idFC;
        public string Name;
    }
    public class MessageTypeModelT
    {
        public int Id;
        public int? Color;
        public string Message;
    }
    public class BanknoteModelT
    {
        public int Id;
        public string Name;
        public string Value;
        public string Code;
        public string Length;
        public string Width;
        public int? EscrowData;
        public int? DisableData;
    }
    public class CoinModelT
    {
        public int Id;
        public string Name;
        public string Value;
        public string Code;
        public int CIndex;
    }
    public class ExecutableDeviceT
    {
        public int Id;
        public int? ExecutableDeviceTypeId;
        public string Name;

    }
    public class ExecutableDeviceTypeModelT
    {
        public int Id;
        public string Name;
    }
    public class AlarmModelT
    {
        public Guid Id;
        public Guid DeviceId;
        public DateTime Begin;
        public DateTime? End;
        public string Value;
        public string ValueEnd;
        public bool _IsDeleted;
        public DateTime? Sync;
        public int? AlarmColorId;
        public int? TypeId;
    }

    public class ClientModelT
    {
        public Guid Id;
        public DateTime? Sync;
        public string ContactName;
        public string Phone;
        public string Email;
        public Nullable<bool> _IsDeleted;
    }
    public class BankDocumetsT
    {
        public Guid Id;
        public int? ShiftNumber;
        public int? TransactionNumber;
        public byte? OperationCode;
        public string Result;
        public string Receipt;
    }

    [DataContract]
    public struct Change
    {
        [DataMember]
        public string ChangeTime;
        [DataMember]
        public WebStatusT OldStatus, NewStatus;
        [DataMember(EmitDefaultValue = false)]
        public string EntryTime;
        [DataMember(EmitDefaultValue = false)]
        public int TariffID, ClientType, CardSum, AmountSum;
        /*[DataMember(EmitDefaultValue = false)]
        public int CashierNumber, TransactionNumber;
        [DataMember(EmitDefaultValue = false)]
        TransactionT Transaction;*/

    }
    [DataContract]
    public enum AlarmSourceT
    {
        Internal, License, CardDispenser, CardReader, CashAcceptor, CoinAcceptor, CashDispenser, CashDispenserUp, CashDispenserDown, CoinHopper1, CoinHopper2, SlaveController, ExternalDoor, InternalDoor, Terminal, KKM, Printer, BankModule, PaymentError
    }
    [DataContract]
    public enum AlarmLevelT
    {
        Green = 0,
        Brown,
        Red,
        Yellow,
        Gray
    }
    [DataContract]
    public struct AlarmT
    {
        [DataMember]
        public AlarmLevelT AlarmLevel;
        [DataMember]
        public AlarmSourceT AlarmSource;
        [DataMember]
        public string Status;
        [DataMember]
        public AlarmCode ErrorCode;
        [DataMember]
        public DateTime Now;
    }
    public enum DocType
    {
        Sale,
        SaleStorno,
        SaleNotCheck
    }
    public enum TypeClose
    {
        Cash,
        Card,
        Card2
    }
    public struct AllPayParams
    {
        public decimal Paid, CardQuery, ResDiff;
        public string OpName;
        public decimal Pay_Summa;
        public decimal Pay_Query;
        public DocType DocType;
        public string What;
        public string CardId;
        public decimal Pay_SummaService;
        public string Pay_NumCheck;
        public int Pay_Num_Shift;
        public decimal Pay_SymmaComissionIP;
        public decimal Pay_SummaComissionBank;
        public decimal Pay_SummaSdachi;
        public decimal Pay_ToCard;
        public DateTime EntryTime;
        public DateTime? ExitTime;
        public List<string> AddStrings;
        public List<string> BankSlip;
        public TypeClose TypeClose;
        public bool Delivery;
        public int VAT;
    };
    public struct AccetpedT
    {
        public decimal Nominal;
        public int Count;
    }
    #endregion Types
    #region Compare
    public class Compare
    {
        public static bool ByteArrayCompare(byte[] p_BytesLeft, byte[] p_BytesRight)
        {
            if (p_BytesLeft.Length != p_BytesRight.Length)
                return false;

            var length = p_BytesLeft.Length;

            for (int i = 0; i < length; i++)
            {
                if (p_BytesLeft[i] != p_BytesRight[i])
                    return false;
            }

            return true;
        }

    }
    #endregion Compare

    #region Log

    /// <summary>
    /// Работа с лог-файлами и логом в БД
    /// </summary>

    public class Log
    {
        object locker = new object();
        object lockersuper = new object();
        object lockerfile = new object();
        struct logstr
        {
            public DateTime dt;
            public int Level;
            public string text;
        }
        Queue<logstr> LogQueue = new Queue<logstr>();
        Queue<logstr> LogQueueSuper = new Queue<logstr>();
        int level = 1;
        bool Run = false;
        Thread LogThread, LogSuperThread;
        Guid deviceId;
        int maxWriteTime = 120;
        struct DeviceEvent
        {
            public int Code;
            public Guid? AlarmId;
            public Guid? TransactionId;
            public DateTime From, To, Now;
        }
        List<DeviceEvent> DeviceEvents = new List<DeviceEvent>();
        List<DeviceEvent> DeviceContinues = new List<DeviceEvent>();
        /// <summary>
        /// Уровень лога - 0 - только критичные, 1 - включая события, 2 - включая отладку
        /// </summary>
        public int Level
        {
            set
            {
                level = value;
            }
        }
        string connectionString;
        /// <summary>
        /// Строка соединения с БД
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return connectionString;
            }
            set
            {
                connectionString = value;
            }
        }
        /// <summary>
        /// Задержка очистки памяти лога в секундах - 120 секунд или более.
        /// </summary>
        public int MaxWriteTime
        {
            set
            {
                if (value > 120)
                    maxWriteTime = value;
                else
                    maxWriteTime = 120;
            }
        }
        /// <summary>
        /// ID устройства.
        /// </summary>
        public Guid DeviceId
        {
            set
            {
                deviceId = value;
            }
        }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="FN">Имя файла лога</param>
        /// <param name="ConnectionString">Строка соединения с БД</param>
        /// <returns>Команда</returns>
        string Name = "";
        string Ext = "";
        public Log(string FN, string ConnectionString)
        {
            connectionString = ConnectionString;

            int index = FN.LastIndexOf('.');
            Name = FN.Substring(0, index);
            Ext = FN.Substring(index + 1);

            LogThread = new Thread(Process);
            LogThread.Name = FN;
            LogThread.IsBackground = true;
            LogSuperThread = new Thread(ProcessSuper);
            LogSuperThread.Name = FN;
            LogSuperThread.IsBackground = true;

            Run = true;
            LogThread.Start();
            LogSuperThread.Start();
        }
        /// <summary>
        /// Передача текста в лог
        /// </summary>
        /// <param name="Level">Уровень лога - 0 - критично, 1 - событие, 2 - отладка</param>
        /// <param name="What">Текст для записи в лог</param>
        /// <returns>Команда</returns>
        public void ToLog(int Level, string What)
        {
            try
            {
                if (What != null)
                    lock (locker)
                    {
                        if (What.Length > 2000)
                        {
                            What = What.Remove(2000);
                            StreamWriter sw = new StreamWriter("LogKassa.txt", true);
                            DateTime d = DateTime.Now;
                            string ds = d.ToString("yyyy/MM/dd HH:mm:ss.fff");
                            sw.WriteLine(ds + " Length > 2000");
                            sw.Close();
                        }

                        if (What.Contains('\0'))
                        {
                            //What = "";
                            try
                            {
                                What = What.Replace("\0", "");
                                StreamWriter sw = new StreamWriter("LogKassa.txt", true);
                                DateTime d = DateTime.Now;
                                string ds = d.ToString("yyyy/MM/dd HH:mm:ss.fff");
                                sw.WriteLine(ds + " попытка записи NULL в лог");
                                sw.Close();
                            }
                            catch (Exception ee)
                            {
                                //Console.WriteLine("Exception запись в лог: " + ee.Message);
                            }

                        }

                        if (What != "")
                        //lock (locker)
                        {
                            logstr ls = new logstr();
                            ls.dt = DateTime.Now;
                            ls.Level = Level;
                            ls.text = What;
                            //lock (locker)
                            LogQueue.Enqueue(ls);
                            lock (lockersuper)
                            {
                                LogQueueSuper.Enqueue(ls);
                            }
                        }
                    }
            }
            catch (Exception ex)
            {
                try
                {
                    StreamWriter sw = new StreamWriter("LogKassa.txt", true);
                    DateTime d = DateTime.Now;
                    string ds = d.ToString("yyyy/MM/dd HH:mm:ss.fff");
                    //sw.WriteLine(new string('-', 20));
                    sw.WriteLine(ds + " " + ex);
                    //Close the file
                    sw.Close();
                }
                catch (Exception ee)
                {
                    //Console.WriteLine("Exception запись в лог: " + ee.Message);
                }
            }
            //int i = What.IndexOf("Ссылка на объект не указывает на экземпляр объекта.");
        }
        /// <summary>
        /// Завершение работы класса (сохраняет оставшиеся сообщения в файл и завершает работу
        /// </summary>
        public void Close()
        {
            logstr ls;
            Run = false;
            lock (lockerfile)
            {
                string fileName = Name + "." + DateTime.Now.ToString("yyyyMMddHH") + "." + Ext;
                using (StreamWriter file = new StreamWriter(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName), true))
                {
                    while (LogQueue.Count > 0)
                    {
                        lock (locker)
                            ls = LogQueue.Dequeue();
                        file.WriteLine(ls.text);
                    }
                }
            }
        }

        /// <summary>
        /// Начало длительного события
        /// </summary>
        /// <param name="Event">Код сообщения в соответствии с DeviceAction - код шаблона, если окончательный код определится позже. Шаблон 100 - транзакция проезд. Шаблон 200 - транзакция оплата.</param>
        public void BeginContinues(int Event)
        {
            if (Event > 0)
            {
                try
                {
                    using (SqlConnection connect = new SqlConnection(connectionString))
                    {
                        connect.Open();
                        string req = "select * from DeviceAction where Id =" + Event.ToString();
                        DataRow dat = null;
                        try
                        {
                            dat = UtilsBase.FillRow(req, connect, null, null);
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, exc.ToString());
                            try
                            {
                                dat = UtilsBase.FillRow(req, connect, null, null);
                            }
                            catch (Exception exc1)
                            {
                                ToLog(0, exc1.ToString());
                            }
                        }
                        if (dat != null)
                            if (dat.GetBool("IsWrite").Value)
                            {
                                DeviceEvent de = new DeviceEvent();
                                de.Now = DateTime.Now;
                                de.From = de.Now - TimeSpan.FromSeconds(dat.GetInt("WriteTime").Value);
                                de.Code = Event;
                                if (dat.GetBool("IsContinuous").Value)
                                {
                                    lock (lockersuper)
                                        DeviceContinues.Add(de);
                                }
                                else
                                {
                                    de.To = de.Now + TimeSpan.FromSeconds(dat.GetInt("WriteTime").Value);
                                    lock (lockersuper)
                                        DeviceEvents.Add(de);
                                }
                            }
                    }
                }
                catch (Exception exc)
                {
                    ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + exc.ToString());
                }
            }
        }
        /// <summary>
        /// Завершение длительного события
        /// </summary>
        /// <param name="Event">Код сообщения в соответствии с DeviceAction - окончательный.</param>
        /// <param name="Now">Дата-время сообщения.</param>
        /// <param name="ID">Id сообщения, если приемлено, либо null</param>
        public void EndContinues(int Event, DateTime Now, Guid? Id)
        {
            bool found = false;
            if (Event > 0)
            {
                DeviceEvent de;
                using (SqlConnection connect = new SqlConnection(connectionString))
                {
                    connect.Open();
                    string req = "select * from DeviceAction where Id =" + Event.ToString();
                    DataRow da = null;
                    try
                    {
                        da = UtilsBase.FillRow(req, connect, null, null);
                    }
                    catch (Exception exc)
                    {
                        ToLog(0, exc.ToString());
                        try
                        {
                            da = UtilsBase.FillRow(req, connect, null, null);
                        }
                        catch (Exception exc1)
                        {
                            ToLog(0, exc1.ToString());
                        }
                    }
                    if (da != null)
                        if (da.GetBool("IsWrite").Value)
                        {
                            #region Пишем
                            int count = DeviceContinues.Count;
                            int FoundNumber = -1;
                            for (int i = count - 1; i >= 0; i--)
                            {
                                lock (lockersuper)
                                    de = DeviceContinues[i];
                                if (Event > 100 && Event < 200)
                                {
                                    #region Проезды (диапазон)
                                    if (de.Code == 100)
                                    {
                                        de.Now = Now;
                                        de.To = de.Now + TimeSpan.FromSeconds(da.GetInt("WriteTime").Value);
                                        de.Code = Event;
                                        de.TransactionId = Id;
                                        lock (lockersuper)
                                            DeviceContinues[i] = de;
                                        found = true;
                                        FoundNumber = i;
                                        break;
                                    }
                                    #endregion Проезды (диапазон)
                                }
                                else if (Event > 200 && Event < 300)
                                {
                                    #region Оплаты (диапазон)
                                    if (de.Code == 200)
                                    {
                                        de.Now = Now;
                                        de.To = de.Now + TimeSpan.FromSeconds(da.GetInt("WriteTime").Value);
                                        de.Code = Event;
                                        de.TransactionId = Id;
                                        lock (lockersuper)
                                            DeviceContinues[i] = de;
                                        found = true;
                                        FoundNumber = i;
                                        break;
                                    }
                                    #endregion Оплаты (диапазон)
                                }
                                else if (Event == de.Code)
                                {
                                    #region Определенные коды
                                    de.Now = Now;
                                    de.To = de.Now + TimeSpan.FromSeconds(da.GetInt("WriteTime").Value);
                                    de.Code = Event;
                                    de.TransactionId = Id;
                                    lock (lockersuper)
                                        DeviceContinues[i] = de;
                                    found = true;
                                    FoundNumber = i;
                                    break;
                                    #endregion Определенные коды
                                }
                            }
                            if (!found)
                            {
                                #region Не найдено (без Begin)
                                de = new DeviceEvent();
                                de.Now = DateTime.Now;
                                de.From = de.Now - TimeSpan.FromSeconds(da.GetInt("WriteTime").Value);
                                de.To = de.Now + TimeSpan.FromSeconds(da.GetInt("WriteTime").Value);
                                de.Code = Event;
                                de.TransactionId = Id;
                                lock (lockersuper)
                                    DeviceContinues.Add(de);
                                #endregion Не найдено (без Begin)
                            }
                            else
                            {
                                #region Удаляем все старше найденного
                                DeviceEvent de1;
                                lock (lockersuper)
                                    for (int i = FoundNumber - 1; i >= 0; i--)
                                    {
                                        de1 = DeviceContinues[i];
                                        if (Event > 100 && Event < 200)
                                        {
                                            #region Проезды (диапазон)
                                            if (de1.Code == 100)
                                                lock (lockersuper)
                                                    DeviceContinues.RemoveAt(i);
                                            #endregion Проезды (диапазон)
                                        }
                                        else if (Event > 200 && Event < 300)
                                        {
                                            #region Оплаты (диапазон)
                                            if (de1.Code == 200)
                                                lock (lockersuper)
                                                    DeviceContinues.RemoveAt(i);
                                            #endregion Оплаты (диапазон)
                                        }
                                        else if (Event == de1.Code)
                                        {
                                            #region Определенные коды
                                            lock (lockersuper)
                                                DeviceContinues.RemoveAt(i);
                                            #endregion Определенные коды
                                        }
                                    }
                                #endregion Удаляем все старше найденного
                            }
                            #endregion Пишем
                        }
                        else
                        {
                            #region Не пишем - удаляем
                            int count = DeviceContinues.Count;
                            for (int i = count - 1; i >= 0; i--)
                            {
                                lock (lockersuper)
                                    de = DeviceContinues[i];
                                if (Event > 100 && Event < 200)
                                {
                                    #region Проезды (диапазон)
                                    if (de.Code == 100)
                                        lock (lockersuper)
                                            DeviceContinues.RemoveAt(i);
                                    #endregion Проезды (диапазон)
                                }
                                else if (Event > 200 && Event < 300)
                                {
                                    #region Оплаты (диапазон)
                                    if (de.Code == 200)
                                        lock (lockersuper)
                                            DeviceContinues.RemoveAt(i);
                                    #endregion Оплаты (диапазон)
                                }
                                else if (Event == de.Code)
                                {
                                    #region Определенные коды
                                    lock (lockersuper)
                                        DeviceContinues.RemoveAt(i);
                                    #endregion Определенные коды
                                }
                            }
                            #endregion Не пишем - удаляем
                        }
                }
            }
        }
        /// <summary>
        /// Моментальное событие
        /// </summary>
        /// <param name="Event">Код сообщения в соответствии с DeviceAction</param>
        /// <param name="AlarmId">AlarmId</param>
        /// <param name="Now">Дата-время сообщения.</param>
        public void LogEvent(int Event, Guid AlarmId, DateTime Now)
        {
            if (Event > 0)
            {
                using (SqlConnection connect = new SqlConnection(connectionString))
                {
                    connect.Open();
                    string req = "select * from DeviceAction where Id =" + Event.ToString();
                    DataTable dat = null;
                    try
                    {
                        dat = UtilsBase.FillTable(req, connect, null);
                    }
                    catch (Exception exc)
                    {
                        //cashierForm.ToLog(0, exc.ToString());
                        try
                        {
                            dat = UtilsBase.FillTable(req, connect, null);
                        }
                        catch (Exception exc1)
                        {
                            //cashierForm.ToLog(0, exc1.ToString());
                        }
                    }
                    if (dat != null && dat.Rows.Count > 0)
                        foreach (DataRow da in dat.Rows)
                        {
                            if (da.GetBool("IsWrite").Value)
                            {
                                DeviceEvent de = new DeviceEvent();
                                de.Now = DateTime.Now;
                                de.From = de.Now - TimeSpan.FromSeconds(da.GetInt("WriteTime").Value);
                                de.To = de.Now + TimeSpan.FromSeconds(da.GetInt("WriteTime").Value);
                                de.AlarmId = AlarmId;
                                de.Code = Event;
                                lock (lockersuper)
                                    DeviceEvents.Add(de);
                            }
                        }
                }
            }
        }

        public int logSize = 2; // глубина лога в днях

        //гуси

        public int LogSize
        {
            set
            {
                logSize = value;
            }
            get
            {
                return logSize;
            }
        }
        void Process()
        {
            int count;
            while (Run)
            {
                try
                {
                    lock (locker)
                    {
                        count = LogQueue.Count;
                    }
                    lock (lockerfile)
                    {
                        string fileName = Name + "." + DateTime.Now.ToString("yyyyMMddHH") + "." + Ext;
                        if (count > 0)
                        {
                            using (StreamWriter file = new StreamWriter(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName), true))
                            {
                                while (count-- > 0)
                                {
                                    logstr ls;

                                    lock (locker)
                                        ls = LogQueue.Dequeue();

                                    if (ls.Level <= level)
                                        file.WriteLine(ls.text);
                                }
                            }
                        }
                        //FileInfo logFile = new FileInfo(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName));

                        string[] files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, Name + ".*." + Ext);

                        if (files.Length > (logSize * 24))
                        {
                            List<string> l_files = new List<string>();
                            foreach (string fn in files)
                            {
                                l_files.Add(fn);
                            }
                            l_files.Sort();
                            //for (int i = 0; i < l_files.Count - 24; i++)
                            {
                                //File.Delete(l_files[i]);
                                File.Delete(l_files[0]);
                            }
                        }
                    }
                }
                catch { };
                Thread.Sleep(50);
            }
        }
        void ProcessSuper()
        {
            DateTime NullDT = new DateTime();
            DateTime LastTrim = DateTime.Now;
            int lcount, dcount;
            while (Run)
            {
                DateTime FromContinues = DateTime.Now - TimeSpan.FromSeconds(maxWriteTime);
                logstr[] lsa;

                lock (lockersuper)
                {
                    lcount = LogQueueSuper.Count;

                    lsa = new logstr[lcount];
                    LogQueueSuper.CopyTo(lsa, 0);
                }
                DateTime Now = DateTime.Now;
                #region Непродолжительные/разовые
                lock (lockersuper)
                    dcount = DeviceEvents.Count;
                if (dcount > 0)
                {
                    using (SqlConnection connect = new SqlConnection(connectionString))
                    {
                        connect.Open();
                        for (int j = dcount - 1; j >= 0; j--)
                        {
                            try
                            {
                                if (DeviceEvents[j].To <= Now)
                                {
                                    try
                                    {
                                        string Message = "";
                                        for (int i = 0; i < lcount; i++)
                                        {
                                            if ((lsa[i].dt >= DeviceEvents[j].From) && (lsa[i].dt <= DeviceEvents[j].To))
                                                Message += lsa[i].text + Environment.NewLine;
                                            if (lsa[i].dt > DeviceEvents[j].To)
                                                break;
                                        }
                                        List<object> lst = new List<object>();
                                        lst.Add("Id");
                                        lst.Add(Guid.NewGuid());
                                        if (deviceId != null)
                                        {
                                            lst.Add("DeviceId");
                                            lst.Add(deviceId);
                                        }
                                        if (DeviceEvents[j].AlarmId != null)
                                        {
                                            lst.Add("AlarmId");
                                            lst.Add(DeviceEvents[j].AlarmId);
                                        }
                                        if (DeviceEvents[j].TransactionId != null)
                                        {
                                            lst.Add("TransactionId");
                                            lst.Add(DeviceEvents[j].TransactionId);
                                        }
                                        //if (DeviceEvents[j].Code != null)
                                        {
                                            lst.Add("DeviceActionId");
                                            lst.Add(DeviceEvents[j].Code);
                                        }
                                        if (DeviceEvents[j].Now != null)
                                        {
                                            lst.Add("TimeBegin");
                                            lst.Add(DeviceEvents[j].Now);
                                        }
                                        if (Message != "")
                                        {
                                            lst.Add("Message");
                                            lst.Add(Message);
                                        }
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        try
                                        {
                                            UtilsBase.InsertCommand("Log", connect, null, lst.ToArray());
                                        }
                                        catch (Exception exc)
                                        {
                                            //ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.InsertCommand("Log", connect, null, lst.ToArray());
                                            }
                                            catch (Exception exc1)
                                            {
                                                //ToLog(0, exc1.ToString());
                                            }
                                        }
                                        //Thread.Sleep(10);
                                    }
                                    catch (Exception exc)
                                    {
                                    };
                                    lock (lockersuper)
                                        DeviceEvents.RemoveAt(j);
                                    dcount--;
                                }
                            }
                            catch { }
                        }
                    }
                }
                #endregion Непродолжительные/разовые
                #region Продолжительные
                lock (lockersuper)
                    dcount = DeviceContinues.Count;
                if (dcount > 0)
                {
                    using (SqlConnection connect = new SqlConnection(connectionString))
                    {
                        connect.Open();
                        for (int j = dcount - 1; j >= 0; j--)
                        {
                            try
                            {
                                if ((DeviceContinues[j].To != NullDT) && (DeviceContinues[j].To < Now))
                                {
                                    try
                                    {
                                        string Message = "";
                                        for (int i = 0; i < lcount; i++)
                                        {
                                            if ((lsa[i].dt >= DeviceContinues[j].From) && (lsa[i].dt <= DeviceContinues[j].To))
                                            {
                                                if ((lsa[i].dt >= DeviceContinues[j].From) && (lsa[i].dt <= DeviceContinues[j].To))
                                                    Message += lsa[i].text + Environment.NewLine;
                                            }
                                            if (lsa[i].dt > DeviceContinues[j].To)
                                                break;
                                        }
                                        List<object> lst = new List<object>();
                                        lst.Add("Id");
                                        lst.Add(Guid.NewGuid());
                                        if (deviceId != null)
                                        {
                                            lst.Add("DeviceId");
                                            lst.Add(deviceId);
                                        }
                                        if (DeviceContinues[j].AlarmId != null)
                                        {
                                            lst.Add("AlarmId");
                                            lst.Add(DeviceContinues[j].AlarmId);
                                        }
                                        if (DeviceContinues[j].TransactionId != null)
                                        {
                                            lst.Add("TransactionId");
                                            lst.Add(DeviceContinues[j].TransactionId);
                                        }
                                        //if (DeviceContinues[j].Code != null)
                                        {
                                            lst.Add("DeviceActionId");
                                            lst.Add(DeviceContinues[j].Code);
                                        }
                                        if (DeviceContinues[j].Now != null)
                                        {
                                            lst.Add("TimeBegin");
                                            lst.Add(DeviceContinues[j].Now);
                                        }
                                        if (Message != "")
                                        {
                                            lst.Add("Message");
                                            lst.Add(Message);
                                        }
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        try
                                        {
                                            UtilsBase.InsertCommand("Log", connect, null, lst.ToArray());
                                        }
                                        catch (Exception exc)
                                        {
                                            //ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.InsertCommand("Log", connect, null, lst.ToArray());
                                            }
                                            catch (Exception exc1)
                                            {
                                                //ToLog(0, exc1.ToString());
                                            }
                                        }
                                        //Thread.Sleep(10);
                                    }
                                    catch (Exception exc)
                                    {
                                    };
                                    lock (lockersuper)
                                        DeviceContinues.RemoveAt(j);
                                    dcount--;
                                }
                            }
                            catch { }
                        }
                    }
                    #endregion Продолжительные
                }

                #region Очистка от старых логов
                lock (lockersuper)
                    dcount = DeviceContinues.Count;
                if (dcount > 0)
                    for (int j = dcount - 1; j >= 0; j--)
                        if (FromContinues > DeviceContinues[j].From)
                            FromContinues = DeviceContinues[j].From;
                try
                {
                    //титька
                    DateTime old = DateTime.Now - TimeSpan.FromSeconds(maxWriteTime);
                    old = (old < FromContinues) ? old : FromContinues;
                    lock (lockersuper)
                    {
                        for (int i = 0; i < lcount; i++)
                        {
                            logstr ls;
                            ls = LogQueueSuper.Peek();
                            if (ls.dt < old)
                                LogQueueSuper.Dequeue();
                            else
                                break;
                        }

                        lcount = LogQueueSuper.Count;
                        if (lcount > 50000)
                        {
                            int delta = lcount - 50000;
                            for (int i = 0; i < delta; i++)
                            {
                                LogQueueSuper.Dequeue();
                            }
                        }
                        /*if (DateTime.Now > LastTrim + TimeSpan.FromMinutes(1))
                        {
                            LogQueueSuper.TrimExcess();
                            LastTrim = DateTime.Now;
                        }*/
                    }
                }
                catch { };
                #endregion Очистка от старых логов

                Thread.Sleep(100);
            }
        }
    }
    #endregion Log
    #region IniFile

    public class IniFile
    {
        // ----------------------- CTORS ----------------------------
        public IniFile()
        {
            FileName = "";
            CurrentSection = new Section("");
            mSections.Add(CurrentSection);
        }
        // ----------------------------------------------------------
        public IniFile(string filename) : this()
        {
            ReadFromFile(filename);
        }

        // ----------------------- PUBLIC ---------------------------

        public string GetStr(string keyName, string defValue = "", string sectName = "")
        {
            Section sect = findSection(sectName);
            return sect != null ? sect[keyName].asString : defValue;
        }

        // ----------------------------------------------------------------
        public bool GetBool(string keyName, bool defValue = false, string sectName = "")
        {
            Section sect = findSection(sectName);
            return sect != null ? sect[keyName].asBool : defValue;
        }


        // ----------------------------------------------------------------
        public int GetInt(string keyName, int defValue = 0, string sectName = "")
        {
            Section sect = findSection(sectName);
            return sect != null ? sect[keyName].asInt : defValue;
        }

        // ----------------------------------------------------------------
        public bool Read()
        {
            if (FileName == null)
                return false;
            string line;
            System.IO.StreamReader reader = null;
            try
            {
                reader = new System.IO.StreamReader(FileName);
                while (!reader.EndOfStream)
                {
                    line = reader.ReadLine();
                    if (line == null)
                        continue;
                    if (line.Length > 2 && line[0] == '[' && line[line.Length - 1] == ']')
                    {
                        CurrentSection = new Section(line.Trim(new char[] { '[', ']' }));
                        mSections.Add(CurrentSection);
                        continue;
                    }

                    string[] pair = line.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries);
                    if (pair.Length == 2)
                    {
                        CurrentSection.Add(pair[0].Trim(), pair[1].Trim());
                    }
                }
            }
            catch (System.IO.FileNotFoundException)
            {
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return true;
        }

        // ----------------------------------------------------------
        public bool ReadFromFile(string fileName)
        {
            FileName = fileName;
            return Read();
        }

        // ----------------------------------------------------------
        Section findSection(string name)
        {
            return mSections.Find(delegate (Section sect)
            {
                return sect.Name == name;
            });
        }


        // ----------------------- members --------------------------
        public string FileName
        {
            get; internal set;
        }
        public Section CurrentSection
        {
            get; internal set;
        }
        /*public ReadOnlyCollection<Channels>
        {
            get
            {
                return mSections.AsReadOnly();
            }
        }*/
        private List<Section> mSections = new List<Section>();




        // -------------------- internal classes --------------------
        public class Section
        {
            public Section(string name)
            {
                Name = name;
            }

            public string Name
            {
                get; internal set;
            }
            public bool IsEmpty
            {
                get
                {
                    return mRecords.Count > 0;
                }
            }

            public Value this[int index]
            {
                get
                {
                    return mRecords[index].mValue;
                }
                set
                {
                    mRecords[index].mValue = value;
                }
            }

            public Value this[string keyName]
            {
                get
                {
                    Record rec = findByKey(keyName);
                    return rec != null ? rec.mValue : new Value();
                }
                set
                {
                    Record rec = findByKey(keyName);
                    if (rec != null)
                        rec.mValue = value;
                    else
                        Add(keyName, value.asString);
                }
            }

            // -----------------------------------------------------------------------------------------
            public void Add(string keyName, string value)
            {
                mRecords.Add(new Record(keyName, value));
            }

            // ===============- Private section =======================
            private List<Record> mRecords = new List<Record>();

            // -------------------------------
            Record findByKey(string keyName)
            {
                return mRecords.Find(delegate (Record rec)
                {
                    return rec.mKeyName == keyName;
                });
            }
            // -------------------------------
            private class Record
            {
                internal Record(string keyName, string value)
                {
                    mKeyName = keyName;
                    mValue = new Value(value);
                }
                public string mKeyName
                {
                    get; internal set;
                }
                public Value mValue
                {
                    get; internal set;
                }
            }

            //-----------------------------------------------------------
            public class Value
            {
                public Value()
                {
                    mValue = "";
                }
                public Value(string value)
                {
                    mValue = value;
                }
                public Value(bool value)
                {
                    asBool = value;
                }
                public Value(char value)
                {
                    asChar = value;
                }
                public Value(byte value)
                {
                    asByte = value;
                }
                public Value(int value)
                {
                    asInt = value;
                }
                public Value(uint value)
                {
                    asUint = value;
                }
                public Value(float value)
                {
                    asFloat = value;
                }
                public Value(double value)
                {
                    asDouble = value;
                }
                public Value(decimal value)
                {
                    asDecimal = value;
                }
                public Value(DateTime value)
                {
                    asDateTime = value;
                }

                // --------------------------------------------
                public bool asBool
                {
                    get
                    {
                        return Convert.ToBoolean(mValue);
                    }
                    set
                    {
                        mValue = value.ToString();
                    }
                }
                // --------------------------------------------
                public char asChar
                {
                    get
                    {
                        return Convert.ToChar(mValue);
                    }
                    set
                    {
                        mValue = value.ToString();
                    }
                }
                // --------------------------------------------
                public float asByte
                {
                    get
                    {
                        return Convert.ToByte(mValue);
                    }
                    set
                    {
                        mValue = value.ToString();
                    }
                }
                // --------------------------------------------
                public int asInt
                {
                    get
                    {
                        return Convert.ToInt32(mValue);
                    }
                    set
                    {
                        mValue = value.ToString();
                    }
                }
                // --------------------------------------------
                public uint asUint
                {
                    get
                    {
                        return Convert.ToUInt32(mValue);
                    }
                    set
                    {
                        mValue = value.ToString();
                    }
                }

                // --------------------------------------------
                public float asFloat
                {
                    get
                    {
                        return Convert.ToSingle(mValue);
                    }
                    set
                    {
                        mValue = value.ToString();
                    }
                }

                // --------------------------------------------
                public double asDouble
                {
                    get
                    {
                        return Convert.ToDouble(mValue);
                    }
                    set
                    {
                        mValue = value.ToString();
                    }
                }

                // --------------------------------------------
                public decimal asDecimal
                {
                    get
                    {
                        return Convert.ToDecimal(mValue);
                    }
                    set
                    {
                        mValue = value.ToString();
                    }
                }

                // --------------------------------------------
                public DateTime asDateTime
                {
                    get
                    {
                        return Convert.ToDateTime(mValue);
                    }
                    set
                    {
                        mValue = value.ToString();
                    }
                }

                // --------------------------------------------
                public string asString
                {
                    get
                    {
                        return mValue;
                    }
                    set
                    {
                        mValue = value;
                    }
                }
                // --------------------------------------------
                private string mValue;
            } // class Value
        } // class Section
    } // class IniFile
    #endregion IniFile
#if false
    public class IniFile   // revision 10
    {
        string Path;
        string EXE = Assembly.GetExecutingAssembly().GetName().Name;

        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string Section, string Key, string Value, string FilePath);

        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);

        public IniFile(string IniPath = null)
        {
            Path = new FileInfo(IniPath ?? EXE + ".ini").FullName.ToString();
        }

        public string Read(string Key, string Section = null)
        {
            var RetVal = new StringBuilder(255);
            GetPrivateProfileString(Section ?? EXE, Key, "", RetVal, 255, Path);
            return RetVal.ToString();
        }

        public void Write(string Key, string Value, string Section = null)
        {
            WritePrivateProfileString(Section ?? EXE, Key, Value, Path);
        }

        public void DeleteKey(string Key, string Section = null)
        {
            Write(Key, null, Section ?? EXE);
        }

        public void DeleteSection(string Section = null)
        {
            Write(null, null, Section ?? EXE);
        }

        public bool KeyExists(string Key, string Section = null)
        {
            return Read(Key, Section).Length > 0;
        }
    }
#endif
}
