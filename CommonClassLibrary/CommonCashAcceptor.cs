﻿using System;
using System.IO.Ports;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace CommonClassLibrary
{
    public enum CommonCashAcceptorStatusT
    {
        Unknown,
        // Status
        Enabled,
        Accepting,
        Escrow,
        Stacking,
        VendValid,
        Stacked,
        Rejecting,
        Returning,
        Holding,
        Disabled,
        Initialize,

        Paying = 0x20,
        Collecting,
        Collected,
        PayValid,
        PayStay,
        ReturnToBox,
        ReturnPayOut,

        ReturnError,

        //Power Up Status
        PowerUp,
        PowerUpBillInAcceptor,
        PowerUpBillInStacker,

        //Response Error Status
        StackerFull,
        StackerOpen,
        JamInAcceptor,
        JamInStacker,
        Pause,
        Cheated,
        Failure,
        CommError,

        RecyclerError,
        ACK,
        InvalidCommand,
        ExtendedStatus
    }
    public struct CashAcceptorResultT
    {
        public string CommunicationsErrorStatus;
        public CommonCashAcceptorStatusT S;
        public string Data;
        public string MainMotor;
        public string NoteQualifier;
        public string NoteDiverterer;
        public string NoteTransport;
        public string NoteOutput;
        public string DataHandler;
    }

    public enum CashAcceptorDenominationT : byte { Unknown, Rub10, Rub50, Rub100, Rub500, Rub1000, Rub5000 };
    public enum CashAcceptorRejectingT
    {
        Unknown, InsertingError, MagnetingPatternError, ReturnActionInAcceptor, CalibrationError,
        ConveyngError, DiscriminationError, PhotoPaternError1, PhotoLevelError, ReturnByInhibit,
        Reserved, OperationError, ReturnActionInStacker, LengthError, PhotoPaternError2, TrueBillFeatureError
    };
    public enum CashAcceptorFailureT { Unknown, StackMotorFailure, TransportSpeedFailure, TransportFailure, SolenoidFailure, PBUnitFailure, CashBoxNotReady, CashAcceptorHeadRemove, BOOTROMFailure, ExternalROMFailure, RAMFailure, ExternalROMWritingFailure };
    public struct CashAcceptorStatusT
    {
        public CommonCashAcceptorStatusT Status;

        public bool CommunicationsError;
        public string CommunicationsErrorStatus;
    }

    public struct CurrencyAssign
    {
        public CashAcceptorDenominationT EscrowCode;
        public byte CountryCode;
        public byte Denomination;
        public byte Exp;
    }

    public abstract class CommonCashAcceptor
    {
        public object locker = new object();
        protected SerialPort _serialPort = new SerialPort();
        protected string CommunicationsErrorStatus = "";
        protected CashAcceptorResultT status;
        public string Version;
        public string BootVersion;

        protected bool newNotesInserted;
        protected Log log;

        public bool NewNotesInserted
        {
            get
            {
                return newNotesInserted;
            }
        }

        public CashAcceptorStatusT CashAcceptorStatus
        {
            get
            {
                return _CashAcceptorStatus;
            }
        }
        protected CashAcceptorStatusT _CashAcceptorStatus = new CashAcceptorStatusT();
        public virtual CashAcceptorFailureT Failure
        {
            get
            {
                return CashAcceptorFailureT.Unknown;
            }
        }
        /*public virtual CommonCashAcceptorStatusT Status
        {
            get
            {
                return CommonCashAcceptorStatusT.Unknown;
            }
        }*/
        /*public virtual CashAcceptorDenominationT Denomination
        {
            get
            {
                return CashAcceptorDenominationT.Unknown;
            }
        }*/
        protected byte commModeData = 0;
        public byte CommModeData
        {
            get
            {
                return commModeData;
            }
            set
            {
                commModeData = value;
            }
        }
        protected byte InhibitData = 1;
        public bool Inhibit
        {
            get { return InhibitData != 0; }
        }
        public byte DirectionData
        {
            get;
            set;
        }
        protected BitVector32 optionalData = new BitVector32(0);
        public BitVector32 OptionalData
        {
            get
            {
                return optionalData;
            }
            set
            {
                optionalData = value;
            }
        }
        protected CurrencyAssign[] currAssignData;
        public CurrencyAssign[] CurrAssignData
        {
            get
            {
                return currAssignData;
            }
            set
            {
                currAssignData = value;
            }
        }


        public virtual bool Open(string Port)
        {
            try
            {
                lock (locker)
                {
                    if (!_serialPort.IsOpen)
                    {
                        try
                        {
                            _serialPort.PortName = Port;
                            _serialPort.Open();
                            //_CashAcceptorStatus.CommunicationsError = false;
                            return true;
                        }
                        catch (Exception exc)
                        {
                            if (log != null)
                            {

                                log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCashAcceptor Open: " + exc.ToString());
                            }
                            _CashAcceptorStatus.CommunicationsError = true;
                            _CashAcceptorStatus.CommunicationsErrorStatus = Properties.Resources.CashAcceptorCommunicationError;
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                {

                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCashAcceptor Open: " + exc.ToString());
                }
                _CashAcceptorStatus.CommunicationsError = true;
                _CashAcceptorStatus.CommunicationsErrorStatus = Properties.Resources.CashAcceptorCommunicationError;
                return false;
            }
        }
        public virtual bool Close()
        {
            try
            {
                lock (locker)
                {
                    if (_serialPort.IsOpen)
                    {
                        try
                        {
                            _serialPort.Close();
                            return true;
                        }
                        catch (Exception exc)
                        {
                            if (log != null)
                            {

                                log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCashAcceptor Close: " + exc.ToString());
                            }
                            _CashAcceptorStatus.CommunicationsErrorStatus = Properties.Resources.CashRecyclerCommunicationError;
                            _CashAcceptorStatus.CommunicationsError = true;
                            return false;
                        }
                    }
                    else
                        return true;
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                {

                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCashAcceptor Close: " + exc.ToString());
                }
                _CashAcceptorStatus.CommunicationsErrorStatus = Properties.Resources.CashRecyclerCommunicationError;
                _CashAcceptorStatus.CommunicationsError = true;
                return false;
            }
        }
        public bool Speed19200
        {
            get;
            set;
        }

        public bool IsOpen
        {
            get
            {
                if (_serialPort != null)
                    return _serialPort.IsOpen;
                else
                    return false;
            }
        }
        //private Collection<decimal> OnOfNominals = new Collection<decimal>();
        //private Collection<decimal> SecurytyNominals = new Collection<decimal>();

        public void Initialize()
        {

            lock (locker)
            {
                // Allow the user to set the appropriate properties.
                if (Speed19200)
                    _serialPort.BaudRate = 19200;
                else
                    _serialPort.BaudRate = 9600;
                _serialPort.Parity = Parity.Even;
                _serialPort.DataBits = 8;
                _serialPort.StopBits = StopBits.One;
                _serialPort.Handshake = Handshake.None;

                // Set the read/write timeouts
                _serialPort.ReadTimeout = 200;
                _serialPort.WriteTimeout = 200;
            }
        }
        public virtual bool Wait_Operation()
        {
            return true;
        }
        public virtual bool SendACK()
        {
            return true;
        }
        public virtual bool Reset()
        {
            return true;
        }
        public virtual bool Stack_1_Operation()
        {
            return true;
        }
        /*public virtual bool Stack_2_Operation()
        {
            return true;
        }*/
        public virtual bool Return_Operation()
        {
            return true;
        }
        public virtual bool Hold_Operation()
        {
            return true;
        }
        /*public virtual bool OnOffSetting(Collection<decimal> Nominals)
        {
            return true;
        }*/
        public virtual bool OnOffSetting(int NominalsCode)
        {
            return true;
        }
        /*public virtual bool SecuritySetting(Collection<decimal> Nominals)
        {
            return true;
        }*/
        public virtual bool SecuritySetting(int NominalsCode)
        {
            return true;
        }
        public virtual bool CommModeSetting(byte Data)
        {
            return true;
        }
        public virtual bool InhibitOn()
        {
            return true;
        }
        public virtual bool InhibitOff()
        {
            return true;
        }
        public virtual bool DirectionSetting(byte Data)
        {
            return true;
        }
        public virtual bool OptionalSetting(BitVector32 Data)
        {
            return true;
        }
        public virtual bool OnOffStatusReq()
        {
            return true;
        }
        public virtual bool SecurityStatusReq()
        {
            return true;
        }
        public virtual bool CommModeStatusReq()
        {
            return true;
        }
        public virtual bool InhibitStatusReq()
        {
            return true;
        }
        public virtual bool DirectionStatusReq()
        {
            return true;
        }
        public virtual bool OptionalStatusReq()
        {
            return true;
        }
        public virtual bool VersionReq()
        {
            return true;
        }
        public virtual bool BootVersionReq()
        {
            return true;
        }
        public virtual bool CurruncyAssStatusReq()
        {
            return true;
        }
        public virtual decimal GetNominalNote()
        {
            return 0;
        }
        public virtual int GetNominalCode()
        {
            return 0;
        }
    }
}
