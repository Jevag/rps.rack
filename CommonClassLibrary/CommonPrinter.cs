﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;

namespace CommonClassLibrary
{
    public abstract class CommonPrinter
    {
        public object locker = new object();
        protected internal SerialPort _serialPort;
        Thread StatusThread;
        bool run = false;

        protected Log log;
        public AllPayParams Params;

        public enum ErrorCode
        {
            Sucsess = 0,
            NotConnect,
            DeviceDisabled,
            LoadError,
            PortAccessError,
            DeviceNotWork,
            CommunicationError,
            NearPaperEnd,
            PaperEnd,
            OtherError,
            InvalidHandle,
        }

        List<string> Items = null;

        public class ResultT
        {
            public bool deviceEnabled;
            public HardStatusT hardStatus;
            public ErrorCode resultCode;
            public string resultDescription;
            public bool boolResult;
        }
        public enum CommandT { Undefined, PrintCheck, PrintNotCheck };
        protected internal CommandT _Command;
        /*public CommandT Command
        {
            get
            {
                return _Command;
            }
        }*/
        protected internal CommandStatusT _CommandStatus;
        /*public CommandStatusT CommandStatus
        {
            get
            {
                return _CommandStatus;
            }
            set
            {
                _CommandStatus = value;
            }
        }*/

        protected ResultT result = new ResultT();
        /*public virtual ResultT Result
        {
            get
            {
                return result;
            }
        }*/

        public struct HardStatusT
        {
            public char RawStatus;
            //public char RawRTStatus;
            //public bool StatusOK;
            public bool CoverOpen;
            //public bool PaperTransporByHand;
            //public bool StopByPaperEnd;
            public bool Error;
            //public bool PrinterMechError;
            public bool PrinterCutMechError;
            //public bool PrinterFatalError;
            public bool PrinterHeadHot;
            public bool PaperNearEnd;
            public bool PaperEnd;
        };
        protected HardStatusT hardStatus;
        public HardStatusT HardStatus
        {
            get
            {
                return hardStatus;
            }
        }
        public enum FlowControlT
        {
            DTR_DSR,
            RTS_CTS,
            XON_XOFF,
            NONE
        }
        protected bool pool = false;
        public bool Pool
        {

            set
            {
                pool = value;
            }
        }
        protected ErrorCode resultCode;
        public ErrorCode ResultCode
        {
            get
            {
                return resultCode;
            }
        }
        protected string resultDescription;
        public string ResultDescription
        {
            set
            {
                resultDescription = value;
            }
            get
            {
                return resultDescription;
            }
        }

        protected bool deviceEnabled;
        public bool DeviceEnabled
        {
            get
            {
                return deviceEnabled;
            }
        }



        public virtual bool Open()
        {
            deviceEnabled = true;
            return true;
        }
        public virtual bool Open(string Name, int ComBaudrate, int ComDataBits, int ComStopBits, Parity ComParity, FlowControlT FlowControl)
        {
            deviceEnabled = true;
            return true;
        }
        public virtual bool Close()
        {
            deviceEnabled = false;
            return true;
        }
        //проверка включен ли принтер
        bool _GetStatus()
        {
            result = getStatus();
            hardStatus = result.hardStatus;
            if (result.resultCode != ErrorCode.Sucsess)
            {
                Close();
                deviceEnabled = false;
            }
            return result.boolResult;
        }
        public bool GetStatus()
        {
            return _GetStatus();
        }
        protected virtual ResultT getStatus()
        {
            ResultT res = new ResultT();
            return res;
        }

        bool _PrintCheck()
        {
            printCheck(Params);
            return true;
        }
        public bool PrintCheck(AllPayParams Params)
        {
            this.Params = Params;
            return _PrintCheck();
        }
        protected virtual ResultT printCheck(AllPayParams Params)
        {
            ResultT res = new ResultT();
            return res;
        }

        bool _PrintNotCheck(List<string> Items)
        {
            printNotCheck(Items);
            return true;
        }
        public bool PrintNotCheck(List<string> Items)
        {
            this.Items = Items;
            return _PrintNotCheck(Items);
        }
        protected virtual ResultT printNotCheck(List<string> Items)
        {
            ResultT res = new ResultT();
            return res;
        }

        /*bool _PrintSimbol(string s)
        {
            printSimbol(s);
            return true;
        }
        public bool PrintSimbol(string s)
        {
            return _PrintSimbol(s);
        }
        protected virtual ResultT printSimbol(string s)
        {
            ResultT res = new ResultT();
            return res;
        }*/

        bool _Cut()
        {
            cut();
            return true;
        }
        public bool Cut()
        {
            return _Cut();
        }
        protected virtual ResultT cut()
        {
            ResultT res = new ResultT();
            return res;
        }

        void PoolThread()
        {
            while (run)
            {
                if (pool)
                {
                    if (_CommandStatus == CommandStatusT.Run)
                    {
                        switch (_Command)
                        {
                            case CommandT.PrintCheck:
                                _CommandStatus = CommandStatusT.Running;
                                _PrintCheck();
                                _Command = CommandT.Undefined;
                                _CommandStatus = CommandStatusT.Completed;
                                break;
                            case CommandT.PrintNotCheck:
                                _CommandStatus = CommandStatusT.Running;
                                _PrintNotCheck(Items);
                                _Command = CommandT.Undefined;
                                _CommandStatus = CommandStatusT.Completed;
                                break;
                        }
                    }
                    else if (_CommandStatus == CommandStatusT.Undeifned)
                    {
                        _GetStatus();
                    }
                }
                Thread.Sleep(1000);
            }
        }
    }
}