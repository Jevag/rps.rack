﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;

namespace CommonClassLibrary
{

    public class CommonBankModule
    {
        #region BankModuleLog
        public enum BMLogType
        {
            LOG_SIMPLE = 0,
            LOG_HEX
        }


        protected void deleteOldBMLogs()
        {
            toLog("deleteOldBMLogs -> START");

            int COUNT_OF_DAYS_FOR_STORE_LOGS = 14;
            string[] sFiles = Directory.GetFiles(@"./", "BankModule_*.log", SearchOption.TopDirectoryOnly);
            int nCurrentDayOfYear = DateTime.Now.DayOfYear;
            try
            {
                toLog("deleteOldBMLogs -> Count log files = " + sFiles.Length.ToString());

                for (int i = 0; i < sFiles.Length; ++i)
                {
                    string sDate = sFiles[i].Split(new string[] { "BankModule_" }, StringSplitOptions.None)[1];
                    sDate = sDate.Split(new string[] { ".log" }, StringSplitOptions.None)[0];
                    DateTime oDate = DateTime.ParseExact(sDate, "yyyy_MM_dd", null);
                    int nDayOfYear = oDate.DayOfYear;
                    if (nCurrentDayOfYear - nDayOfYear > COUNT_OF_DAYS_FOR_STORE_LOGS)
                    {
                        File.Delete(sFiles[i]);
                        toLog("Delete old file log: " + sFiles[i]);
                    }
                }

                toLog("deleteOldBMLogs -> FINISH");
            }
            catch (Exception e)
            {
                toLog("Delete error: " + e.Message);
            }
        }
        private static string ToHexString(string str)
        {
            byte[] ba = Encoding.ASCII.GetBytes(str);
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:X2}.", b);
            return hex.ToString();
        }

        protected void writeBMLogs(string sMessage)
        {
            try
            {
                string exePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                string currDate = DateTime.Now.ToString("yyyy_MM_dd");
                string fDebugLogName = exePath + "\\BankModule_" + currDate + ".log";
                StreamWriter fDebugLog = new StreamWriter(fDebugLogName, true, Encoding.Default);
                fDebugLog.WriteLine(sMessage);
                fDebugLog.Flush();
                fDebugLog.Close();

            }
            catch
            { }
        }
        protected void toLog(string sMessage, BMLogType pType = BMLogType.LOG_SIMPLE)
        {
            StackFrame CallStack = new StackFrame(1, true);
            string sDate = DateTime.Now.ToString("HH:mm:ss.fff ");
            string sNameMethod = CallStack.GetMethod().Name;
            if (sNameMethod.Length > 14)
                sNameMethod = sNameMethod.Substring(0, 14);
            sNameMethod = sNameMethod.PadRight(15);
            string sLineNumber = CallStack.GetFileLineNumber().ToString("D4");
            string sType = type.ToString().PadRight(6);
            string sPrefix = sDate + "[" + sType + sNameMethod + sLineNumber + "] ";

            switch (pType)
            {
                case BMLogType.LOG_SIMPLE:
                    LogSimple(sPrefix, sMessage, false);
                    break;
                case BMLogType.LOG_HEX:
                    LogWithHex(sPrefix, sMessage, false);
                    break;
            }

        }
        protected void LogWithHex(string sPrefix, string sMessage, bool isConsoleUse = false)
        {
            try
            {
                if (sMessage.Length <= 1)
                {
                    writeBMLogs(sPrefix + sMessage);
                    return;
                }

                string[] sArrayMes = sMessage.Split(new char[] { '\n' }, StringSplitOptions.None);
                string[] sArrayMesHex = new string[sArrayMes.Length];

                for (int i = 0; i < sArrayMes.Length; ++i)
                {
                    string sRes = sPrefix + sArrayMes[i];
                    sArrayMesHex[i] = ToHexString(sArrayMes[i]);
                    writeBMLogs(sRes);
                }

                string sResHex = "";
                foreach (string s in sArrayMesHex)
                    sResHex += s + Environment.NewLine;
                LogSimple(sPrefix, sResHex);
            }
            catch { }
        }
        protected void LogSimple(string sPrefix, string sMessage, bool isConsoleUse = false)
        {
            try
            {
                string sDateForCommonLog = DateTime.Now.ToString("yyyy'.'MM'.'dd HH:mm:ss.fff ");
                if (log != null)
                    log.ToLog(2, sDateForCommonLog + " " + sMessage);

                string[] sArrayMes = sMessage.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                for (int i = 0; i < sArrayMes.Length; ++i)
                {
                    string sRes = sPrefix + sArrayMes[i];
                    if (isConsoleUse)
                        Console.WriteLine(sRes);
                    writeBMLogs(sRes);
                }
            }
            catch { }
        }



        #endregion

        #region BankModuleType       
        public enum BM_TYPE
        {
            NONE = -1,
            GPB = 0,
            SBRF = 1,
            TRPOS = 2,
            UNITELLER = 3
        }

        public enum ResponseCode
        {
            // SUCCESS
            Sucsess = 0,

            // SPECIAL USAGE
            SpecialCode,

            // ERROR
            UserError,
            SysytemError,
            OtherError,
        }

        protected internal BM_TYPE type = BM_TYPE.NONE;
        public BM_TYPE Type
        {
            get
            {
                return type;
            }
        }
        public bool isBMSync()
        {
            switch (type)
            {
                case BM_TYPE.TRPOS:
                    return true;
                default:
                    return false;
            }
        }
        #endregion

        // Only for "Stoyka" solution
        #region BankModuleStatus 
        public enum BM_STATUS
        {
            UNKNOWN,
            WAITING_COMMAND,
            GET_COMMAND,
            INSERT_CARD
        }

        protected internal BM_STATUS BMStatus = BM_STATUS.UNKNOWN;

        public BM_STATUS Status
        {
            get
            {
                return BMStatus;
            }
        }

        #endregion

        public object locker = new object();
        protected Log log;
        public CommonBankModule()
        {
        }
        public CommandStatusT CommandStatus
        {
            get;
            set;
        }
        protected internal bool cardInserted = false;
        public bool CardInserted
        {
            get
            {
                return cardInserted;
            }
            set
            {
                cardInserted = value;
            }
        }

        protected internal bool runing = false;
        public bool Runing
        {
            get
            {
                return runing;
            }
            set
            {
                runing = value;
            }
        }

        protected internal bool isCancel = false;
        public bool IsCancel
        {
            get
            {
                return isCancel;
            }
            set
            {
                isCancel = value;
            }
        }

        protected internal bool isCancelled = false;
        public bool IsCancelled
        {
            get
            {
                return isCancelled;
            }
            set
            {
                isCancelled = value;
            }
        }

        protected internal bool isVerification = false;
        public bool IsVerification
        {
            get
            {
                return isVerification;
            }
            set
            {
                isVerification = value;
            }
        }

        protected internal bool busy = false;
        public bool Busy
        {
            get
            {
                return busy;
            }
            set
            {
                busy = value;
            }
        }

        protected internal bool cardWasInserted = false;
        public bool CardWasInserted
        {
            get
            {
                return cardWasInserted;
            }
        }
        public bool AuthorizationStarted
        {
            get; set;
        }
        public enum ErrorCode
        {
            // SUCCESS
            Sucsess = 0,

            // FOR GPB -> START
            AtmPartial,
            SysytemError,
            ResponseError,
            CardNotSupported,
            CardIsDelayed,
            CallSecurity1,
            CardIsBlocked,
            CallIssuer2,
            RequestDeniedByBank,
            CardNotEnoughMoney,
            IncorrectPINCode,
            CardIsDamaged,
            ErrorPINCodeHandling,
            CardIsInvalid,
            InvalidChequeNumber,
            TransactionNotFound,
            InvalidSumm,
            InvalidOriginalSumm,
            InvalidRRN,
            InvalidPIN,
            InvalidCVV,
            InvalidCurrency,
            InvalidAuth,
            InvalidCard,
            InvalidCashierCard,
            InvalidManualCard,
            InvalidCardType,
            InvalidOperation,
            ChequeNumberNeeded,
            ErrorFormatTrack,
            CardNotReaded,
            ErrorChipCard,
            IncorrectSumm,
            CancellNotLast,
            InvalidCancell,
            CancellError,
            ErrorLoadWorkKey,
            SomeElseCard,
            ShiftClaseNeeded,
            AsyncStart,
            BankNotAvailable,
            BankEmitentNotAvailable,
            PaymentSystemNotAvailable,
            PaymentSystemNotResponse,
            RequestTimeOut,
            ConnectionError,
            PINCountLimited,
            SessionOpened,
            SessionClosed,
            ProcessingError,
            // FOR GPB -> STOP

            // OTHER ERROR
            OtherError,
            OtherError1000 = 1000,

            // FOR SBRF
            SBRF_PROC_ERROR,

            // FOR TRPOS -> START
            TRPOS_PROC_ERROR_OPEN_CFG_FILE_RC_1,
            TRPOS_PROC_ERROR_LOAD_INPUT_PARAMS_RC_2,
            TRPOS_PROC_ERROR_CREATE_OUT_PARAMS_RC_3,
            TRPOS_PROC_ERROR_CREATE_RECEIPT_RC_4,
            TRPOS_PROC_ERROR_OPEN_POS_PORT_RC_5,
            TRPOS_PROC_ERROR_FUNC_PARAMS_RC_6,
            TRPOS_PROC_ERROR_INPUT_PARAMS_RC_7,
            TRPOS_PROC_ERROR_SYSTEM_RC_8,
            TRPOS_PROC_ERROR_POS_NOT_READY_RC_9,
            TRPOS_PROC_ERROR_TIMEOUT_RC_80_FF,
            TRPOS_PROC_ERROR_INIT_RC_OTHER,
            TRPOS_PROC_ERROR_TIMEOUT_BY_TIMER,

            TRPOS_CHECK_STATUS_OPERATION_UNKNOWN,
            TRPOS_CHECK_STATUS_OPERATION_COMPLETED,
            TRPOS_CHECK_STATUS_OPERATION_NOT_COMPLETED,

            TRPOS_VOID_OPERATION_UNKNOWN,
            TRPOS_VOID_OPERATION_COMPLETED,
            TRPOS_VOID_OPERATION_NOT_COMPLETED,

            TRPOS_RESPONCE_CODE_RT,
            TRPOS_RESPONCE_CODE_TT,
            TRPOS_RESPONCE_CODE_B4,
            TRPOS_RESPONCE_CODE_B5,
            TRPOS_RESPONCE_CODE_B9,
            TRPOS_RESPONCE_CODE_CE,
            TRPOS_RESPONCE_CODE_OTHER
            // FOR TRPOS -> END
        }


        protected int rawResultCode;
        public int RawResultCode
        {
            get
            {
                return rawResultCode;
            }
        }
        protected ErrorCode resultCode;
        
        public ErrorCode ResultCode
        {
            get
            {
                return resultCode;
            }
        }
        
        protected ResponseCode resultCodeU;
        public ResponseCode ResultCodeU
        {
            get
            {
                return resultCodeU;
            }
        }


        protected string resultDescription;
        public string ResultDescription
        {
            get
            {
                return resultDescription;
            }
        }
        
        protected ErrorCode checkResultCode;
        public ErrorCode CheckResultCode
        {
            get
            {
                return checkResultCode;
            }
        }
        
        protected ResponseCode checkResultCodeU;
        public ResponseCode CheckResultCodeU
        {
            get
            {
                return checkResultCodeU;
            }
        }


        protected string checkResultDescription;
        public string CheckResultDescription
        {
            get
            {
                return checkResultDescription;
            }
        }
        protected string versionDescription;
        public string VersionDescription
        {
            get
            {
                return versionDescription;
            }
        }
        public string AuthReceipt
        {
            get;
            set;
        }
        public string AuthResult
        {
            get;
            set;
        }

        public bool TransactionStarted
        {
            get; set;
        }
        protected string authRequest;
        public string AuthRequest
        {
            get
            {
                return authRequest;
            }
        }
        protected string _status;
        public string StatusS
        {
            get
            {
                return _status;
            }
        }
        /*public bool IsCancell
        {
            set;
            get;
        }*/

        protected bool deviceEnabled;
        public bool DeviceEnabled
        {
            get
            {
                return deviceEnabled;
            }
        }
        public int KKMNumber
        {
            set;
            get;
        }
        public int ChequeNumber
        {
            get;
            set;
        }
        public decimal AmountDue
        {
            set;
            get;
        }
        public virtual bool Open()
        {
            return true;
        }
        public virtual bool Close()
        {
            return true;
        }
        public virtual bool StartSession()
        {
            return true;
        }
        public virtual bool StopSession()
        {
            return true;
        }
        public virtual string GetVersionDescription()
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return "0";
        }
        public bool Purchase(decimal AmountDue)
        {
            this.AmountDue = AmountDue;
            return Purchase();
        }
        public virtual bool Purchase()
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }
        public virtual bool Verification()
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }
        public virtual bool WorkKey()
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }
        public virtual bool ChequeByNum(int chequeNum)
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }
        public virtual bool TechnicalCancelation()
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }
        /*public virtual int GetVersion()
       {
           return 0;
       }*/
        public virtual int GetLastError()
        {
            return 0;
        }
        /*public virtual string GetErrorDescription(int nRet)
        {
            return "";
        }*/
        /*public virtual bool LongReport()
       {
           return true;
       }*/
        /*public virtual bool ShortReport()
        {
            return true;
        }*/
        public virtual bool TerminalInfo()
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }
        public virtual bool LoadSW()
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }
        public virtual bool LoadParams()
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }
        public virtual bool EchoTest()
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }
        public virtual bool CleanCache()
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }
        /*public virtual bool Revert()
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }*/
        public virtual bool Cancel(int ChequeNumber, decimal AmountDue)
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }

        public virtual bool Cancel(string RRN, decimal AmountDue)
        {
            AuthReceipt = "Emulator";
            resultDescription = "0 OK";
            return true;
        }
        public virtual string GetOpStatus(bool IsCancel = false)
        {
            return "";
        }
    }
}