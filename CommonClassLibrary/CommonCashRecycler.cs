﻿using System.Collections.ObjectModel;

namespace CommonClassLibrary
{
    public enum CashRecyclerEStatusT
    {
        Unknown,
        //Status
        Unconnected,
        Normal,
        Empty,
        Full,
        Busy,

        //Error Status
        RecyclerJam,
        DoorOpen,
        MotorError,
        EEPROMError,
        PayOutNoteError,
        RecycleBoxOpen,
        HardwareError
    }

    public abstract class CommonCashRecycler : CommonCashAcceptor, CommonCashDispenserI
    {
        /*protected bool dispenseByOneNote;
        public bool DispenseByOneNote
        {
            get
            {
                return dispenseByOneNote;
            }
            set
            {
                dispenseByOneNote = value;
            }
        }*/

        protected int[] notesCount = new int[2];
        public int[] NotesCount
        {
            get
            {
                return notesCount;
            }
        }
        protected bool notesCountChanged = false;
        public bool NotesCountChanged
        {
            get
            {
                return notesCountChanged;
            }
            set
            {
                notesCountChanged = value;
            }
        }

        protected bool pool = false;
        public bool Pool
        {

            set
            {
                pool = value;
            }
        }

        protected byte[] countData;
        protected CashDispenserResultT cashDispenserResult = new CashDispenserResultT();
        public CashDispenserResultT CashDispenserResult
        {
            get
            {
                return cashDispenserResult;
            }
        }
        //protected CashRecyclerEStatusT _RecyclerEStatus;
        protected CashDispenserCurrencyT[] cashDispenserCurrency = new CashDispenserCurrencyT[2];
        public CashDispenserCurrencyT[] CashDispenserCurrency
        {
            get
            {
                return cashDispenserCurrency;
            }
        }
        /*protected CashDispenserCassetteT[] cashDispenserCassettes = new CashDispenserCassetteT[2];
        public CashDispenserCassetteT[] CashDispenserCassettes
        {
            get
            {
                return cashDispenserCassettes;
            }
        }*/

        private Collection<decimal> OnOfNominals = new Collection<decimal>();

        /*public virtual bool Reset()
        {
            return true;
        }*/
        public virtual bool SetNoteNominal(int BoxNumber, string Currecny, int Value)
        {
            return true;
        }
        public virtual bool SetNoteSize(int BoxNumber, int Width, int Length)
        {
            return true;
        }

        public virtual bool MoveForward(int UpperCount, int LowerCount)
        {
            return true;
        }
        public virtual bool CheckDeliveredNotes()
        {
            return true;
        }
        public virtual bool CheckStatus()
        {
            return true;
        }
        public virtual bool CloseCassette()
        {
            return true;
        }
        public virtual bool OpenCassette()
        {
            return true;
        }
        public virtual bool ReadCassetteID()
        {
            return true;
        }
        /*public virtual bool Stack_3_Operation()
        {
            return true;
        }*/
        public virtual bool RecyclerRefillReq()
        {
            return true;
        }
        public virtual bool SetMaxCount(int BoxNumber, int Value)
        {
            return true;
        }

        public virtual bool SetCount(int BoxNumber, int Value)
        {
            return true;
        }
        public virtual bool GetCount()
        {
            return true;
        }
        public virtual bool GetMaxCount()
        {
            return true;
        }
    }
}