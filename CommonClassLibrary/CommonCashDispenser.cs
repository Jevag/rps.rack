﻿using System;
using System.IO.Ports;

namespace CommonClassLibrary
{
    public enum CashDispenserCommandT { Reset, SetNoteNominal, SetNoteSize, MoveForward, CheckDeliveredNotes, CloseCassette, OpenCassette, ReadCassetteID };
    public enum CashDispenserStatusT
    {
        Unknown, SuccessfulCommand, LowLevel, RejectedNotes,
        RejectVaultAlmostFull, NotesInDeliveryThroat, ModuleNeedService, NoMessageToResend,
        TransmissionError, IllegalCommand, CommunicationsTimeOut, CassetteNotIdentified,
        EmptyCassette, MachineNotOpened, FeedCassetteNotPresent, RejectVaultNotPresent, TooManyNotesRequested, RejectVaultFull,
        DiverterFailure, JamInNoteQualifier, JamInNoteFeederTransport, MainMotorFailure, NoteQualifierFaulty, ErrorInThroat, SensorError, InternalFailure, ErrorInNoteTransport,
        FailureToFeed, NoteFeederExitSensorFailure, CommunicationsTimeOut1, CommunicationsTimeOut2, Busy, DoorOpen, EEPROMError, Full, HardwareError, RecycleBoxOpen
    }
    public struct CashDispenserCassetteT
    {
        public string HopperNumber;
        public CashDispenserStatusT HopperStatus;
        public int Count;
        public char OpenStatus;
        public string CassetteID;
        public string CasseteError;
    }
    public struct CashDispenserResultT
    {
        public string CommunicationsErrorStatus;
        public CashDispenserStatusT S;
        public string Data;
        public CashDispenserCassetteT[] Cassettes;
        public string MainMotor;
        public string NoteQualifier;
        public string NoteDiverterer;
        public string NoteTransport;
        public string NoteOutput;
        public string DataHandler;
    }
    public struct CashDispenserCurrencyT
    {
        public int Value;
        public byte Denomintion1;
        public byte Denomintion2;
        public byte BoxNo;

    }

    public interface CommonCashDispenserI
    {


        int[] NotesCount
        {
            get;
        }
        bool Pool
        {

            set;
        }
        /*CashDispenserCommandT CashDispenserCommand
        {
            get;
        }*/
        /*CommandStatusT CommandStatus
        {
            get;
        }*/
        /*CashDispenserCassetteT[] CashDispenserCassettes
        {
            get;
        }*/

        CashDispenserResultT CashDispenserResult
        {
            get;
        }
        bool NotesCountChanged
        {
            get;
            set;
        }

/*        bool DispenseByOneNote
        {
            get;
            set;
        }*/

        bool IsOpen
        {
            get;
        }

        bool Open(string Port);
        bool Close();
        bool Reset();
        bool SetNoteNominal(int BoxNumber, string Currecny, int Value);
        bool SetNoteSize(int BoxNumber, int Width, int Length);
        bool MoveForward(int UpperCount, int LowerCount);
        bool CheckDeliveredNotes();
        bool CheckStatus();
        bool CloseCassette();
        bool OpenCassette();
        bool ReadCassetteID();
        bool SetCount(int BoxNumber, int Value);
        bool SetMaxCount(int BoxNumber, int Value);
        bool GetCount();
        bool GetMaxCount();
    }
    public abstract class CommonCashDispenser : CommonCashDispenserI
    {
        public object locker = new object();
        protected Log log;
        protected int[] notesCount = new int[2];
        public int[] NotesCount
        {
            get
            {
                return notesCount;
            }
        }
        protected bool notesCountChanged = false;
        public bool NotesCountChanged
        {
            get
            {
                return notesCountChanged;
            }
            set
            {
                notesCountChanged = value;
            }
        }
        protected bool pool = false;
        public bool Pool
        {

            set
            {
                pool = value;
            }
        }

        /*protected bool dispenseByOneNote;
        public bool DispenseByOneNote
        {
            get
            {
                return dispenseByOneNote;
            }
            set
            {
                dispenseByOneNote = value;
            }
        }&/
        protected CashDispenserCurrencyT[] cashDispenserCurrency = new CashDispenserCurrencyT[2];
        public CashDispenserCurrencyT[] CashDispenserCurrency
        {
            get
            {
                return cashDispenserCurrency;
            }
        }
        /*protected CashDispenserCassetteT[] cashDispenserCassettes = new CashDispenserCassetteT[2];
        public CashDispenserCassetteT[] CashDispenserCassettes
        {
            get
            {
                return cashDispenserCassettes;
            }
        }*/


        protected CashDispenserResultT cashDispenserResult;
        public CashDispenserResultT CashDispenserResult
        {
            get
            {
                return cashDispenserResult;
            }
        }

        protected int UpperCount, LowerCount, BoxNumber, Width, Length, Value;
        protected string Currecny;
        protected SerialPort _serialPort;
        protected string CommunicationsErrorStatus = "";

        /*public CashDispenserCommandT CashDispenserCommand
        {
            get
            {
                return _CashDispenserCommand;
            }
        }*/
        public CommonCashDispenser()
        {
            //_CardDispenserStatus = new cashDispenserResult();
            //DespenserRes = new byte[100];

            // Create a new SerialPort object with default settings.
            _serialPort = new SerialPort();

            // Allow the user to set the appropriate properties.
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.Even;
            _serialPort.DataBits = 7;
            _serialPort.StopBits = StopBits.Two;
            _serialPort.Handshake = Handshake.None;

            // Set the read/write timeouts
            _serialPort.ReadTimeout = 1000;
            _serialPort.WriteTimeout = 1000;

        }
        public bool IsOpen
        {
            get
            {
                return _serialPort.IsOpen;
            }
        }

        public virtual bool Open(string Port)
        {
            try
            {
                if (!_serialPort.IsOpen)
                {
                    try
                    {
                        _serialPort.PortName = Port;
                        _serialPort.Open();
                        CommunicationsErrorStatus = "";
                        cashDispenserResult.CommunicationsErrorStatus = CommunicationsErrorStatus;
                        return true;
                    }
                    catch (Exception exc)
                    {
                        if (log != null)
                        {
                            
                                log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCashDispenser Open: " + exc.ToString());
                        }
                        CommunicationsErrorStatus = Properties.Resources.CashDispenserCommunicationError;
                        cashDispenserResult.CommunicationsErrorStatus = CommunicationsErrorStatus;
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCashDispenser Open: " + exc.ToString());
                }
                CommunicationsErrorStatus = Properties.Resources.CashDispenserCommunicationError;
                cashDispenserResult.CommunicationsErrorStatus = CommunicationsErrorStatus;
                return false;
            }
        }
        public virtual bool Close()
        {
            try
            {
                if (_serialPort.IsOpen)
                {
                    try
                    {
                        _serialPort.Close();
                        CommunicationsErrorStatus = "";
                        cashDispenserResult.CommunicationsErrorStatus = CommunicationsErrorStatus;
                        return true;
                    }
                    catch (Exception exc)
                    {
                        if (log != null)
                        {
                            
                                log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCashDispenser Close: " + exc.ToString());
                        }
                        CommunicationsErrorStatus = Properties.Resources.CashDispenserCommunicationError;
                        cashDispenserResult.CommunicationsErrorStatus = CommunicationsErrorStatus;
                        return false;
                    }
                }
                else
                    return true;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCashDispenser Close: " + exc.ToString());
                }
                CommunicationsErrorStatus = Properties.Resources.CashDispenserCommunicationError;
                cashDispenserResult.CommunicationsErrorStatus = CommunicationsErrorStatus;
                return false;
            }
        }
        public virtual bool SetCount(int BoxNumber, int Value)
        {
            return true;
        }
        public virtual bool SetMaxCount(int BoxNumber, int Value)
        {
            return true;
        }
        public virtual bool Reset()
        {
            return true;
        }
        public virtual bool SetNoteNominal(int BoxNumber, string Currecny, int Value)
        {
            return true;
        }
        public virtual bool SetNoteSize(int BoxNumber, int Width, int Length)
        {
            return true;
        }
        public virtual bool MoveForward(int UpperCount, int LowerCount)
        {
            return true;
        }
        public virtual bool CheckDeliveredNotes()
        {
            return true;
        }
        public virtual bool CheckStatus()
        {
            return true;
        }
        public virtual bool CloseCassette()
        {
            return true;
        }
        public virtual bool OpenCassette()
        {
            return true;
        }
        public virtual bool ReadCassetteID()
        {
            return true;
        }
        public virtual bool GetCount()
        {
            return true;
        }
        public virtual bool GetMaxCount()
        {
            return true;
        }
    }
}