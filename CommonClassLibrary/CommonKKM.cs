﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;

namespace CommonClassLibrary
{

    public class CommonKKM
    {
        public object locker = new object();
        public Log log;
        public CommonKKM(Log Log = null)
        {
            log = Log;
        }
        Thread StatusThread;
        private bool run = false;
        Encoding enc = Encoding.UTF8;
        Encoding enc_dos = Encoding.GetEncoding("CP866");
        DateTime LastLog;

        public string LastQR
        {
            get;
            set;
        }

        public int CheckNum
        {
            get;
            set;
        }

        public string LastBillNumber
        {
            get;
            set;
        }

        public bool Initialised
        {
            get;
            set;
        }
        public bool StatusThreadrun
        {
            get;
            set;
        }
        public bool Work
        {
            get;
            set;
        }
        public enum ErrorCode
        {
            Sucsess = 0,
            IncorrectMessageFormat,
            IncorrectFieldFormat,
            IncorrectDateFormat,
            IncorrectBCC,
            IncorrectPass,
            MaxSessionTime,
            MaxFieldLength,
            MaxMessageLength,
            IncorrectState,
            ZReportNeeded,
            FiskalMemoryError,
            FiskalMemoryFull,
            OpenSessionNeeded,
            SessionAlreadyOpen,
            NeedPrintKL,
            NotConnect,
            KeyNotFound,
            UserCancell,
            IncorrectValue,
            IncorrectSequence,
            DeviceDisabled,
            IncorrectMethodKKM,
            LoadError,
            PortAccessError,
            PortNotExistsError,
            PortNameError,
            IncorrectDataFromDevice,
            IncorrectMethodMode,
            LastReportElevent,
            RgistryKeyNotAccessible,
            PasswordNeeded,
            PaperOut,
            CommunicationError,
            ShiftAlreadyOpen,
            NearPaperEnd,
            NotUsedInThisKKM,
            OtherError,

        }

        public struct ResourcesT
        {
            public int RegCount;
            public int ZCount;
            public int LastZNumber;
            public int LastChequeNumber;
            public DateTime? LastShiftOpenTime;
            public override string ToString()
            {
                string s = "";
                s += "RegCount=" + RegCount.ToString() + Environment.NewLine;
                s += "ZCount=" + ZCount.ToString() + Environment.NewLine;
                s += "LastZNumber=" + LastZNumber.ToString() + Environment.NewLine;
                s += "LastChequeNumber=" + LastChequeNumber.ToString() + Environment.NewLine;
                if (LastShiftOpenTime != null)
                    s += "LastShiftOpenTime=" + LastShiftOpenTime.ToString() + Environment.NewLine;
                return s;
            }
        }
        public AllPayParams Params;
        public string StartupPath = "";
        //задание данных для соединения с принтером
        protected string op_name = "";

        string comNum = "";
        public string ComNum
        {
            get
            {
                return comNum;
            }
        }

        public bool LockGetStatus
        {
            get;
            set;
        }

        protected ResourcesT resources;
        public ResourcesT Resources
        {
            get
            {
                return resources;
            }
        }
        public struct HardStatusT
        {
            public byte[] RawStatus;
            //public bool StatusOK;
            public bool CoverOpen;
            public bool PaperTransporByHand;
            public bool StopByPaperEnd;
            public bool Error;
            public bool PrinterMechError;
            public bool PrinterCutMechError;
            public bool PrinterFatalError;
            public bool PrinterHeadHot;
            public bool PaperNearEnd;
            public bool PaperEnd;
            public bool PaperNearEnd1;
            public bool PaperEnd1;
            public bool DrawerOpened;
            public bool TicketInOutput;
            public override string ToString()
            {
                string s = "";
                if (RawStatus != null && RawStatus.Length > 0)
                {
                    s += "RawStatus=";
                    foreach (byte b in RawStatus)
                        s += b.ToString("X2");
                }
                if (s != "")
                    s += Environment.NewLine;
                //s += "StatusOK=" + StatusOK.ToString() + Environment.NewLine;
                s += "PaperTransporByHand=" + PaperTransporByHand.ToString() + Environment.NewLine;
                s += "StopByPaperEnd=" + StopByPaperEnd.ToString() + Environment.NewLine;
                s += "Error=" + Error.ToString() + Environment.NewLine;
                s += "PrinterMechError=" + PrinterMechError.ToString() + Environment.NewLine;
                s += "PrinterCutMechError=" + PrinterCutMechError.ToString() + Environment.NewLine;
                s += "PrinterFatalError=" + PrinterFatalError.ToString() + Environment.NewLine;
                s += "PrinterHeadHot=" + PrinterHeadHot.ToString() + Environment.NewLine;
                s += "PaperNearEnd=" + PaperNearEnd.ToString() + Environment.NewLine;
                s += "PaperEnd=" + PaperEnd.ToString() + Environment.NewLine;
                s += "DrawerOpened=" + DrawerOpened.ToString() + Environment.NewLine;
                s += "TicketInOutput=" + TicketInOutput.ToString() + Environment.NewLine;
                return s;
            }

        };
        protected HardStatusT hardStatus;
        public HardStatusT HardStatus
        {
            get
            {
                return hardStatus;
            }
        }
        protected DateTime? lastShiftOpenTime;
        public DateTime? LastShiftOpenTime
        {
            get
            {
                return lastShiftOpenTime;
            }
        }

        protected string accessPassword;
        protected string defaultPassword;
        protected bool useAccessPassword;
        protected ErrorCode resultCode;
        public ErrorCode ResultCode
        {
            get
            {
                return resultCode;
            }
        }
        public string ResultDescription
        {
            set;
            get;
        }

        protected bool shiftOpened;
        public bool ShiftOpened
        {
            get
            {
                return shiftOpened;
            }
        }
        protected bool deferedZReportEnabled = true;
        public bool DeferedZReportEnabled
        {
            get
            {
                return deferedZReportEnabled;
            }
        }

        protected bool deviceEnabled;
        public bool DeviceEnabled
        {
            get
            {
                return deviceEnabled;
            }
        }
        protected string CashierINN = null;
        public string CashierInn
        {
            set
            {
                CashierINN = value;
            }
            get
            {
                return CashierINN;
            }
        }
        protected int versionKKM;
        public int VersionKKM
        {
            set
            {
                versionKKM = value;
            }
            get
            {
                return versionKKM;
            }
        }
        protected string tag1214;
        public string Tag1214
        {
            set
            {
                tag1214 = value;
            }
            get
            {
                return tag1214;
            }
        }

        protected int portNumber;
        public int PortNumber
        {
            set
            {
                portNumber = value;
                comNum = "COM" + portNumber.ToString();
            }
            get
            {
                return portNumber;
            }
        }
        protected bool outOfPaper;
        public bool OutOfPaper
        {
            get
            {
                return outOfPaper;
            }
        }
        protected bool nearPaperEnd;
        public bool NearPaperEnd
        {
            get
            {
                return nearPaperEnd;
            }
        }
        public object KKMObj;
        int currentMode = 0;
        public int CurrentMode
        {
            get
            {
                return currentMode;
            }
        }
        protected int cashAccepted;
        public int CashAccepted
        {
            set
            {
                cashAccepted = value;
            }
            get
            {
                return cashAccepted;
            }
        }
        protected int creditCardAccepted;
        public int CreditCardAccepted
        {
            set
            {
                creditCardAccepted = value;
            }
            get
            {
                return creditCardAccepted;
            }
        }
        protected int inkasstionSum;
        public int InkasstionSum
        {
            set
            {
                inkasstionSum = value;
            }
            get
            {
                return inkasstionSum;
            }
        }
        protected int sum_kassa = 0;
        public int Sum_kassa
        {
            set
            {
                sum_kassa = value;
            }
            get
            {
                return sum_kassa;
            }
        }
        protected int sumTokassa = 0;
        public int SumTokassa
        {
            set
            {
                sumTokassa = value;
            }
            get
            {
                return sumTokassa;
            }
        }

        public struct ResultT
        {
            public bool? deviceEnabled;
            public ErrorCode resultCode;
            public string resultDescription;
            public HardStatusT? hardStatus;
            public bool? boolResult;
            public ResourcesT? resources;
            public double? time24HourseEnd;
            public int? currentMode;
            public string FW;
            public override string ToString()
            {
                string s = "";
                if (deviceEnabled != null)
                    s += "deviceEnabled=" + ((bool)deviceEnabled).ToString();
                if (s != "") s += Environment.NewLine;
                s += "resultCode=" + resultCode.ToString() + Environment.NewLine;
                if (resultDescription != null)
                    s += "resultDescription=" + resultDescription + Environment.NewLine;
                if (hardStatus != null)
                    s += "hardStatus=" + ((HardStatusT)hardStatus).ToString() + Environment.NewLine;
                if (boolResult != null)
                    s += "boolResult=" + ((bool)boolResult).ToString() + Environment.NewLine;
                if (resources != null)
                    s += "resources=" + ((ResourcesT)resources).ToString() + Environment.NewLine;
                if (time24HourseEnd != null)
                    s += "time24HourseEnd=" + ((double)time24HourseEnd).ToString() + Environment.NewLine;
                if (currentMode != null)
                    s += "currentMode=" + ((int)currentMode).ToString() + Environment.NewLine;
                if (FW != null)
                    s += "FW=" + FW + Environment.NewLine;
                return s;
            }
        }
        protected ResultT res = new ResultT();
        public ResultT Res
        {
            get
            {
                return OldRes;
            }

        }

        ResourcesT _GetResources()
        {
            try
            {
                ResultT res = getResource();
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, "CommonKKM: _GetResources: " + res.ToString());
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _GetResources: " + exc.ToString());
                }
            }
            return resources;
        }
        public ResourcesT GetResources()
        {
            lock (locker)
            {
                return _GetResources();
            }
        }
        protected virtual ResultT getResource()
        {
            ResultT res = new ResultT();
            return res;
        }

        string Psw;
        string RPsw;
        int? ZNo;
        List<string> Items = null;
        ResultT OldRes = new ResultT();
        bool _FR_Connect()
        {
            try
            {
                Work = false;
                Initialised = false;
                ResultT res = fR_Connect(op_name, Psw, RPsw, comNum);
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, "CommonKKM: _FR_Connect: " + res.ToString());
                }
                resultCode = res.resultCode;
                deviceEnabled = (bool)res.deviceEnabled;
                if (deviceEnabled)
                {
                    if (StatusThread == null || !StatusThread.IsAlive)
                    {
                        StatusThread = new Thread(PoolThread);
                        StatusThread.Name = "KKMStatusThread";
                        StatusThread.IsBackground = true;
                        run = true;
                        StatusThread.Start();
                        if (log != null)
                            log.ToLog(2, "CommonKKM: StatusThread.Start");
                    }
                }
                return (bool)res.boolResult;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _FR_Connect: " + exc.ToString());
                }
                return false;
            }
        }
        //метод подключения к фискальному регистратору
        public bool FR_Connect(string _op_name = "", string _Psw = "", string _RPsw = "")
        {
            lock (locker)
            {
                op_name = _op_name;
                Psw = _Psw;
                RPsw = _RPsw;
                return _FR_Connect();
            }
        }
        protected virtual ResultT fR_Connect(string op_name, string Psw, string RPsw, string ComNum)
        {
            ResultT res = new ResultT();
            return res;
        }
        bool _FR_Disconnect()
        {
            try
            {
                ResultT res = fR_Disconnect();
                resultCode = res.resultCode;
                deviceEnabled = false;
                Work = false;
                Initialised = false;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, "CommonKKM: _FR_Disconnect: " + res.ToString());
                }
                return true;
            }
            catch (Exception exc)
            {
                if (log != null)
                {

                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _FR_Disconnect: " + exc.ToString());
                    deviceEnabled = false;
                }
                return false;
            }
        }
        public bool FR_Disconnect()
        {
            run = false;
            Thread.Sleep(200);
            bool res;
            lock (locker)
            {
                res = _FR_Disconnect();
            }
            return res;
        }
        protected virtual ResultT fR_Disconnect()
        {
            ResultT res = new ResultT();
            return res;
        }
        //проверка включен ли принтер
        void _GetStatus()
        {
            try
            {
                ResultT res = getStatus();
                resultCode = res.resultCode;
                hardStatus = (HardStatusT)res.hardStatus;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: _GetStatus: " + res.ToString());
                }
                return;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _GetStatus: " + exc.ToString());
                }
                return;
            }
        }
        public void GetStatus()
        {
            lock (locker) //??? закоменчен лок
            {
                _GetStatus();
            }
            return;
        }

        public string GetStatusString()
        {
            lock (locker)
            {
                return HardStatus.RawStatus[4].ToString("X2");
            }

        }

        protected virtual ResultT getStatus()
        {
            ResultT res = new ResultT();
            return res;
        }
        //Открываем смену
        bool _Shift_Open()
        {
            try
            {
                ResultT res = shift_Open();
                resultCode = res.resultCode;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, "CommonKKM: _Shift_Open: " + res.ToString());
                }
                return (bool)res.boolResult;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _Shift_Open: " + exc.ToString());
                }
                return false;
            }
        }
        public bool Shift_Open()
        {
            bool res;
            lock (locker)
            {
                res = _Shift_Open();
            }
            return res;
        }
        protected virtual ResultT shift_Open()
        {
            ResultT res = new ResultT();
            return res;
        }
        //Закрываем смену
        bool _Shift_Close()
        {
            try
            {
                ResultT res = shift_Close();
                resultCode = res.resultCode;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, "CommonKKM: _Shift_Close: " + res.ToString());
                }
                return (bool)res.boolResult;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _Shift_Close: " + exc.ToString());
                }
                return false;
            }
        }
        public bool Shift_Close()
        {
            bool res = false;
            lock (locker)
            {
                res = _Shift_Close();
            }
            return res;
        }
        protected virtual ResultT shift_Close()
        {
            ResultT res = new ResultT();
            return res;
        }
        //Закрываем смену без чека
        bool _Shift_Close_Ex()
        {
            try
            {
                ResultT res = shift_Close_Ex();
                resultCode = res.resultCode;
                //if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, "CommonKKM: _Shift_Close_Ex: " + res.ToString());
                }
                return (bool)res.boolResult;
            }
            catch (Exception exc)
            {
                if (log != null)
                {

                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _Shift_Close_Ex: " + exc.ToString());
                }
                return false;
            }
        }
        public bool Shift_Close_Ex()
        {
            bool res;
            lock (locker)
            {
                res = _Shift_Close_Ex();
            }
            return res;
        }
        protected virtual ResultT shift_Close_Ex()
        {
            ResultT res = new ResultT();
            return res;
        }

        public double Time24HourseEnd(DateTime OpenShiftDateTime)
        {
            try
            {
                ResultT res = time24HourseEnd(OpenShiftDateTime);
                resultCode = res.resultCode;

                return (double)res.time24HourseEnd;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM Time24HourseEnd: " + exc.ToString());
                }
                return 0;
            }
        }
        protected virtual ResultT time24HourseEnd(DateTime OpenShiftDateTime)
        {
            ResultT res = new ResultT();
            return res;
        }

        public string FR_ERROR_TO_STRING(ErrorCode ErrNumBP)
        {
            try
            {
                string MSBP = "";
                switch (ErrNumBP)
                {
                    case ErrorCode.Sucsess:
                        MSBP = Properties.Resources.KKMSucsess;
                        break;
                    case ErrorCode.IncorrectMessageFormat:
                        MSBP = Properties.Resources.KKMIncorrectMessageFormat;
                        break;
                    case ErrorCode.IncorrectFieldFormat:
                        MSBP = Properties.Resources.KKMIncorrectFieldFormat;
                        break;
                    case ErrorCode.IncorrectDateFormat:
                        MSBP = Properties.Resources.KKMIncorrectDateFormat;
                        break;
                    case ErrorCode.IncorrectBCC:
                        MSBP = Properties.Resources.KKMIncorrectBCC;
                        break;
                    case ErrorCode.IncorrectPass:
                        MSBP = Properties.Resources.KKMIncorrectPass;
                        break;
                    case ErrorCode.MaxSessionTime:
                        MSBP = Properties.Resources.KKMMaxSessionTime;
                        break;
                    case ErrorCode.MaxFieldLength:
                        MSBP = Properties.Resources.KKMMaxFieldLength;
                        break;
                    case ErrorCode.MaxMessageLength:
                        MSBP = Properties.Resources.KKMMaxMessageLength;
                        break;
                    case ErrorCode.ZReportNeeded:
                        MSBP = Properties.Resources.KKMZReportNeeded;
                        break;
                    case ErrorCode.OpenSessionNeeded:
                        MSBP = Properties.Resources.KKMOpenSessionNeeded;
                        break;
                    case ErrorCode.SessionAlreadyOpen:
                        MSBP = Properties.Resources.KKMSessionAlreadyOpen;
                        break;
                    case ErrorCode.PaperOut:
                        MSBP = Properties.Resources.KKMPaperOut;
                        break;
                    default:
                        MSBP = ErrNumBP.ToString();
                        break;
                }
                return MSBP;
            }
            catch (Exception exc)
            {
                if (log != null)
                {

                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM FR_ERROR_TO_STRING: " + exc.ToString());
                }
                return "";
            }
        }
        ErrorCode _ZReportByNum()
        {
            try
            {
                if (ZNo != null)
                {
                    ResultT res = zReportByNum((int)ZNo);
                    resultCode = res.resultCode;
                    if (!OldRes.Equals(res))
                    {
                        OldRes = res;
                        if (log != null)
                            log.ToLog(2, "CommonKKM: _ZReportByNum: " + res.ToString());
                    }
                    return res.resultCode;
                }
                else
                    return ErrorCode.OtherError;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _ZReportByNum: " + exc.ToString());
                }
                return ErrorCode.OtherError;
            }
        }
        public ErrorCode ZReportByNum(int _ZNo)
        {
            ErrorCode res;
            lock (locker)
            {
                ZNo = _ZNo;
                res = _ZReportByNum();
            }
            return res;
        }
        protected virtual ResultT zReportByNum(int ZNo)
        {
            ResultT res = new ResultT();
            return res;
        }
        bool _ShiftFromKKM()
        {
            try
            {
                ResultT res = shiftFromKKM();
                resultCode = res.resultCode;
                resources = GetResources();
                hardStatus = (HardStatusT)res.hardStatus;
                if (Resources.LastShiftOpenTime != null)
                    lastShiftOpenTime = Resources.LastShiftOpenTime;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, "CommonKKM: _ShiftFromKKM: " + res.ToString());
                }
                return (bool)res.boolResult;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _ShiftFromKKM: " + exc.ToString());
                }
                return false;
            }
        }
        public bool ShiftFromKKM()
        {
            bool res;
            lock (locker)
            {
                res = _ShiftFromKKM();
            }
            return res;
        }
        protected virtual ResultT shiftFromKKM()
        {
            ResultT res = new ResultT();
            return res;
        }
        int _GetCurrentMode()
        {
            try
            {
                ResultT res = getCurrentMode();
                resultCode = res.resultCode;
                if (res.currentMode != null)
                    currentMode = (int)res.currentMode;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, "CommonKKM: _GetCurrentMode: " + res.ToString());
                }
                return currentMode;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _GetCurrentMode: " + exc.ToString());
                }
                return 0;
            }
        }

        public int GetCurrentMode()
        {
            int res;
            lock (locker)
            {
                res = _GetCurrentMode();
            }
            return res;
        }
        protected virtual ResultT getCurrentMode()
        {
            ResultT res = new ResultT();
            return res;
        }
        void _XReport()
        {
            try
            {
                ResultT res = xReport();
                resultCode = res.resultCode;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, "CommonKKM: _XReport: " + res.ToString());
                }
                return;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _XReport: " + exc.ToString());
                }
                return;
            }
        }
        public void XReport()
        {
            lock (locker)
            {
                _XReport();
            }
            return;
        }
        protected virtual ResultT xReport()
        {
            ResultT res = new ResultT();
            return res;
        }
        public virtual ErrorCode setResultCode(int RC)
        {
            return ErrorCode.Sucsess;
        }
        bool _PrintCheck()
        {
            try
            {
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: _PrintCheck: 1");

                ResultT res = printCheck(Params);
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: PrintCheck: 3, res=" + res.ToString());
                resultCode = res.resultCode;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: _PrintCheck: " + res.ToString());
                }
                return (bool)res.boolResult;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _PrintCheck Exception " + exc.ToString());
                }
                return false;
            }
        }
        public bool PrintCheck(AllPayParams Params)
        {
            bool res;
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: PrintCheck: 1");

            lock (locker)
            {
                this.Params = Params;
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: PrintCheck: 2");

                res = _PrintCheck();
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: PrintCheck: 3, res=" + res.ToString());
            }
            return res;
        }
        protected virtual ResultT printCheck(AllPayParams Params)
        {
            ResultT res = new ResultT();
            return res;
        }

        bool _PrintNotCheck(List<string> Items)
        {
            try
            {
                ResultT res = printNotCheck(Items);
                resultCode = res.resultCode;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + "CommonKKM: printNotCheck: " + res.ToString());
                }
                return (bool)res.boolResult;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _PrintNotCheck Exception: " + exc.ToString());
                }
                return false;
            }
        }
        public string QRDateString
        {
            get;
            set;
        }
        public string QRTypeString
        {
            get;
            set;
        }

        protected int ticketBarCodeWidth = 1;
        public int TicketBarCodeWidth
        {
            set
            {
                ticketBarCodeWidth = value;
            }
        }

        public bool PrintNotCheck(List<string> _Items, string _QRDateString = null, string _QRTypeString = null)
        {
            Items = _Items;
            bool res;
            lock (locker)
            {
                QRDateString = _QRDateString;
                QRTypeString = _QRTypeString;
                res = _PrintNotCheck(Items);
            }
            if (log != null)
                log.ToLog(2, "CommonKKM: _PrintNotCheck: " + res.ToString());
            return res;
        }
        protected virtual ResultT printNotCheck(List<string> Items)
        {
            ResultT res = new ResultT();
            return res;
        }

        bool _PrintBankSlip(List<string> Items)
        {
            try
            {
                ResultT res = printBankSlip(Items);
                resultCode = res.resultCode;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + "CommonKKM: _PrintBankSlip: " + res.ToString());
                }
                return (bool)res.boolResult;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _PrintBankSlip: " + exc.ToString());
                }
                return false;
            }
        }
        public bool PrintBankSlip(List<string> _Items)
        {
            Items = _Items;
            bool res;
            lock (locker)
            {
                res = _PrintBankSlip(Items);
            }
            return res;
        }
        protected virtual ResultT printBankSlip(List<string> Items)
        {
            ResultT res = new ResultT();
            return res;
        }

        /*public ErrorCode SetInterfaceParam(string BaudRate, byte Is5Wires, byte IsDateTime)
        {
            try
            {
                MethodInfo mi = KKMType.GetMethod("setInterfaceParam", BindingFlags.Instance | BindingFlags.NonPublic);
                object[] o = { BaudRate, Is5Wires, IsDateTime };
                ResultT res = (ResultT)mi.Invoke(KKMObj, o);
                resultCode = res.resultCode;
                return (ErrorCode)res.resultCode;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                     log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM SetInterfaceParam: " + exc.ToString());
                }
                return ErrorCode.OtherError;
            }
        }*/
        protected virtual ResultT setInterfaceParam(string BaudRate, byte Is5Wires, byte IsDateTime)
        {
            ResultT res = new ResultT();
            return res;
        }

        protected virtual bool _EReport()
        {
            return true;
        }

        public bool EReport()
        {
            return _EReport();
        }

        
        protected virtual bool _FNGetStatus()
        {
            return true;
        }

        public bool FNGetStatus()
        {
            return _FNGetStatus();
        }


        ErrorCode _Inkasstion()
        {
            try
            {
                ResultT res = inkasstion();
                resultCode = res.resultCode;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, "CommonKKM: _Inkasstion: " + res.ToString());
                }
                return (ErrorCode)res.resultCode;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _Inkasstion: " + exc.ToString());
                }
                return ErrorCode.OtherError;
            }
        }
        public ErrorCode Inkasstion()
        {
            ErrorCode res = ErrorCode.Sucsess;
            if (KKMObj.GetType().ToString() != "AtolKKM")
            {
                lock (locker)
                {
                    res = _Inkasstion();
                }
            }
            return res;
        }
        protected virtual ResultT inkasstion()
        {
            ResultT res = new ResultT();
            return res;
        }

        ErrorCode _ToCash()
        {
            try
            {
                ResultT res = toCash();
                resultCode = res.resultCode;
                if (!OldRes.Equals(res))
                {
                    OldRes = res;
                    if (log != null)
                        log.ToLog(2, "CommonKKM: _ToCash: " + res.ToString());
                }
                return (ErrorCode)res.resultCode;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM _Inkasstion: " + exc.ToString());
                }
                return ErrorCode.OtherError;
            }
        }
        public ErrorCode ToCashSumm()
        {
            ErrorCode res = ErrorCode.Sucsess;
            if (KKMObj.GetType().ToString() != "AtolKKM")
            {
                lock (locker)
                {
                    res = _ToCash();
                }
            }
            return res;
        }
        protected virtual ResultT toCash()
        {
            ResultT res = new ResultT();
            return res;
        }

        public ErrorCode SetPrezenter(byte IsRetrak, byte IsPrezenter, byte IsSet)
        {
            lock (locker)
            {
                try
                {
                    ResultT res = setPrezenter(IsRetrak, IsPrezenter, IsSet);
                    resultCode = res.resultCode;
                    //if (!OldRes.Equals(res))
                    {
                        OldRes = res;
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: SetPrezenter"
                                + IsRetrak.ToString() + "-" + IsPrezenter.ToString() + "-" + IsSet.ToString() + ": " + res.ToString());
                    }
                    return (ErrorCode)res.resultCode;
                }
                catch (Exception exc)
                {
                    if (log != null)
                    {
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM SetPrezenter Exception: " + exc.ToString());
                    }
                    return ErrorCode.OtherError;
                }
            }
        }
        protected virtual ResultT setPrezenter(byte IsRetrak, byte IsPrezenter, byte IsSet)
        {
            ResultT res = new ResultT();
            return res;
        }
        protected virtual string getFWVer()
        {
            return "";
        }
        protected virtual ResultT Init()
        {
            ResultT res = new ResultT();
            return res;
        }
        void PoolThread()
        {
            int count = 0;
            LockGetStatus = false;
            ResultT res = new ResultT();
            while (run)
            {
                StatusThreadrun = true;
                if (DeviceEnabled)
                {
                    if (!Initialised)
                    {
                        if (count < 20)
                            count++;
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: PoolThread: !Initialised");
                        Thread.Sleep(50);
                        try
                        {
                            lock (locker)
                            {
                                Thread.Sleep(50);
                                res = Init();
                            }
                            if (!OldRes.Equals(res))
                            {
                                OldRes = res;
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: PoolThread: Init: " + res.ToString());
                            }

                            resultCode = res.resultCode;
                            if ((bool)res.boolResult)
                                Initialised = true;
                            else
                            {
                                Initialised = false;
                            }
                        }
                        catch
                        {
                            Initialised = false;
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: PoolThread: Init: " + res.ToString());

                        }
                    }
                    else
                    {
                        //if (count++ >= 5)
                        if (!LockGetStatus)
                        {
                            Thread.Sleep(50);
                            //lock (locker)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: PoolThread: _GetStatus 1");
                                //Thread.Sleep(50);
                                _GetStatus();
                                Thread.Sleep(50);
                            }
                            //if (!OldRes.Equals(res))
                            //{
                            //    OldRes = res;
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonKKM: PoolThread: _GetStatus 2: "
                                    + res.ToString());
                            //}
                            count = 0;
                        }
                        else
                        {
                            count = -1;
                            Thread.Sleep(20);
                        }
                    }
                }
                StatusThreadrun = false;
                Thread.Sleep(1000 * (count + 1));
            }
        }
        protected byte[] Encode(string s)
        {
            byte[] Bytes = Encoding.Convert(enc, enc_dos, enc.GetBytes(s));
            return Bytes;
        }

        public void AzimuthLogControl()//контроль лога | может пускать как сервис каждые 00:12:22???!!!
        {
            try
            {
                //контроль лога | может пускать как сервис каждые 00:12:22???!!!
                DateTime AzimuthLogTime;
                RegistryKey currRegistryKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RPS");
                //дата последнего переименования лога
                if (currRegistryKey.GetValue("AzimuthLogTime") != null)
                {
                    try
                    {
                        AzimuthLogTime = Convert.ToDateTime(currRegistryKey.GetValue("AzimuthLogTime"));
                        // проверим, если AzimuthLogTime не сегодня, переименуем azimuth.log на вчера и перепишем AzimuthLogTime
                        if (AzimuthLogTime.Date < DateTime.Now.Date)
                        {
                            //rename
                            try
                            {
                                string newFileName = "azimuth." + (DateTime.Now - TimeSpan.FromDays(1)).Date.ToString("yyMMdd") + ".log";
                                try
                                {
                                    File.Delete(newFileName); // Delete the existing file if exists
                                }
                                catch { }
                                File.Move("azimuth.log", newFileName); // Rename the oldFileName into newFileName

                                AzimuthLogTime = DateTime.Now;    //  время начала сеанса работы  BankModule
                                currRegistryKey.SetValue("AzimuthLogTime", AzimuthLogTime.ToString());
                            }
                            catch { }
                        }

                    }
                    catch
                    {
                        AzimuthLogTime = DateTime.Now;    //  время начала сеанса работы  BankModule
                        currRegistryKey.SetValue("AzimuthLogTime", AzimuthLogTime.ToString());
                    }
                }
                else
                {
                    AzimuthLogTime = DateTime.Now;    //  время начала сеанса работы  BankModule
                    currRegistryKey.SetValue("AzimuthLogTime", AzimuthLogTime.ToString());
                }
                //currRegistryKey.Close();
                currRegistryKey.Flush();
            }
            catch { }
        }

        protected void ToLog(string Prefix, byte[] ba, int Len)
        {
            if (true)
                try
                {
                    //if (LastLog == DateTime.MinValue || LastLog.AddDays(1) < DateTime.Now)
                    {
                        //LastLog = DateTime.Now;
                        string[] files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "azimuth.*.log");
                        if (files.Length > 48)
                        {
                            List<string> l_files = new List<string>();
                            foreach (string fn in files)
                            {
                                l_files.Add(fn);
                            }
                            l_files.Sort();
                            for (int i = 0; i < l_files.Count - 10; i++)
                            {
                                File.Delete(l_files[i]);
                            }
                        }
                    }
                }
                catch { }

            try
            {
                //string name = this.GetType().Name;
                //using (FileStream file = File.Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, name + "." + DateTime.Now.ToString("yyyyMMdd") + ".log"), FileMode.Append))
                using (FileStream file = File.Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "azimuth.log"), FileMode.Append))
                {
                    string s = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss,fff") + " " + Len.ToString() + " " + Prefix;
                    file.Write(Encoding.Default.GetBytes(s), 0, s.Length);
                    file.Write(ba, 0, Len);
                    file.Write(new byte[] { 0x0D, 0x0A }, 0, 2);
                }
            }
            catch { }
        }
    }
}