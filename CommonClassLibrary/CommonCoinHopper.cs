﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClassLibrary
{
    public abstract class CommonCoinHopper
    {
        protected internal SerialPort _serialPort;

        object locker = new object();
        protected Log log;
        public bool IsOpen
        {
            get
            {
                return _serialPort.IsOpen;
            }
        }
        public bool Open(string Port)
        {
            try
            {
                if (!_serialPort.IsOpen)
                {
                    try
                    {
                        _serialPort.PortName = Port;
                        _serialPort.Open();
                        return true;
                    }
                    catch (Exception exc)
                    {
                        if (log != null)
                            log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCoinHopper Open: " + exc.ToString());
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCoinHopper Open: " + exc.ToString());
                }
                return false;
            }
        }
        public bool Close()
        {
            try
            {
                if (_serialPort.IsOpen)
                {
                    try
                    {
                        _serialPort.Close();
                        return true;
                    }
                    catch (Exception exc)
                    {
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCoinHopper Close: " + exc.ToString());
                        return false;
                    }
                }
                else
                    return true;
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCardDispenser Close: " + exc.ToString());
                }
                return false;
            }
        }
    }
}
