﻿using System;
using System.IO.Ports;
using System.Threading;

namespace CommonClassLibrary
{
    public abstract class CommonCardDispenser
    {
        public struct DispensStatus                // статус , получаемый от диспенсера в ответ на GetStatus
        {
            public bool CardTimeOut;
            public bool CardError;
            public bool CardAccepting;
            public bool CardDispensing;
            public bool CardShortage;
            public bool CardPreSendPos;
            public bool CardDespensePos;
            public bool CardEmpty;
            public bool CommunicationsError;
            public string CommunicationsErrorStatus;
            public override string ToString()
            {
                string res = "";
                res += "CardTimeOut=" + CardTimeOut.ToString() + Environment.NewLine;
                res += "CardError=" + CardError.ToString() + Environment.NewLine;
                res += "CardAccepting=" + CardAccepting.ToString() + Environment.NewLine;
                res += "CardDispensing=" + CardDispensing.ToString() + Environment.NewLine;
                res += "CardShortage=" + CardShortage.ToString() + Environment.NewLine;
                res += "CardPreSendPos=" + CardPreSendPos.ToString() + Environment.NewLine;
                res += "CardDespensePos=" + CardDespensePos.ToString() + Environment.NewLine;
                res += "CardEmpty=" + CardEmpty.ToString() + Environment.NewLine;
                res += "CommunicationsError=" + CommunicationsError.ToString() + Environment.NewLine;
                res += "CommunicationsErrorStatus=" + CommunicationsErrorStatus + Environment.NewLine;

                return res;
            }
            public static bool operator ==(DispensStatus a, DispensStatus b)
            {
                if (a.Equals(b))
                    return true;
                else
                    return false;
            }
            public static bool operator !=(DispensStatus a, DispensStatus b)
            {
                if (a.Equals(b))
                    return false;
                else
                    return true;
            }
        }
        protected internal CardDispenserCommandT _CardDispenserCommand;
        public CommandStatusT CommandStatus
        {
            get; set;
        }
        protected internal DispensStatus _CardDispenserStatus;
        protected internal SerialPort _serialPort;

        public object locker = new object();
        protected Log log;

        public DispensStatus CardDispenserStatus
        {
            get
            {
                return _CardDispenserStatus;
            }
        }

        public bool IsOpen
        {
            get
            {
                return _serialPort.IsOpen;
            }
        }

        protected internal byte[] DespenserRes;

        Thread StatusThread;

        private bool run = false;

        protected internal int rxSize = 0;
        protected internal int Length = 0;
        protected internal byte[] rx = new byte[256];              // прием из Dispens 

        protected internal string status;                          // статус Dispens текст
        protected internal string version;

        public bool Result
        {
            get; set;
        }
        public int TimeOut
        {
            get; set;
        }

        public string Status
        {
            get
            {
                return status;
            }
        }             // статус Dispens Текст
        public string Version
        {
            get
            {
                return version;
            }
        }
        public bool SendFromBoxToReadPosition(int _TimeOut = 5)
        {
            TimeOut = _TimeOut;
            _CardDispenserCommand = CardDispenserCommandT.SendFromBoxToReadPosition;
            CommandStatus = CommandStatusT.Run;
            return true;
            //return _SendFromBoxToReadPosition(TimeOut);
        }
        public bool SendCardToBezelAndHold(int _TimeOut = 5)
        {
            TimeOut = _TimeOut;
            _CardDispenserCommand = CardDispenserCommandT.SendCardToBezelAndHold;
            CommandStatus = CommandStatusT.Run;
            return true;
            //return _SendCardToBezelAndHold(TimeOut);
        }
        public bool SendCardToBezelAndNotHold(int _TimeOut = 5)
        {
            TimeOut = _TimeOut;
            _CardDispenserCommand = CardDispenserCommandT.SendCardToBezelAndNotHold;
            CommandStatus = CommandStatusT.Run;
            return true;
            //return _SendCardToBezelAndNotHold(TimeOut);
        }
        public bool SendCardToRecycleBox(int _TimeOut = 5)
        {
            TimeOut = _TimeOut;
            _CardDispenserCommand = CardDispenserCommandT.SendCardToRecycleBox;
            CommandStatus = CommandStatusT.Run;
            return true;
            //return _SendCardToRecycleBox(TimeOut);
        }
        public bool SetInsert(int _TimeOut = 5)
        {
            TimeOut = _TimeOut;
            _CardDispenserCommand = CardDispenserCommandT.SetInsert;
            CommandStatus = CommandStatusT.Run;
            return true;
            //return _SendFromBoxToReadPosition(TimeOut);
        }
        public bool RecycleToReadPosition(int _TimeOut = 5)
        {
            TimeOut = _TimeOut;
            _CardDispenserCommand = CardDispenserCommandT.RecycleToReadPosition;
            CommandStatus = CommandStatusT.Run;
            return true;
            //return _RecycleToReadPosition(TimeOut);
        }
        public bool Open(string Port)
        {
            try
            {
                lock (locker)
                {
                    if (!_serialPort.IsOpen)
                    {
                        try
                        {
                            _serialPort.PortName = Port;
                            _serialPort.Open();
                            //_CommandStatus = CommandStatusT.Undeifned;
                            _CardDispenserStatus.CommunicationsError = false;
                            _CardDispenserStatus.CommunicationsErrorStatus = "";
                            _GetVersion();
                            _GetStatus();
                            StatusThread = new Thread(PoolThread);
                            StatusThread.Name = "CardDispenserThread";
                            StatusThread.IsBackground = true;
                            StatusThread.Start();
                            run = true;
                            return true;
                        }
                        catch (Exception exc)
                        {
                            if (log != null)

                                log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCardDispenser Open: " + exc.ToString());
                            _CardDispenserStatus.CommunicationsError = true;
                            _CardDispenserStatus.CommunicationsErrorStatus = Properties.Resources.CardDispenserCommunicationsError;
                            status = Properties.Resources.NotResponse;
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCardDispenser Open: " + exc.ToString());
                }
                _CardDispenserStatus.CommunicationsError = true;
                _CardDispenserStatus.CommunicationsErrorStatus = Properties.Resources.CardDispenserCommunicationsError;
                status = Properties.Resources.NotResponse;
                return false;
            }
        }
        public bool Close()
        {
            try
            {
                lock (locker)
                {
                    if (_serialPort.IsOpen)
                    {
                        try
                        {
                            run = false;
                            Thread.Sleep(300);
                            _serialPort.Close();
                            _CardDispenserStatus.CommunicationsError = false;
                            _CardDispenserStatus.CommunicationsErrorStatus = "";
                            return true;
                        }
                        catch (Exception exc)
                        {
                            log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCardDispenser Close: " + exc.ToString());
                            _CardDispenserStatus.CommunicationsError = true;
                            _CardDispenserStatus.CommunicationsErrorStatus = Properties.Resources.CardDispenserCommunicationsError;
                            return false;
                        }
                    }
                    else
                        return true;
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " CommonCardDispenser Close: " + exc.ToString());
                }
                _CardDispenserStatus.CommunicationsError = true;
                _CardDispenserStatus.CommunicationsErrorStatus = Properties.Resources.CardDispenserCommunicationsError;
                status = Properties.Resources.NotResponse;
                return false;
            }
        }
        protected virtual bool _GetVersion()
        {
            return true;
        }
        protected virtual bool _SendFromBoxToReadPosition(int TimeOut = 5)
        {
            return true;
        }
        protected virtual bool _SendCardToBezelAndHold(int TimeOut = 5)
        {
            return true;
        }
        protected virtual bool _SendCardToBezelAndNotHold(int TimeOut = 5)
        {
            return true;
        }
        protected virtual bool _GetStatus()
        {
            return true;
        }
        protected virtual bool _SendCardToRecycleBox(int TimeOut = 5)
        {
            return true;
        }
        protected virtual bool _RecycleToReadPosition(int TimeOut = 5)
        {
            return true;
        }
        protected virtual bool _SetInsert(int TimeOut = 5)
        {
            return true;
        }

        void PoolThread()
        {
            while (run)
            {
                if (CommandStatus == CommandStatusT.Run)
                {
                    switch (_CardDispenserCommand)
                    {
                        case CardDispenserCommandT.RecycleToReadPosition:
                            CommandStatus = CommandStatusT.Running;
                            Result = _RecycleToReadPosition();
                            _GetStatus();
                            CommandStatus = CommandStatusT.Completed;
                            break;
                        case CardDispenserCommandT.SendCardToBezelAndHold:
                            CommandStatus = CommandStatusT.Running;
                            Result = _SendCardToBezelAndHold();
                            _GetStatus();
                            CommandStatus = CommandStatusT.Completed;
                            break;
                        case CardDispenserCommandT.SendCardToBezelAndNotHold:
                            CommandStatus = CommandStatusT.Running;
                            Result = _SendCardToBezelAndNotHold();
                            _GetStatus();
                            CommandStatus = CommandStatusT.Completed;
                            break;
                        case CardDispenserCommandT.SendCardToRecycleBox:
                            CommandStatus = CommandStatusT.Running;
                            Result = _SendCardToRecycleBox();
                            _GetStatus();
                            CommandStatus = CommandStatusT.Completed;
                            break;
                        case CardDispenserCommandT.SendFromBoxToReadPosition:
                            CommandStatus = CommandStatusT.Running;
                            Result = _SendFromBoxToReadPosition();
                            _GetStatus();
                            CommandStatus = CommandStatusT.Completed;
                            break;
                        case CardDispenserCommandT.SetInsert:
                            CommandStatus = CommandStatusT.Running;
                            Result = _SetInsert();
                            _GetStatus();
                            CommandStatus = CommandStatusT.Completed;
                            break;
                    }
                }
                else if ((CommandStatus == CommandStatusT.Completed)
                         || (CommandStatus == CommandStatusT.Undeifned))
                {
                    _GetStatus();
                }
                Thread.Sleep(200);
            }
        }
    }
}