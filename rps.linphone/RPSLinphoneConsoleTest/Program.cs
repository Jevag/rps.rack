﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using RPSLinphone;
using System.Threading;
using sipdotnet;
using Microsoft.Win32;
using System.Text.RegularExpressions;

namespace RPSLinphoneConsoleTest
{
    class Program
    {
        static bool keepRunning = true;
        static SqlConnection conn = null;

        public static string GetStringConnectFromRegistry()
        {
            string constr = string.Empty;
            
            try
            {
                RegistryKey currRegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\RPS");

                if (currRegistryKey != null && currRegistryKey.GetValue("conlocalReg") != null)
                {

                    string conlocalReg = currRegistryKey.GetValue("conlocalReg").ToString();
                    if (!string.IsNullOrEmpty(conlocalReg) && !string.IsNullOrWhiteSpace(conlocalReg))
                    {
                        Rps.Crypto.CryptoDecrypto cryp = new Rps.Crypto.CryptoDecrypto();


                        if (Regex.Match(conlocalReg, "Data Source", RegexOptions.IgnoreCase).Success)
                        {
                            var builder = new SqlConnectionStringBuilder(conlocalReg);

                            builder.Password = cryp.Decrypto(builder.Password);

                            constr = builder.ToString();
                        }
                        else
                        {
                            constr = cryp.Decrypto(conlocalReg);
                        }
                    }
                }
                else
                {
                    //LogData.Log.Instance.SaveToFile(LogData.TypeModule.ConnectDevice, $"Ошибка нет доступа к реестру CURRENT_USER\\SOFTWARE\\RPS");
                }
            }
            catch (Exception ex)
            {
                //LogData.Log.Instance.SaveToFile(LogData.TypeModule.ConnectDevice, $"Ошибка {ex.Message} {ex.StackTrace}");
            }

            return constr;
        }

        static void Main(string[] args)
        {
            string conn_string = @"Data Source=CHUVAVER-PC\SQLEXPRESS;Initial Catalog=rpsMDBG_Test_c;Integrated Security=True";


            if (args.Length > 0)
                conn_string = args[0];
            else
            {
                string loccs = GetStringConnectFromRegistry();
                if (loccs.Length > 10)
                    conn_string = loccs;
            }
            Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e) {
                //e.Cancel = true;
                Program.keepRunning = false;
                Console.WriteLine("Exit...");
                if (conn != null)
                    conn.Close();
            };

            Console.WriteLine($"Use conn string: {conn_string}");
            
            try
            {

                conn = new SqlConnection(conn_string);
                conn.Open();

                RPSLinphone.RPSLinphone rpsl = new RPSLinphone.RPSLinphone(conn);

                VoipParams vp = rpsl.voipParams;
                Console.WriteLine($"{vp.client_ip} -> {vp.server_ip}:{vp.server_port} by {vp.server_pwd}");
                Console.WriteLine(rpsl.GetLinphoneInfo());
                
                while (Program.keepRunning)
                {
                    Console.WriteLine($"isCallActive = {rpsl.isCallActive}; isConnectPhone = {rpsl.phone.CurrentConnectState}");
                    Thread.Sleep(5000);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                if (conn != null)
                    conn.Close();
            }

            

        }
    }
}
