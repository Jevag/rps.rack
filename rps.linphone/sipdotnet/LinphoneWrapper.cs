﻿using System;
using System.Runtime.InteropServices;


namespace sipdotnet
{
    public partial class Linphone
    {
        #region Libraries
        const string LIBNAME = "linphone.dll";

        const string LIBNAME_MS = "mediastreamer_base.dll";
        #endregion

        #region Import

        /// <summary>
        /// Disable a sip transport
        /// </summary>
        const int LC_SIP_TRANSPORT_DISABLED = 0;

        /// <summary>
        /// Randomly chose a sip port for this transport
        /// </summary>
        const int LC_SIP_TRANSPORT_RANDOM = -1;

        /// <summary>
        /// Don't create any server socket for this transport, ie don't bind on any port
        /// </summary>
        const int LC_SIP_TRANSPORT_DONTBIND = -2;

        const int LINPHONE_FIND_PAYLOAD_IGNORE_RATE = -1;
        // Wildcard value used by #linphone_core_find_payload_type to ignore rate in search algorithm

        const int LINPHONE_FIND_PAYLOAD_IGNORE_CHANNELS = -1;
        // Wildcard value used by #linphone_core_find_payload_type to ignore channel in search algorithm

        /// <summary>
        /// Linphone core SIP transport ports
        /// http://www.linphone.org/docs/liblinphone/struct__LinphoneSipTransports.html
        /// </summary>
        struct LCSipTransports
        {
            /// <summary>
            /// UDP port to listening on, negative value if not set
            /// </summary>
			public int udp_port;

            /// <summary>
            /// TCP port to listening on, negative value if not set
            /// </summary>
			public int tcp_port;

            /// <summary>
            /// DTLS port to listening on, negative value if not set
            /// </summary>
			public int dtls_port;

            /// <summary>
            /// TLS port to listening on, negative value if not set
            /// </summary>
			public int tls_port;
        };

        /// <summary>
        /// Describes proxy registration states
        /// http://www.linphone.org/docs/liblinphone/group__proxies.html
        /// </summary>
		public enum LinphoneRegistrationState
        {
            /// <summary>
            /// Initial state for registrations
            /// </summary>
			LinphoneRegistrationNone,

            /// <summary>
            /// Registration is in progress
            /// </summary>
			LinphoneRegistrationProgress,

            /// <summary>
            /// Registration is successful
            /// </summary>
			LinphoneRegistrationOk,

            /// <summary>
            /// Unregistration succeeded
            /// </summary>
			LinphoneRegistrationCleared,

            /// <summary>
            /// Registration failed
            /// </summary>
			LinphoneRegistrationFailed
        };

        /// <summary>
        /// Logging level
        /// https://github.com/BelledonneCommunications/ortp/blob/master/include/ortp/logging.h
        /// https://github.com/BelledonneCommunications/bctoolbox/blob/master/include/bctoolbox/logging.h
        /// </summary>
        public enum OrtpLogLevel
        {
            DEBUG = 1,
            TRACE = 1 << 1,
            MESSAGE = 1 << 2,
            WARNING = 1 << 3,
            ERROR = 1 << 4,
            FATAL = 1 << 5,
            END = 1 << 6
        };

        /// <summary>
        /// Represents the different state a call can reach into
        /// http://www.linphone.org/docs/liblinphone/group__call__control.html
        /// </summary>
		public enum LinphoneCallState
        {
            /// <summary>
            /// Initial call state
            /// </summary>
			LinphoneCallIdle,

            /// <summary>
            /// This is a new incoming call
            /// </summary>
			LinphoneCallIncomingReceived,

            /// <summary>
            /// An outgoing call is started
            /// </summary>
			LinphoneCallOutgoingInit,

            /// <summary>
            /// An outgoing call is in progress
            /// </summary>
			LinphoneCallOutgoingProgress,

            /// <summary>
            /// An outgoing call is ringing at remote end
            /// </summary>
			LinphoneCallOutgoingRinging,

            /// <summary>
            /// An outgoing call is proposed early media
            /// </summary>
			LinphoneCallOutgoingEarlyMedia,

            /// <summary>
            /// Connected, the call is answered
            /// </summary>
			LinphoneCallConnected,

            /// <summary>
            /// The media streams are established and running
            /// </summary>
			LinphoneCallStreamsRunning,

            /// <summary>
            /// The call is pausing at the initiative of local end
            /// </summary>
			LinphoneCallPausing,

            /// <summary>
            /// The call is paused, remote end has accepted the pause
            /// </summary>
			LinphoneCallPaused,

            /// <summary>
            /// The call is being resumed by local end
            /// </summary>
			LinphoneCallResuming,

            /// <summary>
            /// <The call is being transfered to another party, resulting in a new outgoing call to follow immediately
            /// </summary>
			LinphoneCallRefered,

            /// <summary>
            /// The call encountered an error
            /// </summary>
			LinphoneCallError,

            /// <summary>
            /// The call ended normally
            /// </summary>
			LinphoneCallEnd,

            /// <summary>
            /// The call is paused by remote end
            /// </summary>
			LinphoneCallPausedByRemote,

            /// <summary>
            /// The call's parameters change is requested by remote end, used for example when video is added by remote
            /// </summary>
			LinphoneCallUpdatedByRemote,

            /// <summary>
            /// We are proposing early media to an incoming call
            /// </summary>
			LinphoneCallIncomingEarlyMedia,

            /// <summary>
            /// A call update has been initiated by us
            /// </summary>
			LinphoneCallUpdating,

            /// <summary>
            /// The call object is no more retained by the core
            /// </summary>
            LinphoneCallReleased
        };

        /// <summary>
        /// Holds all callbacks that the application should implement. None is mandatory.
        /// http://www.linphone.org/docs/liblinphone/struct__LinphoneCoreVTable.html
        /// </summary>
        struct LinphoneCoreVTable
        {
            /// <summary>
            /// Notifies global state changes
            /// </summary>
			public IntPtr global_state_changed;

            /// <summary>
            /// Notifies registration state changes
            /// </summary>
			public IntPtr registration_state_changed;

            /// <summary>
            /// Notifies call state changes
            /// </summary>
			public IntPtr call_state_changed;

            /// <summary>
            /// Notify received presence events
            /// </summary>
			public IntPtr notify_presence_received;

            /// <summary>
            /// Notify received presence events
            /// </summary>
            public IntPtr notify_presence_received_for_uri_or_tel;

            /// <summary>
            /// Notify about pending presence subscription request
            /// </summary>
            public IntPtr new_subscription_requested;

            /// <summary>
            /// Ask the application some authentication information
            /// </summary>
			public IntPtr auth_info_requested;

            /// <summary>
            /// Ask the application some authentication information
            /// </summary>
            public IntPtr authentication_requested;

            /// <summary>
            /// Notifies that call log list has been updated
            /// </summary>
            public IntPtr call_log_updated;

            /// <summary>
            /// A message is received, can be text or external body
            /// </summary>
			public IntPtr message_received;

            /// <summary>
            /// An encrypted message is received but we can't decrypt it
            /// </summary>
            public IntPtr message_received_unable_decrypt;

            /// <summary>
            /// An is-composing notification has been received
            /// </summary>
            public IntPtr is_composing_received;

            /// <summary>
            /// A dtmf has been received received
            /// </summary>
			public IntPtr dtmf_received;

            /// <summary>
            /// An out of call refer was received
            /// </summary>
			public IntPtr refer_received;

            /// <summary>
            /// Notifies on change in the encryption of call streams
            /// </summary>
			public IntPtr call_encryption_changed;

            /// <summary>
            /// Notifies when a transfer is in progress
            /// </summary>
			public IntPtr transfer_state_changed;

            /// <summary>
            /// A LinphoneFriend's BuddyInfo has changed
            /// </summary>
			public IntPtr buddy_info_updated;

            /// <summary>
            /// Notifies on refreshing of call's statistics.
            /// </summary>
			public IntPtr call_stats_updated;

            /// <summary>
            /// Notifies an incoming informational message received.
            /// </summary>
			public IntPtr info_received;

            /// <summary>
            /// Notifies subscription state change
            /// </summary>
			public IntPtr subscription_state_changed;

            /// <summary>
            /// Notifies a an event notification, see linphone_core_subscribe()
            /// </summary>
			public IntPtr notify_received;

            /// <summary>
            /// Notifies publish state change (only from #LinphoneEvent api)
            /// </summary>
			public IntPtr publish_state_changed;

            /// <summary>
            /// Notifies configuring status changes
            /// </summary>
			public IntPtr configuring_status;

            /// <summary>
            /// Callback that notifies various events with human readable text (deprecated)
            /// </summary>
            [System.Obsolete]
            public IntPtr display_status;

            /// <summary>
            /// Callback to display a message to the user (deprecated)
            /// </summary>
            [System.Obsolete]
            public IntPtr display_message;

            /// <summary>
            /// Callback to display a warning to the user (deprecated)
            /// </summary>
            [System.Obsolete]
            public IntPtr display_warning;

            [System.Obsolete]
            public IntPtr display_url;

            /// <summary>
            /// Notifies the application that it should show up
            /// </summary>
            [System.Obsolete]
            public IntPtr show;

            /// <summary>
            /// Use #message_received instead <br> A text message has been received
            /// </summary>
            [System.Obsolete]
            public IntPtr text_received;

            /// <summary>
            /// Callback to store file received attached to a LinphoneChatMessage
            /// </summary>
            [System.Obsolete]
            public IntPtr file_transfer_recv;

            /// <summary>
            /// Callback to collect file chunk to be sent for a LinphoneChatMessage
            /// </summary>
            [System.Obsolete]
            public IntPtr file_transfer_send;

            /// <summary>
            /// Callback to indicate file transfer progress
            /// </summary>
            [System.Obsolete]
            public IntPtr file_transfer_progress_indication;

            /// <summary>
            /// Callback to report IP network status (I.E up/down)
            /// </summary>
            public IntPtr network_reachable;

            /// <summary>
            /// Callback to upload collected logs
            /// </summary>
            public IntPtr log_collection_upload_state_changed;

            /// <summary>
            /// Callback to indicate log collection upload progress
            /// </summary>
            public IntPtr log_collection_upload_progress_indication;

            public IntPtr friend_list_created;

            public IntPtr friend_list_removed;

            /// <summary>
            /// User data associated with the above callbacks
            /// </summary>
            public IntPtr user_data;
        };

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct PayloadType
        {
            public int type; //  one of PAYLOAD_* macros
            public int clock_rate; //  rtp clock rate
            public char bits_per_sample; //  in case of continuous audio data 
            public string zero_pattern;
            public int pattern_length;
            //  other useful information for the application
            public int normal_bitrate; // in bit/s 
            public string mime_type; // actually the submime, ex: pcm, pcma, gsm
            public int channels; //  number of channels of audio 
            public string recv_fmtp; //  various format parameters for the incoming stream 
            public string send_fmtp; //  various format parameters for the outgoing stream 
            public PayloadTypeAvpfParams avpf; //  AVPF parameters 
            public int flags;
            public IntPtr user_data;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct PayloadTypeAvpfParams
        {
            public char features; // A bitmask of PAYLOAD_TYPE_AVPF_* macros.
            public short trr_interval; // The interval in milliseconds between regular RTCP packets. 
        }

        #region Version
        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr linphone_core_get_version();

        static string linphone_core_get_version_as_string()
        {
            IntPtr ptr = linphone_core_get_version();
            // assume returned string is utf-8 encoded
            return PtrToStringUtf8(ptr);
        }

        static string PtrToStringUtf8(IntPtr ptr) // aPtr is nul-terminated
        {
            if (ptr == IntPtr.Zero)
                return "";
            int len = 0;
            while (Marshal.ReadByte(ptr, len) != 0)
                len++;
            if (len == 0)
                return "";
            byte[] array = new byte[len];
            Marshal.Copy(ptr, array, 0, len);
            return System.Text.Encoding.UTF8.GetString(array);
        }
        #endregion

        #region Initializing

        // http://www.linphone.org/docs/liblinphone/group__initializing.html

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        [System.Obsolete]
        static extern void linphone_core_enable_logs(IntPtr FILE);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        [System.Obsolete]
        static extern void linphone_core_disable_logs();

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        [System.Obsolete]
        static extern IntPtr linphone_core_new(IntPtr vtable, string config_path, string factory_config_path, IntPtr userdata);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_unref(IntPtr lc);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_iterate(IntPtr lc);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_set_log_level(OrtpLogLevel loglevel);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr linphone_core_get_audio_codecs(IntPtr lc);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int linphone_core_set_audio_codecs(IntPtr lc, IntPtr codecs);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern byte linphone_core_payload_type_enabled(IntPtr lc, IntPtr pt);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr linphone_core_find_payload_type(IntPtr lc, string type, int rate, int channels);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int linphone_core_enable_payload_type(IntPtr lc, IntPtr pt, bool enable);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_enable_echo_cancellation(IntPtr lc, bool val);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern byte linphone_core_echo_cancellation_enabled(IntPtr lc);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_enable_echo_limiter(IntPtr lc, bool val);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern byte linphone_core_echo_limiter_enabled(IntPtr lc);

        #endregion

        #region Proxies

        // http://www.linphone.org/docs/liblinphone/group__proxies.html

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr linphone_core_create_proxy_config(IntPtr lc);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int linphone_proxy_config_set_identity(IntPtr obj, string identity);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int linphone_proxy_config_set_server_addr(IntPtr obj, string server_addr);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_proxy_config_enable_register(IntPtr obj, bool val);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int linphone_core_add_proxy_config(IntPtr lc, IntPtr cfg);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_set_default_proxy_config(IntPtr lc, IntPtr config);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        [System.Obsolete]
        static extern int linphone_core_get_default_proxy(IntPtr lc, ref IntPtr config);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern bool linphone_proxy_config_is_registered(IntPtr config);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_proxy_config_edit(IntPtr config);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int linphone_proxy_config_done(IntPtr config);

        #endregion

        #region Network

        // http://www.linphone.org/docs/liblinphone/group__network__parameters.html

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int linphone_core_set_sip_transports(IntPtr lc, IntPtr tr_config);

        #endregion

        #region SIP

        // http://www.linphone.org/docs/liblinphone/group__linphone__address.html

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        [System.Obsolete]
        static extern void linphone_address_destroy(IntPtr u);

        #endregion

        #region Miscenalleous

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_set_user_agent(IntPtr lc, string ua_name, string version);

        #endregion

        #region Calls

        // http://www.linphone.org/docs/liblinphone/group__call__control.html

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr linphone_core_create_call_params(IntPtr lc, IntPtr call);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_call_params_enable_video(IntPtr lc, bool enabled);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_call_params_enable_early_media_sending(IntPtr lc, bool enabled);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr linphone_core_invite_with_params(IntPtr lc, string url, IntPtr callparams);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_call_params_unref(IntPtr callparams);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int linphone_core_terminate_call(IntPtr lc, IntPtr call);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr linphone_call_ref(IntPtr call);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_call_unref(IntPtr call);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int linphone_core_terminate_all_calls(IntPtr lc);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr linphone_call_get_remote_address_as_string(IntPtr call);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int linphone_core_accept_call_with_params(IntPtr lc, IntPtr call, IntPtr callparams);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_call_params_set_record_file(IntPtr callparams, string filename);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr linphone_call_params_get_record_file(IntPtr callparams);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_call_params_enable_audio(IntPtr callparams, bool enabled);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern int linphone_call_send_dtmfs(IntPtr call, string dtmfs);

        #endregion

        #region Authentication

        // http://www.linphone.org/docs/liblinphone/group__authentication.html

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_add_auth_info(IntPtr lc, IntPtr info);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr linphone_auth_info_new(string username, string userid, string passwd, string ha1, string realm, string domain);

        #endregion

        #region Calls miscenalleous

        // http://www.linphone.org/docs/liblinphone/group__call__misc.html

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_call_start_recording(IntPtr call);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_call_stop_recording(IntPtr call);

        #endregion

        #region Media

        // http://www.linphone.org/docs/liblinphone/group__media__parameters.html

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_set_play_file(IntPtr lc, string file);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_set_record_file(IntPtr lc, string file);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_set_ring(IntPtr lc, string file);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_set_remote_ringback_tone(IntPtr lc, string file);

        [DllImport(LIBNAME, CallingConvention = CallingConvention.Cdecl)]
        static extern void linphone_core_set_ringback(IntPtr lc, string file);

        #endregion

        #region MSList
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct MSList
        {
            public IntPtr next;
            public IntPtr prev;
            public IntPtr data;
        }

        [DllImport(LIBNAME_MS, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr ms_list_append(IntPtr list, IntPtr data);
        #endregion

        #endregion

    }
}
