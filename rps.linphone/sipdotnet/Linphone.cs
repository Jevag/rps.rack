using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Text;

namespace sipdotnet
{
	public partial class Linphone
	{
        class LinphoneCall : Call
		{
			IntPtr linphoneCallPtr;

			public IntPtr LinphoneCallPtr {
				get {
                    return linphoneCallPtr;
				}
				set {
					linphoneCallPtr = value;
				}
			}

			public void SetCallType (CallType type)
			{
				this.calltype = type;
			}

			public void SetCallState (CallState state)
			{
				this.callstate = state;
			}

			public void SetFrom (string from)
			{
				this.from = from;
			}

			public void SetTo (string to)
			{
				this.to = to;
			}
		}

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		delegate void LinphoneCoreRegistrationStateChangedCb (IntPtr lc, IntPtr cfg, LinphoneRegistrationState cstate, string message);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		delegate void LinphoneCoreCallStateChangedCb (IntPtr lc, IntPtr call, LinphoneCallState cstate, string message);

        LinphoneCoreRegistrationStateChangedCb registration_state_changed;
		LinphoneCoreCallStateChangedCb call_state_changed;
		IntPtr linphoneCore, callsDefaultParams, proxy_cfg, auth_info, t_configPtr, vtablePtr;
		Thread coreLoop;
		bool running = true;
		string identity, server_addr;
		LinphoneCoreVTable vtable;
        LCSipTransports t_config;

		List<LinphoneCall> calls = new List<LinphoneCall> ();

		LinphoneCall FindCall (IntPtr call)
		{
			return calls.Find (delegate(LinphoneCall obj) {
				return (obj.LinphoneCallPtr == call);
			});
		}

		void SetTimeout (Action callback, int miliseconds)
		{
			System.Timers.Timer timeout = new System.Timers.Timer ();
			timeout.Interval = miliseconds;
			timeout.AutoReset = false;
			timeout.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) => {
				callback ();
			};
			timeout.Start ();
		}

		public void CreatePhone (string username, string password, string server, int port, string agent, string version)
		{
            //#if (TRACE)
            //linphone_core_enable_logs (GetFileHandle());
            //linphone_core_set_log_level (OrtpLogLevel.DEBUG);
            //#else
            linphone_core_disable_logs();
            linphone_core_set_log_level (OrtpLogLevel.END);
			//#endif

            running = true;
            registration_state_changed = new LinphoneCoreRegistrationStateChangedCb(OnRegistrationChanged);
            call_state_changed = new LinphoneCoreCallStateChangedCb(OnCallStateChanged);

#pragma warning disable 0612
            vtable = new LinphoneCoreVTable()
            {
                global_state_changed = IntPtr.Zero,
                registration_state_changed = Marshal.GetFunctionPointerForDelegate(registration_state_changed),
                call_state_changed = Marshal.GetFunctionPointerForDelegate(call_state_changed),
                notify_presence_received = IntPtr.Zero,
                notify_presence_received_for_uri_or_tel = IntPtr.Zero,
                new_subscription_requested = IntPtr.Zero,
                auth_info_requested = IntPtr.Zero,
                authentication_requested = IntPtr.Zero,
                call_log_updated = IntPtr.Zero,
                message_received = IntPtr.Zero,
                message_received_unable_decrypt = IntPtr.Zero,
                is_composing_received = IntPtr.Zero,
                dtmf_received = IntPtr.Zero,
                refer_received = IntPtr.Zero,
                call_encryption_changed = IntPtr.Zero,
                transfer_state_changed = IntPtr.Zero,
                buddy_info_updated = IntPtr.Zero,
                call_stats_updated = IntPtr.Zero,
                info_received = IntPtr.Zero,
                subscription_state_changed = IntPtr.Zero,
                notify_received = IntPtr.Zero,
                publish_state_changed = IntPtr.Zero,
                configuring_status = IntPtr.Zero,
                display_status = IntPtr.Zero,
                display_message = IntPtr.Zero,
                display_warning = IntPtr.Zero,
                display_url = IntPtr.Zero,
                show = IntPtr.Zero,
                text_received = IntPtr.Zero,
                file_transfer_recv = IntPtr.Zero,
                file_transfer_send = IntPtr.Zero,
                file_transfer_progress_indication = IntPtr.Zero,
                network_reachable = IntPtr.Zero,
                log_collection_upload_state_changed = IntPtr.Zero,
                log_collection_upload_progress_indication = IntPtr.Zero,
                friend_list_created = IntPtr.Zero,
                friend_list_removed = IntPtr.Zero,
                user_data = IntPtr.Zero
            };
#pragma warning restore 0612

            vtablePtr = Marshal.AllocHGlobal(Marshal.SizeOf(vtable));
            Marshal.StructureToPtr(vtable, vtablePtr, false); 

            linphoneCore = linphone_core_new(vtablePtr, null, null, IntPtr.Zero);
            
            coreLoop = new Thread(LinphoneMainLoop);
            coreLoop.IsBackground = false;
            coreLoop.Start();

			t_config = new LCSipTransports()
			{
				udp_port = LC_SIP_TRANSPORT_RANDOM,
				tcp_port = LC_SIP_TRANSPORT_RANDOM,
				dtls_port = LC_SIP_TRANSPORT_RANDOM,
				tls_port = LC_SIP_TRANSPORT_RANDOM
			};
			t_configPtr = Marshal.AllocHGlobal(Marshal.SizeOf(t_config));
			Marshal.StructureToPtr (t_config, t_configPtr, false);
			linphone_core_set_sip_transports (linphoneCore, t_configPtr);

            linphone_core_set_user_agent (linphoneCore, agent, version);

            callsDefaultParams = linphone_core_create_call_params (linphoneCore, IntPtr.Zero);
            linphone_call_params_enable_video (callsDefaultParams, false);
            linphone_call_params_enable_audio (callsDefaultParams, true);
            linphone_call_params_enable_early_media_sending (callsDefaultParams, true);

            identity = "sip:" + username + "@" + server;
			server_addr = "sip:" + server + ":" + port.ToString();

			auth_info = linphone_auth_info_new (username, null, password, null, null, null);
			linphone_core_add_auth_info (linphoneCore, auth_info);

            proxy_cfg = linphone_core_create_proxy_config(linphoneCore);
			linphone_proxy_config_set_identity (proxy_cfg, identity);
			linphone_proxy_config_set_server_addr (proxy_cfg, server_addr);
			linphone_proxy_config_enable_register (proxy_cfg, true);
			linphone_core_add_proxy_config (linphoneCore, proxy_cfg);
            linphone_core_set_default_proxy_config (linphoneCore, proxy_cfg);

            // Set codecs
            LoadAudioCodecs();
            // Set echo params
            linphone_core_enable_echo_cancellation(linphoneCore, true);
            linphone_core_enable_echo_limiter(linphoneCore, true);

        }

		public void DestroyPhone ()
		{
            if (RegistrationStateChangedEvent != null)
                RegistrationStateChangedEvent(LinphoneRegistrationState.LinphoneRegistrationProgress); // disconnecting

			linphone_core_terminate_all_calls (linphoneCore);

			SetTimeout (delegate {
                linphone_call_params_unref (callsDefaultParams);

				if (linphone_proxy_config_is_registered (proxy_cfg)) {
					linphone_proxy_config_edit (proxy_cfg);
					linphone_proxy_config_enable_register (proxy_cfg, false);
					linphone_proxy_config_done (proxy_cfg);
				}

				SetTimeout (delegate {
					running = false;
				}, 10000);

			}, 5000);
		}

        void LinphoneMainLoop()
        {
            while (running)
            {
                linphone_core_iterate(linphoneCore); // roll
                System.Threading.Thread.Sleep(100);
            }

            linphone_core_unref(linphoneCore);

            if (vtablePtr != IntPtr.Zero)
                Marshal.FreeHGlobal(vtablePtr);
            if (t_configPtr != IntPtr.Zero)
                Marshal.FreeHGlobal(t_configPtr);
            registration_state_changed = null;
            call_state_changed = null;
            linphoneCore = callsDefaultParams = proxy_cfg = auth_info = t_configPtr = IntPtr.Zero;
            coreLoop = null;
            identity = null;
            server_addr = null;

            if (RegistrationStateChangedEvent != null)
                RegistrationStateChangedEvent(LinphoneRegistrationState.LinphoneRegistrationCleared);

		}
        
        public void SendDTMFs (Call call, string dtmfs)
        {
            if (call == null)
                throw new ArgumentNullException("call");

            if (linphoneCore == IntPtr.Zero || !running)
            {
                if (ErrorEvent != null)
                    ErrorEvent(call, "Cannot make or receive calls when Linphone Core is not working.");
                return;
            }

            LinphoneCall linphonecall = (LinphoneCall)call;
            linphone_call_send_dtmfs (linphonecall.LinphoneCallPtr, dtmfs);
        }

        public void SetRingbackSound (string file)
        {
            if (linphoneCore == IntPtr.Zero || !running)
            {
                if (ErrorEvent != null)
                    ErrorEvent(null, "Cannot modify configuration when Linphone Core is not working.");
                return;
            }

            linphone_core_set_ringback (linphoneCore, file);
        }

        public void SetIncomingRingSound (string file)
        {
            if (linphoneCore == IntPtr.Zero || !running)
            {
                if (ErrorEvent != null)
                    ErrorEvent(null, "Cannot modify configuration when Linphone Core is not working.");
                return;
            }

            linphone_core_set_ring (linphoneCore, file);
        }
        
        public void TerminateCall (Call call)
		{
			if (call == null)
				throw new ArgumentNullException ("call");

			if (linphoneCore == IntPtr.Zero || !running) {
				if (ErrorEvent != null)
					ErrorEvent (call, "Cannot make or receive calls when Linphone Core is not working.");
				return;
			}

			LinphoneCall linphonecall = (LinphoneCall) call;
			linphone_core_terminate_call (linphoneCore, linphonecall.LinphoneCallPtr);
        }
	
        public void TerminateAllCalls()
        {
			if (linphoneCore == IntPtr.Zero || !running) {
				if (ErrorEvent != null)
					ErrorEvent (null, "Cannot terminate calls when Linphone Core is not working.");
				return;
			}
            linphone_core_terminate_all_calls(linphoneCore);
        }

		public void MakeCall (string uri)
		{
			if (linphoneCore == IntPtr.Zero || !running) {
				if (ErrorEvent != null)
					ErrorEvent (null, "Cannot make or receive calls when Linphone Core is not working.");
				return;
			}

			IntPtr call = linphone_core_invite_with_params (linphoneCore, uri, callsDefaultParams);

			if (call == IntPtr.Zero) {
				if (ErrorEvent != null)
					ErrorEvent (null, "Cannot call.");
				return;
			}

            linphone_call_ref(call);
        }

		public void MakeCallAndRecord (string uri, string filename)
		{
			if (linphoneCore == IntPtr.Zero || !running) {
				if (ErrorEvent != null)
					ErrorEvent (null, "Cannot make or receive calls when Linphone Core is not working.");
				return;
			}

			linphone_call_params_set_record_file (callsDefaultParams, filename);

			IntPtr call = linphone_core_invite_with_params (linphoneCore, uri, callsDefaultParams);
			if (call == IntPtr.Zero) {
				if (ErrorEvent != null)
					ErrorEvent (null, "Cannot call.");
				return;
			}

            linphone_call_ref(call);
            linphone_call_start_recording (call);
		}

		public void ReceiveCallAndRecord (Call call, string filename)
		{
			if (call == null)
				throw new ArgumentNullException ("call");

			if (linphoneCore == IntPtr.Zero || !running) {
				if (ErrorEvent != null)
					ErrorEvent (call, "Cannot make or receive calls when Linphone Core is not working.");
				return;
			}

			LinphoneCall linphonecall = (LinphoneCall) call;
            linphone_call_ref(linphonecall.LinphoneCallPtr);
            linphone_call_params_set_record_file (callsDefaultParams, filename);
			linphone_core_accept_call_with_params (linphoneCore, linphonecall.LinphoneCallPtr, callsDefaultParams);
			linphone_call_start_recording (linphonecall.LinphoneCallPtr);
		}

		public void ReceiveCall (Call call)
		{
			if (call == null)
				throw new ArgumentNullException ("call");

			if (linphoneCore == IntPtr.Zero || !running) {
				if (ErrorEvent != null)
					ErrorEvent (call, "Cannot receive call when Linphone Core is not working.");
				return;
			}

			LinphoneCall linphonecall = (LinphoneCall) call;
            linphone_call_ref(linphonecall.LinphoneCallPtr);
            linphone_call_params_set_record_file (callsDefaultParams, null);
			linphone_core_accept_call_with_params (linphoneCore, linphonecall.LinphoneCallPtr, callsDefaultParams);
		}

		public delegate void RegistrationStateChangedDelegate (LinphoneRegistrationState state);
		public event RegistrationStateChangedDelegate RegistrationStateChangedEvent;

		public delegate void CallStateChangedDelegate (Call call);
		public event CallStateChangedDelegate CallStateChangedEvent;

		public delegate void ErrorDelegate (Call call, string message);
		public event ErrorDelegate ErrorEvent;

		void OnRegistrationChanged (IntPtr lc, IntPtr cfg, LinphoneRegistrationState cstate, string message) 
		{
			if (linphoneCore == IntPtr.Zero || !running) return;
            #if (TRACE)
            Console.WriteLine("OnRegistrationChanged: {0}", cstate);
            #endif
            if (RegistrationStateChangedEvent != null)
				RegistrationStateChangedEvent (cstate);
		}

		void OnCallStateChanged (IntPtr lc, IntPtr call, LinphoneCallState cstate, string message)
		{
			if (linphoneCore == IntPtr.Zero || !running) return;
            #if (TRACE)
            Console.WriteLine("OnCallStateChanged: {0}", cstate);
            #endif

			Call.CallState newstate = Call.CallState.None;
			Call.CallType newtype = Call.CallType.None;
			string from = "";
			string to = "";
            IntPtr addressStringPtr;

			// detecting direction, state and source-destination data by state
			switch (cstate) {
				case LinphoneCallState.LinphoneCallIncomingReceived:
				case LinphoneCallState.LinphoneCallIncomingEarlyMedia:
					newstate = Call.CallState.Loading;
					newtype = Call.CallType.Incoming;
                    addressStringPtr = linphone_call_get_remote_address_as_string(call);
                    if (addressStringPtr != IntPtr.Zero) from = Marshal.PtrToStringAnsi(addressStringPtr);
					to = identity;
					break;

				case LinphoneCallState.LinphoneCallConnected:
				case LinphoneCallState.LinphoneCallStreamsRunning:
				case LinphoneCallState.LinphoneCallPausedByRemote:
				case LinphoneCallState.LinphoneCallUpdatedByRemote:
					newstate = Call.CallState.Active;
					break;

				case LinphoneCallState.LinphoneCallOutgoingInit:
				case LinphoneCallState.LinphoneCallOutgoingProgress:
				case LinphoneCallState.LinphoneCallOutgoingRinging:
				case LinphoneCallState.LinphoneCallOutgoingEarlyMedia:
					newstate = Call.CallState.Loading;
					newtype = Call.CallType.Outcoming;
                    addressStringPtr = linphone_call_get_remote_address_as_string(call);
                    if (addressStringPtr != IntPtr.Zero) to = Marshal.PtrToStringAnsi(addressStringPtr);
					from = this.identity;
					break;

                case LinphoneCallState.LinphoneCallError:
                    newstate = Call.CallState.Error;
                    break;

				case LinphoneCallState.LinphoneCallReleased:
				case LinphoneCallState.LinphoneCallEnd:
					newstate = Call.CallState.Completed;
					if (linphone_call_params_get_record_file (callsDefaultParams) != IntPtr.Zero)
						linphone_call_stop_recording (call);
					break;

				default:
					break;
			}

            IntPtr callref = linphone_call_ref(call);

            LinphoneCall existCall = FindCall (callref);

			if (existCall == null) {
				existCall = new LinphoneCall ();
				existCall.SetCallState (newstate);
				existCall.SetCallType (newtype);
				existCall.SetFrom (from);
				existCall.SetTo (to);
				existCall.LinphoneCallPtr = callref;

				calls.Add (existCall);

				if (CallStateChangedEvent != null)
					CallStateChangedEvent (existCall);
			} else {
				if (existCall.GetState () != newstate) {
					existCall.SetCallState (newstate);
                    if (CallStateChangedEvent != null)
                        CallStateChangedEvent(existCall);
                }
			}

            if (cstate == LinphoneCallState.LinphoneCallReleased)
            {
                linphone_call_unref(existCall.LinphoneCallPtr);
                calls.Remove(existCall);
            }
        }

        public string GetLinphoneInfo()
        {
            if (linphoneCore == IntPtr.Zero)
                throw new Exception("Linphone not initialized");

            string ver = "VERSION:" + Environment.NewLine + linphone_core_get_version_as_string();

            string audioCodecsInfo = "CODECS:" + Environment.NewLine;

            IntPtr audioCodecListPtr = linphone_core_get_audio_codecs(linphoneCore);

            MSList curStruct;
            do
            {
                curStruct.next = IntPtr.Zero;
                curStruct.prev = IntPtr.Zero;
                curStruct.data = IntPtr.Zero;
                curStruct = (MSList)Marshal.PtrToStructure(audioCodecListPtr, typeof(MSList));
                if (curStruct.data != IntPtr.Zero)
                {
                    var payload = (PayloadType)Marshal.PtrToStructure(curStruct.data, typeof(PayloadType));

                    byte enabled = linphone_core_payload_type_enabled(linphoneCore, curStruct.data);

                    audioCodecsInfo += $"Name: {payload.mime_type}\t";
                    audioCodecsInfo += (enabled == 1 ? "On  " : "Off ");
                    audioCodecsInfo += $"Rate: {payload.clock_rate}\t";
                    audioCodecsInfo += $"Bitrate: {payload.normal_bitrate}\t";
                    if (payload.recv_fmtp != null && payload.recv_fmtp != string.Empty && payload.recv_fmtp.Length > 0)
                        audioCodecsInfo += $"Recv params: {payload.recv_fmtp}";
                    //audioCodecsInfo += $"\tSend: {payload.send_fmtp}\n";
                    
                    audioCodecsInfo += Environment.NewLine;
                    
                }
                audioCodecListPtr = curStruct.next;
            } while (curStruct.next != IntPtr.Zero);

            string echoInfo = "ECHO SETTINGS:" + Environment.NewLine;
            echoInfo += "Echo cancellation = " + (linphone_core_echo_cancellation_enabled(linphoneCore).ToString()) + Environment.NewLine;
            echoInfo += "Echo limiter = " + linphone_core_echo_limiter_enabled(linphoneCore).ToString() + Environment.NewLine;

            string result = "====================================" + Environment.NewLine;
            result += ver + Environment.NewLine + Environment.NewLine;
            result += audioCodecsInfo + Environment.NewLine;
            result += echoInfo + Environment.NewLine + Environment.NewLine;

            return result;
        }

        public void LoadAudioCodecs()
        {
            if (linphoneCore == IntPtr.Zero)
                throw new Exception("Linphone not initialized");


            // remove 
            //string[] primaryCodecs = { "G722", "PCMU", "PCMA", "speex", "speex" };
            //int[] rate = { LINPHONE_FIND_PAYLOAD_IGNORE_RATE, LINPHONE_FIND_PAYLOAD_IGNORE_RATE, LINPHONE_FIND_PAYLOAD_IGNORE_RATE, 28000, 8000 };

            string[] primaryCodecs = { "G722", "PCMU", "PCMA" };
            int[] rate = { LINPHONE_FIND_PAYLOAD_IGNORE_RATE, LINPHONE_FIND_PAYLOAD_IGNORE_RATE, LINPHONE_FIND_PAYLOAD_IGNORE_RATE };


            IntPtr _linphoneAudioCodecsList = IntPtr.Zero;
            int i;
            for (i = 0; i < primaryCodecs.Length; i++)
            {
                var pt = linphone_core_find_payload_type(linphoneCore, primaryCodecs[i],
                    rate[i],
                    LINPHONE_FIND_PAYLOAD_IGNORE_CHANNELS);

                if (pt != IntPtr.Zero)
                {
                    _linphoneAudioCodecsList = ms_list_append(_linphoneAudioCodecsList, pt);
                }
            }

            IntPtr audioCodecListPtr = linphone_core_get_audio_codecs(linphoneCore);

            MSList curStruct;
            //do
            //{
            //    curStruct.next = IntPtr.Zero;
            //    curStruct.prev = IntPtr.Zero;
            //    curStruct.data = IntPtr.Zero;
            //    curStruct = (MSList)Marshal.PtrToStructure(audioCodecListPtr, typeof(MSList));
            //    if (curStruct.data != IntPtr.Zero)
            //    {
            //        var payload = (PayloadType)Marshal.PtrToStructure(curStruct.data, typeof(PayloadType));

            //        bool found = false;
            //        for (i = 0; i < primaryCodecs.Length; i++)
            //        {
            //            if (primaryCodecs[i] == payload.mime_type)
            //            {
            //                if (rate[i] == LINPHONE_FIND_PAYLOAD_IGNORE_RATE || payload.clock_rate == rate[i])
            //                {
            //                    found = true;
            //                    break;
            //                }
            //            }
            //        }

            //        if (!found)
            //        {
            //            _linphoneAudioCodecsList = ms_list_append(_linphoneAudioCodecsList, curStruct.data);
            //        }
            //    }
            //    audioCodecListPtr = curStruct.next;
            //} while (curStruct.next != IntPtr.Zero);

            linphone_core_set_audio_codecs(linphoneCore, _linphoneAudioCodecsList);

            //  enable codecs
            audioCodecListPtr = linphone_core_get_audio_codecs(linphoneCore);
            do
            {
                curStruct.next = IntPtr.Zero;
                curStruct.prev = IntPtr.Zero;
                curStruct.data = IntPtr.Zero;
                curStruct = (MSList)Marshal.PtrToStructure(audioCodecListPtr, typeof(MSList));
                if (curStruct.data != IntPtr.Zero)
                {
                    var payload = (PayloadType)Marshal.PtrToStructure(curStruct.data, typeof(PayloadType));

                    bool enable = false;
                    for (i = 0; i < primaryCodecs.Length; i++)
                    {
                        if (primaryCodecs[i] == payload.mime_type)
                        {
                            if (rate[i] == LINPHONE_FIND_PAYLOAD_IGNORE_RATE || payload.clock_rate == rate[i])
                            {
                                enable = true;
                                break;
                            }
                        }
                    }
                    IntPtr payloadPtr = linphone_core_find_payload_type(linphoneCore, payload.mime_type, payload.clock_rate, payload.channels);
                    linphone_core_enable_payload_type(linphoneCore, payloadPtr, enable);
                }
                audioCodecListPtr = curStruct.next;
            } while (curStruct.next != IntPtr.Zero);

        }
        
        IntPtr GetFileHandle()
        {
            string exePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            //string currDate = DateTime.Now.ToString("yyyy_MM_dd");
            string fDebugLogName = exePath + "\\rpslinphone.log";
            FileStream fDebugLog = File.Create(fDebugLogName);

            return fDebugLog.Handle;
            //fDebugLog.WriteLine(sMessage);
            //fDebugLog.Flush();
            //fDebugLog.Close();
        }

    }
}

