﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using sipdotnet;

namespace RPSLinphone
{

    public class VoipParams
    {
        public string client_ip { get; set; }
        public string server_ip { get; set; }
        public int server_port { get; set; }
        public string server_pwd { get; set; }

        public override string ToString()
        {
            string sRes = string.Empty;

            sRes += $"client_ip = {client_ip}" + Environment.NewLine;
            sRes += $"server_ip = {server_ip}" + Environment.NewLine;
            sRes += $"server_port = {server_port}" + Environment.NewLine;
            sRes += $"server_pwd = {server_pwd}" + Environment.NewLine;

            return sRes;
        }

    }

    public class RPSLinphone
    {
        public static string VERSION = "1.0.0.4-09.02.23";

        #region Logging

        private static string sLogPrefix = "Linphone_";

        protected void deleteOldBMLogs()
        {
            toLog("deleteOldBMLogs -> START");

            int COUNT_OF_DAYS_FOR_STORE_LOGS = 14;
            string[] sFiles = Directory.GetFiles(@"./", sLogPrefix + "*.log", SearchOption.TopDirectoryOnly);
            int nCurrentDayOfYear = DateTime.Now.DayOfYear;
            try
            {
                toLog("deleteOldBMLogs -> Count log files = " + sFiles.Length.ToString());

                for (int i = 0; i < sFiles.Length; ++i)
                {
                    string sDate = sFiles[i].Split(new string[] { sLogPrefix }, StringSplitOptions.None)[1];
                    sDate = sDate.Split(new string[] { ".log" }, StringSplitOptions.None)[0];
                    DateTime oDate = DateTime.ParseExact(sDate, "yyyy_MM_dd", null);
                    int nDayOfYear = oDate.DayOfYear;
                    if (nCurrentDayOfYear - nDayOfYear > COUNT_OF_DAYS_FOR_STORE_LOGS)
                    {
                        File.Delete(sFiles[i]);
                        toLog("Delete old file log: " + sFiles[i]);
                    }
                }

                toLog("deleteOldBMLogs -> FINISH");
            }
            catch (Exception e)
            {
                toLog("Delete error: " + e.Message);
            }
        }
        protected void writeBMLogs(string sMessage)
        {
            try
            {
                double Ffree = 101;//свободное место на диске С
                DriveInfo di = new DriveInfo(@"C:\");
                Ffree = (di.AvailableFreeSpace / 1024) / 1024;// MB
                                                              //if (Ffree > 200)
                                                              //{
                string exePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                string currDate = DateTime.Now.ToString("yyyy_MM_dd");
                string fDebugLogName = exePath + "\\" + sLogPrefix + currDate + ".log";
                StreamWriter fDebugLog = new StreamWriter(fDebugLogName, true, Encoding.Default);
                fDebugLog.WriteLine(sMessage);
                fDebugLog.Flush();
                fDebugLog.Close();
                //}
            }
            catch
            { }
        }
        protected string HexDump(byte[] bytes, int bytesPerLine = 16)
        {
            char[] HexChars = "0123456789ABCDEF".ToCharArray();

            if (bytes == null) return "<null>";
            int bytesLength = bytes.Length;

            int firstHexColumn =
                  8                   // 8 characters for the address
                + 3;                  // 3 spaces

            int firstCharColumn = firstHexColumn
                + bytesPerLine * 3       // - 2 digit for the hexadecimal value and 1 space
                + (bytesPerLine - 1) / 8 // - 1 extra space every 8 characters from the 9th
                + 2;                  // 2 spaces 

            int lineLength = firstCharColumn
                + bytesPerLine           // - characters to show the ascii value
                + Environment.NewLine.Length; // Carriage return and line feed (should normally be 2)

            char[] line = (new String(' ', lineLength - Environment.NewLine.Length) + Environment.NewLine).ToCharArray();
            int expectedLines = (bytesLength + bytesPerLine - 1) / bytesPerLine;
            StringBuilder result = new StringBuilder(expectedLines * lineLength);

            for (int i = 0; i < bytesLength; i += bytesPerLine)
            {
                line[0] = HexChars[(i >> 28) & 0xF];
                line[1] = HexChars[(i >> 24) & 0xF];
                line[2] = HexChars[(i >> 20) & 0xF];
                line[3] = HexChars[(i >> 16) & 0xF];
                line[4] = HexChars[(i >> 12) & 0xF];
                line[5] = HexChars[(i >> 8) & 0xF];
                line[6] = HexChars[(i >> 4) & 0xF];
                line[7] = HexChars[(i >> 0) & 0xF];

                int hexColumn = firstHexColumn;
                int charColumn = firstCharColumn;

                for (int j = 0; j < bytesPerLine; j++)
                {
                    if (j > 0 && (j & 7) == 0) hexColumn++;
                    if (i + j >= bytesLength)
                    {
                        line[hexColumn] = ' ';
                        line[hexColumn + 1] = ' ';
                        line[charColumn] = ' ';
                    }
                    else
                    {
                        byte b = bytes[i + j];
                        line[hexColumn] = HexChars[(b >> 4) & 0xF];
                        line[hexColumn + 1] = HexChars[b & 0xF];
                        line[charColumn] = (b < 32 ? '·' : (char)b);
                    }
                    hexColumn += 3;
                    charColumn++;
                }
                result.Append(line);
            }
            return result.ToString();
        }
        protected void toLog(string sMessage)
        {
            StackFrame CallStack = new StackFrame(1, true);
            string sDate = DateTime.Now.ToString("HH:mm:ss.fff ");
            string sNameMethod = CallStack.GetMethod().Name;
            if (sNameMethod.Length > 14)
                sNameMethod = sNameMethod.Substring(0, 14);
            sNameMethod = sNameMethod.PadRight(15);
            string sLineNumber = CallStack.GetFileLineNumber().ToString("D4");

            string sPrefix = sDate + "[" + sNameMethod + sLineNumber + "] ";

            LogSimple(sPrefix, sMessage, true);

        }
        protected void LogSimple(string sPrefix, string sMessage, bool isConsoleUse = false)
        {
            try
            {
                string sDateForCommonLog = DateTime.Now.ToString("yyyy'.'MM'.'dd HH:mm:ss.fff ");

                string[] sArrayMes = sMessage.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                for (int i = 0; i < sArrayMes.Length; ++i)
                {
                    string sRes = sPrefix + sArrayMes[i];
                    if (isConsoleUse)
                        Console.WriteLine(sRes);
                    writeBMLogs(sRes);
                }
            }
            catch { }
        }

        #endregion


        public VoipParams voipParams { get; set; }
        public bool isCallActive { get; set; }
        public Phone phone { get; set; }

        private VoipParams getVoipParams(SqlConnection conn)
        {

            VoipParams vp = new VoipParams();

            try
            {
                SqlDataReader reader = null;
                SqlCommand cmd = null;

                cmd = new SqlCommand("select top 1 Value from Setting where Name = 'WEBServer'", conn);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    vp.server_ip = reader["Value"].ToString();
                }
                reader.Close();

                cmd = new SqlCommand("select top 1 Host from DeviceModel", conn);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    vp.client_ip = reader["Host"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                toLog(e.Message + e.StackTrace);
            }
           
            vp.server_port = 6153;
            vp.server_pwd = "1111";

            toLog($"{vp.client_ip} -> {vp.server_ip}:{vp.server_port} by {vp.server_pwd}");


            return vp;
        }

        public RPSLinphone(SqlConnection conn)
        {
            toLog("START");

            voipParams = getVoipParams(conn);
            isCallActive = false;

            toLog(voipParams.ToString());

            Account account = new Account(voipParams.client_ip, voipParams.server_pwd, voipParams.server_ip, voipParams.server_port);
            phone = new Phone(account);

            phone.PhoneConnectedEvent += delegate () {
                try
                {
                    toLog("Phone connected");
                }
                catch { }
            };

            phone.CallActiveEvent += delegate (Call call) {
                try
                {
                    toLog("Answered. Call is active!");
                }
                catch { }
            };

            phone.CallCompletedEvent += delegate (Call call) {
                try
                {
                    toLog("Completed");
                    phone.TerminateCall(call);
                    isCallActive = false; 
                }
                catch (Exception e)
                {
                    toLog(e.Message + e.StackTrace);
                }
                
            };

            phone.ErrorEvent += delegate (Call call, Phone.Error error) {

                try
                {
                    toLog(error.ToString());
                    phone.TerminateCall(call);
                    isCallActive = false;
                }
                catch (Exception e)
                {
                    toLog(e.Message + e.StackTrace);
                }

            };

            phone.IncomingCallEvent += delegate (Call call) {

                try
                {
                    toLog("Incoming call.");
                    phone.ReceiveCall(call);
                    isCallActive = true;
                }
                catch (Exception e)
                {
                    toLog(e.Message + e.StackTrace);
                }

            };

            try
            {
                phone.Connect();
            }
            catch (Exception e)
            {
                toLog(e.Message + e.StackTrace);
            }

            toLog(phone.CurrentConnectState.ToString());

            toLog(GetLinphoneInfo());
        }

        ~RPSLinphone()
        {
            try
            {
                phone.Disconnect();
            }
            catch (Exception e)
            {
                toLog(e.Message + e.StackTrace);
            }

            toLog("STOP");
        }

        public string GetLinphoneInfo()
        {
            if (phone == null)
                throw new InvalidOperationException("not connected");

            return phone.GetLinphoneInfo();
        }

    }
}
