﻿using PCSC;
using PCSC.Iso7816;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
//using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Communications
{
    
    public enum KeyStructure : byte
    {
        VolatileMemory = 0x00,
        NonVolatileMemory = 0x20
    }

    public enum KeyType : byte
    {
        KeyA = 0x60,
        KeyB = 0x61
    }

    public enum CardType : byte
    {
        MifareCard = 0x00,
        ProxCardII = 0x01,
        OtherCard = 0x02,
        iClass=0x03
    }

    public enum ReadWriteStatus : byte
    {
        Good = 0x00,
        TryAgain = 0x01,
        BadKey = 0x02,
        NoAuth = 0x03,
        Error = 0x04,
        NoCard = 0x05,
        YesCard = 0x06
    }

    public class GeneralAuthenticate
    {
        public byte Version { get; } = 0x01;

        public byte MSB { get; set; }
        public byte LSB { get; set; }
        public KeyType KeyType { get; set; }
        public byte KeyNumber { get; set; }
        public byte[] ToArray()
        {
            return new[] { Version, MSB, LSB, (byte)KeyType, KeyNumber };
        }
    }

    public class MifareCard
    {
        private const byte CUSTOM_CLA = 0xFF;
        private readonly IIsoReader _isoReader;

        public MifareCard(IIsoReader isoReader)
        {
            if (isoReader == null)
            {
                throw new ArgumentNullException(nameof(isoReader));
            }
            _isoReader = isoReader;
        }

        public bool LoadKey(KeyStructure keyStructure, byte keyNumber, byte[] key)
        {
            var loadKeyCmd = new CommandApdu(IsoCase.Case3Short, SCardProtocol.Any)
            {
                CLA = CUSTOM_CLA,
                Instruction = InstructionCode.ExternalAuthenticate,
                P1 = (byte)keyStructure,
                P2 = keyNumber,
                Data = key
            };

            //Debug.WriteLine("Load Authentication Keys: {0}", BitConverter.ToString(loadKeyCmd.ToArray()));
            var response = _isoReader.Transmit(loadKeyCmd);
            //Debug.WriteLine("SW1 SW2 = {0:X2} {1:X2}", response.SW1, response.SW2);

            return IsSuccess(response);
        }


        public bool Authenticate(byte msb, byte lsb, KeyType keyType, byte keyNumber)
        {
            var authBlock = new GeneralAuthenticate
            {
                MSB = msb,
                LSB = lsb,
                KeyNumber = keyNumber,
                KeyType = keyType
            };

            var authKeyCmd = new CommandApdu(IsoCase.Case3Short, SCardProtocol.Any)
            {
                CLA = CUSTOM_CLA,
                Instruction = InstructionCode.InternalAuthenticate,
                P1 = 0x00,
                P2 = 0x00,
                Data = authBlock.ToArray()
            };

            Debug.WriteLine("General Authenticate: {0}", BitConverter.ToString(authKeyCmd.ToArray()));
            var response = _isoReader.Transmit(authKeyCmd);
            Debug.WriteLine("SW1 SW2 = {0:X2} {1:X2}", response.SW1, response.SW2);

            return (response.SW1 == 0x90) && (response.SW2 == 0x00);
        }

        public byte[] ReadBinary(byte msb, byte lsb, int size)
        {
            unchecked
            {
                CommandApdu readBinaryCmd = new CommandApdu(IsoCase.Case2Short, SCardProtocol.Any)
                {
                    CLA = CUSTOM_CLA,
                    Instruction = InstructionCode.ReadBinary,
                    P1 = msb,
                    P2 = lsb,
                    Le = size
                };

                Debug.Print("Read Binary (before update): {0}", BitConverter.ToString(readBinaryCmd.ToArray()));
                Response response = _isoReader.Transmit(readBinaryCmd);
                Debug.Print("SW1 SW2 = {0:X2} {1:X2}", response.SW1, response.SW2);
                /*
                Debug.Print("SW1 SW2 = {0:X2} {1:X2} Data: {2}",
                    response.SW1,
                    response.SW2,
                    BitConverter.ToString(response.GetData()));
                    */
                return IsSuccess(response)
                    ? response.GetData() ?? new byte[0]
                    : null;
            }
        }

        public bool UpdateBinary(byte msb, byte lsb, byte[] data)
        {
            var updateBinaryCmd = new CommandApdu(IsoCase.Case3Short, SCardProtocol.Any)
            {
                CLA = CUSTOM_CLA,
                Instruction = InstructionCode.UpdateBinary,
                P1 = msb,
                P2 = lsb,
                Data = data
            };

            //Debug.WriteLine("Update Binary: {0}", BitConverter.ToString(updateBinaryCmd.ToArray()));
            var response = _isoReader.Transmit(updateBinaryCmd);
            Debug.WriteLine("SW1 SW2 = {0:X2} {1:X2}", response.SW1, response.SW2);

            return IsSuccess(response);
        }

        /*
        public bool UpdateTime1()
        {
            var updateTimeCmd = new CommandApdu(IsoCase.Case3Short, SCardProtocol.Any)
            {
                CLA = CUSTOM_CLA,
                Instruction = InstructionCode.UpdateBinary,
                P1 = 0xFF,
                P2 = 0x68,
                Data = data
            };

            //Debug.WriteLine("Update Binary: {0}", BitConverter.ToString(updateBinaryCmd.ToArray()));
            var response = _isoReader.Transmit(updateTimeCmd);
            Debug.WriteLine("SW1 SW2 = {0:X2} {1:X2}", response.SW1, response.SW2);

            return IsSuccess(response);
        }
        */
        private static bool IsSuccess(Response response) =>
            (response.SW1 == (byte)SW1Code.Normal) && (response.SW2 == 0x00);
    }

    public class OmniKey5427
    {
        public string OmniStat { get; set; }

        public string Status;
        public string ReaderMode;
        public CardType cardtype;

        public List<string> DevicesList;
        public int DevicesCount = 0;
        public string DeviceName = "";

        public byte[] CardID;

        public byte[] ReadBlock = new byte[48];
        public byte[] WriteBlock = new byte[48];

        public byte[] rBlock1 = new byte[16];
        public byte[] rBlock2 = new byte[16];
        public byte[] rBlock3 = new byte[16];

        public byte[] wBlock1 = new byte[16];
        public byte[] wBlock2 = new byte[16];
        public byte[] wBlock3 = new byte[16];

        public byte[] KEY { get; set; } //6 байтовый ключ (берем с класса...)
        public byte Sector;

        public byte KeySlot = 0x00; //по умолчанию 1 слот...

        private const byte MSB = 0x00;
        public byte LSB = 0x00;

        public Timer tmrW;

        public OmniKey5427()
        {
            CardID = new byte[8];

            for (int i = 0; i < 8; i++)
            {
                CardID[i] = 0x00;
            }

            if (GetReader())
            {
            }
            else
            {
                Status = "не отвечает";
            }
        }

        public ReadWriteStatus ReadMifare()
        {
            try
            {
            OmniStat = "before_factory";
                IContextFactory contextFactory = ContextFactory.Instance;
            OmniStat = "before_using";
            using (ISCardContext context = contextFactory.Establish(SCardScope.System))
                {
                OmniStat = "before_isoreader";
                using (var isoReader = new IsoReader(context, DeviceName, SCardShareMode.Shared, SCardProtocol.Any, false))
                    {
                    OmniStat = "before_mifarecard";
                    MifareCard card = new MifareCard(isoReader);

                        bool loadKeySuccessful = card.LoadKey(
                            KeyStructure.NonVolatileMemory,
                            KeySlot, // first key slot
                            KEY // key
                        );

                        if (!loadKeySuccessful)
                        {
                            return ReadWriteStatus.BadKey;
                        }

                        LSB = 0x04;
                        bool authSuccessful = card.Authenticate(MSB, LSB, KeyType.KeyA, 0x00);
                        if (!authSuccessful)
                        {
                            return ReadWriteStatus.NoAuth;
                        }

                        LSB = 0x04;
                        var result = card.ReadBinary(MSB, LSB, 16);
                        if (result != null)
                        {
                            rBlock1 = result;
                        }

                        LSB = 0x05;
                        var result2 = card.ReadBinary(MSB, LSB, 16);
                        if (result2 != null)
                        {
                            rBlock2 = result2;
                        }

                        LSB = 0x06;
                        var result3 = card.ReadBinary(MSB, LSB, 16);
                        if (result3 != null)
                        {
                            rBlock3 = result3;
                        }
                        if (result != null && result2 != null && result3 != null)
                        {
                            card = null;
                            FillTheReadArray();
                            return ReadWriteStatus.Good;
                        }
                        else
                        {
                            return ReadWriteStatus.TryAgain;
                        }                        
                    }
                }
            }
            
            catch (RemovedCardException rex)
            {
                return ReadWriteStatus.NoCard;
            }
            catch (Exception ex)
            {
                return ReadWriteStatus.TryAgain;
            }
            
        }

        public ReadWriteStatus WriteMifare()
        {
            try
            {
                IContextFactory contextFactory = ContextFactory.Instance;
                using (ISCardContext context = contextFactory.Establish(SCardScope.System))
                {
                    using (var isoReader = new IsoReader(context, DeviceName, SCardShareMode.Shared, SCardProtocol.Any, false))
                    {
                        MifareCard card = new MifareCard(isoReader);

                        bool loadKeySuccessful = card.LoadKey(
                            KeyStructure.NonVolatileMemory,
                            KeySlot, // first key slot
                            KEY // key
                        );

                        if (!loadKeySuccessful)
                        {
                            //Debug.Print("LOAD KEY failed.");
                            return ReadWriteStatus.BadKey;
                        }
                        LSB = 0x04;
                        bool authSuccessful = card.Authenticate(MSB, LSB, KeyType.KeyA, 0x00);
                        if (!authSuccessful)
                        {
                            //Debug.Print("AUTHENTICATE failed.");
                            return ReadWriteStatus.NoAuth;
                        }

                        FillTheWriteArray();
                        //byte[] wdata;

                        LSB = 0x04;
                        bool result = card.UpdateBinary(MSB, LSB, wBlock1);

                        LSB = 0x05;
                        bool result2 = card.UpdateBinary(MSB, LSB, wBlock2);

                        LSB = 0x06;
                        bool result3 = card.UpdateBinary(MSB, LSB, wBlock3);

                        if (result && result2 && result3)
                        {
                            card = null;
                            return ReadWriteStatus.Good;
                        }
                        else
                        {
                            return ReadWriteStatus.TryAgain;
                        }
                    }
                }
            }
            catch (RemovedCardException rex)
            {
                return ReadWriteStatus.NoCard;
            }
            catch (Exception ex)
            {
                return ReadWriteStatus.Error;
            }
        }


        public void FillTheReadArray()
        {
            for (int j = 0; j < 3; j++)
            {
                for (int i = 0; i < 16; i++)
                {
                    if (j == 0)
                    {
                        ReadBlock[i] = rBlock1[i];
                    }
                    else if (j == 1)
                    {
                        ReadBlock[i + 16] = rBlock2[i];
                    }
                    else if (j == 2)
                    {
                        ReadBlock[i + 32] = rBlock3[i];
                    }
                }
            }
        }

        public void FillTheWriteArray()
        {
            for (int j = 0; j < 3; j++)
            {
                for (int i = 0; i < 16; i++)
                {
                    if (j == 0)
                    {
                        wBlock1[i] = WriteBlock[i];
                    }
                    else if (j == 1)
                    {
                        wBlock2[i] = WriteBlock[i + 16];
                    }
                    else if (j == 2)
                    {
                        wBlock3[i] = WriteBlock[i + 32];
                    }
                }
            }
            //WriteBlock[39] = 0x13; //!!!
            //WriteBlock[40] = 0x13; //!!!
        }

        public bool GetReader()
        {
            try
            {
                IContextFactory contextFactory = ContextFactory.Instance;
                using (ISCardContext context = contextFactory.Establish(SCardScope.System))
                {
                    DevicesList = new List<string>();
                    string[] readerNames = context.GetReaders();
                    foreach (string readerName in readerNames)
                    {
                        DevicesList.Add(readerName);
                    }
                    if (DevicesList.Count > 0)
                    {
                        Debug.Print(DevicesList[0]);
                        DeviceName = DevicesList[0];
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //Reader Status...

        /*
    public void GetReaderState()
    {
        SCardReaderState readerState = context.GetReaderStatus(DeviceName);
        PrintReaderState(readerState);
        readerState.Dispose();
    }


    public void PrintReaderState(SCardReaderState state)
    {
        //Debug.Print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        var atr = BitConverter.ToString(state.Atr ?? new byte[0]);

        Debug.Print($"Reader: {state.ReaderName}\n" +
                          $"CurrentState: {state.CurrentState}\n" +
                          $"EventState: {state.EventState}\n" +
                          $"CurrentStateValue: {state.CurrentStateValue}\n" +
                          $"EventStateValue: {state.EventStateValue}\n" +
                          $"UserData: {state.UserData}\n" +
                          $"CardChangeEventCnt: {state.CardChangeEventCnt}\n" +
                          $"ATR: {atr}");
        //ATR = atr;
    }
    */

        //Получение типа карты...
        public ReadWriteStatus GetCardType()
        {
                IContextFactory contextFactory = ContextFactory.Instance;
                using (ISCardContext context = contextFactory.Establish(SCardScope.System))
                {
                    try
                    {
                        SCardReaderState state = context.GetReaderStatus(DeviceName);
                        if (state.Atr.Length == 0)
                        {
                        context.Dispose();
                        return ReadWriteStatus.Error;
                        }
                        else
                        {
                            int atrlen = state.Atr.Length;
                            byte lastbyte = state.Atr[atrlen - 1];
                            if (lastbyte == 0x6A || lastbyte==0x5D)
                            {
                                cardtype = CardType.MifareCard;
                            }
                            else if (lastbyte == 0x28)
                            {
                                cardtype = CardType.ProxCardII;
                            }
                            else if (lastbyte == 0x7A)
                            {
                            cardtype = CardType.iClass;
                            }
                            else
                            {
                                cardtype = CardType.OtherCard;
                            }
                        
                        return ReadWriteStatus.Good;
                        }
                    }
                    catch (Exception exx)
                    {
                    context.Dispose();
                    return ReadWriteStatus.Error;
                    }
                }
        }

        public void RageDisconnect()
        {
            IContextFactory contextFactory = ContextFactory.Instance;
            using (ISCardContext context = contextFactory.Establish(SCardScope.System))
            {
                using (SCardReader rfidReader = new SCardReader(context))
                {
                    try
                    {
                        SCardError sc = rfidReader.Connect(DeviceName, SCardShareMode.Shared, SCardProtocol.Any);
                        rfidReader.Disconnect(SCardReaderDisposition.Reset);
                    }
                    catch (Exception ex)
                    {
                        if (rfidReader.IsConnected)
                        {
                            //rfidReader.EndTransaction(SCardReaderDisposition.Leave);
                            rfidReader.Disconnect(SCardReaderDisposition.Reset);
                        }
                        rfidReader.Dispose();
                        context.Dispose();
                    }
                }
            }
        }

        //Получение наличия карты...
        public ReadWriteStatus GetCard()
        {
                IContextFactory contextFactory = ContextFactory.Instance;
                using (ISCardContext context = contextFactory.Establish(SCardScope.System))
                {
                    using (SCardReader rfidReader = new SCardReader(context))
                    {
                    try
                    {
                        SCardError sc = rfidReader.Connect(DeviceName, SCardShareMode.Shared, SCardProtocol.Any);
                        //Debug.Print(sc.ToString());
                        if (sc != SCardError.Success)
                        {
                            if (rfidReader.IsConnected)
                            {
                                rfidReader.EndTransaction(SCardReaderDisposition.Leave);
                            }
                            rfidReader.Dispose();
                            context.Dispose();
                            return ReadWriteStatus.NoCard;
                        }
                        else
                        {
                            if (rfidReader.IsConnected)
                            {
                            rfidReader.EndTransaction(SCardReaderDisposition.Leave);
                            }
                            //rfidReader.Disconnect(SCardReaderDisposition.Reset);
                            //rfidReader.Dispose();
                            //context.Dispose();
                            return ReadWriteStatus.Good;
                        }
                    }
                    catch (Exception ex)
                    {
                        if (rfidReader.IsConnected)
                        {
                            rfidReader.EndTransaction(SCardReaderDisposition.Leave);
                            //rfidReader.Disconnect(SCardReaderDisposition.Reset);
                        }
                        rfidReader.Dispose();
                        context.Dispose();
                        return ReadWriteStatus.Error;
                    }
                }
            }

        }

        //Получение ID карты
        public ReadWriteStatus GetID()
        {
                IContextFactory contextFactory = ContextFactory.Instance;
                using (ISCardContext context = contextFactory.Establish(SCardScope.System))
                {
                    using (SCardReader rfidReader = new SCardReader(context))
                    {
                    try
                    {
                        var sc = rfidReader.Connect(DeviceName, SCardShareMode.Shared, SCardProtocol.Any);
                        if (sc != SCardError.Success)
                        {
                            //Debug.Print("No Card");
                            if (rfidReader.IsConnected)
                            {
                                rfidReader.EndTransaction(SCardReaderDisposition.Leave);
                            }
                            rfidReader.Dispose();
                            context.Dispose();
                            return ReadWriteStatus.NoCard;
                        }

                        CommandApdu apdu = new CommandApdu(IsoCase.Case2Short, rfidReader.ActiveProtocol)
                        {
                            CLA = 0xFF,
                            Instruction = InstructionCode.GetData,
                            P1 = 0x00,
                            P2 = 0x00,
                            Le = 0
                        };

                        sc = rfidReader.BeginTransaction();
                        if (sc != SCardError.Success)
                        {
                            if (rfidReader.IsConnected)
                            {
                                rfidReader.EndTransaction(SCardReaderDisposition.Leave);
                            }
                            rfidReader.Dispose();
                            context.Dispose();
                            return ReadWriteStatus.TryAgain;
                        }

                        SCardPCI receivePci = new SCardPCI(); // IO returned protocol control information.
                        IntPtr sendPci = SCardPCI.GetPci(rfidReader.ActiveProtocol);

                        var receiveBuffer = new byte[256];
                        var command = apdu.ToArray();

                        sc = rfidReader.Transmit(
                            sendPci, // Protocol Control Information (T0, T1 or Raw)
                            command, // command APDU
                            receivePci, // returning Protocol Control Information
                            ref receiveBuffer); // data buffer

                        if (sc != SCardError.Success)
                        {
                            //Debug.Print("Error: " + SCardHelper.StringifyError(sc));
                            if (rfidReader.IsConnected)
                            {
                                rfidReader.EndTransaction(SCardReaderDisposition.Leave);
                                //rfidReader.Disconnect(SCardReaderDisposition.Reset);
                            }
                            rfidReader.Dispose();
                            context.Dispose();
                            return ReadWriteStatus.Error;
                        }

                        var responseApdu = new ResponseApdu(receiveBuffer, IsoCase.Case2Short, rfidReader.ActiveProtocol);
                        Debug.Print("SW1: {0:X2}, SW2: {1:X2}\nUid: {2}",
                            responseApdu.SW1,
                            responseApdu.SW2,
                            responseApdu.HasData ? BitConverter.ToString(responseApdu.GetData()) : "Не получил UID");

                        if (responseApdu == null)
                        {
                            if (rfidReader.IsConnected)
                            {
                                rfidReader.EndTransaction(SCardReaderDisposition.Leave);
                                //rfidReader.Disconnect(SCardReaderDisposition.Reset);
                            }
                            rfidReader.Dispose();
                            context.Dispose();
                            return ReadWriteStatus.Error;
                        }
                        else
                        {
                            //Сделать проверку типа!!!
                            for (int i = 0; i < 8; i++)
                            {
                                CardID[i] = 0x00;
                            }

                            byte[] iCardID = responseApdu.GetData();
                            if (cardtype == CardType.MifareCard)
                            {
                                if (iCardID.Length == 4)
                                {
                                    for (int i = 0; i < 4; i++)
                                    {
                                        CardID[i + 4] = iCardID[i];
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < 7; i++)
                                    {
                                        CardID[i + 1] = iCardID[i];
                                    }
                                }
                            }
                            else
                            {
                                /*
                                for (int i = 0; i < iCardID.Length; i++)
                                {
                                    CardID[i + (8 - iCardID.Length)] = iCardID[i];
                                }
                                */
                                CardID[4] = iCardID[1];
                                CardID[5] = iCardID[2];
                                CardID[6] = iCardID[3];
                                if (iCardID.Length > 4)
                                {
                                    CardID[7] = iCardID[4];
                                }
                            }
                        }
                        if (rfidReader.IsConnected)
                        {
                            rfidReader.EndTransaction(SCardReaderDisposition.Leave);
                            //rfidReader.Disconnect(SCardReaderDisposition.Reset);
                        }
                        //rfidReader.Dispose();
                        //context.Dispose();

                        //rfidReader.Disconnect(SCardReaderDisposition.Reset); //Прикол тут!!!???
                        return ReadWriteStatus.Good;
                    }
                    catch (Exception ex)
                    {
                        if (rfidReader.IsConnected)
                        {
                            rfidReader.EndTransaction(SCardReaderDisposition.Leave);
                            //rfidReader.Disconnect(SCardReaderDisposition.Reset);
                        }
                        rfidReader.Dispose();
                        context.Dispose();
                        return ReadWriteStatus.Error;
                    }
                }
            }

        }
    }
}
