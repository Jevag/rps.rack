﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class ScanNT7780
    {

        bool isOpen;
        string command = "";
        SerialPort _serialPort;
        public string Status { get; private set; }             // статус сканера Barcode
        public bool LoopA { get; set; }

        public string barcode { get; set; }

        System.Timers.Timer tmrScanImpulse;
        System.Timers.Timer tmrRX;

        int bytescountrx = 0;
        int bytescountrx2 = 0;
        public bool scan_started = false;

        public static bool IsNumeric(string s)
        {
            double output;
            return double.TryParse(s, out output);
        }

        public ScanNT7780()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.None;
            //_serialPort.WriteTimeout = 200;
            //_serialPort.ReadTimeout = 200;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            //_serialPort.Handshake = Handshake.None;
            _serialPort.DataReceived += _serialPort_DataReceived;
            
            tmrScanImpulse = new System.Timers.Timer();
            tmrScanImpulse.Interval = 5000; //250...
            tmrScanImpulse.Elapsed += TmrScanImpulse_Elapsed;
            tmrScanImpulse.Enabled = false;
            tmrScanImpulse.AutoReset = true;
            
            tmrRX = new System.Timers.Timer();
            tmrRX.Interval = 200; //250...
            tmrRX.Elapsed += TmrRX_Elapsed;
            tmrRX.Enabled = false;
            tmrRX.AutoReset = true;

            scan_started = false;
        }

        private void TmrRX_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            try
            {
                if (isOpen)
                {
                    bytescountrx2 += _serialPort.BytesToRead;
                    //Debug.Print(bytescountrx.ToString());


                    if (bytescountrx2 > 0)
                    {
                        byte[] tbytes = new byte[bytescountrx2];
                        _serialPort.Read(tbytes, 0, bytescountrx2);
                        //Debug.Print(ByteArrayToString(tbytes));
                        //Debug.Print(ByteArrayEncode(tbytes));

                        string bc = ByteArrayEncode(tbytes);
                        string p1 = bc.Substring(0, 10);
                        string p2 = bc.Substring(10, 9);
                        if (tbytes.Length == 22 && IsNumeric(p1) && IsNumeric(p2))
                        {
                            barcode = ByteArrayEncode(tbytes).Substring(0, 19);
                        }
                        else
                        {
                            barcode = ByteArrayEncode(tbytes);
                            barcode = barcode.Trim(' ');
                            barcode = barcode.Replace(Environment.NewLine, "");
                        }

                        bytescountrx = 0;
                        bytescountrx2 = 0;
                        _serialPort.DiscardInBuffer();
                        _serialPort.DiscardOutBuffer();
                        //scan_started = true; //когда получил данные
                        tmrScanImpulse.Enabled = true;
                        tmrRX.Enabled = false;
                    }
                }
            }
            catch (Exception ex) { }
        }

        private void TmrScanImpulse_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            scan_started = false;
            tmrScanImpulse.Enabled = false;
            //throw new NotImplementedException();
            //EnableLight();
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            if (isOpen)
            {
                bytescountrx += _serialPort.BytesToRead;

                if (bytescountrx > 0)
                {
                    tmrRX.Enabled = true;
                }
            }
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    isOpen = true;
                    return true;
                }
                catch (Exception e)
                {
                    isOpen = false;
                    Status = "не отвечает";

                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    isOpen = false;
                    return true;
                }
                catch (Exception e)
                {
                    Status = "не отвечает";
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool CommandTrigger()
        {
            try
            {
                byte[] ba = new byte[] { 0x02, 0xf4, 0x03 };
                _serialPort.Write(ba, 0, 3);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool ReleaseTrigger()
        {
            try
            {
                byte[] ba = new byte[] { 0x02, 0xf5, 0x03 };
                _serialPort.Write(ba, 0, 3);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public void SendCrlF()
        {
            try
            {
                string prms = "090300.";
                byte[] ba0 = Encoding.ASCII.GetBytes(prms);
                byte[] ba1 = new byte[] { 0x02, 0xf0, 0x03 };
                byte[] ba = ba1.Concat(ba0).ToArray();

                //Debug.Print(ByteArrayToString(ba));

                _serialPort.Write(ba, 0, ba.Length);
            }
            catch (Exception ex) { }
        }

        public void ManualTriggerMode()
        {
            try
            {
                string prms = "091A00.";
                byte[] ba0 = Encoding.ASCII.GetBytes(prms);
                byte[] ba1 = new byte[] { 0x02, 0xf0, 0x03 };
                byte[] ba = ba1.Concat(ba0).ToArray();

                //Debug.Print(ByteArrayToString(ba));

                _serialPort.Write(ba, 0, ba.Length);
            }
            catch (Exception ex) { }
        }

        public void PresentationMode1()
        {
            try
            {
                string prms = "090901.";
                byte[] ba0 = Encoding.ASCII.GetBytes(prms);
                byte[] ba1 = new byte[] { 0x02, 0xf0, 0x03 };
                byte[] ba = ba1.Concat(ba0).ToArray();

                //Debug.Print(ByteArrayToString(ba));

                _serialPort.Write(ba, 0, ba.Length);
            }
            catch (Exception ex) { }
        }

        public void GetFirmware()
        {
            try
            {
                string prms = "0D1302?.";
                byte[] ba0 = Encoding.ASCII.GetBytes(prms);
                byte[] ba1 = new byte[] { 0x02, 0xf0, 0x03 };
                byte[] ba = ba1.Concat(ba0).ToArray();

                //Debug.Print(ByteArrayToString(ba));

                _serialPort.Write(ba, 0, ba.Length);
            }
            catch (Exception ex) { }
        }

        public void GetStatus()
        {
            try
            {
                byte[] ba = new byte[] { 0x02, 0x41, 0x03 };

                //Debug.Print(ByteArrayToString(ba));

                _serialPort.Write(ba, 0, 3);
            }
            catch (Exception ex) { }
        }

        public bool TestCommand()
        {
            //21 61 42 01
            //[0X02][0XF0][0X03]090300
            //byte[] ba = new byte[] { 0x02, 0x41, 0x03 };
            byte[] ba = new byte[] { 0x02, 0xf4, 0x03 };
            //tx[0] = 0x32; tx[1] = 0x75; tx[2] = 0x01;
            //txSize = 3;
            //try
            //{
            //    _serialPort.DiscardInBuffer();
            //    _serialPort.DiscardOutBuffer();
            _serialPort.Write(ba, 0, 3);
                /*
                rxSize = 0;
                qrid = "";
                status = "ожидание";
                */
                return true;
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("Exception3: " + e.Message);
            //    return false;
            //}
        }

      

        #region statics
        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba); //.Replace("-", "");
        }

        public static string ByteArrayEncode(byte[] data)
        {
            char[] characters = data.Select(b => (char)b).ToArray();
            return new string(characters);
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string SingleByteToString(byte ba)
        {
            byte[] b = new byte[1];
            b[0] = ba;

            return BitConverter.ToString(b);
        }
        /*
        public static void CalcExample()
        {
            byte Datas = (0xD4 & (~0x30)) + 0x30;    //(0xD4 & （！ 0x03） ) + 0x02;
            Debug.Print(SingleByteToString(Datas));
        }
        */
        #endregion


    }
}
