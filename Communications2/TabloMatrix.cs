﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class TabloMatrix
    {
        SerialPort _serialPort;

        string status;

        public void SerialInit()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 2400;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 2000;
            _serialPort.ReadTimeout = 2000;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
            //_serialPort.Encoding = Encoding.GetEncoding("CP866");
            _serialPort.DataReceived += _serialPort_DataReceived; ;
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public bool Open(string Port)
        {
            if (_serialPort == null)
            {
                SerialInit();
            }
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    if (status == "не отвечает") status = "";

                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    Console.WriteLine("Возникло исключение: " + e.ToString());
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool Close()
        {
            try
            {
                _serialPort.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void SendValue(int Num, byte clr, byte bright)
        {
            //int a = 7788;

            byte[] bt = BitConverter.GetBytes(Num);

            byte b1 = bt[0];
            byte b2 = bt[1];

            byte[] ba = new byte[] { 0xff, 0xd0, clr, b2, b1, 0x01, bright, 0xFD };
            _serialPort.Write(ba, 0, ba.Length);
        }
    }
}
