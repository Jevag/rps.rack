using System;

namespace communication
{
    public abstract class SlaveIf
    {
        public SlaveIf() { }

        public virtual bool Open(string portName, bool startReopenTimer = true) { return false; }
        public virtual bool Close() { return false; }

        public virtual bool GetSlaveInfo() { return false; }
        public virtual bool SetBootLoaderMode() { return false; }
        public virtual void WriteToAllDiskrets(string mask_a, string mask_b, string mask_h) { }
        public virtual void WriteToDiskretAB(string mask_a, string mask_b) { }
        public virtual void GetPowerErrors() { }
        public virtual void GetPowerOut() { }

        /**
         * @brief �������� ������� �� �����
         * @param channel - ����� ������ ������� �� 1 �� 10 ������������
         */
        public virtual void SetPowerTo1(int channel) { }

        /**
         * @brief ��������� ������� �� �����
         * @param channel ����� ������ ������� �� 1 �� 10 ������������
         */
        public virtual void SetPowerTo0(int channel) { }

        /**
         * @brief ���������� ������� �������
         * @param mask ��������� ����� �� 0 � 1 ��� ������ � ���� �������
         */
        public virtual void WriteToDiskretPower(string mask) { }

        public virtual void GetClimate() { }
        public virtual void GetClimate2() { }
        public virtual void SetClimate(bool HeaterStatus, bool FanStatus, bool AutoMode) { }

        /**
         * @brief ��������� ������ ������ ����������
         * @param Mode ����� ������. 0 - �������������� �����, 1 - ������ �����
         * @param FanEn true - �������� ����������, false - ��������� ����������
         * @param HeaterEn true - �������� �����, false - ��������� �����
         * @param save_config true - ��������� ��������� � ���
         * @param high_thrs ����������� ��������� �����������, �
         * @param low_thrs ����������� ��������� �����, �
         * @param hum_thrs ����� ���������, %
         * @param channel ����� ���������� �����������, 0 - ������ �� ������, 1 - ������� ������
         */
        public virtual void SetClimate2(byte Mode, bool FanEn, bool HeaterEn, bool save_config, float high_thrs, float low_thrs, float hum_thrs, byte channel) { }
        public virtual void GetWiegand() { }

        public virtual void GetAllParameters() { }


        /**
         * @brief ������� ������� �������
           @param out_type ����� �����. 0 - PORTA, 1 - PORTB, 2 - PORTH (Relays)
         * @param channel ����� ������ � �����. ��� PORTA, PORTB � ��������� �� 0 �� 3, ��� PORTH (Relays) � ��������� �� 
         * @param width_ms ������ �������� � ��
         * @param delay_ms �������� ������ �������� � ��
         * 
         * ���������� ������� �� ������ ��������. ����� �������� ��������� ������������������ ��� ������:
         *   -# ��������� �������
         *   -# ����������� �������� �� delay_ms ����������
         *   -# �������������� ������ � ������������ � out_type � channel
         *   -# ����������� �������� �� width_ms ����������
         *   -# �������������� ������         
         */
        public virtual void SetPulseCommand(int out_type, uint channel, uint width_ms, uint delay_ms) { }

        public struct Climate
        {
            public float LocalT;
            public float OutsideSensorT;
            public float GigrometerT;
            public float Humidity;

            public bool HeaterStatus;
            public bool FanStatus;
            public bool AutoMode;
        }
        public Climate ClimateState;

        public string ClimateStateInfo { get; set; }
        public string command { get; set; }
        public bool IsOpen { get; set; } = false;
        public bool SlaveSuccess { get; set; }
        public bool BlockMove { get; set; }
        public bool ChangeCommand { get; set; }
        public bool BusyWrite { get; set; }
        public int SlaveEtap { get; set; }

        public struct SlaveInfoS
        {
            public string MemorySize;
            public string ID;
            public string Version;
            public string mName;
        };
        public SlaveInfoS SlaveInfo;

        public struct DiskretInput
        {
            public bool D1;
            public bool D2;
            public bool D3;
            public bool D4;
        }
        public DiskretInput PortA_In = new DiskretInput();
        public DiskretInput PortB_In = new DiskretInput();
        public DiskretInput PortA_Out = new DiskretInput();
        public DiskretInput PortB_Out = new DiskretInput();
        static public string PortABRepr(in DiskretInput portab)
        {
            return String.Format("{3}{2}{1}{0}",
                                    portab.D1 ? 1 : 0,
                                    portab.D2 ? 1 : 0,
                                    portab.D3 ? 1 : 0,
                                    portab.D4 ? 1 : 0
                                );
        }
        public struct RelaysInOut
        {
            public bool R1;
            public bool R2;
            public bool R3;
            public bool R4;
            public bool R5;
            public bool R6;
            public bool R7;
            public bool R8;
            public bool R9;
            public bool R10;
        }
        public RelaysInOut RelaysStateIn;
        public RelaysInOut RelaysStateOut;
        public RelaysInOut PowerState;
        public RelaysInOut PowerErrors;

        static public string RelaysRepr(in RelaysInOut relay)
        {
            return String.Format("{9}{8}{7}{6}{5}_{4}{3}{2}{1}{0}",
                                    relay.R1 ? 1 : 0,
                                    relay.R2 ? 1 : 0,
                                    relay.R3 ? 1 : 0,
                                    relay.R4 ? 1 : 0,
                                    relay.R5 ? 1 : 0,
                                    relay.R6 ? 1 : 0,
                                    relay.R7 ? 1 : 0,
                                    relay.R8 ? 1 : 0,
                                    relay.R9 ? 1 : 0,
                                    relay.R10 ? 1 : 0
                                );
        }
        public void DecodePulseChannel(in int channel, out int out_type, out int out_channel)
        {
            if (channel >= 0 && channel < 4)
            {
                out_type = 0;
                out_channel = channel;
            }
            else if (channel >= 4 && channel < 8)
            {
                out_type = 1;
                out_channel = channel - 4;
            }
            else if (channel >= 8)
            {
                out_type = 2;
                out_channel = channel - 8;
            }
            else
            {
                throw new System.Exception("Invalid argument");
            }
        }
    }
}
