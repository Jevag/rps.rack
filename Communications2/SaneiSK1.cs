﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Communications
{
    public class SaneiSK1
    {
        #region vars
        public bool wait;
        public bool port_find;

        public int ToCut = 10;
        public int ReplayTime = 5;

        public SerialPort _serialPort;

        public string command = "";

        public string status { get; set; } = "";
        public string presenterstatus { get; set; } = "";
        public string hardstatus { get; set; } = "";

        const byte stx = 0x02;
        const byte etx = 0x03;
        const byte nak = 0x15;
        byte[] Nak = new byte[] { 0x15 };
        const byte esc = 0x1B;
        //byte[] ba;
        byte[] rb = new byte[2048];
        int rd;
        private byte[] rx = new byte[2048];              // прием из принтера 
        private int rxSize = 0;

        public int PRINT_DENSITY = 2;
        public int MECHANISM_SPEED = 5;

        byte[] ESC = new byte[] { esc }; ///ESC command (1B)
        byte[] Init = new byte[] { esc, 0x40 }; ///Init printer commands - ESC @ (1B 40)
        byte[] CommandSize = new byte[] { esc, 0x21 }; ///Set size chars - ESC ! (1B 21)
        byte[] CommandDoubleSize = new byte[] { esc, 0x21, 0x30 }; ///Double charsize - ESC ! 0 (1B 21 30)
        byte[] CommandDefaultSize = new byte[] { esc, 0x21, 0x00 }; ///Default style - ESC ! null (1B 21 00)
        byte[] GS = new byte[] { 0x1D };
        byte[] LF = new byte[] { 0x0A }; ///LF command (0A)
        byte[] DC2 = new byte[] { 0x12 }; ///DC2 command (12)
        byte[] DLE = new byte[] { 0x10 }; ///DLE command (10)
        byte[] EOT = new byte[] { 0x04 }; ///EOT command (04)

        byte[] bb0 = { 0 };

        
        public byte[] statusB = new byte[5];
        public string baS = "";
        public string rxS = " ";
        public bool BusyFlag { get; set; } = false;
        public string printS = "";
        public bool flag = false;

        byte left_margin = 20;

        //new ass
        public string ROMVersion = "---";
        public string PrinterModel = "---";
        public string BTAddress = "---";
        public string ModuleVersion = "---";
        public string DeviceName = "---";
        public string PINCode = "---";
        #endregion

        #region Перегрузки
        public byte[] CombineByteArray(byte[] a, byte[] b)
        {
            byte[] c = new byte[a.Length + b.Length];
            System.Buffer.BlockCopy(a, 0, c, 0, a.Length);
            System.Buffer.BlockCopy(b, 0, c, a.Length, b.Length);
            return c;
        }

        public byte[] CombineByteArrayAndString(byte[] a, string bs)
        {
            byte[] b = Encoding.GetEncoding("CP866").GetBytes(bs);
            return CombineByteArray(a, b);
        }

        public byte[] CombineByteArrayAndByte(byte[] a, byte bb)
        {
            byte[] b = new byte[] { bb };
            return CombineByteArray(a, b);
        }
        #endregion

        #region open
        //Инициализация на скорости!
        public void InitPOSPrint(int speed)
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = speed;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 2000;
            _serialPort.ReadTimeout = 2000;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
            _serialPort.Encoding = Encoding.GetEncoding("CP866");
            _serialPort.DataReceived += _serialPort_DataReceived;
        }

        public bool Open(string Port, int speed)
        {
            InitPOSPrint(speed);
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    if (status == "не отвечает") status = "";

                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    Console.WriteLine("Возникло исключение: " + e.ToString());
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool Close()
        {
            try
            {
                _serialPort.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int bytescount = 0;
            status = "работает";
            switch (command)
            {
                case "newcommand":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print("bytescount: " + bytescount.ToString());
                    byte[] i00 = new byte[bytescount];
                    _serialPort.Read(i00, 0, bytescount);
                    //парсим!!!
                    //string HZ = Encoding.UTF8.GetString(i0);
                    //Debug.Print(HZ);
                    break;
                case "deviceinfo":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print("bytescount: " + bytescount.ToString());
                    byte[] i0 = new byte[bytescount];
                    _serialPort.Read(i0, 0, bytescount);
                    string HZ = Encoding.UTF8.GetString(i0);
                    Debug.Print(HZ);
                    break;

                case "ROMVersion":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print("bytescount: " + bytescount.ToString());
                    byte[] i1 = new byte[bytescount];
                    _serialPort.Read(i1, 0, bytescount);
                    ROMVersion = Encoding.UTF8.GetString(i1);
                    Debug.Print(ROMVersion);
                    port_find = true;
                    break;
                case "PrinterModel":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print("bytescount: " + bytescount.ToString());
                    byte[] i2 = new byte[bytescount];
                    _serialPort.Read(i2, 0, bytescount);
                    PrinterModel = Encoding.UTF8.GetString(i2);
                    Debug.Print(PrinterModel);
                    break;

                case "presenter":
                    //Debug.Print(_serialPort.BytesToRead.ToString());
                    bytescount = _serialPort.BytesToRead;
                    //Debug.Print("bytescount: " + bytescount.ToString());
                    byte[] infobytes = new byte[bytescount];
                    _serialPort.Read(infobytes, 0, bytescount);
                    byte pstatus = infobytes[0];
                    Debug.Print("Presenter: " + SingleByteToString(pstatus));
                    if (pstatus == 0x4c)
                    {
                        presenterstatus = "в губах";
                    }
                    else if (pstatus == 0x0c)
                    {
                        presenterstatus = "свободен";
                    }
                    else
                    {
                        presenterstatus = "другой";
                    }

                    break;
                case "hardstatus":
                    bytescount = _serialPort.BytesToRead;
                    //Debug.Print("bytescount: " + bytescount.ToString());
                    byte[] infobytes2 = new byte[bytescount];
                    _serialPort.Read(infobytes2, 0, bytescount);
                    //Debug.Print(ByteArrayToStringX(infobytes2));

                    byte hstatus = infobytes2[0];

                    int cutter_error = hstatus & 0x10;
                    int temp_error = hstatus & 0x08;
                    int no_paper = hstatus & 0x04;
                    int head_open = hstatus & 0x02;
                    int near_end = hstatus & 0x01;
                    /*
                    Debug.Print("cutter_error: " + cutter_error.ToString());
                    Debug.Print("temp_error: " + temp_error.ToString());
                    Debug.Print("no_paper: " + no_paper.ToString());
                    Debug.Print("head_open " + head_open.ToString());
                    Debug.Print("near_end " + near_end.ToString());
                    */
                    if (cutter_error > 0)
                    {
                        hardstatus = "Ошибка ножа";
                    }
                    else if (temp_error > 0)
                    {
                        hardstatus = "Перегрелся";
                    }
                    else if (no_paper > 0)
                    {
                        hardstatus = "Нет бумаги";
                    }
                    else if (head_open > 0)
                    {
                        hardstatus = "Крышка открыта";
                    }
                    else
                    {
                        hardstatus = "Работает";
                    }

                    break;

            }
        }

        public void mDelay()
        {
            Thread.Sleep(500);
        }

        public void mDelay2()
        {
            Thread.Sleep(50);
        }

        /*
         str 45!! 
          
         [Sample Code]
= 0;
// receive data（
e
// First byte（Printer information）
if(res[0] &
Label1->Caption = "Offline";
}
else{
Label1->Cap
}
// Second byte（e n）
if(res[1] & 0 8) {
Label2->Cap
}
L
}
] & 0x40) {
Label2->Caption = "Auto recoverable error";
n = "Non error";
// Third byte（Paper detection information）
) {
}
}
= "No error";
rth byte（Paper detection status of option）
ted within presenter unit";
etected within presenter unit";
n
buf[n++] = 0x1d;
buf[n++] = 0x61;
buf[n++] = 0x0F;
// send command
Ret = WriteFile(hPort,buf,n,&dwSendSize,NULL);
4byte）
Ret = RadFile(hPort,res,4,&dwSendSize,NULL);
// Determination of received data
0x08) {
tion = "Online";
rror informatio
x0
tion = "Auto cutter error";
else if(res[1] & 0x20) {
abel2->Caption = "Voltage error";
else if(res[1
}
else{
Label2->Captio
}
if(res[2] & 0x03
Label3->Caption = "No paper ";
else if(res[2] & 0x0C) {
Label3->Caption = "Near end";
else{
Label3->Caption
}
// Fo
if(res[3] & 0x40) {
Label4->Caption = "Paper detec
}
else{
Label4->Caption = "No paper d
}
        */
        public void NewTCommand()
        {
            command = "newcommand";
            byte[] newbyte = new byte[] { 0x1D,0x61,0x0f };
            SendDirectCommand(newbyte);
            mDelay2();
        }

        public void Print_Density(byte percent)
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "~");
            ba = CombineByteArrayAndByte(ba, percent);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void GetROMVersion()
        {
            command = "ROMVersion";
            byte[] ba = GS;
            ba = CombineByteArrayAndString(ba, "I");
            ba = CombineByteArrayAndByte(ba, 65);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void GetPrinterModel()
        {
            command = "PrinterModel";
            byte[] ba = GS;
            ba = CombineByteArrayAndString(ba, "I");
            ba = CombineByteArrayAndByte(ba, 67);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void SendDirectCommand(byte[] cmd)
        {
            _serialPort.Write(cmd, 0, cmd.Length);
        }

        #region AllCommands

        public void OnlyCut()
        {
            Print_DC2_l(); //протяжка
            Cut();
        }

        public void SetLMargin(byte LMargin)
        {
            byte[] ba = GS;
            ba = CombineByteArrayAndString(ba, "L");
            ba = CombineByteArrayAndByte(ba, LMargin);
            ba = CombineByteArrayAndByte(ba, 0x00);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void SetLabelLength()
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "L");
            ba = CombineByteArrayAndByte(ba, 0x29);// = 41x2mm = 82 0x29
            ba = CombineByteArrayAndByte(ba, 0x02); //n2=2
            ba = CombineByteArrayAndByte(ba, 0x06);
            ba = CombineByteArrayAndByte(ba, 0x06);
            //baS = DC2 + "L" + "\u0064" + "\u0002" + "\u0005" + Encoding.UTF8.GetString(bb0);
            SendDirectCommand(ba);
            mDelay();
        }

        public void SetLabelLengthB()
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "L");
            ba = CombineByteArrayAndByte(ba, 0x36);// = 41x2mm = 82 0x29
            ba = CombineByteArrayAndByte(ba, 0x00); //n2=2
            ba = CombineByteArrayAndByte(ba, 0x00);
            ba = CombineByteArrayAndByte(ba, 0x00);
            //baS = DC2 + "L" + "\u0064" + "\u0002" + "\u0005" + Encoding.UTF8.GetString(bb0);
            SendDirectCommand(ba);
            mDelay();
        }

        public void Print_DC2_L()
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "L");
            ba = CombineByteArrayAndByte(ba, 0x29);// = 41x2mm = 82
            ba = CombineByteArrayAndByte(ba, 0x02);
            ba = CombineByteArrayAndByte(ba, 0x00);
            ba = CombineByteArrayAndByte(ba, 0x00);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void SetActivePresenterMode(int mode)
        {
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "h");//Select the active mode on the presenter
            byte mp = (byte)mode;
            ba = CombineByteArrayAndByte(ba, mp);//n=4 : Execute the release action forcibly
            SendDirectCommand(ba);
            mDelay();
        }

        public void SetPresenterMode(int mode)
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "K");
            ba = CombineByteArrayAndByte(ba, 0x07);//PRESENTER MODE
            byte pm = (byte)mode;
            //ba = AddByteC(ba, 0x03);
            ba = CombineByteArrayAndByte(ba, pm);
            SendDirectCommand(ba);
            mDelay();
        }

        public void SetPresenterMode2(int mode)
        {
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "r3");//PRESENTER MODE
            //byte pm = (byte)numericUpDown3.Value;
            byte pm = (byte)mode;
            ba = CombineByteArrayAndByte(ba, pm);//n=3 : CONTINUOUS
            SendDirectCommand(ba);
            mDelay();
        }

        public void StartObmen()
        {
            SendInit();
            ValidStatus();
        }

        #endregion

        #region StatusFunction
        public void GetStatus()
        {
            command = "presenter";
            byte[] ba = DLE;
            ba = CombineByteArray(ba, EOT);
            ba = CombineByteArrayAndByte(ba, 0x04);
            SendDirectCommand(ba);
        }

        public void GetHardStatus()
        {
            //sleepTime = 200;
            command = "hardstatus";
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "v");
            SendDirectCommand(ba);
        }
        #endregion

        #region memory_switch
        public void MarkDetectionOn() //memory switch byte 1 MD_ON
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "K");
            ba = CombineByteArrayAndByte(ba, 0x01);
            ba = CombineByteArrayAndByte(ba, 0xD2);//MARK DETECTION ON; SELECT SENSOR 1: Transmission; CUT AFTER FEED-SW 2: Full cut; PAPER FEED 1: ON
            SendDirectCommand(ba);
            mDelay();
        }

        public void MarkDetectionOff() //memory switch byte 1 MD_OFF
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "K");
            ba = CombineByteArrayAndByte(ba, 0x01);
            ba = CombineByteArrayAndByte(ba, 0x52);//MARK DETECTION OFF; SELECT SENSOR 1: Transmission; CUT AFTER FEED-SW 2: Full cut; PAPER FEED 1: ON
            SendDirectCommand(ba);
            mDelay();
        }

        public void MarkReDetectionOn() //PC866, NEAR_END, MarkReDetection - On, Speed=200
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "K");
            ba = CombineByteArrayAndByte(ba, 0x02);
            ba = CombineByteArrayAndByte(ba, 0xA8);//MARK RE-DETECTION ON; PRINT DENSITY 100%; CHARACTER TABLE 0: PC437
            SendDirectCommand(ba);
            mDelay();
        }

        public void MarkReDetectionOff() //PC866, NEAR_END, MarkReDetection - On, Speed=200
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "K");
            ba = CombineByteArrayAndByte(ba, 0x02);
            ba = CombineByteArrayAndByte(ba, 0x28);//MARK RE-DETECTION ON; PRINT DENSITY 100%; CHARACTER TABLE 0: PC437
            SendDirectCommand(ba);
            mDelay();
        }

        //byte 3 Mech_Speed=200, Near_End ON, Print Width
        public void SetMechSpeed()
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "K");
            ba = CombineByteArrayAndByte(ba, 0x03);
            ba = CombineByteArrayAndByte(ba, 0x2A);//PRINT WIDTH 2:58/54; MECHANISM_SPEED; SELLECT NEAR-END 0:ON; CHARACTER TABLE 0: PC437
            SendDirectCommand(ba);
            mDelay();
        }

        //byte 4 => ComPort Settings
        public void SetComSettings()
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "K");
            ba = CombineByteArrayAndByte(ba, 0x04);
            ba = CombineByteArrayAndByte(ba, 0x17);
            SendDirectCommand(ba);
            mDelay();
        }

        //byte 5 => Paper Feed x30
        public void SetPaperFeed()
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "K");
            ba = CombineByteArrayAndByte(ba, 0x05);//Paper Feed 30
            ba = CombineByteArrayAndByte(ba, 0x1E);
            SendDirectCommand(ba);
            mDelay();
        }

        //byte 6 => Bezel Mode Off
        public void SetBezelMode()
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "K");
            ba = CombineByteArrayAndByte(ba, 0x06);//BEZEL MODE
            ba = CombineByteArrayAndByte(ba, 0x00);
            SendDirectCommand(ba);
            mDelay();
        }
        #endregion

        public void SendStartParameters(bool mark)
        {
            Debug.Print("Init");
            SendInit(); //"ESC @" Initialize
            Debug.Print("ValidStatus");
            ValidStatus();
            //switch!

            if (mark)
            {
                Debug.Print("Mark Detection On");
                MarkDetectionOn();
                Debug.Print("Mark ReDetection On");
                MarkReDetectionOn();
            }
            else
            {
                Debug.Print("Mark ReDetection Off");
                MarkDetectionOff();

                Debug.Print("Mark ReDetection Off");
                MarkReDetectionOff();
            }
            Debug.Print("SetMechSpeed");
            SetMechSpeed();
            Debug.Print("SetPaperFeed");
            SetPaperFeed();
            Debug.Print("SetBezelMode");
            SetBezelMode();

            //geometry + presenter
            if (mark)
            {
                Debug.Print("SetLabelLength");
                SetLabelLength();
            }
            else
            {
                Debug.Print("SetLabelLengthB");
                SetLabelLengthB();
            }

            Debug.Print("SetActivePresenterMode");
            SetActivePresenterMode(4);

            Debug.Print("SetPresenterMode");
            SetPresenterMode(3);

            Debug.Print("SetPresenterMode2");
            SetPresenterMode2(3);

            Debug.Print("Init2");
            SendInit();

            MessageBox.Show("Готово!");
        }

        public void DetectSpeed(string Port)
        {
            port_find = false;

            List<int> SpeedList = new List<int>();
            SpeedList.Add(1200);
            SpeedList.Add(2400);
            SpeedList.Add(4800);
            SpeedList.Add(9600);
            SpeedList.Add(19200);
            SpeedList.Add(38400);
            SpeedList.Add(57600);
            SpeedList.Add(115200);

            int _currspeed = 0;

            foreach (int currspeed in SpeedList)
            {
                _currspeed = currspeed;

                Debug.Print(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " bod" + currspeed.ToString());

                int waiter = 0;
                bool open = Open(Port, currspeed);
                Debug.Print(open.ToString());
                if (open)
                {
                    Thread.Sleep(100);
                    GetROMVersion();
                    while (!wait)
                    {
                        Thread.Sleep(100);
                        waiter += 1;
                        if (waiter == 10)
                        {
                            wait = true;
                        }
                        Application.DoEvents();
                    }

                    if (port_find)
                    {
                        Thread.Sleep(100);
                        Close();
                        goto babai;
                    }

                    Thread.Sleep(100);
                    Close();
                    Thread.Sleep(200);
                }
            }
            //никакого порта не нашлось! ребутни по питанию! (Slave 3???)
            MessageBox.Show("Скорость не найдена! Перезагрузите принтер по питанию и повторите попытку.");
            return;
            babai:
            if (_currspeed == 115200)
            {
                MessageBox.Show("Скорость" + _currspeed + " бод." + "Переустановка скорости не требуется.");
            }
            else
            {
                if (MessageBox.Show("Скорость" + _currspeed + " бод." + "Для переустановки скорости нажмите ОК") == DialogResult.OK)
                {
                    Thread.Sleep(100);
                    bool open2 = Open(Port, _currspeed);
                    Debug.Print(open2.ToString());
                    if (open2)
                    {
                        SetComSettings();
                    }
                    else
                    {

                    }
                    Thread.Sleep(100);
                    Close();
                    MessageBox.Show("Скорость переустановлена. Напечатайте чек с настройками!");
                }
            }
        }

        //инициализация
        public void SendInit()
        {
            SendDirectCommand(Init);
            mDelay();
        }

        public void ValidStatus()
        {
            byte[] ba = GS;
            ba = CombineByteArray(ba, DLE); //Valid / Invalid of real-time status transmission
            ba = CombineByteArrayAndByte(ba, 0x01);//Valid
            SendDirectCommand(ba);
            mDelay();
        }

        #region PrintingFunc
        public void Print_LF()
        {
            byte[] ba = LF;
            SendDirectCommand(ba);
            mDelay2();
        }

        public void Print_DC2_l()
        {
            byte[] ba = DC2;
            ba = CombineByteArrayAndString(ba, "l");
            SendDirectCommand(ba);
            mDelay2();
        }

        public void Cut()
        {
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "i");
            SendDirectCommand(ba);
            mDelay2();
        }

        public void PrintString(byte lm, string s)
        {
            byte[] ba = new byte[0];
            ba = CombineByteArrayAndString(ba, s);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void SetCenter()
        {
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "a");
            ba = CombineByteArrayAndByte(ba, 0x01);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void SetInverse()
        {
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "{");
            byte inv = 0b00000001;
            ba = CombineByteArrayAndByte(ba, inv);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void SetPosition(byte l, byte h)
        {
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "$");
            ba = CombineByteArrayAndByte(ba, l);
            ba = CombineByteArrayAndByte(ba, h);
            SendDirectCommand(ba);
            mDelay2();
        }

        //проработать/доработать
        public void SetCharSize(byte inc)
        {
            byte[] ba = GS;
            ba = CombineByteArrayAndString(ba, "!");

            bool[] bools = new bool[8];

            switch (inc)
            {
                case 1:
                    bools = new bool[] { false, false, false, true, false, false, false, true };
                    break;
            }

            BitArray a = new BitArray(bools);
            byte[] bytes = new byte[a.Length / 8];
            a.CopyTo(bytes, 0);
            ba = CombineByteArray(ba, bytes);
            SendDirectCommand(ba);
            mDelay2();
        }

        #endregion

        #region StartPrint

        public void Retract()
        {
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "h");
            ba = CombineByteArrayAndByte(ba, 5);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void Release()
        {
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "h");
            ba = CombineByteArrayAndByte(ba, 4);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void FontMF()
        {
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "!");
            ba = CombineByteArrayAndByte(ba, 0b00001111);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void FontMF2()
        {
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "!");
            ba = CombineByteArrayAndByte(ba, 0b01111110);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void FontDeMF()
        {
            byte[] ba = ESC;
            ba = CombineByteArrayAndString(ba, "!");
            ba = CombineByteArrayAndByte(ba, 0b00000000);
            SendDirectCommand(ba);
            mDelay2();
        }

        public void TicketTest(bool roolon)
        {
            SetCenter();
            SetInverse();

            if (roolon)
                FontDeMF();

            PrintString(0x30, "Спасибо за посещение!");
            Print_LF();

            PrintString(0x30, "три часа. С 4 часа 50 руб./ч");
            Print_LF();

            PrintString(0x30, "Тарифный план 100 руб./ч первые");
            Print_LF();

            //Print_LF();

            if (roolon)
            {
                FontMF();
                PrintString(0x40, "...................................");
                FontDeMF();
            }

            Print_LF();
            PrintString(0x30, "Ельцин-Центр Зона А");
            Print_LF();

            if (roolon)
            {
                FontMF();
                PrintString(0x50, "УСТРОЙСТВО ВЪЕЗДА");
                FontDeMF();
            }

            Print_LF();
            Print_LF();

            PrintString(0x30, "X773CB74");
            Print_LF();

            if (roolon)
            {
                FontMF();
                PrintString(0x63, "НОМЕР ТС");
                FontDeMF();
            }

            Print_LF();
            Print_LF();

            PrintString(0x30, DateTime.Now.ToString());
            Print_LF();

            if (roolon)
            {
                FontMF();
                PrintString(0x50, "ВРЕМЯ ВЪЕЗДА");
                FontDeMF();
            }

            Print_LF();
            Print_LF();

            if (roolon)
            {
                FontMF();
                PrintString(0x40, "...................................");
                FontDeMF();
            }

            Print_LF();

            //Задание цифр для штрихкода
            byte[] nosymbols = new byte[] { 0x1d, 0x48, 0x02 }; //пока печатаем для тестов, вообще нужно включить в 0
            SendDirectCommand(nosymbols);
            mDelay2();

            //высота
            byte h = 0x70;
            byte[] setheight = new byte[] { 0x1d, 0x68, h }; //15-32,5%
            SendDirectCommand(setheight);
            mDelay2();

            //Ширина % для Code128 =1
            byte w = 0x01;
            byte[] setwidth = new byte[] { 0x1d, 0x77, w };
            SendDirectCommand(setwidth);
            mDelay2();

            //печать
            byte[] printdata = new byte[] { 0x1d, 0x6b, 0x07 }; //здесь

            string data = "1234567890123456789"; //19-digits...

            printdata = CombineByteArrayAndString(printdata, "i"); //start 2

            printdata = CombineByteArrayAndString(printdata, data);

            printdata = CombineByteArray(printdata, new byte[] { 0x00, 0x0a });

            Debug.Print(ByteArrayToString(printdata));

            SendDirectCommand(printdata);

            if (roolon)
            {
                //FontMF2();
                //PrintString(0x70, "^");
                //FontDeMF();
                Print_LF();
                Print_LF();
                Print_LF();
                Print_LF();
            }

            Print_DC2_l(); //протяжка
            Cut();
            _serialPort.DiscardInBuffer();
            _serialPort.DiscardOutBuffer();
        }

        public void PrintTicket(string TPLine1, string TPLine2, string Zone, string Plate, string DT, string BC, bool roolon)
        {
            SetCenter();
            SetInverse();

            if (roolon)
                FontDeMF();

            PrintString(0x30, "Спасибо за посещение!");
            Print_LF();

            PrintString(0x30, TPLine2);
            Print_LF();

            PrintString(0x30, TPLine1);
            Print_LF();

            //Print_LF();

            if (roolon)
            {
                FontMF();
                PrintString(0x40, "...................................");
                FontDeMF();
            }

            Print_LF();
            PrintString(0x30, Zone);
            Print_LF();

            if (roolon)
            {
                FontMF();
                PrintString(0x50, "УСТРОЙСТВО ВЪЕЗДА");
                FontDeMF();
            }

            Print_LF();
            Print_LF();

            PrintString(0x30, Plate);
            Print_LF();

            if (roolon)
            {
                FontMF();
                PrintString(0x63, "НОМЕР ТС");
                FontDeMF();
            }

            Print_LF();
            Print_LF();

            PrintString(0x30, DT);
            Print_LF();

            if (roolon)
            {
                FontMF();
                PrintString(0x50, "ВРЕМЯ ВЪЕЗДА");
                FontDeMF();
            }

            Print_LF();
            Print_LF();

            if (roolon)
            {
                FontMF();
                PrintString(0x40, "...................................");
                FontDeMF();
            }

            Print_LF();

            //Задание цифр для штрихкода
            byte[] nosymbols = new byte[] { 0x1d, 0x48, 0x02 }; //пока печатаем для тестов, вообще нужно включить в 0
            SendDirectCommand(nosymbols);
            mDelay2();

            //высота
            byte h = 0x70;
            byte[] setheight = new byte[] { 0x1d, 0x68, h }; //15-32,5%
            SendDirectCommand(setheight);
            mDelay2();

            //Ширина % для Code128 =1
            byte w = 0x01;
            byte[] setwidth = new byte[] { 0x1d, 0x77, w };
            SendDirectCommand(setwidth);
            mDelay2();

            //печать
            byte[] printdata = new byte[] { 0x1d, 0x6b, 0x07 }; //здесь

            string data = BC; //19-digits...

            printdata = CombineByteArrayAndString(printdata, "i"); //start 2

            printdata = CombineByteArrayAndString(printdata, data);

            printdata = CombineByteArray(printdata, new byte[] { 0x00, 0x0a });

            SendDirectCommand(printdata);

            if (roolon)
            {
                //FontMF2();
                //PrintString(0x70, "^");
                FontDeMF();
                Print_LF();
                Print_LF();
                Print_LF();
                Print_LF();
            }

            Print_DC2_l(); //протяжка
            Cut();
            _serialPort.DiscardInBuffer();
            _serialPort.DiscardOutBuffer();
        }

        #endregion

        public SaneiSK1()
        {
            //InitPOSPrint();
        }

        #region Конвертеры
        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba); //.Replace("-", "");
        }

        public static string ByteArrayEncode(byte[] data)
        {
            char[] characters = data.Select(b => (char)b).ToArray();
            return new string(characters);
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string SingleByteToString(byte ba)
        {
            byte[] b = new byte[1];
            b[0] = ba;
            return BitConverter.ToString(b);
        }

        public static string ByteArrayToStringX(byte[] ba)
        {
            return BitConverter.ToString(ba);
        }
        #endregion
    }
}
