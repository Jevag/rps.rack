﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class WiegandAdapter
    {

        public int WiegandType { get; set; } //0 - 1 //1 - 2


        public bool IsOpen
        {
            get
            {
                return _serialPort.IsOpen;
            }
        }

        public string Result
        {
            get { return RxData; }
        }

        public string Status
        {
            get { return status; }
        }

        SerialPort _serialPort;
        private string RxData;
        private string status;

        public WiegandAdapter()
        {
            // Create a new SerialPort object with default settings.
            _serialPort = new SerialPort();

            // Allow the user to set the appropriate properties.
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.None;
            _serialPort.DataBits = 8;
            _serialPort.StopBits = StopBits.One;
            _serialPort.Handshake = Handshake.None;
            _serialPort.DataReceived += _serialPort_DataReceived;
            RxData = "";
            // Set the read/write timeouts
         //   _serialPort.ReadTimeout = 100;
         //   _serialPort.WriteTimeout = 100;
        }

        
        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int bytescount = _serialPort.BytesToRead;
            if (bytescount == 12)
            {
                byte[] r_bytes = new byte[12];
                _serialPort.Read(r_bytes, 0, 12);
                string str = System.Text.Encoding.Default.GetString(r_bytes);
                Debug.Print(str);

                RxData = str.Substring(2, 8);

                if (WiegandType == 1)
                {
                    RxData = RxData.TrimStart('0');
                }

                status = RxData;
                Debug.Print(status);

            }

        }
        

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    status = "ожидание";
                    return true;
                }
                catch (Exception e)
                {
                   // _CardDispenserStatus.CommunicationsErrorStatus = "Возникло исключение: " + e.ToString();
                    status = "не отвечает";
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    return true;
                }
                catch (Exception e)
                {
                   // _CardDispenserStatus.CommunicationsErrorStatus = "Возникло исключение: " + e.ToString();
                    return false;
                }
            }
            else return false;
        }

        public bool Wait1()                        // ожидание ответа
        {
            //Debug.Print("Wait1Wiegand");
       //     try
       //     {
                if (_serialPort.BytesToRead > 0)
                {
                    string rxdata = _serialPort.ReadLine();
                    RxData = rxdata.Substring(1, 10);
                   // Debug.Print("RxData=" +RxData);
                    status = RxData;
                     if ( WiegandType == 1)
                    {
                     status = status.Trim('0');
                        }

                }
                else
                {
                    status = "ожидание";
                }
                return true;
       //     }
       //     catch (Exception ex)
       //     {
       //         status = "не отвечает";
       //         return false;
      //      }

        }

    }
}
