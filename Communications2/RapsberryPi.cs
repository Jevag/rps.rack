﻿using ExternalDisplay;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Communications
{
    public class RapsberryPi
    {
        bool babaika=false;

        SerialPort _serialPort;

        public bool isOpened { get; set; }

        public string clear_json;

        //таймер на неответ.
        System.Windows.Forms.Timer tmrRX;

        System.Windows.Forms.Timer tmrCounter;

        public int counter = 0;

        public string Status { get; set; }

        public bool Sended = false;

        public RapsberryPi()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 115200;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 500;
            _serialPort.ReadTimeout = 500;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
            _serialPort.DataReceived += _serialPort_DataReceived;

            tmrRX = new System.Windows.Forms.Timer();
            tmrRX.Interval = 5000; //250...
            tmrRX.Tick += TmrRX_Tick;
            tmrRX.Enabled = false;

            //таймер для пинга?
            tmrCounter = new System.Windows.Forms.Timer();
            tmrCounter.Interval = 1000; //250...
            tmrCounter.Tick += TmrCounter_Tick;
            tmrCounter.Enabled = false;


            clear_json = GenerateEmptyJson();
        }

        private void TmrCounter_Tick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            if (babaika)
            {
                counter = 0;
                babaika = false;
            }

            counter += 1;
            Debug.Print(counter.ToString());
            if (counter == 30)
            {
                //ping
                SendPing();
                counter = 0;
            }
        }

        private void TmrRX_Tick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        public void SendPing()
        {
            string ping_json = "{ \"ping \": 1 }";
            Debug.Print("isOpened " + isOpened.ToString());
            if (isOpened)
            {
                WakeDataFrame wdf1 = ExternalDisplay.Wake.encode(0x17, 0x12, ping_json);
                _serialPort.Write(wdf1.FRAME, 0, wdf1.FRAME.Length);
                tmrRX.Enabled = true;
            }
        }

        //сколько времени прошло между командами.
        /*
        private void TmrCounter_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();

            if (babaika)
            {
                counter = 0;
                babaika = false;
            }

            counter += 1;
            Debug.Print(counter.ToString());
            if (counter == 30)
            {
                //ping
                SendPing();
                counter = 0;
            }
        }

        private void TmrRX_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            Status = "не отвечает";
            SendPing();
        }
        */

        public static string ByteArrayToStringX(byte[] ba)
        {
            return BitConverter.ToString(ba);
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int bytescount = _serialPort.BytesToRead;
            byte[] infobytes = new byte[bytescount];
            _serialPort.Read(infobytes, 0, bytescount);
            //Debug.Print("Econom:");
            //Debug.Print(ByteArrayToStringX(infobytes));

            //throw new NotImplementedException();
            Status = "работает";
            babaika = true;
            tmrRX.Enabled = false;
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();

                    isOpened = true;
                    return true;
                }
                catch (Exception e)
                {
                    //Status = "не отвечает";

                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    isOpened = false;
                    return true;
                }
                catch (Exception e)
                {
                    //Status = "не отвечает";
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public static string getJsonString(object val)
        {
            JsonSerializerSettings jss = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore,
                Formatting = Formatting.None
            };
            string jsonString = JsonConvert.SerializeObject(val, jss);
            return jsonString;
        }

        public void GenerateWorkJson(List<string> arg_lst, bool imgUse, string imgPath, string a_color)
        {

            RGBDisplayScreen screen = new RGBDisplayScreen();

            int y = 116; //+32
            List<Text> txts = new List<Text>();

            foreach (string str in arg_lst)
            {
                txts.Add(new Text() { align = "c", color = a_color, font = "RPSPixel.ttf", size = 16, text = str, xpos = 1, ypos = y });
                y -= 10;
            }

            screen.texts = txts.ToArray();

            if (imgUse)
            {
                Image img = new Image();
                img.image = ExternalDisplayUtils.convertImageToBase64(imgPath);
                if (imgPath.IndexOf("100.png") > -1)
                {
                    y -= 32;
                    img.xpos = 11;
                    img.ypos = y;
                }
                else
                {
                    y -= 20;
                    img.xpos = 16;
                    img.ypos = y;
                }
                List<Image> imgList = new List<Image>();
                imgList.Add(img);

                screen.images = imgList.ToArray();
            }


            string scr = getJsonString(screen);

            if (isOpened)
            {
                WakeDataFrame wdf1 = ExternalDisplay.Wake.encode(0x17, 0x12, clear_json);
                _serialPort.Write(wdf1.FRAME, 0, wdf1.FRAME.Length);

                Thread.Sleep(200);

                WakeDataFrame wdf = ExternalDisplay.Wake.encode(0x17, 0x12, scr);
                _serialPort.Write(wdf.FRAME, 0, wdf.FRAME.Length);
            }
        }

        public string GenerateEmptyJson()
        {
            RGBDisplayScreen clear = new RGBDisplayScreen()
            {
                background = new Background()
                {
                    xpos = 0,
                    ypos = 0,
                    image = Background.IMAGE_NONE
                }
            };
            string clear_json = clear.getJsonString();
            return clear_json;
        }

        public class Background : JsonObject
        {
            public const string IMAGE_NONE = "none";
            public const string IMAGE_LAST = "last";
            public const string IMAGE_CLEAR = "clear";

            public int xpos { get; set; }
            public int ypos { get; set; }
            public string image { get; set; }
        }

        public class Image : JsonObject
        {
            public int xpos { get; set; }
            public int ypos { get; set; }
            public string image { get; set; }
        }
        public class Text : JsonObject
        {
            public const string ALIGN_LEFT = "l";
            public const string ALIGN_CENTER = "c";
            public const string ALIGN_RIGHT = "r";

            public int xpos { get; set; }
            public int ypos { get; set; }
            public string font { get; set; }
            public int size { get; set; }
            public string align { get; set; }
            public string color { get; set; }
            public string text { get; set; }

        }
        public class RGBDisplayScreen : JsonObject
        {
            public Background background { get; set; }
            public Image[] images { get; set; }
            public Text[] texts { get; set; }
        }

    }
}
