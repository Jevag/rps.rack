﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Communications
{
    public class MT163
    {
        SerialPort COM;
        //System.Windows.Forms.Timer StateTimer;
        System.Windows.Forms.Timer Waiting;

        //CommandConstants
        private const string getversion = "\x02\x00\x02\x30\x30\x03";
        private const string eject = "\x02\x00\x02\x31\x30\x03";
        private const string retain = "\x02\x00\x02\x31\x33\x03";
        private const string front_insert = "\x02\x00\x02\x31\x31\x03";
        private const string back_insert = "\x02\x00\x02\x31\x32\x03";
        private const string search_card = "\x02\x00\x02\x32\x30\x03";
        private const string timeout_recovery_off = "\x02\x00\x03\x32\x31\x00\x03";
        private const string timeout_recovery_on = "\x02\x00\x03\x32\x31\x01\x03";

        private byte command_index = 0;

        //Захваты спереди сзади удались или нет
        bool FrontSuccess = false;
        bool BackSuccess = false;

        //public fields
        public string Version { get; set; }
        public string CardStatus { get; set; }

        public string Status { get; set; }


        public bool IsError { get; set; }
        public string ErrorStatus { get; set; }
        public bool IsAlarmError { get; set; }

        public struct DeviceBitStatus
        {
            //b0
            public bool CardAtReadOperationPosition;
            //b1
            public bool DeviceError;
            //b2
            public bool ReturningCard;
            //b3
            public bool AcceptingCard;
            //b4
            public bool Q1HaveCard;
            //b5
            public bool Q2HaveCard;
            //b6
            public bool Q3HaveCard;
        }

        public DeviceBitStatus CurrentDeviceState;

        //public methods
        public bool Open(string Port)
        {
            if (!COM.IsOpen)
            {
                try
                {
                    COM.PortName = Port;
                    COM.Open();
                    return true;
                }
                catch (Exception e)
                {
                    // _CardDispenserStatus.CommunicationsErrorStatus = "Возникло исключение: " + e.ToString();
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        public bool Close()
        {
            if (COM.IsOpen)
            {
                try
                {
                    COM.Close();
                    return true;
                }
                catch (Exception e)
                {
                    // _CardDispenserStatus.CommunicationsErrorStatus = "Возникло исключение: " + e.ToString();
                    return false;
                }
            }
            else return false;
        }

        public bool IsOpen
        {
            get
            {
                return COM.IsOpen;
            }
        }

        /*
        public bool BezelCardStatus
        {
            get
            {
                return CurrentDeviceState.Q1HaveCard;
            }
        }
        */

        public bool HardwareError
        {
            get
            {
                return CurrentDeviceState.DeviceError;
            }
        }

        public bool WaitCard;

        public void Eject()
        {
            GetStatus();
            if (CurrentDeviceState.Q2HaveCard && CurrentDeviceState.Q3HaveCard) //Карта дома
            {
                EjectCard();
            }
        }

        public void Retain()
        {
            GetStatus();
            if (CurrentDeviceState.Q1HaveCard)
            {
                RetainCard();
            }


            if (CurrentDeviceState.Q2HaveCard && CurrentDeviceState.Q3HaveCard) //Карта дома
            {
                RetainCard();
            }
        }

        public void Front()
        {
            GetStatus();
            if (CurrentDeviceState.Q1HaveCard)
                {
                    FrontInsert();
                }
        }

        public void GetVersion()
        {
            if (COM.IsOpen)
            {
                Waiting.Start();
                string Req = AddBCC(getversion);
                command_index = 0;
                COM.Write(Req);
            }
        }

        void EjectCard()
        {
            if (COM.IsOpen)
            {
                Waiting.Start();
                string Req = AddBCC(eject);
                command_index = 1;
                COM.Write(Req);
            }
        }

        public void RetainCard()
        {
            if (COM.IsOpen)
            {
                Waiting.Start();
                string Req = AddBCC(retain);
                command_index = 2;
                COM.Write(Req);
            }
        }

        void FrontInsert()
        {
            if (COM.IsOpen)
            {
                Waiting.Start();
                string Req = AddBCC(front_insert);
                command_index = 3;
                COM.Write(Req);
            }
        }

        public void BackInsert()
        {
            if (COM.IsOpen)
            {
                Waiting.Start();
                string Req = AddBCC(back_insert);
                command_index = 4;
                COM.Write(Req);
            }
        }

        public void GetStatus()
        {
            if (COM.IsOpen)
            {
                Waiting.Start();
                string Req = AddBCC(search_card);
                command_index = 5;
                COM.Write(Req);
            }
        }

        public void TimeoutRecoveryOff()
        {
            string Req = AddBCC(timeout_recovery_off);
            command_index = 6;
            COM.Write(Req);
        }

        public void TimeoutRecoveryOn()
        {
            string Req = AddBCC(timeout_recovery_on);
            command_index = 6;
            COM.Write(Req);
        }

        //Service Operations
        private string AddBCC(string Req)
        {
            byte[] bstr = Encoding.ASCII.GetBytes(Req);
            int BCC = 0;
            foreach (byte b in bstr)
            {
                BCC = BCC ^ b;
            }
            Req = String.Concat(Req, ((char)BCC).ToString());
            return Req;
        }

        //Constructor
        public MT163()
        {
            // Create a new SerialPort object with default settings.
            COM = new SerialPort();

            // Allow the user to set the appropriate properties.
            COM.BaudRate = 38400;
            COM.Parity = Parity.None;
            COM.DataBits = 8;
            COM.StopBits = StopBits.One;
            COM.Handshake = Handshake.None;
            COM.DataReceived += COM_DataReceived;

            //StateTimer = new System.Windows.Forms.Timer();
            //StateTimer.Interval = 20;
            //StateTimer.Enabled = true;
            //StateTimer.Tick += StateTimer_Tick;
            Waiting = new System.Windows.Forms.Timer();
            Waiting.Interval = 5000;
            Waiting.Tick += Waiting_Tick;
        }

        private void Waiting_Tick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            Status = "не отвечает";
        }

        /*
        private void StateTimer_Tick(object sender, EventArgs e)
        {
            SearchCard();
            //throw new NotImplementedException();
            if (WaitCard)
            {
                if (CurrentDeviceState.Q1HaveCard)
                {
                    StateTimer.Stop();
                    FrontInsert();
                    Thread.Sleep(500);
                    if (FrontSuccess)
                    {
                        StopCardWait();
                    }
                }
            }
        }
        */

        private void COM_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                Waiting.Stop();
                //throw new NotImplementedException();
                int count = COM.BytesToRead;
                byte[] r_bytes = new byte[count];
                COM.Read(r_bytes, 0, count);
                switch (command_index)
                {
                    case 0: //Получаем версию прошивки
                        if (r_bytes[5] == 0x59)
                        {

                            string str2 = "";
                            for (int i = 6; i < 18; i++)
                            {
                                str2 += (char)r_bytes[i];
                            }
                            Version = str2;
                            IsError = false;
                            IsAlarmError = false;
                        }
                        else
                        {
                            IsError = true;
                            ErrorStatus = "Не удалось получить версию прошивки";
                        }

                        break;
                    case 1: //Выдать карту
                        if (r_bytes[5] == 0x59)
                        {
                            IsError = false;
                            IsAlarmError = false;
                        }
                        else
                        {
                            //IsError = true;
                            ErrorStatus = "Не удалось выдать карту в губы. Нет карты в коллекторе.";
                        }

                        break;
                    case 2: //Пробросить карту
                        if (r_bytes[5] == 0x59)
                        {
                            IsError = false;
                            IsAlarmError = false;
                        }
                        else
                        {
                            //IsError = true;
                            ErrorStatus = "Не удалось пробросить карту. Нет карты в коллекторе.";
                        }

                        break;
                    case 3: //Возьми меня спереди
                        if (r_bytes[5] == 0x59)
                        {
                            //Збс взял
                            IsError = false;
                            IsAlarmError = false;
                            FrontSuccess = true;
                        }
                        else
                        {
                            //IsError = true;
                            FrontSuccess = false;
                            ErrorStatus = "Безуспешная попытка фронтального захвата карты";
                        }

                        break;
                    case 4: //Возьми меня сзади
                        if (r_bytes[5] == 0x59)
                        {
                            //Збс взял
                            IsError = false;
                            IsAlarmError = false;
                            BackSuccess = true;
                        }
                        else
                        {
                            //IsError = true;
                            BackSuccess = false;
                            ErrorStatus = "Безуспешная попытка захвата карты сзади";
                        }
                        break;
                    case 5: //Запрашиваем статус карты
                        BitArray ba = new BitArray(new byte[] { r_bytes[5] });
                        CurrentDeviceState.CardAtReadOperationPosition = ba.Get(0);
                        CurrentDeviceState.DeviceError = ba.Get(1);
                        CurrentDeviceState.ReturningCard = ba.Get(2); //Видимо идет операция возврата карты
                        CurrentDeviceState.AcceptingCard = ba.Get(3); //Видимо идет операция отдачи карты
                        CurrentDeviceState.Q1HaveCard = ba.Get(4); //Карта есть на внешнем датчике (фронтальном)
                        CurrentDeviceState.Q2HaveCard = ba.Get(5); //Внутренние датчики полюбому
                        CurrentDeviceState.Q3HaveCard = ba.Get(6);
                        if (CurrentDeviceState.DeviceError)
                        {
                            IsError = true;
                            ErrorStatus = "Аппаратная ошибка коллектора.";
                            IsAlarmError = true; //По этой теме выдаем аларм
                            Status = "карта застряла";
                        }
                        else
                        {
                            IsError = false;
                            IsAlarmError = false;
                            if (CurrentDeviceState.Q1HaveCard)
                            {
                                Status = "карта в губах";
                            }
                            if (CurrentDeviceState.Q2HaveCard && CurrentDeviceState.Q3HaveCard)
                            {
                                Status = "карта под ридером";
                            }
                            if (!CurrentDeviceState.Q1HaveCard && !CurrentDeviceState.Q2HaveCard && !CurrentDeviceState.Q3HaveCard)
                            {
                                Status = "нет карт";
                            }
                        }

                        break;
                    case 6:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex) { }
            }
            
    }
}
