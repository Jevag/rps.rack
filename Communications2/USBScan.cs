﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class USBScan
    {
        string command = "";
        SerialPort _serialPort;
        public string Status { get; private set; }             // статус сканера Barcode
        public bool LoopA { get; set; }

        public string barcode { get; set; }

        public System.Timers.Timer tmrScanImpulse;
        public System.Timers.Timer tmrRX;
        public System.Timers.Timer tmrWD;

        int bytescountrx = 0;
        public string port { get; set; }

        public bool PortExist = false;

        public USBScan()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 9600;// 115200;
            _serialPort.Parity = Parity.None;
            //_serialPort.WriteTimeout = 500;
            //_serialPort.ReadTimeout = 500;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
            _serialPort.DataReceived += _serialPort_DataReceived;

            tmrScanImpulse = new System.Timers.Timer();
            tmrScanImpulse.Interval = 3000; //5200...
            tmrScanImpulse.Elapsed += TmrScanImpulse_Elapsed;
            tmrScanImpulse.Enabled = false;
            tmrScanImpulse.AutoReset = true;

            tmrRX = new System.Timers.Timer();
            tmrRX.Interval = 300; //250...
            tmrRX.Elapsed += TmrRX_Elapsed;
            tmrRX.Enabled = false;
            tmrRX.AutoReset = true;

            tmrWD = new System.Timers.Timer();
            tmrWD.Interval = 5000; //5200...
            tmrWD.Elapsed += TmrWD_Elapsed;
            tmrWD.Enabled = true;
            tmrWD.AutoReset = true;

            command = "none";
        }

        private void TmrWD_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            PortExist= SerialPort.GetPortNames().Any(x => x == port);
            if (!PortExist)
            {
                Status = "не отвечает";
            }
            else
            {
                Status = "работает";
                try
                {
                    if (!_serialPort.IsOpen)
                    {
                        Status = "не отвечает";
                        Open(port);
                    }
                }
                catch (Exception ex) { }
            }            
        }

        private void TmrRX_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //throw new NotImplementedException();
                bytescountrx = _serialPort.BytesToRead;
                Debug.Print(bytescountrx.ToString());
                if (bytescountrx > 0 && bytescountrx < 22)
                {
                    Debug.Print(bytescountrx.ToString());

                    byte[] readregbytes = new byte[bytescountrx];
                    _serialPort.Read(readregbytes, 0, bytescountrx);
                    Debug.Print(ByteArrayToString(readregbytes));

                }
                else if (bytescountrx == 22)
                {
                    Debug.Print("good");

                    //byte[] barcodebytes = new byte[bytescountrx];
                    string bc = _serialPort.ReadExisting();
                    bytescountrx = 0;
                    //Debug.Print(bc);
                    barcode = bc.Substring(0, 19);
                    Debug.Print(barcode);
                    //вкл мануал режим...
                    LoopA = false;
                    //ManualMode(); //уберем???
                    tmrScanImpulse.Enabled = false;
                    tmrRX.Enabled = false;
                }
                if (bytescountrx > 0)
                {
                    bytescountrx = 0;
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                }
            }
            catch (Exception ex) { }
        }

        private void TmrScanImpulse_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            try
            {
                if (LoopA)
                {
                    if (LoopA)
                    {
                        //TriggerMode();
                        tmrRX.Enabled = true;
                        StartScan();
                    }
                    else
                    {
                        ManualMode();
                        tmrRX.Enabled = false;
                    }
                }
            }
            catch (Exception ex) { }
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();

                    return true;
                }
                catch (Exception e)
                {
                    Status = "не отвечает";

                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    return true;
                }
                catch (Exception e)
                {
                    Status = "не отвечает";
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public void StartScan()
        {
            try
            {
                command = "startscan";
                byte[] cmd = new byte[8];
                cmd[0] = 0x16; //prefix
                cmd[1] = 0x42; //prefix
                cmd[2] = 0x65; //types
                cmd[3] = 0x52; //length
                cmd[4] = 0x65; //address
                cmd[5] = 0x51; //address
                cmd[6] = 0x62; //command
                cmd[7] = 0x2e;
                _serialPort.Write(cmd, 0, 8);
            }
            catch (Exception ex) { }
        }

        public void ManualMode()
        {
            try
            {
                command = "manualmode";
                byte[] cmd = new byte[8];
                cmd[0] = 0x16; //prefix
                cmd[1] = 0x56; //prefix
                cmd[2] = 0x62; //types
                cmd[3] = 0x42; //length
                cmd[4] = 0x65; //address
                cmd[5] = 0x4a; //address
                cmd[6] = 0x62; //command
                cmd[7] = 0x2e;
                _serialPort.Write(cmd, 0, 8);
            }
            catch (Exception ex) { }
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        #region statics
        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba); //.Replace("-", "");
        }

        public static string ByteArrayEncode(byte[] data)
        {
            char[] characters = data.Select(b => (char)b).ToArray();
            return new string(characters);
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string SingleByteToString(byte ba)
        {
            byte[] b = new byte[1];
            b[0] = ba;

            return BitConverter.ToString(b);
        }

        #endregion

    }
}
