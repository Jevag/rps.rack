
using System.Collections.Generic;

namespace communication
{
    public enum WakeStatus
    {
        INIT,
        RECEIVING,
        READY
    };

    public class WakeSlave
    {
        public const byte COMMAND_NONE = 0x00;
        public const int FRAME_SIZE = 128;
        private const byte WAKE_CODE_FEND   = 0xC0;
        private const byte WAKE_CODE_FESC   = 0xDB;
        private const byte WAKE_CODE_TFEND  = 0xDC;
        private const byte WAKE_CODE_TFESC  = 0xDD;
        private const byte CRC_INIT         = 0x00;
        private enum FsmRxState
        {
            WAIT_FEND,     /// �������� ������ FEND
            WAIT_ADDR,     /// �������� ������ ������
            WAIT_CMD,      /// �������� ������ �������
            WAIT_NBT,      /// �������� ������ ���������� ���� � ������
            WAIT_DATA,     /// ����� ������
            WAIT_CRC,      /// �������� ��������� ������ CRC
            WAIT_CARR 	   /// �������� �������
        };
        
        private byte m_iAddress;
        private FsmRxState Rx_FSM = FsmRxState.WAIT_FEND;
        private byte Rx_Pre;
        private byte Rx_Cmd;
        private byte Rx_Nbt;
        private byte Rx_Crc;
        private int Rx_Ptr = 0;

        public byte[] ReceivedBuffer { get; set; }
        public int ReceivedSize { set; get; }

        public byte Command { get; set; }
        public int ValidReceiveCounter { get; set; }

        public WakeSlave(byte address, byte FrameSize = 128)
        {
            m_iAddress = address;
            ReceivedBuffer = new byte[FRAME_SIZE];
            Command = COMMAND_NONE;
            ValidReceiveCounter = 0;
        }

        public WakeStatus ProcessInByte(byte data_byte)
        {
            Command = COMMAND_NONE;
            ReceivedSize = 0;
            WakeStatus ret = WakeStatus.INIT;            
            if (data_byte == WAKE_CODE_FEND) {
                Rx_Pre = data_byte;
                Rx_Crc = CRC_INIT;
                Rx_FSM = FsmRxState.WAIT_ADDR;
                Rx_Crc = Do_Crc8(data_byte, Rx_Crc);
                return WakeStatus.INIT;
            }
            if (Rx_FSM == FsmRxState.WAIT_FEND)
                return WakeStatus.INIT;

            byte Pre = Rx_Pre;
            Rx_Pre = data_byte;
            if (Pre == WAKE_CODE_FESC) {
                if (data_byte == WAKE_CODE_TFESC)
                    data_byte = WAKE_CODE_FESC;
                else if (data_byte == WAKE_CODE_TFEND)
                    data_byte = WAKE_CODE_FEND;
                else {
                    Rx_FSM = FsmRxState.WAIT_FEND;
                    return WakeStatus.INIT;
                }
            } else {
                if (data_byte == WAKE_CODE_FESC)
                    return WakeStatus.INIT;
            }

            switch (Rx_FSM) {
                case FsmRxState.WAIT_ADDR:
                    {
                        if ((data_byte & 0x80) != 0) {
                            // ������� ��� ������ 1, ��������� ��� ������
                            data_byte = (byte)(data_byte & 0x7F);
                            Rx_Crc = Do_Crc8(data_byte, Rx_Crc);
                            Rx_FSM = FsmRxState.WAIT_CMD;
                            break;
                        }
                        Rx_FSM = FsmRxState.WAIT_FEND;
                        break;
                    }
                case FsmRxState.WAIT_CMD:
                    {
                        if ((data_byte & 0x80) != 0)
                        {
                            // ������� ��� 1, ��� �� �������
                            Rx_FSM = FsmRxState.WAIT_FEND;
                            break;
                        }
                        Rx_Cmd = data_byte;
                        Rx_Crc = Do_Crc8(data_byte, Rx_Crc);
                        Rx_FSM = FsmRxState.WAIT_NBT;
                        break;
                    }
                case FsmRxState.WAIT_NBT:
                    {
                        if (data_byte > FRAME_SIZE)
                        {
                            Rx_FSM = FsmRxState.WAIT_FEND;
                            break;
                        }
                        Rx_Nbt = data_byte;
                        Rx_Crc = Do_Crc8(data_byte, Rx_Crc);
                        Rx_Ptr = 0;
                        Rx_FSM = FsmRxState.WAIT_DATA;
                        break;
                    }
                case FsmRxState.WAIT_DATA:
                    {
                        if (Rx_Ptr < Rx_Nbt)
                        {
                            ReceivedBuffer[Rx_Ptr++] = data_byte;
                            Rx_Crc = Do_Crc8(data_byte, Rx_Crc);
                            break;
                        }
                        if (data_byte != Rx_Crc)
                        {
                            Rx_FSM = FsmRxState.WAIT_FEND;
                            break;
                        }
                        Rx_FSM = FsmRxState.WAIT_FEND;
                        ret = WakeStatus.READY;
                        Command = Rx_Cmd;
                        ReceivedSize = Rx_Nbt;
                        ValidReceiveCounter += 1;
                        break;
                    }
            }
            return ret;
        }
        public static byte[] CreateWakeTxData(byte address, byte cmd, in byte[] tx_data)
        {
            if (tx_data.Length > FRAME_SIZE)
                return new byte[0];

            byte crc = CRC_INIT;
            List<byte> vec = new List<byte>();
            crc = Do_Crc8(WAKE_CODE_FEND, crc);
            vec.Add(WAKE_CODE_FEND);

            crc = Do_Crc8(address, crc);
            address |= 0x80;
            StuffTx(vec, address);

            crc = Do_Crc8(cmd, crc);
            StuffTx(vec, cmd);

            byte len = (byte)tx_data.Length;
            crc = Do_Crc8(len, crc);
            StuffTx(vec, len);
            for (int i = 0; i < tx_data.Length; i++)
            {
                crc = Do_Crc8(tx_data[i], crc);
                StuffTx(vec, tx_data[i]);
            }
            StuffTx(vec, crc);
            return vec.ToArray();
        }

        static void StuffTx(List<byte> buffer, byte data)
        {
            if (data == WAKE_CODE_FEND) {
                buffer.Add(WAKE_CODE_FESC);
                buffer.Add(WAKE_CODE_TFEND);
                return;
            } else if (data == WAKE_CODE_FESC) {
                buffer.Add(WAKE_CODE_FESC);
                buffer.Add(WAKE_CODE_TFESC);
                return;
            }
            buffer.Add(data);
        }

        private static byte Do_Crc8(byte b, byte crc)
        {
            for (byte i = 0; i < 8; b = (byte)(b >> 1), i++)
            {
                if (((b ^ crc) & 1) == 1)
                {
                    crc = (byte)(((crc ^ 0x18) >> 1) | 0x80);
                }
                else
                {
                    crc = (byte)((crc >> 1) & ~0x80);
                }
            }
            return crc;
        }
    } // class Wake

} // namespace communication
