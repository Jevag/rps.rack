﻿//------------------
//статус ридера
//    неопределен
//    не отвечает
//    нет карты
//    есть карта                            
//    карта прочитана
//    карта записана
//    ошибка чтения
//    ошибка записи
//    чужая карта
//    "есть ID"
//    "нет ID"
//  
//-------------------
//!!!!!!!!!!!!!!!!!!!!!!!!!
//  РЕКОМЕНДУЕМЫЙ ПЕРИОД МЕЖДУ КОМАНДАМИ (GetCard, ...CheckKeyA, ...Wait) НЕ МЕНЕЕ 50 ms
//!!!!!!!!!!!!!!!!!!!!!!!!!
// таймаут 5 сек - внешний таймер запускается после выдачи команд (GetCard, ...CheckKeyA, ...Write) и проверяется при Wait
//---
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

namespace Communications
{
    public class MT625Reader
    {
        SerialPort _serialPort;
        private byte numerSector;
        private byte numerBlock;
        private byte[] keyA = new byte[6];              // ключ 6 байт
        private byte[] wBlock = new byte[48];           // запись 3 блока карты
        private byte[] rBlock = new byte[48];           // чтение 3 блока карты
        private int txSize = 0;
        private int rxSize = 0;
        private int Length = 0;
        private byte[] tx = new byte[256];              // передача в ридер 
        private byte[] rx = new byte[256];              // прием из ридер 
        private string status;                          // статус ридера
        private int step = 0;                           // фаза (этап) чтения/записи
        private bool booli;
        private byte[] cardIdCard = new byte[8];        // номер карты клиента
        private int cardIdL = 0;                        //длинна ID в байтах

        private int newComand = 0;                           // неудачные попытки новой команды
        //private DateTime datetime0 = new DateTime(2000, 1, 1, 0, 0, 0, 0);  // НУЛЕВОЕ время

        public string Status { get { return status; } }             // статус ридера
        public byte[] Rx { get { return rx; } }                     // прием из ридер
        public byte[] CardIdCard { get { return cardIdCard; } }     // номер карты клиента
        public byte[] RBlock { get { return rBlock; } }             // чтение сектора карты

        public int NewComand { get { return newComand; } }                           // неудачные попытки новой команды

        public byte NumerSector { set { numerSector = value; } }    // номер сектора
        public byte NumerBlock { set { numerBlock = value; } }      // номер блока
        public byte[] KeyA { set { keyA = value; } }                // ключ 6 байт
        public byte[] WBlock { set { wBlock = value; } }            // запись сектора карты

        public int Step { get { return step; } set { step = value; } }    // фаза (этап) чтения/записи

        public MT625Reader()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 115200;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 200;
            _serialPort.ReadTimeout = 200;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    //GetStatus();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    Console.WriteLine("Возникло исключение: " + e.ToString());
                    return false;
                }
            }
            else
            {
                //GetStatus();
                return true;
            }
        }

        public bool GetVersion()
        {
            tx[1] = 0x00; tx[2] = 0x02;
            tx[3] = 0x31; tx[4] = 0x40;
            if (BccTx()) return true;
            else return false;
        }

        public bool GetCard()
        {
            tx[1] = 0x00; tx[2] = 0x02;
            tx[3] = 0x34; tx[4] = 0x30;
            if (BccTx()) return true;
            else return false;
        }

        public bool GetIdCard()
        {
            tx[1] = 0x00; tx[2] = 0x02;
            tx[3] = 0x34; tx[4] = 0x31; 
            if (BccTx()) return true;
            else return false;
        }

        public bool CheckKeyA()
        {
            tx[1] = 0x00; tx[2] = 0x09;
            tx[3] = 0x34; tx[4] = 0x32;
            tx[5] = numerSector;
            for (int i = 0; i < 6; i++)
            {
                tx[i + 6] = keyA[i];
            }
            if (BccTx()) return true;
            else return false;
        }

        public bool CheckKeyB()
        {
            tx[1] = 0x00; tx[2] = 0x09;
            tx[3] = 0x34; tx[4] = 0x39;
            tx[5] = numerSector;
            for (int i = 0; i < 6; i++)
            {
                tx[i + 6] = keyA[i];
            }
            if (BccTx()) return true;
            else return false;
        }

        public bool Read1()             // чтение блока
        {
            tx[1] = 0x00; tx[2] = 0x04;
            tx[3] = 0x34; tx[4] = 0x33;
            tx[5] = numerSector; tx[6] = numerBlock;
            if (BccTx()) return true;
            else return false;
        }

        public bool ReadNew()             // чтение сектора
        {
            tx[1] = 0x00; tx[2] = 0x0A;
            tx[3] = 0x34; tx[4] = 0x3A;
            tx[5] = numerSector; tx[6] = 0x00; //passwordType; 00=A, 01=B
            for (int i = 0; i < 6; i++)
            {
                tx[i + 7] = keyA[i];
            }

           if (BccTx()) return true;
            else return false;
        }
        public bool Write1()            // запись блока
        {
            tx[1] = 0x00; tx[2] = 0x14;
            tx[3] = 0x34; tx[4] = 0x34;
            tx[5] = numerSector; tx[6] = numerBlock;
            for (int i = 0; i < 16; i++)
            {
                tx[i + 7] = wBlock[i + (16 * numerBlock)];
            }
            if (BccTx()) return true;
            else return false;
        }

        public bool WriteNew()            // запись сектора
        {
            tx[1] = 0x00; tx[2] = 0x3C;
            tx[3] = 0x34; tx[4] = 0x3B;
            tx[5] = numerSector; tx[6] = 0x00; //passwordType; 00=A, 01=B
            for (int i = 0; i < 6; i++)
            {
                tx[i + 7] = keyA[i];
            }
            for (int i = 0; i < 16; i++)
            {
                tx[i + 13] = wBlock[i + (16 * 0)];       //numerBlock
            }
            tx[4] = (byte)';';
            for (int i = 0; i < 16; i++)
            {
                tx[i + 30] = wBlock[i + (16 * 1)];       //numerBlock
            }
            tx[4] = (byte)';';
            for (int i = 0; i < 16; i++)
            {
                tx[i + 47] = wBlock[i + (16 * 2)];       //numerBlock
            }
            if (BccTx()) return true;
            else return false;
        }

        public bool WriteKey()
        {
            tx[1] = 0x00; tx[2] = 0x09;
            tx[3] = 0x34; tx[4] = 0x35;
            tx[5] = numerSector;
            for (int i = 0; i < 6; i++)
            {
                tx[i + 6] = keyA[i];
            }
            if (BccTx()) return true;
            else return false;
        }

        public bool Read()              // CheckKey и чтение сектора
        {
            if (newComand < 3 && ReadNew())          // неудачные попытки новой команды 
            {

                step = 100;              // фаза (этап) чтения/записи
                return true;
            }
            else
            {
                if (CheckKeyA())
                {
                    step = 1;              // фаза (этап) чтения/записи
                    return true;
                }
                else
                    return false;
            }                           
        }

        public bool Write()             // CheckKey и запись сектора
        {
            if (newComand < 3 && WriteNew())          // неудачные попытки новой команды 
            {

                step = 200;              // фаза (этап) чтения/записи
                return true;
            }
            else
            {
                if (CheckKeyA())
                {
                    step = 11;              // фаза (этап) чтения/записи
                    return true;
                }
                else
                    return false;
            }
        }

        public bool Wait()              // ожидание ответа
        {
            switch (step)
            {
                case 0:         // одиночные команды ридера
                    if (Wait1()) booli = true;
                    else booli = false;
                    break;

                #region чтение сектора
                case 1:         // 
                    if (Wait1())
                    {
                        if (status == "есть карта")
                        {
                            numerBlock = 0;
                            if (Read1())
                            {
                                step = 2;
                                booli = true;       // status = "ожидание";
                            }
                            else
                            {
                                step = 0;
                                booli = false;
                            }
                        }
                        else
                        {
                            booli = true;
                        }
                    }
                    else
                    {
                        step = 0;
                        booli = false;
                    }
                    break;
                case 2:         // 
                    if (Wait1())
                    {
                        if (status == "карта прочитана")
                        {
                            numerBlock = 1;
                            if (Read1())
                            {
                                step = 3;
                                booli = true;       // status = "ожидание";
                            }
                            else
                            {
                                step = 0;
                                booli = false;
                            }
                        }
                        else
                        {
                            booli = true;
                        }
                    }
                    else
                    {
                        step = 0;
                        booli = false;
                    }
                    break;
                case 3:         // 
                    if (Wait1())
                    {
                        if (status == "карта прочитана")
                        {
                            numerBlock = 2;
                            if (Read1())
                            {
                                step = 4;
                                booli = true;       // status = "ожидание";
                            }
                            else
                            {
                                step = 0;
                                booli = false;
                            }
                        }
                        else
                        {
                            booli = true;
                        }
                    }
                    else
                    {
                        step = 0;
                        booli = false;
                    }
                    break;
                case 4:         // 
                    if (Wait1())
                    {
                        if (status == "карта прочитана")
                        {
                            //step = 0;
                            booli = true;
                        }
                        else
                        {
                            //step = 0;
                            booli = true;            // status = "ошибка чтения" | "ожидание"
                        }
                    }
                    else
                    {
                        step = 0;
                        booli = false;
                    }
                    break;
                #endregion

                #region запись сектора
                case 11:         // 
                    if (Wait1())
                    {
                        if (status == "есть карта")
                        {
                            numerBlock = 0;
                            if (Write1())
                            {
                                step = 12;
                                booli =  true;       // status = "ожидание";
                            }
                            else
                            {
                                step = 0;
                                booli = false;
                            }
                        }
                        else
                        {
                            booli = true;
                        }
                    }
                    else
                    {
                        step = 0;
                        booli = false;
                    }
                    break;
                case 12:         // 
                    if (Wait1())
                    {
                        if (status == "карта записана")
                        {
                            numerBlock = 1;
                            if (Write1())
                            {
                                step = 13;
                                booli = true;       // status = "ожидание";
                            }
                            else
                            {
                                step = 0;
                                booli = false;
                            }
                        }
                        else
                        {
                            booli = true;
                        }
                    }
                    else
                    {
                        step = 0;
                        booli = false;
                    }
                    break;
                case 13:         // 
                    if (Wait1())
                    {
                        if (status == "карта записана")
                        {
                            numerBlock = 2;
                            if (Write1())
                            {
                                step = 14;
                                booli = true;       // status = "ожидание";
                            }
                            else
                            {
                                step = 0;
                                booli = false;
                            }
                        }
                        else
                        {
                            booli = true;
                        }
                    }
                    else
                    {
                        step = 0;
                        booli = false;
                    }
                    break;
                case 14:         // 
                    if (Wait1())
                    {
                        if (status == "карта записана")
                        {
                            //step = 0;
                            //status = "карта записана";
                            booli = true;
                        }
                        else
                        {
                            //step = 0;
                            booli = true;            //  "ожидание"
                        }
                    }
                    else
                    {
                        step = 0;
                        booli = false;
                    }
                    break;
                #endregion

                #region чтение сектора new
                case 100:         // 
                    if (Wait1())
                    {
                        if (status == "карта прочитана")
                        {
                            //step = 0;
                            booli = true;
                        }
                        if(status == "ошибка чтения")
                        {
                            
                            booli = true;
                        }
                        else
                        {
                            //step = 0;
                            booli = true;            // status = "ожидание"
                        }
                    }
                    else
                    {
                        step = 0;
                        booli = false;
                    }
                    break;
                #endregion

                #region запись сектора new
                case 200:         // 
                    if (Wait1())
                    {
                        if (status == "карта записана")
                        {
                            //step = 0;
                            //status = "карта записана";
                            booli = true;
                        }
                        if (status == "ошибка записи")
                        {
                            
                            booli = true;
                        }
                        else
                        {
                            //step = 0;
                            booli = true;            //  "ожидание"
                        }

                    }
                    else
                    {
                        step = 0;
                        booli = false;
                    }
                    break;
                #endregion

                default:
                    step = 0;
                    booli = false;
                    break;
            }
                   
                    return booli;
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    //GetStatus();
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    Console.WriteLine("Возникло исключение: " + e.ToString());
                    return false;
                }
            }
            else
            {
                //GetStatus();
                return true;
            }
        }

        private bool CheckBCC()
        {
            byte BCC = 0;
            Length = (rx[1] << 8) + rx[2];
            for (int i = 0; i < Length + 4; i++)
            {
                BCC = (byte)(BCC ^ rx[i]);
            }
            if (BCC == rx[Length + 4]) return true;

            else return false;
        }

        private bool BccTx()
        {
            txSize = (tx[1] << 8) + tx[2] + 5;
            tx[0] = 0x02;
            tx[txSize - 2] = 0x03;
            tx[txSize - 1] = 0;
            for (int i = 0; i < txSize - 1; i++)
            {
                tx[txSize - 1] = (byte)(tx[txSize - 1] ^ tx[i]);
            }

            try
            {
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
                _serialPort.Write(tx, 0, txSize);
                rxSize = 0;
                status = "ожидание";
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception3: " + e.Message);
                return false;
            }
        }

        private bool Wait1()
        {
            try
            {
                if (_serialPort.BytesToRead > 0)
                {
                    int _rxSize = _serialPort.BytesToRead;
                    _serialPort.Read(
                        rx,
                        rxSize,
                        _serialPort.BytesToRead
                        );
                    rxSize += _rxSize;
                    if (rxSize > 7 && rxSize >= (rx[1] << 8) + rx[2] + 5 && CheckBCC()) // "ответ"
                    {
                        switch (rx[4])
                        {
                            case 0x40:  //GetVersion
                                status = "неопределен";
                                break;

                            case 0x30:  //GetCard
                                if (rx[5] == 0x59) status = "есть карта";
                                //if (rx[5] == 0x4E) status = "нет карты";
                                else status = "нет карты";
                                break;

                            case 0x31:  //GetIdCard
                                if (rx[5] == 0x59)
                                {
                                    cardIdL = rx[2] - 3;    //длинна ID в байтах
                                    for (int i = 0; i < 8; i++)
                                    {
                                        cardIdCard[i] = 0;
                                    }
                                    for (int i = 0; i < cardIdL; i++) 
                                    {
                                        int ii = i + 8 - cardIdL;
                                        cardIdCard[ii] = rx[i + 6];
                                    }
                                    status = "есть ID";
                                }
                                //if (rx[5] == 0x4E) status = "нет карты";
                                else status = "нет ID";
                                break;

                            case 0x32:  //CheckKeyA
                                if (rx[6] == 0x59) status = "есть карта";
                                //if (rx[6] == 0x33) status = "чужая карта";
                                else status = "чужая карта";
                                break;

                            case 0x39:  //CheckKeyB
                                if (rx[6] == 0x59) status = "есть карта";
                                //if (rx[6] == 0x33) status = "чужая карта";
                                else status = "чужая карта";
                                break;

                            case 0x33:  //Read1
                                if (rx[7] == 0x59)
                                {
                                    for (int i = 0; i < 16; i++)
                                    {
                                        rBlock[i + (16 * numerBlock)] = rx[i + 8];
                                    }
                                    status = "карта прочитана";
                                }
                                //if (rx[7] == 0x31) status = "ошибка чтения";  // ошибка сектора (неверен пароль)
                                //if (rx[7] == 0x34) status = "ошибка чтения";  // ошибка данных
                                else status = "ошибка чтения";
                                break;

                            case 0x34:  //Write1
                                if (rx[7] == 0x59) status = "карта записана";
                                //if (rx[7] == 0x31) status = "ошибка чтения";  // ошибка сектора (неверен пароль)
                                //if (rx[7] == 0x34) status = "ошибка чтения";  // ошибка данных
                                else status = "ошибка записи";
                                break;

                            case 0x35:  //WriteKey
                                if (rx[6] == 0x59) status = "WriteKeyOk";
                                //if (rx[6] == 0x34) status = "чужая карта";
                                else status = "чужая карта";
                                break;

                            case 0x3A:  //Read Sector
                                if (rx[5] == 0x59)
                                {
                                    for (int i = 0; i < 16; i++)
                                    {
                                        rBlock[i + (16 * 0)] = rx[i + 6];   // numerBlock=0
                                    }
                                    for (int i = 0; i < 16; i++)
                                    {
                                        rBlock[i + (16 * 1)] = rx[i + 23];   // numerBlock=1
                                    }
                                    for (int i = 0; i < 16; i++)
                                    {
                                        rBlock[i + (16 * 2)] = rx[i + 40];   // numerBlock=2
                                    }
                                    //for (int i = 0; i < 16; i++)
                                    //{
                                    //    rBlock[i + (16 * 2)] = rx[i + 57];   // numerBlock=3
                                    //}
                                    status = "карта прочитана";
                                }
                                //if (rx[5] == 0x31) status = "ошибка чтения";  // ошибка сектора (неверен пароль)
                                //if (rx[5] == 0x34) status = "ошибка чтения";  // ошибка данных
                                else status = "ошибка чтения";
                                break;

                            case 0x3B:  //Write Sector
                                if (rx[5] == 0x59) status = "карта записана";
                                //if (rx[5] == 0x31) status = "ошибка чтения";  // ошибка сектора (неверен пароль)
                                //if (rx[5] == 0x34) status = "ошибка чтения";  // ошибка данных
                                else status = "ошибка записи";
                                break;

                            default:
                                status = "неопределен";
                                break;
                        }
                    }
                    if (rxSize == 2 && rx[0] == 0x03 && rx[1] == 0x03)
                    {
                        if(tx[4] == 0x3A && CheckKeyA())
                        {
                            step = 1;
                        }
                        if (tx[4] == 0x3B && CheckKeyA())
                        {
                            step = 11;
                        }
                        newComand++;
                    }
                }
                else
                {

                }
                return true;
            }
            catch (Exception e)
            {
                status = "не отвечает";
                Console.WriteLine("Exception4: " + e.Message);
                return false;
            }
        }

    }
}
