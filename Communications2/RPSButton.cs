﻿using Google.Protobuf;
using LibUsbDotNet;
using LibUsbDotNet.Main;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Communications
{
    public delegate void TouchEventHandler(object sender, EventArgs e);
    //public delegate void HideEventHandler(object sender, EventArgs e);

    public class RPSButton
    {
        public static bool _TouchDetected=false;

        public static int touch_timer;

        public static int MifareDetectedCounter;

        public static bool FPGAEnabled;

        public static int VerifyItr = 0;
        public static int ReadItr = 0;
        public static int WriteItr = 0;

        public static MifareCommand.Types.MFKey.Types.KeyType MyKeyType;

        public static string Status { get; set; } //General Function

        public static bool BulkEnabled = false; //когда включен булк!

        public static event TouchEventHandler ButtonTouch;

        protected virtual void OnTouch(EventArgs e)
        {
            ButtonTouch?.Invoke(this, e);
        }

        public static int cs = 300;
        public static int b4 = 7;

        public static string InterruptAnswer = "";

        public static bool InterruptTimer = false;

        public static byte[] MifareId;

        public static string MifareId_String;

        public static bool MifareDetected; //событие!

        public static bool TouchDetected { get; set; } //событие!

        public bool IntrTimer = false;

        public bool DeviceEnabled = false;

        //public System.Timers.Timer tmrInterrupt;

        public static string MyDeviceInfo;

        public static string MyDeviceConfig;

        public static string MyDeviceEndPoints;

        public static UsbDevice MyUsbDevice;

        #region SET YOUR USB Vendor and Product ID!

        static int vid = 0x05dc;
        static int pid = 0x16c0;

        public static UsbDeviceFinder MyUsbFinder = new UsbDeviceFinder(vid, pid);
        #endregion

        public static bool CheckDevice()
        {
            ErrorCode ec = ErrorCode.None;
            MyUsbDevice = UsbDevice.OpenUsbDevice(MyUsbFinder);

            // If the device is open and ready
            if (MyUsbDevice == null)
            {
                return false;
            }
            else
            {
                if (MyUsbDevice.IsOpen)
                {
                    IUsbDevice wholeUsbDevice = MyUsbDevice as IUsbDevice;
                    if (!ReferenceEquals(wholeUsbDevice, null))
                    {
                        // Release interface #0.
                        wholeUsbDevice.ReleaseInterface(0);
                    }

                    MyUsbDevice.Close();
                }
                MyUsbDevice = null;

                // Free usb resources
                UsbDevice.Exit();

            }
            return true;
        }

        public static void CloseDevice()
        {
            try
            {
                if (MyUsbDevice.IsOpen)
                {
                    IUsbDevice wholeUsbDevice = MyUsbDevice as IUsbDevice;
                    if (!ReferenceEquals(wholeUsbDevice, null))
                    {
                        // Release interface #0.
                        wholeUsbDevice.ReleaseInterface(0);
                    }

                    MyUsbDevice.Close();
                }
                MyUsbDevice = null;

                // Free usb resources
                UsbDevice.Exit();
                Thread.Sleep(100);
            }
            catch (Exception ex) { }
        }

        //STM32L486RG
        public static bool BulkSetFPGA()
        {
            //InterruptTimer = false;
            //Thread.Sleep(300);

            ErrorCode ec = ErrorCode.None;

            try
            {            
            // Find and open the usb device.
            MyUsbDevice = UsbDevice.OpenUsbDevice(MyUsbFinder);

            UsbEndpointReader reader = MyUsbDevice.OpenEndpointReader(ReadEndpointID.Ep02); //EP2 - 0x82 чтение

            // open write endpoint 1.
            UsbEndpointWriter writer = MyUsbDevice.OpenEndpointWriter(WriteEndpointID.Ep02); //EP3 0x03 запись ?????FUCK Yeah!

            //читаем данные из файла в массив байт
            byte[] filebytes = File.ReadAllBytes(Application.StartupPath + "\\btn2.bin");

            //Отправить пакет: creset = true, conf_size, seq = 0, data - первые 39 байт кон-
            //фигурации;

            Debug.Print(filebytes.Length.ToString());

            byte cmd = 0x02;
            uint seq = 0;
            uint max_data_size = 39;
            int currbytes = 39; //текущее кол-во байт
            int allbytes = filebytes.Length;

            FPGAConfig fpga = new FPGAConfig();
            fpga.Creset = true;
            fpga.ConfSize = allbytes;
            fpga.Seq = 0;
            fpga.Data = ByteString.CopyFrom(filebytes, 0, (byte)max_data_size);

            //пакуем команду через Protobuf           
            int structlen = fpga.CalculateSize();

            byte mylen = (byte)(structlen);

            byte[] structbytes;

            using (var output = new MemoryStream())
            {
                fpga.WriteTo(output);
                structbytes = output.ToArray();
            }

            byte[] writebytes = new byte[mylen + 2];

            writebytes[0] = cmd;
            writebytes[1] = mylen;

            for (int i = 0; i < mylen; i++)
            {
                writebytes[i + 2] = structbytes[i];
            }
            Debug.Print(ByteArrayToStringX(writebytes));

            int bytesWritten;
            ec = writer.Write(writebytes, 100, out bytesWritten);
            if (ec != ErrorCode.None) throw new Exception(UsbDevice.LastErrorString);

            Thread.Sleep(300);

            seq = 1;
            while (currbytes < allbytes)
            {
                if (allbytes - currbytes >= 39)
                {
                    //пакеты 2..n-1
                    fpga = new FPGAConfig();
                    fpga.Seq = seq;
                    fpga.Data = ByteString.CopyFrom(filebytes, currbytes, (byte)max_data_size);

                    //пакуем команду через Protobuf           
                    structlen = fpga.CalculateSize();

                    mylen = (byte)(structlen);

                    //byte[] structbytes;

                    using (var output = new MemoryStream())
                    {
                        fpga.WriteTo(output);
                        structbytes = output.ToArray();
                    }

                    writebytes = new byte[mylen + 2];

                    writebytes[0] = cmd;
                    writebytes[1] = mylen;

                    for (int i = 0; i < mylen; i++)
                    {
                        writebytes[i + 2] = structbytes[i];
                    }

                    ec = writer.Write(writebytes, 100, out bytesWritten);
                    if (ec != ErrorCode.None) throw new Exception(UsbDevice.LastErrorString);

                    seq += 1;
                    currbytes += 39;
                    Debug.Print(currbytes.ToString());
                }
                else
                {
                    int mysize = allbytes - currbytes;
                    Debug.Print("mysize " + mysize.ToString());
                    //последний пакет с данными...
                    fpga = new FPGAConfig();
                    fpga.Seq = seq;
                    fpga.Data = ByteString.CopyFrom(filebytes, currbytes, (byte)mysize);

                    //пакуем команду через Protobuf           
                    structlen = fpga.CalculateSize();

                    mylen = (byte)(structlen);

                    //byte[] structbytes;

                    using (var output = new MemoryStream())
                    {
                        fpga.WriteTo(output);
                        structbytes = output.ToArray();
                    }

                    writebytes = new byte[mylen + 2];

                    writebytes[0] = cmd;
                    writebytes[1] = mylen;

                    for (int i = 0; i < mylen; i++)
                    {
                        writebytes[i + 2] = structbytes[i];
                    }
                    //Debug.Print(ByteArrayToStringX(writebytes));

                    ec = writer.Write(writebytes, 100, out bytesWritten);
                    if (ec != ErrorCode.None) throw new Exception(UsbDevice.LastErrorString);

                    seq += 1;
                    currbytes += 39;
                    Debug.Print(currbytes.ToString());
                }
            }

            fpga = new FPGAConfig();
            fpga.Nreset = true;

            //пакуем команду через Protobuf           
            structlen = fpga.CalculateSize();

            mylen = (byte)(structlen);

            //byte[] structbytes;

            using (var output = new MemoryStream())
            {
                fpga.WriteTo(output);
                structbytes = output.ToArray();
            }

            writebytes = new byte[mylen + 2];

            writebytes[0] = cmd;
            writebytes[1] = mylen;

            for (int i = 0; i < mylen; i++)
            {
                writebytes[i + 2] = structbytes[i];
            }

            ec = writer.Write(writebytes, 100, out bytesWritten);
            if (ec != ErrorCode.None) throw new Exception(UsbDevice.LastErrorString);

            byte[] readBuffer = new byte[64];
            while (ec == ErrorCode.None)
            {
                int bytesRead;

                // If the device hasn't sent data in the last 100 milliseconds,
                // a timeout error (ec = IoTimedOut) will occur. 
                ec = reader.Read(readBuffer, 1000, out bytesRead);
                Debug.Print(bytesRead.ToString());
                //if (bytesRead == 0) throw new Exception("No more bytes!");

                Debug.Print(ByteArrayToStringX(readBuffer));

            }
                return true;

            }
            
            catch (Exception ex)
            {
                return false;
                //Console.WriteLine();
                //Console.WriteLine((ec != ErrorCode.None ? ec + ":" : String.Empty) + ex.Message);
            }
            finally
            {
                if (MyUsbDevice != null)
                {
                    if (MyUsbDevice.IsOpen)
                    {
                        IUsbDevice wholeUsbDevice = MyUsbDevice as IUsbDevice;
                        if (!ReferenceEquals(wholeUsbDevice, null))
                        {
                            // Release interface #0.
                            wholeUsbDevice.ReleaseInterface(0);
                        }

                        MyUsbDevice.Close();
                    }
                    MyUsbDevice = null;

                    // Free usb resources
                    UsbDevice.Exit();
                }                
            }           
        }

        public static void ReadInterrupt(int cs_1, int b_4, bool e_cs) //Interrupt Endpoint1
        {
            ErrorCode ec = ErrorCode.None;

            try
            {
                // Find and open the usb device.
                MyUsbDevice = UsbDevice.OpenUsbDevice(MyUsbFinder);

                // If the device is open and ready
                //if (MyUsbDevice == null) throw new Exception("Device Not Found.");

                // open read endpoint 1.
                UsbEndpointReader reader = MyUsbDevice.OpenEndpointReader(ReadEndpointID.Ep01);

                byte[] readBuffer = new byte[64];
                while (ec == ErrorCode.None && InterruptTimer)
                {
                    int bytesRead;

                    // If the device hasn't sent data in the last 5 seconds,
                    // a timeout error (ec = IoTimedOut) will occur. 
                    ec = reader.Read(readBuffer, 64, out bytesRead);

                    //if (bytesRead == 0) throw new Exception(string.Format("{0}:No more bytes!", ec));
                    //Console.WriteLine("{0} bytes read", bytesRead);

                    byte cmd = readBuffer[0];
                    byte size = readBuffer[1];

                    //Debug.Print("cmd=0x" + SingleByteToString(cmd));
                    //Debug.Print("size=0x" + SingleByteToString(size));

                    if (size > 0)
                    {
                        byte[] databuffer = new byte[size];

                        for (int i = 0; i < size; i++)
                        {
                            databuffer[i] = readBuffer[i + 2]; 
                        }

                        //Debug.Print(ByteArrayToStringX(databuffer));

                        HIDResponce hidresp;

                        hidresp = HIDResponce.Parser.ParseFrom(databuffer);

                        Debug.Print(hidresp.ToString());

                        InterruptAnswer = hidresp.ToString();

                        if (hidresp.Resp != null)
                        {
                            MifareDetected = true;
                            MifareDetectedCounter = 0;
                            MifareHID mfr = hidresp.Resp;
                            MifareId = mfr.UID.ToByteArray();
                            MifareId_String = ByteArrayToStringX(MifareId);
                        }
                        else
                        {
                            if (MifareDetected)
                            {
                                MifareDetectedCounter += 1;
                                if (MifareDetectedCounter >= 10)
                                {
                                    MifareDetectedCounter = 0;
                                    MifareDetected = false;
                                    MifareId_String = "";
                                }
                            }
                        }

                        if (hidresp.Prox != null)
                        {
                            FPGAEnabled = true;

                            Proximity prox = hidresp.Prox;
                            if (!e_cs)
                            {
                                if (prox.Cs <= cs_1 && prox.Byte4 == b_4)
                                {
                                    TouchDetected = true;
                                    //OnTouch(EventArgs.Empty)
                                }
                                else
                                {
                                    TouchDetected = false;
                                }
                            }
                            else
                            {
                                if (prox.Byte4 == b_4)
                                {
                                    TouchDetected = true;
                                    //OnTouch(EventArgs.Empty)
                                }
                                else
                                {
                                    TouchDetected = false;
                                }
                            }
                        }
                        else
                        {
                            //у нас нет FPGA
                            //InterruptTimer = false;
                            FPGAEnabled = false;
                            //MessageBox.Show("FPGA Not Detected"); //наверное нужно переделать???
                            return;

                        }
                    }
                    Thread.Sleep(10);
                    Debug.Print("*******************");
                    Application.DoEvents();
                }
            }
            catch (Exception ex) { }
            finally
            {
                if (MyUsbDevice != null)
                {
                    if (MyUsbDevice.IsOpen)
                    {
                        IUsbDevice wholeUsbDevice = MyUsbDevice as IUsbDevice;
                        if (!ReferenceEquals(wholeUsbDevice, null))
                        {
                            // Release interface #0.
                            wholeUsbDevice.ReleaseInterface(0);
                        }

                        MyUsbDevice.Close();
                    }
                    MyUsbDevice = null;
                    // Free usb resources
                    UsbDevice.Exit();
                }
            }
        }

        public static bool MifareVerifyKey(byte[] CardId, byte[] CardKey, MifareCommand.Types.MFKey.Types.KeyType KeyType)
        {
            Debug.Print("Start: " + DateTime.Now.ToString("o"));
            ErrorCode ec = ErrorCode.None;
            try
            {
                // Find and open the usb device.
                MyUsbDevice = UsbDevice.OpenUsbDevice(MyUsbFinder);

                UsbEndpointReader reader = MyUsbDevice.OpenEndpointReader(ReadEndpointID.Ep02); //EP2 - 0x82 чтение

                // open write endpoint 1.
                UsbEndpointWriter writer = MyUsbDevice.OpenEndpointWriter(WriteEndpointID.Ep02); //EP3 0x03 запись ?????FUCK Yeah! Нужно установить соответствия

                //пакуем команду через Protobuf
                MifareCommand mfr = new MifareCommand();

                mfr.ECommand = MFCmd.MfVerifyKey;

                mfr.ITimeout = 10000;

                mfr.VUID = ByteString.CopyFrom(CardId);

                mfr.XKey = new MifareCommand.Types.MFKey();

                mfr.XKey.EType = KeyType; //0; //MifareCommand.Types.MFKey.Types.KeyType;

                mfr.IBlockNum = 0;

                mfr.XKey.VKey = ByteString.CopyFrom(CardKey);

                Debug.Print(mfr.ToString());

                byte cmd = 0x04; //команда MiFare из протобуф

                int structlen = mfr.CalculateSize();

                byte mylen = (byte)(structlen);

                byte[] structbytes;

                using (var output = new MemoryStream())
                {
                    mfr.WriteTo(output);
                    structbytes = output.ToArray();
                }

                byte[] writebytes = new byte[mylen + 2];

                writebytes[0] = cmd;
                writebytes[1] = mylen;

                for (int i = 0; i < mylen; i++)
                {
                    writebytes[i + 2] = structbytes[i];
                }

                int bytesWritten;
                ec = writer.Write(writebytes, 20, out bytesWritten); //было 2000
                if (ec != ErrorCode.None) throw new Exception(UsbDevice.LastErrorString);

                byte[] readBuffer = new byte[64];
                VerifyItr = 0;
                while (VerifyItr <= 5)
                {
                    int bytesRead;
                    ec = reader.Read(readBuffer, 20, out bytesRead); //было 1000
                                                                     //Debug.Print(bytesRead.ToString());
                                                                     //if (bytesRead == 0) throw new Exception("No more bytes!");

                    byte cmd_r = readBuffer[0];
                    byte size_r = readBuffer[1];

                    byte[] databuffer = new byte[size_r];

                    for (int i = 0; i < size_r; i++)
                    {
                        databuffer[i] = readBuffer[i + 2]; //тут титя!
                    }

                    MifareResponce mfr_resp = new MifareResponce();

                    mfr_resp = MifareResponce.Parser.ParseFrom(databuffer);

                    Debug.Print(mfr_resp.ToString());

                    Debug.Print(mfr_resp.EStatus.ToString());

                    if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatNoCard) //ждем какой-то таймаут, потом говорим, что несистемная!
                    {
                        VerifyItr += 1;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatUidNotValid) //ждем счетчик штук 5 (UID берем с интеррупта)
                    {
                        VerifyItr += 1;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatPasswdError) //не системная
                    {
                        VerifyItr += 1;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatOk) //return true;
                    {
                        goto good;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MsStatDummy) //дрм
                    {
                        VerifyItr += 1;
                    }
                    else
                    {
                        VerifyItr += 1;
                    }
                    if (VerifyItr == 3) goto bad;
                    Application.DoEvents();
                }

                good: Debug.Print(VerifyItr.ToString() + "===>>" + DateTime.Now.ToString("o"));
                return true;
                bad: Debug.Print(DateTime.Now.ToString("o"));
                return false;
            }

            catch (Exception ex)
            {
                //Console.WriteLine();
                //Console.WriteLine((ec != ErrorCode.None ? ec + ":" : String.Empty) + ex.Message);
                return false;
            }
            finally
            {
                if (MyUsbDevice != null)
                {
                    if (MyUsbDevice.IsOpen)
                    {
                        IUsbDevice wholeUsbDevice = MyUsbDevice as IUsbDevice;
                        if (!ReferenceEquals(wholeUsbDevice, null))
                        {
                            // Release interface #0.
                            wholeUsbDevice.ReleaseInterface(0);
                        }

                        MyUsbDevice.Close();
                    }
                    MyUsbDevice = null;
                    // Free usb resources
                    UsbDevice.Exit();
                }
            }
        }

        public static byte[] MifareReadData(byte[] CardId, byte[] CardKey, uint block, MifareCommand.Types.MFKey.Types.KeyType KeyType)
        {
            Debug.Print("Start: " + DateTime.Now.ToString("o"));
            byte[] retval = null;
            ErrorCode ec = ErrorCode.None;

            try
            {
                // Find and open the usb device.
                MyUsbDevice = UsbDevice.OpenUsbDevice(MyUsbFinder);

                UsbEndpointReader reader = MyUsbDevice.OpenEndpointReader(ReadEndpointID.Ep02); //EP2 - 0x82 чтение

                // open write endpoint 1.
                UsbEndpointWriter writer = MyUsbDevice.OpenEndpointWriter(WriteEndpointID.Ep02); //EP3 0x03 запись ?????FUCK Yeah! Нужно установить соответствия

                //пакуем команду через Protobuf
                MifareCommand mfr = new MifareCommand();

                mfr.ECommand = MFCmd.MfReadBlock;

                mfr.ITimeout = 10000;

                mfr.VUID = ByteString.CopyFrom(CardId);

                mfr.XKey = new MifareCommand.Types.MFKey();

                mfr.XKey.EType = KeyType;

                mfr.IBlockNum = block;

                mfr.XKey.VKey = ByteString.CopyFrom(CardKey);

                Debug.Print(mfr.ToString());

                byte cmd = 0x04; //команда MiFare из протобуф

                int structlen = mfr.CalculateSize();

                byte mylen = (byte)(structlen);

                byte[] structbytes;

                using (var output = new MemoryStream())
                {
                    mfr.WriteTo(output);
                    structbytes = output.ToArray();
                }

                byte[] writebytes = new byte[mylen + 2];

                writebytes[0] = cmd;
                writebytes[1] = mylen;

                for (int i = 0; i < mylen; i++)
                {
                    writebytes[i + 2] = structbytes[i];
                }


                int bytesWritten;
                ec = writer.Write(writebytes, 10, out bytesWritten);
                if (ec != ErrorCode.None) throw new Exception(UsbDevice.LastErrorString);

                byte[] readBuffer = new byte[64];
                ReadItr = 0;
                while (ReadItr <= 3)
                {
                    int bytesRead;

                    ec = reader.Read(readBuffer, 10, out bytesRead);

                    byte cmd_r = readBuffer[0];
                    byte size_r = readBuffer[1];

                    byte[] databuffer = new byte[size_r];

                    for (int i = 0; i < size_r; i++)
                    {
                        databuffer[i] = readBuffer[i + 2]; //тут титя!
                    }

                    MifareResponce mfr_resp = new MifareResponce();

                    mfr_resp = MifareResponce.Parser.ParseFrom(databuffer);

                    Debug.Print(mfr_resp.ToString());

                    Debug.Print(mfr_resp.EStatus.ToString());

                    if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatNoCard) //ждем какой-то таймаут, потом говорим, что несистемная!
                    {
                        ReadItr += 1;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatUidNotValid) //ждем счетчик штук 5 (UID берем с интеррупта)
                    {
                        ReadItr += 1;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatPasswdError) //не системная
                    {
                        ReadItr += 1;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatOk) //return true;
                    {
                        retval = mfr_resp.VBlockData.ToByteArray();
                        goto good;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MsStatDummy) //дрм
                    {
                        ReadItr += 1;
                    }
                    else
                    {
                        ReadItr += 1;
                    }
                    if (ReadItr == 3) goto bad;
                    Application.DoEvents();
                }
                good: Debug.Print(VerifyItr.ToString() + "===>>" + DateTime.Now.ToString("o"));
                return retval;
                bad: Debug.Print(DateTime.Now.ToString("o"));
                return null;

            }
            catch (Exception ex)
            {
                return null;
                //Console.WriteLine();
                //Console.WriteLine((ec != ErrorCode.None ? ec + ":" : String.Empty) + ex.Message);
            }
            finally
            {
                if (MyUsbDevice != null)
                {
                    if (MyUsbDevice.IsOpen)
                    {
                        IUsbDevice wholeUsbDevice = MyUsbDevice as IUsbDevice;
                        if (!ReferenceEquals(wholeUsbDevice, null))
                        {
                            // Release interface #0.
                            wholeUsbDevice.ReleaseInterface(0);
                        }

                        MyUsbDevice.Close();
                    }
                    MyUsbDevice = null;

                    // Free usb resources
                    UsbDevice.Exit();

                }
            }
        }

        public static bool MifareWriteData(byte[] CardId, byte[] CardKey, uint block, MifareCommand.Types.MFKey.Types.KeyType KeyType, byte[] blockdata)
        {
            Debug.Print("Start: " + DateTime.Now.ToString("o"));
            ErrorCode ec = ErrorCode.None;

            try
            {
                // Find and open the usb device.
                MyUsbDevice = UsbDevice.OpenUsbDevice(MyUsbFinder);

                UsbEndpointReader reader = MyUsbDevice.OpenEndpointReader(ReadEndpointID.Ep02); //EP2 - 0x82 чтение

                // open write endpoint 1.
                UsbEndpointWriter writer = MyUsbDevice.OpenEndpointWriter(WriteEndpointID.Ep02); //EP3 0x03 запись ?????FUCK Yeah! Нужно установить соответствия

                //пакуем команду через Protobuf
                MifareCommand mfr = new MifareCommand();

                mfr.ECommand = MFCmd.MfWriteBlock;

                mfr.ITimeout = 10000;

                mfr.VUID = ByteString.CopyFrom(CardId);

                mfr.XKey = new MifareCommand.Types.MFKey();

                mfr.XKey.EType = KeyType;

                mfr.IBlockNum = block;

                mfr.XKey.VKey = ByteString.CopyFrom(CardKey);

                mfr.VBlockData = ByteString.CopyFrom(blockdata);

                Debug.Print(mfr.ToString());

                byte cmd = 0x04; //команда MiFare из протобуф

                int structlen = mfr.CalculateSize();

                byte mylen = (byte)(structlen);

                byte[] structbytes;

                using (var output = new MemoryStream())
                {
                    mfr.WriteTo(output);
                    structbytes = output.ToArray();
                }

                byte[] writebytes = new byte[mylen + 2];

                writebytes[0] = cmd;
                writebytes[1] = mylen;

                for (int i = 0; i < mylen; i++)
                {
                    writebytes[i + 2] = structbytes[i];
                }

                int bytesWritten;
                ec = writer.Write(writebytes, 10, out bytesWritten);

                byte[] readBuffer = new byte[64];
                WriteItr = 0;
                while (WriteItr <= 3)
                {
                    int bytesRead;

                    ec = reader.Read(readBuffer, 10, out bytesRead);

                    byte cmd_r = readBuffer[0];
                    byte size_r = readBuffer[1];

                    byte[] databuffer = new byte[size_r];

                    for (int i = 0; i < size_r; i++)
                    {
                        databuffer[i] = readBuffer[i + 2]; //тут титя!
                    }

                    MifareResponce mfr_resp = new MifareResponce();

                    mfr_resp = MifareResponce.Parser.ParseFrom(databuffer);

                    if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatNoCard) //ждем какой-то таймаут, потом говорим, что несистемная!
                    {
                        WriteItr += 1;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatUidNotValid) //ждем счетчик штук 5 (UID берем с интеррупта)
                    {
                        WriteItr += 1;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatPasswdError) //не системная
                    {
                        WriteItr += 1;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MfStatOk) //return true;
                    {
                        goto good;
                    }
                    else if (mfr_resp.EStatus == MifareResponce.Types.MFStatus.MsStatDummy) //дрм
                    {
                        WriteItr += 1;
                    }
                    else
                    {
                        WriteItr += 1;
                    }
                    if (WriteItr == 3) goto bad;
                    Application.DoEvents();

                }
                good: Debug.Print(VerifyItr.ToString() + "===>>" + DateTime.Now.ToString("o"));
                return true;
                bad: Debug.Print(DateTime.Now.ToString("o"));
                return false;

                //Console.WriteLine("\r\nDone!\r\n");

            }

            catch (Exception ex)
            {
                return false;
                //Console.WriteLine();
                //Console.WriteLine((ec != ErrorCode.None ? ec + ":" : String.Empty) + ex.Message);
            }
            finally
            {
                if (MyUsbDevice != null)
                {
                    if (MyUsbDevice.IsOpen)
                    {
                        IUsbDevice wholeUsbDevice = MyUsbDevice as IUsbDevice;
                        if (!ReferenceEquals(wholeUsbDevice, null))
                        {
                            // Release interface #0.
                            wholeUsbDevice.ReleaseInterface(0);
                        }

                        MyUsbDevice.Close();
                    }
                    MyUsbDevice = null;

                    // Free usb resources
                    UsbDevice.Exit();

                }
            }
        }


        #region IsService
        public static string SingleByteToString(byte ba)
        {
            byte[] b = new byte[1];
            b[0] = ba;

            return BitConverter.ToString(b);
        }

        public static string ByteArrayToStringX(byte[] ba)
        {
            return BitConverter.ToString(ba);
        }

        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }

        public static string SingleByteToBitString(byte ba)
        {
            return Convert.ToString(ba, 2).PadLeft(8, '0');
        }
        #endregion
    }
}
