﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class RT200_New
    {
        string command = "";
        SerialPort _serialPort;
        public string Status { get; private set; }             // статус сканера Barcode
        public bool LoopA { get; set; }

        public string barcode { get; set; }

        public bool scan_started { get; set; }

        System.Timers.Timer tmrScanImpulse;
        System.Timers.Timer tmrRX;

        int bytescountrx = 0;

        public RT200_New()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 115200;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 200;
            _serialPort.ReadTimeout = 200;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
            _serialPort.DataReceived += _serialPort_DataReceived;

            tmrScanImpulse = new System.Timers.Timer();
            tmrScanImpulse.Interval = 5250; //250...
            tmrScanImpulse.Elapsed += TmrScanImpulse_Elapsed;
            tmrScanImpulse.Enabled = false;
            tmrScanImpulse.AutoReset = true;

            scan_started = false;

            
            tmrRX = new System.Timers.Timer();
            tmrRX.Interval = 500; //250...
            tmrRX.Elapsed += TmrRX_Elapsed;
            tmrRX.Enabled = true;
            tmrRX.AutoReset = true;

            //command = "none";
            
        }

        private void TmrRX_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            try
            {
                bytescountrx = _serialPort.BytesToRead;

                byte[] tbytes = new byte[bytescountrx];
                _serialPort.Read(tbytes, 0, bytescountrx);
                barcode = ByteArrayEncode(tbytes);
                if (barcode.Contains("3u"))
                {
                    barcode = "";
                }
                Debug.Print(barcode);
                bytescountrx = 0;
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
            }
            catch (Exception ex) { }
        }

        private void TmrScanImpulse_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            EnableLight();
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //throw new NotImplementedException();
            //bytescountrx = _serialPort.BytesToRead;
            //Debug.Print(bytescountrx.ToString());
            //byte[] tbytes = new byte[bytescountrx];
            //_serialPort.Read(tbytes, 0, bytescountrx);
            //barcode = ByteArrayEncode(tbytes);
            //Debug.Print(barcode);
            //bytescountrx = 0;
            //3u
            /*
            if (bytescountrx > 0 && bytescountrx <= 5)
            {
                byte[] bbytes = new byte[bytescountrx];
                _serialPort.Read(bbytes, 0, bytescountrx);
                bytescountrx = 0;
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
            }
            else if (bytescountrx > 5)
            {
            */
                        /*
                        byte[] tbytes = new byte[bytescountrx];
                        _serialPort.Read(tbytes, 0, bytescountrx);
                        barcode = ByteArrayEncode(tbytes);
                        if (barcode.Contains("3u"))
                        {
                        barcode = "";
                        }
                        Debug.Print(barcode);
                        bytescountrx = 0;
                        _serialPort.DiscardInBuffer();
                        _serialPort.DiscardOutBuffer();
                        */
            //} 
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();

                    return true;
                }
                catch (Exception e)
                {
                    Status = "не отвечает";

                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    return true;
                }
                catch (Exception e)
                {
                    Status = "не отвечает";
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool StartScan()
        {
            if (EnableLight())
            {
                tmrScanImpulse.Enabled = true;
                scan_started = true;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool StopScan()
        {
            if (DisableLight())
            {
                scan_started = false;
                tmrScanImpulse.Enabled = false;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EnableLight()
        {
            byte[] ba = new byte[] { 0x32, 0x75, 0x01 };

            //tx[0] = 0x32; tx[1] = 0x75; tx[2] = 0x01;
            //txSize = 3;
            try
            {
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
                _serialPort.Write(ba, 0, 3);
                /*
                rxSize = 0;
                qrid = "";
                status = "ожидание";
                */
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception3: " + e.Message);
                return false;
            }
        }

        public bool DisableLight()
        {
            byte[] ba = new byte[] { 0x32, 0x75, 0x02 };
            //tx[0] = 0x32; tx[1] = 0x75; tx[2] = 0x02;
            //txSize = 3;
            try
            {
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
                _serialPort.Write(ba, 0, 3);

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception3: " + e.Message);
                return false;
            }
        }


        #region statics
        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba); //.Replace("-", "");
        }

        public static string ByteArrayEncode(byte[] data)
        {
            char[] characters = data.Select(b => (char)b).ToArray();
            return new string(characters);
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string SingleByteToString(byte ba)
        {
            byte[] b = new byte[1];
            b[0] = ba;

            return BitConverter.ToString(b);
        }
        /*
        public static void CalcExample()
        {
            byte Datas = (0xD4 & (~0x30)) + 0x30;    //(0xD4 & （！ 0x03） ) + 0x02;
            Debug.Print(SingleByteToString(Datas));
        }
        */
        #endregion

    }
}
