﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Communications
{
    public class YJD30
    {
        public string Port { get; set; }

        public bool first_open = false;

        public bool otval2 = false;

        int bytescountrx = 0;
        string command = "";
        SerialPort _serialPort;
        System.Timers.Timer tmrRX;
        System.Timers.Timer tmrReboot;
        public string Status { get; private set; }             // статус сканера Barcode
        public string barcode { get; set; }

        /// <summary>Стартовый байт. Каждая посылка от сканера начинается с 0x02</summary>
        private const byte START = 0x02;
        /// <summary>Стоповый байт. Каждая посылка от сканера заканчивается на 0x03</summary>
        private const byte STOP = 0x03;

        public bool scan_started = false;

        public enum Symbology
        {
            CODABAR = 0x61,
            CODE_11 = 0x68,
            CODE_128 = 0x6A,
            CODE_32 = 0x3C,
            CODE_39 = 0x62,
            CODE_93 = 0x69,
            QR = 0x73,
            UNKNOWN = 0xFF
        }

        public enum StatusE
        {
            INIT,
            ERROR,
            READY
        }

        private enum FsmState
        {
            WAIT_START,
            RECEIVING
        }

        public YJD30()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 200;
            _serialPort.ReadTimeout = 200;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            //_serialPort.Handshake = Handshake.None;
            //_serialPort.DataReceived += _serialPort_DataReceived;

            tmrReboot = new System.Timers.Timer();
            tmrReboot.Interval = 5000; //250...
            tmrReboot.Elapsed += TmrReboot_Elapsed;
            tmrReboot.Enabled = true;
            tmrReboot.AutoReset = true;

            tmrRX = new System.Timers.Timer();
            tmrRX.Interval = 250; //250...
            tmrRX.Elapsed += TmrRX_Elapsed;
            tmrRX.Enabled = true;
            tmrRX.AutoReset = true;

            scan_started = false;
        }

        private void TmrReboot_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            try
            {
                if (!_serialPort.IsOpen && first_open)
                {
                    Debug.Print("reopened_port");
                    Open(Port);
                }
            }
            catch (Exception ex) { }
        }

        private void TmrRX_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //throw new NotImplementedException();
                bytescountrx += _serialPort.BytesToRead;
                //Debug.Print(bytescountrx.ToString());


                if (bytescountrx > 0)
                {
                    byte[] tbytes = new byte[bytescountrx];
                    _serialPort.Read(tbytes, 0, bytescountrx);
                    //Debug.Print(ByteArrayToString(tbytes));
                    //Debug.Print(ByteArrayEncode(tbytes));
                    CheckBarcode(tbytes);
                    bytescountrx = 0;
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                }
            }
            catch (Exception ex) { }
        }

        //private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        //{
        //throw new NotImplementedException();
        //Debug.Print("sosoos");
        //}

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    Status = "работает";
                    first_open = true;
                    return true;
                }
                catch (Exception e)
                {
                    Status = "не отвечает";

                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    return true;
                }
                catch (Exception e)
                {
                    Status = "не отвечает";
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public void EnableHostMode()
        {
            //57 00 00 03 04 01 00 00 00 00
            //00 1F 71 50 41
            try
            {
                if (_serialPort.IsOpen)
                {
                    byte[] TxArr = new byte[] { 0x57, 0x00, 0x00, 0x03, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f, 0x71, 0x50, 0x41 };
                    _serialPort.Write(TxArr, 0, TxArr.Length);
                }
            }
            catch (Exception ex) { }
        }

        public void EnableContMode()
        {
            //57 00 00 03 03 00 00 00 00 00
            //00 68 60 50 41
            try
            {
                if (_serialPort.IsOpen)
                {
                    byte[] TxArr = new byte[] { 0x57, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x68, 0x60, 0x50, 0x41 };
                    _serialPort.Write(TxArr, 0, TxArr.Length);
                }
            }
            catch (Exception ex) { }

        }

        public void EnableSenseMode()
        {
            try
            {
                if (_serialPort.IsOpen)
                {
                    byte[] TxArr = new byte[] { 0x57, 0x00, 0x00, 0x03, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4a, 0x71, 0x50, 0x41 };
                    _serialPort.Write(TxArr, 0, TxArr.Length);
                }
            }
            catch (Exception ex) { }

        }

        //сделать на bool???
        public void StartScan()
        {
            // 57 00 00 03 04 03 00 00 00 04
            // 00 00 00 00 00 F7 81 50 41
            try
            {
                if (_serialPort.IsOpen)
                {
                    Debug.Print("start_scan");
                    byte[] TxArr = new byte[] { 0x57, 0x00, 0x00, 0x03, 0x04, 0x03, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf7, 0x81, 0x50, 0x41 };
                    _serialPort.Write(TxArr, 0, TxArr.Length);
                }
            }
            catch (Exception ex) {
                Debug.Print("sasaii");
            }

        }

        //сделать на bool???
        public void StopScan()
        {
            //57 00 00 03 04 03 00 00 00 04
            //00 01 00 00 00 F6 7d 50 41
            try
            {
                if (_serialPort.IsOpen)
                {
                    byte[] TxArr = new byte[] { 0x57, 0x00, 0x00, 0x03, 0x04, 0x03, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00, 0xf6, 0x7d, 0x50, 0x41 };
                    _serialPort.Write(TxArr, 0, TxArr.Length);
                }
            }
            catch (Exception ex) { }
        }

        public void EnterSettings()
        {
            //57 00 00 02 01 00 00 00 00 04
            //00 01 00 00 00 EA 7D 50 41
            try
            {
                if (_serialPort.IsOpen)
                {
                    byte[] TxArr = new byte[] { 0x57, 0x00, 0x00, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00, 0xea, 0x7d, 0x50, 0x41 };
                    _serialPort.Write(TxArr, 0, TxArr.Length);
                }
            }
            catch (Exception ex) { }
        }

        public void ExitSettings()
        {
            //57 00 00 02 01 00 00 00 00 04
            //00 00 00 00 00 EB 81 50 41
            try
            {
                if (_serialPort.IsOpen)
                {
                    byte[] TxArr = new byte[] { 0x57, 0x00, 0x00, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0xeb, 0x81, 0x50, 0x41 };
                    _serialPort.Write(TxArr, 0, TxArr.Length);
                }
            }
            catch (Exception ex) { }
        }

        public void ToFactory()
        {
            //57 00 00 02 02 00 00 00 00 00
            //00 B9 6C 50 41
            try
            {
                if (_serialPort.IsOpen)
                {
                    byte[] TxArr = new byte[] { 0x57, 0x00, 0x00, 0x02, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xb9, 0x6c, 0x50, 0x41 };
                    _serialPort.Write(TxArr, 0, TxArr.Length);
                }
            }
            catch (Exception ex) { }
        }

        public void SetDefault()
        {
            Debug.Print("start_default");
            EnterSettings();
            Thread.Sleep(1000);
            ToFactory();
            Thread.Sleep(10000);
            ExitSettings();
            Debug.Print("end_default");
        }

        public void SetSTX()
        {
            //57 00 00 05 05 00 00 00 00 04
            //00 01 00 00 00 0A 86 50 41
            try
            {
                if (_serialPort.IsOpen)
                {
                    byte[] TxArr = new byte[] { 0x57, 0x00, 0x00, 0x05, 0x05, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0a, 0x86, 0x50, 0x41 };
                    _serialPort.Write(TxArr, 0, TxArr.Length);
                }
            }
            catch (Exception ex) { }

        }

        public void EnableAIMID()
        {
            //57 00 00 05 04 00 00 00 00 04
            //00 01 00 00 00 0E 7A 50 41
            try
            {
                if (_serialPort.IsOpen)
                {
                    byte[] TxArr = new byte[] { 0x57, 0x00, 0x00, 0x05, 0x04, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0e, 0x7a, 0x50, 0x41 };
                    _serialPort.Write(TxArr, 0, TxArr.Length);
                }
            }
            catch (Exception ex) { }
        }

        public void SetETX()
        {
            //57 00 00 05 06 00 00 00 00 04
            //00 04 00 00 00 05 0E 50 41
            try
            {
                if (_serialPort.IsOpen)
                {
                    byte[] TxArr = new byte[] { 0x57, 0x00, 0x00, 0x05, 0x06, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00, 0x05, 0x0e, 0x50, 0x41 };
                    _serialPort.Write(TxArr, 0, TxArr.Length);
                }
            }
            catch (Exception ex) { }

        }

        public void SetPrefixes()
        {
            Debug.Print("start_settings!");
            EnterSettings();
            Thread.Sleep(1000);
            SetSTX();
            Thread.Sleep(1000);
            EnableAIMID();
            Thread.Sleep(1000);
            SetETX();
            Thread.Sleep(1000);
            ExitSettings();
            Debug.Print("done!");
            System.Windows.Forms.MessageBox.Show("YJD30 Settings Update Success");
        }

        public bool CheckBarcode(byte[] arr)
        {
            //Debug.Print(arr.Length.ToString());
            try
            {
                if (arr.Length > 0)
                {
                    if (arr[0] == START && arr[arr.Length - 1] == STOP)
                    {
                        string retval = "";
                        byte[] arr2 = new byte[arr.Length - 5];
                        Buffer.BlockCopy(arr, 4, arr2, 0, arr.Length - 5);
                        //Debug.Print(ByteArrayEncode(arr2));
                        if (arr2.Length == 20 && arr2[arr2.Length - 1] == 0x30) //&& arr2[arr2.Length - 2] == 0x30)
                        {
                            byte[] arr3 = new byte[arr2.Length - 1];
                            Buffer.BlockCopy(arr2, 0, arr3, 0, arr2.Length - 1);
                            retval = ByteArrayEncode(arr3);
                        }
                        else
                        {
                            retval = ByteArrayEncode(arr2);
                        }
                        barcode = retval;
                        Debug.Print(retval);
                    }
                }
            }
            catch (Exception ex) {
                barcode = "";
            }

            return false;
        }


        #region statics
        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba); //.Replace("-", "");
        }

        public static string ByteArrayEncode(byte[] data)
        {
            char[] characters = data.Select(b => (char)b).ToArray();
            return new string(characters);
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string SingleByteToString(byte ba)
        {
            byte[] b = new byte[1];
            b[0] = ba;

            return BitConverter.ToString(b);
        }
        /*
        public static void CalcExample()
        {
            byte Datas = (0xD4 & (~0x30)) + 0x30;    //(0xD4 & （！ 0x03） ) + 0x02;
            Debug.Print(SingleByteToString(Datas));
        }
        */
        #endregion


    }
}
