﻿////
//  каждая тетрада в comDiskret (команда в дискрет) - это 4 выхода на один порт (одна плата на разъеме Tablo)
//  каждая тетрада в statDiskret (статус дискрета) - это 4 входа на той же плате (порту)
//  0 на выходе - контакты реле разомкнуты, 1 - замкнуты
//  0 на входе - напряжения на входе нет, 1 - есть
////
//формат команды запись/чтение (запись) дискрета 15 байт
/*---
02-stx
скорость 9600
Отключен паритет + 1 Стоп бит + 8 бит
порт F TX1|RX1


=====================
'2'           ;32-код команды чтение SIGNATURE
=====================
'7'           ;37-код команды чтение версии ПО
=====================
'1'           ;31-код команды запись/чтение дискрета
=====================
'5'		;35-код команды вывод на табло
======================
формат команды вывод на табло
----
00-длинна ст байт
0A-(10)длинна мл байт
35-код команды вывод на табло
30-просто код
31-"1" цифра старший разряд GREEN
00-" " цифра старший разряд RED
32-"2" цифра
00-" " цифра
30-"0" цифра
00-" " цифра
33-"3" цифра младший разряд GREEN
00-" " цифра младший разряд RED
03-etx
BCC
---------------------------------
формат ответа вывод на табло
---
02-stx
00-длинна ст байт
01-(1)длинна мл байт
35-код команды вывод на табло
30-просто код
30-код "0" (нет ошибок)
03-etx
BCC
*/

////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Globalization;


namespace Communications
{
    public class Tablo
    {
        private int comDiskret;             // команда в дискрет шлаг.вниз=1, шлаг.вверх=2, светофор GRIN=4. светофор RED=8
        //private string TabloPort1 = " ";          // порт 1 дискрет
        //private string TabloPort2 = " ";          // порт 2 дискрет
        //private string TabloPort3 = " ";          // порт 3 дискрет
        //private string TabloPort4 = " ";          // порт 4 дискрет
// дискретный порт из ряда (соответствует мануалу на микросхему и обозначению на плате контроллера):
//      - пробел для неиспользуемых портов   
//A
//B
//C
//D     - не использовать под дискрет, там СОМ для связи с РС
//E
//F
//H
//J
//K

        private int txSize = 0;
        private int rxSize = 0;
        private int Length = 0;
        private byte[] tx = new byte[256];              // передача в ридер 
        private byte[] rx = new byte[256];              // прием из ридер 
        
        private string status;                          // статус Tablo

        // реальный статус дискретных устройств пишем в statDiskret  
        private int statDiskret;            // статус loopA=1, ИК=2, loopB=4, кнопка "НАЖМИТЕ"=8,  
        // кнопка "ВЫЗОВ"=16, датчик верхней двери=32,  датчик нижней двери=64, ...=128

        SerialPort _serialPort;

        public int StatDiskret { get { return statDiskret; } }      
        public string Status { get { return status; } }             // статус Tablo контроллера

        public int ComDiskret { set { comDiskret = value; } }       //команда в дискрет шлаг.вниз=1, шлаг.вверх=2, светофор GRIN=4. светофор RED=8
        //public string TabloPort1 { get { return TabloPort1; } set { TabloPort1 = value; } }       // порт 1 дискрет
        //public string TabloPort2 { get { return TabloPort2; } set { TabloPort2 = value; } }       // порт 2 дискрет
        //public string TabloPort3 { get { return TabloPort3; } set { TabloPort3 = value; } }       // порт 3 дискрет
        //public string TabloPort4 { get { return TabloPort4; } set { TabloPort4 = value; } }       // порт 4 дискрет
        public string TabloPort1 { get; set; }      // 0x31-"1" цифра старший разряд GREEN
        public string TabloPort2 { get; set; }      // 0x20-" " цифра старший разряд RED - любой символ не цифры высветит " "
        public string TabloPort3 { get; set; }
        public string TabloPort4 { get; set; }
        public string TabloPort5 { get; set; }
        public string TabloPort6 { get; set; }
        public string TabloPort7 { get; set; }      // 0x33-"3" цифра младший разряд GREEN
        public string TabloPort8 { get; set; }      // 0x20-" " цифра младший разряд RED - любой символ не цифры высветит " "

        private string signatureTablo;                          // SignatureTablo
        public string SignatureTablo { get { return signatureTablo; } }           // строка SignatureTablo контроллера  

        private string versionTablo = "";                          // версии ПО Tablo
        public string VersionTablo { get { return versionTablo; } }            // строка версии ПО Tablo контроллера 

        public Tablo()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 600;
            _serialPort.ReadTimeout = 600;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    //GetStatus();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    Console.WriteLine("Возникло исключение: " + e.ToString());
                    return false;
                }
            }
            else
            {
                //GetStatus();
                return true;
            }
        }

        public bool Display()                      // команда в Tablo выдачи цифр
        {
            tx[1] = 0x00; tx[2] = 0x0A;
            tx[3] = 0x35; tx[4] = 0x30;
            // формирование команды
            //tx[5] = Convert.ToByte(TabloPort1);
            //tx[6] = Convert.ToByte(TabloPort2);
            //tx[7] = Convert.ToByte(TabloPort3);
            //tx[8] = Convert.ToByte(TabloPort4);
            byte[]
            bytes = Encoding.ASCII.GetBytes(TabloPort1);      // 0x31-"1" цифра старший разряд GREEN
            tx[5] = bytes[0];
            bytes = Encoding.ASCII.GetBytes(TabloPort2);
            tx[6] = bytes[0];
            bytes = Encoding.ASCII.GetBytes(TabloPort3);
            tx[7] = bytes[0];
            bytes = Encoding.ASCII.GetBytes(TabloPort4);
            tx[8] = bytes[0];

            bytes = Encoding.ASCII.GetBytes(TabloPort5);
            tx[9] = bytes[0];
            bytes = Encoding.ASCII.GetBytes(TabloPort6);
            tx[10] = bytes[0];
            bytes = Encoding.ASCII.GetBytes(TabloPort7);
            tx[11] = bytes[0];
            bytes = Encoding.ASCII.GetBytes(TabloPort8);      // 0x20-" " цифра младший разряд RED - любой символ не цифры высветит " "
            tx[12] = bytes[0];


            if (BccTx()) return true;
            else return false;
        }

        public bool Diskret()                      // команда в Tablo выдачи дискретных сигналов на выход и запрос состояния дискретных входов
        {
                tx[1] = 0x00; tx[2] = 0x0A;
                tx[3] = 0x31; tx[4] = 0x30;
                // формирование команды
                //tx[5] = Convert.ToByte(TabloPort1);
                //tx[6] = Convert.ToByte(TabloPort2);
                //tx[7] = Convert.ToByte(TabloPort3);
                //tx[8] = Convert.ToByte(TabloPort4);
                byte[] 
                bytes = Encoding.ASCII.GetBytes(TabloPort1);
                tx[5] = bytes[0];
                bytes = Encoding.ASCII.GetBytes(TabloPort2);
                tx[6] = bytes[0];
                bytes = Encoding.ASCII.GetBytes(TabloPort3);
                tx[7] = bytes[0];
                bytes = Encoding.ASCII.GetBytes(TabloPort4);
                tx[8] = bytes[0];

                for (int i = 0; i < 4; i++)
                {
                    int a = (comDiskret & (15 << (i * 4))) >> (i * 4);  // выбор тетрады
                    tx[i + 9] = (byte) a;
                }

                if (BccTx()) return true;
                else return false;
        }

        public bool SignatureTabloRead()                      // команда в Tablo чтения SignatureTablo
        {
            tx[1] = 0x00; tx[2] = 0x02;
            tx[3] = 0x32; tx[4] = 0x30;
            if (BccTx()) return true;
            else return false;
        }

        public bool VersionTabloRead()                      // команда в Tablo чтения версии ПО
        {
            tx[1] = 0x00; tx[2] = 0x02;
            tx[3] = 0x37; tx[4] = 0x30;
            if (BccTx()) return true;
            else return false;
        }

        public bool Wait1()                        // ожидание ответа
        {
            try
            {
                if (_serialPort.BytesToRead > 0)
                {
                    int _rxSize = _serialPort.BytesToRead;
                    _serialPort.Read(
                        rx,
                        rxSize,
                        _serialPort.BytesToRead
                        );
                    rxSize += _rxSize;
                    if(rxSize >= 3 & rx[0] == 3 & rx[1] == 3 & rx[2] ==3)
                    {
                        status = "BCC in Tablo er";
                    }
                    else
                    {
                        if (rxSize > 7 && rxSize >= (rx[1] << 8) + rx[2] + 5) // "ответ"
                        {
                            if (CheckBCC())
                            {
                                switch (rx[3])
                                {
                                    case 0x31:  //Diskret
                                        if (rx[4] == 0x30) status = "Diskret ok";
                                        //if (rx[5] == 0x4E) status = "нет карты";
                                        else status = "Diskret er"; // + rx[4].ToString("X2");

                                        statDiskret = 0;
                                        int a;
                                        for (int i = 0; i < 4; i++)
                                        {
                                            //if (rx[i + 5] != 0x39)          // в поле адреса - ошибка (плата не подключена, например)
                                            //{
                                            a = rx[i + 9] << (i * 4);
                                            //}
                                            //else a = 0;
                                            statDiskret += a;
                                        }
                                        //TabloPort1 = Convert.ToString(tx[5]);
                                        //TabloPort2 = Convert.ToString(tx[6]);
                                        //TabloPort3 = Convert.ToString(tx[7]);
                                        //TabloPort4 = Convert.ToString(tx[8]);
                                        TabloPort1 = Convert.ToString(tx[5]);
                                        TabloPort2 = Convert.ToString(tx[6]);
                                        TabloPort3 = Convert.ToString(tx[7]);
                                        TabloPort4 = Convert.ToString(tx[8]);
                                        break;

                                    case 0x32:  //чтения SignatureTablo
                                        signatureTablo = "";
                                        if (rx[4] == 0x30)
                                        {
                                            status = "SignatureTablo ok";
                                            for(int i = 0; i < 11; i++)
                                            {
                                                signatureTablo += rx[5 + i].ToString("X2");
                                                //signatureTablo += String.Format("{0:X2} ", rx[5 + i]);
                                            }
                                        }
                                        else status = "SignatureTablo er"; // + rx[4].ToString("X2");

                                       break;

                                    case 0x35:  //команда Display в Tablo выдачи цифр
                                        signatureTablo = "";
                                        if (rx[4] == 0x30)
                                        {
                                            status = "Display ok";
                                            //for (int i = 0; i < 11; i++)
                                            //{
                                            //    signatureTablo += rx[5 + i].ToString("X2");
                                            //    //signatureTablo += String.Format("{0:X2} ", rx[5 + i]);
                                            //}
                                        }
                                        else status = "Display er"; // + rx[4].ToString("X2");

                                        break;

                                    case 0x37:  //чтения версии ПО Tablo
                                        versionTablo = "";
                                        if (rx[4] == 0x30)
                                        {
                                            status = "VersionTablo ok";
                                            for (int i = 0; i < 11; i++)
                                            {
                                                versionTablo += (char) rx[5 + i];
                                            }
                                        }
                                        else status = "VersionTablo er"; // + rx[4].ToString("X2");

                                        break;

                                    default:
                                        status = "неопределен";
                                        break;
                                }

                            }
                            else status = "BCC er";
                        }
                        else
                        {
                            status = "ожидание";
                        }
                    }
                    
                }
                else
                {
                    status = "ожидание";
                }
                return true;
            }
            catch (Exception e)
            {
                status = "не отвечает";
                Console.WriteLine("Exception1: " + e.Message);
                return false;
            }
        }

        public bool WaitDiskret()
        {
            if (Wait1())
            {

                return true;
            }
            else return false;
        }

        private bool BccTx()
        {
            txSize = (tx[1] << 8) + tx[2] + 5;
            tx[0] = 0x02;
            tx[txSize - 2] = 0x03;
            tx[txSize - 1] = 0;
            for (int i = 0; i < txSize - 1; i++)
            {
                tx[txSize - 1] = (byte)(tx[txSize - 1] ^ tx[i]);
            }

            try
            {
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
                _serialPort.Write(tx, 0, txSize);
                rxSize = 0;
                status = "ожидание";
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception2: " + e.Message);
                return false;
            }
        }

        private bool CheckBCC()
        {
            byte BCC = 0;
            Length = (rx[1] << 8) + rx[2];
            for (int i = 0; i < Length + 4; i++)
            {
                BCC = (byte)(BCC ^ rx[i]);
            }
            if (BCC == rx[Length + 4]) return true;

            else return false;
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    //GetStatus();
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    Console.WriteLine("Возникло исключение: " + e.ToString());
                    return false;
                }
            }
            else
            {
                //GetStatus();
                return true;
            }
        }

    }

    /*// пример работы с SignatureTablo
                        //private string SignatureTablo;          // SignatureTablo текстовый 
                        //private DateTime LastSignatureTime = new DateTime(2000, 1, 1, 0, 0, 0, 0);     //  время последнего чтения SignatureTablo
                        //private int SignatureTime = 2;          // период опроса SignatureTablo в секундах
     
    // выбор команды                   
    DateTime dt = DateTime.Now;
                        tt = dt - LastSignatureTime;
                        int ttt = (int)tt.TotalSeconds;
                        if (ttt > SignatureTime)
                        {
                            // готовим для чтения SignatureTablo
                            if (Tablo.SignatureTabloRead())
                            {
                                LastSignatureTime = dt;
                            }
                            else
                            {
                                labelStatTablo.Text = "не отвечает";
                                labelComTablo.Text = nameTablo + "  не отвечает #2" + statTablo;
                                statTablo = "не отвечает";
                            }
                        }
                        else
                        {
                        if (Tablo.Diskret())
                            {
                            }
                        else
                            {
                                labelStatTablo.Text = "не отвечает";
                                labelComTablo.Text = nameTablo + "  не отвечает #2" + statTablo;
                                statTablo = "не отвечает";
                            }
                        }

    //ожидание ответа
    if (statTablo == "SignatureTablo ok")
                            {
                                SignatureTablo = Tablo.SignatureTablo;
                                // СОХРАНИТЬ В БАЗЕ

                                labelSignatureTablo.Text = "SignatureTablo - " + SignatureTablo;
                            }

    */
}
