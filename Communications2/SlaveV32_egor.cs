﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Protobuf;

namespace Communications
{
    public class SlaveV32
    {
        public bool TimerEnabled;
        public int TimerEnableCounter;
        public string LogString;
        public string ErrString;

        public int slave3_buffercount = 0;
        public List<byte[]> bufferlist;

        System.Timers.Timer tmrRX;

        System.Timers.Timer tmrDebug;

        string fn;

        //private const string getinfo = "\xc0\x82\x03\x00\x23"; //C0 82 03 00 23
        public string ClimateStateInfo { get; set; }

        private string status;                          // статус Slave
        public string command { get; set; }

        private int txSize = 0;
        private int rxSize = 0;
        private int Length = 0;
        private byte[] tx = new byte[256];              // передача в ридер 
        private byte[] rx = new byte[256];              // прием из ридер 

        SerialPort _serialPort;
        public string Status { get { return status; } }             // статус Slave контроллера

        public bool IsOpen { get; set; }

        public bool SlaveSuccess { get; set; }

        public bool BlockMove { get; set; }

        public bool ChangeCommand { get; set; } //будем использовать при изменении состояния!!!

        public bool BusyWrite { get; set; }

        //public bool BusyForAll { get; set; }

        public int SlaveEtap { get; set; }

        public struct SlaveInfoS
        {
            public string MemorySize;
            public string ID;
            public string Version;
            public string mName;
        }

        public SlaveInfoS SlaveInfo;

        public struct DiskretInput
        {
            public bool D1; //1
            public bool D2;
            public bool D3;
            public bool D4;
        }
        //структуры портов A и B
        public DiskretInput PortA_In;
        public DiskretInput PortB_In;

        public DiskretInput PortA_Out;
        public DiskretInput PortB_Out;

        public struct RelaysInOut
        {
            public bool R1;
            public bool R2;
            public bool R3;
            public bool R4;
            public bool R5;
            public bool R6;
            public bool R7;
            public bool R8;
            public bool R9;
            public bool R10;
        }

        public RelaysInOut RelaysStateIn;
        public RelaysInOut RelaysStateOut;

        public RelaysInOut PowerState;
        public RelaysInOut PowerErrors;

        public struct Climate
        {
            public float LocalT;
            public float OutsideSensorT;
            public float GigrometerT;
            public float Humidity;

            public bool HeaterStatus;
            public bool FanStatus;
            public bool AutoMode;
        }

        public Climate ClimateState;

        /*
          Первый флоат – Локальная температура(33 B3 96 43), 301.400,
Второй флоат – Температура внешнего датчика(33 A7 96 43), 301.30,
Третий флоат – Температура гигрометра(52 68 96 43), 300.815
Четверный флоат – Влажность относительная в процентах(3B 0C 19 42), 38.3 %

Статусный байт ST: 
•	ST[0] – состояние нагревателя(0 – выключен, 1 – включен)
•	ST[1] - состояние вентилятора(0 – выключен, 1 – включен)
•	ST[2] – режим работы(0 – ручное управление нагревателем и вентилятором, 1 – автоматическое)
         */

        /*
      static void Do_Crc8(uint8_t b, uint8_t *crc) {
for (uint8_t i = 0; i < 8; b = b >> 1, i++)
     if ((b ^ *crc) & 1)
         *crc = ((*crc ^ 0x18) >> 1) | 0x80;
     else
         *crc = (*crc >> 1) & ~0x80;
}
      */

        #region Log_Func

        public static void OpenLog()
        {
            //try
            //{
            DateTime startTime = DateTime.Now;
            string fname = Application.StartupPath + "\\log_slave3_.txt"; //+ Convert.ToString(startTime.Year) + Convert.ToString(startTime.Month) + Convert.ToString(startTime.Day) + "_" +
                                                                          //Convert.ToString(startTime.Hour) + "-" + Convert.ToString(startTime.Minute) + "-" + Convert.ToString(startTime.Second) + "-" + Convert.ToString(startTime.Millisecond) + ".txt";
            using (StreamWriter log = new StreamWriter(fname, true))
            {
                log.WriteLine(fname);
                log.WriteLine("---log started---");
            }
            //}
            //catch (Exception ex) { }

        }



        public void CloseLog()
        {
            string fname = Application.StartupPath + "\\log_slave3_.txt";
            using (StreamWriter log = new StreamWriter(fname, true))
            {
                log.WriteLine("---log ended---");
            }
        }


        public static void LWrite(string fname, string arg)
        {
            //string fname = Application.StartupPath + "\\log_slave3_.txt";
            if (fname != "")
            {
                using (StreamWriter log = new StreamWriter(fname, true))
                {
                    log.WriteLine(DateTime.Now.ToString("H:mm:ss:fff") + " " + arg);
                }
            }
        }

        #endregion

        #region static_func
        public static byte Do_Crc8(byte b, byte crc)
        {
            for (byte i = 0; i < 8; b = (byte)(b >> 1), i++)
            {
                if (((b ^ crc) & 1) == 1)
                    crc = (byte)(((crc ^ 0x18) >> 1) | 0x80);
                else
                    crc = (byte)((crc >> 1) & ~0x80);
            }
            return crc;
        }

        public static byte CalcCRC(params byte[] arg)
        {

            byte init = 0x00;

            byte crc = Do_Crc8(arg[0], init);

            byte twobytebefore = arg[1];
            //Debug.Print(SingleByteToBitString(twobytebefore));

            int twobyteafter_i = twobytebefore & 0x7f;

            byte twobyteafter = Convert.ToByte(twobyteafter_i);

            //Debug.Print(SingleByteToBitString(twobyteafter));

            crc = Do_Crc8(twobyteafter, crc);

            if (arg.Length > 2)
            {
                for (int i = 2; i < arg.Length; i++)
                {
                    crc = Do_Crc8(arg[i], crc);
                }
            }
            return crc;
        }

        public void TestCRC()
        {
            byte c1 = Do_Crc8(0x31, 0x00);

            Debug.Print("'1' " + SingleByteToString(c1));

            byte c2 = Do_Crc8(0x32, c1);

            Debug.Print("'12' " + SingleByteToString(c2));

            byte c3 = Do_Crc8(0x33, c2);

            Debug.Print("'123' " + SingleByteToString(c3));

            byte c4 = Do_Crc8(0x34, c3);

            Debug.Print("'1234' " + SingleByteToString(c4));

            byte c5 = Do_Crc8(0x35, c4);

            Debug.Print("'12345' " + SingleByteToString(c5));

            byte c6 = Do_Crc8(0x36, c5);

            Debug.Print("'123456' " + SingleByteToString(c6));

            byte c7 = Do_Crc8(0x37, c6);

            Debug.Print("'1234567' " + SingleByteToString(c7));

            byte c8 = Do_Crc8(0x38, c7);

            Debug.Print("'12345678' " + SingleByteToString(c8));

            byte c9 = Do_Crc8(0x39, c8);

            Debug.Print("'123456789' " + SingleByteToString(c9));

            //C0 82 03 00 
            byte[] tst1 = new byte[] { 0xc0, 0x82, 0x03, 0x00 };
            Debug.Print(SingleByteToString(CalcCRC(tst1)));

            //C0 82 06 00 
            /*
            byte[] tst2 = new byte[] { 0xc0, 0x82, 0x06, 0x00 };
            Debug.Print(SingleByteToString(CalcCRC(tst2)));

            //C0 81 06 02 00 00
            byte[] tst3 = new byte[] { 0xc0, 0x81, 0x06, 0x02,0x00,0x00 };
            Debug.Print(SingleByteToString(CalcCRC(tst3)));
            */

            //byte[] microtest = new byte[] { 0x31, 0x32, 0x33 };
            //Debug.Print(SingleByteToString(CalcCRC(microtest)));

            //C0 82 08 02 07 03 ======= C3
            byte[] tst4 = new byte[] { 0xc0, 0x82, 0x08, 0x02, 0x07, 0x03 };
            Debug.Print(SingleByteToString(CalcCRC(tst4)));
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string ByteArrayToStringX(byte[] ba)
        {
            return BitConverter.ToString(ba);
        }

        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }

        public static string SingleByteToString(byte ba)
        {
            byte[] b = new byte[1];
            b[0] = ba;

            return BitConverter.ToString(b);
        }

        public static string ByteArrayEncode(byte[] data)
        {
            char[] characters = data.Select(b => (char)b).ToArray();
            return new string(characters);
        }

        public static string SingleByteToBitString(byte ba)
        {
            return Convert.ToString(ba, 2).PadLeft(8, '0');
        }

        public static bool GetBit(byte b, int bitNumber) //Гетбит... (надобно еще сетбит...)
        {
            return (b & (1 << bitNumber)) != 0;
        }

        public static byte[] BytesStuffing(byte[] arg)
        {
            string mystr = ByteArrayToStringX(arg);

            mystr = mystr.Substring(3, mystr.Length - 3);

            //DB на DB + DD 'этот сначала
            mystr = mystr.Replace("-DB", "-DB-DD");

            //С0 на DB + DC
            mystr = mystr.Replace("-C0", "-DB-DC");

            mystr = mystr.Replace("-", "");

            mystr = "C0" + mystr;
            //Debug.Print(mystr);
            return StringToByteArray(mystr);
        }

        public static byte[] BackBytesStuffing(byte[] arg)
        {
            string mystr = ByteArrayToStringX(arg);

            mystr = mystr.Substring(3, mystr.Length - 3);

            //Ищем в полученном байты

            //DB на DB + DD 'этот сначала
            mystr = mystr.Replace("-DB-DD", "-DB");

            //С0 на DB + DC
            mystr = mystr.Replace("-DB-DC", "-C0");

            mystr = mystr.Replace("-", "");

            mystr = "C0" + mystr;
            //Debug.Print(mystr);
            return StringToByteArray(mystr);
        }

        public static int GetArrayCount(byte[] arg)
        {
            int cnt = 0;
            for (int i = 0; i < arg.Length; i++)
            {
                if (arg[i] == 0xc0)
                {
                    cnt++;
                }
            }
            return cnt;
        }

        public static List<byte[]> MergeBigArray(byte[] arg)
        {
            List<byte[]> bufferlist = new List<byte[]>();
            List<int> indexlist = new List<int>();

            for (int i = 0; i < arg.Length; i++)
            {
                if (arg[i] == 0xc0)
                {
                    indexlist.Add(i);
                }
            }

            int cnt = indexlist.Count;

            for (int i = 0; i < cnt; i++)
            {
                if (i < cnt - 1)
                {
                    byte[] bb = new byte[indexlist[i + 1] - indexlist[i]];
                    Buffer.BlockCopy(arg, indexlist[i], bb, 0, indexlist[i + 1] - indexlist[i]);
                    bufferlist.Add(bb);
                }
                else
                {
                    byte[] bb = new byte[arg.Length - indexlist[i]];
                    Buffer.BlockCopy(arg, indexlist[i], bb, 0, arg.Length - indexlist[i]);
                    bufferlist.Add(bb);
                }
            }
            return bufferlist;
        }

        #endregion

        public SlaveV32()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 256000;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 300;
            _serialPort.ReadTimeout = 300;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
            _serialPort.DataReceived += _serialPort_DataReceived;
            //_serialPort.ErrorReceived += _serialPort_ErrorReceived;

            tmrRX = new System.Timers.Timer();
            tmrRX.Interval = 250; //250...
            tmrRX.Elapsed += TmrRX_Elapsed;
            tmrRX.Enabled = true;
            tmrRX.AutoReset = true;

            TimerEnabled = false;
            TimerEnableCounter = 0;
            LogString = "";
            ErrString = "";

            tmrDebug = new System.Timers.Timer();
            tmrDebug.Interval = 300; //250...
            tmrDebug.Elapsed += TmrDebug_Elapsed;
            tmrDebug.Enabled = false;
            tmrDebug.AutoReset = false;

            //_serialPort.ReadTimeout = 200;
            //_serialPort.WriteTimeout = 200;
        }

        private void TmrDebug_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            //WriteToAllDiskrets("0000", "0000", "0000000000");
        }

        /*
        private void _serialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            //throw new NotImplementedException();
            ErrString = e.ToString();
        }
        */

        private void TmrRX_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            DateTime dt = DateTime.Now;
            string y = dt.Year.ToString();
            string m = dt.Month.ToString();
            string d = dt.Day.ToString();
            string h = dt.Hour.ToString();
            fn = y + "_" + m + "_" + d + "_" + h + "_log.txt";
            fn = Application.StartupPath + "\\slave_logs\\" + fn;
            if (!File.Exists(fn)) File.Create(fn);

            try
            {
                if (LogString != "")
                {
                    LWrite(fn, "in:" + Environment.NewLine);
                    LWrite(fn, LogString);
                    LogString = "";
                }

                if (ErrString != "")
                {
                    LWrite(fn, ErrString);
                    ErrString = "";
                }
            }
            catch (Exception ex) { }

            if (!TimerEnabled)
            {
                TimerEnableCounter++;
                if (TimerEnableCounter == 150)
                {
                    TimerEnabled = true;
                    TimerEnableCounter = 0;
                }
            }
            else
            {
                try
                {
                    //переоткрытие порта!
                    if (!_serialPort.IsOpen)
                    {
                        _serialPort.Open();
                    }
                }
                catch (Exception ex) { };
                /*
                try
                {
                    if (LogString != "")
                    {
                        LWrite(fn, "in:" + Environment.NewLine);
                        LWrite(fn, LogString);
                        LogString = "";
                    }

                    if (ErrString != "")
                    {
                        LWrite(fn, ErrString);
                        ErrString = "";
                    }
                }
                catch (Exception ex) { }
                */
            }
        }

        void ConvertData(byte[] arg)
        {
            byte[] decryptedbytes = BackBytesStuffing(arg);

            int databytesize = decryptedbytes.Length - 5;

            byte[] databytes = new byte[databytesize];

            for (int i = 4; i < decryptedbytes.Length - 1; i++)
            {
                databytes[i - 4] = decryptedbytes[i];
            }

            ResponseAll resp = new ResponseAll();

            try
            {
                resp = ResponseAll.Parser.ParseFrom(databytes);
            }
            catch (Exception ex)
            {

            }
            //входы а и б
            byte[] AportIn = BitConverter.GetBytes(resp.PORTAIDR);
            byte[] BportIn = BitConverter.GetBytes(resp.PORTBIDR);

            //Debug.Print(SingleByteToBitString(AportIn[0]));

            SetBits(AportIn[0], BportIn[0], 0);

            byte[] AportOut = BitConverter.GetBytes(resp.PORTAODR);
            byte[] BportOut = BitConverter.GetBytes(resp.PORTBODR);

            SetBits(AportOut[0], BportOut[0], 1);

            byte[] RelaysOut = BitConverter.GetBytes(resp.RELAYSODR);

            RelaysStateOut = new RelaysInOut();

            byte RelayOut1 = RelaysOut[0]; //Весь байт
            byte RelayOut2 = RelaysOut[1]; //2 бита

            if (GetBit(RelayOut1, 0)) { RelaysStateOut.R1 = true; }
            else { RelaysStateOut.R1 = false; }

            if (GetBit(RelayOut1, 1)) { RelaysStateOut.R2 = true; }
            else { RelaysStateOut.R2 = false; }

            if (GetBit(RelayOut1, 2)) { RelaysStateOut.R3 = true; }
            else { RelaysStateOut.R3 = false; }

            if (GetBit(RelayOut1, 3)) { RelaysStateOut.R4 = true; }
            else { RelaysStateOut.R4 = false; }

            if (GetBit(RelayOut1, 4)) { RelaysStateOut.R5 = true; }
            else { RelaysStateOut.R5 = false; }

            if (GetBit(RelayOut1, 5)) { RelaysStateOut.R6 = true; }
            else { RelaysStateOut.R5 = false; }

            if (GetBit(RelayOut1, 6)) { RelaysStateOut.R7 = true; }
            else { RelaysStateOut.R6 = false; }

            if (GetBit(RelayOut1, 7)) { RelaysStateOut.R8 = true; }
            else { RelaysStateOut.R8 = false; }

            if (GetBit(RelayOut2, 0)) { RelaysStateOut.R9 = true; }
            else { RelaysStateOut.R9 = false; }

            if (GetBit(RelayOut2, 1)) { RelaysStateOut.R10 = true; }
            else { RelaysStateOut.R10 = false; }

            byte[] RelaysIn = BitConverter.GetBytes(resp.RELAYSIDR);

            RelaysStateIn = new RelaysInOut();

            byte RelayIn1 = RelaysIn[0]; //Весь байт
            byte RelayIn2 = RelaysIn[1]; //2 бита

            if (GetBit(RelayIn1, 0)) { RelaysStateIn.R1 = true; }
            else { RelaysStateIn.R1 = false; }

            if (GetBit(RelayIn1, 1)) { RelaysStateIn.R2 = true; }
            else { RelaysStateIn.R2 = false; }

            if (GetBit(RelayIn1, 2)) { RelaysStateIn.R3 = true; }
            else { RelaysStateIn.R3 = false; }

            if (GetBit(RelayIn1, 3)) { RelaysStateIn.R4 = true; }
            else { RelaysStateIn.R4 = false; }

            if (GetBit(RelayIn1, 4)) { RelaysStateIn.R5 = true; }
            else { RelaysStateIn.R5 = false; }

            if (GetBit(RelayIn1, 5)) { RelaysStateIn.R6 = true; }
            else { RelaysStateIn.R6 = false; }

            if (GetBit(RelayIn1, 6)) { RelaysStateIn.R7 = true; }
            else { RelaysStateIn.R7 = false; }

            if (GetBit(RelayIn1, 7)) { RelaysStateIn.R8 = true; }
            else { RelaysStateIn.R8 = false; }

            if (GetBit(RelayIn2, 0)) { RelaysStateIn.R9 = true; }
            else { RelaysStateIn.R9 = false; }

            if (GetBit(RelayIn2, 1)) { RelaysStateIn.R10 = true; }
            else { RelaysStateIn.R10 = false; }
            Thread.Sleep(50);
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //LWrite(fn, command);

            SlaveSuccess = true;
            int bytescount = 0;
            switch (command)
            {

                case "getall": //все здесь...
                    ChangeCommand = true; //???

                    bytescount = _serialPort.BytesToRead;
                    byte[] getallbytes_s = new byte[bytescount];
                    _serialPort.Read(getallbytes_s, 0, bytescount);
                    LogString = ByteArrayToStringX(getallbytes_s);
                    if (bytescount == 5) //точно ответка
                    {
                        //byte[] getallbytes1 = new byte[bytescount];
                        //LogString = "ответка: " + ByteArrayToStringX(getallbytes1);
                        _serialPort.DiscardInBuffer();
                        //_serialPort.DiscardOutBuffer(); //пробуем!!
                        break;
                    }
                    if (bytescount == 10 && getallbytes_s[0] == 0xc0 && getallbytes_s[1] == 0x81 && getallbytes_s[2] == 0x54 && getallbytes_s[3] == 0x00 && getallbytes_s[4] == 0xde &&
                        getallbytes_s[5] == 0xc0 && getallbytes_s[6] == 0x81 && getallbytes_s[7] == 0x54 && getallbytes_s[8] == 0x00 && getallbytes_s[9] == 0xde)
                    {
                        _serialPort.DiscardInBuffer();
                        //_serialPort.DiscardOutBuffer(); //пробуем!!
                        break;
                    }
                    if (bytescount == 15 && getallbytes_s[0] == 0xc0 && getallbytes_s[1] == 0x81 && getallbytes_s[2] == 0x54 && getallbytes_s[3] == 0x00 && getallbytes_s[4] == 0xde &&
                        getallbytes_s[5] == 0xc0 && getallbytes_s[6] == 0x81 && getallbytes_s[7] == 0x54 && getallbytes_s[8] == 0x00 && getallbytes_s[9] == 0xde
                        && getallbytes_s[10] == 0xc0 && getallbytes_s[11] == 0x81 && getallbytes_s[12] == 0x54 && getallbytes_s[13] == 0x00 && getallbytes_s[14] == 0xde)
                    {
                        _serialPort.DiscardInBuffer();
                        //_serialPort.DiscardOutBuffer(); //пробуем!!
                        break;
                    }

                    //Debug.Print(bytescount.ToString());
                    if (bytescount > 5)
                    {
                        //byte[] getallbytes = new byte[bytescount];

                        //_serialPort.Read(getallbytes, 0, bytescount);
                        //LWrite(fn, ByteArrayToStringX(getallbytes));
                        //LogString = ByteArrayToStringX(getallbytes);
                        //вычленяем ответку. она может быть в начале и в конце
                        //C0-81-54-00-DE
                        byte[] getallbytes;
                        //режем с начала
                        int glb_len = getallbytes_s.Length;
                        if (getallbytes_s[0] == 0xc0 && getallbytes_s[1] == 0x81 && getallbytes_s[2] == 0x54 && getallbytes_s[3] == 0x00 && getallbytes_s[4] == 0xde)
                        {
                            getallbytes = new byte[bytescount - 5];
                            for (int i = 5; i < getallbytes_s.Length; i++)
                            {
                                getallbytes[i - 5] = getallbytes_s[i];
                            }
                        }
                        //режем с конца
                        else if (getallbytes_s[glb_len-1]== 0xde && getallbytes_s[glb_len-2]==0x00 && getallbytes_s[glb_len-3]==0x54 && getallbytes_s[glb_len-4]==0x81 && getallbytes_s[glb_len-5]==0xc0)
                        {
                            getallbytes = new byte[bytescount - 5];
                            for (int i = 0; i < getallbytes_s.Length-5; i++)
                            {
                                getallbytes[i] = getallbytes_s[i];
                            }
                        }
                        else
                        {
                            getallbytes = new byte[bytescount];
                            for (int i = 0; i < getallbytes_s.Length; i++)
                            {
                                getallbytes[i] = getallbytes_s[i];
                            }
                        }

                        int pack_count = GetArrayCount(getallbytes);

                        //Debug.Print("All Answer");

                        //Трансформация посылки - ищем начало с конца C0
                        if (pack_count > 1)
                        {
                            //LWrite(fn, "KURWA");
                            bufferlist = MergeBigArray(getallbytes);
                            foreach (byte[] b in bufferlist)
                            {
                                ConvertData(b);
                            }
                        }
                        else
                        {
                            byte[] decryptedbytes = BackBytesStuffing(getallbytes);

                            //Debug.Print(ByteArrayToStringX(decryptedbytes));

                            int databytesize = decryptedbytes.Length - 5;

                            byte[] databytes = new byte[databytesize];

                            for (int i = 4; i < decryptedbytes.Length - 1; i++)
                            {
                                databytes[i - 4] = decryptedbytes[i];
                            }

                            //Debug.Print("Data Bytes:");

                            //Debug.Print(ByteArrayToStringX(databytes));
                            //LWrite(fn, ByteArrayToStringX(databytes));
                            //мутик
                            ResponseAll resp = new ResponseAll();

                            try
                            {
                                resp = ResponseAll.Parser.ParseFrom(databytes);
                                //resp = ResponseAll.Parser.ParseFrom(new byte[] {0x96,0x78,0x54,0x67 });
                            }
                            catch (Exception ex)
                            {
                                //LWrite(fn, ex.Message);
                                break;
                            }

                            //входы а и б
                            byte[] AportIn = BitConverter.GetBytes(resp.PORTAIDR);
                            byte[] BportIn = BitConverter.GetBytes(resp.PORTBIDR);

                            //Debug.Print(SingleByteToBitString(AportIn[0]));

                            SetBits(AportIn[0], BportIn[0], 0);

                            //LWrite(fn, "A_" + PortA_In.D1.ToString() + PortA_In.D2.ToString() + PortA_In.D3.ToString() + PortA_In.D4.ToString());
                            //LWrite(fn, "B_" + PortB_In.D1.ToString() + PortB_In.D2.ToString() + PortB_In.D3.ToString() + PortB_In.D4.ToString());
                            LogString += Environment.NewLine + "A_In: " + PortA_In.D1.ToString() + PortA_In.D2.ToString() + PortA_In.D3.ToString() + PortA_In.D4.ToString();
                            LogString += Environment.NewLine + "B_In: " + PortB_In.D1.ToString() + PortB_In.D2.ToString() + PortB_In.D3.ToString() + PortB_In.D4.ToString();
                            //LogString += Environment.NewLine + "out:";
                            //Debug.Print(PortB_In.D1.ToString());
                            //Debug.Print(PortB_In.D2.ToString());
                            //Debug.Print(PortB_In.D3.ToString());
                            //Debug.Print(PortB_In.D4.ToString());

                            //выходы а и б

                            //закодить
                            //BlockMove = true; //!!!
                            if (BlockMove)
                            {
                                //LWrite(fn, "-");
                                //Debug.Print("ReRead Diskrets!");

                                byte[] AportOut = BitConverter.GetBytes(resp.PORTAODR);
                                byte[] BportOut = BitConverter.GetBytes(resp.PORTBODR);

                                SetBits(AportOut[0], BportOut[0], 1);

                                //LWrite(fn, "drv_A_" + PortA_Out.D1.ToString() + PortA_Out.D2.ToString() + PortA_Out.D3.ToString() + PortA_Out.D4.ToString());
                                //LWrite(fn, "drv_B_" + PortB_Out.D1.ToString() + PortB_Out.D2.ToString() + PortB_Out.D3.ToString() + PortB_Out.D4.ToString());

                                //LogString += Environment.NewLine + "A_out: " + PortA_Out.D1.ToString() + PortA_Out.D2.ToString() + PortA_Out.D3.ToString() + PortA_Out.D4.ToString();
                                //LogString += Environment.NewLine + "B_out: " + PortB_Out.D1.ToString() + PortB_Out.D2.ToString() + PortB_Out.D3.ToString() + PortB_Out.D4.ToString();
                                byte[] RelaysOut = BitConverter.GetBytes(resp.RELAYSODR);

                                RelaysStateOut = new RelaysInOut();

                                byte RelayOut1 = RelaysOut[0]; //Весь байт
                                byte RelayOut2 = RelaysOut[1]; //2 бита

                                if (GetBit(RelayOut1, 0)) { RelaysStateOut.R1 = true; }
                                else { RelaysStateOut.R1 = false; }

                                if (GetBit(RelayOut1, 1)) { RelaysStateOut.R2 = true; }
                                else { RelaysStateOut.R2 = false; }

                                if (GetBit(RelayOut1, 2)) { RelaysStateOut.R3 = true; }
                                else { RelaysStateOut.R3 = false; }

                                if (GetBit(RelayOut1, 3)) { RelaysStateOut.R4 = true; }
                                else { RelaysStateOut.R4 = false; }

                                if (GetBit(RelayOut1, 4)) { RelaysStateOut.R5 = true; }
                                else { RelaysStateOut.R5 = false; }

                                if (GetBit(RelayOut1, 5)) { RelaysStateOut.R6 = true; }
                                else { RelaysStateOut.R5 = false; }

                                if (GetBit(RelayOut1, 6)) { RelaysStateOut.R7 = true; }
                                else { RelaysStateOut.R6 = false; }

                                if (GetBit(RelayOut1, 7)) { RelaysStateOut.R8 = true; }
                                else { RelaysStateOut.R8 = false; }

                                if (GetBit(RelayOut2, 0)) { RelaysStateOut.R9 = true; }
                                else { RelaysStateOut.R9 = false; }

                                if (GetBit(RelayOut2, 1)) { RelaysStateOut.R10 = true; }
                                else { RelaysStateOut.R10 = false; }
                            }
                            byte[] RelaysIn = BitConverter.GetBytes(resp.RELAYSIDR);

                            RelaysStateIn = new RelaysInOut();

                            byte RelayIn1 = RelaysIn[0]; //Весь байт
                            byte RelayIn2 = RelaysIn[1]; //2 бита

                            if (GetBit(RelayIn1, 0)) { RelaysStateIn.R1 = true; }
                            else { RelaysStateIn.R1 = false; }

                            if (GetBit(RelayIn1, 1)) { RelaysStateIn.R2 = true; }
                            else { RelaysStateIn.R2 = false; }

                            if (GetBit(RelayIn1, 2)) { RelaysStateIn.R3 = true; }
                            else { RelaysStateIn.R3 = false; }

                            if (GetBit(RelayIn1, 3)) { RelaysStateIn.R4 = true; }
                            else { RelaysStateIn.R4 = false; }

                            if (GetBit(RelayIn1, 4)) { RelaysStateIn.R5 = true; }
                            else { RelaysStateIn.R5 = false; }

                            if (GetBit(RelayIn1, 5)) { RelaysStateIn.R6 = true; }
                            else { RelaysStateIn.R6 = false; }

                            if (GetBit(RelayIn1, 6)) { RelaysStateIn.R7 = true; }
                            else { RelaysStateIn.R7 = false; }

                            if (GetBit(RelayIn1, 7)) { RelaysStateIn.R8 = true; }
                            else { RelaysStateIn.R8 = false; }

                            if (GetBit(RelayIn2, 0)) { RelaysStateIn.R9 = true; }
                            else { RelaysStateIn.R9 = false; }

                            if (GetBit(RelayIn2, 1)) { RelaysStateIn.R10 = true; }
                            else { RelaysStateIn.R10 = false; }

                            //изм состояния???
                            //BlockMove = false;
                            //if (ChangeCommand)
                            //{
                            //SlaveEtap = 2; //спорно
                            //ChangeCommand = false;
                            //}
                            //else
                            //{
                            //    SlaveEtap = 0;
                            //    ChangeCommand = false;
                            //}
                            //}
                            //catch (Exception ex) { }
                        }
                    }
                    break;

                case "info":
                    bytescount = _serialPort.BytesToRead;

                    byte[] infobytes = new byte[bytescount];
                    _serialPort.Read(infobytes, 0, bytescount);

                    SlaveInfo = new SlaveInfoS();

                    byte[] slaveinfo_bytememory = new byte[2];
                    for (int i = 0; i < 2; i++)
                    {
                        slaveinfo_bytememory[i] = infobytes[i + 4];
                    }
                    SlaveInfo.MemorySize = ByteArrayToString(slaveinfo_bytememory);

                    byte[] slaveinfo_id = new byte[12];
                    for (int i = 0; i < 12; i++)
                    {
                        slaveinfo_id[i] = infobytes[i + 6];
                    }
                    SlaveInfo.ID = ByteArrayToString(slaveinfo_id);

                    byte[] slaveinfo_ver = new byte[4];
                    for (int i = 0; i < 4; i++)
                    {
                        slaveinfo_ver[i] = infobytes[i + 18];
                    }
                    SlaveInfo.Version = Encoding.Default.GetString(slaveinfo_ver);

                    byte[] slaveinfo_arm = new byte[9];
                    for (int i = 0; i < 9; i++)
                    {
                        slaveinfo_arm[i] = infobytes[i + 22];
                    }
                    SlaveInfo.mName = Encoding.Default.GetString(slaveinfo_arm); //ByteArrayEncode(slaveinfo_arm);

                    //Debug.Print(SlaveInfo.ID);
                    //Debug.Print(SlaveInfo.MemorySize);
                    //Debug.Print(SlaveInfo.Version);
                    //Debug.Print(SlaveInfo.mName);

                    break;
                case "write_diskret_all":
                    bytescount = _serialPort.BytesToRead;
                    //Debug.Print(bytescount.ToString());
                    byte[] writedbytes8 = new byte[bytescount];
                    _serialPort.Read(writedbytes8, 0, bytescount);
                    //Debug.Print(Encoding.Default.GetString(writedbytes2));
                    Debug.Print("write_diskret_all");

                    //LWrite("write: " + ByteArrayToStringX(writedbytes8));

                    BusyWrite = false;

                    break;
                case "getpower_out":
                    bytescount = _serialPort.BytesToRead;

                    byte[] infobytes6 = new byte[bytescount];
                    _serialPort.Read(infobytes6, 0, bytescount);

                    RelaysStateIn = new RelaysInOut();

                    byte PowerByte1 = infobytes6[4]; //2 бита
                    byte PowerByte2 = infobytes6[5]; //весь байт

                    if (GetBit(PowerByte1, 1)) { PowerState.R1 = true; }
                    if (GetBit(PowerByte1, 0)) { PowerState.R2 = true; }

                    if (GetBit(PowerByte2, 7)) { PowerState.R3 = true; }
                    if (GetBit(PowerByte2, 6)) { PowerState.R4 = true; }
                    if (GetBit(PowerByte2, 5)) { PowerState.R5 = true; }
                    if (GetBit(PowerByte2, 4)) { PowerState.R6 = true; }
                    if (GetBit(PowerByte2, 3)) { PowerState.R7 = true; }
                    if (GetBit(PowerByte2, 2)) { PowerState.R8 = true; }
                    if (GetBit(PowerByte2, 1)) { PowerState.R9 = true; }
                    if (GetBit(PowerByte2, 0)) { PowerState.R10 = true; }

                    Debug.Print(SingleByteToBitString(PowerByte1));
                    Debug.Print(SingleByteToBitString(PowerByte2));
                    break;
                case "getpower_err":
                    bytescount = _serialPort.BytesToRead;

                    byte[] infobytes7 = new byte[bytescount];
                    _serialPort.Read(infobytes7, 0, bytescount);

                    RelaysStateIn = new RelaysInOut();

                    byte PowerByte1e = infobytes7[4]; //2 бита
                    byte PowerByte2e = infobytes7[5]; //весь байт

                    if (GetBit(PowerByte1e, 1)) { PowerErrors.R1 = true; }
                    if (GetBit(PowerByte1e, 0)) { PowerErrors.R2 = true; }

                    if (GetBit(PowerByte2e, 7)) { PowerErrors.R3 = true; }
                    if (GetBit(PowerByte2e, 6)) { PowerErrors.R4 = true; }
                    if (GetBit(PowerByte2e, 5)) { PowerErrors.R5 = true; }
                    if (GetBit(PowerByte2e, 4)) { PowerErrors.R6 = true; }
                    if (GetBit(PowerByte2e, 3)) { PowerErrors.R7 = true; }
                    if (GetBit(PowerByte2e, 2)) { PowerErrors.R8 = true; }
                    if (GetBit(PowerByte2e, 1)) { PowerErrors.R9 = true; }
                    if (GetBit(PowerByte2e, 0)) { PowerErrors.R10 = true; }

                    Debug.Print(SingleByteToBitString(PowerByte1e));
                    Debug.Print(SingleByteToBitString(PowerByte2e));
                    break;

                case "write_diskret_power":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] writedbytes3 = new byte[bytescount];
                    _serialPort.Read(writedbytes3, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(writedbytes3));
                    break;

                case "setpower_1":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] setpbytes1 = new byte[bytescount];
                    _serialPort.Read(setpbytes1, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(setpbytes1)); //byte[] slaveinfo_arm = new byte[9];
                    break;
                case "setpower_0":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] setpbytes2 = new byte[bytescount];
                    _serialPort.Read(setpbytes2, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(setpbytes2)); //byte[] slaveinfo_arm = new byte[9];
                    break;
                case "getclimate": //флоаты
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] climatebytes = new byte[bytescount];
                    _serialPort.Read(climatebytes, 0, bytescount);

                    Debug.Print(ByteArrayToStringX(climatebytes));

                    ClimateState = new Climate();

                    byte[] firstfloat = new byte[4];
                    for (int i = 0; i < 4; i++)
                    {
                        firstfloat[i] = climatebytes[i + 4];
                    }
                    float f1 = BitConverter.ToSingle(firstfloat, 0);
                    ClimateState.LocalT = f1 - 273;
                    Debug.Print(ClimateState.LocalT.ToString());

                    byte[] secondfloat = new byte[4];
                    for (int i = 0; i < 4; i++)
                    {
                        secondfloat[i] = climatebytes[i + 8];
                    }
                    float f2 = BitConverter.ToSingle(secondfloat, 0);
                    ClimateState.OutsideSensorT = f2 - 273;
                    Debug.Print(ClimateState.OutsideSensorT.ToString());

                    byte[] thirdfloat = new byte[4];
                    for (int i = 0; i < 4; i++)
                    {
                        thirdfloat[i] = climatebytes[i + 12];
                    }
                    float f3 = BitConverter.ToSingle(thirdfloat, 0);
                    ClimateState.GigrometerT = f3 - 273;
                    Debug.Print(ClimateState.GigrometerT.ToString());

                    byte[] fourthfloat = new byte[4];
                    for (int i = 0; i < 4; i++)
                    {
                        fourthfloat[i] = climatebytes[i + 16];
                    }
                    float f4 = BitConverter.ToSingle(fourthfloat, 0);
                    ClimateState.Humidity = f3;
                    Debug.Print(ClimateState.Humidity.ToString());

                    byte statebyte = climatebytes[20];
                    Debug.Print(SingleByteToBitString(statebyte));

                    ClimateState.HeaterStatus = GetBit(statebyte, 0);
                    ClimateState.FanStatus = GetBit(statebyte, 1);
                    ClimateState.AutoMode = GetBit(statebyte, 2);
                    break;

                case "getclimate2":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] climatebytes2 = new byte[bytescount];
                    _serialPort.Read(climatebytes2, 0, bytescount);

                    Debug.Print(ByteArrayToStringX(climatebytes2));

                    byte[] decrypted_climate_bytes2 = BackBytesStuffing(climatebytes2);

                    //string ClimateAnswerTest = ByteArrayToStringX(climatebytes2);
                    int climate_bytes2_size = decrypted_climate_bytes2.Length - 5;

                    byte[] climate_bytes2 = new byte[climate_bytes2_size];

                    for (int i = 4; i < decrypted_climate_bytes2.Length - 1; i++)
                    {
                        climate_bytes2[i - 4] = decrypted_climate_bytes2[i];
                    }

                    ClimateStateInfo = "";
                    ClimateResponse climateresp;

                    climateresp = ClimateResponse.Parser.ParseFrom(climate_bytes2);

                    ClimateMode cl_mode = climateresp.Mode;

                    ClimateStateInfo += "Режим: " + cl_mode.ToString() + Environment.NewLine;
                    ClimateStateInfo += "Вентилятор: " + climateresp.CoolerEn.ToString() + Environment.NewLine;
                    ClimateStateInfo += "Печка: " + climateresp.HeaterEn.ToString() + Environment.NewLine;
                    ClimateStateInfo += "t внутр. " + Math.Round(climateresp.TemperatureLocal - 273, 2).ToString() + Environment.NewLine;
                    ClimateStateInfo += "t внешн. " + Math.Round(climateresp.TemperatureExternal - 273, 2).ToString() + Environment.NewLine;
                    ClimateStateInfo += "t датчик влажн. " + Math.Round(climateresp.TemperatureAlternate - 273, 2).ToString() + Environment.NewLine;
                    ClimateStateInfo += "Влажность: " + Math.Round(climateresp.Humidity, 2).ToString() + "%" + Environment.NewLine;
                    TemperatureChannel t_channel = climateresp.Channel;
                    ClimateStateInfo += "Канал регулировки t: " + t_channel.ToString() + Environment.NewLine;
                    ClimateStateInfo += "Порог t_max: " + Math.Round(climateresp.ThresholdTHigh, 2) + Environment.NewLine;
                    ClimateStateInfo += "Порог t_min: " + Math.Round(climateresp.ThresholdTLow, 2) + Environment.NewLine;
                    ClimateStateInfo += "Порог влажности: " + Math.Round(climateresp.ThresholdHumidity, 2) + "%" + Environment.NewLine;

                    Debug.Print(ClimateStateInfo);
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    break;

                case "setclimate":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] setclimate = new byte[bytescount];
                    _serialPort.Read(setclimate, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(setclimate)); //byte[] slaveinfo_arm = new byte[9];
                    break;
                case "setclimate2":
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    break;

                case "getwiegand":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] wiegandbytes = new byte[bytescount];
                    _serialPort.Read(wiegandbytes, 0, bytescount);

                    Debug.Print(ByteArrayToStringX(wiegandbytes));

                    //Debug.Print(Encoding.Default.GetString(wiegandbytes)); //byte[] slaveinfo_arm = new byte[9];
                    break;

                case "blasting":
                    bytescount = _serialPort.BytesToRead;
                    //Debug.Print(bytescount.ToString());
                    byte[] pulsebytes = new byte[bytescount];

                    _serialPort.Read(pulsebytes, 0, bytescount);
                    //Debug.Print(ByteArrayToStringX(pulsebytes));
                    //BusyPort = false;
                    break;
                default:
                    break;
            }
            _serialPort.DiscardInBuffer();
            //_serialPort.DiscardOutBuffer();
        }

        public void SetBits(byte pA, byte pB, byte InOut) //0 or 1
        {
            if (InOut == 0)
            {
                PortA_In = new DiskretInput();
                if (GetBit(pA, 0)) { PortA_In.D1 = true; }
                if (GetBit(pA, 1)) { PortA_In.D2 = true; }
                if (GetBit(pA, 2)) { PortA_In.D3 = true; }
                if (GetBit(pA, 3)) { PortA_In.D4 = true; }

                PortB_In = new DiskretInput();
                if (GetBit(pB, 0)) { PortB_In.D1 = true; }
                if (GetBit(pB, 1)) { PortB_In.D2 = true; }
                if (GetBit(pB, 2)) { PortB_In.D3 = true; }
                if (GetBit(pB, 3)) { PortB_In.D4 = true; }
            }
            else if (InOut == 1)
            {
                PortA_Out = new DiskretInput();
                if (GetBit(pA, 0)) { PortA_Out.D1 = true; }
                if (GetBit(pA, 1)) { PortA_Out.D2 = true; }
                if (GetBit(pA, 2)) { PortA_Out.D3 = true; }
                if (GetBit(pA, 3)) { PortA_Out.D4 = true; }

                PortB_Out = new DiskretInput();
                if (GetBit(pB, 0)) { PortB_Out.D1 = true; }
                if (GetBit(pB, 1)) { PortB_Out.D2 = true; }
                if (GetBit(pB, 2)) { PortB_Out.D3 = true; }
                if (GetBit(pB, 3)) { PortB_Out.D4 = true; }
            }
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    IsOpen = true;
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    Console.WriteLine("Возникло исключение: " + e.ToString());
                    IsOpen = false;
                    return false;
                }
            }
            else
            {
                IsOpen = true;
                return true;
            }
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    Console.WriteLine("Возникло исключение: " + e.ToString());
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public void GetSlaveInfo()                      // команда в Slave чтения SignatureSlave
        {
            command = "info";
            if (_serialPort.IsOpen)
            {
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x03, 0x00, 0x23 };

                _serialPort.Write(bytes, 0, 5);
            }
        }

        public void SetBootLoaderMode() //в конце
        {

        }

        //Запись в дискрет порта H (пока не будем использовать)
        public void WriteToAllDiskrets(string maska, string maskb, string maskh)
        {
            /*
           Команда OUTPUT_ALL_ODRW (0x54), OUTPUT_ALL_SET (0x55), OUTPUT_ALL_RESET (0x56), OUTPUT_ALL_TOGGLE(0x57)
Описание
Записать выходное состояние PORTA, PORTB и Relays. По сути, соединение команд PORTS* и  RELAYS* в одну.
*_ODRW – установка значения; _SET – установка в 1; _RESET – установка в 0; _TOGGLE – переключение выхода.
Запрос
C0 82 54 04 01 02 00 84 61
4 значащих байта (01 02 00 84). 01 – PORTA, 02 – PORTB, 0084 – RELAYS. Позиции байт такие же как в материнских командах.


             */
            //???
            //_serialPort.DiscardInBuffer();
            //контролить ответ!
            try
            {
                if (_serialPort.IsOpen)
                {
                    //command = "write_diskret_all";

                    byte aport = Convert.ToByte("0000" + maska, 2);
                    byte bport = Convert.ToByte("0000" + maskb, 2);


                    string maskh1 = maskh.Substring(0, 2);
                    string maskh2 = maskh.Substring(2, 8);
                    LWrite(fn, "Out A_Write: " + maska + " Out B_Write: " + maskb + " Out H_Write: " + maskh);

                    byte byte1h = Convert.ToByte("000000" + maskh1, 2);
                    byte byte2h = Convert.ToByte(maskh2, 2);

                    byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x54, 0x04, aport, bport, byte1h, byte2h };

                    byte crc = CalcCRC(bytesbefore);

                    byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x54, 0x04, aport, bport, byte1h, byte2h, crc };

                    byte[] bytesafter = BytesStuffing(byteswithcrc);

                    LWrite(fn, ByteArrayToStringX(bytesafter));

                    _serialPort.Write(bytesafter, 0, bytesafter.Length);
                }
            }
            catch (Exception ex)
            {
                //LWrite(fn, "Exception Write!");

            }
        }

        public void GetPowerErrors()
        {
            /*
            Команда POWERS_IDR (0x11)
Описание
Считать значение ошибок силовых выходов.
Запрос
C0 82 11 00 5E
Ответ
C0 81 11 02 00 00 AA
2 байта, всегда нули, не реализовано.



             */

            if (_serialPort.IsOpen)
            {
                command = "getpower_err";
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x11, 0x00, 0x5e };

                _serialPort.Write(bytes, 0, 5);
            }
        }

        public void GetPowerOut()
        {
            /*
             Команда POWERS_ODRR (0x12)
Описание
Считать состояние силовых выходов.
Запрос
C0 82 12 00 0B
Ответ
C0 81 12 02 02 A7 9F
2 байта 02 A7. Состояние силовых выходов: 0 – отключен, 1 – включен. Значащими битами являются 10 бит (10_1010_0111)


             */

            if (_serialPort.IsOpen)
            {
                command = "getpower_out";
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x12, 0x00, 0x0b };

                _serialPort.Write(bytes, 0, 5);
            }

        }


        //Устанавливает силовой разъем в 1
        public void SetPowerTo1(int diskret)
        {
            /*
Команда POWERS_SET (0x14)
Описание
Установить Силовые выходы в включенное состояние.
Запрос
C0 82 14 02 02 A7 D8
2 байта данных (02 А7). Перевести силовые выходы в включенное состояние. Значащими битами являются 10 бит (10_1010_0111).
Ответ
POWERS_SET в случае успешности
ERR – во всех остальных

             */

            if (_serialPort.IsOpen)
            {
                command = "setpower_1";

                //byte managebyte = 0x00;

                byte byte1 = 0x00;
                byte byte2 = 0x00;

                if (diskret == 1)
                {
                    byte1 = 0x02; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 2)
                {
                    byte1 = 0x01; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 3)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x80;
                }
                else if (diskret == 4)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x40;
                }
                else if (diskret == 5)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x20;
                }
                else if (diskret == 6)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x10;
                }
                else if (diskret == 7)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x08;
                }
                else if (diskret == 8)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x04;
                }
                else if (diskret == 9)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x02;
                }
                else if (diskret == 10)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x01;
                }

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x14, 0x02, byte1, byte2 };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x14, 0x02, byte1, byte2, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                //Debug.Print(ByteArrayToString(NewBytes));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        //Устанавливает силовой разъем в 0
        public void SetPowerTo0(int diskret)
        {
            /*
Команда POWERS_RESET (0x15)
Описание
Установить Силовые выходы в отключенное состояние.
Запрос
C0 82 15 02 02 A7 57
2 байта данных (02 А7). Перевести силовые выходы в отключенное состояние. Значащими битами являются 10 бит (10_1010_0111).
Ответ
POWERS_RESET в случае успешности
ERR – во всех остальных


             */

            if (_serialPort.IsOpen)
            {
                command = "setpower_1";

                //byte managebyte = 0x00;

                byte byte1 = 0x00;
                byte byte2 = 0x00;

                if (diskret == 1)
                {
                    byte1 = 0x02; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 2)
                {
                    byte1 = 0x01; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 3)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x80;
                }
                else if (diskret == 4)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x40;
                }
                else if (diskret == 5)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x20;
                }
                else if (diskret == 6)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x10;
                }
                else if (diskret == 7)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x08;
                }
                else if (diskret == 8)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x04;
                }
                else if (diskret == 9)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x02;
                }
                else if (diskret == 10)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x01;
                }

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x15, 0x02, byte1, byte2 };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x15, 0x02, byte1, byte2, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        //Запись в дискрет порта A,B (пока не будем использовать)
        public void WriteToDiskretPower(string mask)
        {
            /*
           Команда POWERS_ODRW (0x13)
Описание
Установить Силовые выходы в соответствующее состояние.
Запрос
C0 82 13 02 02 A7 5E
2 байта данных (02 А7). Перевести силовые выходы в указанное состояние: 0 – отключен, 1 – включен. Значащими битами являются 10 бит (10_1010_0111).
Ответ
POWERS_ODRW в случае успешности
ERR – во всех остальных


             */

            if (_serialPort.IsOpen)
            {
                command = "write_diskret_power";

                string mask1 = mask.Substring(0, 2);
                string mask2 = mask.Substring(2, 8);

                byte byte1 = Convert.ToByte("000000" + mask1, 2);
                byte byte2 = Convert.ToByte(mask2, 2);



                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x13, 0x02, byte1, byte2 };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x13, 0x02, byte1, byte2, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        //Slave Functions:
        public void GetClimate()
        {
            /*
             Команда CLIMATE_GET (0x16)
Описание
Запрос состояние контроллера климата. Считывание температур, влажности, состояние печки, вентилятора…
Запрос
C0 82 16 00 30
Ответ
C0 81 16 11 33 B3 96 43 33 A7 96 43 52 68 96 43 3B 0C 19 42 00 83
В полезной нагрузке 4 флоата по 32 бита (little-endian) и 1 байт статус. Температура в Кельвинах
Первый флоат – Локальная температура (33 B3 96 43), 301.400,
Второй флоат – Температура внешнего датчика (33 A7 96 43), 301.30,
Третий флоат – Температура гигрометра (52 68 96 43), 300.815
Четверный флоат – Влажность относительная в процентах (3B 0C 19 42), 38.3%

Статусный байт ST: 
•	ST[0] – состояние нагревателя (0 – выключен, 1 – включен)
•	ST[1]  - состояние вентилятора (0 – выключен, 1 – включен)
•	ST[2] – режим работы (0 – ручное управление нагревателем и вентилятором, 1 – автоматическое)


             */

            if (_serialPort.IsOpen)
            {
                command = "getclimate";
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x16, 0x00, 0x30 };

                _serialPort.Write(bytes, 0, 5);
            }

        }

        //public bool HeaterStatus;
        //public bool FanStatus;
        //public bool AutoMode;
        public void GetClimate2()
        {
            /*
             Ответ
В полезной нагрузке protobuf
message ClimateResponse {
	ClimateMode Mode = 1;
	bool CoolerEn = 2;
	bool HeaterEn = 3;
	float TemperatureLocal = 4;
	float TemperatureExternal = 5;
	float TemperatureAlternate = 6;
	float Humidity = 7;
	TemperatureChannel Channel = 8;
	float ThresholdTHigh = 9;
	float ThresholdTLow = 10;
	float ThresholdHumidity = 11;
}

Mode – режим работы: автомат, ручной.
CoolerEn, HeaterEn – состояние вентилятора и печки
Temperature* - температуры в К. Local – датчик на плате, External – внешний датчик, Alternate – температура датчика влажность.
Humidity – влажность %
Channel – канал регулировки температуры (датчик на плате/датчик внешний)
Threshold – пороговые значения температур, влажности.

//41
C0-81-16-23-25-33-F3-96-43-2D-33-19-C8-43-35-59-FD-96-43-3D-21-27-0E-42-4D-00-00-48-42-55-00-00-A0-DB-DC-5D-00-00-BE-42-E6

             */

            if (_serialPort.IsOpen)
            {
                command = "getclimate2";
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x16, 0x00, 0x30 };

                _serialPort.Write(bytes, 0, 5);
            }

        }


        //Устанавливает силовой разъем в 1
        public void SetClimate(bool HeaterStatus, bool FanStatus, bool AutoMode)
        {
            /*
Команда CLIMATE_SET (0x17)
Описание
Установить режим работы климатики: ручное, автоматическое.
Запрос
C0 82 17 01 03 33
1 байт полезной информации ST:
•	ST[0] – состояние нагревателя (0 – выключен, 1 – включен)
•	ST[1] – состояние вентилятора (0 – выключен, 1 – включен)
•	ST[2] – режим работы (0 – ручное управление нагревателем и вентилятором, 1 – автоматическое)
ST[0], ST[1] работают только при ST[2] == 0. В автоматическом режиме изменение ST[0:1] ни на что не влияет.


             */

            if (_serialPort.IsOpen)
            {
                command = "setclimate";

                //byte managebyte = 0x00;

                byte climatbyte = 0x00;
                //byte byte2 = 0x00;
                if (HeaterStatus)
                    climatbyte |= 1;
                if (FanStatus)
                    climatbyte |= 2;
                if (AutoMode)
                    climatbyte |= 4;

                Debug.Print(SingleByteToBitString(climatbyte));

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x17, 0x01, climatbyte };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x17, 0x01, climatbyte, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                //Debug.Print(ByteArrayToString(NewBytes));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        public void SetClimate2(byte Mode, bool Cool, bool Heat, bool cfg, float thigh, float tlow, float thum, byte chn)
        {
            /*
            Protobuf:
message ClimateSet {
	ClimateMode Mode = 1;
	bool CoolerEn = 2;
	bool HeaterEn = 3;
	bool IsConfig = 4;
	float ThresholdTHigh = 5;
	float ThresholdTLow = 6;
	float ThresholdHumidity = 7;
	TemperatureChannel Channel = 8;
}
Аналогично ClimateGet.
В ручном режиме (Mode == Manual) CoolerEn, HeaterEn управляют вентилятором и печкой (1 – вкл, 0-выкл).
Установка IsConfig в True указывает, что настройки нужно сохранить в энергонезависимую память. ВНИМАНИЕ! При IsConfig == True Заполнить все поля структуры!!!

             */

            command = "setclimate2";

            if (_serialPort.IsOpen)
            {
                ClimateSet climate_set = new ClimateSet();
                ClimateMode mode = ClimateMode.Auto;
                if (Mode == 1) mode = ClimateMode.Manual;
                climate_set.Mode = mode;
                climate_set.CoolerEn = Cool;
                climate_set.HeaterEn = Heat;
                climate_set.IsConfig = cfg;
                climate_set.ThresholdTHigh = thigh;
                climate_set.ThresholdTLow = tlow;
                climate_set.ThresholdHumidity = thum;
                TemperatureChannel tchannel = TemperatureChannel.TcLocal;
                if (chn == 1) tchannel = TemperatureChannel.TcExternal;
                climate_set.Channel = tchannel;



                int structlen = climate_set.CalculateSize();

                byte[] climat_bytes;

                using (var output = new MemoryStream())
                {
                    climate_set.WriteTo(output);
                    climat_bytes = output.ToArray();
                }

                int beforecount = structlen + 4;
                byte[] bytesbefore = new byte[beforecount];

                bytesbefore[0] = 0xc0;
                bytesbefore[1] = 0x82;
                bytesbefore[2] = 0x17;
                bytesbefore[3] = Convert.ToByte(structlen);

                for (int i = 4; i < beforecount; i++)
                {
                    bytesbefore[i] = climat_bytes[i - 4];
                }

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[beforecount + 1];

                for (int i = 0; i < beforecount; i++)
                {
                    byteswithcrc[i] = bytesbefore[i];
                }
                byteswithcrc[beforecount] = crc;

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                Debug.Print(ByteArrayToStringX(bytesbefore));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);

            }

        }



        public void GetWiegand()
        {
            /*
             Команда WIEGAND (0x0B)
Описание
Чтение данных из Виганда.
Запрос
C0 82 0B 00 55
Ответ
C0 81 0B 06 01 1A 8F CD CF 80 68
             */

            if (_serialPort.IsOpen)
            {
                command = "getwiegand";
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x0b, 0x00, 0x55 };

                _serialPort.Write(bytes, 0, 5);
            }

        }

        //crc

        public void GetAllParameters()
        {

            if (_serialPort.IsOpen)
            {
                //чистка буфера
                //мука
                _serialPort.DiscardInBuffer();

                command = "getall";

                //BusyForAll = true;

                //byte managebyte = 0x00;

                //byte climatbyte = 0xff;

                //Debug.Print(SingleByteToBitString(climatbyte));

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x50, 0x01, 0xFF };

                byte crc = CalcCRC(bytesbefore);

                //Debug.Print("CRC byte:");

                //Debug.Print(SingleByteToString(crc));

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x50, 0x01, 0xFF, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                //Debug.Print(ByteArrayToString(NewBytes));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        public void SetPulseCommand(int diskret, uint diskretnum, uint width, uint delay) //дискрет 0=а 1=б 2=реле. номера соответственно 1-4 или 1-10
        {
            if (_serialPort.IsOpen)
            {
                command = "blasting";

                Pulse pulse = new Pulse();
                pulse.Delay = delay;
                if (diskret == 0)
                {
                    pulse.Pin = diskretnum;
                }
                else if (diskret == 1)
                {
                    pulse.Pin = diskretnum + 4;
                }
                else if (diskret == 2)
                {
                    pulse.Pin = diskretnum + 8;
                }
                pulse.Width = width;

                Debug.Print(pulse.Pin.ToString());

                int structlen = pulse.CalculateSize();

                Debug.Print(structlen.ToString());

                byte[] pulsebytes;

                //CodedOutputStream output = new CodedOutputStream(pulsebytes);

                using (var output = new MemoryStream())
                {
                    pulse.WriteTo(output);
                    pulsebytes = output.ToArray();
                }


                Debug.Print(ByteArrayToStringX(pulsebytes));

                int beforecount = structlen + 4;
                byte[] bytesbefore = new byte[beforecount];

                bytesbefore[0] = 0xc0;
                bytesbefore[1] = 0x82;
                bytesbefore[2] = 0x51;
                bytesbefore[3] = Convert.ToByte(structlen);

                for (int i = 4; i < beforecount; i++)
                {
                    bytesbefore[i] = pulsebytes[i - 4];
                }

                //C0 82 17 01 
                Debug.Print(ByteArrayToStringX(bytesbefore));

                // byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x17, 0x01, climatbyte };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[beforecount + 1];

                for (int i = 0; i < beforecount; i++)
                {
                    byteswithcrc[i] = bytesbefore[i];
                }
                byteswithcrc[beforecount] = crc;

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                Debug.Print(ByteArrayToStringX(bytesbefore));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }
        }

    }
}
