using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
using Google.Protobuf;

namespace communication
{
    public class SlaveV32 : SlaveIf
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger("Slave3.2");
        private const int INFO_MINIMAL_LENGTH = 2 + 12 + 4; // 2 ����� ������ FLASH, 12 ���� - ID, 4 ����� ������.
        private SerialPort m_xSerial;
        private List<byte> m_aReceivedData;

        private WakeSlave m_xWake;
        private byte SlaveAddress = 2;
        private string m_sPortName = "";
        private System.Timers.Timer m_xTimerReopen;

        public SlaveV32()
        {
            SlaveSuccess = true;
            m_xTimerReopen = new System.Timers.Timer();
            m_xTimerReopen.Interval = 250;
            m_xTimerReopen.Elapsed += TimerReopen_Elapsed;
            m_xTimerReopen.Enabled = false;
            m_xTimerReopen.AutoReset = true;

            m_xSerial = new SerialPort();
            m_xSerial.BaudRate = 256000;
            m_xSerial.DataBits = 8;
            m_xSerial.StopBits = StopBits.One;
            m_xSerial.Parity = Parity.None;
            m_xSerial.Handshake = Handshake.None;
            m_xSerial.WriteTimeout = 20;
            m_xSerial.ReadTimeout = 20;
            logger.Info("Version Slave Code: 1.0.2 poll auto reconnect");
            logger.Debug(String.Format("Received Bytes Threshold: {0} bytes", m_xSerial.ReceivedBytesThreshold));
            logger.Debug(String.Format("Read timeout: {0} ms", m_xSerial.ReadTimeout));
            m_aReceivedData = new List<byte>();

            m_xWake = new WakeSlave(1, 128);
        }

        private void TimerReopen_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //logger.Debug(String.Format("Trying reopen serial port `{0}`", m_sPortName));
            if (m_xSerial.IsOpen)
                return;
            if (m_sPortName == "")
                return;
            if (Open(m_sPortName, false))
            {
                logger.Info(String.Format("Port `{0}` reopen succcessfully", m_sPortName));
            }
        }

        public override bool Open(string portName, bool startReopenTimer = true)
        {
            logger.Info($"Open port `{portName}`");
            if (m_xSerial.IsOpen)
            {
                IsOpen = true;
                return true;
            }

            try
            {
                if (startReopenTimer)
                {
                    m_xTimerReopen.Start();
                }
                m_sPortName = portName;
                m_xSerial.PortName = portName;
                m_xSerial.Open();
                IsOpen = true;
               
                return true;
            }
            catch (Exception ex)
            {
                logger.Warn($"Cannot open Serial Port `{portName}`", ex);
                IsOpen = false;
                return false;
            }
        }
        public override bool Close()
        {
            logger.Info($"Close port `{m_sPortName}`");
            if (!m_xSerial.IsOpen)
            {
                m_sPortName = "";
                SlaveInfo.ID = null;
                IsOpen = false;
                return true;
            }
            try
            {
                m_sPortName = "";
                SlaveInfo.ID = null;
                m_xSerial.Close();
                IsOpen = false;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public void Poll()
        {
            if (!(m_xSerial.IsOpen && (m_xSerial.BytesToRead > 0)))
                return;

            while (m_xSerial.BytesToRead > 0)
            {
                var btr = m_xSerial.BytesToRead;
                byte[] readed = new byte[btr];
                try
                {
                    m_xSerial.Read(readed, 0, m_xSerial.BytesToRead);
                    AppendToList(m_aReceivedData, readed);
                }
                catch (Exception ex)
                {
                    logger.Error($"Cannot read {btr} bytes from Serial", ex);
                }
            }

            while (m_aReceivedData.Count > 0)
            {
                byte b = m_aReceivedData[0];
                m_aReceivedData.RemoveAt(0);
                if (m_xWake.ProcessInByte(b) == WakeStatus.READY)
                {
                    ChangeCommand = true; //INFO ���� �� ��������� � true, �� �� �����-������ �� ����� ���������� ������ ���������
                    logger.Info(String.Format("[{0}] Received Command: {1}, size {2}", m_xWake.ValidReceiveCounter, m_xWake.Command, m_xWake.ReceivedSize));
                    if (m_xWake.Command == (byte)Commands.CmdInfo)
                        ParseCommandINFO(m_xWake.ReceivedBuffer, m_xWake.ReceivedSize);
                    if (m_xWake.Command == (byte)Commands.CmdReadAll)
                        ParseCommandGetAll(m_xWake.ReceivedBuffer, m_xWake.ReceivedSize);
                }
            }
        }


        private void ParseCommandINFO(byte[] rcv_data, int rcv_size)
        {
            logger.Info(String.Format("INFO Command. Size {0}", rcv_size));
            if (rcv_size < INFO_MINIMAL_LENGTH)
            {
                logger.Warn("INFO Reply too small");
                return;
            }

            SlaveInfo = new SlaveInfoS();
            var slave_memory = new byte[] { rcv_data[0], rcv_data[1] };
            var slave_id = new byte[] { rcv_data[ 2], rcv_data[ 3], rcv_data[ 4], rcv_data[ 5],
                                        rcv_data[ 6], rcv_data[ 7], rcv_data[ 8], rcv_data[ 9],
                                        rcv_data[10], rcv_data[11], rcv_data[12], rcv_data[13] };
            var version = new byte[] { rcv_data[14], rcv_data[15], rcv_data[16], rcv_data[17] };

            SlaveInfo.MemorySize = String.Format("{0} kB", BitConverter.ToUInt16(slave_memory, 0));
            SlaveInfo.ID = BitConverter.ToString(slave_id).Replace("-", "");
            var version_int = BitConverter.ToUInt32(version, 0);
            SlaveInfo.Version = VersionString(version_int);


            if (rcv_size > INFO_MINIMAL_LENGTH)
            {
                var name = new byte[rcv_size - INFO_MINIMAL_LENGTH];
                for (int i = 0; i < name.Length; i++)
                    name[i] = rcv_data[INFO_MINIMAL_LENGTH + i];
                SlaveInfo.mName = Encoding.ASCII.GetString(name);
            }
            else
            {
                SlaveInfo.mName = "Unknown";
            }
            logger.Info("Found Slave controller:");
            logger.Info(String.Format("  Name: {0}", SlaveInfo.mName));
            logger.Info(String.Format("  Version: {0}, {1}", version_int, SlaveInfo.Version));
            logger.Info(String.Format("  ID: {0}", SlaveInfo.ID));
        }

        private void ParseCommandGetAll(byte[] rcv_data, int rcv_length)
        {
            logger.Debug(String.Format("Received GetAll command, id {0}, length {1}", (int)Commands.CmdReadAll, rcv_length));
            ResponseAll resp;
            try
            {
                resp = ResponseAll.Parser.ParseFrom(rcv_data, 0, rcv_length);
            }
            catch (Exception ex)
            {
                logger.Error(String.Format("Cannot parse Protobuf: {0}", ex.Message));
                return;
            }
            logger.Debug(String.Format("Proto Responce: {0}", resp.ToString()));

            SetStatePortAB(out PortA_In, resp.PORTAIDR);
            SetStatePortAB(out PortB_In, resp.PORTBIDR);
            //SetStatePortAB(out PortA_Out, resp.PORTAODR); // �� ���������!! _Out ����������� �� frmRack.cs
            //SetStatePortAB(out PortB_Out, resp.PORTBODR); // �� ���������!! _Out ����������� �� frmRack.cs
            SetStateRelays(out RelaysStateIn, resp.RELAYSIDR);
            //SetStateRelays(out RelaysStateOut, resp.RELAYSODR); // �� ���������!! _Out ����������� �� frmRack.cs

            using (log4net.NDC.Push("GetAll Rx"))
            {
                logger.Debug(String.Format("PortA  IN: {0}", SlaveIf.PortABRepr(PortA_In)));
                logger.Debug(String.Format("PortB  IN: {0}", SlaveIf.PortABRepr(PortB_In)));
                logger.Debug(String.Format("PortA OUT: {0}", SlaveIf.PortABRepr(PortA_Out)));
                logger.Debug(String.Format("PortB_OUT: {0}", SlaveIf.PortABRepr(PortB_Out)));
                logger.Debug(String.Format("Relay  IN: {0}", SlaveIf.RelaysRepr(RelaysStateIn)));
                logger.Debug(String.Format("Relay OUT: {0}", SlaveIf.RelaysRepr(RelaysStateOut)));
            }
        }

        /**
         * @brief ������������� ������ �� GetAll ��� ������ � ������� PORTA, PORTB
         */
        void SetStatePortAB(out DiskretInput portab, uint data)
        {
            portab.D1 = (data & (1 << 0)) != 0;
            portab.D2 = (data & (1 << 1)) != 0;
            portab.D3 = (data & (1 << 2)) != 0;
            portab.D4 = (data & (1 << 3)) != 0;
        }

        /**
         * @brief ������������� ������ �� GetAll ��� ���������� ������ � �������
         */
        void SetStateRelays(out RelaysInOut relays, uint data)
        {
            relays.R1 = (data & (1 << 0)) != 0;
            relays.R2 = (data & (1 << 1)) != 0;
            relays.R3 = (data & (1 << 2)) != 0;
            relays.R4 = (data & (1 << 3)) != 0;
            relays.R5 = (data & (1 << 4)) != 0;
            relays.R6 = (data & (1 << 5)) != 0;
            relays.R7 = (data & (1 << 6)) != 0;
            relays.R8 = (data & (1 << 7)) != 0;
            relays.R9 = (data & (1 << 8)) != 0;
            relays.R10 = (data & (1 << 9)) != 0;
        }

        static string VersionString(uint version)
        {
            var major = version / 10000;
            var minor = (version - (major * 10000)) / 100;
            var patch = version - major * 10000 - minor * 100;
            return String.Format("{0}.{1}.{2}", major, minor, patch);
        }
        static void AppendToList(List<byte> list_out, byte[] data)
        {
            foreach (byte d in data)
                list_out.Add(d);
        }

        static public bool CheckPortExist(string portName)
        {
            if (portName.Length == 0)
                return false;

           

            SerialPort serial = new SerialPort(portName);

            try
            {
                serial.Open();
                serial.Close();
                logger.Info("Port " + portName + " exists");
                return true;
            }
            catch (Exception)
            {
                logger.Warn("Port not opened");
                return false;
            }
        }

        public override bool GetSlaveInfo()
        {
            SlaveInfo.ID = null;
            if (!m_xSerial.IsOpen)
            {
                return false;
            }

            var tx_data = WakeSlave.CreateWakeTxData(SlaveAddress, (byte)Commands.CmdInfo, new byte[0]);
            try
            {
                m_xSerial.Write(tx_data, 0, tx_data.Length);
                SlaveSuccess = true;
                return true;
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Cannot write SendINFO: {0}", e.Message));
                return false;
            }
        }

        public override void GetAllParameters()
        {
            if (!m_xSerial.IsOpen)
            {
                return;
            }
            ChangeCommand = true;
            var tx_data = WakeSlave.CreateWakeTxData(SlaveAddress, (byte)Commands.CmdReadAll, new byte[] { 0xFF });
            try
            {
                m_xSerial.Write(tx_data, 0, tx_data.Length);
                SlaveSuccess = true;
                return;
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Cannot write GetAllParameters: {0}", e.Message));
            }
        }

        public override void WriteToDiskretAB(string mask_a, string mask_b)
        {
            using (log4net.NDC.Push("WriteToDiskretAB"))
            {
                logger.Debug(String.Format("mask_a: `{0}`, mask_b: `{1}`", mask_a, mask_b));
                if (mask_a == null || mask_b == null)
                {
                    logger.Error("One of parameters is null");
                    return;
                }
                if (!m_xSerial.IsOpen)
                {
                    return;
                }
                byte mask_a_int = 0;
                byte mask_b_int = 0;
                try
                {
                    mask_a_int = Convert.ToByte(mask_a, 2);
                    mask_b_int = Convert.ToByte(mask_b, 2);
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format("Cannot convert mask: {0}", ex.Message));
                    return;
                }
                logger.Debug(String.Format("Decoded. mask_a `{0}`, mask_b `{1}`", mask_a_int, mask_b_int));
                var tx_data = new byte[] { mask_a_int, mask_b_int };
                var encoded_data = WakeSlave.CreateWakeTxData(SlaveAddress, (byte)Commands.CmdPortsOdrw, tx_data);
                try
                {
                    m_xSerial.Write(encoded_data, 0, encoded_data.Length);
                    SlaveSuccess = true;
                    return;
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format("Cannot write to serial port: {0}", ex.Message));
                }
            }
        }

        public override void WriteToAllDiskrets(string mask_a, string mask_b, string mask_h)
        {
            using (log4net.NDC.Push("WriteToAllDiskrets"))
            {

                logger.Debug(String.Format("mask_a: `{0}`, mask_b: `{1}`, mask_h: `{2}`", mask_a, mask_b, mask_h));
                if (mask_a == null || mask_b == null || mask_h == null)
                {
                    logger.Error("One of parameters is null");
                    return;
                }

                byte mask_a_int = 0;
                byte mask_b_int = 0;
                ushort mask_h_int = 0;

                try
                {
                    mask_a_int = Convert.ToByte(mask_a, 2);
                    mask_b_int = Convert.ToByte(mask_b, 2);
                    mask_h_int = Convert.ToUInt16(mask_h, 2);
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format("Cannot convert mask {0}", ex.Message));
                    return;
                }
                logger.Debug(String.Format("Decoded mask_a: `{0}`, mask_b: `{1}`, mask_h: `{2}`", mask_a_int, mask_b_int, mask_h_int));

                var tx_data = new byte[] { mask_a_int, mask_b_int, (byte)(mask_h_int >> 8), (byte)(mask_h_int & 0x00FF) }; // Big Endian, ������� ������
                var encoded_data = WakeSlave.CreateWakeTxData(SlaveAddress, (byte)Commands.CmdOutputAllOdrw, tx_data);
                try
                {
                    m_xSerial.Write(encoded_data, 0, encoded_data.Length);
                    SlaveSuccess = true;
                    return;
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format("Cannot write to Serial port: `{0}`", ex.Message));
                }
            }
        }

        public override void GetPowerErrors()
        {
            logger.Warn("GetPowerErrors not implemented");
        }
        public override void GetPowerOut()
        {
            logger.Warn("GetPowerOut not implemented");
        }
        static bool CheckPowerChannel(int channel)
        {
            return (channel >= 1) && (channel <= 10);
        }

        /**
         * @brief ������������ ����� ������ � ��� ����� ��� ������� CmdPowerSet/CmdPowerReset
         * @param channel ����� ������ �� 1 �� 10 ������������
         * @attention ����� ������ channel �� ����������� � ������� ������� �� �����, �� ������������ ������� ����!
         * 
         * ����� 3 ������������ ������ 8 �� �����, � ����� 10 - ������ 0 �� �����.
         */
        static byte[] EncodePowerChannel(int channel)
        {
            ushort mask = (ushort)(1 << (10 - channel));
            var encoded = new byte[] { (byte)(mask >> 8), (byte)(mask & 0xFF) };
            return encoded;
        }

        public override void SetPowerTo1(int channel)
        {
            using (log4net.NDC.Push("SetPowerTo1"))
            {
                logger.Debug(String.Format("Set to channel `{0}`", channel));
                if (!m_xSerial.IsOpen)
                {
                    return;
                }
                if (!CheckPowerChannel(channel))
                {
                    logger.Warn(String.Format("Channel `{0}` is Invalid", channel));
                    return;
                }

                var encoded_data = WakeSlave.CreateWakeTxData(SlaveAddress, (byte)Commands.CmdPowersSet, EncodePowerChannel(channel));
                try
                {
                    m_xSerial.Write(encoded_data, 0, encoded_data.Length);
                    SlaveSuccess = true;
                    return;
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format("Cannot write to serial: `{0}`", ex.Message));
                    return;
                }
            }
        }

        public override void SetPowerTo0(int channel)
        {
            using (log4net.NDC.Push("SetPowerTo0"))
            {
                if (!m_xSerial.IsOpen)
                {
                    return;
                }

                if (!CheckPowerChannel(channel))
                {
                    logger.Warn(String.Format("Channel `{0}` is Invalid", channel));
                    return;
                }
                var encoded_data = WakeSlave.CreateWakeTxData(SlaveAddress, (byte)Commands.CmdPowersReset, EncodePowerChannel(channel));
                try
                {
                    m_xSerial.Write(encoded_data, 0, encoded_data.Length);
                    SlaveSuccess = true;
                    return;
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format("Cannot write to serial: `{0}`", ex.Message));
                    return;
                }
            }
        }

        public override void WriteToDiskretPower(string mask)
        {
            using (log4net.NDC.Push("WriteToDiskretPower"))
            {
                logger.Debug(String.Format("Set Power to `{0}`", mask));
                if (!m_xSerial.IsOpen)
                {
                    return;
                }
                ushort mask_int = 0;
                try
                {
                    mask_int = Convert.ToUInt16(mask, 2);
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format("Cannot convert mask `{0}` to integer: `{1}`", mask, ex.Message));
                    return;
                }
                var tx_data = new byte[] { (byte)(mask_int >> 8), (byte)(mask_int & 0xFF) };
                var encoded_data = WakeSlave.CreateWakeTxData(SlaveAddress, (byte)Commands.CmdPowersOdrw, tx_data);
                try
                {
                    m_xSerial.Write(encoded_data, 0, encoded_data.Length);
                    SlaveSuccess = true;
                    return;
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format("Cannot write to serial: `{0}`", ex.Message));
                    return;
                }
            }
        }

        public override void SetClimate2(byte Mode, bool FanEn, bool HeaterEn, bool save_config, float high_thrs, float low_thrs, float hum_thrs, byte channel)
        {
            using (log4net.NDC.Push("SetClimate2"))
            {
                if (!m_xSerial.IsOpen)
                {
                    return;
                }

                ClimateSet climate_pb = new ClimateSet();
                if (Mode == 1)
                    climate_pb.Mode = ClimateMode.Manual;
                else
                    climate_pb.Mode = ClimateMode.Auto;
                climate_pb.CoolerEn = FanEn;
                climate_pb.HeaterEn = HeaterEn;
                climate_pb.IsConfig = save_config;
                climate_pb.ThresholdTHigh = high_thrs;
                climate_pb.ThresholdTLow = low_thrs;
                climate_pb.ThresholdHumidity = hum_thrs;
                if (channel == 1)
                    climate_pb.Channel = TemperatureChannel.TcExternal;
                else
                    climate_pb.Channel = TemperatureChannel.TcLocal;

                var encoded_data = WakeSlave.CreateWakeTxData(SlaveAddress, (byte)Commands.CmdClimateSet, climate_pb.ToByteArray());
                try
                {
                    m_xSerial.Write(encoded_data, 0, encoded_data.Length);
                    SlaveSuccess = true;
                    return;
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format("Cannot write to serial: `{0}`", ex.Message));
                    return;
                }
            }
        }

        public override void SetPulseCommand(int out_type, uint channel, uint width_ms, uint delay_ms)
        {
            using (log4net.NDC.Push("SetPulseCommand"))
            {
                logger.Debug(String.Format("out_type: `{0}`, channel: `{1}`, width_ms: `{2}`, delay_ms: `{3}`", out_type, channel, width_ms, delay_ms));
                if (!m_xSerial.IsOpen)
                {
                    return;
                }

                Pulse pulse_pb = new Pulse
                {
                    Delay = delay_ms,
                    Width = width_ms,
                    Pin = channel
                };

                switch (out_type) // ��������. ������ 0-3 - PORTA, 4-7 - PORTB, 8-17 - PORTH (Relay)
                {
                    case 0:
                        pulse_pb.Pin += 0;
                        break;
                    case 1:
                        pulse_pb.Pin += 4;
                        break;
                    case 2:
                        pulse_pb.Pin += 8;
                        break;
                    default:
                        logger.Error(String.Format("out_type `{0}` invalid", out_type));
                        return;
                }
                var encoded_data = WakeSlave.CreateWakeTxData(SlaveAddress, (byte)Commands.CmdPulse, pulse_pb.ToByteArray());
                try
                {
                    m_xSerial.Write(encoded_data, 0, encoded_data.Length);
                    SlaveSuccess = true;
                    return;
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format("Cannot write to serial: `{0}`", ex.Message));
                    return;
                }
            }
        }
    } // public class SlaveV32

} // namespace communication
