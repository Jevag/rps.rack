﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    //класс сканера
    public class ScannerRT211
    {
        int t=0;

        public int power_counter = 0;
        public string command = "";
        SerialPort _serialPort;
        public bool PowerStatus { get; set; }
        public string Status { get; set; }             // статус сканера Barcode
        public bool LoopA { get; set; }

        public string barcode { get; set; }

        public System.Timers.Timer tmrScanImpulse;
        public System.Timers.Timer tmrRX;
        public System.Timers.Timer tmrPing;

        int bytescountrx = 0;

        int tt = 0;

        byte[] etalon = new byte[] { 0x02, 0x00, 0x00, 0x01, 0x00, 0x33, 0x31 };

        //
        //byte[] suck_etalon;

        public ScannerRT211()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 9600;// 115200;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 500;
            _serialPort.ReadTimeout = 500;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;

            tmrScanImpulse = new System.Timers.Timer();
            tmrScanImpulse.Interval = 5200; //5200...
            tmrScanImpulse.Elapsed += TmrScanImpulse_Elapsed;
            tmrScanImpulse.Enabled = false;
            tmrScanImpulse.AutoReset = true;

            tmrRX = new System.Timers.Timer();
            tmrRX.Interval = 250; //250...
            tmrRX.Elapsed += TmrRX_Elapsed;
            tmrRX.Enabled = false;
            tmrRX.AutoReset = true;

            tmrPing = new System.Timers.Timer();
            tmrPing.Interval = 200; //250...
            tmrPing.Elapsed += TmrPing_Elapsed;
            tmrPing.Enabled = false;
            tmrPing.AutoReset = true;

            command = "none";
        }

        private void TmrPing_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            /*
            PowerCounter += 1;
            if (PowerCounter == 20)
            {
                PowerStatus = false;
                PowerCounter = 0;
            }
            */
        }

        //мануалом проверять доступность сканера...

        private void TmrRX_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //Debug.Print(command);

                //Debug.Print("LOOP " + LoopA.ToString());

                /*
                if (LoopA)
                {
                    tmrScanImpulse.Enabled = true;
                }
                else
                {
                    tmrScanImpulse.Enabled = false;
                }
                */

                //throw new NotImplementedException();
                
                bytescountrx = _serialPort.BytesToRead;
                //Debug.Print("btc!:" + bytescountrx.ToString());
                /*
                Debug.Print("btc!:" + bytescountrx.ToString());

                if (bytescountrx != 7 && bytescountrx != 22)
                {
                    power_counter += 1;
                    Debug.Print(power_counter.ToString());
                    if (power_counter == 54)
                    {
                        power_counter = 0;
                        PowerStatus = false;
                    }
                    _serialPort.DiscardInBuffer();
                    return;
                }
                else
                {
                    PowerStatus = true;
                    power_counter = 0;
                    //tmrPing.Enabled = false;
                    Debug.Print("Titya!");
                    Debug.Print(bytescountrx.ToString());
                }
                */
                PowerStatus = true;
                /*
                if (bytescountrx > 0)
                {
                    PowerStatus = true;
                    power_counter = 0;
                    //tmrPing.Enabled = false;
                    //Debug.Print("Titya!");
                    //Debug.Print(bytescountrx.ToString());
                }
                else
                {
                    power_counter += 1;
                    //Debug.Print(power_counter.ToString());
                    if (power_counter == 50)
                    {
                        power_counter = 0;
                        PowerStatus = false;
                    }
                }
                */

                //Debug.Print(bytescountrx.ToString());
                //Debug.Print(command);
                switch (command)
                {
                    case "triggermode":
                        /*
                        if (bytescountrx > 7)
                        {
                            //command = "barcodewait";
                            //goto sisk;
                            //t = 5;
                        }
                    break;
                    */
                    case "manualmode":
                    //break;
                    case "startscan":
                        //command = "barcodewait";
                        if (bytescountrx == 7)
                        {
                            //Сравнивание с эталоном
                            byte[] tbytes = new byte[bytescountrx];
                            _serialPort.Read(tbytes, 0, bytescountrx);
                            Debug.Print(ByteArrayToString(tbytes));
                            bytescountrx = 0;
                            if (tbytes.SequenceEqual(etalon))
                            {
                                Debug.Print("OK");
                                Status = "ok";
                                if (command == "startscan") command = "barcodewait";
                                if (command == "triggermode") StartScan();
                                if (command == "manualmode") command = "none";
                            }
                            else
                            {
                                //Аларма...
                            }
                        }
                        else if (bytescountrx == 29)
                        {
                            //Debug.Print("good");

                            //byte[] barcodebytes = new byte[bytescountrx];
                            string bc = _serialPort.ReadExisting();
                            bytescountrx = 0;
                            //Debug.Print(bc);
                            barcode = bc.Substring(7, 19);
                            //вкл мануал режим...
                            LoopA = false;
                            ManualMode(); //уберем???
                            tmrScanImpulse.Enabled = false;
                            t = 0;
                        }
                        else if (bytescountrx !=7 && bytescountrx !=29 && bytescountrx>0)
                        {
                            //Debug.Print("fast_scan");

                            byte[] tbytes2 = new byte[7];
                            _serialPort.Read(tbytes2, 0, 7);
                            if (command == "startscan") command = "barcodewait";
                        }
                        /*
                        else if (bytescountrx > 7)
                        {
                            //bytescountrx = 0; //???
                            Debug.Print("fast_scan");

                            byte[] tbytes2 = new byte[7];
                            _serialPort.Read(tbytes2, 0, 7);
                            if (command == "startscan") command = "barcodewait";
                        }                        
                        else if (bytescountrx == 0)
                        {

                        }
                        */
                        break;
                    case "barcodewait":
                        //Debug.Print("zzz " + bytescountrx.ToString());

             sisk:      if (bytescountrx == 22) //штрихкод правильной длины
                        {
                            Debug.Print("good");

                            byte[] barcodebytes = new byte[bytescountrx];
                            string bc = _serialPort.ReadExisting();
                            bytescountrx = 0;
                            //Debug.Print(bc);
                            barcode = bc.Substring(0, 19);
                            //вкл мануал режим...
                            LoopA = false;
                            ManualMode(); //уберем???
                            tmrScanImpulse.Enabled = false;
                            t = 0;
                        }
                        else if (bytescountrx > 0 && bytescountrx < 22)  //БЭД
                        {
                            t += 1;
                            if (t == 3)
                            {
                                Debug.Print("bad");
                                string bcb = _serialPort.ReadExisting();
                                barcode = "bad";
                                //rawbarcode = bcb.Substring(0, bytescountrx);
                                bytescountrx = 0;
                                _serialPort.DiscardInBuffer();
                                //_serialPort.DiscardOutBuffer();
                                LoopA = false;
                                ManualMode();
                                tmrScanImpulse.Enabled = false;
                                t = 0;
                                return;
                            }
                        }
                        else if (bytescountrx > 22)
                        {
                            string bcb = _serialPort.ReadExisting();
                            barcode = "bad";
                            //rawbarcode = bcb.Substring(0, bytescountrx);
                            bytescountrx = 0;
                            _serialPort.DiscardInBuffer();
                            LoopA = false;
                            ManualMode();
                            tmrScanImpulse.Enabled = false;
                            t = 0;
                        }

                        break;
                    case "triggermodereboot":
                        if (bytescountrx == 0)
                        {
                            tt += 1;
                            if (tt == 2)
                            {
                                Status = "failed";
                                command = "none";
                            }
                        }
                        else
                        {
                            tt = 0;
                            bytescountrx = 0;
                            string bcb = _serialPort.ReadExisting();
                            Status = "ok";
                            StartScan();
                        }
                        break;
                    default:
                        break;
                }
                if (t == 0)
                {
                    bytescountrx = 0;
                    _serialPort.DiscardInBuffer();
                }
            }
            catch (Exception ex)
            {
                if (_serialPort.IsOpen)
                {
                    bytescountrx = 0;
                    _serialPort.DiscardInBuffer();
                }
            }
        }

        private void TmrScanImpulse_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Debug.Print("epsl");
            //power_counter = 0;
            if (LoopA)
            {
                //TriggerMode();
                StartScan();
            }
            else
            {
                ManualMode();
            }
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();

                    return true;
                }
                catch (Exception e)
                {
                    Status = "не отвечает";

                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    return true;
                }
                catch (Exception e)
                {
                    Status = "не отвечает";
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        #region mode
        public byte[] CalcCrcTx(byte[] arg)
        {
            byte[] retvalue = new byte[2];
            int len = arg[3] + 8; //len

            uint crc = 0;
            for (int ii = 2; ii < len - 2; ii++)
            {
                for (ushort i = 0x80; i != 0; i /= 2)
                {
                    crc *= 2;
                    if ((crc & 0x10000) != 0) //After previvous CRC multiplied by 2, if the first bit is 1, then divide by 0x11021
                        crc ^= 0x11021;
                    if ((arg[ii] & i) != 0) //If current bit is 1, then CRC = Previous CRC + current/CRC_CCITT, Crc ^= 0x1021
                        crc ^= 0x1021;
                }
            }
            byte byte1 = (byte)((crc & 0xFF00) >> 8);
            byte byte2 = (byte)(crc & 0xFF);
            retvalue[0] = byte1;
            retvalue[1] = byte2;
            return retvalue;
        }

        public void StartScan()
        {
            try
            {
                //PowerCounter = 0;
                command = "startscan";
                byte[] cmd = new byte[9];
                cmd[0] = 0x7E; //prefix
                cmd[1] = 0x00; //prefix
                cmd[2] = 0x08; //types
                cmd[3] = 0x01; //length
                cmd[4] = 0x00; //address
                cmd[5] = 0x02; //address
                cmd[6] = 0x01; //command ?????разобраться как вычисляется
                byte[] crc = CalcCrcTx(cmd);
                cmd[7] = crc[0];
                cmd[8] = crc[1];
                _serialPort.Write(cmd, 0, 9);
            }
            catch (Exception ex) { }
        }

        public void ManualMode() //W030000
        {
            try
            {
                Debug.Print("Manual Mode");
                //PowerCounter = 0;
                command = "manualmode";
                byte[] cmd = new byte[9];
                cmd[0] = 0x7E; //prefix
                cmd[1] = 0x00; //prefix
                cmd[2] = 0x08; //types
                cmd[3] = 0x01; //length
                cmd[4] = 0x00; //address
                cmd[5] = 0x00; //address
                cmd[6] = 0xD4; //command ?????разобраться как вычисляется
                byte[] crc = CalcCrcTx(cmd);
                cmd[7] = crc[0];
                cmd[8] = crc[1];
                _serialPort.Write(cmd, 0, 9);
                //ответ 02-00-00-01-00-33-31
            }
            catch (Exception ex) { }
        }

        public void TriggerModeReboot() //W030001
        {
            try
            {
                //PowerCounter = 0;
                command = "triggermodereboot";
                byte[] cmd = new byte[9];
                cmd[0] = 0x7E; //prefix
                cmd[1] = 0x00; //prefix
                cmd[2] = 0x08; //types
                cmd[3] = 0x01; //length
                cmd[4] = 0x00; //address
                cmd[5] = 0x00; //address
                cmd[6] = 0xD5; //command ?????разобраться как вычисляется
                byte[] crc = CalcCrcTx(cmd);
                cmd[7] = crc[0];
                cmd[8] = crc[1];
                _serialPort.Write(cmd, 0, 9);
                //ответ 02-00-00-01-00-33-31
            }
            catch (Exception ex) { }
        }

        public void TriggerMode() //W030001
        {
            try
            {
                //PowerCounter = 0;
                command = "triggermode";
                byte[] cmd = new byte[9];
                cmd[0] = 0x7E; //prefix
                cmd[1] = 0x00; //prefix
                cmd[2] = 0x08; //types
                cmd[3] = 0x01; //length
                cmd[4] = 0x00; //address
                cmd[5] = 0x00; //address
                cmd[6] = 0xD5; //command ?????разобраться как вычисляется
                byte[] crc = CalcCrcTx(cmd);
                cmd[7] = crc[0];
                cmd[8] = crc[1];
                _serialPort.Write(cmd, 0, 9);
                //ответ 02-00-00-01-00-33-31
            }
            catch (Exception ex) { }
        }

        #endregion

        #region statics
        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba); //.Replace("-", "");
        }

        public static string ByteArrayEncode(byte[] data)
        {
            char[] characters = data.Select(b => (char)b).ToArray();
            return new string(characters);
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string SingleByteToString(byte ba)
        {
            byte[] b = new byte[1];
            b[0] = ba;

            return BitConverter.ToString(b);
        }
        #endregion

    }
}
