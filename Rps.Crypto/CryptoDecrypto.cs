﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Rps.Crypto
{
    public class CryptoDecrypto
    {
        public Aes myAes;

        public CryptoDecrypto()
        {
            // Инициализация ключей
            string KeyS = "87032F64D483EE7D5B992F6B73784ADE0ADDAF74D0BDDCCF7B232A8239D53FC3";
            byte[] KeyA = StringToByteA(KeyS);
            string IVS = "080FA8FC18D842A0F7E27745974992E3";
            byte[] IV = StringToByteA(IVS);

            myAes = Aes.Create();
            myAes.Key = KeyA;
            myAes.IV = IV;
        }

        public string Decrypto(string encrypt)
        {
            /*
            byte[] encrypted = EncryptStringToBytes_Aes(CommandPassword.Text);

            //Шифрация текста в массив байт - CommandPassword.Text - текстовое поле
            deviceCMModel.CommandPassword = cashierForm.ByteAToString(encrypted);
            //Сохранение в БД

            encrypted = cashierForm.StringToByteA(cashierForm.deviceCMModel.CommandPassword);
            //Подготовка массива байт для дешифрации из строки
            CommandPassword.Text = cashierForm.DecryptStringFromBytes_Aes(encrypted);
            //Дешифрация в строку.
            */
            byte[] encrypted = StringToByteA(encrypt);
            return DecryptStringFromBytes_Aes(encrypted);
        }

        public byte[] EncryptStringToBytes_Aes(string plainText)
        {
            try
            {
                // Check arguments.
                if (plainText == null || plainText.Length <= 0)
                    throw new ArgumentNullException("plainText");
                byte[] encrypted;
                // Create an Aes object
                // with the specified key and IV.
                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = myAes.CreateEncryptor(myAes.Key, myAes.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }

                // Return the encrypted bytes from the memory stream.
                return encrypted;
            }
            catch (Exception exc)
            {
                //ToLog(exc.ToString());
                return null;
            }
        }

        public string DecryptStringFromBytes_Aes(byte[] cipherText)
        {
            try
            {
                // Check arguments.
                if (cipherText == null || cipherText.Length <= 0)
                    throw new ArgumentNullException("cipherText");
                // Declare the string used to hold
                // the decrypted text.
                string plaintext = null;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = myAes.CreateDecryptor(myAes.Key, myAes.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting  stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
                return plaintext;
            }
            catch (Exception exc)
            {
                //ToLog(exc.ToString());
                return null;
            }
        }

        public string ByteAToString(byte[] bA)
        {
            try
            {
                string S = "";
                foreach (byte b in bA)
                    S = S + b.ToString("X2");
                return S;
            }
            catch (Exception exc)
            {
                //ToLog(exc.ToString());
                return "";
            }
        }

        public byte[] StringToByteA(string S)
        {
            try
            {
                int a = S.Length / 2;
                byte[] bA = new byte[a];
                for (int i = 0; i < a * 2; i += 2)
                {
                    bool result = byte.TryParse(S.Substring(i, 2), NumberStyles.HexNumber, null as IFormatProvider, out bA[i / 2]);
                    if (!result)
                        bA[i / 2] = 0xff;
                }
                return bA;
            }
            catch (Exception exc)
            {
                //ToLog(exc.ToString());
                return null;
            }
        }
    }
}
