﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace rps.Data
{
  internal class IntervalComparer : IEqualityComparer<Interval>
  {
    public static readonly IntervalComparer Instance = new IntervalComparer();

    #region [ IEqualityComparer ]

    public virtual bool Equals(Interval x, Interval y)
    {
      if (x == null)
        return (y == null);
      if (y == null)
        return (x == null);

      if (x.Start > y.End)
        return false;
      if (y.Start > x.End)
        return false;

      return true;
    }

    public int GetHashCode(Interval obj)
    {
      if (obj == null)
        return 0;

      return obj.GetHashCode();
    }

    #endregion
  }

  internal class ListComparer : IEqualityComparer<List<int>>
  {
    public static readonly ListComparer Instance = new ListComparer();

    #region [ IEqualityComparer ]

    public virtual bool Equals(List<int> x, List<int> y)
    {
      if (x == null)
        return (y == null);
      if (y == null)
        return (x == null);

      x.Sort();
      var a1 = x.ToArray();
      y.Sort();
      var a2 = y.ToArray();

      return Helper.IsEqual(a1, a2);
    }

    public int GetHashCode(List<int> obj)
    {
      //if (obj == null)
      //  return 0;

      return 0; //must compare each
    }

    #endregion
  }

  internal static class Helper
  {
    public static IEnumerable<Interval> Normalize(this IEnumerable<Interval> ranges)
    {
      List<Interval> normalized = new List<Interval>();
      TimeSpan t = TimeSpan.Zero;
      Interval p = null;
      foreach (Interval i in ranges.OrderBy(r => r.Start))
      {
        if ((p != null) && (p.End > i.Start))
        {
          TimeSpan d = p.Duration;
          p.Duration = i.Start - p.Start;
          if (p.Duration < TimeSpan.Zero)
          {
            //p.Duration = d;
            throw new ApplicationException(String.Format("Invalid interval\r\n{0}", p));
          }

          Trace.WriteLine(String.Format("---> [interval duration adjuted] interval = {0}, old = {1}", p, d));
        }

        if (i.Start > t)
        {
          normalized.Add(new Interval()
          {
            Start = t,
            Duration = i.Start - t,
            Amount = 0,
            Period = TimeSpan.Zero,
            ProcessingMode = IntervalProcessingMode.Inherit,
            Description = "zero padding"
          });

          Trace.WriteLine(String.Format("---> [empty interval padded] from {0} to {1}", t, i.Start));
        }

        normalized.Add(i);
        if (i.Duration == Interval.Infinite)
          break;

        t += i.Duration;
        p = i;
      }

      return normalized.ToArray();
    }

    public static SwitchMode GetSwitchMode(this EvaluationContext c)
    {
      Rule r = c.Rule;
      SwitchMode w = r.Match.Mode;
      if (w == SwitchMode.Inherit)
        w = (r.SwitchMode == SwitchMode.Inherit) ? c.Tariff.SwitchMode : r.SwitchMode;

      return w;
    }

    public static TimeSpan GetInitial(this EvaluationContext c)
    {
      Rule r = c.Rule;

      return c.IsFirstInterval ? (r.InitialMode == InitialMode.Inherit ? c.Tariff.Initial : r.Initial) : TimeSpan.Zero;
    }

    public static decimal GetInitialAmount(this EvaluationContext c)
    {
      Rule r = c.Rule;

      return r.InitialMode == InitialMode.Inherit ? c.Tariff.InitialAmount : r.InitialAmount;
    }

    public static TimeSpan GetProtection(this EvaluationContext c)
    {
      Rule r = c.Rule;
      Interval i = c.Interval;

      return (i.ProtectionMode == ProtectionMode.Inherit) ? 
        (r.ProtectionMode == ProtectionMode.Inherit ? c.Tariff.Protection : r.Protection) : i.Protection;
    }

    public static IntervalProcessingMode GetProcessingMode(this EvaluationContext c)
    {
      if (c.SwitchExact)
        return IntervalProcessingMode.Exact;

      Rule r = c.Rule;
      Interval i = c.Interval;

      return (i.ProcessingMode == IntervalProcessingMode.Inherit) ? (r.ProcessingMode == IntervalProcessingMode.Inherit ? c.Tariff.ProcessingMode : r.ProcessingMode) : i.ProcessingMode;
    }

    public static void AddInterval(this EvaluationContext context, DateTime dts, DateTime dte, decimal amount, bool se)
    {
      ((EvaluationMetadata)context.Metadata).Intervals.Add(new IntervalContext(context.Rule, context.Interval, dts, dte, amount, se));
    }

    public static IEnumerable<IntervalContext> GetIntervals(this EvaluationContext context)
    {
      return ((EvaluationMetadata)context.Metadata).Intervals;
    }

    public static IEnumerable<LimitationContext> GetLimitations(this EvaluationContext context)
    {
      return ((EvaluationMetadata)context.Metadata).Limitations;
    }

    public static bool InScope(this Limitation limitation, EvaluationContext context)
    {
      bool b = false;
      var rid = context.Rule.ID;
      switch (limitation.Scope)
      {
        case LimitationScope.Global:
          b = true;

          break;
        case LimitationScope.Rule:
          if (rid != Guid.Empty)
            b = (limitation.RuleID == rid);

          break;
        case LimitationScope.Interval:
          var iid = context.Interval.ID;
          if ((rid != Guid.Empty) && (iid != Guid.Empty))
            b = ((limitation.RuleID == rid) && (limitation.IntervalID == iid));

          break;
      }

      return b;
    }

    internal static bool IsEqual(int[] ia1, int[] ia2)
    {
      int length = ia1.Length;
      if (ia2.Length != length)
        return false;

      for (int i = 0; i < length; i++)
        if (ia1[i] != ia2[i])
          return false;

      return true;
    }

    internal static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IEnumerable<TKey> keys, IEnumerable<TValue> values)
    {
      if (values.Count() < keys.Count())
        throw new ArgumentException("Items and keys count must be equal");

      Dictionary<TKey, TValue> d = new Dictionary<TKey, TValue>();
      int i = 0;
      foreach (var k in keys)
        d.Add(k, values.ElementAt(i++));

      return d;
    }

    internal static IList<T> RemoveLast<T>(this IList<T> c)
    {
      if (c.Count > 0)
        c.RemoveAt(c.Count - 1);

      return c;
    }
  }
}