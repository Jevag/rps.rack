﻿using System;

namespace rps.Data
{
  /// <summary>
  /// Условия расчета
  /// </summary>
  [Flags]
  public enum CalculationOptions : int
  {
    /// <summary>
    /// Нет условий
    /// </summary>
    None = 0,

    /// <summary>
    /// Не учитывать ограничения
    /// </summary>
    NoLimitations = 1
  }
}