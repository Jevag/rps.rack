﻿using System;

namespace rps.Data
{
  /// <summary>
  /// Тип подитога
  /// </summary>
  internal enum SubTotalType : int
  {
    Normal = 0,
    Before = 1,
    After = 2
  }

  /// <summary>
  /// Подитог
  /// </summary>
  internal class SubTotal
  {
    public readonly int LimitationIndex;
    public readonly SubTotalType Type;
    public readonly DateTime TimeStart;
    public readonly DateTime TimeEnd;
    public readonly decimal Amount;

    #region [ properties ]

    public TimeSpan Duration
    {
      get { return TimeEnd - TimeStart; }
    }

    #endregion

    public SubTotal(int i, SubTotalType t, DateTime s, DateTime e, decimal a)
    {
      LimitationIndex = i;
      Type = t;
      TimeStart = s;
      TimeEnd = e;
      Amount = a;
    }

    public override string ToString()
    {
      return String.Format("[{0}] {1}, {2} - {3}, {4:C}", LimitationIndex, Type, TimeStart, TimeEnd, Amount);
    }
  }
}