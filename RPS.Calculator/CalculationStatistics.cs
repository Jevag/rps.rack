﻿using System;
using System.Diagnostics;

namespace rps.Data
{
  /// <summary>
  /// Статистика расчета
  /// </summary>
  public class CalculationStatistics : ICloneable
  {
    private TimeSpan _timeElapsed;
    private Stopwatch _sw;
    private int _intervalsProcessed;
    private int _rulesProcessed;
    private int _subTotalsProcessed;
    private decimal _discount;

    #region [ properties ]

    /// <summary>
    /// Возвращает время расчета
    /// </summary>
    public TimeSpan TimeElapsed
    {
      get { return _timeElapsed; }
    }

    /// <summary>
    /// Возвращает количество обработанных интервалов
    /// </summary>
    public int IntervalsProcessed
    {
      get { return _intervalsProcessed; }
      internal set { _intervalsProcessed = value; }
    }

    /// <summary>
    /// Возвращает количество обработанных правил
    /// </summary>
    public int RulesProcessed
    {
      get { return _rulesProcessed; }
      internal set { _rulesProcessed = value; }
    }

    /// <summary>
    /// Возвращает количество обработанных подитогов
    /// </summary>
    public int SubTotalsProcessed
    {
      get { return _subTotalsProcessed; }
      internal set { _subTotalsProcessed = value; }
    }

    /// <summary>
    /// Сумма скидки по ограничениям
    /// </summary>
    public decimal Discount
    {
      get { return _discount; }
      internal set { _discount = value; }
    }

    #endregion

    public CalculationStatistics()
    {
      _sw = new Stopwatch();
      Reset();
    }

    protected CalculationStatistics(CalculationStatistics cs)
    {
      _sw = new Stopwatch();
      _timeElapsed = cs.TimeElapsed;
      _intervalsProcessed = cs.IntervalsProcessed;
      _rulesProcessed = cs.RulesProcessed;
      _subTotalsProcessed = cs.SubTotalsProcessed;
      _discount = cs.Discount;
    }

    #region [ ICloneable ]

    public virtual object Clone()
    {
      return new CalculationStatistics(this);
    }

    #endregion
    public void Start()
    {
      Reset();
      _sw.Reset();
      _sw.Start();
    }

    public void Complete()
    {
      _sw.Stop();
      _timeElapsed = _sw.Elapsed;
    }

    protected virtual void Reset()
    {
      _timeElapsed = TimeSpan.Zero;
      _intervalsProcessed = 0;
      _rulesProcessed = 0;
      _subTotalsProcessed = 0;
      _discount = 0;
    }

    public override string ToString()
    {
      return _timeElapsed.ToString();
    }
  }
}