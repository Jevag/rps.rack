﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace rps.Data
{
  /// <summary>
  /// Контекст интервала
  /// </summary>
  internal class IntervalContext
  {
    public readonly Rule Rule;
    public readonly Interval Interval;
    public readonly DateTime TimeStart;
    public readonly DateTime TimeEnd;
    public readonly decimal Amount;
    public readonly bool SwitchExact;

    public IntervalContext(Rule r, Interval i, DateTime dts, DateTime dte, decimal amount, bool se)
    {
      Rule = r;
      Interval = i;
      TimeStart = dts;
      TimeEnd = dte;
      Amount = amount;
      SwitchExact = se;
    }

    public override string ToString()
    {
      return String.Format("{0}, {1} - {2}, {3:C}", Rule.Name, TimeStart, TimeEnd, Amount);
    }
  }

  /// <summary>
  /// Контекст ограничения
  /// </summary>
  internal class LimitationContext
  {
    public readonly Limitation Limitation;
    public readonly Dictionary<DateTime, List<SubTotal>> SubTotals;

    public LimitationContext(Limitation limitation)
    {
      Limitation = limitation;
      SubTotals = new Dictionary<DateTime, List<SubTotal>>();
    }

    public override string ToString()
    {
      return String.Format("{0} | {1} {2}",
        Limitation, SubTotals.Count,
        SubTotals.Select(s => String.Format("{0:C}", s.Value.Sum(u => u.Amount)).GetString(",")));
    }
  }

  /// <summary>
  /// Метаданные для вычисления
  /// </summary>
  internal class EvaluationMetadata
  {
    public readonly List<IntervalContext> Intervals;
    public readonly List<LimitationContext> Limitations;

    public static EvaluationMetadata Create(Calculator c, Tariff t)
    {
      var m = new EvaluationMetadata();
      if ((c.Options & CalculationOptions.NoLimitations) == 0)
        m.Limitations.AddRange(t.Limitations.Select(l => new LimitationContext(l)));

      return m;
    }

    private EvaluationMetadata()
    {
      Intervals = new List<IntervalContext>();
      Limitations = new List<LimitationContext>();
    }
  }
}