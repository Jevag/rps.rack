﻿#define METADATA

using System;
using System.Collections.Generic;
using System.Linq;

namespace rps.Data
{
  /// <summary>
  /// Калькулятор
  /// </summary>
  public class Calculator
  {
    #region [ const ]

    private const int InfiniteLoopCount = 100000;

    #endregion

    [ThreadStatic]
    private CalculationStatistics _statistics;
    private CalculationOptions _options;

    #region [ properties ]

    public CalculationOptions Options
    {
      get { return _options; }
      set { _options = value; }
    }

    #endregion

    protected Calculator(CalculationOptions options)
    {
      _options = options;
    }

    public Calculator()
    {
      _statistics = null;
    }

    public CalculationResult Calculate(Tariff t, DateTime dateEntry, DateTime dateExitAssumed)
    {
      if (dateExitAssumed < dateEntry)
        throw new ArgumentException("Дата выезда должна быть не меньше даты въезда");

      EvaluationContext c = new EvaluationContext(t, dateEntry, dateExitAssumed, EvaluationMetadata.Create(this, t));
      LogMessage(0, "[ context created ] context = {0}", c);

      _statistics = new CalculationStatistics();
      _statistics.Start();
      try
      {
        while (!c.IsClosed)
        {
                    //check all rules
          var r1 = t.Rules.Select(u => u.Match.Evaluate(c));
          var a = t.Rules.ToDictionary(r1);
          Rule r = a.Where(kvp => kvp.Value).Select(kvp => kvp.Key).FirstOrDefault();
          if (r == null)
          {
            //no one begins, get nearest
            var d = DateTime.MaxValue;
            foreach (var k in a)
            {
              var x = k.Value.TimeNext;
              if (x.HasValue && x < d)
              {
                r = k.Key;
                d = x.Value;
              }
            }

            //advance current time to next rule
            if (r != null)
            {
              TimeSpan j = d - c.TimeCurrent;
              if (j < TimeSpan.Zero)
                throw new ApplicationException(String.Format("Can't advance back : {0} + {1}", c, j));

              c.Next(j);
              LogMessage(1, "[ context advanced ] current time = {0}", d);
            }
          }

          //no rule found, exit
          if (r == null)
          {
            LogMessage(1, "[ break ] no more rules...");
            break;
          }

          LogMessage(1, "[ rule selected ] rule = {0}", r);

          //process rule
          c.Rule = r;
          c.TimeRule = c.TimeCurrent;
          try
          {
            ProcessRule(c);
          }
          finally
          {
            _statistics.RulesProcessed++;
            c.Rule = null;
          }

          //check infinite loop
          if (_statistics.RulesProcessed > InfiniteLoopCount)
            throw new ApplicationException(String.Format("Infinite loop detected in core : {0}", c));
        }

#if METADATA
        //process limitations
        ProcessLimitations(c);
#endif

        //process subtotals
        ProcessSubTotals(c);
      }
      finally
      {
        _statistics.Complete();
      }

      LogMessage(0, "[ completed ] context = {0}", c);

      return new CalculationResult(c.Amount, dateExitAssumed.TrimMilliseconds(), (CalculationStatistics)_statistics.Clone());
    }

    private void ProcessRule(EvaluationContext c)
    {
      Rule r = c.Rule;
      Interval[] ia = r.Intervals.Normalize().ToArray();
      foreach (Interval i in ia)
      {
        c.Interval = i;
        try
        {
          //check context ready
          if (c.IsClosed)
          {
            LogMessage(2, "[ context closed ] context = {0}", c);
            break;
          }

          //check rule still actual
          var u = r.Match.Evaluate(c);
          if (!u)
          {
            LogMessage(2, "[ rule exit ] context = {0}", c);
            break;
          }

          //release shift
          if (c.ReleaseShift())
            LogMessage(2, "[ shift released ] context = {0}", c);

          //start processing
          LogMessage(2, "[ processing interval ] interval = {0}", i);

          //get absolute max duration
          TimeSpan td = i.Duration;
          TimeSpan tm = c.TimeEnd - c.TimeCurrent;
          if ((td > tm) || (td == Interval.Infinite)) td = tm;

          if (i.Duration == Interval.Infinite)
            LogMessage(2, "[ infinite interval ] max duration = {0}", td);

          //adjust duration
          bool skip = false;
          if (u.TimeMax.HasValue)
          {
            //get normal duration
            tm = u.TimeMax.Value - c.TimeCurrent;

            //respect switch mode
            bool switching = false;
            c.SwitchExact = false;
            SwitchMode w = c.GetSwitchMode();
            //check interval is last
            using (c.TryNext(td > tm ? tm : td))
              if (!r.Match.Evaluate(c))
              {
                TimeSpan tp = c.GetProtection();
                if (tm < tp)
                {
                  //switch overridden by protection
                  //switching = false;
                  //skip = true;
                  w = SwitchMode.NextPriority;
                  LogMessage(2, "[ rule switch override ] protection time = {0}", tp);
                }

                //switching
                switching = true;
                
                //check difference
                TimeSpan tf = TimeSpan.Zero;
                if (i.Period < tm)
                  tf = TimeSpan.FromSeconds(i.Period.TotalSeconds * Math.Ceiling(tm.TotalSeconds / i.Period.TotalSeconds));

                //normal switch
                switch (w)
                {
                  case SwitchMode.PreviousPriority:
                    if (i.Period > tm)
                      tm = i.Period;
                    else if (tf > TimeSpan.Zero)
                      tm = tf;

                    break;
                  case SwitchMode.NextPriority:
                    if ((i.Period > tm) || (tf > TimeSpan.Zero))
                      if (c.SetShift())
                      {
                        LogMessage(2, "[ shift set ] time = {0}", c.TimePrevious);
                        skip = true;
                      }
                    break;
                  case SwitchMode.Match:
                    c.SwitchExact = true;
                    break;
                }
            }

            if (switching)
              LogMessage(2, "[ rule switch ] mode = {0}", w);

            //adjust duration
            if (td > tm) td = tm;
          }

          //set duration
          c.Duration = td;
          LogMessage(2, "[ duration set ] actual duration = {0}", td);

          if (!skip)
          {
            //process interval and add to total
            decimal a = ProcessInterval(c);
            c.Amount += a;

            //save results
            c.AddInterval(c.TimeCurrent, c.TimeCurrent + td, a, c.SwitchExact);
            _statistics.IntervalsProcessed++;

#if !METADATA
          //process limitations
          ProcessLimitations(c);
#endif
          }
          else
            LogMessage(2, "[ interval skipped to next ]");

          //advance context
          c.Next(td, !skip);
          LogMessage(2, "[ context advanced ] context = {0}", c);

#if !METADATA
          //catch the rest inner limitations
          if (c.IsClosed && !skip)
            ProcessLimitations(c);
#endif
        }
        finally
        {
          c.Interval = null;
        }
      }
    }

    private decimal ProcessInterval(EvaluationContext c)
    {
      decimal a = 0;
      Rule r = c.Rule;
      Interval i = c.Interval;
      TimeSpan td = c.Duration;

      //prepare vars
      int d = (int)td.TotalSeconds;
      int p = Math.Max(1, (int)i.Period.TotalSeconds);
      double e = (double)d / p;
      int f = d % p;
      int n = (p == 0) ? 0 : (int)e;

      //check initial
      TimeSpan ti = c.GetInitial();
      if ((td > ti) || ((c.TimeEnd - c.TimeCurrent) > ti))
      {
        //check protection
        TimeSpan tp = c.GetProtection();
        if (td > tp)
        {
          //apply protection
          if (f <= tp.TotalSeconds)
          {
            if ((f != 0) || (e != n))
              LogMessage(3, "[ protection used ] time = {0}", tp);

            f = 0;
            e = n;
          }

          //calculate
          IntervalProcessingMode m = c.GetProcessingMode();
          LogMessage(3, "[ processing interval ] mode = {0}", m);
          switch (m)
          {
            case IntervalProcessingMode.Maximum:
              a += n * i.Amount;
              if (f != 0)
                a += i.Amount;
              break;
            case IntervalProcessingMode.Exact:
              a += (decimal)(e * (double)i.Amount);
              break;
            case IntervalProcessingMode.Integer:
              a += n * i.Amount;
              break;
          }
        }
        else
          //protection only
          LogMessage(3, "[ protection only used ] time = {0}", tp);
      }
      else
      {
        //initial only
        a = c.GetInitialAmount();
        LogMessage(3, "[ initial used ] time = {0}", ti);
      }

      //done
      LogMessage(3, "[ amount calculated ] amount = {0:C}", a);

      return a;
    }

    #region [ limitations ]

#if METADATA

    private void ProcessLimitations(EvaluationContext c)
    {
      IntervalContext[] ia = c.GetIntervals().ToArray();
      if (ia.Length == 0)
        return;
      
      LogMessage(1, "[ processing limitations ] intervals = {0}", ia.Length);

      foreach (var x in c.GetLimitations())
      {
        //process limitations
        Limitation l = x.Limitation;
        c.Limitation = l;
        LogMessage(1, "[ limitation begin ] limitation = {0}", l);
        try
        {
          int n = 0;
          while (n < l.Repeat)
          {
            //next chain
            c.RepeatIndex = n;
            LogMessage(2, "[ repeat ] index = {0}", n);

            //intervals
            EvaluationResult e = null;
            IntervalContext ip = null;
            bool scope = false;
            bool ended = false;
            for (int i = 0; i < ia.Length; i++)
            {
              //prepare context
              IntervalContext ic = ia[i];
              PrepareContext(c, ic, ip);

              //check scope
              if (l.InScope(c))
              {
                scope = true;

                //evaluate limitation
                e = l.Match.Evaluate(c);
                LogMessage(2, "[ evaluated ] interval = {0}, result = {1}", i, e);

                DateTime dts = e.TimeStart.HasValue ? e.TimeStart.Value : DateTime.MaxValue;
                DateTime dte = e.TimeMax.HasValue ? e.TimeMax.Value : (e.TimeNext.HasValue ? e.TimeNext.Value : dts);
                List<SubTotal> ss = null;
                SubTotal s = null;
                //interval match
                if (e)
                {
                  if (dts < ic.TimeEnd)
                  {
                    //get or create new chain
                    if (!x.SubTotals.TryGetValue(dts, out ss))
                    {
                      ss = new List<SubTotal>();
                      x.SubTotals.Add(dts, ss);
                      LogMessage(2, "[ subtotal chain created ] key = {0}", dts);

                      //check before
                      if (dts > c.TimeCurrent)
                      {
                        c.Duration = dts - c.TimeCurrent;
                        s = new SubTotal(n, SubTotalType.Before, c.TimeCurrent, dts, ProcessInterval(c));
                        ss.Add(s);
                        LogMessage(3, "[ subtotal before ] subtotal = {0}", s);
                      }
                    }

                    //add normal
                    //maybe before, so take maximun start time
                    DateTime dtns = (dts > ic.TimeStart) ? dts : ic.TimeStart;
                    //maybe after, so take minimum end time
                    DateTime dtne = (dte < ic.TimeEnd) ? dte : ic.TimeEnd;
                    c.Duration = dtne - dtns;
                    s = new SubTotal(n, SubTotalType.Normal, dtns, dtne, ProcessInterval(c));
                    ss.Add(s);
                    LogMessage(3, "[ subtotal added ] subtotal = {0}", s);

                    //check exact end
                    if (dte == dtne) //ic.TimeEnd)
                    {
                      LogMessage(2, "[ subtotal chain ended exact ] key = {0}", dts);
                      break;
                    }
                  }
                }
                else
                {
                  //ensure chain started
                  if (x.SubTotals.TryGetValue(dts, out ss))
                  {
                    //check after
                    s = ss.Last();
                    if (s.Type != SubTotalType.After)
                    {
                      //prepare previous context, ignore TimeRule
                      PrepareContext(c, ip, null);
                      //adjust duration to skip last normal
                      c.Duration = ic.TimeStart - s.TimeEnd;
                      s = new SubTotal(n, SubTotalType.After, s.TimeEnd, ic.TimeStart, ProcessInterval(c));
                      x.SubTotals[dts].Add(s);
                      LogMessage(3, "[ subtotal after ] subtotal = {0}", s);
                    }

                    //done
                    LogMessage(2, "[ subtotal chain ended ] key = {0}", dts);
                    //ended = true;
                    break;
                  }
                  else if (dts > c.TimeEnd)
                  {
                    //check we out of time
                    LogMessage(2, "[ subtotal out of time ] start = {0}", dts);
                    ended = true;
                    break;
                  }
                }
              }

              //update previous
              ip = ic;
            }

            //not in scope, next limitation
            if (!scope || ended)
              break;

            //next chain
            n++;

            //check infinite loop
            if (n > InfiniteLoopCount)
              throw new ApplicationException(String.Format("Infinite loop detected in limitations : {0}", x));
          }
        }
        finally
        {
          LogMessage(1, "[ limitation end ] limitation = {0}", l);

          c.Limitation = null;
        }
      }
    }

    private void PrepareContext(EvaluationContext c, IntervalContext ic, IntervalContext ip)
    {
      c.Rule = ic.Rule;
      c.Interval = ic.Interval;
      c.TimeCurrent = ic.TimeStart;
      if (ip == null || ic.Rule != ip.Rule) c.TimeRule = ic.TimeStart;
      //c.TimePrevious = (ip != null) ? ip.TimeStart : ic.TimeStart;
      c.Duration = ic.TimeEnd - ic.TimeStart;
    }

#else

    private void ProcessLimitations(EvaluationContext c)
    {
      TimeSpan td = c.Duration;
      DateTime dtsi = c.TimeCurrent;
      DateTime dtei = dtsi + td;
      foreach (var x in c.GetLimitations().ByContext(c))
      {
        Limitation l = x.Limitation;
        c.Limitation = l;
        try
        {
          //process chains
          int n = 0;
          while (n < l.Repeat)
          {
            //evaluate match
            c.LimitationIndex = n;
            EvaluationResult e = l.Match.Evaluate(c);

            //break if we out of interval
            DateTime dts = e.TimeStart.HasValue ? e.TimeStart.Value : DateTime.MaxValue;
            if (dts >= dtei)
              break;

            DateTime dte = e.TimeMax.HasValue ? e.TimeMax.Value : dts;
            List<SubTotal> ss = null;
            SubTotal s = null;
            DateTime dtu;
            //match, normal or cross chain
            if (e)
            {
              //create new chain
              if (!x.SubTotals.TryGetValue(dts, out ss))
              {
                ss = new List<SubTotal>();
                x.SubTotals.Add(dts, ss);
                LogMessage(3, "[ subtotal chain created ] key = {0}", dts);
              }

              //get current chain time
              dtu = ss.Any() ? ss.Last().TimeEnd : dts;

              //add subtotals on chain start
              if (dtu < dtsi)
              {
                //before
                if (dtu == dts)
                {
                  //exclude start
                  c.Duration = dtu - c.TimePrevious;
                  if (c.Duration > TimeSpan.Zero)
                  {
                    s = new SubTotal(n, SubTotalType.Before, c.TimePrevious, dtu, ProcessInterval(c));
                    ss.Add(s);
                    LogMessage(3, "[ subtotal before ] subtotal = {0}", s);
                  }
                }
                //first
                if (dtu > c.TimeStart)
                {
                  c.Duration = dtsi - dtu; //use previous rule !!!
                  s = new SubTotal(n, SubTotalType.Normal, dtu, dtsi, ProcessInterval(c));
                  ss.Add(s);
                  LogMessage(3, "[ subtotal added ] subtotal = {0}", s);
                }
              }

              //add subtotal within current chain and interval
              if ((dtu < dte) && (dtu < dtei)) // && (dte < c.TimeEnd))
              {
                //advance to limitation end (inner)
                dtu = dte;
                //or interval end (exact)
                if (dtu > dtei)
                  dtu = dtei;
                //check global end
                if (dtu > c.TimeEnd)
                  dtu = c.TimeEnd;

                //add subtotal
                c.Duration = dtu - dtsi;
                if (c.Duration > TimeSpan.Zero)
                {
                  s = new SubTotal(n, SubTotalType.Normal, dtsi, dtu, ProcessInterval(c));
                  ss.Add(s);
                  LogMessage(3, "[ subtotal added ] subtotal = {0}", s);
                }
              }

              //chain end reached
              if (dtu == dte)
              {
                //subtotal after interval end
                if (dtu < dtei)
                {
                  c.Duration = dtei - dtu;
                  s = new SubTotal(n, SubTotalType.After, dtu, dtei, ProcessInterval(c));
                  ss.Add(s);
                  LogMessage(3, "[ subtotal after ] subtotal = {0}", s);
                }

                //end of chain
                LogMessage(3, "[ subtotal chain ended ] key = {0}", dts);
              }
            }
            //not match, check inner
            else if (dts >= c.TimeStart)
            {
              //get assumed end time
              dte = l.Match.GetTimeMax(dts) ?? dts;
              //check day change, to catch inner before interval ends
              bool b = (dte.Date > dts.Date);
              if ((dts < dtsi) || b)
              {
                //check inner
                if ((dte <= dtei) && (dte > dts))
                {
                  //create new chain
                  if (!x.SubTotals.TryGetValue(dts, out ss))
                  {
                    ss = new List<SubTotal>();
                    x.SubTotals.Add(dts, ss);
                    LogMessage(3, "[ subtotal chain created ] key = {0}", dts);
                  }

                  //check already added
                  if (!ss.Any(u => u.LimitationIndex == n))
                  {
                    //get current chain time
                    dtu = ss.Any() ? ss.Last().TimeEnd : dts;

                    //add before
                    DateTime z = dtsi > dtu ? c.TimePrevious : dtsi;
                    c.Duration = dtu - z;
                    s = new SubTotal(n, SubTotalType.Before, z, dtu, ProcessInterval(c));
                    ss.Add(s);
                    LogMessage(3, "[ subtotal before ] subtotal = {0}", s);

                    //add inner
                    c.Duration = dte - dtu;
                    s = new SubTotal(n, SubTotalType.Normal, dtu, dte, ProcessInterval(c));
                    ss.Add(s);
                    LogMessage(3, "[ subtotal added ] subtotal = {0}", s);

                    //add after
                    z = b ? dtei : dtsi;
                    if (dte < z)
                    {
                      c.Duration = z - dte;
                      s = new SubTotal(n, SubTotalType.After, dte, z, ProcessInterval(c));
                      ss.Add(s);
                      LogMessage(3, "[ subtotal after ] subtotal = {0}", s);
                    }

                    //end of chain
                    if ((l.Repeat - n) == 1)
                      LogMessage(3, "[ subtotal chain ended ] key = {0}", dts);
                  }
                }
              }
            }

            //repeat
            n++;

            //check infinite loop
            if (n > InfiniteLoopCount)
              throw new ApplicationException(String.Format("Infinite loop detected in limitations : {0}", x));
          }
        }
        finally
        {
          c.Limitation = null;
        }
      }
    }

#endif

    private void ProcessSubTotals(EvaluationContext c)
    {
      IEnumerable<LimitationContext> lcs = c.GetLimitations();
      if (!lcs.Any())
        return;

      LogMessage(1, "[ processing subtotals ] count = {0}", lcs.Sum(l => l.SubTotals.Count()));

      decimal d = 0;
      foreach (var v in lcs)
      {
        c.Limitation = v.Limitation;
        try
        {
          foreach (var u in v.SubTotals)
          {
            List<SubTotal> ss = new List<SubTotal>();
            int k = 0;
            //before
            SwitchMode w = c.GetLimitationSwitchMode(v.Limitation.BeforeMode);
            SubTotal sba = u.Value.FirstOrDefault(s => s.Type == SubTotalType.Before);
            if (sba != null)
              if (w != SwitchMode.PreviousPriority)
              {
                k++;
                if (w == SwitchMode.NextPriority) //&& (u.Value.Count > 1))
                  k++;
              }

            //rest
            ss.AddRange(u.Value.Skip(k));

            //after
            w = c.GetLimitationSwitchMode(v.Limitation.AfterMode);
            sba = u.Value.LastOrDefault(s => s.Type == SubTotalType.After);
            if (sba != null)
              if (w != SwitchMode.NextPriority)
              {
                ss.RemoveLast();
                if (w == SwitchMode.PreviousPriority)
                  ss.RemoveLast();
              }

            //count
            if (ss.Any())
            {
              decimal x = ss.Sum(s => s.Amount) - v.Limitation.Amount;
              if (x > 0)
              {
                d += x;
                c.Amount -= x;
                _statistics.SubTotalsProcessed++;
              }
            }
            else
              LogMessage(1, "[ subtotal truncated ] key = {0}, limitation = {1}", u.Key, v);
          }
        }
        finally
        {
          c.Limitation = null;
        }
      }

      _statistics.Discount = d;
      LogMessage(1, "[ subtotals done ] discount = {0:C}", d);
    }

    #endregion

    private void LogMessage(int level, params object[] values)
    {
      LogMessageCore(new string('-', level * 3) + '>', String.Format((string)values[0], values.Skip(1).ToArray()));
    }

    protected virtual void LogMessageCore(string prefix, string message)
    {
#if DEBUG
      System.Diagnostics.Trace.WriteLine(prefix + message);
#endif
    }
  }
}