﻿using System;

namespace rps.Data
{
  /// <summary>
  /// Результаты расчета
  /// </summary>
  public sealed class CalculationResult
  {
    /// <summary>
    /// Сумма к оплате
    /// </summary>
    public readonly decimal Amount;

    /// <summary>
    /// Ориентировочное время выезда
    /// </summary>
    public readonly DateTime DateExitEstimated;

    /// <summary>
    /// Статистика расчета
    /// </summary>
    public readonly CalculationStatistics Statistics;

    /// <summary>
    /// Оператор преобразования в decimal
    /// </summary>
    public static explicit operator decimal(CalculationResult result)
    {
      return result.Amount;
    }

    public CalculationResult(decimal amount, DateTime dateExitEstimated)
      : this(amount, dateExitEstimated, new CalculationStatistics())
    {
      //
    }

    public CalculationResult(decimal amount, DateTime dateExitEstimated, CalculationStatistics statistics)
	  {
      Amount = amount;
      DateExitEstimated = dateExitEstimated;
      Statistics = statistics;
	  }
  }
}