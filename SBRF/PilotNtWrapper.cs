﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class PilotNtWrapper
    {
        private const string m_sDllPath = "C:\\SBRF\\pilot_nt.dll";

        #region constants
      
        private const int MAX_AUTH_CODE    = 6;                ///< Длина кода авторизации
        private const int MAX_AUTHCODE     = MAX_AUTH_CODE+1;  ///< Длина кода авторизации с учетом завершающего ноля
        private const int MAX_RRN          = 12;               ///< Длина номера ссылки/идентификатора смены
        private const int MAX_REFNUM       = MAX_RRN+1;        ///< Длина номера ссылки с учетом завершающего ноля
        private const int MAX_TERM         = 8;                ///< Длина номера терминала
        private const int MAX_TERMID       = MAX_TERM+1;       ///< Длина номера терминала с учетом завершающего ноля
        private const int MAX_PAN_N        = 19;               ///< Длинна PAN
        private const int MAX_PAN          = MAX_PAN_N+1;      ///< Длинна PAN с учетом завершающего ноля
        private const int MAX_VALTHRU      = 5+1;
        private const int MAX_CURRCODE     = 3+1;
        private const int MAX_REPLY        = 2+1;

        //Константы, используемые при определении структур
        private const int MAX_CARD_NUM     = 30;  ///< Длина номера карты
        private const int MAX_ACCOUNT_NUM  = 11;  ///< Длина номера счета (Сберкарт)
        private const int MAX_BRANCH_NUM   = 11;  ///< Длина кода банка (Сберкарт)
        private const int MAX_CERT         = 16;  ///< Длина сертификата транзакции
        private const int MAX_NAME         = 38;  ///< Длина названия карты (Visa, Maestro, ...)
        private const int MAX_DATE         = 10;  ///< Длина строки даты (ДД.ММ.ГГГГ)
        private const int MAX_TIME         = 8;  ///< Длина времени операции (ЧЧ:ММ:СС)
        private const int MAX_ENCR_DATA    = 32*2 + 1;                                          ///MAX_ENCR_DATA

        private const int MAX_MERCHANT_LN  = 15;  ///< Длина номера мерчанта
        private const int MAX_OPER_NAME    = 64;  ///< Длина имени пользователя

        private const int MAX_HASH         = 20;  ///< Длинна хеша номера карты
        private const int MAX_CARD_DATE    = 4;   ///< Длинна даты выпуска карты MMYY
        private const int MAX_PASSPORT     = 512; ///< Длинна паспортных данных
        private const int MAX_PAYMENT_TAG  = 32;  ///< Тэг дополнительных реквизитов платежей для передачи в банк
        private const int MAX_PAYMENT_VALUE= 200; ///< Значение дополнительных реквизитов платежей для передачи в банк
        private const int MAX_CARD_LS_DATA = 800; ///< Размер буфера данных для экранной подписи
        private const int MAX_TEXT_MESSAGE = 700; ///< Размер максимального текстового сообщения
        private const int MAX_CLIENT_NAME  = 80; ///< Имя владельца карты
        private const int MAX_GOODS_NAME   = 25; ///< Максимальная длина имени товара
        private const int MAX_GOODS_CODE   = 24; ///< Максимальная длина кода товара
        private const int MAX_CASH_STATS   = 24; ///< Длинна полей для передачи информации о наличном обороте
        private const int MAX_FIO_N        = 58; ///< Длинна имени кассира
        private const int CARD_ID_LEN      = 25; ///< Длина буфера номера карты в структурах библиотеки pilot_nt.dll
        private const int CARD_HASH_LEN    = 41; ///< Длина буфера хеша карты в структурах библиотеки pilot_nt.dll
        private const int TRANSDATE_LEN    = 20; ///< Размер даты и времени операции в структурах библиотеки pilot_nt.dll
        private const int CARD_TRACK3_LEN  = 107;
        #endregion

        #region structs

        // Structs
        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        public struct AuthAnswer
        {
            public int TType;       // int              // in
            public uint Amount;     // unsigned long    // in 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 3)]
            public string RCode;    // char[3]          // out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 16)]
            public string AMessage; // char[16]         // out
            public int CType;       // int              // in/out
            public IntPtr Check;    // char*            // out

            public override string ToString()
            {
                string nRes = "*** AuthAnswer ***" + Environment.NewLine;

                nRes += "TType = "      + TType.ToString() + Environment.NewLine;
                nRes += "nAmount = "    + Amount.ToString() + Environment.NewLine;
                nRes += "RCode = "      + RCode + Environment.NewLine;
                nRes += "AMessage = "   + AMessage + Environment.NewLine;
                nRes += "CType = "      + CType.ToString() + Environment.NewLine;

                try
                {
                    if (Check != null && RCode != null /*&& Convert.ToInt32(RCode) == 0*/)
                        nRes += Marshal.PtrToStringAnsi(Check) + Environment.NewLine;
                }
                catch (Exception e)
                {
                
                }
                
                return nRes;
            }
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct AuthAnswer2
        {
            public AuthAnswer ans;                              // in/out 
            
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_AUTHCODE)]
            public string AuthCode;     // char[MAX_AUTHCODE]   // out 

            public override string ToString()
            {
                string nRes = "*** AuthAnswer2 ***" + Environment.NewLine;

                nRes += "AuthCode = " + AuthCode + Environment.NewLine;

                nRes += ans.ToString();

                return nRes;
            }
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct AuthAnswer3
        {
            public AuthAnswer ans;                              // in/out
 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_AUTHCODE)]
            public string AuthCode;     // char[MAX_AUTHCODE]   // out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_ID_LEN)]
            public string CardID;       // char[CARD_ID_LEN]    // out 

            public override string ToString()
            {
                string nRes = "*** AuthAnswer3 ***" + Environment.NewLine;

                nRes += "AuthCode = "   + AuthCode + Environment.NewLine;
                nRes += "CardID = "     + CardID + Environment.NewLine;

                nRes += ans.ToString();

                return nRes;
            }
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct AuthAnswer4
        {
            public AuthAnswer ans;                              // in/out 

            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_AUTHCODE)]
            public string AuthCode;     // char[MAX_AUTHCODE]   // out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_ID_LEN)]
            public string CardID;       // char[CARD_ID_LEN]    // out 
            public int ErrorCode;       // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = TRANSDATE_LEN)]
            public string TransDate;    // char[TRANSDATE_LEN]  // out
            public int TransNumber;     // int                  // out

            public override string ToString()
            {
                string nRes = "*** AuthAnswer4 ***" + Environment.NewLine;

                nRes += "AuthCode = " + AuthCode + Environment.NewLine;
                nRes += "CardID = " + CardID + Environment.NewLine;
                nRes += "ErrorCode = " + ErrorCode.ToString() + Environment.NewLine;
                nRes += "TransDate = " + TransDate + Environment.NewLine;
                nRes += "TransNumber = " + TransNumber + Environment.NewLine;

                nRes += ans.ToString();

                return nRes;
            }
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct AuthAnswer5
        {
            public AuthAnswer ans;                              // in/out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_REFNUM)]
            public string RRN;          // char[MAX_REFNUM]     // in/out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_AUTHCODE)]
            public string AuthCode;     // char[MAX_AUTHCODE]   // out 

            public override string ToString()
            {
                string nRes = "*** AuthAnswer5 ***" + Environment.NewLine;

                nRes += "RRN=" + RRN + Environment.NewLine;
                nRes += "AuthCode=" + AuthCode + Environment.NewLine;

                nRes += ans.ToString(); 

                return nRes;
            }
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        public struct AuthAnswer6
        {
            public AuthAnswer ans;                                  // in/out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_AUTHCODE)]
            public string AuthCode;         // char[MAX_AUTHCODE]   // out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_ID_LEN)]
            public string CardID;           // char[CARD_ID_LEN]    // out 
            public int ErrorCode;           // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = TRANSDATE_LEN)]
            public string TransDate;        // char[TRANSDATE_LEN]  // out
            public int TransNumber;         // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_REFNUM)]
            public string RRN;              // char[MAX_REFNUM]     // in/out

            public override string ToString()
            {
                string nRes = "*** AuthAnswer6 ***" + Environment.NewLine;

                nRes += "RRN=" + RRN + Environment.NewLine;
                nRes += "AuthCode=" + AuthCode + Environment.NewLine;

                nRes += ans.ToString();

                return nRes;
            }
        }   

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct AuthAnswer7
        {
            public AuthAnswer   ans;                                  // in/out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_AUTHCODE)]
            public string       AuthCode;     // char[MAX_AUTHCODE]   // out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_ID_LEN)]
            public string       CardID;       // char[CARD_ID_LEN]    // out 
            public int          SberOwnCard;  // int                  // out

            public override string ToString()
            {
                string nRes = "*** AuthAnswer7 ***" + Environment.NewLine;

                nRes += "AuthCode=" + AuthCode + Environment.NewLine;

                nRes += ans.ToString();

                return nRes;
            }
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct AuthAnswer8
        {
            public AuthAnswer ans;                                  // in/out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_AUTHCODE)]
            public string AuthCode;         // char[MAX_AUTHCODE]   // out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_ID_LEN)]
            public string CardID;           // char[CARD_ID_LEN]    // out 
            public int ErrorCode;           // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = TRANSDATE_LEN)]
            public string TransDate;        // char[TRANSDATE_LEN]  // out
            public int TransNumber;         // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_REFNUM)]
            public string RRN;              // char[MAX_REFNUM]     // in/out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_ENCR_DATA)]
            public string EncryptedData;    // char[MAX_ENCR_DATA]  // in/out

            public override string ToString()
            {
                string nRes = "*** AuthAnswer8 ***" + Environment.NewLine;

                nRes += "RRN=" + RRN + Environment.NewLine;
                nRes += "AuthCode=" + AuthCode + Environment.NewLine;

                nRes += ans.ToString();

                return nRes;
            }
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct AuthAnswer9
        {
            public AuthAnswer   ans;                                  // in/out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_AUTHCODE)]
            public string       AuthCode;     // char[MAX_AUTHCODE]   // out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_ID_LEN)]
            public string       CardID;       // char[CARD_ID_LEN]    // out 
            public int          SberOwnCard;  // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_HASH_LEN)]
            public string       Hash;         // char[CARD_HASH_LEN]  // in/out

            public override string ToString()
            {
                string nRes = "*** AuthAnswer9 ***" + Environment.NewLine;

                nRes += "AuthCode=" + AuthCode + Environment.NewLine;
                nRes += "Hash=" + Hash + Environment.NewLine;

                nRes += ans.ToString();

                return nRes;
            }
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct AuthAnswer10
        {
            public AuthAnswer   ans;                                  // in/out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_AUTHCODE)]
            public string       AuthCode;     // char[MAX_AUTHCODE]   // out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_ID_LEN)]
            public string       CardID;       // char[CARD_ID_LEN]    // out 
            public int          ErrorCode;    // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = TRANSDATE_LEN)]
            public string       TransDate;    // char[TRANSDATE_LEN]  // out
            public int          TransNumber;  // int                  // out
            public int          SberOwnCard;  // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_HASH_LEN)]
            public string       Hash;         // char[CARD_HASH_LEN]  // in/out

            public override string ToString()
            {
                string nRes = "*** AuthAnswer10 ***" + Environment.NewLine;

                nRes += "AuthCode=" + AuthCode + Environment.NewLine;
                nRes += "Hash=" + Hash + Environment.NewLine;

                nRes += ans.ToString();

                return nRes;
            }
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct AuthAnswer11
        {
            public AuthAnswer ans;                                  // in/out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_AUTHCODE)]
            public string AuthCode;         // char[MAX_AUTHCODE]   // out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_ID_LEN)]
            public string CardID;           // char[CARD_ID_LEN]    // out 
            public int ErrorCode;           // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = TRANSDATE_LEN)]
            public string TransDate;        // char[TRANSDATE_LEN]  // out
            public int TransNumber;         // int                  // out
            public int SberOwnCard;         // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_HASH_LEN)]
            public string Hash;             // char[CARD_HASH_LEN]  // in/out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_TRACK3_LEN)]
            public string Track3;           // char[CARD_TRACK3_LEN]// out
            public uint RequestID;          // unsigned long        // in 

            public override string ToString()
            {
                string nRes = "*** AuthAnswer11 ***" + Environment.NewLine;

                nRes += "AuthCode=" + AuthCode + Environment.NewLine;
                nRes += "Hash=" + Hash + Environment.NewLine;

                nRes += ans.ToString();

                return nRes;
            }
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct AuthAnswer12
        {
            public AuthAnswer ans;                                  // in/out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_AUTHCODE)]
            public string AuthCode;         // char[MAX_AUTHCODE]   // out 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_ID_LEN)]
            public string CardID;           // char[CARD_ID_LEN]    // out 
            public int ErrorCode;           // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = TRANSDATE_LEN)]
            public string TransDate;        // char[TRANSDATE_LEN]  // out
            public int TransNumber;         // int                  // out
            public int SberOwnCard;         // int                  // out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_HASH_LEN)]
            public string Hash;             // char[CARD_HASH_LEN]  // in/out
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = CARD_TRACK3_LEN)]
            public string Track3;           // char[CARD_TRACK3_LEN]// out
            public uint RequestID;          // unsigned long        // in 
            public uint Department;         // unsigned long        // in 
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_REFNUM)]
            public string RRN;              // char[MAX_REFNUM]     // in/out

            public override string ToString()
            {
                string nRes = "*** AuthAnswer12 ***" + Environment.NewLine;

                nRes += "RRN=" + RRN + Environment.NewLine;
                nRes += "AuthCode=" + AuthCode + Environment.NewLine;
                nRes += "Hash=" + Hash + Environment.NewLine;

                nRes += ans.ToString();

                return nRes;
            }
        }
        #endregion

        #region addfunctions
        //Additional functions
        [DllImportAttribute(m_sDllPath, EntryPoint = "_GetTerminalID", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetTerminalID(IntPtr pTerminalID);

        [DllImportAttribute(m_sDllPath, EntryPoint = "_ServiceMenu", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ServiceMenu(ref AuthAnswer auth_ans);

        [DllImportAttribute(m_sDllPath, EntryPoint = "_close_day", CallingConvention = CallingConvention.Cdecl)]
        public static extern int close_day(ref AuthAnswer auth_ans);

        [DllImportAttribute(m_sDllPath, EntryPoint = "_close_day_info", CallingConvention = CallingConvention.Cdecl)]
        public static extern int close_day_info(ref AuthAnswer auth_ans, IntPtr pParams);

        [DllImportAttribute(m_sDllPath, EntryPoint = "_get_statistics", CallingConvention = CallingConvention.Cdecl)]
        public static extern int get_statistics(ref AuthAnswer auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_GetVer", CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt32 GetVersion();

        [DllImportAttribute(m_sDllPath, EntryPoint = "_SetConfigData", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetConfigData(IntPtr pConfData);

        [DllImportAttribute(m_sDllPath, EntryPoint = "_Done", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Done();

        [DllImportAttribute(m_sDllPath, EntryPoint = "_TestPinpad", CallingConvention = CallingConvention.Cdecl)]
        public static extern int TestPinpad();

        [DllImportAttribute(m_sDllPath, EntryPoint = "_SuspendTrx", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SuspendTrx(UInt32 nAmount, IntPtr pAuthCode);

        [DllImportAttribute(m_sDllPath, EntryPoint = "_CommitTrx", CallingConvention = CallingConvention.Cdecl)]
        public static extern int CommitTrx(UInt32 nAmount, IntPtr pAuthCode);

        [DllImportAttribute(m_sDllPath, EntryPoint = "_RollbackTrx", CallingConvention = CallingConvention.Cdecl)]
        public static extern int RollbackTrx(UInt32 nAmount, IntPtr pAuthCode);

        [DllImportAttribute(m_sDllPath, EntryPoint = "_AbortTransaction", CallingConvention = CallingConvention.Cdecl)]
        public static extern int AbortTransaction();

        [DllImportAttribute(m_sDllPath, EntryPoint = "_TestCard", CallingConvention = CallingConvention.Cdecl)]
        public static extern int TestCard();
        #endregion  

        #region functions
        //Functions
        [DllImport(m_sDllPath, EntryPoint = "_card_authorize", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize(byte[] track2, ref AuthAnswer auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_card_authorize2", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize2(byte[] track2, ref AuthAnswer2 auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_card_authorize3", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize3(byte[] track2, ref AuthAnswer3 auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_card_authorize4", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize4(byte[] track2, ref AuthAnswer4 auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_card_authorize5", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize5(byte[] track2, ref AuthAnswer5 auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_card_authorize6", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize6(byte[] track2, ref AuthAnswer6 auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_card_authorize7", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize7(byte[] track2, ref AuthAnswer7 auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_card_authorize8", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize8(byte[] track2, ref AuthAnswer8 auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_card_authorize9", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize9(byte[] track2, ref AuthAnswer9 auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_card_authorize10", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize10(byte[] track2, ref AuthAnswer10 auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_card_authorize11", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize11(byte[] track2, ref AuthAnswer11 auth_ans);

        [DllImport(m_sDllPath, EntryPoint = "_card_authorize12", CallingConvention = CallingConvention.Cdecl)]
        public static extern int card_authorize12(byte[] track2, ref AuthAnswer12 auth_ans);
        #endregion
    }
}
