﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonClassLibrary;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO;

namespace Communications
{

    // Attantion!
    // Maybe use conditional compilation symbols:
    // 1. RPS_ENTRANCE_EXIT_GATE  - only for entrance-exit gate solutions
    //    Include protaction of BM against "spam" for opeartions "Verification" and "Tehnical cancelation"                                     
    public class SBRF : CommonBankModule
    {

        private PilotNtWrapper pilotNtWrapper = new PilotNtWrapper();

        static string sStatusBeginOperation = "Пожалуйста, следуйте инструкциям на пин-паде";
        static string sStatusCardInserted = "Карта обнаружена, следуйте инструкциям на пин-паде";
        static string sStatusAbortOperation = "Отказ от оплаты картой";

        static int nResponseCodeCardInserted = 4353;
        static int nResponseCodeAbortOperation = 2000;

        static int nTimeoutWaitVerification = 90; // in seconds
        static int nTimeoutWaitTechnicalCancelation = 90; // in seconds

        string sLastRRN = "";
        uint sLastAmount = 0;
        enum BankModuleCommandT
        {
            Unknown,
            Purchase = 1,
            Cancell = 3,
            Verification = 7,
        }

        BankModuleCommandT bankModuleCommand = BankModuleCommandT.Unknown;
        private static Thread StatusThread = null;
        private static bool run = false;

        private static DateTime dtStartVerification = DateTime.MinValue;
        private static DateTime dtStartTechnicalCancelation = DateTime.MinValue;

        private static bool isOpened = false;

        private void releaseAuthSession()
        {
            isCancelled = false;
            AuthorizationStarted = false;
            TransactionStarted = false;
            cardInserted = false;
            cardWasInserted = false;
            toLog("DONE");
        }

        public SBRF(Log Log)
        {
            log = Log;
            type = BM_TYPE.SBRF;

            toLog("Session SBRF start");
        }
        ~SBRF()
        {
            toLog("Session SBRF finish");
        }

        ErrorCode SetResultCode(int Ret)
        {
            switch (Ret)
            {
                case 0:
                    return ErrorCode.Sucsess;
                default:
                    return ErrorCode.SBRF_PROC_ERROR;
            }
        }
        public override bool Open()
        {
            toLog("Start");
            toLog("isOpened = " + isOpened);

            resultCode = ErrorCode.Sucsess;
            resultDescription = "";
            deviceEnabled = true;
            BMStatus = BM_STATUS.WAITING_COMMAND;

            if (isOpened)
            {
                toLog("Already opened...");
                run = true;
                toLog("Stop; return always true");
                return true;
            }

            if (StatusThread != null)
            {
                ThreadState tsct = StatusThread.ThreadState;
                toLog("ThreadState Current Thread = " + tsct.ToString());
            }

            try
            {
                versionDescription = Convert.ToString(PilotNtWrapper.GetVersion());

                //if (StatusThread == null)
                {
                    StatusThread = new Thread(PoolThread);
                    StatusThread.Name = "BankModeuleStatusThread";
                    StatusThread.IsBackground = true;
                }
                ThreadState ts = StatusThread.ThreadState;
                toLog("ThreadState = " + ts.ToString());
                //if ((ts & (ThreadState.Stopped | ThreadState.Unstarted)) != 0)
                //{
                if (run)
                {
                    toLog("run is true... Why?..");
                    run = false;
                    Thread.Sleep(3000);
                }
                run = true;
                StatusThread.Start();
                ts = StatusThread.ThreadState;
                toLog("ThreadState after Start = " + ts.ToString());
                isOpened = true;
                //}
                //else
                //{
                //   toLog("StatusThread is already started");
                //}
            }
            catch (Exception e)
            {
                toLog(e.Message);
            }

            toLog("Stop; return always true");
            return true;
        }
        public override bool Close()
        {
            toLog("Start");
            isOpened = false;
            run = false;
            deviceEnabled = false;
            //StatusThread.Abort();
            releaseAuthSession();
            Thread.Sleep(1500);
            BMStatus = BM_STATUS.UNKNOWN;
            toLog("Stop; return always trues");
            return true;
        }
        public override bool StartSession()
        {
            return true;
        }
        public override bool StopSession()
        {
            return true;
        }
        public override string GetVersionDescription()
        {
            try
            {
                versionDescription = Convert.ToString(PilotNtWrapper.GetVersion());
                toLog("VersionDescription=" + versionDescription);
                return versionDescription;
            }
            catch (Exception e)
            {
                toLog(e.Message);
                return "";
            }
        }
        public override string GetOpStatus(bool IsCancel = false)
        {
            int nRes;
            toLog("Start");
            toLog("IsCancel=" + IsCancel.ToString() + " isCancelled=" + isCancelled.ToString());
            toLog("CommandStatus = " + CommandStatus.ToString());

            //lock (locker)
            {
                if (deviceEnabled)
                {
                    if (IsCancel && !isCancelled)
                    {
                        try
                        {
                            nRes = PilotNtWrapper.AbortTransaction();
                        }
                        catch (Exception e)
                        {
                            nRes = -1;
                            toLog(e.Message);
                        }
                        toLog("AbortTransaction nRes: " + nRes);
                        resultCode = SetResultCode(nRes);
                    }
                    toLog("Stop; Ret: " + sStatusAbortOperation);
                    isCancel = false;
                    busy = false;
                    return sStatusAbortOperation;
                }
                toLog("Stop; deviceEnabled = " + deviceEnabled);
                isCancel = false;
                busy = false;
                return "";
            }
        }
        public override bool Purchase()
        {
            toLog("Start");

            releaseAuthSession();

            bankModuleCommand = BankModuleCommandT.Purchase;
            CommandStatus = CommandStatusT.Run;
            toLog("set BankModuleCommandT.Purchase and CommandStatusT.Run");

            toLog("Stop");

            return true;
        }
        public override bool Cancel(string RRN, decimal AmountDue)
        {
            int nRes;
            string sRes;
            toLog("Start");

            releaseAuthSession();

            toLog("Cancel RRN=" + RRN + " AmountDue=" + AmountDue.ToString());

            lock (locker)
            {
                if (RRN == null || RRN == String.Empty)
                {
                    resultDescription = "Не задан RRN";
                    toLog(resultDescription);
                    return false;
                }

                PilotNtWrapper.AuthAnswer5 ans5 = new PilotNtWrapper.AuthAnswer5();
                ans5.ans.CType = 0;
                ans5.ans.TType = (int)BankModuleCommandT.Cancell;
                ans5.ans.Amount = (uint)Math.Round((AmountDue * 100), 0);
                ans5.RRN = RRN;
                ans5.ans.Check = Marshal.AllocHGlobal(4096);

                nRes = PilotNtWrapper.card_authorize5(null, ref ans5);

                resultCode = SetResultCode(nRes);
                resultDescription = nRes.ToString() + " " + ans5.ans.AMessage;

                sRes = ans5.ToString();
                AuthResult = sRes;
                sRes = Marshal.PtrToStringAnsi(ans5.ans.Check);
                AuthReceipt = sRes.Replace("\r", String.Empty);

                if (ans5.ans.Check != null && ans5.ans.Check != IntPtr.Zero)
                {
                    toLog("Cancel -> FreeHGlobal start");
                    Marshal.FreeHGlobal(ans5.ans.Check);
                    ans5.ans.Check = IntPtr.Zero;
                    toLog("Cancel -> FreeHGlobal stop");
                }

                toLog(AuthResult);
                toLog(AuthReceipt, BMLogType.LOG_HEX);
                toLog("Stop");
                return true;
            }
        }

        public override bool Verification()
        {
            toLog("Start");

#if RPS_ENTRANCE_EXIT_GATE
            if (dtStartVerification != DateTime.MinValue)
            {
                TimeSpan timeDiff = DateTime.Now - dtStartVerification;
                if (timeDiff.Seconds < nTimeoutWaitVerification)
                    return true;
                else
                {
                    resultCode = ErrorCode.SBRF_PROC_ERROR;
                    resultDescription = "Таймаут выполнения сверки";
                    return false;
                }
            }
#endif

            dtStartVerification = DateTime.Now;
            toLog("Verifiction started at " + dtStartVerification.ToLongDateString());

            toLog("Try delete old logs...");
            deleteOldBMLogs();
            toLog("Finish delete old logs...");

            //lock (locker)
            {
                PilotNtWrapper.AuthAnswer ans = new PilotNtWrapper.AuthAnswer();
                ans.TType = (int)BankModuleCommandT.Verification;
                ans.Check = Marshal.AllocHGlobal(4096);
                int nRet = PilotNtWrapper.close_day(ref ans);
                toLog("PilotNtWrapper.close_day nRet = " + nRet);
                resultCode = SetResultCode(nRet);
                if (resultCode != ErrorCode.Sucsess)
                    resultDescription = nRet.ToString() + " " + ans.AMessage;
                else
                {
                    resultDescription = nRet.ToString() + " " + ans.AMessage;
                    AuthResult = ans.ToString();
                    string res = Marshal.PtrToStringAnsi(ans.Check);
                    AuthReceipt = res.Replace("\r", String.Empty);
                    if (ans.Check != null && ans.Check != IntPtr.Zero)
                        Marshal.FreeHGlobal(ans.Check);
                    ans.Check = IntPtr.Zero;
                }

                toLog(AuthResult);
                toLog(AuthReceipt, BMLogType.LOG_HEX);

                toLog("Stop");

                dtStartVerification = DateTime.MinValue;

                return true;
            }
        }
        public override bool TechnicalCancelation()
        {
            string sRes;
            int nRes;

            toLog("Start");
#if RPS_ENTRANCE_EXIT_GATE
            if (CommandStatus != CommandStatusT.Completed && CommandStatus != CommandStatusT.Undeifned)
            {
                toLog("CommandStatus == " + CommandStatus.ToString());
                return true;
            }

            if (dtStartTechnicalCancelation != DateTime.MinValue)
            {
                TimeSpan timeDiff = DateTime.Now - dtStartTechnicalCancelation;
                if (timeDiff.Seconds < nTimeoutWaitTechnicalCancelation)
                    return true;
                else
                {
                    resultCode = ErrorCode.SBRF_PROC_ERROR;
                    resultDescription = "Таймаут выполнения Technical Cancelation";
                    return false;
                }
            }
#endif
            dtStartTechnicalCancelation = DateTime.Now;
            toLog("TechnicalCancelation started at " + dtStartTechnicalCancelation.ToLongDateString());
            releaseAuthSession();
            //lock (locker)
            {
                isCancelled = false;

                PilotNtWrapper.AuthAnswer5 ans5 = new PilotNtWrapper.AuthAnswer5();
                ans5.ans.CType = 0;
                ans5.ans.TType = (int)BankModuleCommandT.Cancell;
                ans5.ans.Amount = sLastAmount;
                ans5.RRN = sLastRRN;
                ans5.ans.Check = Marshal.AllocHGlobal(4096);
                byte[] b = Encoding.ASCII.GetBytes("QSELECT" + "\0");

                nRes = PilotNtWrapper.card_authorize5(b, ref ans5);
                toLog("PilotNtWrapper.card_authorize5 nRes = " + nRes);
                resultCode = SetResultCode(nRes);
                resultDescription = nRes.ToString() + " " + ans5.ans.AMessage;

                sRes = ans5.ToString();
                AuthResult = sRes;
                sRes = Marshal.PtrToStringAnsi(ans5.ans.Check);
                AuthReceipt = sRes.Replace("\r", String.Empty);

                if (ans5.ans.Check != null && ans5.ans.Check != IntPtr.Zero)
                {
                    toLog("TechnicalCancelation -> FreeHGlobal start");
                    Marshal.FreeHGlobal(ans5.ans.Check);
                    ans5.ans.Check = IntPtr.Zero;
                    toLog("TechnicalCancelation -> FreeHGlobal stop");
                }
                toLog(AuthResult);
                toLog(AuthReceipt, BMLogType.LOG_HEX);
            }

            toLog("Stop");

            dtStartTechnicalCancelation = DateTime.MinValue;

            return true;
        }

        public override bool EchoTest()
        {
            string sErrorDescription = " Пин-пад не подключен или неисправен";
            string sOkDescription = " Пин-пад подключен";
            int nRes;

            toLog("Start");
            //lock (locker)
            {
                try
                {
                    nRes = PilotNtWrapper.TestPinpad();
                }
                catch (Exception e)
                {
                    nRes = -1;
                    toLog(e.Message);
                }

                toLog("nRes =" + nRes);
                resultCode = SetResultCode(nRes);
                if (resultCode != ErrorCode.Sucsess)
                {
                    resultDescription = nRes.ToString() + sErrorDescription;
                }
                else
                {
                    resultDescription = nRes.ToString() + sOkDescription;
                }
                toLog("EchoTest res=" + resultDescription);
                toLog("Stop");
                return true;
            }
        }
        private string sLastAutoVerifactionLog = "";
        private void autoVerifacation()
        {

            if (deviceEnabled)
            {
                bool bResVer = Verification();
                toLog("bResVer=" + bResVer);
                if (bResVer)
                {
                    isVerification = false;
                }
                else
                {
                    deviceEnabled = false;
                }
            }
            string sLog = "isVerification=" + isVerification + " deviceEnabled=" + deviceEnabled;
            if (sLastAutoVerifactionLog != sLog)
            {
                toLog(sLog);
                sLastAutoVerifactionLog = sLog;
            }
        }
        protected internal void PoolThread()
        {
            toLog("PoolThread -> START");

            string res;
            int nRes;
            CommandStatusT csLast = CommandStatusT.Null;

            PilotNtWrapper.AuthAnswer12 ans12 = new PilotNtWrapper.AuthAnswer12();

            //try
            //{
            while (run)
            {
                //if (csLast != CommandStatus)
                //csLast = CommandStatus;

                switch (CommandStatus)
                {
                    case CommandStatusT.Run:
                        #region CommandStatusT.Run
                        toLog("CommandStatusT.Run -> start");

                        switch (bankModuleCommand)
                        {
                            case BankModuleCommandT.Purchase:
                                toLog("CommandStatusT.Run -> BankModuleCommandT.Purchase -> start");

                                if (!deviceEnabled)
                                {
                                    try
                                    {
                                        toLog("deviceEnabled = " + deviceEnabled + ". Try open");
                                        Open();
                                    }
                                    catch
                                    {
                                        resultCode = ErrorCode.OtherError;
                                        CommandStatus = CommandStatusT.Completed;
                                    }
                                }

                                if (deviceEnabled)
                                {
                                    lock (locker)
                                    {
                                        toLog("CommandStatusT.Run -> BankModuleCommandT.Purchase -> sStatusBeginOperation: " + sStatusBeginOperation);
                                        authRequest = sStatusBeginOperation;
                                        _status = sStatusBeginOperation;
                                        ans12.ans.CType = 0;
                                        ans12.ans.TType = (int)BankModuleCommandT.Purchase;
                                        ans12.ans.Amount = (uint)Math.Round((AmountDue * 100), 0);
                                        ans12.ans.Check = Marshal.AllocHGlobal(4096);

                                        try
                                        {
                                            TransactionStarted = true;
                                            AuthorizationStarted = true;
                                            nRes = PilotNtWrapper.card_authorize12(null, ref ans12);
                                            BMStatus = BM_STATUS.GET_COMMAND;
                                        }
                                        catch (Exception e)
                                        {
                                            nRes = -1;
                                            toLog(e.Message);
                                        }
                                        toLog("CommandStatusT.Run -> card_authorize12 nRes = " + nRes);
                                        if (nRes == nResponseCodeCardInserted)
                                        {

                                            cardInserted = true;
                                            cardWasInserted = true;
                                            authRequest = sStatusCardInserted;
                                            _status = sStatusCardInserted;
                                            CommandStatus = CommandStatusT.Running;
                                            BMStatus = BM_STATUS.INSERT_CARD;
                                            toLog("nResponseCodeCardInserted; go to CommandStatusT.Running");
                                        }
                                        else if (nRes == nResponseCodeAbortOperation)
                                        {
                                            isCancelled = true;
                                            resultCode = ErrorCode.Sucsess;
                                            authRequest = sStatusAbortOperation;
                                            _status = sStatusAbortOperation;
                                            CommandStatus = CommandStatusT.Completed;
                                            toLog("nResponseCodeAbortOperation; go to CommandStatusT.Completed");
                                        }
                                        else
                                        {
                                            resultCode = SetResultCode(nRes);
                                            CommandStatus = CommandStatusT.Completed;
                                            toLog("nRes other; go to CommandStatusT.Completed");
                                        }
                                        toLog("CommandStatusT.Run -> BankModuleCommandT.Purchase -> ans12: \n" + ans12.ToString());
                                    }
                                }
                                else
                                    resultCode = ErrorCode.OtherError;
                                break;
                            default:
                                break;
                        }
                        break;
                    #endregion CommandStatusT.Run
                    case CommandStatusT.Running:
                        #region CommandStatusT.Running
                        toLog("CommandStatusT.Running -> start");
                        //lock (locker)
                        {
                            authRequest = sStatusCardInserted;
                            _status = sStatusCardInserted;

                            try
                            {
                                nRes = PilotNtWrapper.card_authorize12(null, ref ans12);
                            }
                            catch (Exception e)
                            {
                                nRes = -1;
                                toLog(e.Message);
                            }

                            toLog("CommandStatusT.Running -> card_authorize12 nRes = " + nRes);
                            toLog("CommandStatusT.Running -> ans12.ans.Check: " + ans12.ans.Check.ToString());
                            toLog("CommandStatusT.Running -> ans12: " + ans12.ToString());
                            AuthorizationStarted = false;
                            TransactionStarted = false;
                            cardInserted = false;
                            cardWasInserted = false;

                            resultCode = SetResultCode(nRes);
                            resultDescription = nRes.ToString() + " " + ans12.ans.AMessage;
                            if (resultCode == ErrorCode.Sucsess)
                            {
                                AuthResult = ans12.ToString();
                                res = Marshal.PtrToStringAnsi(ans12.ans.Check);
                                AuthReceipt = res.Replace("\r", String.Empty);

                                sLastRRN = ans12.RRN;
                                sLastAmount = ans12.ans.Amount;
                                toLog("CommandStatusT.Running -> FreeHGlobal start");
                                Marshal.FreeHGlobal(ans12.ans.Check);
                                ans12.ans.Check = IntPtr.Zero;
                                toLog("CommandStatusT.Running -> FreeHGlobal stop");

                                toLog(AuthResult);
                                toLog(AuthReceipt, BMLogType.LOG_HEX);
                            }

                            CommandStatus = CommandStatusT.Completed;
                        }
                        toLog("CommandStatusT.Running -> stop");
                        break;
                    #endregion
                    case CommandStatusT.Completed:
                        #region CommandStatusT.Completed
                        if (csLast != CommandStatus)
                        {
                            toLog("CommandStatusT.Completed -> start");
                            csLast = CommandStatus;
                        }

                        AuthorizationStarted = false;
                        TransactionStarted = false;
                        cardInserted = false;
                        cardWasInserted = false;
                        BMStatus = BM_STATUS.WAITING_COMMAND;
                        try
                        {
                            if (csLast != CommandStatus)
                                toLog("CommandStatusT.Completed ans12.ans.Check: \n" + ans12.ans.Check.ToString());
                            if (ans12.ans.Check != null && ans12.ans.Check != IntPtr.Zero)
                            {
                                toLog("CommandStatusT.Completed -> FreeHGlobal");
                                Marshal.FreeHGlobal(ans12.ans.Check);
                                ans12.ans.Check = IntPtr.Zero;
                            }

                            if (isVerification)
                            {
                                Thread.Sleep(200);
                                autoVerifacation();
                            }

                        }
                        catch (Exception e)
                        {
                            toLog("CommandStatusT.Completed -> err: " + e.Message);
                        }
                        break;
                    #endregion
                    case CommandStatusT.Undeifned:
                        #region CommandStatusT.Undeifned

                        if (csLast != CommandStatus)
                        {
                            toLog("CommandStatusT.Undeifned -> start");
                            csLast = CommandStatus;
                        }

                        if (AuthorizationStarted)
                            AuthorizationStarted = false;
                        if (resultDescription != "")
                            resultDescription = "";


                        if (isVerification)
                        {
                            Thread.Sleep(200);
                            toLog("CommandStatusT.Undeifned autoVerifacation");
                            autoVerifacation();
                        }

                        break;
                        #endregion CommandStatusT.Undeifned
                }
                Thread.Sleep(200);
            }
            //}
            //catch (Exception e)
            //{
            //    toLog(e.Message);
            //}

            toLog("PoolThread -> FINISH");
        }

    }

}
