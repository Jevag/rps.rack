USE [rpsMDBG]
GO
ALTER TABLE [dbo].[CardsTransaction] DROP COLUMN [LinkCardId];
GO
ALTER TABLE [dbo].[CardsTransaction] ALTER COLUMN [CardId] VARCHAR (50) NOT NULL;

GO
ALTER TABLE [dbo].[Cards] DROP CONSTRAINT [DF__Cards__Id__3587F3E0];
GO
ALTER TABLE [dbo].[Cards] DROP CONSTRAINT [DF__Cards__ClientId__367C1819];
GO
ALTER TABLE [dbo].[Cards] DROP CONSTRAINT [DF__Cards__CompanyId__37703C52];
GO
ALTER TABLE [dbo].[Cards] DROP CONSTRAINT [Foreign_key01aadfh];
GO
ALTER TABLE [dbo].[Cards] DROP CONSTRAINT [Foreign_key01asdg];
GO
ALTER TABLE [dbo].[Cards] DROP CONSTRAINT [Foreign_key01client];
GO
ALTER TABLE [dbo].[Cards] DROP CONSTRAINT [Foreign_key01company];

GO
ALTER TABLE [dbo].[CardsInReaderModel] DROP CONSTRAINT [CardsInReaderModel_fk];
GO
ALTER TABLE [dbo].[CompanyCards] DROP CONSTRAINT [CompanyCards_fk];
GO
ALTER TABLE [dbo].[ClientModel] DROP CONSTRAINT [ClientModel_fk];

GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Cards] (
    [Blocked]          TINYINT          NULL,
    [_IsDeleted]       BIT              NULL,
    [Sync]             DATETIME         NOT NULL,
    [CardId]           VARCHAR (50)     NOT NULL,
    [ClientId]         UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    [CompanyId]        UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    [ParkingEnterTime] DATETIME         NULL,
    [LastRecountTime]  DATETIME         NULL,
    [LastPaymentTime]  DATETIME         NULL,
    [TSidFC]           TINYINT          NULL,
    [TPidFC]           TINYINT          NULL,
    [ZoneidFC]         TINYINT          NULL,
    [ClientGroupidFC]  TINYINT          NULL,
    [SumOnCard]        INT              NULL,
    [Nulltime1]        DATETIME         NULL,
    [Nulltime2]        DATETIME         NULL,
    [Nulltime3]        DATETIME         NULL,
    [TVP]              DATETIME         NULL,
    [TKVP]             INT              NULL,
    [ClientTypidFC]    INT              NULL,
    [DateSaveCard]     DATETIME         NULL,
    [LastPlate]        NVARCHAR (MAX)   NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_dbo.Cards] PRIMARY KEY CLUSTERED ([CardId] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Cards])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Cards] ([CardId], [Blocked], [_IsDeleted], [Sync], [ClientId], [CompanyId], [ParkingEnterTime], [LastRecountTime], [LastPaymentTime], [TSidFC], [TPidFC], [ZoneidFC], [ClientGroupidFC], [SumOnCard], [Nulltime1], [Nulltime2], [Nulltime3], [TVP], [TKVP], [ClientTypidFC], [DateSaveCard], [LastPlate])
        SELECT   [CardId],
                 [Blocked],
                 [_IsDeleted],
                 [Sync],
                 [ClientId],
                 [CompanyId],
                 [ParkingEnterTime],
                 [LastRecountTime],
                 [LastPaymentTime],
                 [TSidFC],
                 [TPidFC],
                 [ZoneidFC],
                 [ClientGroupidFC],
                 [SumOnCard],
                 [Nulltime1],
                 [Nulltime2],
                 [Nulltime3],
                 [TVP],
                 [TKVP],
                 [ClientTypidFC],
                 [DateSaveCard],
                 [LastPlate]
        FROM     [dbo].[Cards]
        ORDER BY [CardId] ASC;
    END

DROP TABLE [dbo].[Cards];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Cards]', N'Cards';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_dbo.Cards]', N'PK_dbo.Cards', N'OBJECT';

COMMIT TRANSACTION;
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
ALTER TABLE [dbo].[CardsInReaderModel] ALTER COLUMN [CardId] VARCHAR (50) NOT NULL;
GO
ALTER TABLE [dbo].[ClientModel] ALTER COLUMN [CardId] VARCHAR (50) NULL;
GO
ALTER TABLE [dbo].[CompanyCards] ALTER COLUMN [CardId] VARCHAR (50) NULL;
GO
ALTER TABLE [dbo].[CardsInReaderModel] WITH NOCHECK
    ADD CONSTRAINT [CardsInReaderModel_fk] FOREIGN KEY ([CardId]) REFERENCES [dbo].[Cards] ([CardId]);
GO
ALTER TABLE [dbo].[ClientModel] WITH NOCHECK
    ADD CONSTRAINT [ClientModel_fk] FOREIGN KEY ([CardId]) REFERENCES [dbo].[Cards] ([CardId]);
GO
ALTER TABLE [dbo].[CompanyCards] WITH NOCHECK
    ADD CONSTRAINT [CompanyCards_fk] FOREIGN KEY ([CardId]) REFERENCES [dbo].[Cards] ([CardId]) ON DELETE SET NULL;
GO
ALTER TABLE [dbo].[Cards] WITH NOCHECK
    ADD CONSTRAINT [Foreign_key01aadfh] FOREIGN KEY ([ClientGroupidFC]) REFERENCES [dbo].[Group] ([idFC]);
GO
ALTER TABLE [dbo].[Cards] WITH NOCHECK
    ADD CONSTRAINT [Foreign_key01asdg] FOREIGN KEY ([ZoneidFC]) REFERENCES [dbo].[ZoneModel] ([IdFC]);
GO
ALTER TABLE [dbo].[Cards] WITH NOCHECK
    ADD CONSTRAINT [Foreign_key01client] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientModel] ([Id]) ON DELETE SET NULL;
GO
ALTER TABLE [dbo].[Cards] WITH NOCHECK
    ADD CONSTRAINT [Foreign_key01company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[CompanyModel] ([Id]) ON DELETE SET NULL;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'���������� �����.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'Blocked';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'����� ���������� ��������� ������', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'Sync';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�� �� �����', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'CardId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'����� ������ �� ��������', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'ParkingEnterTime';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'����� ���������� ���������', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'LastRecountTime';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'����� ������', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'LastPaymentTime';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�� �� ��� �����', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'TSidFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�� �� ��� �����', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'TPidFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�� ������� ���� ��� �����', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'ZoneidFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'������ �������', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'ClientGroupidFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'����� �� �����', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'SumOnCard';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'����� ��������� ��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'Nulltime1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'����� ��������� ���', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'Nulltime2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'����� ��������� ���������� (���������)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'Nulltime3';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'������� ����� �� ������', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'TVP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'������� ���������� ������� �� ������', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'TKVP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'��� �������: 0 - �������, 1 - ���������� (1 ��� �� �����)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'ClientTypidFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'��� ����� ��������� ������������', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'LastPlate';

GO
ALTER TABLE [dbo].[CardsInReaderModel] WITH CHECK CHECK CONSTRAINT [CardsInReaderModel_fk];
GO
ALTER TABLE [dbo].[ClientModel] WITH CHECK CHECK CONSTRAINT [ClientModel_fk];
GO
ALTER TABLE [dbo].[CompanyCards] WITH CHECK CHECK CONSTRAINT [CompanyCards_fk];
GO
ALTER TABLE [dbo].[Cards] WITH CHECK CHECK CONSTRAINT [Foreign_key01aadfh];
GO
ALTER TABLE [dbo].[Cards] WITH CHECK CHECK CONSTRAINT [Foreign_key01asdg];
GO
ALTER TABLE [dbo].[Cards] WITH CHECK CHECK CONSTRAINT [Foreign_key01client];
GO
ALTER TABLE [dbo].[Cards] WITH CHECK CHECK CONSTRAINT [Foreign_key01company];

GO
CREATE TRIGGER [dbo].[SyncCards]
ON [dbo].[Cards]
AFTER INSERT,UPDATE
AS
BEGIN
SET NOCOUNT ON;
declare @level int;
if(Exists(select top 1 CurrentLevel from ConfigSync))
begin
set @level=(select top 1 CurrentLevel from ConfigSync);
if(@level=2)
begin
declare @dt datetime;
set @dt=GetDate();
update [Cards] set Sync=@dt	where CardId in (select CardId from inserted)
if(Exists(select top 1 Id from SyncTables where TableName='Cards'))
begin
update SyncTables set [DateTime]=@dt where TableName='Cards'
end
else
begin
insert into SyncTables (TableName,[DateTime])values('Cards',@dt)
end
end
end
end

GO
ALTER TRIGGER [dbo].[SyncCardsTransaction]
ON [dbo].[CardsTransaction]
AFTER INSERT,UPDATE
AS
BEGIN
SET NOCOUNT ON;
declare @level int;
if(Exists(select top 1 CurrentLevel from ConfigSync))
begin
set @level=(select top 1 CurrentLevel from ConfigSync);
if(@level=1)
begin
declare @dt datetime;
set @dt=GetDate();
update [CardsTransaction] set Sync=@dt	where Id in (select Id from inserted)
if(Exists(select top 1 Id from SyncTables where TableName='CardsTransaction'))
begin
update SyncTables set [DateTime]=@dt where TableName='CardsTransaction'
end
else
begin
insert into SyncTables (TableName,[DateTime])values('CardsTransaction',@dt)
end
end
end

if(@level=2)
begin
	update Cards set 
		Blocked=i.Blocked,
		_IsDeleted=i._IsDeleted,
		ParkingEnterTime=i.ParkingEnterTime,
		LastRecountTime=i.LastRecountTime,
		LastPaymentTime=i.LastPaymentTime,
		TSidFC=i.TSidFC,
		TPidFC=i.TPidFC,
		ZoneidFC=i.ZoneidFC,
		ClientGroupidFC=i.ClientGroupidFC,
		SumOnCard=i.SumOnCard,
		Nulltime1=i.Nulltime1,
		Nulltime2=i.Nulltime2,
		Nulltime3=i.Nulltime3,
		TVP=i.TVP,
		TKVP=i.TKVP,
		ClientTypidFC=i.ClientTypidFC,
		DateSaveCard=i.DateSaveCard,
		LastPlate=i.LastPlate
	from Cards c,inserted i
	where c.CardId=i.CardId and c.DateSaveCard<i.DateSaveCard;

	insert into Cards (Blocked,_IsDeleted,CardId,ParkingEnterTime,LastRecountTime,LastPaymentTime,
				TSidFC,TPidFC,ZoneidFC,ClientGroupidFC,SumOnCard,Nulltime1,Nulltime2,Nulltime3,TVP,TKVP,
				ClientTypidFC,DateSaveCard,LastPlate)
	select Blocked,_IsDeleted,CardId,ParkingEnterTime,LastRecountTime,LastPaymentTime,
			TSidFC,TPidFC,ZoneidFC,ClientGroupidFC,SumOnCard,Nulltime1,Nulltime2,Nulltime3,TVP,TKVP,
			ClientTypidFC,DateSaveCard,LastPlate
	from inserted
	where CardId not in (select CardId from Cards)
end

end
