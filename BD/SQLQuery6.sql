exec sp_configure 'show advanced options', 1
go
reconfigure WITH override
go
exec sp_configure 'clr enabled', 1
go
reconfigure WITH override
go
ALTER database rpsMDBG SET trustworthy ON
ALTER authorization ON database::rpsMDBG TO sa
go
RECONFIGURE
go