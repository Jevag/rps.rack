USE [rpsMDBG]
GO

DELETE FROM [dbo].[SyncTables]
     
GO

INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffModel', CAST(0x0000A513013ACCAC AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffIntervalModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 10, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffTariffPlanModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 20, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffPlanPeriodModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'TariffPlanPeriodId', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'Tariffs', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffMaxAmountIntervalModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 10, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffPlanTariffScheduleModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 20, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffSchedule', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'Client', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'Cards', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 10, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ClientCompany', CAST(0x0000A513013ACDD8 AS DateTime), NULL, 30, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ClientCard', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ClientCars', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'Group', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ClientGroup', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ZoneModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'BlackList', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'AlarmModel', CAST(0x0000A55A00D7A0AA AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'DeviceCMModel', CAST(0x0000A55A00C9D3FA AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'DeviceId', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffScheduleModel', CAST(0x0000A56F0146C33F AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ZoneGroup', CAST(0x0000A56F014769E7 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'CompanyCards', CAST(0x0000A56F0147DEDF AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ClientModel', CAST(0x0000A56F01492029 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 20, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'Transactions', CAST(0x0000A56F014AF715 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'DeviceRackModel', CAST(0x0000A56F014B2AAA AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'DeviceId', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'RackCustomerGroup', CAST(0x0000A56F014B657D AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'RackAltimetrModel', CAST(0x0000A56F014B995F AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'CashAcceptorAllowModel', CAST(0x0000A56F014C05C5 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'CoinAcceptorAllowModel', CAST(0x0000A56F014C3DE5 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'CardsTransaction', CAST(0x0000A56F014C65EB AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'Id', N'Sync', 1)
GO

