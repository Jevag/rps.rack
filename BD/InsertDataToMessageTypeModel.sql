USE [rpsMDBG]
GO

DELETE FROM [dbo].[MessageTypeModel]
     
GO

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (200, 3, N'���������������� ����� ����������. ���������� �������� ����������')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (201, 2, N'���������������� ����������. ������ �������� �� �����������. ���������� �������� ����������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (202, 1, N'������� ������ � �����������������. ���������� �� ��������! ��������� ����������� ���������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (203, 2, N'��� ����� � ������������������. ������ �� �����������. ��������� ����������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (204, 2, N'���� ����������������� �� ����������. ������ �� �����������. ���������� ����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (205, 2, N'������ �����������������.')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (206, 3, N'� ������ ������� ���������� ����� ��������� ������. ��������� ����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (207, 2, N'� ������ ������� ���������� ��������� ������. ��������� ����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (208, 2, N'� ���������� ������� ����������� ������ ��������')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (209, 3, N'� ������� ������� ���������� ����� ��������� ������. ��������� ����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (210, 2, N'� ������� ������� ���������� ��������� ������. ��������� ����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (211, 2, N'� ���������� ������� ����������� ������� ��������')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (213, 2, N'�������� ������� ��������� ������� �����������. ��������� �������� ����������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (214, 1, N'� ���������� ������� �������� ������. ������ ����� �� ��������! ��������� ����������� ���������!')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (215, 2, N'��� ����� � ����������� �������. ����� �������� �� ��������. ��������� ����������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (216, 2, N'������ ������ ���������� �����. ����� �������� �� ��������. ��������� ����������.')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (220, 3, N'� ������ ������� ����� ��������� ������. ��������� ����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (221, 2, N'� ������ ������� ����������� ������. ������ ����� �������� ����������. ��������� ����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (222, 1, N'� ������ ������� �������� ������. ������ ����� �� ��������! ��������� ����������� ���������!')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (223, 2, N'��� ����� � ������ �������� �����. ����� �������� ����������. ��������� ����������.')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (225, 3, N'� ����� ������� ����� ��������� ������. ��������� ����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (226, 2, N'� ����� ������� ����������� ������. ������ ����� �������� ����������. ��������� ����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (227, 1, N'� ����� ������� �������� ������. ������ ����� �� ��������! ��������� ����������� ���������!')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (228, 2, N'��� ����� � ����� �������� �����. ����� ���������� ����������. ��������� ����������.')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (230, 3, N'�������������� ����� ����������. ���������� �������� ����������')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (231, 2, N'�������������� ����������. ������ �������� �� �����������. ���������� �������� ����������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (232, 1, N'� ��������������� �������� ������. ���������� �� ��������! ��������� ����������� ���������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (233, 2, N'��� ����� � ����������������. ������ �� �����������. ��������� ����������.')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (240, 3, N'����� � ���������� ���� �������� � �����. ��������� � ����� ����� �����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (241, 3, N'� ���������� ���� ����������� �����. ��������� � ����� ����� �����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (242, 1, N'� ���������� ���� �������� �����. ��������� ����������� ���������!')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (243, 1, N'��� ����� � ����������� ����. ���������� �� ��������. ��������� ����������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (244, 2, N'�������� ������� �����������. �������� ����� �� �������� ������� ����������.')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (245, 1, N'��� ����� � �������. ��������� �����������, ������� � ����������������� ����������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (246, 1, N'������ ������/������ � ������')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (250, 2, N'���������� ������ �� ��������. ����� � ������ �� �����������. ��������� ����������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (251, 2, N'� ���������� ������������� ���������� �������� �����. ��������� ����������� ���������.')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (255, 1, N'��� ����� �� ����� ������������. ��������� �����������, ������� � ���������������� �����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (256, 1, N'��� ������ ��������')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (280, 3, N'� ��� ������������� ������. �������� ������� �����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (281, 1, N'� ��� ����������� ������. ����� �� ��������. �������� ������� �����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (282, 1, N'��� ����� � ���. ����� �� �������. ��������� ����������.')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (290, 3, N'������� ����� �������')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (291, 3, N'���������� ����� �������')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (292, 1, N'��� �������. ����� �������� � ���������� ������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (293, 4, N'��� ����� � �����������. ��������� ����, ������� � ������ ���������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (294, 1, N'����� �������')


INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (101, 2, N'��� ����� � ������� �������. ��������� �����������, ������� � ����������������� ����������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (102, 1, N'������ ������/������ �� ������� ������')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (103, 2, N'��� ����� � ���������� �������. ��������� �����������, ������� � ����������������� ����������.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (104, 1, N'������ ������/������ �� ���������� ������')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (105, 3, N'����� � ���������� ���� �������� � �����. ��������� � ����� ����� �����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (106, 2, N'� ���������� ���� ����������� �����. ��������� � ����� ����� �����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (107, 1, N'� ���������� ���� �������� �����. ��������� ����������� ���������!')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (108, 2, N'��� ����� � ����������� ����. ���������� �� ��������. ��������� ����������.')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (109, 2, N'��� ����� �� ����� ������������. ��������� �����������, ������� � ����������������� �����.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (110, 12, N'������ ��������')

INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (111, 2, N'��� ������� 220�. ������ �� UPS.')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (112, 3, N'������� ����� �������')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (113, 3, N'������ ��� ������')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (114, 2, N'�������� � ��������� (�����, ��)')
INSERT [dbo].[MessageTypeModel] (Id, Color, [Message]) VALUES (115, 2, N'�������� �� ����������')
go
