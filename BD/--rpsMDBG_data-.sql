 
/*
Шаблон скрипта после развертывания							
--------------------------------------------------------------------------------------
 В данном файле содержатся инструкции SQL, которые будут добавлены в скрипт построения.		
 Используйте синтаксис SQLCMD для включения файла в скрипт после развертывания.			
 Пример:      :r .\myfile.sql								
 Используйте синтаксис SQLCMD для создания ссылки на переменную в скрипте после развертывания.		
 Пример:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
--MS SQL Maestro 14.4.0.1
------------------------------------------
--Host     : localhost
--Database : rpsMDBG

/*
EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

-- Data from table "dbo.__MigrationHistory"
BEGIN TRANSACTION
GO
*/
-- Data from table "dbo.ActionJournal"



-- Data from table "dbo.AlarmColor"
INSERT INTO dbo.AlarmColor ([Name], Id) VALUES (N'Критично, устройство не работает', 1);
INSERT INTO dbo.AlarmColor ([Name], Id) VALUES (N'Устройство работает с ограниченным функционалом', 2);
INSERT INTO dbo.AlarmColor ([Name], Id) VALUES (N'Устройство скоро перейдет на тревогу 1ого или 2ого уровня', 3);
INSERT INTO dbo.AlarmColor ([Name], Id) VALUES (N'Нет связи.', 4);

GO



-- Data from table "dbo.AlarmModel"



-- Data from table "dbo.AuthAssignment"


/*
-- Data from table "dbo.AuthItem"
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('admin.*', 0, N'admin.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/default.*', 0, N'rest/default.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/default.list', 0, N'rest/default.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/organizationmodel.*', 0, N'rest/organizationmodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/organizationmodel.list', 0, N'rest/organizationmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/organizationparkingmodel.*', 0, N'rest/organizationparkingmodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/parkingmodel.*', 0, N'rest/parkingmodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/parkingmodel.view', 0, N'rest/parkingmodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/rolemodel.*', 0, N'rest/rolemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/usermodel.*', 0, N'rest/usermodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/usermodel.list', 0, N'rest/usermodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/usermodel.view', 0, N'rest/usermodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rest/userparkingrolemodel.*', 0, N'rest/userparkingrolemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rs.*', 0, N'rs.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rs.alarmmodel.*', 0, N'rs.alarmmodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rs.alarmmodel.list', 0, N'rs.alarmmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rs.mapmodel.*', 0, N'rs.mapmodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rs.mapmodel.list', 0, N'rs.mapmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rs.usermodel.*', 0, N'rs.usermodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rs.usermodel.view', 0, N'rs.usermodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rs.zonemodel.*', 0, N'rs.zonemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('rs.zonemodel.list', 0, N'rs.zonemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('schema.*', 0, N'schema.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('schema.collection', 0, N'schema.collection', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.*', 0, N'settings.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.devicemodel.*', 0, N'settings.devicemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.devicemodel.view', 0, N'settings.devicemodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.tariffplantariffschedulemodel.*', 0, N'settings.tariffplantariffschedulemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.tariffplantariffschedulemodel.sorted', 0, N'settings.tariffplantariffschedulemodel.sorted', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.tariffschedulemodel.*', 0, N'settings.tariffschedulemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.tariffschedulemodel.list', 0, N'settings.tariffschedulemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.timetypemodel.*', 0, N'settings.timetypemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.timetypemodel.list', 0, N'settings.timetypemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.usermodel.*', 0, N'settings.usermodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.usermodel.view', 0, N'settings.usermodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.zonemodel.*', 0, N'settings.zonemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES ('settings.zonemodel.list', 0, N'settings.zonemodel.list', NULL, N'N;');

GO



-- Data from table "dbo.AuthItemChild"



-- Data from table "dbo.AuthRole"
INSERT INTO dbo.AuthRole ([Name], [Description]) VALUES (N'SARole', N'СуперАдминистратор');

GO



-- Data from table "dbo.AuthUser"
INSERT INTO dbo.AuthUser (Id, LoginName, [Password], CreateDate) VALUES ('{17D66717-65A1-419A-85E9-0BE834A9968A}', N'user', N'user', CONVERT(datetime, '2015-08-21 23:21:54', 120));
INSERT INTO dbo.AuthUser (Id, LoginName, [Password], CreateDate) VALUES ('{3D2D2838-E6F6-4820-9EFE-BEB6EC833632}', N'sa@ws.r-p-s.ru', N'123456', CONVERT(datetime, '2015-09-13 06:20:23', 120));

GO



-- Data from table "dbo.AuthUserRole"
INSERT INTO dbo.AuthUserRole (Id, [User], [Role]) VALUES ('{1ACB25D4-3634-4C15-A79A-A1C29D681B6E}', N'sa@ws.r-p-s.ru', N'SARole');
INSERT INTO dbo.AuthUserRole (Id, [User], [Role]) VALUES ('{13CAC321-7521-41E6-ACE7-A7CF342F3A32}', N'sa@ws.r-p-s.ru', N'SARole');

GO

*/

-- Data from table "dbo.BanknoteModel"
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (0, N'Выбрать', N'0', N'', N'', N'');
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (1, N'10 рублей', N'10', N'RUB', N'150', N'65');
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (2, N'50 рублей', N'50', N'RUB', N'150', N'65');
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (3, N'100 рублей', N'100', N'RUB', N'150', N'65');
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (4, N'500 рублей', N'500', N'RUB', N'150', N'65');
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (5, N'1000 рублей', N'1000', N'RUB', N'157', N'69');
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (6, N'5000 рублей', N'5000', N'RUB', N'157', N'69');
--INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (7, N'1 RMB', N'1', N'RMB', N'130', N'63');
--INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (8, N'5 RMB', N'5', N'RMB', N'135', N'63');
--INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (9, N'10 RMB', N'10', N'RMB', N'140', N'70');
--INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (10, N'20 RMB', N'20', N'RMB', N'145', N'70');
--INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (11, N'50 RMB', N'50', N'RMB', N'150', N'70');
--INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width) VALUES (12, N'100 RMB', N'100', N'RMB', N'155', N'77');

GO



-- Data from table "dbo.BlackList"



-- Data from table "dbo.Cards"



-- Data from table "dbo.CardsInReaderModel"



-- Data from table "dbo.CarParkCapacity"



-- Data from table "dbo.CashAcceptorAllowModel"



-- Data from table "dbo.ClientCars"



-- Data from table "dbo.ClientCompany"



-- Data from table "dbo.ClientGroup"



-- Data from table "dbo.ClientModel"



-- Data from table "dbo.CoinAcceptorAllowModel"



-- Data from table "dbo.CoinModel"
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code) VALUES (0, N'Выбрать', N'0', N'');
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code) VALUES (1, N'1 рубль', N'1', N'RUB');
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code) VALUES (2, N'2 рубля', N'2', N'RUB');
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code) VALUES (3, N'5 рублей', N'5', N'RUB');
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code) VALUES (4, N'10 рублей', N'10', N'RUB');
--INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code) VALUES (5, N'1 RMB', N'1', N'RMB');
--INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code) VALUES (6, N'2 RMB', N'2', N'RMB');
--INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code) VALUES (7, N'5 RMB', N'5', N'RMB');

GO



-- Data from table "dbo.CompanyCards"



-- Data from table "dbo.CompanyModel"



-- Data from table "dbo.ComPortModel"
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (0, N'Выбрать');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (1, N'COM1');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (2, N'COM2');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (3, N'COM3');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (4, N'COM4');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (5, N'COM5');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (6, N'COM6');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (7, N'COM7');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (8, N'COM8');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (9, N'COM9');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (10, N'COM10');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (11, N'COM11');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (12, N'COM12');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES (13, N'COM13');

GO



-- Data from table "dbo.DeviceCMModel"



-- Data from table "dbo.DeviceDisplayModel"



-- Data from table "dbo.DeviceMapModel"



-- Data from table "dbo.DeviceModel"



-- Data from table "dbo.DeviceRackModel"



-- Data from table "dbo.DeviceStateModel"



-- Data from table "dbo.DeviceTypeModel"
INSERT INTO dbo.DeviceTypeModel (Id, [Name], _IsDeleted) VALUES (0, N'Выбрать', '0');
INSERT INTO dbo.DeviceTypeModel (Id, [Name], _IsDeleted) VALUES (1, N'Стойка', '0');
INSERT INTO dbo.DeviceTypeModel (Id, [Name], _IsDeleted) VALUES (2, N'Касса', '0');
INSERT INTO dbo.DeviceTypeModel (Id, [Name], _IsDeleted) VALUES (3, N'Табло', '0');
INSERT INTO dbo.DeviceTypeModel (Id, [Name], _IsDeleted) VALUES (4, N'Зонный контроллер', '0');

GO



-- Data from table "dbo.DeviceZCUModel"

-- Data from table "dbo.ExecutableDeviceTypeModel"
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (1, N'Диспенсер купюр');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (2, N'Банкнотоприемник');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (3, N'Хоппер');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (4, N'Монетоприемник');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (5, N'Банковский модуль');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (6, N'Сканер ШК');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (7, N'Диспенсер карт');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (8, N'Считыватель карт');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (9, N'Дисплей');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (10, N'Картоприемник');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (11, N'ККМ');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (12, N'Принтер');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES (13, N'Slave-контроллер');

GO


-- Data from table "dbo.ExecutableDevice"
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (1, 1, N'Multimech');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (2, 2, N'iVision');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (3, 3, N'Azkoen');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (4, 4, N'Munzprufer');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (5, 5, N'Uniteller');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (6, 6, N'ХЗ что...');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (7, 7, N'Mingte MT166');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (8, 8, N'Mingte MT625');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (9, 9, N'A07-OPHB01');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (10, 10, N'Kytronics KYT1020');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (11, 11, N'ККМ PayPPU700K');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (12, 12, N'Принтер');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES (13, 13, N'Slave-контроллер');


GO

/*
-- Data from table "dbo.ParkingModel"
INSERT INTO dbo.ParkingModel (Id, [Name], SULocal, SURemote, SUPort, SUUser, SUPassword, S3Site, S3Sql, S3User, S3Password, S3Db, S2Sql, S2User, S2Password, S2Db, Longitude, Latitude) VALUES ('{9DB9D1CE-05A3-4329-B64A-3DA3B33A44AD}', N'НОВАЯ РОЛЬ', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO dbo.ParkingModel (Id, [Name], SULocal, SURemote, SUPort, SUUser, SUPassword, S3Site, S3Sql, S3User, S3Password, S3Db, S2Sql, S2User, S2Password, S2Db, Longitude, Latitude) VALUES ('{1FAA3592-ADF9-444B-B0C1-73ED34E21C12}', N'Новый обьект', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO dbo.ParkingModel (Id, [Name], SULocal, SURemote, SUPort, SUUser, SUPassword, S3Site, S3Sql, S3User, S3Password, S3Db, S2Sql, S2User, S2Password, S2Db, Longitude, Latitude) VALUES ('{3F9A3509-D263-4BEE-8A6B-A2323CE170D5}', N'НОВАЯ РОЛЬ', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO dbo.ParkingModel (Id, [Name], SULocal, SURemote, SUPort, SUUser, SUPassword, S3Site, S3Sql, S3User, S3Password, S3Db, S2Sql, S2User, S2Password, S2Db, Longitude, Latitude) VALUES ('{5A21CD59-4900-4935-981D-E168D7D5822C}', N'Новый обьект', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO dbo.ParkingModel (Id, [Name], SULocal, SURemote, SUPort, SUUser, SUPassword, S3Site, S3Sql, S3User, S3Password, S3Db, S2Sql, S2User, S2Password, S2Db, Longitude, Latitude) VALUES ('{8C052CD7-861D-4E1E-94BD-F858B661DCD0}', N'Новый обьект', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

GO
*/

/*-- Data from table "dbo.Group"
INSERT INTO dbo.[Group]  (IdFC, [Name]) VALUES (1, N'Группа 1');
INSERT INTO dbo.[Group]  (IdFC, [Name]) VALUES (2, N'Группа 2');

GO*/


-- Data from table "dbo.MapModel"
INSERT INTO dbo.MapModel (Id, [Level], [Name], [Image], ImageW, ImageH, ImageName, ParkingId, ChangedDateTime, [Deleted],  _IsDeleted) VALUES ('{6DE415F8-FED1-4843-AE56-83F46A0381EA}', 1, N'Level1', CONVERT(VARBINARY(max),''), 8192, 5418, N'/app/styles/img/map7.png', '{5A21CD59-4900-4935-981D-E168D7D5822C}', CONVERT(datetime, '2015-08-21 23:26:19', 120), '0', '0');

GO



-- Data from table "dbo.MessageTypeModel"
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (200, 3, N'Банкнотоприемник почти переполнен. Необходимо провести инкассацию');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (201, 2, N'Банкнотоприемник переполнен. Оплата купюрами не принимается. Необходимо провести инкассацию.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (202, 1, N'Замятие купюры в банкнотоприемнике. Устройство не работает! Требуется присутствие оператора.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (203, 2, N'Нет связи с банкнотопримеником. Купюры не принимаются. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (204, 2, N'Ящик банкнотоприемника не установлен. Купюры не принимаются. Установите ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (205, 2, N'Ошибка банкнотоприемника.');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (206, 3, N'В нижней кассете диспенсера почти кончились купюры. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (207, 2, N'В нижней кассете диспенсера кончились купюры. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (208, 2, N'В диспенсере банкнот отсутсвтует нижняя кассаета');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (209, 3, N'В верхней кассете диспенсера почти кончились купюры. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (210, 2, N'В верхней кассете диспенсера кончились купюры. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (211, 2, N'В диспенсере банкнот отсутсвтует верхняя кассаета');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (213, 2, N'Отказная ёмкость диспенера банкнот переполнена. Требуется провести инкассацию.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (214, 1, N'В диспенсере банкнот застряла купюра. Выдача сдачи не возможна! Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (215, 2, N'Нет связи с диспенсером банкнот. Сдача купюрами не выдается. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (216, 2, N'Прочие ошибки диспенсера купюр. Сдача купюрами не выдается. Проверьте устройство.');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (220, 3, N'В правом хоппере почти кончились монеты. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (221, 2, N'В правом хоппере закончились монеты. Выдача сдачи монетами ограничена. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (222, 1, N'В правом хоппере застряла монета. Выдача сдачи не возможна! Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (223, 2, N'Нет связи с правым Хоппером монет. Сдача монетами ограничена. Проверьте устройство.');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (225, 3, N'В левом хоппере почти кончились монеты. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (226, 2, N'В левом хоппере закончились монеты. Выдача сдачи монетами ограничена. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (227, 1, N'В левом хоппере застряла монета. Выдача сдачи не возможна! Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (228, 2, N'Нет связи с левым Хоппером монет. Сдача монетамами ограничена. Проверьте устройство.');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (230, 3, N'Монетоприемник почти переполнен. Необходимо провести инкассацию');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (231, 2, N'Монетоприемник переполнен. Оплата монетами не принимается. Необходимо провести инкассацию.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (232, 1, N'В монетоприемнике застряла монета. Устройство не работает! Требуется присутствие оператора.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (233, 2, N'Нет связи с монетоприемником. Монеты не принимаются. Проверьте устройство.');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (240, 3, N'Карты в диспенсере карт подходят к концу. Загрузите в тубус новые карты.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (241, 3, N'В диспенсере карт закончились карты. Загрузите в тубус новые карты.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (242, 1, N'В диспенсере карт застряла карта. Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (243, 1, N'Нет связи с диспенсером карт. Устройство не работает. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (244, 2, N'Отказная кассета переполнена. Заберите карты из отказной кассеты диспенсера.');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (245, 1, N'Нет связи с ридером. Проверьте подключение, питание и работоспособность устройства.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (246, 1, N'Ошибка чтения/записи в ридере');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (250, 2, N'Банковский модуль не доступен. Карты к оплате не принимаются. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (251, 2, N'В банковском картоприменом устройстве застряла карта. Требуется присутствие оператора.');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (255, 1, N'Нет связи со слейв контроллером. Проверьте подключение, питание и раотоспособность платы.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (256, 1, N'Нет модуля дискрета');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (280, 3, N'В ККМ заканчивается бумага. Замените чековую ленту.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (281, 1, N'В ККМ закончилась бумага. Касса не работает. Замените чековую ленту.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (282, 1, N'Нет связи с ККМ. Касса не работет. Проверьте устройство.');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (290, 3, N'Внешняя дверь открыта');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (291, 3, N'Внутренняя дверь открыта');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (292, 1, N'Нет питания. Касса работает в автономном режиме.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (293, 4, N'Нет связи с устройством. Проверьте сеть, питание и прочие параметры.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (294, 1, N'Смена закрыта');


INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (101, 2, N'Нет связи с внешним ридером. Проверьте подключение, питание и работоспособность устройства.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (102, 1, N'Ошибка чтения/записи во внешнем ридере');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (103, 2, N'Нет связи с внутренним ридером. Проверьте подключение, питание и работоспособность устройства.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (104, 1, N'Ошибка чтения/записи во внутреннем ридере');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (105, 3, N'Карты в диспенсере карт подходят к концу. Загрузите в тубус новые карты.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (106, 2, N'В диспенсере карт закончились карты. Загрузите в тубус новые карты.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (107, 1, N'В диспенсере карт застряла карта. Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (108, 2, N'Нет связи с диспенсером карт. Устройство не работает. Проверьте устройство.');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (109, 2, N'Нет связи со слейв контроллером. Проверьте подключение, питание и работоспособность платы.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (110, 12, N'Ошибка Дискрета');

INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (111, 2, N'Нет питания 220В. Работа от UPS.');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (112, 3, N'Верхняя дверь открыта');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (113, 3, N'Нижний люк открыт');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (114, 2, N'Проблемы с датчиками (петли, ИК)');
INSERT INTO dbo.MessageTypeModel (Id, Color, Message) VALUES (115, 2, N'Проблемы со шлагбаумом');

GO



-- Data from table "dbo.MenuItemModel"



-- Data from table "dbo.Operation"
INSERT INTO dbo.Operation (Id, [Name]) VALUES (1, N'Голосовой вызов на устройство');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (2, N'Прием голосовго вызова с устройства');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (3, N'Изменнение режима работы проезда');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (4, N'Изменение данных на карте');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (5, N'Добавление устройства');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (6, N'Удаление устройства');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (7, N'Изменение параметров устройства');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (8, N'Добавление зоны');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (9, N'Удаление зоны');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (10, N'Изменение параметров зоны');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (11, N'Добаление тарифа');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (12, N'Добавление тарифного плана');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (13, N'Добавление тарифной сетки');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (14, N'Удаление тарифа');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (15, N'Удаление трифного плана');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (16, N'Удаление тарифной сетки');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (17, N'Изменение параметров Тарифа');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (18, N'Изменение параметров Тарифного плана');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (19, N'Изменение параметров Тарифной сетки');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (20, N'Добавление нового пользователя');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (21, N'Удаление пользователя');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (22, N'Изменение параметров пользователя');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (23, N'Добавление новой роли');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (24, N'Удаление роли');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (25, N'Изменение существующей роли');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (26, N'Добавление Юридического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (27, N'Удаление Юридического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (28, N'Изменение общей информации Юрдического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (29, N'Изменение информации о контактном лице Юрдического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (30, N'Привязка карты к Юрдическому лицу');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (31, N'Удаление связи карты и Юридического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (32, N'Добавление ТС к Юрдическом улицу');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (33, N'Удаление ТС у Юрдического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (34, N'Изменение данных ТС у Юридичекого лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (35, N'Привязка Физического лица к Юридическому');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (36, N'Удаление связи физического лица с Юрдическим.');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (37, N'Добавление Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (38, N'Удаление Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (39, N'Изменение общей информации Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (40, N'Привязка карты к Физического лицу');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (41, N'Удаление связи карты и Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (42, N'Добавление ТС к Физического улицу');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (43, N'Удаление ТС у Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (44, N'Изменение данных ТС у Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (45, N'Изменение данных на карте');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (46, N'Создание новой группы');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (47, N'Удаление группы доступпа');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (48, N'Изменение группы доступа');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (49, N'Создание нового отчета');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (50, N'Удаление отчета');
INSERT INTO dbo.Operation (Id, [Name]) VALUES (51, N'Изменение отчета');

GO

/*

-- Data from table "dbo.OrganizationModel"
INSERT INTO dbo.OrganizationModel (Id, [Name]) VALUES ('{08C6C0A3-A4BB-44DB-B754-0E7F9C88796F}', N'Новая компания');

GO
*/


-- Data from table "dbo.OrganizationParkingModel"





-- Data from table "dbo.ParkingSpaceModel"



-- Data from table "dbo.ParkingSpaceStateModel"



-- Data from table "dbo.PassageTransactionType"

INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES (N'Штатный проезд', 1);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES (N'Проезд с отбоем стрелы', 2);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES (N'проезд  в режиме свободный', 3);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES (N'отказ от проезда', 4);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES (N'кража карты', 5);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES (N'Попытка проезда - не та зона', 6);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES (N'Попытка проезда - стоп-лист (заблокирована)', 7);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES (N'Попытка проезда - не та группа', 8);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES (N'Попытка неоплаченного проезда', 9);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES (N'Попытка проезда на переполненую парковку', 10);

GO



-- Data from table "dbo.PaymentTransactionType"

INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES (1, N'Успешная оплата наличными');
INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES (2, N'Успешная оплата банковской картой');
INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES (3, N'Отмена оплаты до внесения денежных средств');
INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES (4, N'Отмена оплаты после внесения денежных средств');
INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES (5, N'Незавершенная оплата.');

GO



-- Data from table "dbo.Port"

INSERT INTO dbo.Port (Id, [Name]) VALUES (0, N'Выбор');
INSERT INTO dbo.Port (Id, [Name]) VALUES (1, N'A');
INSERT INTO dbo.Port (Id, [Name]) VALUES (2, N'B');
INSERT INTO dbo.Port (Id, [Name]) VALUES (3, N'C');
INSERT INTO dbo.Port (Id, [Name]) VALUES (4, N'D');
INSERT INTO dbo.Port (Id, [Name]) VALUES (5, N'E');
INSERT INTO dbo.Port (Id, [Name]) VALUES (6, N'F');
INSERT INTO dbo.Port (Id, [Name]) VALUES (7, N'H');
INSERT INTO dbo.Port (Id, [Name]) VALUES (8, N'J');
INSERT INTO dbo.Port (Id, [Name]) VALUES (9, N'K');

GO



-- Data from table "dbo.RackAltimetrModel"



-- Data from table "dbo.RackAltimetrModeModel"

INSERT INTO dbo.RackAltimetrModeModel (Id, [Name]) VALUES (0, N'Назначение');
INSERT INTO dbo.RackAltimetrModeModel (Id, [Name]) VALUES (1, N'Определение тарифа');
INSERT INTO dbo.RackAltimetrModeModel (Id, [Name]) VALUES (2, N'Пропуск транспортного средства');

GO



-- Data from table "dbo.RackCustomerGroup"



-- Data from table "dbo.RackWorkingMode"

INSERT INTO dbo.RackWorkingMode (Id, [Name]) VALUES (0, N'Выбор');
INSERT INTO dbo.RackWorkingMode (Id, [Name]) VALUES (1, N'Проезд закрыт');
INSERT INTO dbo.RackWorkingMode (Id, [Name]) VALUES (2, N'Свободный проезд с поднятой стрелой');
INSERT INTO dbo.RackWorkingMode (Id, [Name]) VALUES (3, N'Штатный режим');

GO



-- Data from table "dbo.ReportAccess"



-- Data from table "dbo.ReportModel"



-- Data from table "dbo.ReportUserRight"
/*
INSERT INTO dbo.ReportUserRight (Id, [Name]) VALUES ('{9EA2452E-628E-4AA8-8D5C-0B35BA8EE9EF}', N'Просмотр');
INSERT INTO dbo.ReportUserRight (Id, [Name]) VALUES ('{4C9C2ED9-C1F3-4549-8A53-2E2A8600D73E}', N'Изменение');

GO

*/

-- Data from table "dbo.RightModel"
/*
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{4F6A9844-1A84-4EB4-99D3-0B107A18D3CA}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Общий доступ к разделу "Устройства"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{D59C6A3E-03BA-481D-9AFD-0B6ADDB9A728}', '{009C1E8D-8833-41B2-9AB8-F24D945E247F}', N'Общий досутп к модулю "Касса"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{27CFF50E-65EF-4FE7-A362-0FDC6FDD93B4}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Редактирование Юридических лиц');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{3C9B2CC8-0577-4902-91CE-1AF97D436717}', '{40A6EEBA-CC9D-43B5-9762-AE1AFA238969}', N'Досутп к графическим отчетам.');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{DF146F55-86E7-420B-AFBA-21293AC425D9}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Редактирование информации на карте (2ой уровень доступа)');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{C6A864AC-D1C0-4DB1-B376-23765B777BA1}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Редактирование тарифных сеток');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{5F2F54BC-D185-47AA-A9A7-2C9FE3F93FBF}', '{A77C0A98-7D67-4C57-B58B-74AF0C26A88D}', N'Возможность инициировать голосовой вызов');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{32BE6C2C-750B-4CA4-A1E9-30B652206847}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Общий доступ к разделу "Карта"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{151A0F22-3C67-4E92-B95C-3324F9254C14}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Редактирование информации на карте (1ый уровень доступа):');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{AA7EA92B-D418-4B49-96BF-350CFBBA8087}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Общий доступ к "Настройки"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{16377B7D-538B-4B3E-A3E5-35B00C2BD7D2}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Работа с картой физ. лица.');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{C7E01233-16F9-422D-90E4-38D7DF15D067}', '{A77C0A98-7D67-4C57-B58B-74AF0C26A88D}', N'Возможность изменять режим проезда');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{1FC0B8B9-EB2B-48AB-B334-3A4B99F9FA9F}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Общий доступ к модулю "Клиенты"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{CE91855E-4160-4A4F-AD77-413828752A02}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Редактирование информации на карте (2ой уровень доступа)');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{D8D213AB-C57B-4CDE-9D1F-4739865F065A}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Управление ролями');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{13AB247E-63ED-4647-9AC4-4981A72E67C5}', '{A77C0A98-7D67-4C57-B58B-74AF0C26A88D}', N'Доступ к разделу "Устройства"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{105BA6EE-5AC1-4D82-91EE-4FEC721DA182}', '{40A6EEBA-CC9D-43B5-9762-AE1AFA238969}', N'Возможность содания отчетов');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{988E7681-1501-4186-89F9-51BC61B2A990}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Редактирование тарифных планов');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{69DDD76B-54CD-4A74-B899-5DAEA3812580}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Редактирование тарифов');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{1E3F34CA-8051-4733-9E42-5F05A5CA0A11}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Добавление и редактироание карт');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{2E3CCB53-3395-481B-8420-6624AF26207E}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Просмотр Раздела группы.');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{23FEF2A2-82E7-4AE1-8110-68F0C4DCF6C2}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Общий доступ к разделу "Пользователи"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{3B3410C1-D156-42B0-ACF0-700A077D5908}', '{A77C0A98-7D67-4C57-B58B-74AF0C26A88D}', N'Возможность редактировать карты клиента в устройстве (доступ 2ого уровня)');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{9B903DA4-FE12-4C74-9659-70AC0D229295}', '{A77C0A98-7D67-4C57-B58B-74AF0C26A88D}', N'Возможность принять голосовой вызов');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{3CBAA60C-3DDF-4653-9C5F-733980FC8D52}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Работа с ТС физ. лица.');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{A77C0A98-7D67-4C57-B58B-74AF0C26A88D}', NULL, N'Основные');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{28EB9B8D-A818-4BA5-B7D4-75F06178F738}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Удаление тарифных планов');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{65E4CA4A-8E37-41CB-95E1-79299AE6C225}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Редактирование раздела группы.');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{D9820B36-7073-4AAC-B35A-7ABD2AE8C225}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Дбавление и редактирование устройств на карте.');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{C4AB8861-5C67-4B9F-946E-99C7D4B53368}', '{A77C0A98-7D67-4C57-B58B-74AF0C26A88D}', N'Возможность редактировать карты клиента в устройстве (доступ 1ого уровня)');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{2FBE4DE8-0FAE-46CF-979C-9C2C008F804B}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Удаление пользователей');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{170C8DFB-30F2-43CB-B929-A3026DC95103}', NULL, N'Клиенты');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{B5B075AC-4613-432C-9742-A6021BE23AAE}', '{40A6EEBA-CC9D-43B5-9762-AE1AFA238969}', N'Досутп к карте');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{87AE354E-B3A6-4BD5-A746-A7E288682C00}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Общий доступ к разделу "Калькулятор"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{0C16734C-3769-4198-B62A-AAEE9C9FE8C5}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Общий доступ к модулю "Клиенты"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{DB607B08-148E-4862-8910-ABC573D7EEF1}', '{40A6EEBA-CC9D-43B5-9762-AE1AFA238969}', N'Общий доступ к модулю "Отчеты"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{40A6EEBA-CC9D-43B5-9762-AE1AFA238969}', NULL, N'Отчеты');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{7B0DF284-1C8C-47A6-9D99-B54092A0FED2}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Удаление тарифов');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{BC354098-A32E-473B-AEA5-B7EFDBA4E863}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Добавление устройств и зон.');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{315CC786-7E5D-411E-99A8-B8263F411CA0}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Добавление тарифных планов');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{11D701D5-0BAD-4B6A-A8E3-B926D41C0EC6}', '{A77C0A98-7D67-4C57-B58B-74AF0C26A88D}', N'Общий доступ к модулю "Центр управления"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{38D278BA-E458-483B-A321-C1DC83918C99}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Редактирование информации контактных лиц');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{60EE8C41-A2F9-402E-B7C5-C39A95206C79}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Удаление тарифных сеток');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{143C920F-2A62-4EF3-BD4D-C47058A3C360}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Общий доступ к разделу "Карты"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{0B73638B-9E77-400B-9B78-CC22762F6880}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Добавление тарифных сеток');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{A1923A6E-E917-4633-8566-D34015B6A5CE}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Редактирование пользователей');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{CD996DA1-CA87-4981-9828-D3590C68C920}', '{A77C0A98-7D67-4C57-B58B-74AF0C26A88D}', N'Доступ к разделу "Карта"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{0C7C22A7-8D63-47E5-BCE0-D403455BDD0E}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Работа с картами Юр лиц');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{AC284794-90C0-4D7E-909E-D585D4BA749A}', NULL, N'Настройки');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{1C4CFBE7-23AC-497C-8FC7-D81B3D94C473}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Редактирование общей информации');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{4FD42524-2C48-4F48-96B7-DA46D41E2D0F}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Доступ к разделу "Прочие"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{F222492F-97A4-4599-BDD7-DE60FCEA5EB6}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Редактирование физических лиц');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{D8025424-31B4-41A7-BBD7-DEFC92E54FD8}', '{40A6EEBA-CC9D-43B5-9762-AE1AFA238969}', N'Возможность удаления отчетов');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{DD788E5E-7BB0-4364-9CF1-E114E59FE00B}', '{40A6EEBA-CC9D-43B5-9762-AE1AFA238969}', N'Возможность просмотра всех отчетов.');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{298DA503-3CAD-4B5C-A1A8-E878F1ECBB2A}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Общий доступ к разделу "Интерфейс"');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{A376DFA8-7CD5-41AA-B174-EF6FBEBAB3C5}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Редактирование параметров устройств и зон');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{009C1E8D-8833-41B2-9AB8-F24D945E247F}', NULL, N'Касса');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{605ED76E-BC33-45C5-A33C-F43CB900B125}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Редактирование общей информации');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{53547495-C255-4E1E-B97A-F6A81E75B570}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Добавление тарифов');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{03C81AD7-9583-4B49-BBA8-F894782E8B9D}', '{170C8DFB-30F2-43CB-B929-A3026DC95103}', N'Работа с ТС Юр лиц.');
INSERT INTO dbo.RightModel (Id, ParentId, [Name]) VALUES ('{B9604C07-BFE8-4360-877D-FEA6B35D2220}', '{AC284794-90C0-4D7E-909E-D585D4BA749A}', N'Добавление новых пользователей');

GO
*/


-- Data from table "dbo.RightsInRoleModel"



-- Data from table "dbo.RoleModel"
/*
INSERT INTO dbo.RoleModel (Id, [Name], ParkingId) VALUES ('{A62B8B8F-9AFB-4407-A2F3-0DA2919C5B2C}', N'Полные права', '{8C052CD7-861D-4E1E-94BD-F858B661DCD0}');
INSERT INTO dbo.RoleModel (Id, [Name], ParkingId) VALUES ('{D7074FE9-E186-4168-B3D5-321A667DF6F8}', N'Полные права', '{9DB9D1CE-05A3-4329-B64A-3DA3B33A44AD}');
INSERT INTO dbo.RoleModel (Id, [Name], ParkingId) VALUES ('{E63372C1-0B52-4F97-8470-8D23ADE88CC6}', N'Полные права', '{1FAA3592-ADF9-444B-B0C1-73ED34E21C12}');
INSERT INTO dbo.RoleModel (Id, [Name], ParkingId) VALUES ('{EBBD19CB-9F48-4A6E-9287-90801F154DCF}', N'Полные права', '{5A21CD59-4900-4935-981D-E168D7D5822C}');
INSERT INTO dbo.RoleModel (Id, [Name], ParkingId) VALUES ('{BFBBA394-0948-4D6E-ADEB-A56C1CEB58C6}', N'Полные права', '{3F9A3509-D263-4BEE-8A6B-A2323CE170D5}');

GO
*/


-- Data from table "dbo.ServerExchangeProtocolModel"

INSERT INTO dbo.ServerExchangeProtocolModel (Id, [Name]) VALUES (0, N'Выбрать');
INSERT INTO dbo.ServerExchangeProtocolModel (Id, [Name]) VALUES (1, N'Внутренняя БД');
INSERT INTO dbo.ServerExchangeProtocolModel (Id, [Name]) VALUES (2, N'Касса создает события');
INSERT INTO dbo.ServerExchangeProtocolModel (Id, [Name]) VALUES (3, N'Сервер опрашивает кассу');

GO



-- Data from table "dbo.Setting"



-- Data from table "dbo.SyncTables"

/* -- Изменения для нового синхронизатора
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'TariffModel', CONVERT(datetime, '2015-09-14 19:06:09', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'TariffIntervalModel', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'TariffTariffPlanModel', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'TariffPlanPeriodModel', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'Tariff', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'TariffMaxAmountIntervalModel', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'TariffPlanTariffScheduleModel', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'TariffSchedule', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'Client', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'Cards', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'ClientComany', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'ClientCard', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'ClientCars', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'Groups', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'ClientGroup', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'ZoneGroup', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'ZoneModel', CONVERT(datetime, '2015-09-14 19:06:10', 120));
INSERT INTO dbo.SyncTables (TableName, [DateTime]) VALUES (N'BlackList', CONVERT(datetime, '2015-09-14 19:06:10', 120));
*/
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffModel', CAST(0x0000A513013ACCAC AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffIntervalModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 10, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffTariffPlanModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 20, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffPlanPeriodModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'TariffPlanPeriodId', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'Tariffs', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffMaxAmountIntervalModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 10, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffPlanTariffScheduleModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 20, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffSchedule', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'Client', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'Cards', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 10, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ClientCompany', CAST(0x0000A513013ACDD8 AS DateTime), NULL, 30, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ClientCard', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ClientCars', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'Group', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ClientGroup', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ZoneModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'BlackList', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'AlarmModel', CAST(0x0000A55A00D7A0AA AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'DeviceCMModel', CAST(0x0000A55A00C9D3FA AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'DeviceId', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'TariffScheduleModel', CAST(0x0000A56F0146C33F AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ZoneGroup', CAST(0x0000A56F014769E7 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'CompanyCards', CAST(0x0000A56F0147DEDF AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'ClientModel', CAST(0x0000A56F01492029 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 20, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'Transactions', CAST(0x0000A56F014AF715 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'DeviceRackModel', CAST(0x0000A56F014B2AAA AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'DeviceId', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'RackCustomerGroup', CAST(0x0000A56F014B657D AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'RackAltimetrModel', CAST(0x0000A56F014B995F AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'CashAcceptorAllowModel', CAST(0x0000A56F014C05C5 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'CoinAcceptorAllowModel', CAST(0x0000A56F014C3DE5 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'CardsTransaction', CAST(0x0000A56F014C65EB AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'Id', N'Sync', 1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'CompanyModel', CAST(0x0000A56F0147DEDF AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2)

GO

-- Data from table "dbo.TariffChangingTypeModel"

INSERT INTO dbo.TariffChangingTypeModel (Id, [Name]) VALUES (0, N'Выбрать');
INSERT INTO dbo.TariffChangingTypeModel (Id, [Name]) VALUES (1, N'Преим. сменяемого');
INSERT INTO dbo.TariffChangingTypeModel (Id, [Name]) VALUES (2, N'Точный расчет');
INSERT INTO dbo.TariffChangingTypeModel (Id, [Name]) VALUES (3, N'Преим. сменяющего');

GO
-- Data from table "dbo.TariffChangingTypeModel"



-- Data from table "dbo.TariffIntervalModel"
/*
INSERT INTO dbo.TariffIntervalModel (Id, TariffId, [Start], Duration, Amount, _IsDeleted, Sync) VALUES ('{E189E06D-5220-4BC6-8DC2-BABA263B5E60}', '{6187F7A4-9AB1-414B-8CE7-10B8A08D5899}', 0, 3, 100, '0', NULL);
INSERT INTO dbo.TariffIntervalModel (Id, TariffId, [Start], Duration, Amount, _IsDeleted, Sync) VALUES ('{E1640D8D-334F-4B71-84D4-332BF67DB29C}', '{6187F7A4-9AB1-414B-8CE7-10B8A08D5899}', 3, 1, 50, '0', NULL);
INSERT INTO dbo.TariffIntervalModel (Id, TariffId, [Start], Duration, Amount, _IsDeleted, Sync) VALUES ('{370896BF-4307-4211-BF35-E8BA775756D1}', '{FBEF4BCD-2684-4959-80AB-8182EEA82383}', 0, 3, 50, '0', NULL);
INSERT INTO dbo.TariffIntervalModel (Id, TariffId, [Start], Duration, Amount, _IsDeleted, Sync) VALUES ('{5DF2497A-CC69-4E89-8BF6-FA2D2A3ECB1B}', '{FBEF4BCD-2684-4959-80AB-8182EEA82383}', 3, 1, 50, '0', NULL);
INSERT INTO dbo.TariffIntervalModel (Id, TariffId, [Start], Duration, Amount, _IsDeleted, Sync) VALUES ('{332250F9-BB4A-4D0E-9B93-4F14D59900B8}', '{E5F78BC6-ACBC-420D-BA05-E7DA0D0FB7FB}', 0, 1, 50, '0', NULL);

GO
*/


-- Data from table "dbo.TariffMaxAmountIntervalModel"
/*
INSERT INTO dbo.TariffMaxAmountIntervalModel (Id, Amount, _IsDeleted, Sync, TariffId, [Start], StartTimeTypeId, Duration, DurationTimeTypeId, Repeat) VALUES ('{E7D242C4-E013-4A12-B6D0-F0081D87DC74}', 3000, '0', NULL, '{B10D9157-3563-4E4D-B0C8-2ADCB89DF8FB}', 1, 2, 24, 1, 1);
INSERT INTO dbo.TariffMaxAmountIntervalModel (Id, Amount, _IsDeleted, Sync, TariffId, [Start], StartTimeTypeId, Duration, DurationTimeTypeId, Repeat) VALUES ('{E72EF1C6-9D78-4C44-9A51-F9CFBBF3140D}', 500, '0', NULL, '{2976562E-F7A5-41E7-92B5-BFBA8CC0A600}', 0, 2, 12, 2, 10);

GO
*/


-- Data from table "dbo.TariffModel"
/*
INSERT INTO dbo.TariffModel (Id, [Name], TimeTypeId, _IsDeleted, Color, Sync) VALUES ('{6187F7A4-9AB1-414B-8CE7-10B8A08D5899}', N'вт-сб 9-20', 2, '0', N'#fff', NULL);
INSERT INTO dbo.TariffModel (Id, [Name], TimeTypeId, _IsDeleted, Color, Sync) VALUES ('{FBEF4BCD-2684-4959-80AB-8182EEA82383}', N'вс-пн', 2, '0', N'0906fa', NULL);
INSERT INTO dbo.TariffModel (Id, [Name], TimeTypeId, _IsDeleted, Color, Sync) VALUES ('{E5F78BC6-ACBC-420D-BA05-E7DA0D0FB7FB}', N'вт-сб 20-9', 2, '0', N'#fff', NULL);

GO
*/


-- Data from table "dbo.TariffPlanPeriodModel"
/*
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{EA0BD244-41A4-47B9-935D-17CF4850BF36}', '{6FAAA4A6-F5DD-445B-9430-F89DBF1009E5}', CONVERT(datetime, '2015-01-06 09:00:00', 120), CONVERT(datetime, '2015-01-06 20:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{EA13E05C-56F4-4F88-B539-33CB1D97F47C}', '{8233F29A-BFA8-4BFB-8F3F-F3A8EACF2AB9}', CONVERT(datetime, '2015-08-11 12:00:00', 120), CONVERT(datetime, '2015-08-15 12:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{B188E8D1-EF8A-4FD4-B368-47D1AA7BB479}', '{CF99C81E-4B11-4BB4-9EFC-F3BB0C6DB20F}', CONVERT(datetime, '2015-01-10 20:00:00', 120), CONVERT(datetime, '2015-01-11 00:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{6DF86C2E-17CE-4506-836F-5BBB9D0DFD4F}', '{1ECB7210-5C5A-4E0B-93E0-15B2AD4BD5CF}', CONVERT(datetime, '2015-01-20 09:00:00', 120), CONVERT(datetime, '2015-01-20 21:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{DA8CEED4-9CB2-4903-873A-5FB145D402D0}', '{CF99C81E-4B11-4BB4-9EFC-F3BB0C6DB20F}', CONVERT(datetime, '2015-01-06 00:00:00', 120), CONVERT(datetime, '2015-01-06 09:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{26E6063E-F8F9-407B-9B3F-6CB9246939CE}', '{CF99C81E-4B11-4BB4-9EFC-F3BB0C6DB20F}', CONVERT(datetime, '2015-01-07 20:00:00', 120), CONVERT(datetime, '2015-01-08 09:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{516AF1A7-AC39-4B65-ABB8-726CD300FC6A}', '{CF99C81E-4B11-4BB4-9EFC-F3BB0C6DB20F}', CONVERT(datetime, '2015-01-06 20:00:00', 120), CONVERT(datetime, '2015-01-07 09:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{2B08194F-7B10-4E20-B3E7-770BA9C1B04B}', '{CF99C81E-4B11-4BB4-9EFC-F3BB0C6DB20F}', CONVERT(datetime, '2015-01-09 20:00:00', 120), CONVERT(datetime, '2015-01-10 09:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{C4B4E5F3-AC5F-4E09-A8C4-8125A2EAFAFC}', '{CF99C81E-4B11-4BB4-9EFC-F3BB0C6DB20F}', CONVERT(datetime, '2015-01-08 20:00:00', 120), CONVERT(datetime, '2015-01-09 09:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{E15D53B5-023D-4316-9C01-8A75FF380619}', '{6FAAA4A6-F5DD-445B-9430-F89DBF1009E5}', CONVERT(datetime, '2015-01-07 09:00:00', 120), CONVERT(datetime, '2015-01-07 20:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{985B2598-AB2A-4C36-AD87-8A977A289D6D}', '{B8BADBDC-77A9-4289-B487-2AEE8BA80695}', CONVERT(datetime, '2015-01-11 00:00:00', 120), CONVERT(datetime, '2015-01-06 00:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{3F174D4C-6F90-40AF-8D32-8A9E269A588D}', '{6FAAA4A6-F5DD-445B-9430-F89DBF1009E5}', CONVERT(datetime, '2015-01-10 09:00:00', 120), CONVERT(datetime, '2015-01-10 20:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{6C2D320A-D902-4B04-99DD-9B4CECFEAE11}', '{1ECB7210-5C5A-4E0B-93E0-15B2AD4BD5CF}', CONVERT(datetime, '2015-01-10 09:00:00', 120), CONVERT(datetime, '2015-01-10 21:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{4467CA36-844B-4295-8413-A91E68F99305}', '{8233F29A-BFA8-4BFB-8F3F-F3A8EACF2AB9}', CONVERT(datetime, '2015-08-10 12:00:00', 120), CONVERT(datetime, '2015-08-10 14:30:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{A6268727-E221-4F5D-AD08-B39E5256CBAB}', '{6FAAA4A6-F5DD-445B-9430-F89DBF1009E5}', CONVERT(datetime, '2015-01-08 09:00:00', 120), CONVERT(datetime, '2015-01-08 20:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{5E714F9E-5350-4468-838F-BCA54961B96D}', '{8233F29A-BFA8-4BFB-8F3F-F3A8EACF2AB9}', CONVERT(datetime, '2015-08-10 19:00:00', 120), CONVERT(datetime, '2015-08-10 20:30:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{C30C47FF-BA7E-4058-A7B8-D09FBF8567B1}', '{6FAAA4A6-F5DD-445B-9430-F89DBF1009E5}', CONVERT(datetime, '2015-01-09 09:00:00', 120), CONVERT(datetime, '2015-01-09 20:00:00', 120));
INSERT INTO dbo.TariffPlanPeriodModel (_IsDeleted, TariffPlanPeriodId ,TariffTariffPlanId ,[Start] ,[End]) VALUES (0, '{9DCA4953-CA56-4904-A521-F659529DE428}', '{1ECB7210-5C5A-4E0B-93E0-15B2AD4BD5CF}', CONVERT(datetime, '2015-01-30 09:00:00', 120), CONVERT(datetime, '2015-01-31 21:00:00', 120));

GO
*/

/*
-- Data from table "dbo.TariffScheduleModel"
INSERT INTO dbo.TariffScheduleModel (Id, [Name], _IsDeleted, Sync, IdFC) VALUES ('{17D66717-64A1-419A-85E0-0BE834A99101}', N'ТС1', 0, NULL, 1);

GO

*/
/*
-- Data from table "dbo.TariffPlanTariffScheduleModel"
INSERT INTO dbo.TariffPlanTariffScheduleModel (Id, TariffScheduleId, TariffPlanId, TariffPlanNextId, TypeId, _IsDeleted, Sync, ConditionZoneAfter, ConditionParkingTime, ConditionParkingTimeTypeId, ConditionEC, ConditionECProtectInterval, ConditionECProtectIntervalTimeTypId, ConditionAbonementPrice, ConditionBackTime, ConditionBackTimeTypeId) VALUES ('{17D66717-64A1-419A-85E0-0BE834A99201}', '{17D66717-64A1-419A-85E0-0BE834A99101}','{2976562E-F7A5-41E7-92B5-BFBA8CC0A600}','{3A9D735F-2DD6-483E-ABBC-2DA3BAF79156}',4,0,NULL, '{17D66717-64A1-419A-85E0-0BE834A99000}',1,1,1,1,1,1,1,1 );

GO
*/

-- Data from table "dbo.TariffPlanTypeModel"

INSERT INTO dbo.TariffPlanTypeModel (Id, [Name]) VALUES (0, N'Выбрать');
INSERT INTO dbo.TariffPlanTypeModel (Id, [Name]) VALUES (1, N'Еженедельный');
INSERT INTO dbo.TariffPlanTypeModel (Id, [Name]) VALUES (2, N'Ежемесячный');
INSERT INTO dbo.TariffPlanTypeModel (Id, [Name]) VALUES (3, N'Интервалы');

GO



-- Data from table "dbo.Tariffs"
/*
INSERT INTO dbo.Tariffs (_IsDeleted, Id, [Name], ChangingTypeId, TypeId, Initial, InitialTimeTypeId, InitialAmount, ProtectedInterval, ProtectedIntervalTimeTypeId, FreeTime, FreeTimeTypeId, IdFC) VALUES (0, '{B10D9157-3563-4E4D-B0C8-2ADCB89DF8FB}', 'ТП2 из гугла (ежемесячный)',	1,	2,	10,	1,	0, 2, 1, 10, 1,	1);
INSERT INTO dbo.Tariffs (_IsDeleted, Id, [Name], ChangingTypeId, TypeId, Initial, InitialTimeTypeId, InitialAmount, ProtectedInterval, ProtectedIntervalTimeTypeId, FreeTime, FreeTimeTypeId, IdFC) VALUES (0, '{3A9D735F-2DD6-483E-ABBC-2DA3BAF79156}', 'ТП2 из гугла (интервальный)',	1,	3,	10,	1,	0, 2, 1, 10, 1,	2);
INSERT INTO dbo.Tariffs (_IsDeleted, Id, [Name], ChangingTypeId, TypeId, Initial, InitialTimeTypeId, InitialAmount, ProtectedInterval, ProtectedIntervalTimeTypeId, FreeTime, FreeTimeTypeId, IdFC) VALUES (0, '{2976562E-F7A5-41E7-92B5-BFBA8CC0A600}', 'ТП2 из гугла (еженедельный)',	1,	1,	10,	1,	100, 3, 1, 10, 1, 3);
GO
*/
-- Data from table "dbo.TariffScheduleTypeModel"

INSERT INTO dbo.TariffScheduleTypeModel (Id, [Name]) VALUES (0, N'Выбрать');
INSERT INTO dbo.TariffScheduleTypeModel (Id, [Name]) VALUES (1, N'Время на парковке');
INSERT INTO dbo.TariffScheduleTypeModel (Id, [Name]) VALUES (2, N'Кол-во проездов');
INSERT INTO dbo.TariffScheduleTypeModel (Id, [Name]) VALUES (3, N'Абонемент');
INSERT INTO dbo.TariffScheduleTypeModel (Id, [Name]) VALUES (4, N'Зона');

GO

-- Data from table "dbo.TariffTariffPlanModel"

/*
INSERT INTO dbo.TariffTariffPlanModel (_IsDeleted, Id, TariffId, TariffPlanId) VALUES (0, '{1ECB7210-5C5A-4E0B-93E0-15B2AD4BD5CF}', '{6187F7A4-9AB1-414B-8CE7-10B8A08D5899}', '{B10D9157-3563-4E4D-B0C8-2ADCB89DF8FB}');
INSERT INTO dbo.TariffTariffPlanModel (_IsDeleted, Id, TariffId, TariffPlanId) VALUES (0, '{B8BADBDC-77A9-4289-B487-2AEE8BA80695}', '{FBEF4BCD-2684-4959-80AB-8182EEA82383}', '{2976562E-F7A5-41E7-92B5-BFBA8CC0A600}');
INSERT INTO dbo.TariffTariffPlanModel (_IsDeleted, Id, TariffId, TariffPlanId) VALUES (0, '{8233F29A-BFA8-4BFB-8F3F-F3A8EACF2AB9}', '{FBEF4BCD-2684-4959-80AB-8182EEA82383}', '{3A9D735F-2DD6-483E-ABBC-2DA3BAF79156}');
INSERT INTO dbo.TariffTariffPlanModel (_IsDeleted, Id, TariffId, TariffPlanId) VALUES (0, '{CF99C81E-4B11-4BB4-9EFC-F3BB0C6DB20F}', '{E5F78BC6-ACBC-420D-BA05-E7DA0D0FB7FB}', '{2976562E-F7A5-41E7-92B5-BFBA8CC0A600}');
INSERT INTO dbo.TariffTariffPlanModel (_IsDeleted, Id, TariffId, TariffPlanId) VALUES (0, '{6FAAA4A6-F5DD-445B-9430-F89DBF1009E5}', '{6187F7A4-9AB1-414B-8CE7-10B8A08D5899}', '{2976562E-F7A5-41E7-92B5-BFBA8CC0A600}');
GO
*/
-- Data from table "dbo.TimeTypeModel"

INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES (0, N'ед.');
INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES (1, N'мин');
INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES (2, N'час');
INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES (3, N'сут');
INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES (4, N'нед');
INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES (5, N'мес');

GO



-- Data from table "dbo.Transactions"



-- Data from table "dbo.TransitTypeModel"

INSERT INTO dbo.TransitTypeModel (Id, [Name], _IsDeleted) VALUES (0, N'Выбрать', '0');
INSERT INTO dbo.TransitTypeModel (Id, [Name], _IsDeleted) VALUES (1, N'Въезд', '0');
INSERT INTO dbo.TransitTypeModel (Id, [Name], _IsDeleted) VALUES (2, N'Выезд', '0');
INSERT INTO dbo.TransitTypeModel (Id, [Name], _IsDeleted) VALUES (3, N'Переезд', '0');

GO



-- Data from table "dbo.UserModel"
/*
INSERT INTO dbo.UserModel (Id, LoginName, [Password], Email, FullName) VALUES ('{17D66717-65A1-419A-85E9-0BE834A9968A}', N'user', N'user', N'test@r-p-s.ru', NULL);
INSERT INTO dbo.UserModel (Id, LoginName, [Password], Email, FullName) VALUES ('{3D2D2838-E6F6-4820-9EFE-BEB6EC833632}', N'sa@ws.r-p-s.ru', N'123456', N'sa@ws.r-p-s.ru', NULL);

GO
*/

-- Data from table "dbo.ZoneGroup"


-- Data from table "dbo.ZoneModel"
/*
INSERT INTO dbo.ZoneModel (Id, [Name],Capacity,Reserved, _IsDeleted, FreeSpace, OccupId, IdFC) VALUES ('{17D66717-64A1-419A-85E0-0BE834A99000}', N'Вне парковки', 20000, 0, '0', 12000, 0,0);
INSERT INTO dbo.ZoneModel (Id, [Name],Capacity,Reserved, _IsDeleted, FreeSpace, OccupId, IdFC) VALUES ('{17D66717-64A1-419A-85E0-0BE834A99001}', N'Парковка', 120, 0, '0', 120, 0,1);

GO
*/
-- Data from table "dbo.UserParkingRoleModel"
/*
INSERT INTO dbo.UserParkingRoleModel (Id, UserId, ParkingId, RoleId) VALUES ('{6EE3C4A0-C91C-45B9-AEB7-F78B1F359A80}', '{17D66717-65A1-419A-85E9-0BE834A9968A}', '{5A21CD59-4900-4935-981D-E168D7D5822C}', NULL);
COMMIT
GO

EXEC sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"
*/



