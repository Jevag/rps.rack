//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RPS_Shared
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReportModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ReportModel()
        {
            this.ReportAccess = new HashSet<ReportAccess>();
        }
    
        public System.Guid Id { get; set; }
        public string Name { get; set; }
        public string Colum1 { get; set; }
        public string Colum2 { get; set; }
        public string Colum3 { get; set; }
        public string Colum4 { get; set; }
        public string Colum5 { get; set; }
        public string Colum6 { get; set; }
        public string Colum7 { get; set; }
        public string Colum8 { get; set; }
        public string Colum9 { get; set; }
        public string Colum10 { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public string Line4 { get; set; }
        public string Line5 { get; set; }
        public string Line6 { get; set; }
        public string Line7 { get; set; }
        public string Line8 { get; set; }
        public string Line9 { get; set; }
        public string Line10 { get; set; }
        public string data_json { get; set; }
        public string type { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReportAccess> ReportAccess { get; set; }
    }
}
