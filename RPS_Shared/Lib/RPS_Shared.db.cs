﻿using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using Microsoft.Win32;
using System.Configuration;

namespace RPS_Shared
{
    public partial class Entities : DbContext
    {
        public interface ISyncTraking
        {
            Guid Id { get; set; }

            DateTime? ChangedDateTime { get; set; }

            bool Deleted { get; set; }
        }

        public override int SaveChanges()
        {
            try
            {
                foreach (var item in this.ChangeTracker.Entries())
                {
                    if (item.Entity is ISyncTraking)
                        (item.Entity as ISyncTraking).ChangedDateTime = DateTime.Now;
                }
                base.SaveChanges();
            }
            catch (DbEntityValidationException exc)
            {
                var s = "DbEntityValidationException   ";
                foreach (var entry in exc.EntityValidationErrors)
                    foreach (var error in entry.ValidationErrors)
                        s += error.ErrorMessage + ": " + error.PropertyName + ". ";

                //Elmah.ErrorSignal.FromCurrentContext().Raise(exc);
                throw new Exception(s);
            }
            catch (Exception exc)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(exc);
                throw exc;
            }

            return 1;
        }
    }
}