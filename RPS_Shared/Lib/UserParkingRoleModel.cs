//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RPS_Shared
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserParkingRoleModel
    {
        public System.Guid Id { get; set; }
        public System.Guid UserId { get; set; }
        public System.Guid ParkingId { get; set; }
        public Nullable<System.Guid> RoleId { get; set; }
    
        public virtual ParkingModel ParkingModel { get; set; }
        public virtual RoleModel RoleModel { get; set; }
        public virtual UserModel UserModel { get; set; }
    }
}
