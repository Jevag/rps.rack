//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RPS_Shared
{
    using System;
    using System.Collections.Generic;
    
    public partial class TimeTypeModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TimeTypeModel()
        {
            this.TariffMaxAmountIntervalModel = new HashSet<TariffMaxAmountIntervalModel>();
            this.TariffMaxAmountIntervalModel1 = new HashSet<TariffMaxAmountIntervalModel>();
            this.TariffModel = new HashSet<TariffModel>();
            this.TariffPlanTariffScheduleModel = new HashSet<TariffPlanTariffScheduleModel>();
            this.TariffPlanTariffScheduleModel1 = new HashSet<TariffPlanTariffScheduleModel>();
            this.TariffPlanTariffScheduleModel2 = new HashSet<TariffPlanTariffScheduleModel>();
            this.Tariffs = new HashSet<Tariffs>();
            this.Tariffs1 = new HashSet<Tariffs>();
            this.Tariffs2 = new HashSet<Tariffs>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TariffMaxAmountIntervalModel> TariffMaxAmountIntervalModel { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TariffMaxAmountIntervalModel> TariffMaxAmountIntervalModel1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TariffModel> TariffModel { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TariffPlanTariffScheduleModel> TariffPlanTariffScheduleModel { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TariffPlanTariffScheduleModel> TariffPlanTariffScheduleModel1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TariffPlanTariffScheduleModel> TariffPlanTariffScheduleModel2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tariffs> Tariffs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tariffs> Tariffs1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tariffs> Tariffs2 { get; set; }
    }
}
