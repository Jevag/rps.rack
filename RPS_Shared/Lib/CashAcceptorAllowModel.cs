//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RPS_Shared
{
    using System;
    using System.Collections.Generic;
    
    public partial class CashAcceptorAllowModel
    {
        public System.Guid DeviceId { get; set; }
        public int BanknoteId { get; set; }
        public Nullable<System.DateTime> Sync { get; set; }
        public bool Allow { get; set; }
        public System.Guid Id { get; set; }
    
        public virtual BanknoteModel BanknoteModel { get; set; }
        public virtual DeviceModel DeviceModel { get; set; }
    }
}
