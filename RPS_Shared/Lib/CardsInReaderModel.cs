//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RPS_Shared
{
    using System;
    using System.Collections.Generic;
    
    public partial class CardsInReaderModel
    {
        public System.Guid Id { get; set; }
        public long CardId { get; set; }
        public System.DateTime ParkingEnterTime { get; set; }
        public System.DateTime LastRecountTime { get; set; }
        public System.DateTime LastPaymentTime { get; set; }
        public int TSidFC { get; set; }
        public int TPidFC { get; set; }
        public byte ZoneidFC { get; set; }
        public byte ClientGroupidFC { get; set; }
        public int SumOnCard { get; set; }
        public System.DateTime Nulltime1 { get; set; }
        public System.DateTime Nulltime2 { get; set; }
        public System.DateTime Nulltime3 { get; set; }
        public System.DateTime TVP { get; set; }
        public int TKVP { get; set; }
        public int ClientTypidFC { get; set; }
        //public System.DateTime BlockTime { get; set; }

        public virtual Cards Cards { get; set; }
    }
}
