﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Linq.Expressions;
using EntityFramework.Extensions;
using EntityFramework.Batch;
using EntityFramework.Future;

using RPS_Shared;

namespace rps.Sync
{
    public static partial class Synchronize
    {
        /// <summary>
        /// USAGE ONLY LEVEL 1
        /// </summary>
        /// <param name="model"></param>
        private static int DeviceCMModel<TEntity>(
            DeviceModel remoteDb, 
            Expression<Func<DeviceCMModel, bool>> predicate, 
            Expression<Func<DeviceCMModel,DeviceCMModel>> update) where TEntity : class
        {
            if (remoteDb == null)
                return 1;

            try
            {
                using (var db = new Entities()) //LOCAL
                {
                    var cs = String.Format("data source={0};Initial Catalog={1};User ID={2};Password={3};{4}", remoteDb.Host, remoteDb.DataBase, remoteDb.User, remoteDb.Password, remoteDb.Advanced);
                    if (cs.ToLower().IndexOf(";connection timeout=") == -1)
                        cs += ";Connection Timeout=10";

                    using (var dbR = new Entities(cs)) //SITE2-3
                    {
                        var list = db.DeviceCMModel.Where(predicate).ToList();
                        ASE.Log.L("Count", list.Count);

                        foreach (var item in list)
                        {
                            var device = item.DeviceModel;
                            db.Entry(device).State = System.Data.Entity.EntityState.Detached;                            
                            db.Entry(item).State = System.Data.Entity.EntityState.Detached;

                            var deviceR = dbR.DeviceModel.Find(device.Id);
                            if (deviceR == null)
                                dbR.DeviceModel.Add(device);

                            var itemR = dbR.DeviceCMModel.Find(item.DeviceId);

                            if (itemR == null)
                                dbR.Entry(item).State = System.Data.Entity.EntityState.Added;
                            else
                            {
                                dbR.Entry(itemR).State = System.Data.Entity.EntityState.Detached;
                                dbR.Entry(item).State = System.Data.Entity.EntityState.Modified;
                            }

                            foreach (var ca in db.CashAcceptorAllowModel.Where(x => x.DeviceId == item.DeviceId).ToList())
                            {
                                try
                                {
                                    var caDb = dbR.CashAcceptorAllowModel.FirstOrDefault(x => x.DeviceId == ca.DeviceId & x.BanknoteId == ca.BanknoteId);
                                    if (caDb == null)
                                        dbR.CashAcceptorAllowModel.Add(new CashAcceptorAllowModel { DeviceId = ca.DeviceId, BanknoteId = ca.BanknoteModel.Id, Allow = ca.Allow });
                                    else
                                        caDb.Allow = ca.Allow;
                                }
                                catch
                                {

                                }
                            }

                            foreach (var ca in db.CoinAcceptorAllowModel.Where(x => x.DeviceId == item.DeviceId).ToList())
                            {
                                try
                                {
                                    var caDb = dbR.CoinAcceptorAllowModel.FirstOrDefault(x => x.DeviceId == ca.DeviceId & x.CoinId == ca.CoinId);
                                    if (caDb == null)
                                        dbR.CoinAcceptorAllowModel.Add(new CoinAcceptorAllowModel { DeviceId = ca.DeviceId, CoinId = ca.CoinId, Allow = ca.Allow });
                                    else
                                        caDb.Allow = ca.Allow;
                                }
                                catch
                                {

                                }
                            }

                            dbR.SaveChanges();

                            db.DeviceCMModel.Where(x => x.C_Modified == item.C_Modified & x.DeviceId == item.DeviceId).Update<DeviceCMModel>(update);
                        }
                    }
                }
            }
            catch(Exception exc)
            {
                ASE.Log.L(exc);

                return 2;
            }

            return 0;
        }
    }
}
