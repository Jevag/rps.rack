﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPS_Shared;

namespace RPS_Shared
{
    public class Configurator
    {
        /// <summary>
        /// http://dbg.ws.r-p-s.ru/root/sqlmanager/
        /// </summary>
        /// <param name="url"></param>
        public static void SetRemoteNotifier(Entities db, string url)
        {
            db.Database.ExecuteSqlCommand(String.Format("ALTER PROCEDURE SPRemoteNotifierL @url NVARCHAR (MAX) AS BEGIN DECLARE @S varchar(max); SET @S = Concat('{0}', @url); EXEC SPRemoteNotifier @S END;", url));
        }
    }
}
