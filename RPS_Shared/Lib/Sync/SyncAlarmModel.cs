﻿using System;
using System.Linq;
using System.Linq.Expressions;
using EntityFramework.Extensions;
using EntityFramework.Batch;
using EntityFramework.Future;

using RPS_Shared;

namespace rps.Sync
{
    public static partial class Synchronize
    {
        private static int AlarmModel<TEntity>(
            DeviceModel remoteDb,
            Expression<Func<AlarmModel, bool>> predicate,
            Expression<Func<AlarmModel, AlarmModel>> update) where TEntity : class
        {
            if (remoteDb == null)
                return 1;

            try
            {
                using (var db = new Entities()) //LOCAL
                {
                    var cs = String.Format("data source={0};Initial Catalog={1};User ID={2};Password={3};{4}", remoteDb.Host, remoteDb.DataBase, remoteDb.User, remoteDb.Password, remoteDb.Advanced);
                    if (cs.ToLower().IndexOf(";connection timeout=") == -1)
                        cs += ";Connection Timeout=10";

                    using (var dbR = new Entities(cs)) //SITE2-3
                    {
                        var list = db.AlarmModel.Where(predicate).ToList();

                        foreach (var item in list)
                        {
                            var itemR = dbR.AlarmModel.Find(item.DeviceId);
                            db.Entry(item).State = System.Data.Entity.EntityState.Detached;

                            if (itemR == null)
                                dbR.Entry(item).State = System.Data.Entity.EntityState.Added;

                            dbR.SaveChanges();

                            db.AlarmModel.Where(x => x.C_Modified == item.C_Modified & x.Id == item.Id).Update<AlarmModel>(update);
                            db.AlarmModel.Where(x => x.C_IsSync2 & x.C_IsSync3).Delete<AlarmModel>();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ASE.Log.L(exc);

                return 2;
            }

            return 0;
        }
    }
}
