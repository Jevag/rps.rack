﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Microsoft.Win32;
using Rps.Crypto;
using System.Threading;
using System.Xml;

namespace ConnectFromLevel2
{
    public class UDPListener
    {
        public object Locker = new object();
        public bool ConnectStringUpdated = false;
        private const int listenPort = 4444;
        static Thread StatusThread;

        CryptoDecrypto cryp = new CryptoDecrypto();

        IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);

        RegistryKey currRegistryKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RPS");

        public bool Run = true;

        Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

        public UDPListener()
        {
            StatusThread = new Thread(StartListener);
            StatusThread.Name = "UDPListenerThread";
            StatusThread.IsBackground = true;
            StatusThread.Start();
        }

        public void StartListener()
        {
            try
            {
                sock.Bind(groupEP);
                EndPoint ep = (EndPoint)groupEP;

                while (Run)
                {
                    ConnectStringUpdated = false;

                    byte[] encrypted;
                    string conserver1;
                    string conserverReg = "";
                    string conserver = "";

                    byte[] data = new byte[2048];

                    int recv = sock.ReceiveFrom(data, ref ep);

                    string stringData = Encoding.ASCII.GetString(data, 0, recv);
                    encrypted = cryp.StringToByteA(stringData);
                    stringData = cryp.DecryptStringFromBytes_Aes(encrypted);

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(stringData);

                    conserver1 = doc.SelectSingleNode("/Settings/ConnectionString").InnerText.ToString();

                    string cardKey = doc.SelectSingleNode("/Settings/CardKey").InnerText.ToString();

                    string cardKeyReg = "";

                    if (conserver1 != null && conserver1.Substring(0, "data source".Length).ToLower().CompareTo("data source") == 0)
                    {
                        if (currRegistryKey.GetValue("conserverReg") != null)
                        {
                            conserverReg = currRegistryKey.GetValue("conserverReg").ToString();
                            encrypted = cryp.StringToByteA(conserverReg);
                            conserver = cryp.DecryptStringFromBytes_Aes(encrypted);
                        }
                        if (conserver1 != conserver)
                        {
                            encrypted = cryp.EncryptStringToBytes_Aes(conserver1);
                            currRegistryKey.SetValue("conserverReg", cryp.ByteAToString(encrypted));
                            lock (Locker)
                            {
                                ConnectStringUpdated = true;
                            }
                        }

                        if (currRegistryKey.GetValue("KeyA") != null)
                        {
                            cardKeyReg = cryp.DecryptStringFromBytes_Aes((byte[])currRegistryKey.GetValue("KeyA"));
                        }
                        if (cardKey != null && cardKeyReg != cardKey)
                        {
                            encrypted = cryp.EncryptStringToBytes_Aes(cardKey);
                            currRegistryKey.SetValue("KeyA", encrypted, RegistryValueKind.Binary);
                            lock (Locker)
                            {
                                ConnectStringUpdated = true;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                sock.Close();

            }
        }
    }
}
