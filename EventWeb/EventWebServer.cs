﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Xml;
using CommonClassLibrary;

/// <summary>
/// netsh http add urlacl url=http://+:3333/ user=RPS
/// </summary>
/// 
namespace EventWeb
{
    public class EventWebServer
    {
        public static DataContractSerializer serializer;
        private readonly HttpListener _listener = new HttpListener();
        private readonly Func<HttpListenerRequest, HttpResp> _responderMethod;
        public struct HttpResp
        {
            public HttpStatusCode StatusCode;
            public string Response;
        }
        Log log;

        public EventWebServer(string[] prefixes, Func<HttpListenerRequest, HttpResp> method, Log Log = null)
        {
            log = Log;
            /*if (log != null)
                log.ToLog(2, "EventWebServer");*/
            try
            {
                if (!HttpListener.IsSupported)
                    throw new NotSupportedException(
                        "Needs Windows XP SP2, Server 2003 or later.");

                serializer = new DataContractSerializer(typeof(Response));

                // URI prefixes are required, for example 
                // "http://localhost:8080/index/".
                if (prefixes == null || prefixes.Length == 0)
                    throw new ArgumentException("prefixes");

                // A responder method is required
                if (method == null)
                    throw new ArgumentException("method");

                foreach (string s in prefixes)
                    _listener.Prefixes.Add(s);

                _responderMethod = method;
                /*if (log != null)
                    log.ToLog(2, "EventWebServer: _listener.Start");*/
                _listener.Start();
            }
            catch (Exception exc)
            {
                if (log != null)
                {
                    log.ToLog(0, exc.ToString());
                    log.ToLog(0, "Необходимо запустить программу один раз с правами Администратора для установки необходимых разрешений.");
                }
            }
        }
        /*public EventWebServer(Func<HttpListenerRequest, HttpResp> method, Log Log, params string[] prefixes)
            : this(prefixes, method, Log)
        {
        }*/

        /*public EventWebServer(Func<HttpListenerRequest, HttpResp> method, params string[] prefixes)
            : this(prefixes, method)
        {
        }*/

        public void Run()
        {
            /*if (log != null)
                log.ToLog(2, "EventWebServer: Run");*/
            ThreadPool.QueueUserWorkItem((o) =>
            {
                //Console.WriteLine("Webserver running..." + this._listener.Prefixes.FirstOrDefault());
                try
                {
                    while (_listener.IsListening)
                    {
                        ThreadPool.QueueUserWorkItem((c) =>
                        {
                            var ctx = c as HttpListenerContext;
                            string rstr = "", rstr1 = "";
                            try
                            {

                                /*if (log != null)
                                    log.ToLog(2, "EventWebServer: Run: Request");*/
                                HttpResp Res = _responderMethod(ctx.Request);
                                rstr = Res.Response;
                                ctx.Response.StatusCode = (int)Res.StatusCode;
                                ctx.Response.AppendHeader("Control-Allow-Origin", "*");
                                ctx.Response.AppendHeader("Content-Type", "text/html; charset=UTF-8");
                                ctx.Response.ContentType = "text/html";
                                //byte[] buf = Encoding.UTF8.GetBytes("<HTML><HEAD><meta charset=\"utf-8\" Control-Allow-Origin: * /></HEAD><BODY> " + rstr + "</BODY></HTML>");
                                byte[] buf = Encoding.UTF8.GetBytes(rstr);
                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                            catch (Exception exc)
                            {
                                try
                                {
                                    if (log != null)
                                        log.ToLog(2, "EventWebServer: Run: Exception:" + exc.ToString());
                                    HttpResp Res = new HttpResp() { /*Response = exc.ToString(), */StatusCode = HttpStatusCode.OK };
                                    rstr1 = exc.ToString();
                                    ctx.Response.StatusCode = (int)Res.StatusCode;
                                    ctx.Response.AppendHeader("Control-Allow-Origin", "*");
                                    ctx.Response.AppendHeader("Content-Type", "text/html; charset=UTF-8");
                                    ctx.Response.ContentType = "text/html";
                                    //byte[] buf = Encoding.UTF8.GetBytes("<HTML><HEAD><meta charset=\"utf-8\" Control-Allow-Origin: * /></HEAD><BODY> " + rstr + "</BODY></HTML>");
                                    byte[] buf = Encoding.UTF8.GetBytes(rstr1);
                                    //byte[] buf = Encoding.UTF8.GetBytes("<HTML><HEAD><meta charset=\"utf-8\"/></HEAD><BODY> " + rstr1 + "</BODY></HTML>");
                                    ctx.Response.ContentLength64 = buf.Length;
                                    ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                                }
                                catch
                                {

                                }
                            } // suppress any exceptions
                            finally
                            {
                                // always close the stream
                                ctx.Response.OutputStream.Close();
                            }
                        }, _listener.GetContext());
                    }
                }
                catch (Exception exc)
                {
                    if (log != null)
                        log.ToLog(0, exc.ToString());
                }
            });
        }

        public void Stop()
        {
            /*if (log != null)
                log.ToLog(2, "EventWebServer: Stop");*/
            try
            {
                _listener.Stop();
                _listener.Close();
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, exc.ToString());
            }
        }
        //public CommonClassLibrary.Response ProgramStatus;

        /// <summary>
        /// Этот метод вызывается на внешние события
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        static public EventWebServer.HttpResp SendResponse(HttpListenerRequest request)
        {
            Response ProgramStatus = new Response();
            EventWebServer.HttpResp Res;
            //Console.WriteLine(request.Url.ToString());
            //Console.WriteLine(request.RawUrl);
            //Console.WriteLine();
            if (request.RawUrl.IndexOf("GetStatusHistory") != -1)
            {
                ProgramStatus.Status = WebAnswerT.Succsess;
                ProgramStatus.Now = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");
                ProgramStatus.CurrentStatus = WebStatusT.Empty;
                ProgramStatus.StatusHistory = new List<Change>(1);
                Change ch = new Change();
                ch.OldStatus = WebStatusT.Paid;
                ch.NewStatus = WebStatusT.Empty;
                ch.ChangeTime = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");
                ProgramStatus.StatusHistory.Add(ch);

            }
            else if (request.RawUrl.IndexOf("GetAmount") != -1)
            {
                ProgramStatus.Status = WebAnswerT.Succsess;
                ProgramStatus.Now = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");
                ProgramStatus.AmountSum = 500;

            }
            else if (request.RawUrl.IndexOf("StatusChanged") != -1)
            {
                ProgramStatus.Status = WebAnswerT.Succsess;
                ProgramStatus.Now = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");

            }
            else if (request.RawUrl.IndexOf("Transaction") != -1)
            {
                ProgramStatus.Status = WebAnswerT.Succsess;
                ProgramStatus.Now = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");

            }
            else if (request.RawUrl.IndexOf("Alarm") != -1)
            {
                ProgramStatus.Status = WebAnswerT.Succsess;
                ProgramStatus.Now = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");

            }
            else if (request.RawUrl.IndexOf("CheckStatus") != -1)
            {
                ProgramStatus.Status = WebAnswerT.Succsess;
                ProgramStatus.Now = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");
            }
            else
            {
                Res.StatusCode = HttpStatusCode.NotImplemented;
                Res.Response = "Function not implemented";
                return Res;
            }

            string result;
            Encoding utf8noBOM = new UTF8Encoding(false);
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = utf8noBOM;
            using (var output = new MemoryStream())
            //using (var output = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(output, settings))
                {
                    try
                    {
                        serializer.WriteObject(writer, ProgramStatus);
                    }
                    catch (Exception exc)
                    {
                        Res.StatusCode = HttpStatusCode.OK;
                        Res.Response = "Error! " + exc.ToString();
                        return Res;
                    }
                }
                result = Encoding.Default.GetString(output.ToArray());
                //result = output.ToString();
            }
            StringReader strReader = new StringReader(result);

            Res.StatusCode = HttpStatusCode.OK;
            Res.Response = result;
            return Res;
        }

        /*        static void Main(string[] args)
                {
                    ProgramStatus = new CommonClassLibrary.GetStatusHistoryResponse();
                    serializer =
              new DataContractSerializer(typeof(GetStatusHistoryResponse));

                    webServer = new EventWebServer(SendResponse, "http://localhost:8080/");
                    webServer.Run();
                }
                */
    }
}
