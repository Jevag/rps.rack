﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EventWeb
{
    [DataContract]
    public enum WebAnswerT
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Succsess,
        [EnumMember]
        TemporaryError,
        [EnumMember]
        FatalError
    };

    [DataContract]
    public enum WebStatusT
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Empty,
        [EnumMember]
        Busy,
        [EnumMember]
        Paid
    };
    [DataContract]
    public enum TransactionTypeT
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Shift_close,
        [EnumMember]
        Shift_open,
        [EnumMember]
        Payment_aprowed,
        [EnumMember]
        Payment_is_canceled_by_the_client,
        [EnumMember]
        Payment_is_canceled_by_the_client_and_money_should_be_returned
    };
    [DataContract]
    public enum WayOfPaymentT
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Cash,
        [EnumMember]
        Banking_card
    };
    [DataContract]
    public enum ConclusionCodeT
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Done,
        [EnumMember]
        Paid_Partly,
        [EnumMember]
        Payment_is_finished_but_change_had_not_been_given_or_given_not_fully
    };

    [DataContract]
    public struct Response
    {
        public bool Responsed;
        [DataMember]
        /// Статус ответа
        public WebAnswerT Status;
        [DataMember]
        public string Now;
        [DataMember]
        public decimal AmountSum;
        [DataMember]
        public string ErrorText;
        [DataMember]
        public WebStatusT CurrentStatus;
        [DataMember(EmitDefaultValue = false)]
        public List<Change> StatusHistory;
    };

    [DataContract]
    public struct TransactionT
    {
        [DataMember]
        public string TransactionTime, LastShiftOpenTime;
        [DataMember]
        public TransactionTypeT TransactionType;
        [DataMember]
        public WayOfPaymentT WayOfPayment;
        [DataMember]
        public int PurchaseSum;
        [DataMember]
        public ConclusionCodeT ConclusionCode;
        [DataMember]
        public string PaymentTime, ExitBefore;
        [DataMember]
        public int PrepaymentSum, ToPay, Paid, Issued, SumStayedInCM;
        [DataMember]
        public bool BillIsued;
        [DataMember]
        public int ErrorCodeTransactionStop;
        [DataMember]
        public int BanknotesAccepted1cny, BanknotesAccepted10cny;
        [DataMember]
        public int CoinsAccepted0_1cny, CoinsAccepted0_5cny;
        [DataMember]
        public int IssuedFromUpper, IssuedFromLower, IssuedFrom1Hopper, IssuedFrom2Hopper;
    }
    [DataContract]
    public struct Change
    {
        [DataMember]
        public string ChangeTime;
        [DataMember]
        public WebStatusT OldStatus, NewStatus;
        [DataMember(EmitDefaultValue = false)]
        public string EntryTime;
        [DataMember(EmitDefaultValue = false)]
        public int TariffID, ClientType, CardSum, AmountSum;
        [DataMember(EmitDefaultValue = false)]
        public int CashierNumber, TransactionNumber;
        [DataMember(EmitDefaultValue = false)]
        TransactionT Transaction;

    }
    [DataContract]
    public enum AlarmSourceT
    {
        Internal, CardDispenser, CardReader, CashAcceptor, CoinAcceptor, CashDispenser, CoinHopper1, CoinHopper2, SlaveController, ExternalDoor, InternalDoor, Terminal, KKM, Printer, BankModule
    }
    [DataContract]
    public enum AlarmLevelT
    {
        Green,
        Yellow,
        Red
    }
    [DataContract]
    public struct AlarmT
    {
        [DataMember]
        public AlarmLevelT AlarmLevel;
        [DataMember]
        public AlarmSourceT AlarmSource;
        [DataMember]
        public string Status;
        [DataMember]
        public int ErrorCode;
    }

    //    }
}
