﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Xml;
using CommonClassLibrary;

namespace EventWeb
{
    public class EventWebClient
    {
        Log log;
        public Response response = new Response();
        public string Response_str = "";
        private string baseURL = null;
        public string BaseURL
        {
            get
            {
                return baseURL;
            }
            set
            {
                if ((value != null) && (value != ""))
                    if (value.IndexOf("http://") >= 0)
                        baseURL = value;
                    else
                        baseURL = "http://" + value;
                else
                    baseURL = null;

                if ((baseURL != null) && (baseURL.Length > 0) && (baseURL[baseURL.Length - 1] == '/'))
                    baseURL = baseURL.Substring(0, baseURL.Length - 2);
            }
        }

        public EventWebClient(string BaseURL)
        {
            /*if (log != null)
                log.ToLog(2, "EventWebClient");*/
            try
            {
                if ((BaseURL != null) && (BaseURL != ""))
                    if (BaseURL.IndexOf("http://") >= 0)
                        baseURL = BaseURL;
                    else if (BaseURL.IndexOf("https://") >= 0)
                        baseURL = BaseURL;
                    else
                        baseURL = "http://" + BaseURL;
                else
                    baseURL = null;

                if ((baseURL != null) && (baseURL.Length > 0) && (baseURL[baseURL.Length - 1] == '/'))
                    baseURL = baseURL.Substring(0, baseURL.Length - 2);
            }
            catch (Exception e)
            {
                if (log != null)
                    log.ToLog(0, e.Message);
            }
        }
        public EventWebClient(string BaseURL, Log Log)
        {
            log = Log;
            /*if (log != null)
                log.ToLog(2, "EventWebClient");*/
            try
            {
                if ((BaseURL != null) && (BaseURL != ""))
                    if (BaseURL.IndexOf("http://") >= 0)
                        baseURL = BaseURL;
                    else if (BaseURL.IndexOf("https://") >= 0)
                        baseURL = BaseURL;
                    else
                        baseURL = "http://" + BaseURL;
                else
                    baseURL = null;

                if ((baseURL != null) && (baseURL.Length > 0) && (baseURL[baseURL.Length - 1] == '/'))
                    baseURL = baseURL.Substring(0, baseURL.Length - 2);
            }
            catch (Exception e)
            {
                if (log != null)
                    log.ToLog(0, e.Message);
            }
        }

        public void Process(string Request)
        {
            /*if (log != null)
                log.ToLog(2, "EventWebClient: Process");*/
            try
            {
                response.Responsed = false;
                if (baseURL == null)
                {
                    response.Status = WebAnswerT.FatalError;
                    response.ErrorText = "В EventWebClient не задан базвый URL.";
                }
                else
                {
                    try
                    {
                        string Req;
                        Req = baseURL + Request;


                        HttpWebRequest wrGETURL;
                        wrGETURL = (HttpWebRequest)WebRequest.Create(Req);
                        wrGETURL.KeepAlive = false;
                        //wrGETURL.Timeout = 1000;
                        try
                        {
                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"EventWebClient.txt", true))
                            {
                                file.WriteLine(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff") + ": " + Req);
                            }
                        }
                        catch { }


                        try
                        {
                            Stream objStream = wrGETURL.GetResponse().GetResponseStream();
                            System.IO.StreamReader sr = new System.IO.StreamReader(objStream);
                            Response_str = sr.ReadToEnd();
                            sr.Close();
                            objStream.Close();

                            try
                            {
                                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"EventWebClient.txt", true))
                                {
                                    file.WriteLine(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff") + ": " + "Responsed: " + Response_str);
                                }
                            }
                            catch { }
                        }
                        catch (Exception e)
                        {
                            try
                            {
                                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"EventWebClient.txt", true))
                                {
                                    file.WriteLine(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff") + ": " + e.ToString());
                                }
                            }
                            catch { }
                        };

                    }
                    catch
                    {
                    }
                }
                response.Responsed = true;

            }
            catch (Exception e)
            {
                if (log != null)
                    log.ToLog(0, e.Message);
            }
        }

    }
}
