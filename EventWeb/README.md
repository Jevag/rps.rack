1. Включить в решение проект EventWeb
2. Добавить переменную private static EventWebServer webServer = null;
3. В модуле старта программы добавить строки
            string[] pref = new string[] { "http://+:3333/" };
            webServer = new EventWebServer(pref, SendResponse);
            webServer.Run();
4. По примеру Cashier/MainFormEventSenders.cs сделать нужные BackgroundWorker, которые работают в отдельных потоках и общаются с главным модулем только через определенные переменные. Например TalkOperatorEventSenderBackgroundWorker. Там достаточно понятно.
5. В модуле старта программы добавить строки
            TalkOperatorEventSenderBackgroundWorker = new BackgroundWorker();
            InitializeTalkOperatorEventSenderBackgroundWorker();
            TalkOperatorEventSenderBackgroundWorker.RunWorkerAsync();

            Talk = new OperatorTalk(TechForm.TechPanel.SettingsPanel.OtherFeaturesPanel.ServerUrl.Text, device.Id.ToString());
            Talk.Init();

Это пример инициализации взаимодействия с оператором.
TechForm.TechPanel.SettingsPanel.OtherFeaturesPanel.ServerUrl.Text - это ServerURL.

Например TalkOperatorEventSenderBackgroundWorker работает с классом
        public OperatorTalk Talk;
Код класса в Cashier/OperatorTalk.cs
Класс при инициализации стартует c:\GoogleChromePortable\GoogleChromePortable.exe с необходимыми параметрами.
Важно - Talk.ServerURL при инициализации должен передаваться без замыкающего слеша (http://rps.sytes.net) иначе голос работать не будет.

У Talk можно менять ServerURL и DeviceId - при этом Хром рестартует с новыми параметрами.

SendResponse - функция обработки запросов к WebServer (устройства) в примере Cashier/MainFormEventSenders.cs. Там можно убрать обработку лишнего и добавить необходимое.
В этом-же файле идет формирование запросов к главному серверу. Там тоже можно убрать лишнее и добавить необходимое.

6. Для того,чтобы можно запускать программу с http-listener необходимо дать разрешение на использование порта командой типа:

netsh http add urlacl url=http://+:3333/ user=DOMAIN\user

команда выполняется под правами Админа.
