﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlBaseImport.Model
{
    public class UtilsCalculate
    {
        protected int? Level
        {
            get;
            set;
        }

        private SqlConnection connect = null;

        public UtilsCalculate(SqlConnection connection)
        {
            Level = null;
            connect = connection;
            string comm = "select top 1 CurrentLevel from ConfigSync ";
            using (SqlCommand c = new SqlCommand(comm, connect))
            {
                object objlevel = c.ExecuteScalar();
                if (objlevel != null && objlevel != System.DBNull.Value)
                {
                    Level = Convert.ToInt32(objlevel);
                }
            }
        }

        public DataTable ReadColumns()
        {
            //string constr = "Data Source=MSI; Initial Catalog=MDAOperator; User Id=sa; Password=1";
            //"--IDENT_CURRENT(T.TABLE_SCHEMA + '.' + T.TABLE_NAME) [CURRENT_IDENTITY_VALUE],
            string query = " SELECT T.TABLE_SCHEMA, T.TABLE_NAME, C.COLUMN_NAME, C.DATA_TYPE, " +
                         " COLUMNPROPERTY (OBJECT_ID(T.TABLE_SCHEMA + '.' + T.TABLE_NAME),C.COLUMN_NAME, 'IsIdentity') as IsIdentity, " +
                         " COLUMNPROPERTY (OBJECT_ID(T.TABLE_SCHEMA + '.' + T.TABLE_NAME),C.COLUMN_NAME, 'AllowsNull') as IsAllowsNull, " +
                         " COLUMNPROPERTY (OBJECT_ID(T.TABLE_SCHEMA + '.' + T.TABLE_NAME),C.COLUMN_NAME, 'Precision') as [Length], " +
                         " COLUMNPROPERTY (OBJECT_ID(T.TABLE_SCHEMA + '.' + T.TABLE_NAME),C.COLUMN_NAME, 'IsComputed') as [IsComputed] " +
                         " FROM INFORMATION_SCHEMA.TABLES AS T (NOLOCK) " +
                         " INNER JOIN INFORMATION_SCHEMA.COLUMNS AS C (NOLOCK)  " +
                         " ON C.TABLE_SCHEMA = T.TABLE_SCHEMA AND C.TABLE_NAME = T.TABLE_NAME " +
                         " WHERE T.TABLE_TYPE = 'BASE TABLE' " +
                         " ORDER BY T.TABLE_SCHEMA, T.TABLE_NAME, C.COLUMN_NAME";

            string query2 = " SELECT " +
        " FK_Table  = FK.TABLE_NAME, " +
        " FK_Column = CU.COLUMN_NAME, " +
        " PK_Table  = PK.TABLE_NAME, " +
        " PK_Column = PT.COLUMN_NAME, " +
        " Constraint_Name = C.CONSTRAINT_NAME " +
        " FROM " +
        " INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS C " +
        " INNER JOIN " +
        " INFORMATION_SCHEMA.TABLE_CONSTRAINTS FK " +
        " ON C.CONSTRAINT_NAME = FK.CONSTRAINT_NAME " +
        " INNER JOIN " +
        " INFORMATION_SCHEMA.TABLE_CONSTRAINTS PK " +
        " ON C.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME " +
        " INNER JOIN " +
        " INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU " +
        " ON C.CONSTRAINT_NAME = CU.CONSTRAINT_NAME " +
        " INNER JOIN " +
        " ( " +
        "    SELECT " +
        "        i1.TABLE_NAME, i2.COLUMN_NAME " +
        "    FROM " +
        "        INFORMATION_SCHEMA.TABLE_CONSTRAINTS i1 " +
        "        INNER JOIN " +
        "        INFORMATION_SCHEMA.KEY_COLUMN_USAGE i2 " +
        "        ON i1.CONSTRAINT_NAME = i2.CONSTRAINT_NAME " +
        "        WHERE i1.CONSTRAINT_TYPE = 'PRIMARY KEY' " +
        " ) PT " +
        " ON PT.TABLE_NAME = PK.TABLE_NAME";


            DataTable tbl = new DataTable();
            using (SqlDataAdapter adap = new SqlDataAdapter(query, connect))
            {

                adap.Fill(tbl);

            }
            DataTable tblfk = new DataTable();
            using (SqlDataAdapter adap = new SqlDataAdapter(query2, connect))
            {
                adap.Fill(tblfk);
            }

            tbl.Columns.Add("LinkTable", typeof(string));
            tbl.Columns.Add("LinkField", typeof(string));
            foreach (DataRow row in tbl.Rows)
            {
                string tblname = Convert.ToString(row["TABLE_NAME"]);
                string colname = Convert.ToString(row["COLUMN_NAME"]);
                var find = (from t in tblfk.Rows.OfType<DataRow>() where tblname.Equals(t["FK_Table"]) && colname.Equals(t["FK_Column"]) select t).FirstOrDefault();
                if (find != null)
                {
                    row["LinkTable"] = find["PK_Table"];
                    row["LinkField"] = find["PK_Column"];
                }
                else
                {
                    row["LinkTable"] = string.Empty;
                    row["LinkField"] = string.Empty;
                }
            }
            return tbl;
        }

        public SortedList<string, InfoTable> FillTable(DataTable table, List<string> needtables)
        {
            SortedList<string, InfoTable> sort = new SortedList<string, InfoTable>();

            foreach (DataRow row in table.Rows)
            {
                object objtable = row["TABLE_NAME"];
                if (objtable != null && objtable != System.DBNull.Value)
                {
                    string nametable = Convert.ToString(objtable);
                    if (!string.IsNullOrEmpty(nametable) && !string.IsNullOrWhiteSpace(nametable))
                    {
                        string name = nametable.Trim();
                        if (!sort.ContainsKey(name) && needtables.Contains(name))
                        {
                            InfoTable inf = new InfoTable();
                            inf.TableName = name;
                            inf.FillTable(table, needtables);
                            sort.Add(name, inf);
                        }
                    }
                }
            }
            return sort;
        }

        public void CalculateOrder(SortedList<string, InfoTable> sorted)
        {
            List<string> uselist = new List<string>();
            bool flag = true;
            int order = 1;
            int oldcount = 0;
            do
            {
                var items = from t in sorted.Values where t.Order == 0 select t;
                int newcount = items.Count();
                if (newcount != oldcount)
                {
                    oldcount = newcount;
                    if (items.Count() > 0)
                    {
                        foreach (var item in items)
                        {
                            if (item.CanUpdate(uselist))
                            {
                                item.Order = order;
                                order++;
                                uselist.Add(item.TableName);
                            }
                        }
                    }
                    else
                    {
                        flag = false;
                    }
                }
                else
                { flag = false; }

            }
            while (flag);
        }

        public void RunData()
        {

            var table = ReadColumns();

            var lst = ReadTableBySyncFrom(1,"SyncFrom");
            var fillinfo = FillTable(table, lst);
            CalculateOrder(fillinfo);
            UpdateOrder(fillinfo,"OrderSync");

            var lst2 = ReadTableBySyncFrom(2,"SyncFrom");
            var fillinfo2 = FillTable(table, lst2);
            CalculateOrder(fillinfo2);
            UpdateOrder(fillinfo2,"OrderSync");

            var lst3 = ReadTableBySyncFrom(1, "SyncFrom2");
            var fillinfo3 = FillTable(table, lst);
            CalculateOrder(fillinfo3);
            UpdateOrder(fillinfo3, "OrderSync2");

            var lst4 = ReadTableBySyncFrom(2, "SyncFrom2");
            var fillinfo4 = FillTable(table, lst4);
            CalculateOrder(fillinfo4);
            UpdateOrder(fillinfo4, "OrderSync2");
        }

        public void UpdateOrder(SortedList<string,InfoTable> data,string fieldorder)
        {
            string comupdate = string.Empty;
            comupdate = "update SyncTables set "+ fieldorder + "=@ord where TableName=@tbl";
            if (!string.IsNullOrEmpty(comupdate) && !string.IsNullOrWhiteSpace(comupdate))
            {
                using (SqlCommand command = new SqlCommand(comupdate, connect))
                {
                    SqlParameter prord = new SqlParameter("@ord", 1);
                    SqlParameter prtbl = new SqlParameter("@tbl", "Table1");
                    command.Parameters.Add(prord);
                    command.Parameters.Add(prtbl);
                    foreach (var item in data.Values)
                    {
                        prord.Value = item.Order;
                        prtbl.Value = item.TableName;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public List<string> ReadTableBySyncFrom(int SyncFrom,string fieldsync)
        {
            List<string> result = new List<string>();

            string seltxtcommand = string.Empty;

                seltxtcommand = "select * from SyncTables where "+ fieldsync + "=" + SyncFrom.ToString();
            

            if (!string.IsNullOrEmpty(seltxtcommand) && !string.IsNullOrWhiteSpace(seltxtcommand))
            {
                using (SqlDataAdapter adap = new SqlDataAdapter(seltxtcommand, connect))
                {
                    DataTable tbl = new DataTable();
                    adap.Fill(tbl);
                    foreach(DataRow row in tbl.Rows)
                    {
                        object objtablename = row["TableName"];
                        if(objtablename!=null && objtablename!=System.DBNull.Value)
                        {
                            string tablename = Convert.ToString(objtablename).Trim();
                            if(!string.IsNullOrEmpty(tablename) && !string.IsNullOrWhiteSpace(tablename) && !result.Contains(tablename))
                            {
                                result.Add(tablename);
                            }
                        }
                    }
                }
            }

            return result;
        }

    }

    public class InfoTable
    {
        public string TableName { get; set; }
        public List<string> linktables = new List<string>();

        private int order = 0;
        public int Order
        {
            get { return order; }
            set { order = value; }
        }

        public bool CanUpdate(List<string> hastable)
        {
            bool result = true;
            foreach(var item in linktables)
            {
                if(!hastable.Contains(item))
                { result = false;break; }
            }

            return result;
        }

        public void FillTable(DataTable table,List<string> usetable)
        {
            var rows=table.Select("TABLE_NAME='" + TableName + "'");
            foreach(DataRow row in rows)
            {
                object objlink = row["LinkTable"];
                if(objlink!=null && objlink!=System.DBNull.Value)
                {
                    string linktable = Convert.ToString(objlink);
                    if(!string.IsNullOrEmpty(linktable) && !string.IsNullOrWhiteSpace(linktable))
                    {
                        string ltable = linktable.Trim();
                        if(!linktables.Contains(ltable) && !ltable.Equals(TableName) && usetable.Contains(ltable))
                        {
                            linktables.Add(ltable);
                        }
                    }
                }
            }
        }
    }
}
