﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlBaseImport.Model
{
    public class Table
    {
        public TypeSync Sync { get; set; }
        public string NameTable { get; set; }
        public string FieldUpdate { get; set; }
        public int Order { get; set; }

        private bool isRemove = false;
        public bool IsRemove
        {
            get { return isRemove; }
            set { isRemove = value; }
        }

        public Table()
        {
            DeviceLevel = 1;
        }

        public int DeviceLevel { get; set; }
        public string GetNameTableSync()
        {
            string res = "SyncTables";
            //if (DeviceLevel > 1) { res = "SyncTables3"; }
            return res;
        }

        public string UslovieRemove { get; set; }

        private SortedList<string, Field> fields = new SortedList<string, Field>();

        public Table AddField(string namefield/*,SqlDbType type*/)
        {
            if (!fields.ContainsKey(namefield))
            {
                //fields.Add(namefield, new Field() {IsKey=false,NameField=namefield,TypeField=type });
                fields.Add(namefield, new Field() { IsKey = false, NameField = namefield });
            }
            return this;
        }

        public Table SetKey(string namefield)
        {
            var finds = from t in fields.Values where t.IsKey select t;
            if (finds != null)
            {
                foreach (var item in finds.ToArray())
                {
                    item.IsKey = false;
                }
            }
            if (fields.ContainsKey(namefield))
            {
                fields[namefield].IsKey = true;
            }
            else
            {
                fields.Add(namefield, new Field() { IsKey = true, NameField = namefield });
            }
            return this;
        }

        public object GetKeyValue(DataRow row)
        {
            object result = null;
            var find = (from t in fields.Values where t.IsKey select t).FirstOrDefault();
            if (find != null && row != null)
            {
                if (row.Table.Columns.Contains(find.NameField))
                {
                    object res = row[find.NameField];
                    if (res != null && res != System.DBNull.Value)
                    {
                        result = res;
                    }
                }
            }
            return result;
        }

        public object GetData(DataRow row, string namefield)
        {
            object result = null;
            if (row != null)
            {
                if (row.Table.Columns.Contains(namefield))
                {
                    object res = row[namefield];
                    if (res != null && res != System.DBNull.Value)
                    {
                        result = res;
                    }
                }
            }
            return result;
        }



        public void InsertFromRow(SqlConnection connect, DataRow row, SqlTransaction transact = null)
        {
            StringBuilder strfl = new StringBuilder();
            StringBuilder strdata = new StringBuilder();
            using (SqlCommand comm = new SqlCommand())
            {
                comm.Connection = connect;
                bool isfirst = true;
                foreach (var field in fields.Values)
                {
                    object dat = GetData(row, field.NameField);
                    if (dat != null)
                    {
                        if (isfirst)
                        {
                            strfl.Append("[" + field.NameField + "]");
                            strdata.Append("@" + field.NameField);
                            isfirst = false;
                        }
                        else
                        {
                            strfl.Append(",[" + field.NameField + "]");
                            strdata.Append(",@" + field.NameField);
                        }
                        SqlParameter par = new SqlParameter("@" + field.NameField, dat);
                        //par.SqlDbType = field.TypeField;
                        comm.Parameters.Add(par);
                    }
                }
                string strins = "insert into [" + NameTable + "](" + strfl.ToString() + ") values (" + strdata.ToString() + ")";
                comm.CommandText = strins;
                if (transact != null) { comm.Transaction = transact; }
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateFromRow(SqlConnection connect, DataRow row, SqlTransaction transact = null)
        {
            StringBuilder set = new StringBuilder();
            bool isfirst = true;
            set.Append("update [" + NameTable + "] set ");
            object key = GetKeyValue(row);
            if (key != null)
            {
                using (SqlCommand comm = new SqlCommand())
                {
                    foreach (var field in fields.Values)
                    {
                        object dat = GetData(row, field.NameField);
                        if (dat != null)
                        {
                            if (isfirst)
                            {
                                set.Append("[" + field.NameField + "]=@" + field.NameField); isfirst = false;
                            }
                            else
                            {
                                set.Append(",[" + field.NameField + "]=@" + field.NameField);
                            }
                            SqlParameter par = new SqlParameter("@" + field.NameField, dat);
                            //par.SqlDbType = field.TypeField;
                            comm.Parameters.Add(par);
                        }
                    }
                    var findkey = (from t in fields.Values where t.IsKey select t).FirstOrDefault();
                    if (findkey != null)
                    {
                        string strcomm = set.ToString() + " where [" + findkey.NameField + "]=@Key_" + findkey.NameField;
                        SqlParameter par = new SqlParameter("@Key_" + findkey.NameField, key);
                        //par.SqlDbType = findkey.TypeField;
                        comm.Parameters.Add(par);
                        comm.CommandText = strcomm;
                        comm.Connection = connect;
                        if (transact != null) { comm.Transaction = transact; }

                        comm.ExecuteNonQuery();
                    }
                }
            }
        }


        public DataTable SelectTable(SqlConnection connect, string namefield, DateTime datafrom, DateTime datato, SqlTransaction transact = null)
        {
            DataTable table = new DataTable();
            using (SqlCommand comm = new SqlCommand())
            {
                StringBuilder set = new StringBuilder();
                bool isfirst = true;
                foreach (var field in fields.Values)
                {
                    if (isfirst)
                    { set.Append("[" + field.NameField + "]"); isfirst = false; }
                    else { set.Append(",[" + field.NameField + "]"); }
                }
                isfirst = true;
                StringBuilder strwhere = new StringBuilder();
                if (!string.IsNullOrEmpty(namefield))//&& fields.ContainsKey(namefield))
                {
                    //var fl = fields[namefield];
                    if (datafrom != null)
                    {
                        strwhere.Append("[" + namefield + "]>@from");
                        isfirst = false;
                        SqlParameter par = new SqlParameter("@from", datafrom);
                        //par.SqlDbType = fl.TypeField;
                        comm.Parameters.Add(par);
                    }
                    if (datato != null)
                    {
                        if (isfirst) { strwhere.Append("[" + namefield + "]<=@to"); isfirst = false; }
                        else { strwhere.Append(" and [" + namefield + "]<=@to"); }
                        SqlParameter par = new SqlParameter("@to", datato);
                        //par.SqlDbType = fl.TypeField;
                        comm.Parameters.Add(par);
                    }
                }
                StringBuilder strres = new StringBuilder();
                strres.Append("select ");
                strres.Append(set.ToString());
                strres.Append(" from [" + NameTable + "]");
                if (!isfirst)
                {
                    strres.Append(" where ");
                    strres.Append(strwhere.ToString());
                }
                comm.CommandText = strres.ToString();
                comm.Connection = connect;
                if (transact != null) { comm.Transaction = transact; }
                using (SqlDataAdapter adap = new SqlDataAdapter(comm))
                {
                    adap.Fill(table);
                }
            }
            return table;
        }

        public bool HasRow(SqlConnection connect, DataRow row, SqlTransaction transact = null)
        {
            bool result = true;
            var findkey = (from t in fields.Values where t.IsKey select t).FirstOrDefault();

            if (findkey != null)
            {
                object key = GetData(row, findkey.NameField);
                if (key != null)
                {
                    result = false;
                    string command = "select top 1 [" + findkey.NameField + "] from [" + NameTable + "] where [" + findkey.NameField + "]=@key";
                    SqlParameter pr = new SqlParameter("@key", key);
                    //pr.SqlDbType = findkey.TypeField;
                    using (SqlCommand comm = new SqlCommand(command, connect))
                    {
                        if (transact != null) { comm.Transaction = transact; }
                        comm.Parameters.Add(pr);
                        object res = comm.ExecuteScalar();
                        if (res != null && res != System.DBNull.Value)
                        {
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        /*
        public void SyncTable(SqlConnection from, SqlConnection to, DateTime datafrom, DateTime datato, string FileLog,
            SqlTransaction transact = null)
        {
            if (TestNeedSinc(from, Convert.ToDateTime(datafrom)))
            {

                if (!string.IsNullOrEmpty(FieldUpdate) && !string.IsNullOrWhiteSpace(FieldUpdate))
                {
                    //Из from берем все строки за указанный промежуток
                    var fromtable = SelectTable(from, FieldUpdate, datafrom, datato, transact);
                    //Проходим по каждой строку таблица и ищем ей соответсвие в соответсвующей таблице to
                    if (fromtable != null && fromtable.Rows.Count > 0)
                    {
                        foreach (DataRow row in fromtable.Rows)
                        {
                            try
                            {
                                if (HasRow(to, row))
                                {
                                    UpdateFromRow(to, row, transact);
                                }
                                else
                                {
                                    InsertFromRow(to, row, transact);
                                }
                            }
                            catch (Exception ex)
                            {
                                SaveLog(FileLog, "Error: " + ex.Message, "Error");
                            }
                        }
                        fromtable.Dispose();
                    }
                }
            }
        }
        */

        public DataTable SelectTableAllField(SqlConnection connect, string namefield, DateTime datafrom, DateTime datato, SqlTransaction transact = null)
        {
            DataTable table = new DataTable();

            using (SqlCommand comm = new SqlCommand())
            {
                bool isfirst = true;
                StringBuilder strwhere = new StringBuilder();
                if (!string.IsNullOrEmpty(namefield))//&& fields.ContainsKey(namefield))
                {
                    //var fl = fields[namefield];
                    if (datafrom != null)
                    {
                        strwhere.Append("[" + namefield + "]>@from");
                        isfirst = false;
                        SqlParameter par = new SqlParameter("@from", datafrom);
                        //par.SqlDbType = fl.TypeField;
                        comm.Parameters.Add(par);
                    }
                    if (datato != null)
                    {
                        if (isfirst) { strwhere.Append("[" + namefield + "]<=@to"); isfirst = false; }
                        else { strwhere.Append(" and [" + namefield + "]<=@to"); }
                        SqlParameter par = new SqlParameter("@to", datato);
                        //par.SqlDbType = fl.TypeField;
                        comm.Parameters.Add(par);
                    }
                }
                StringBuilder strres = new StringBuilder();
                strres.Append("select * ");
                strres.Append(" from [" + NameTable + "]");
                if (!isfirst)
                {
                    strres.Append(" where ");
                    strres.Append(strwhere.ToString());
                }
                comm.CommandText = strres.ToString();
                comm.Connection = connect;
                if (transact != null) { comm.Transaction = transact; }
                using (SqlDataAdapter adap = new SqlDataAdapter(comm))
                {
                    adap.Fill(table);
                }
            }
            return table;
        }

        public bool SyncTableAllField(SqlConnection from, SqlConnection to, DateTime datafrom, DateTime dateto, string FileLog, SqlTransaction transact = null)
        {
            //List<Guid> lstsync = new List<Guid>();
            List<object> lstsync = new List<object>();
            bool result = true;
            // if (TestNeedSinc(from, Convert.ToDateTime(datafrom)))
            //{
            if (!string.IsNullOrEmpty(FieldUpdate) && !string.IsNullOrWhiteSpace(FieldUpdate))
            {
                //Из from берем все строки за указанный промежуток
                var fromtable = SelectTableAllField(from, FieldUpdate, datafrom, dateto, transact);
                //Заполняем список полей

                var zapto = "select top 1 * from [" + NameTable + "]";
                DataTable tableto = new DataTable();
                using (var adap = new SqlDataAdapter(zapto, to))
                {
                    adap.Fill(tableto);
                }

                foreach (var fl in this.fields.Values.ToArray())
                {
                    if (!fl.IsKey)
                    {
                        this.fields.Remove(fl.NameField);
                    }
                }

                foreach (DataColumn col in fromtable.Columns)
                {
                    string field = col.ColumnName;
                    if (!string.IsNullOrEmpty(field) && !string.IsNullOrWhiteSpace(field) && !field.Equals(FieldUpdate))
                    {
                        if (tableto.Columns.Contains(field))
                        {
                            this.AddField(field);
                        }
                    }
                }

                //Проходим по каждой строку таблица и ищем ей соответсвие в соответсвующей таблице to
                if (fromtable != null && fromtable.Rows.Count > 0)
                {
                    var findkey = (from t in fields.Values where t.IsKey select t).FirstOrDefault();

                    foreach (DataRow row in fromtable.Rows)
                    {
                        try
                        {
                            object data = GetData(row, findkey.NameField);
                            lstsync.Add((dynamic)data);
                            if (HasRow(to, row))
                            {
                                UpdateFromRow(to, row, transact);
                            }
                            else
                            {
                                InsertFromRow(to, row, transact);
                            }
                        }
                        catch (Exception ex)
                        {
                            result = false;
                            SaveLog(FileLog, "Error in table \""+NameTable+"\" : " + ex.Message, "Error");
                            break;
                        }
                    }
                    fromtable.Dispose();
                }
                
                if(result && lstsync.Count>0 && this.IsRemove)
                {
                    RemoveRows(from, lstsync);
                }
                
            }
            //}
            return result;
        }

        public void RemoveRows(SqlConnection connect,List<object> lst)
        {
            int current = 0;
            int maxcount = 500;
            int index = 0;
            while (current < lst.Count)
            {
                var items = (from t in lst select t).Skip(index * maxcount).Take(maxcount).ToArray();
                if (items != null && items.Length > 0)
                {
                    var find = (from t in fields.Values where t.IsKey select t).FirstOrDefault();
                    StringBuilder strbuild = new StringBuilder();
                    strbuild.Append("delete from " + this.NameTable + " where " + find.NameField + " in (");
                    bool isfirst = true;
                    using (SqlCommand comm = new SqlCommand())
                    {
                        int ind = 1;
                        foreach (var item in items)
                        {
                            string nameparam = "@pdel" + ind.ToString();
                            if (isfirst)
                            {
                                strbuild.Append(nameparam);
                                isfirst = false;
                            }
                            else
                            {
                                strbuild.Append(", " + nameparam);
                            }
                            SqlParameter sparam = new SqlParameter(nameparam, (dynamic)item);
                            comm.Parameters.Add(sparam);
                            ind++;
                        }
                        strbuild.Append(")");
                        comm.Connection = connect;
                        //string comand = strbuild.ToString();
                        if (!string.IsNullOrEmpty(this.UslovieRemove) && !string.IsNullOrWhiteSpace(this.UslovieRemove))
                        {
                            strbuild.Append(" and " + this.UslovieRemove);
                        }
                        comm.CommandText = strbuild.ToString();
                        comm.ExecuteNonQuery();
                    }
                }
                index++;
                current = maxcount * index;
            }
        }

        public bool TestNeedSinc(SqlConnection connect, DateTime olddate)
        {
            bool result = true;
            string strcomm = "select * from "+GetNameTableSync()+" where TableName=@ntable";
            using (SqlCommand comm = new SqlCommand(strcomm, connect))
            {
                comm.Parameters.Add(new SqlParameter("@ntable", NameTable));
                //comm.Parameters.Add(new SqlParameter("@date", olddate));
                using (SqlDataAdapter adap = new SqlDataAdapter(comm))
                {
                    DataTable table = new DataTable();
                    adap.Fill(table);
                    if (table.Rows != null && table.Rows.Count > 0)
                    {
                        object objdate = table.Rows[0]["DateTime"];
                        if (objdate != null && objdate != System.DBNull.Value)
                        {
                            DateTime lastupdate = Convert.ToDateTime(objdate);
                            if (lastupdate >= olddate) { result = true; }
                            else { result = false; }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                    table.Dispose();
                }
            }
            return result;
        }

        public void SaveLog(string filelog, string Message, string status)
        {
            string mes = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
            mes = mes + ";" + status + ";" + Message + ";" + Environment.NewLine;
            File.AppendAllText(filelog, mes);
        }

        public void SetLastUpdate(SqlConnection connect, DateTime date)
        {
            string strhas = "select top 1 * from "+GetNameTableSync()+" where TableName=@tblname";
            using (SqlCommand comm = new SqlCommand(strhas, connect))
            {
                SqlParameter ptblsel = new SqlParameter("@tblname", this.NameTable);
                comm.Parameters.Add(ptblsel);
                object res = comm.ExecuteScalar();
                if (res != null && res != System.DBNull.Value)
                {
                    string upd = "update "+GetNameTableSync()+" set LastUpdate=@date where TableName=@tblname";
                    SqlParameter pr = new SqlParameter("@date", date);
                    SqlParameter param = new SqlParameter("@tblname", this.NameTable);

                    using (SqlCommand comupd = new SqlCommand(upd, connect))
                    {
                        comupd.Parameters.Add(param);
                        comupd.Parameters.Add(pr);
                        comupd.ExecuteNonQuery();
                    }
                }
                else
                {
                    string ins = "insert into "+GetNameTableSync()+" (LastUpdate,TableName) values (@date,@tblname)";
                    SqlParameter pr = new SqlParameter("@date", date);
                    SqlParameter pins = new SqlParameter("@tblname", this.NameTable);
                    using (SqlCommand comupd = new SqlCommand(ins, connect))
                    {
                        comupd.Parameters.Add(pr);
                        comupd.Parameters.Add(pins);
                        comupd.ExecuteNonQuery();
                    }
                }
            }
        }

        public DateTime GetLastUpdate(SqlConnection connect)
        {
            DateTime date = new DateTime(2000, 1, 1);
            string strhas = "select top 1 LastUpdate from "+GetNameTableSync()+" where TableName=@tblname";
            using (SqlCommand comm = new SqlCommand(strhas, connect))
            {
                SqlParameter param = new SqlParameter("@tblname", this.NameTable);
                comm.Parameters.Add(param);
                object res = comm.ExecuteScalar();
                if (res != null && res != System.DBNull.Value)
                {
                    try
                    {
                        date = Convert.ToDateTime(res);
                    }
                    catch { }
                }
            }
            return date;
        }

        public DateTime? GetLastChange(SqlConnection connect)
        {
            DateTime? date = null;
            string strhas = "select top 1 DateTime from "+GetNameTableSync()+" where TableName=@tblname";
            using (SqlCommand comm = new SqlCommand(strhas, connect))
            {
                SqlParameter param = new SqlParameter("@tblname", this.NameTable);
                comm.Parameters.Add(param);
                object res = comm.ExecuteScalar();
                if (res != null && res != System.DBNull.Value)
                {
                    try
                    {
                        date = Convert.ToDateTime(res);
                    }
                    catch { }
                }
            }
            return date;
        }

        public void ClearTable(SqlConnection connect)
        {
            string command = "delete from [" + this.NameTable+"]";
            string comupdate = "update ["+GetNameTableSync()+"] set LastUpdate=null where TableName='" + this.NameTable + "'";
            using (SqlCommand comm = new SqlCommand(command, connect))
            {
                comm.ExecuteNonQuery();
            }
            using (SqlCommand commnull = new SqlCommand(comupdate, connect))
            {
                commnull.ExecuteNonQuery();
            }

        }
    }

    public class BaseSync
    {
        public static int DeviceLevel { get; set; }
        public static string GetNameTableSync()
        {
            string res = "SyncTables";
           // if (DeviceLevel > 1) { res = "SyncTables3"; }
            return res;
        }

        private SortedList<string, Table> tables = new SortedList<string, Table>();
        public Table GetTable(string NameTable)
        {
            Table result = null;
            if (tables.ContainsKey(NameTable))
            {
                result = tables[NameTable];
            }
            else
            {
                result = new Table();
                result.NameTable = NameTable;
                tables.Add(NameTable, result);
            }
            return result;
        }

        public Table RegisterTable(string NameTable, TypeSync sync,int order)
        {
            Table result = null;
            if (!tables.ContainsKey(NameTable))
            {
                result = new Table();
                result.Order = order;
                result.Sync = sync;
                result.NameTable = NameTable;
                tables.Add(NameTable, result);
            }
            else
            {
                result = tables[NameTable];
            }
            return result;
        }

        public void SyncAllField(SqlConnection connectserver, SqlConnection connectdevice)
        {
            try
            {
                var fromservs = from t in tables.Values where t.Sync == TypeSync.FromServer orderby t.Order select t;
                foreach (var tbl in fromservs)
                {
                    try
                    {
                        SaveLog("Start \"" + tbl.NameTable, "\" Start");

                        DateTime datefrom = tbl.GetLastUpdate(connectdevice);
                        DateTime? dateto = tbl.GetLastChange(connectserver);
                        if (dateto!=null && dateto.HasValue && dateto > datefrom)
                        {
                            var res = tbl.SyncTableAllField(connectserver, connectdevice, datefrom, dateto.Value, this.FileLog);
                            if (res) { tbl.SetLastUpdate(connectdevice, dateto.Value); }
                        }
                    }
                    catch (Exception ex)
                    {
                        SaveLog("Error: \"" + tbl.NameTable + "\" Exception: " + ex.Message, "Error");
                    }
                    SaveLog("End \"" + tbl.NameTable, "\"End");
                }
                var fromdevice = from t in tables.Values where t.Sync == TypeSync.FromDevice orderby t.Order select t;
                foreach (var tbl in fromdevice)
                {
                    try
                    {
                        SaveLog("Start \"" + tbl.NameTable, "\"Start");
                        DateTime datefrom = tbl.GetLastUpdate(connectdevice);
                        DateTime? dateto = tbl.GetLastChange(connectdevice);
                        if (dateto != null && dateto.HasValue && dateto > datefrom)
                        {
                            var res = tbl.SyncTableAllField(connectdevice, connectserver, datefrom, dateto.Value, this.FileLog);
                            if (res) { tbl.SetLastUpdate(connectdevice, dateto.Value); }
                        }
                    }
                    catch (Exception ex)
                    {
                        SaveLog("Error: \"" + tbl.NameTable + "\" Exception: " + ex.Message, "Error");
                    }
                    SaveLog("End \"" + tbl.NameTable, "\" End");
                }
            }
            catch (Exception ex)
            {
                SaveLog(" Exception: " + ex.Message, "Error");
            }
        }

        private string FileLog
        {
            get
            {
                string filename = "LogSync_" + DateTime.Today.ToString("yyyy_MM_dd") + ".txt";
                string fullname = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filename);
                return fullname;
            }
        }

        public void SaveLog(string Message,string status)
        {
            if (!string.IsNullOrEmpty(status) && !string.IsNullOrWhiteSpace(status)
                && status.ToLower().Trim().Equals("error"))
            {
                string mes = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
                mes = mes + ";" + status + ";" + Message + ";" + Environment.NewLine;
                File.AppendAllText(FileLog, mes);
            }
        }

        public static BaseSync Create(SqlConnection connectserv, SqlConnection connectdevice)
        {
            return CreateSyncTables(connectdevice);
        }

        private static BaseSync CreateSyncTables(SqlConnection connectdevice)
        {
            BaseSync.DeviceLevel = -1;
            string readlevel = "select top 1 CurrentLevel from ConfigSync";

            using (SqlCommand comm = new SqlCommand(readlevel, connectdevice))
            {
                object levobj = comm.ExecuteScalar();
                if (levobj != null && levobj != System.DBNull.Value)
                {
                    BaseSync.DeviceLevel = Convert.ToInt32(levobj);
                }
            }

            BaseSync bs = new BaseSync();
            if (BaseSync.DeviceLevel > 0)
            {
                //bs.SaveLog("00", "error");          // start control
                //string command = "select * from SyncTables where TableName='TestSync'";
                string command = "select * from " + GetNameTableSync();
                using (SqlDataAdapter adap = new SqlDataAdapter(command, connectdevice))
                {
                    DataTable table = new DataTable();
                    adap.Fill(table);
                    if (table != null && table.Rows != null && table.Rows.Count > 0)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            string keyfield = GetStringValue(row["KeyField"]);
                            string syncfield = GetStringValue(row["SyncField"]);
                            int? order = GetIntValue(row["OrderSync"]);
                            if(BaseSync.DeviceLevel>=2)
                            {
                                order = GetIntValue(row["OrderSync2"]);
                            }
                            int? syncfrom = GetIntValue(row["SyncFrom"]);
                            if(BaseSync.DeviceLevel>=2)
                            {
                                syncfrom = GetIntValue(row["SyncFrom2"]);
                            }
                            string uslovremove = GetStringValue(row["UslovieRemove"]);
                            bool? isremove = GetBoolValue(row["IsRemove"]);

                            if (!string.IsNullOrEmpty(keyfield) && !string.IsNullOrWhiteSpace(keyfield)
                                && !string.IsNullOrEmpty(syncfield) && !string.IsNullOrWhiteSpace(syncfield)
                                && order != null && order.HasValue && order>0 && syncfrom != null && syncfrom.HasValue)
                            {
                                TypeSync ts = TypeSync.FromDevice;
                                if (syncfrom.Value == 2)
                                {
                                    ts = TypeSync.FromServer;
                                }

                                Table tbl = bs.RegisterTable(GetStringValue(row["TableName"]), ts, order.Value);
                                tbl.SetKey(keyfield);
                                tbl.FieldUpdate = syncfield;
                                tbl.DeviceLevel = BaseSync.DeviceLevel;
                                if (BaseSync.DeviceLevel == 1)
                                {
                                    if (isremove != null && isremove.HasValue)
                                    {
                                        tbl.IsRemove = isremove.Value;
                                    }
                                    else { tbl.IsRemove = false; }
                                    if (!string.IsNullOrEmpty(uslovremove) && !string.IsNullOrWhiteSpace(uslovremove))
                                    {
                                        tbl.UslovieRemove = uslovremove;
                                    }
                                    else { tbl.UslovieRemove = string.Empty; }
                                }
                                else
                                {
                                    tbl.IsRemove = false;
                                    tbl.UslovieRemove = string.Empty;
                                }
                            }
                        }
                    }
                }
            }
            return bs;
        }

        private static string GetStringValue(object data)
        {
            string result = string.Empty;
            if(data!=null && data!=System.DBNull.Value)
            {
                result = Convert.ToString(data);
            }
            return result;
        }

        private static int? GetIntValue(object data)
        {
            int? result = null;
            if (data != null && data !=System.DBNull.Value)
            {
                try
                {
                    result = Convert.ToInt32(data);
                }
                catch { }
            }
            return result;
        }

        private static bool? GetBoolValue(object data)
        {
            bool? result = null;
            if (data != null && data != System.DBNull.Value)
            {
                try
                {
                    result = Convert.ToBoolean(data);
                }
                catch { }
            }
            return result;

        }

        public static void ClearDevice(SqlConnection connectdevice)
        {
            BaseSync bs = CreateSyncTables(connectdevice);

            var fromservs = from t in bs.tables.Values where t.Sync == TypeSync.FromServer orderby t.Order descending select t;
            foreach (var tbl in fromservs)
            {
                try
                {
                    bs.SaveLog("Start " + tbl.NameTable, "Start");
                        tbl.ClearTable(connectdevice);
                }
                catch (Exception ex)
                {
                    bs.SaveLog("Error: " + tbl.NameTable + " Exception: " + ex.Message, "Error");
                }
                bs.SaveLog("End " + tbl.NameTable, "End");
            }

        }
    }

    public class Field
    {
        public string NameField { get; set; }
        private bool isKey = false;
        public bool IsKey
        {
            get { return isKey; }
            set { isKey = value; }
        }
        //public SqlDbType TypeField { get; set; }
    }

    public enum TypeSync
    {
        FromServer = 1,
        FromDevice = 2
    }
}
