﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Globalization;
using CommonClassLibrary;
using System.Collections.Specialized;
using System.Threading;
using System.Text;
using System.IO.Ports;
using System.IO;
using System.Diagnostics;
using Communications.Properties;

namespace Communications
{
    public class PrimFAKKM : CommonKKM
    {
        #region DllFunctions
        [DllImport("azimuth.dll", EntryPoint = "OpenDLL", CallingConvention = CallingConvention.StdCall)]
        private static extern int OpenDLL(string op_name, string Psw, string ComNum, int Oem);

        //закрытие соединения с принтером
        [DllImportAttribute("azimuth.dll", EntryPoint = "CloseDLL", CallingConvention = CallingConvention.StdCall)]
        private static extern int CloseDLL();

        //Проверить подключение
        [DllImportAttribute("azimuth.dll", EntryPoint = "CheckHealth", CallingConvention = CallingConvention.StdCall)]
        private static extern int CheckHealth();

        //получение серийного номера принтера (запрос на получение)
        [DllImportAttribute("azimuth.dll", EntryPoint = "GetSerialNum", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetSerialNum();

        //получение фискального номера принтера (запрос на получение)
        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "GetFiscalNums", CallingConvention = CallingConvention.StdCall)]
        //public static extern int GetFiscalNums();

        //получение номера последнего документа (запрос на получение)
        [DllImportAttribute("azimuth.dll", EntryPoint = "GetNumbers", CallingConvention = CallingConvention.StdCall)]
        public static extern int GetNumbers();

        //Установка параметров
        [DllImportAttribute("azimuth.dll", EntryPoint = "SetParamDoc", CallingConvention = CallingConvention.StdCall)]
        private static extern int SetParamDoc(int ParamDoc1, int ParamDoc2);

        //получение версии длл
        [DllImportAttribute("azimuth.dll", EntryPoint = "GetDllVer", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr GetDllVer(IntPtr pBuff);

        //считывание ответа от принтера (на последний запрос)
        [DllImportAttribute("azimuth.dll", EntryPoint = "GetFldStr", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr GetFldStr(byte num, IntPtr str);

        //получение количества полей
        [DllImportAttribute("azimuth.dll", EntryPoint = "GetFldsCount", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetFldsCount();

        [DllImportAttribute("azimuth.dll", EntryPoint = "SetInterfaceParam", CallingConvention = CallingConvention.StdCall)]
        private static extern int SetInterfaceParam(string BaudRate, byte Is5Wires, byte IsDateTime);

        //открытие смены
        [DllImportAttribute("azimuth.dll", EntryPoint = "ShiftOpenPlus", CallingConvention = CallingConvention.StdCall)]
        private static extern int ShiftOpenPlus(string Cashier, string OFDMessage, string SubParam, string PrintFirstString);

        //закрытие смены z-отчет
        [DllImportAttribute("azimuth.dll", EntryPoint = "ShiftClosePlus", CallingConvention = CallingConvention.StdCall)]
        private static extern int ShiftClosePlus(string Cashier, string OFDMessage, string SubParam, byte isPrinting);

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "GRunCommand", CallingConvention = CallingConvention.StdCall)]
        //private static extern int GRunCommand(byte n, string ll);

        [DllImportAttribute("azimuth.dll", EntryPoint = "GetFldWord", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetFldWord(int m);

        [DllImportAttribute("azimuth.dll", EntryPoint = "GetFldByte", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetFldByte(int m);

        //печать Х-отчета
        [DllImportAttribute("azimuth.dll", EntryPoint = "XReport", CallingConvention = CallingConvention.StdCall)]
        private static extern new int XReport();

        //получение текущей даты и времени
        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "GetDate", CallingConvention = CallingConvention.StdCall)]
        //private static extern int GetDate();

        [DllImportAttribute("azimuth.dll", EntryPoint = "FromCash", CallingConvention = CallingConvention.StdCall)]
        private static extern int FromCash(int SummToCollect);

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "OpenFiscalDoc", CallingConvention = CallingConvention.StdCall)]
        //private static extern int OpenFiscalDoc(byte DocType, byte PayType, byte FlipFOffs, byte PageNum, byte HCopyNum, byte VCopyNum, int LOffs, int VGap, int LGap, int Sum);

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "PrintFiscalReceipt", CallingConvention = CallingConvention.StdCall)]
        //private static extern int PrintFiscalReceipt();

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "AddPosField", CallingConvention = CallingConvention.StdCall)]
        /*private static extern int AddPosField(int SerNoLine, int SerNoCol, byte SerNoFont, int DocNoLine, int DocNoCol,
            byte DocNoFont, int DateLine, int DateCol, byte DateFont, int TimeLine, int TimeCol, byte TimeFont,
            int InnLine, int InnCol, byte InnFont,
            //int OperationLine, int OperationCol, byte OperationFont, int KPKLine, int KPKCol, byte KPKeFont, 
            int OperLine, int OperCol, byte OperFont, int SumLine,
            int SumCol, byte SumFont);*/

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "AddFreeField", CallingConvention = CallingConvention.StdCall)]
        //private static extern int AddFreeField(int Line, int Col, byte Font, byte PrintMode, byte JourNo, string Info);

        [DllImportAttribute("azimuth.dll", EntryPoint = "ToCashPlus", CallingConvention = CallingConvention.StdCall)]
        private static extern int ToCashPlus(int Summ, string FreeField);

        [DllImportAttribute("azimuth.dll", EntryPoint = "ToCash", CallingConvention = CallingConvention.StdCall)]
        private static extern int ToCash(int SummToCash);

        [DllImportAttribute("azimuth.dll", EntryPoint = "OpenFDoc", CallingConvention = CallingConvention.StdCall)]
        private static extern int OpenFDoc();

        //[DllImport("azimuth", EntryPoint = "PrintFDoc", CallingConvention = CallingConvention.StdCall)]
        //private static extern int PrintFDoc(string Info, long InfoLen);
        [DllImport("azimuth.dll", EntryPoint = "PrintOEMDoc", CallingConvention = CallingConvention.StdCall)]
        private static extern int PrintOEMDoc(string Info, long InfoLen);

        [DllImport("azimuth.dll", EntryPoint = "LineFeedFDoc", CallingConvention = CallingConvention.StdCall)]
        private static extern int LineFeedFDoc(byte Info);

        /*
        [DllImport("azimuth", EntryPoint = "PrintOEMDoc", CallingConvention = CallingConvention.StdCall)]
        private static extern int PrintOEMDoc(string Info, long InfoLen);

        [DllImport("azimuth", EntryPoint = "PrintOEMCRLFDoc", CallingConvention = CallingConvention.StdCall)]
        private static extern int PrintOEMCRLFDoc(string Info, long InfoLen);
        */
        [DllImportAttribute("azimuth.dll", EntryPoint = "CloseFDoc", CallingConvention = CallingConvention.StdCall)]
        private static extern int CloseFDoc();


        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "CutFDoc", CallingConvention = CallingConvention.StdCall)]
        //private static extern int CutFDoc();

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "CloseFreeDoc", CallingConvention = CallingConvention.StdCall)]
        //private static extern int CloseFreeDoc();

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "FreeDoc", CallingConvention = CallingConvention.StdCall)]
        //private static extern int FreeDoc(string Str, int StrCount);

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "FreeDocCut", CallingConvention = CallingConvention.StdCall)]
        //private static extern int FreeDocCut();

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "CloseFDocEx", CallingConvention = CallingConvention.StdCall)]
        //private static extern int CloseFDocEx(string Zagolovok, long ZagolovokLen);

        [DllImportAttribute("azimuth.dll", EntryPoint = "GetResource", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetResource();


        [DllImportAttribute("azimuth.dll", EntryPoint = "GetStatusPlus", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetStatusPlus();

        /*[DllImportAttribute("AzimuthNew.dll", EntryPoint = "GetStatus", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetStatus();*/

        [DllImportAttribute("azimuth.dll", EntryPoint = "GetStatusNoPlus", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetStatusNoPlus(int No);


        /*[DllImportAttribute("AzimuthNew.dll", EntryPoint = "ShiftCloseEx", CallingConvention = CallingConvention.StdCall)]
        private static extern int ShiftCloseEx();*/

        [DllImportAttribute("azimuth.dll", EntryPoint = "SetPrezenter", CallingConvention = CallingConvention.StdCall)]
        private static extern new int SetPrezenter(byte Isretrak, byte IsPrezenter, byte IsSet);

        [DllImportAttribute("azimuth.dll", EntryPoint = "GetStatusNum", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetStatusNum(int Num);


        [DllImportAttribute("azimuth.dll", EntryPoint = "CheckStatusNum", CallingConvention = CallingConvention.StdCall)]
        private static extern int CheckStatusNum(int Num, int BitNum);
        [DllImportAttribute("azimuth.dll", EntryPoint = "GetPrnByte", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetPrnByte(int Num);


        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "SetHeaderNew", CallingConvention = CallingConvention.StdCall)]
        //private static extern int SetHeaderNew(string Head1, string Head2, string Head3, string Head4, string Head5, string Head6);

        [DllImportAttribute("azimuth.dll", EntryPoint = "GetEReport", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetEReport(byte Command, byte param);

        [DllImportAttribute("azimuth.dll", EntryPoint = "GetFldInt", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetFldInt(byte Num);


        [DllImportAttribute("azimuth.dll", EntryPoint = "EJZreport", CallingConvention = CallingConvention.StdCall)]
        private static extern int EJZreport(int ZNo);

        [DllImportAttribute("azimuth.dll", EntryPoint = "GetShiftState", CallingConvention = CallingConvention.StdCall)]
        private static extern int GetShiftState(ref int ShiftState, ref int ShiftTime);

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "SetDTRControl", CallingConvention = CallingConvention.StdCall)]
        //private static extern int SetDTRControl(int Enable);
        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "SetDTR", CallingConvention = CallingConvention.StdCall)]
        //private static extern int SetDTR(int Enable);
        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "XonXoffEnable", CallingConvention = CallingConvention.StdCall)]
        //private static extern int XonXoffEnable(int Enable);
        [DllImportAttribute("azimuth.dll", EntryPoint = "GetFWVer", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr GetFWVer(IntPtr pBuff);
        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "FontSelectFDoc", CallingConvention = CallingConvention.StdCall)]
        //private static extern int FontSelectFDoc(byte Font);
        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "WriteComm", CallingConvention = CallingConvention.StdCall)]
        //private static extern int WriteComm(byte[] Buf, int Count);
        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "ReadComm", CallingConvention = CallingConvention.StdCall)]
        //private static extern int ReadComm(ref byte[] Buf, int Count);
        [DllImportAttribute("azimuth.dll", EntryPoint = "StartReceipt", CallingConvention = CallingConvention.StdCall)]
        private static extern int StartReceipt(byte DocType, byte Copies, byte Taxation, string TableNo, string PlaceNo, string AccountNo);

        [DllImportAttribute("azimuth.dll", EntryPoint = "StartReceiptPlus", CallingConvention = CallingConvention.StdCall)]
        private static extern int StartReceiptPlus(byte DocType, byte Copies, byte Taxation, string TableNo, string PlaceNo, string AccountNo, string FreeField);

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "CommentReceipt", CallingConvention = CallingConvention.StdCall)]
        //private static extern int CommentReceipt(string Buf);


        [DllImportAttribute("azimuth.dll", EntryPoint = "ItemReceipt", CallingConvention = CallingConvention.StdCall)]
        private static extern int ItemReceipt(string WareName, string WareCode, string Measure, string SecID, int Price, int Count, short WareType);

        [DllImportAttribute("azimuth.dll", EntryPoint = "ItemReceiptPlus20", CallingConvention = CallingConvention.StdCall)]
        private static extern int ItemReceiptPlus20(string WareName, string WareCode, string Measure, string SecID, string FreeField, int Price, int Count, int WareType, int ItemType, int PayItemType, int AgentType);

        [DllImportAttribute("azimuth.dll", EntryPoint = "TotalReceipt", CallingConvention = CallingConvention.StdCall)]
        private static extern int TotalReceipt();

        [DllImportAttribute("azimuth.dll", EntryPoint = "TenderReceipt", CallingConvention = CallingConvention.StdCall)]
        private static extern int TenderReceipt(byte PayType, int TenderSum, string CardName);

        [DllImportAttribute("azimuth.dll", EntryPoint = "TenderReceiptPlus", CallingConvention = CallingConvention.StdCall)]
        private static extern int TenderReceiptPlus(byte PayType, int TenderSum, string CardName, string FreeField);

        //[DllImportAttribute("AzimuthNew.dll", EntryPoint = "TaxReceipt", CallingConvention = CallingConvention.StdCall)]
        //private static extern new int TaxReceipt(byte ТaxType);

        [DllImportAttribute("azimuth.dll", EntryPoint = "CloseReceipt", CallingConvention = CallingConvention.StdCall)]
        private static extern  int CloseReceipt();
        [DllImportAttribute("azimuth.dll", EntryPoint = "QRPaint", CallingConvention = CallingConvention.StdCall)]
        private static extern int QRPaint(byte Options, byte Version, string QR);

        [DllImportAttribute("azimuth.dll", EntryPoint = "PrintBarcodeFDoc", CallingConvention = CallingConvention.StdCall)]
        private static extern int PrintBarcodeFDoc(byte BType, byte BWith, byte BHight, byte HRIFont, byte HRIMode, byte BarCodeLen, string BarCode);

        [DllImportAttribute("azimuth.dll", EntryPoint = "BarcodeReceipt", CallingConvention = CallingConvention.StdCall)]
        private static extern int BarcodeReceipt(byte BarcodeType, byte HRI, byte Font, byte Height, byte Width, string Barcode);

        #endregion

        //0 - нет перекодирования из ansi в OEM; 1 - есть;
        string Cashier = "Касса1";
        //string Cashier = "Касса1|780000000001";
        //string CashierINN = null;
        //public string CashierInn
        //{
        //    set
        //    {
        //        CashierINN = value;
        //    }
        //    get
        //    {
        //        return CashierINN;
        //    }
        //}
        string TagWhat = null;
        string TagNalog = null;
        string TagShiftNo = null;
        string TagNalogSch = null;
        int ChequeNo = 0;
        string TagChckAddr = null;
        int Oem = 0;
        byte isPrezenter = 0;
        int FNLastZNumber = 0;
        int RLastZNumber = 0;


        byte[] StatusB = new byte[6];
        byte[] GetStatusB = { 0x10, 0x04, 0x20 };
        //что искать в ответном сообщении от фискального регистратора
        //string BB = "            ";
        string[] info = new string[5];
        int myBit0, myBit1, myBit2, myBit3, myBit4, myBit5, myBit6, myBit7;

        new string op_name = "";
        string psw = "";
        string comNum = ";";
        DateTime StatusDT, StatusDT1;
        //ResultT res = new ResultT();
        byte fs = 0x1c;
        int rd = 0;
        DateTime dt;
        SerialPort _serialPort;
        byte stx = 0x02;
        byte etx = 0x03;
        byte nak = 0x15;
        byte[] Nak = new byte[] { 0x15 };
        byte esc = 0x1B;
        byte[] ba;
        byte[] rb = new byte[2048];
        string KKMRes = "";
        string[] KKMResa;
        byte cn = 0x21;

        struct FixField { public int Line; public int Col; public byte Font; }
        struct FreeField { public int Line; public int Col; public byte Font; public byte PrintMode; public byte JourNo; public string Info; }
        struct FreeDocT
        {
            public bool IsOpen;
            public byte DocType;
            public byte PayType;
            public byte FlipFOffs;
            public byte PageNum;
            public byte HCopyNum;
            public byte VCopyNum;
            public int LOffs;
            public int VGap;
            public int LGap;
            public int Sum;
            public FixField SerNo;
            public FixField DocNo;
            public FixField Date;
            public FixField Time;
            public FixField Inn;
            public FixField Oper;
            public FixField fSum;
            public List<FreeField> FreeField;
        }
        FreeDocT FreeDoc;
        byte isRetrak;
        byte isSet;
        List<byte> AddBCC(List<byte> cmd)
        {
            short BCC = 0;
            for (int i = 0; i < cmd.Count; i++)
            {
                BCC = (short)(BCC + cmd[i]);
            }
            string bccs = BCC.ToString("X4");
            cmd.Add((byte)bccs[2]);
            cmd.Add((byte)bccs[3]);
            cmd.Add((byte)bccs[0]);
            cmd.Add((byte)bccs[1]);
            return cmd;
        }
        List<byte> CreateCmd()
        {
            List<byte> cmd = new List<byte>();
            string s;
            cmd.Add(stx);
            s = psw;
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(cn++);
            if (cn < 0x21) cn = 0x21;
            return cmd;
        }
        void StartSeans()
        {
            List<byte> cmd = CreateCmd();
            string s;
            dt = DateTime.Now;

            s = "01";
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = dt.ToString("ddMMyy");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = dt.ToString("HHmm");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            cmd = AddEnd(cmd);
            ba = cmd.ToArray();
        }

        int StartReceiptC(byte DocType, byte Copies, byte Taxation, string TableNo, string PlaceNo, string AccountNo)
        {
            List<byte> cmd = CreateCmd();
            string s;
            s = "10";
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            string s0 = DateTime.Now.ToString("ddMMyy HHmm");
            s = s0.Substring(0, 6);
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = s0.Substring(7, 4);
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = DocType.ToString("X2");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(TableNo))
                cmd.Add((byte)c);
            cmd.Add(fs);
            cmd.Add(fs);
            foreach (char c in Encode(PlaceNo))
                cmd.Add((byte)c);
            cmd.Add(fs);

            s = Copies.ToString("X2");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = Taxation.ToString("X2");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(AccountNo))
                cmd.Add((byte)c);
            cmd.Add(fs);
            //Коментарии
            cmd.Add(fs);
            cmd = AddEnd(cmd);
            ba = cmd.ToArray();

            ToLog("T->", ba, ba.Length);
            SendCmd();
            ToLog("R->", rb, rd);
            int ret = int.Parse(KKMResa[3].Substring(0, 2), NumberStyles.HexNumber);
            res.resultCode = setResultCode(ret);
            res.hardStatus = SetHardStatus_Dir();
            return ret;
        }
        /*
        int ItemReceiptPlus(string WareName, string WareCode, string Measure, string SecID, string FreeField, int Price, int Count, byte WareType)
        {
            List<byte> cmd = CreateCmd();
            string s;
            s = "11";
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(WareName))
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(WareCode))
                cmd.Add((byte)c);
            cmd.Add(fs);
            NumberFormatInfo nfi = new CultureInfo("ru-RU", false).NumberFormat;
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = "";
            s = ((decimal)Price / 100).ToString("N2", nfi);
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = ((decimal)Count).ToString("N3", nfi);
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(Measure))
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = "01" + WareType.ToString("X2");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(SecID))
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(FreeField))
                cmd.Add((byte)c);
            cmd.Add(fs);
            cmd = AddEnd(cmd);
            ba = cmd.ToArray();

            ToLog("T->", ba, ba.Length);
            SendCmd();
            ToLog("R->", rb, rd);
            int ret = int.Parse(KKMResa[3].Substring(0, 2), NumberStyles.HexNumber);
            res.resultCode = setResultCode(ret);
            res.hardStatus = SetHardStatus_Dir();
            return ret;
        }
        */
        int ItemReceiptPlusC(string WareName, string WareCode, string Measure, string SecID, string FreeField, int Price, int Count, byte WareType)
        {
            List<byte> cmd = CreateCmd();
            string s;
            s = "11";
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(WareName))
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(WareCode))
                cmd.Add((byte)c);
            cmd.Add(fs);
            NumberFormatInfo nfi = new CultureInfo("ru-RU", false).NumberFormat;
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = "";
            s = ((decimal)Price / 100).ToString("N2", nfi);
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = ((decimal)Count).ToString("N3", nfi);
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(Measure))
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = "01" + WareType.ToString("X2");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(SecID))
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(FreeField))
                cmd.Add((byte)c);
            cmd.Add(fs);
            cmd = AddEnd(cmd);
            ba = cmd.ToArray();

            ToLog("T->", ba, ba.Length);
            SendCmd();
            ToLog("R->", rb, rd);
            int ret = int.Parse(KKMResa[3].Substring(0, 2), NumberStyles.HexNumber);
            res.resultCode = setResultCode(ret);
            res.hardStatus = SetHardStatus_Dir();
            return ret;
        }

        int TotalReceiptC()
        {
            List<byte> cmd = CreateCmd();
            string s;
            s = "12";
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            //Комментарий 
            cmd.Add(fs);
            cmd = AddEnd(cmd);
            ba = cmd.ToArray();

            ToLog("T->", ba, ba.Length);
            SendCmd();
            ToLog("R->", rb, rd);
            int ret = int.Parse(KKMResa[3].Substring(0, 2), NumberStyles.HexNumber);
            res.resultCode = setResultCode(ret);
            res.hardStatus = SetHardStatus_Dir();
            return ret;
        }
        int TenderReceiptC(byte PayType, int TenderSum, string CardName)
        {
            List<byte> cmd = CreateCmd();
            string s;
            s = "13";
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = PayType.ToString("X2");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            NumberFormatInfo nfi = new CultureInfo("ru-RU", false).NumberFormat;
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = "";
            s = ((decimal)TenderSum / 100).ToString("N2", nfi);
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (char c in Encode(CardName))
                cmd.Add((byte)c);
            cmd.Add(fs);
            //Комментарий
            cmd.Add(fs);
            cmd = AddEnd(cmd);
            ba = cmd.ToArray();

            ToLog("T->", ba, ba.Length);
            SendCmd();
            ToLog("R->", rb, rd);
            int ret = int.Parse(KKMResa[3].Substring(0, 2), NumberStyles.HexNumber);
            res.resultCode = setResultCode(ret);
            res.hardStatus = SetHardStatus_Dir();
            return ret;
        }
        int TaxReceipt(byte TaxType)
        {
            List<byte> cmd = CreateCmd();
            string s;
            s = "1B";
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = TaxType.ToString("X2");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            cmd = AddEnd(cmd);
            ba = cmd.ToArray();

            ToLog("T->", ba, ba.Length);
            SendCmd();
            ToLog("R->", rb, rd);
            int ret = int.Parse(KKMResa[3].Substring(0, 2), NumberStyles.HexNumber);
            res.resultCode = setResultCode(ret);
            res.hardStatus = SetHardStatus_Dir();
            return ret;
        }

        int FNGetStatus()
        {
            FNLastZNumber = 0;
            CloseDLL();
            _serialPort.Close();
            _serialPort.Open();

            List<byte> cmd = CreateCmd();
            string s;
            s = "29";
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            cmd = AddEnd(cmd);
            ba = cmd.ToArray();

            ToLog("T->", ba, ba.Length);
            SendCmd();
            ToLog("R->", rb, rd);
            int ret = int.Parse(KKMResa[3].Substring(0, 2), NumberStyles.HexNumber);
            res.resultCode = setResultCode(ret);
            res.hardStatus = SetHardStatus_Dir();
            byte phaze = byte.Parse(KKMResa[5], NumberStyles.HexNumber);
            byte doc_type = byte.Parse(KKMResa[6], NumberStyles.HexNumber);
            byte doc_data = byte.Parse(KKMResa[7], NumberStyles.HexNumber);
            /*if (byte.Parse(KKMResa[8], NumberStyles.HexNumber) == 0)
                shiftOpened = false;
            else
                shiftOpened = true;*/

            byte flags = byte.Parse(KKMResa[9], NumberStyles.HexNumber);
            string dt = KKMResa[10];
            string zn = KKMResa[11];
            int doc_no = int.Parse(KKMResa[12].Substring(2, 2) + KKMResa[12].Substring(0, 2), NumberStyles.HexNumber);
            string dt_end = KKMResa[13];
            string ver = KKMResa[14];
            string type = KKMResa[15];

            /*string stringD = getFldStr(4 + 3);
            string stringT = getFldStr(4 + 4);

            string format = "ddMMyy HHmm";
            string DD = stringD + " " + stringT;
            if (stringD != "000000" && stringT != "0000")
            {
                resDT = DateTime.TryParseExact(DD, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out tmpDT);
            }
            if (resDT)
                resources.LastShiftOpenTime = tmpDT;*/

            FNLastZNumber = int.Parse(KKMResa[16].Substring(2, 2) + KKMResa[16].Substring(0, 2), NumberStyles.HexNumber);
            //if ((FNLastZNumber > 0)/* && (resources.LastZNumber == 0)*/)
            resources.LastZNumber = FNLastZNumber;
            int cheque_no = int.Parse(KKMResa[17].Substring(2, 2) + KKMResa[17].Substring(0, 2), NumberStyles.HexNumber);
            resources.LastChequeNumber = cheque_no;
            res.resources = resources;

            _serialPort.Close();
            OpenDLL(op_name, psw, comNum, Oem);
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: FNGetStatus: phaze=" + phaze.ToString()
                    + " doc_type=" + doc_type.ToString() + " doc_data=" + doc_data.ToString() + " flags=" + flags.ToString() + " dt=" + dt + " zn=" + zn
                    + " doc_no=" + doc_no.ToString() + " dt_end=" + dt_end + " ver=" + ver + " type" + type + " LastZNumber=" + FNLastZNumber.ToString()
                    + " cheque_no=" + cheque_no.ToString());
            return ret;
        }
        int CloseReceiptC()
        {
            List<byte> cmd = CreateCmd();
            string s;
            s = "14";
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            cmd = AddEnd(cmd);
            ba = cmd.ToArray();

            ToLog("T->", ba, ba.Length);
            SendCmd();
            ToLog("R->", rb, rd);
            int ret = int.Parse(KKMResa[3].Substring(0, 2), NumberStyles.HexNumber);
            res.resultCode = setResultCode(ret);
            res.hardStatus = SetHardStatus_Dir();
            return ret;
        }
        List<byte> AddFreeField(List<byte> cmd, FreeField ff)
        {
            if (ff.Info != null && ff.Info != "")
            {
                string param;
                string s = "";
                param = ff.Line.ToString("X4");
                s = param.Substring(2, 2) + param.Substring(0, 2);
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                param = (ff.Col + 3).ToString("X4");
                s = param.Substring(2, 2) + param.Substring(0, 2);
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = ff.Font.ToString("X2");
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = ff.PrintMode.ToString("X2");
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = ff.JourNo.ToString("X2");
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                foreach (byte c in Encode(ff.Info))
                    cmd.Add(c);
                cmd.Add(fs);
            }
            return cmd;
        }
        int AddFreeField(int Line, int Col, byte Font, byte PrintMode, byte JourNo, string Info)
        {
            if (!FreeDoc.IsOpen) return 0xb3;
            FreeField ff;
            if (Info != null && Info != "")
            {
                ff = new FreeField();
                ff.Line = Line;
                ff.Col = Col;
                ff.Font = Font;
                ff.PrintMode = PrintMode;
                ff.JourNo = JourNo;
                ff.Info = Info;
                FreeDoc.FreeField.Add(ff);
            }
            return 0;
        }
        int OpenFiscalDoc(byte DocType, byte PayType, byte FlipFOffs, byte PageNum, byte HCopyNum, byte VCopyNum, int LOffs, int VGap, int LGap, int Sum)
        {
            if (FreeDoc.IsOpen) return 0xb4;
            FreeDoc.IsOpen = true;
            FreeDoc.FreeField = new List<FreeField>();
            FreeDoc.DocType = DocType;
            FreeDoc.PayType = PayType;
            FreeDoc.FlipFOffs = FlipFOffs;
            FreeDoc.PageNum = PageNum;
            FreeDoc.HCopyNum = HCopyNum;
            FreeDoc.VCopyNum = VCopyNum;
            FreeDoc.LOffs = LOffs;
            FreeDoc.VGap = VGap;
            FreeDoc.LGap = LGap;
            FreeDoc.Sum = Sum;
            return 0;
        }
        List<byte> AddPosField(List<byte> cmd, FixField ff)
        {
            string param;
            string s = "";
            param = ff.Line.ToString("X4");
            s = param.Substring(2, 2) + param.Substring(0, 2);
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            param = (ff.Col + 3).ToString("X4");
            s = param.Substring(2, 2) + param.Substring(0, 2);
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = ff.Font.ToString("X2");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            return cmd;
        }


        int AddPosField(int SerNoLine, int SerNoCol, byte SerNoFont, int DocNoLine, int DocNoCol,
           byte DocNoFont, int DateLine, int DateCol, byte DateFont, int TimeLine, int TimeCol, byte TimeFont,
           int InnLine, int InnCol, byte InnFont,
           int OperLine, int OperCol, byte OperFont, int SumLine,
           int SumCol, byte SumFont)
        {
            if (!FreeDoc.IsOpen) return 0xb3;
            FreeDoc.SerNo.Line = SerNoLine;
            FreeDoc.SerNo.Col = SerNoCol;
            FreeDoc.SerNo.Font = SerNoFont;
            FreeDoc.DocNo.Line = DocNoLine;
            FreeDoc.DocNo.Col = DocNoCol;
            FreeDoc.DocNo.Font = DocNoFont;
            FreeDoc.Date.Line = DateLine;
            FreeDoc.Date.Col = DateCol;
            FreeDoc.Date.Font = DateFont;
            FreeDoc.Time.Line = TimeLine;
            FreeDoc.Time.Col = TimeCol;
            FreeDoc.Time.Font = TimeFont;
            FreeDoc.Inn.Line = InnLine;
            FreeDoc.Inn.Col = InnCol;
            FreeDoc.Inn.Font = InnFont;
            FreeDoc.Oper.Line = OperLine;
            FreeDoc.Oper.Col = OperCol;
            FreeDoc.Oper.Font = OperFont;
            FreeDoc.fSum.Line = SumLine;
            FreeDoc.fSum.Col = SumCol;
            FreeDoc.fSum.Font = SumFont;
            return 0;
        }
        void FormFiscalReceipt()
        {
            List<byte> cmd = CreateCmd();
            string s;
            s = "73";
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = FreeDoc.DocType.ToString("X2");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = FreeDoc.PayType.ToString("X2");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = "00"; //Шрифт
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = "01"; //	Количество листов
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = "01"; //Количество копий на документе по горизонтали
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = "01"; //Количество копий на документе по вертикали
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = "2000"; //Смещение слева второй копии по горизонтали
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = "0300"; //Смещение между копиями по вертикали
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = "18"; //Смещение между строками
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            cmd = AddPosField(cmd, FreeDoc.SerNo);
            cmd = AddPosField(cmd, FreeDoc.DocNo);
            cmd = AddPosField(cmd, FreeDoc.Date);
            cmd = AddPosField(cmd, FreeDoc.Time);
            cmd = AddPosField(cmd, FreeDoc.Inn);
            cmd = AddPosField(cmd, FreeDoc.Oper);
            foreach (char c in op_name)
                cmd.Add((byte)c);
            cmd.Add(fs);
            cmd = AddPosField(cmd, FreeDoc.fSum);
            NumberFormatInfo nfi = new CultureInfo("ru-RU", false).NumberFormat;
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = "";

            s = ((decimal)FreeDoc.Sum / 100).ToString("N2", nfi);
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = FreeDoc.FreeField.Count.ToString("X2");
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            foreach (FreeField ff in FreeDoc.FreeField)
                cmd = AddFreeField(cmd, ff);
            cmd = AddEnd(cmd);
            ba = cmd.ToArray();
        }
        int PrintFiscalReceipt() //не будем менять пока что!!!!
        {
            if (!FreeDoc.IsOpen) return 0xb3;
            StartSeans();
            SendCmd();
            FormFiscalReceipt();
            ToLog("T->", ba, ba.Length);
            SendCmd();
            ToLog("R->", rb, rd);
            string str = System.Text.Encoding.Default.GetString(rb);
            int pos1 = str.IndexOf("t=");
            if (pos1 > -1)
            {
                int pos2 = str.LastIndexOf("=");
                string Qr = str.Substring(pos1, pos2 - pos1 + 2);
                LastQR = Qr;
                Debug.Print(Qr);
                int pos3 = str.IndexOf("i=");
                int pos4 = str.LastIndexOf("&fp");
                string LBN = str.Substring(pos3 + 2, pos4 - pos3 - 2);
                LastBillNumber = LBN;
            }
            else
            {
                LastQR = "";
            }

            res.resultCode = setResultCode(int.Parse(KKMResa[3].Substring(0, 2), NumberStyles.HexNumber));
            CloseFreeDoc();
            res.hardStatus = SetHardStatus_Dir();

            return int.Parse(KKMResa[3].Substring(0, 2));
        }
        void SendCmd()
        {
            _serialPort.ReadTimeout = 40000;// 8000;
            _serialPort.DiscardInBuffer();
            _serialPort.DiscardOutBuffer();
            bool flag = false;
            try
            {
                _serialPort.Write(ba, 0, ba.Length);
                if (log != null)
                {
                    string baS = " ";
                    for (int y = 0; y < ba.Length; y++)
                    {
                        char sss = ' ';
                        try
                        {
                            sss = Convert.ToChar(ba[y]);
                        }
                        catch { }
                        baS += ba[y].ToString() + "(" + sss + "), ";
                    }
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: SendCmd:" + baS);
                }

                Thread.Sleep(50);
                do
                {
                    rd = 0;
                    do
                    {
                        rb[rd++] = (byte)_serialPort.ReadByte();
                        if (log != null)
                        {
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: rb[" + (rd - 1).ToString() + "]=" + rb[rd - 1].ToString());
                        }
                    } while (rb[rd - 1] != etx);
                    rb[rd++] = (byte)_serialPort.ReadByte();
                    rb[rd++] = (byte)_serialPort.ReadByte();
                    rb[rd++] = (byte)_serialPort.ReadByte();
                    rb[rd++] = (byte)_serialPort.ReadByte();
                    if (!CheckBCC()) { _serialPort.Write(Nak, 0, Nak.Length); }
                    else { flag = true; }
                } while (!flag);
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": SendCmd: Exception " + exc.ToString());
            }


            KKMRes = System.Text.Encoding.Default.GetString(rb, 0, rd);
            KKMResa = KKMRes.Split(new char[] { (char)fs });
            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: SendCmd: KKMRes= " + KKMRes);
        }
        bool CheckBCC()
        {
            string sbcc = "";
            sbcc = sbcc + (char)rb[rd - 2] + (char)rb[rd - 1] + (char)rb[rd - 4] + (char)rb[rd - 3];
            short bcc = short.Parse(sbcc, NumberStyles.HexNumber);
            short BCC = 0;
            for (int i = 0; i < rd - 4; i++)
            {
                BCC = (short)(BCC + rb[i]);
            }

            return BCC == bcc;
        }

        List<byte> AddEnd(List<byte> cmd)
        {
            cmd.Add(etx);
            cmd = AddBCC(cmd);
            return cmd;
        }
        int CloseFreeDoc()
        {
            if (!FreeDoc.IsOpen) return 0xb3;
            FreeDoc.FreeField.Clear();
            FreeDoc.FreeField = null;
            FreeDoc.DocNo.Line = 0;
            FreeDoc.Date.Line = 0;
            FreeDoc.FlipFOffs = 0;
            FreeDoc.fSum.Line = 0;
            FreeDoc.HCopyNum = 0;
            FreeDoc.Inn.Line = 0;
            FreeDoc.LGap = 0;
            FreeDoc.LOffs = 0;
            FreeDoc.Oper.Line = 0;
            FreeDoc.PageNum = 0;
            FreeDoc.PayType = 0;
            FreeDoc.SerNo.Line = 0;
            FreeDoc.Sum = 0;
            FreeDoc.Time.Line = 0;
            FreeDoc.VCopyNum = 0;
            FreeDoc.VGap = 0;
            FreeDoc.IsOpen = false;
            return 0;
        }

        public PrimFAKKM(Log Log/*, string TagWhat, string TagNalog, string TagShiftNo, string TagNalogSch, string TagChequeNo, string TagChckAddr*/)
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = SerialPort.InfiniteTimeout;
            _serialPort.ReadTimeout = 40000;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;

            log = Log;
            hardStatus = new HardStatusT();
            hardStatus.RawStatus = new byte[5];
            myBit0 = BitVector32.CreateMask();
            myBit1 = BitVector32.CreateMask(myBit0);
            myBit2 = BitVector32.CreateMask(myBit1);
            myBit3 = BitVector32.CreateMask(myBit2);
            myBit4 = BitVector32.CreateMask(myBit3);
            myBit5 = BitVector32.CreateMask(myBit4);
            myBit6 = BitVector32.CreateMask(myBit5);
            myBit7 = BitVector32.CreateMask(myBit6);
            KKMObj = this;
            /*this.TagWhat = TagWhat;
            this.TagNalog = TagNalog;
            this.TagNalogSch = TagNalogSch;
            this.TagChequeNo = TagChequeNo;
            this.TagChckAddr = TagChckAddr;*/
        }


        //из десятичной системы в двоичную
        protected string ToBinary(int l, int k)
        {
            string b = "";
            int i = 0;

            for (i = k; i >= 1; i--)
            {
                if (l - Math.Pow(2, (i - 1)) >= 0)
                {
                    b = "1" + b;
                    l = (int)(l - Math.Pow(2, (i - 1)));
                }
                else
                {
                    b = "0" + b;
                }
            }
            return b;
        }

        public override ErrorCode setResultCode(int RC)
        {
            switch (RC)
            {
                case 0:
                    return ErrorCode.Sucsess;
                case 1:
                    return ErrorCode.IncorrectMessageFormat;
                case 2:
                    return ErrorCode.IncorrectFieldFormat;
                case 3:
                    return ErrorCode.IncorrectDataFromDevice;
                case 4:
                    return ErrorCode.IncorrectBCC;
                case 5:
                    return ErrorCode.IncorrectPass;
                case 8:
                    return ErrorCode.MaxSessionTime; //Время (длительность смены) изменилось больше чем на 24 часа
                case 9:
                    return ErrorCode.MaxFieldLength;
                case 10:
                    return ErrorCode.MaxMessageLength;
                case 13:
                    return ErrorCode.IncorrectState;
                case 0x15:
                    return ErrorCode.ZReportNeeded; //Необходимо выполнить Z отчёт (закрытие смены)
                case 0x18:
                    return ErrorCode.PaperOut;//Нет бумаги (принтер не готов к печати)
                case 0x19:
                    return ErrorCode.NearPaperEnd;//Лента близка к концу
                case 0x29:
                    return ErrorCode.ShiftAlreadyOpen;
                case 0x30:
                    return ErrorCode.FiskalMemoryError;
                case 0x33:
                    return ErrorCode.FiskalMemoryFull;
                case 0x26:
                    return ErrorCode.NeedPrintKL;
                case 0xA0:
                case 0xFC61:
                    return ErrorCode.CommunicationError;
                case 0xA5:
                    return ErrorCode.PortAccessError; //Нет связи с ККМ
                case 0xF005:
                    return ErrorCode.PortAccessError; //Нет связи с ККМ
                case 0xF002:
                    return ErrorCode.PortNotExistsError; //Нет связи с ККМ
                case 0xF003:
                    return ErrorCode.PortNameError; //Нет связи с ККМ
                case 0xC2:
                    return ErrorCode.NotConnect; //DLL не подключена
                default:
                    return ErrorCode.OtherError; //Прочие ошибки
            }
        }
        //получение заголовка чека из ини файла для фискальных документов (построчное возвращение)
        protected string[] headerFromIniForFiscalDocs()
        {
            string[] Zagolovok = new string[6];
            try
            {
                IniFile clsIni_file = new IniFile("Cheque.ini");
                //clsIni_file.IniFile = "Cheque.ini";

                Zagolovok[0] = clsIni_file.GetStr("CHEQUE1", "", "HEADER").Replace("`", "");
                Zagolovok[1] = clsIni_file.GetStr("CHEQUE2", "", "HEADER").Replace("`", "");
                Zagolovok[2] = clsIni_file.GetStr("CHEQUE3", "", "HEADER").Replace("`", "");
                Zagolovok[3] = clsIni_file.GetStr("CHEQUE4", "", "HEADER").Replace("`", "");
                Zagolovok[4] = clsIni_file.GetStr("CHEQUE5", "", "HEADER").Replace("`", "");
                Zagolovok[5] = clsIni_file.GetStr("CHEQUE6", "", "HEADER").Replace("`", "");

                return Zagolovok;
            }
            catch (Exception exc)
            {

                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: ZagolovokFromIniForFiscalDocs: " + exc.ToString());
                return Zagolovok;
            }
        }
        //получение концовки чека из ини файла для фискальных документов (построчное возвращение)
        protected string[] footerFromIniForFiscalDocs()
        {
            string[] Zagolovok = new string[6];
            try
            {
                IniFile clsIni_file = new IniFile("Cheque.ini");
                //clsIni_file.IniFile = "Cheque.ini";

                Zagolovok[0] = clsIni_file.GetStr("CHEQUE1", "", "FOOTER").Replace("`", "");
                Zagolovok[1] = clsIni_file.GetStr("CHEQUE2", "", "FOOTER").Replace("`", "");
                Zagolovok[2] = clsIni_file.GetStr("CHEQUE3", "", "FOOTER").Replace("`", "");
                Zagolovok[3] = clsIni_file.GetStr("CHEQUE4", "", "FOOTER").Replace("`", "");
                Zagolovok[4] = clsIni_file.GetStr("CHEQUE5", "", "FOOTER").Replace("`", "");
                Zagolovok[5] = clsIni_file.GetStr("CHEQUE6", "", "FOOTER").Replace("`", "");

                return Zagolovok;
            }
            catch (Exception exc)
            {

                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: ZagolovokFromIniForFiscalDocs: " + exc.ToString());

                return Zagolovok;
            }
        }

        private string getDllVer()
        {
            /*if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getDllVer");*/
            lock (locker)
            {
                IntPtr pBuff = IntPtr.Zero;
                pBuff = Marshal.AllocHGlobal(100);
                /*unsafe
                {
                    byte* src = (byte*)pBuff.ToPointer();

                    for (int i = 0; i < 100; i++)
                    {
                        *src++ = 0;
                    }
                }*/
                IntPtr p = GetDllVer(pBuff);

                string str = Marshal.PtrToStringAnsi(p);

                Marshal.FreeHGlobal(pBuff);

                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getDllVer: " + str);
                return str;
            }
        }
        protected override string getFWVer()
        {
            /*if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getFWVer");*/
            lock (locker)
            {

                IntPtr pBuff = IntPtr.Zero;

                pBuff = Marshal.AllocHGlobal(100);
                /*unsafe
                {
                    byte* src = (byte*)pBuff.ToPointer();

                    for (int i = 0; i < 100; i++)
                    {
                        *src++ = 0;
                    }
                }*/

                IntPtr p = GetFWVer(pBuff);

                string str = Marshal.PtrToStringAnsi(p);

                Marshal.FreeHGlobal(pBuff);

                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getFWVer: " + str);
                return str;
            }
        }

        private string getFldStr(byte num)
        {
            lock (locker)
            {
                IntPtr pBuff = IntPtr.Zero;

                pBuff = Marshal.AllocHGlobal(100);

                IntPtr p = GetFldStr(num, pBuff);

                string str = Marshal.PtrToStringAnsi(p);

                Marshal.FreeHGlobal(pBuff);

                return str;
            }
        }

        //возвращает серийный номер фискального регистратора
        public virtual string fiskRegSerialfiskRegSerial()
        {
            /*if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: fiskRegSerialfiskRegSerial");*/
            lock (locker)
            {
                string FiskRegSerialRes = "";

                resultCode = setResultCode(GetSerialNum());
                if (resultCode == ErrorCode.Sucsess)
                {

                    for (byte i = 1; i < 6; i++)
                    {

                        info[i - 1] = getFldStr(i);

                        if (info[0] == "")
                        {
                            resultCode = ErrorCode.NotConnect; //Нет связи с ККМ
                            ResultDescription = "не установлено соединение с принтером";
                            //дописать и запустить

                            return "не установлено соединение с принтером";
                        }
                    }
                    FiskRegSerialRes = info[4];
                }


                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: fiskRegSerialfiskRegSerial: " + FiskRegSerialRes.Trim());
                return FiskRegSerialRes.Trim();
            }
        }

        //возвращает номер документа
        protected int fiskRegNumDoc()
        {
            resultCode = setResultCode(GetNumbers());
            if (resultCode == ErrorCode.Sucsess)
            {
                int NumDoc = GetFldWord(5);
                return NumDoc;
            }
            else
                return 0;
        }

        //Получаем сумму в фискальном регистраторе
        /*protected override int get_Summa_FR()
        {
            try
            {
                resultCode = setResultCode(GetEReport(52, 0));
                if (resultCode == ErrorCode.Sucsess)
                {
                    int sum_kassa = GetFldInt(23);
                    return sum_kassa;
                }
                return 0;
            }
            catch (Exception err)
            {
                resultDescription = "Error Get_Summa_FR(): " + err.Message;
                resultCode = ErrorCode.NotConnect; //Нет связи с ККМ
                return 0;
            }
        }*/

        //возвращает Дату и Время первого открытого документа в смене
        protected override ResultT getResource()
        {

            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getResource");
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getResource: KKM busy ");
                    res.resultCode = ErrorCode.NotConnect;
                    return res;
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                lock (locker)
                {
                    DateTime tmpDT = new DateTime();
                    bool resDT = false;
                    if (res.resources == null)
                    {
                        resources = new ResourcesT();
                    }
                    else
                    {
                        resources = (ResourcesT)res.resources;
                    }
                    res.resultCode = setResultCode(GetResource());
                    if (res.resultCode != ErrorCode.CommunicationError && res.resultCode != ErrorCode.IncorrectBCC
                        && res.resultCode != ErrorCode.NotConnect && res.resultCode != ErrorCode.PortAccessError
                        )
                    //if (res.resultCode == ErrorCode.Sucsess)
                    {
                        resources.RegCount = GetFldByte(4 + 1);
                        //resources.ZCount = GetFldByte(4 + 2);

                        //RLastZNumber = GetFldByte(4 + 3);
                        RLastZNumber = GetFldInt(4 + 3);
                        //if ((RLastZNumber > 0) && (resources.LastZNumber == 0)) resources.LastZNumber = RLastZNumber;

                        string stringD = getFldStr(4 + 4);
                        string stringT = getFldStr(4 + 5);

                        string format = "ddMMyy HHmm";
                        string DD = stringD + " " + stringT;
                        if (!(stringD == "000000" && stringT == "0000"))
                        {
                            resDT = DateTime.TryParseExact(DD, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out tmpDT);
                        }
                        if (resDT)
                            resources.LastShiftOpenTime = tmpDT;
                        res.resources = resources;
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff")
                                + ": PrimFAKKM: getResource: resultCode=" + res.resultCode.ToString() + " RegCount=" + resources.RegCount.ToString()
                                + " RLastZNumber=" + RLastZNumber.ToString() + " stringD=" + stringD + " stringT=" + stringT);
                    }
                    else
                    {

                        resources.LastShiftOpenTime = null;
                        res.resources = resources;
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff")
                                + ": PrimFAKKM: getResource: resultCode=" + res.resultCode.ToString());
                    }
                    busy = false;
                    return res;
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getResource: Exception " + exc.ToString());
                res.resultCode = ErrorCode.NotConnect;
                busy = false;
                return res;
            }
        }
        //метод подключения к фискальному регистратору
        protected override ResultT fR_Connect(string Op_name, string Psw, string RPsw, string ComNum)
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: fR_Connect");

            lock (locker)
            {
                try
                {
                    Oem = 0;

                    CloseDLL();
                    _serialPort.Close();
                    op_name = Op_name;
                    //if (versionKKM == 1)
                    //    op_name = op_name + "|780000000001";
                    //Cashier = op_name;
                    Cashier = Op_name;
                    if (CashierINN != null && CashierINN != "")
                    {
                        Cashier = Cashier + "|" + CashierINN;

                    }

                    psw = Psw;
                    comNum = ComNum;
                    _serialPort.PortName = comNum;
                    res.resultCode = setResultCode(OpenDLL(op_name, psw, comNum, Oem));

                    StatusDT = DateTime.Now;
                    StatusDT1 = DateTime.Now;
                    if ((res.resultCode == ErrorCode.Sucsess) /*|| (res.resultCode == ErrorCode.NearPaperEnd)*/)
                    {
                        res.boolResult = true;
                        res.deviceEnabled = true;
                        deviceEnabled = true;
                    }
                    else
                    {
                        res.boolResult = false;
                        deviceEnabled = false;
                        res.deviceEnabled = false;
                    }
                }
                catch (Exception exc)
                {

                    res.resultCode = ErrorCode.NotConnect; //Нет связи с ККМ
                    res.resultDescription = "не установлено соединение с принтером" + exc.ToString();
                    res.deviceEnabled = false;
                    deviceEnabled = false;
                    res.boolResult = false;

                    //if (log != null)
                    //log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: fR_Connect: " + PrintResultT(res));
                }
                return res;
            }
        }
        protected override ResultT Init()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: Init: ");

            //res.FW = getFWVer();
            //Param1
            //0x02E7 - 0000 0010 1110 0111
            //0 - не печатать окончание документа
            //1 - не печатать пустую строку между продажами
            //2 - не печатать нулевые счетчики в отчете о закрытии смены и промежуточном отчете
            //5 - не печатать чек ресурсы
            //6 - не печатать поле количество в командах приход, если оно равно 1
            //7 - не требуется команда начало сеанса
            //8+9 = 10 - для открытия смены необходима команда открыть смену 
            //Param2 - 100 0000 0000 00000
            //0020 не резать чековую ленту
            //15 - 8000 накопление чека
            try
            {
                CloseDLL();
                Thread.Sleep(100);
                _serialPort.Open();
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
                _serialPort.Close();
                //Thread.Sleep(200);
                OpenDLL(op_name, psw, comNum, Oem);

                res.resultCode = setResultCode(CheckHealth());
                if ((res.resultCode == ErrorCode.Sucsess) || (res.resultCode == ErrorCode.NearPaperEnd))
                {
                    Thread.Sleep(100);
                    res.resultCode = setResultCode(SetParamDoc(0x02E7, 0x8020));
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: Init: SetParamDoc: resultCode=" + res.resultCode.ToString());

                    res = getStatus();
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: Init: getStatus: res=" + res.ToString());

                    if ((res.resultCode == ErrorCode.Sucsess) || (res.resultCode == ErrorCode.NearPaperEnd))
                    {
                        res.boolResult = true;
                        if (CheckStatusNum(0, 7) == 1)
                            CloseFDocC();

                        res = getResource();
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: Init: getResource: res=" + res.ToString());

                    }
                    return res;

                }
                else
                {
                    res.boolResult = false;

                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: Init: CheckHealth: res=" + res.ToString());
                    return res;
                }
            }
            catch (Exception exc)
            {
                res.boolResult = false;

                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: Init Exception: " + exc.ToString());
                return res;
            }
        }
        protected override ResultT fR_Disconnect()
        {
            lock (locker)
            {
                try
                {
                    CloseDLL();
                    _serialPort.Close();
                    res.deviceEnabled = false;
                    deviceEnabled = false;
                    res.boolResult = true;

                    return res;
                }
                catch (Exception exc)
                {
                    res.resultCode = ErrorCode.NotConnect;
                    res.resultDescription = "не установлено соединение с принтером" + exc.ToString();

                    return res;
                }
            }
        }

        //проверка включен ли принтер
        ResultT OldRes;
        protected override ResultT getStatus()
        {
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getStatus: KKM busy ");
                    res.resultCode = ErrorCode.NotConnect;
                    return res;
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                lock (locker)
                {
                    {
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getStatus: locker 1 ");
                        bool printerPortOpen = false;
                        try
                        {
                            res.resultCode = setResultCode(CheckHealth());
                            StatusDT = DateTime.Now;
                            if (res.resultCode == ErrorCode.Sucsess)
                            {
                                res.resultCode = setResultCode(GetSerialNum());
                                if (res.resultCode == ErrorCode.Sucsess)
                                    res.hardStatus = SetHardStatus();
                            }
                            if (res.resultCode == ErrorCode.Sucsess)
                                res.boolResult = true;
                            else
                                res.boolResult = false;

                            if (!OldRes.Equals(res))
                            {
                                OldRes = res;
                            }
                        }
                        catch (Exception exc1)
                        {
                            res.resultCode = ErrorCode.NotConnect;
                            res.resultDescription = "не установлено соединение с принтером" + ": " + exc1.ToString();
                            printerPortOpen = false;
                            res.boolResult = printerPortOpen;
                            StatusDT = DateTime.Now;
                            StatusDT1 = DateTime.Now;
                            if (log != null)
                                log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getStatus: Exception 1 = " + exc1.ToString());
                        }
                    }
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getStatus: locker 2 ");

                    busy = false;
                    return res;
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: getStatus: Exception = " + exc.ToString());
                res.resultCode = ErrorCode.NotConnect;
                busy = false;
                return res;
            }
        }
        //перевод из шестнадцатиричной системы счисления в десятичную (для перевода номера последней закрытой смены)
        protected /*override */ string convertFromSixteenToTen(int k, int LastSmenaNumber)
        {
            string CurrentFullNum = "";
            string CurrentNum = "";
            string masd = "";
            int MassMax = 0;

            CurrentFullNum = "";
            CurrentNum = "";
            masd = LastSmenaNumber.ToString();

            switch (k)
            {
                case 1:   //для перевода номера последней закрытой смены 
                    MassMax = masd.Length;
                    break;
                case 0:
                    MassMax = 2;
                    break;
            }
            for (int j = 0; j < MassMax; j++)
            {
                char CurrentNumm = masd[j];
                // char ch1 = '0';
                switch (CurrentNumm)
                {
                    case '0':
                        CurrentNum = "0";
                        break;
                    case '1':
                        CurrentNum = "1";
                        break;
                    case '2':
                        CurrentNum = "2";
                        break;
                    case '3':
                        CurrentNum = "3";
                        break;
                    case '4':
                        CurrentNum = "4";
                        break;
                    case '5':
                        CurrentNum = "5";
                        break;
                    case '6':
                        CurrentNum = "6";
                        break;
                    case '7':
                        CurrentNum = "7";
                        break;
                    case '8':
                        CurrentNum = "8";
                        break;
                    case '9':
                        CurrentNum = "9";
                        break;
                    case 'A':
                        CurrentNum = "10";
                        break;
                    case 'B':
                        CurrentNum = "11";
                        break;
                    case 'C':
                        CurrentNum = "12";
                        break;
                    case 'D':
                        CurrentNum = "13";
                        break;
                    case 'E':
                        CurrentNum = "14";
                        break;
                    case 'F':
                        CurrentNum = "15";
                        break;

                    default:
                        break;
                }
                CurrentFullNum += CurrentNum;
            }

            return CurrentFullNum;
        }
        //Открываем смену и пишем в базу данных
        protected override ResultT shift_Open()
        {
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: shift_Open: KKM busy ");
                    res.resultCode = ErrorCode.NotConnect;
                    return res;
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                lock (locker)
                {
                    try
                    {
                        //Установка параметров по умолчанию для ФР
                        //Param1 - 11110111
                        //0 - не печатать окончание документа
                        //1 - не печатать пустую строку между продажами
                        //2 - не печатать нулевые счетчики в отчете о закрытии смены и промежуточном отчете
                        //5 - не печатать чек ресурсы
                        //6 - не печатать поле количество в командах приход, если оно равно 1
                        //7 - не требуется команда начало сеанса
                        //8+9 = 01 - фискальный документ открывает смену

                        //                    int res_fr_def = SetParamDoc(0xF7, 0); //0011110111, 00000000‬

                        res.resultCode = setResultCode(ShiftOpenPlus(Cashier, "", "", "открытие смены"));
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": shift_Open: resultCode="
                            + Environment.NewLine + res.ToString()
                            );

                        if ((res.resultCode == ErrorCode.Sucsess) || (res.resultCode == ErrorCode.ShiftAlreadyOpen)
                                || (res.resultCode == ErrorCode.NearPaperEnd))
                        {
                            DateTime openSHIFTDT = DateTime.Now;
                            res.boolResult = true;
                            ChequeNo = 1;
                            busy = false;
                            return res;
                        }
                        else
                        {
                            res.resultDescription = "ошибка открытия смены" + res.resultCode.ToString() + " " + FR_ERROR_TO_STRING(res.resultCode);
                            ResultDescription = res.resultDescription;

                            res.boolResult = false;
                            busy = false;
                            return res;
                        }
                    }
                    catch (Exception err)
                    {
                        res.resultDescription = "ошибка открытия смены" + err.Message;
                        res.boolResult = false;
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": shift_Open: resultCode="
                            + Environment.NewLine + res.ToString()
                            + Environment.NewLine + "Exception: " + err.ToString()
                            );
                        busy = false;
                        return res;
                    }
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: shift_Open: Exception " + exc.ToString());
                res.resultCode = ErrorCode.NotConnect;
                busy = false;
                return res;
            }
        }
        public int[] EReport = new int[32];
        public bool EReport_()
        {
            try//EReportS/
            {
                //res.resultCode = setResultCode(GetEReport(0x37, 0));
                res.resultCode = setResultCode(GetEReport(0x34, 0));
                if (res.resultCode == ErrorCode.Sucsess)
                {
                    string EReportS = "";
                    for (int i = 5; i < 32; i++)
                    {
                        try
                        {
                            EReport[i] = GetFldInt((byte)i);
                            EReportS += " поле " + i.ToString() + "=" + EReport[i].ToString() + ", ";
                            if (i == 5 && versionKKM == 1)
                                cashAccepted = EReport[i];
                            if (i == 5 && versionKKM == 0)
                                cashAccepted = EReport[i];
                            if (i == 9 && versionKKM == 1)
                                creditCardAccepted = EReport[i];
                            if (i == 13 && versionKKM == 0)
                                creditCardAccepted = EReport[i];
                            if (i == 31 && versionKKM == 1)
                                sum_kassa = EReport[i];
                            if (i == 23 && versionKKM == 0)
                                sum_kassa = EReport[i];

                        }
                        catch { break; }
                    }

                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GetEReport: "
                            + Environment.NewLine + EReportS);
                    return true;
                }
                else
                {
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GetEReport: resultCode="
                        + Environment.NewLine + res.ToString()
                        );
                    return false;
                }

            }
            catch (Exception err)
            {
                res.resultDescription = "ошибка закрытия смены " + err.Message;
                res.boolResult = false;
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GetEReport: resultCode="
                    + Environment.NewLine + res.ToString()
                    + Environment.NewLine + "Exception: " + err.ToString()
                    );
                return false;
            } //EReportS

        }
        //Закрываем смену и пишем в базу данных
        protected override ResultT shift_Close()
        {
            lock (locker)
            {
                if (!EReport_())
                {
                    //res.boolResult = false;
                    //busy = false;
                    //return res;
                }

                try
                {
                    res.resultCode = setResultCode(ShiftClosePlus(Cashier, "", "", 0));
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": shift_Close: resultCode="
                        + Environment.NewLine + res.ToString()
                        );
                    if (res.resultCode == ErrorCode.Sucsess)
                    {
                        DateTime openSHIFTDT = DateTime.Now;

                        res.boolResult = true;

                        return res;
                    }
                    else
                    {
                        res.boolResult = false;
                        res.resultDescription = "ошибка закрытия смены " + res.resultCode.ToString() + " " + FR_ERROR_TO_STRING(res.resultCode);
                        ResultDescription = res.resultDescription;

                        return res;
                    }
                }
                catch (Exception err)
                {
                    res.resultDescription = "ошибка закрытия смены " + err.Message;
                    res.boolResult = false;
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": shift_Close: resultCode="
                        + Environment.NewLine + res.ToString()
                        + Environment.NewLine + "Exception: " + err.ToString()
                        );
                    return res;
                }
            }
        }
        //Закрываем смену без чека и пишем в базу данных
        protected override ResultT shift_Close_Ex()
        {
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: shift_Close_Ex: KKM busy ");
                    res.resultCode = ErrorCode.NotConnect;
                    return res;
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                lock (locker)
                {
                    if (!EReport_())
                    {
                        //res.boolResult = false;
                        //busy = false;
                        //return res;
                    }

                    try
                    {
                        res.resultCode = setResultCode(ShiftClosePlus(Cashier, "", "", 1));
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": shift_Close_Ex: resultCode="
                            + Environment.NewLine + res.ToString()
                            );
                        if (res.resultCode == 0)
                        {
                            res.boolResult = true;
                            busy = false;
                            return res;
                        }
                        else
                        {
                            res.boolResult = false;
                            res.resultDescription = "ошибка закрытия смены " + res.resultCode.ToString() + " " + FR_ERROR_TO_STRING(res.resultCode);
                            ResultDescription = res.resultDescription;
                            busy = false;
                            return res;
                        }
                    }
                    catch (Exception err)
                    {
                        res.resultDescription = "ошибка закрытия смены " + err.Message;
                        res.boolResult = false;
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": shift_Close_Ex: resultCode="
                            + Environment.NewLine + res.ToString()
                            + Environment.NewLine + "Exception: " + err.ToString()
                            );
                        busy = false;
                        return res;
                    }
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: shift_Close_Ex: Exception " + exc.ToString());
                res.resultCode = ErrorCode.NotConnect;
                busy = false;
                return res;
            }
        }
        protected override ResultT time24HourseEnd(DateTime OpenShiftDateTime)
        {
            try
            {
                DateTime dt = DateTime.Now;
                TimeSpan ts = (dt - OpenShiftDateTime);
                res.time24HourseEnd = ts.TotalHours;
                res.boolResult = true;
                res.resultCode = ErrorCode.Sucsess;

                return res;
            }
            catch (Exception err)
            {
                res.resultCode = ErrorCode.OtherError;
                res.resultDescription = "ошибка закрытия смены " + err.Message;
                res.boolResult = false;

                return res;
            }
        }


        private string Obrezanie(string txt, byte Len)
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: Obrezanie");
            try
            {
                string tmp_str = "";

                if (txt.Length > Len)
                {
                    tmp_str = txt.Substring(0, Len - 1);
                }
                else
                {
                    tmp_str = txt;
                }

                return tmp_str;
            }
            catch
            {
                return "";
            }
        }

        HardStatusT OldHardStatus;
        public HardStatusT SetHardStatus()
        {
            for (byte i = 1; i < 6; i++)
            {
                info[i - 1] = getFldStr(i);
                if (info[0] == "")
                {
                    break;
                }
            }

            if (info[0] == "")
                return hardStatus;
            for (byte i = 0; i < 5; i++)
                hardStatus.RawStatus[i] = byte.Parse(info[3].Substring(i * 2, 2), NumberStyles.AllowHexSpecifier);
            BitVector32 hs = new BitVector32(hardStatus.RawStatus[1]);
            hardStatus.CoverOpen = hs[myBit2];
            hardStatus.PaperTransporByHand = hs[myBit3];
            hardStatus.StopByPaperEnd = hs[myBit5];
            hardStatus.Error = hs[myBit6];
            hs = new BitVector32(hardStatus.RawStatus[2]);
            hardStatus.PrinterMechError = hs[myBit2];
            hardStatus.PrinterCutMechError = hs[myBit3];
            hardStatus.PrinterFatalError = hs[myBit5];
            hardStatus.PrinterHeadHot = hs[myBit6];
            hs = new BitVector32(hardStatus.RawStatus[3]);
            hardStatus.PaperNearEnd = hs[myBit3];
            hardStatus.PaperEnd = hs[myBit6];
            hs = new BitVector32(hardStatus.RawStatus[4]);
            //hardStatus.PaperNearEnd = hs[myBit6];   //Чек в презентере ???!!!
            //hardStatus.PaperEnd1 = hs[myBit5];
            res.hardStatus = hardStatus;
            return hardStatus;
        }
        string getFldStr_Dir(byte num)
        {
            return KKMResa[num];
        }

        HardStatusT SetHardStatus_Dir()
        {
            for (byte i = 1; i < 6; i++)
            {
                info[i - 1] = getFldStr_Dir(i);
                if (info[0] == "")
                {
                    break;
                }
            }

            if (info[0] == "")
                return hardStatus;
            for (byte i = 0; i < 5; i++)
                hardStatus.RawStatus[i] = byte.Parse(info[3].Substring(i * 2, 2), NumberStyles.AllowHexSpecifier);
            BitVector32 hs = new BitVector32(hardStatus.RawStatus[1]);
            hardStatus.CoverOpen = hs[myBit2];
            hardStatus.PaperTransporByHand = hs[myBit3];
            hardStatus.StopByPaperEnd = hs[myBit5];
            hardStatus.Error = hs[myBit6];
            hs = new BitVector32(hardStatus.RawStatus[2]);
            hardStatus.PrinterMechError = hs[myBit2];
            hardStatus.PrinterCutMechError = hs[myBit3];
            hardStatus.PrinterFatalError = hs[myBit5];
            hardStatus.PrinterHeadHot = hs[myBit6];
            hs = new BitVector32(hardStatus.RawStatus[3]);
            hardStatus.PaperNearEnd = hs[myBit3];
            hardStatus.PaperEnd = hs[myBit6];
            hs = new BitVector32(hardStatus.RawStatus[4]);
            //hardStatus.PaperNearEnd = hs[myBit6];   //Чек в презентере ???!!!
            //hardStatus.PaperEnd1 = hs[myBit5];
            res.hardStatus = hardStatus;
            return hardStatus;
        }
        protected override ResultT shiftFromKKM()
        {
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: shiftFromKKM: KKM busy ");
                    res.resultCode = ErrorCode.NotConnect;
                    return res;
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                lock (locker)
                {
                    FNGetStatus();

                    int ShiftState = 0;
                    int ShiftTime = 0;
                    res.resultCode = setResultCode(GetShiftState(ref ShiftState, ref ShiftTime));
                    //res.hardStatus = SetHardStatus();
                    if (res.resultCode != ErrorCode.Sucsess)
                    {
                        res.boolResult = false;
                        //shiftOpened = true;
                        busy = false;
                        return res;
                    }
                    else
                    {
                        if (ShiftState == 0)
                        {
                            res.boolResult = true;// false;
                            shiftOpened = false;
                            busy = false;
                            return res;
                        }
                        else
                        if (ShiftState == 1)
                        {
                            res = shift_Close_Ex();
                            res = shift_Open();

                            /*                        res.resultCode = ErrorCode.MaxSessionTime;*/
                            res.boolResult = true;
                            shiftOpened = true;
                            busy = false;
                            return res;
                        }
                        else
                        {
                            //res.resultCode = ErrorCode.Sucsess;
                            res.boolResult = true;
                            shiftOpened = true;
                            ChequeNo = fiskRegNumDoc();
                            busy = false;
                            return res;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: shiftFromKKM: Exception " + exc.ToString());
                res.resultCode = ErrorCode.NotConnect;
                busy = false;
                return res;
            }
        }
        protected override ResultT getCurrentMode()
        {
            lock (locker)
            {
                res.currentMode = 0;
                res.resultCode = ErrorCode.Sucsess;
                return res;
            }
        }
        protected override ResultT xReport()
        {
            lock (locker)
            {
                res.resultCode = setResultCode(XReport());
                return res;
            }
        }


        //печать чека (!!!пока оставим, тк нам не нужен штрафной билет)
        protected override ResultT printCheck(AllPayParams Params)
        {
            byte PrintMode = 1;
            //01
            //на основном документе
            //02
            //на копии
            //03
            //на обоих документах
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: KKM busy ");
                    res.resultCode = ErrorCode.NotConnect;
                    return res;
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: countS=" + countS.ToString());

                lock (locker)
                {
                    //Сумма на которую печатается чек Продажи
                    int SummaChek_BP = (int)(Params.Pay_Query * 100);
                    int PaySum = (int)(Params.Pay_Summa * 100);
                    int ad_lines = Params.AddStrings.Count;
                    byte PayType = 0;
                    byte docType = 0;

                    //string WareName = Params.What;
                    //string FreeField = " ---------------------------- |" + "Номер карты " + Params.CardId + "|";

                    /*
                    string WareName = Params.What;
                    string FreeField = " ---------------------------- |" + "Номер карты " + Params.CardId + "|";
                    for (int i = 0; i < ad_lines; i++)
                    {
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Params.AddStrings[i]);
                        FreeField += Params.AddStrings[i];
                    }

                    FreeField += "|"
                        + "|" + string.Format(Properties.Resources.EnterTime, Params.EntryTime.ToString("HH:mm  dd-MM-yyyy"))
                        + "|" + string.Format(Properties.Resources.PaymentTime, DateTime.Now.ToString("HH:mm  dd-MM-yyyy"))
                        + "|" + Properties.Resources.ExitBefore
                        + "|" + ((DateTime)Params.ExitTime).ToString("HH:mm dd-MM-yyyy")
                        + "| ---------------------------- |";
                        */
                    switch (Params.DocType)
                    {
                        case DocType.Sale:
                            docType = 0;
                            break;
                        case DocType.SaleStorno:
                            docType = 1;
                            break;
                        case DocType.SaleNotCheck: //это я вводил
                            docType = 0x80;
                            break;
                    }
                    if (Params.TypeClose == TypeClose.Card)
                        PayType = 1;
                    if (Params.TypeClose == TypeClose.Card2)
                        PayType = 2;
                    //if (Params.TypeClose == TypeClose.Cash)
                    //    PayType = 0;
                    string vatS = "";
                    int vatI = 0;
                    switch (Params.VAT)
                    {
                        case 0: // Без НДС
                            vatS = "00";
                            vatI = 0;
                            break;
                        case 1: // НДС 0%
                            vatS = "01";
                            vatI = 1;
                            break;
                        case 2: // НДС 10%
                            vatS = "04";
                            vatI = 4;
                            break;
                        //case 3: // НДС 18%
                        default:
                            vatS = "05";
                            vatI = 5;
                            break;
                    }
                    if (versionKKM == 1)
                    {
                        Params.What += "|" + Params.Pay_Query.ToString() + ".00|1|01" + vatS + tag1214 + "04|";

                    }
                    if (false)
                    {
                        string FreeFields = string.Format(Properties.Resource.CardId, Params.CardId);
                        if (ad_lines > 0)
                        {
                            for (int i = 0; i < ad_lines; i++)
                                FreeFields += "|" + Params.AddStrings[i];
                        }
                        FreeFields += "|" + string.Format(Properties.Resource.EnterTime, Params.EntryTime.ToString("HH:mm  dd-MM-yyyy"));
                        if (Params.ExitTime != null)
                        {
                            FreeFields += "|             " + Properties.Resource.ExitBefore;
                            FreeFields += "|        " + ((DateTime)Params.ExitTime).ToString("HH:mm dd-MM-yyyy");
                        }
                        string CardName = "";
                        if (PayType == 2)
                            CardName = "Банковская карта";
                        CloseDLL();
                        _serialPort.Close();
                        _serialPort.Open();
                        try
                        {
                            StartSeans();
                            SendCmd();
                            res.resultCode = setResultCode(StartReceiptC(docType, 1, 0, Cashier, "", "")); //10
                            if (res.resultCode == ErrorCode.Sucsess)
                                res.resultCode = setResultCode(ItemReceiptPlusC(Params.What, "", "", "", FreeFields, SummaChek_BP, 1, 0x05)); //11
                            if (res.resultCode == ErrorCode.Sucsess)
                                res.resultCode = setResultCode(TotalReceiptC()); //12
                                                                                 //if (res.resultCode == ErrorCode.Sucsess)
                                                                                 //    res.resultCode = setResultCode(TaxReceipt(5)); //14
                            if (res.resultCode == ErrorCode.Sucsess)
                                res.resultCode = setResultCode(TenderReceiptC(PayType, PaySum, CardName)); //13
                            if (res.resultCode == ErrorCode.Sucsess)
                                res.resultCode = setResultCode(CloseReceiptC()); //14
                            if (res.resultCode == ErrorCode.Sucsess)
                                res.boolResult = true;
                            else
                            {
                                res.boolResult = false;
                                res.resultDescription = Properties.Resource.PrintChequeError + res.resultCode.ToString() + " " + FR_ERROR_TO_STRING(res.resultCode);
                                ResultDescription = res.resultDescription;
                            }
                        }
                        catch (Exception exc)
                        {

                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: Exception " + exc.ToString());

                            res.boolResult = false;
                        }

                        _serialPort.Close();
                        OpenDLL(op_name, psw, comNum, Oem);
                    }
                    
                    else if (docType == 0x80) //Это виртуальный чек...
                    {
                    res.resultCode = setResultCode(StartReceipt(docType, 1, 0, "", "", "")); //10
                        if (res.resultCode == ErrorCode.Sucsess)
                            res.resultCode = setResultCode(ItemReceiptPlus20("Оплата парковки", "", "", "", "", SummaChek_BP, 1000, vatI * 256 + 1, 04, 01, 00)); //11
                                                                                                                                                                  //res.resultCode = setResultCode(ItemReceiptPlus20("Услуги парковки", "", "00", "", "Произвольные данные|строка 2|", SummaChek_BP, 1, 0x0501, 04, 01, 00)); //11
                                                                                                                                                                  //res.resultCode = setResultCode(ItemReceiptPlus("Услуги парковки", "", "шт", "", "", SummaChek_BP, 1, (short)(vatI * 8 + 1))); //11
                        if (res.resultCode == ErrorCode.Sucsess)
                            res.resultCode = setResultCode(TotalReceipt()); //12

                        if (res.resultCode == ErrorCode.Sucsess)
                            res.resultCode = setResultCode(TenderReceiptPlus(PayType, PaySum, "", "")); //13

                        if (res.resultCode == ErrorCode.Sucsess)
                            res.resultCode = setResultCode(CloseReceipt()); //14

                        if (res.resultCode == ErrorCode.Sucsess)
                        {
                            res.boolResult = true;
                            LastQR = "";
                            CheckNum = 0;
                            LastQR = getFldStr(6);
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: LastQR=" + LastQR);

                            int pos3 = LastQR.LastIndexOf("i=");
                            int pos4 = LastQR.LastIndexOf("fp=");
                            string ch = LastQR.Substring(pos3 + 2, pos4 - pos3 - 3);
                            LastBillNumber = ch;
                            //if (Convert.ToInt32(ch) > 0)
                            //    CheckNum = Convert.ToInt32(ch);
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: CheckNum=" + CheckNum.ToString());
                        }
                        else
                        {
                            res.boolResult = false;
                            res.resultDescription = res.resultCode.ToString() + " " + FR_ERROR_TO_STRING(res.resultCode);
                            //res.resultDescription = Properties.Resources.PrintChequeError + res.resultCode.ToString() + " " + FR_ERROR_TO_STRING(res.resultCode);
                            ResultDescription = res.resultDescription;
                        }
                        //LastBillNumber нужно записать!!!
                            /*
                            //try
                            //{
                            res.resultCode = setResultCode(StartReceipt(docType, 1, 0, "", "", "")); //10
                                                                                                     //res.resultCode = setResultCode(StartReceiptPlus(docType, 1, 0, "", "", "", " ------------------------ ")); //10

                            if (res.resultCode == ErrorCode.Sucsess)
                                res.resultCode = setResultCode(ItemReceiptPlus20("Оплата парковки", "", "", "", "", SummaChek_BP, 1000, vatI * 256 + 1, 04, 01, 00)); //11
                                                                                                                                                                      //res.resultCode = setResultCode(ItemReceiptPlus20("Услуги парковки", "", "00", "", "Произвольные данные|строка 2|", SummaChek_BP, 1, 0x0501, 04, 01, 00)); //11
                                                                                                                                                                      //res.resultCode = setResultCode(ItemReceiptPlus("Услуги парковки", "", "шт", "", "", SummaChek_BP, 1, (short)(vatI * 8 + 1))); //11

                            if (res.resultCode == ErrorCode.Sucsess)
                                res.resultCode = setResultCode(TotalReceipt()); //12

                            if (res.resultCode == ErrorCode.Sucsess)
                                res.resultCode = setResultCode(TenderReceiptPlus(PayType, PaySum, "", "")); //13

                            if (res.resultCode == ErrorCode.Sucsess)
                                res.resultCode = setResultCode(CloseReceipt()); //14

                            if (res.resultCode == ErrorCode.Sucsess)
                            {
                                res.boolResult = true;
                                LastQR = "";
                                CheckNum = 0;
                                //try
                                //{
                                LastQR = getFldStr(6);
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: LastQR=" + LastQR);
                                //try
                                //{
                                {
                                    int pos3 = LastQR.LastIndexOf("i=");
                                    int pos4 = LastQR.LastIndexOf("fp=");
                                    string ch = LastQR.Substring(pos3 + 2, pos4 - pos3 - 3);
                                    if (Convert.ToInt32(ch) > 0)
                                        CheckNum = Convert.ToInt32(ch);
                                    if (log != null)
                                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: CheckNum=" + CheckNum.ToString());
                                }
                                //}
                                //catch { }

                                //catch (Exception e)
                                //{
                                //    if (log != null)
                                //        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: LastQR Exception " + e);
                                //}
                            }
                            else
                            {
                                res.boolResult = false;
                                res.resultDescription = Properties.Resources.PrintChequeError + res.resultCode.ToString() + " " + FR_ERROR_TO_STRING(res.resultCode);
                                ResultDescription = res.resultDescription;
                            }
                            //}

                            //catch { res.boolResult = false; }
                        */
                        }

                    else
                    {
                        CloseDLL();
                        //Thread.Sleep(200);
                        _serialPort.Close();
                        //Thread.Sleep(100);
                        _serialPort.Open();
                        _serialPort.DiscardInBuffer();
                        _serialPort.DiscardOutBuffer();
                        try
                        {
                            int res_Add = 2;

                            //открываем ПФД тип продажа. сумма = наложенный платеж
                            int ZagolovokHeight = 6;
                            int FooterHeight = 6;

                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: OpenFiscalDoc: SummaChek_BP " + SummaChek_BP.ToString());

                            //ьшгы
                            res_Add = OpenFiscalDoc(docType, PayType, 0, 1, 1, 1, 32, 3, 0, (SummaChek_BP));
                            res.resultCode = setResultCode(res_Add);
                            //для печати заголовка
                            string[] Zagolovok;
                            Zagolovok = headerFromIniForFiscalDocs();
                            for (int i = 5; i >= 0; i--)
                                if (Zagolovok[i].Trim().Length == 0)
                                    ZagolovokHeight--;
                            string[] Footer;
                            Footer = footerFromIniForFiscalDocs();
                            for (int i = 5; i >= 0; i--)
                                if (Footer[i].Trim().Length == 0)
                                    FooterHeight--;

                            for (int i = 0; i < ZagolovokHeight; i++)
                            {

                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Zagolovok[i]);
                                res_Add = AddFreeField(i + 1, 0, 1, PrintMode, 0, Zagolovok[i]);
                                if (res_Add != 0)
                                    break;
                            }
                            int vat = 0;
                            //if (Params.VAT != 0)
                            //    vat = 1;

                            if (res_Add == 0)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddPosField ");

                                //ssss
                                res_Add = AddPosField(ZagolovokHeight + 1, 4, 1,
                                                      //SerNo
                                                      ZagolovokHeight + 2, 8, 1,
                                                      //DocNo
                                                      ZagolovokHeight + ad_lines + 7, 19, 1,
                                                      //Date
                                                      ZagolovokHeight + ad_lines + 7, 14, 1,
                                                      //Time
                                                      ZagolovokHeight + 1, 20, 1,
                                                      //INN
                                                      ZagolovokHeight + 2, 28, 1,
                                                      //Oper
                                                      ZagolovokHeight + ad_lines + 10, 10, 17)
                                                      //Summ
                                                      ;
                                //реально печатает только: № документа DocNo, оператор имя Oper, сумма документа Summ !!!
                            }
                            if (res_Add == 0 && versionKKM == 0)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Properties.Resource.SerNo);
                                res_Add = AddFreeField(ZagolovokHeight + 1, 0, 1, PrintMode, 0, Properties.Resource.SerNo); //SerNo
                            }

                            if (res_Add == 0 && versionKKM == 0)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Properties.Resource.INN);
                                res_Add = AddFreeField(ZagolovokHeight + 1, 17, 1, PrintMode, 0, Properties.Resource.INN); //INN
                            }

                            if (res_Add == 0)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Properties.Resource.OpName);
                                res_Add = AddFreeField(ZagolovokHeight + 2, 17, 1, PrintMode, 0, Properties.Resource.OpName); //Oper
                            }

                            if (res_Add == 0)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Properties.Resource.DocNo);
                                res_Add = AddFreeField(ZagolovokHeight + 2, 0, 1, PrintMode, 0, Properties.Resource.DocNo); //DocNo
                            }

                            if (res_Add == 0)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Properties.Resource.CashReceipt);
                                res_Add = AddFreeField(ZagolovokHeight + 3, 10, 1, PrintMode, 0, Properties.Resource.CashReceipt); //Кассовый чек
                            }

                            if (res_Add == 0)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField <1030> " + Params.What);
                                res_Add = AddFreeField(ZagolovokHeight + 4, 0, 1, PrintMode, 0, "<1030> " + Params.What); //<Наименование товара> Оплата парковки
                            }
                            if (res_Add == 0)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + string.Format(Properties.Resource.CardId, Params.CardId));
                                res_Add = AddFreeField(ZagolovokHeight + 5, 0, 1, PrintMode, 0, string.Format(Properties.Resource.CardId, Params.CardId));//Номер карты 
                            }
                            if ((res_Add == 0) && (ad_lines > 0))
                            {
                                for (int i = 0; i < ad_lines; i++)
                                {
                                    if (log != null)
                                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Params.AddStrings[i]);
                                    res_Add = AddFreeField(ZagolovokHeight + i + 6, 0, 1, PrintMode, 0, Params.AddStrings[i]);
                                    if (res_Add != 0)
                                        break;
                                }
                            }

                            if (res_Add == 0)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + string.Format(Properties.Resource.EnterTime, Params.EntryTime.ToString("HH:mm  dd-MM-yyyy")));
                                res_Add = AddFreeField(ZagolovokHeight + ad_lines + 6, 0, 1, PrintMode, 0, string.Format(Properties.Resource.EnterTime, Params.EntryTime.ToString("HH:mm  dd-MM-yyyy")));
                            }
                            if (res_Add == 0)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + string.Format(Properties.Resource.PaymentTime, DateTime.Now.ToString("HH:mm  dd-MM-yyyy")));
                                if (versionKKM == 0)
                                    res_Add = AddFreeField(ZagolovokHeight + ad_lines + 7, 0, 1, PrintMode, 0, Properties.Resource.PaymentTime);
                                //    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + string.Format(Properties.Resources.PaymentTime, DateTime.Now.ToString("HH:mm  dd-MM-yyyy")));
                                else
                                    res_Add = AddFreeField(ZagolovokHeight + ad_lines + 7, 0, 1, 3, 0, Properties.Resource.PaymentTime + ": " + DateTime.Now.ToString("HH:mm  dd-MM-yyyy"));
                            }
                            if (Params.ExitTime != null)
                            {
                                if (res_Add == 0)
                                {
                                    if (log != null)
                                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Properties.Resource.ExitBefore);
                                    res_Add = AddFreeField(ZagolovokHeight + ad_lines + 8, 0, 49, PrintMode, 0, Properties.Resource.ExitBefore);
                                }
                                if (res_Add == 0)
                                {
                                    if (log != null)
                                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + ((DateTime)Params.ExitTime).ToString("HH:mm dd-MM-yyyy"));
                                    res_Add = AddFreeField(ZagolovokHeight + ad_lines + 9, 3, 49, PrintMode, 0, ((DateTime)Params.ExitTime).ToString("HH:mm dd-MM-yyyy"));
                                }
                            }
                            if (res_Add == 0)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Properties.Resource.Paid);
                                res_Add = AddFreeField(ZagolovokHeight + ad_lines + 10, 0, 17, PrintMode, 0, Properties.Resource.Paid);
                            }
                            if (res_Add == 0)
                            {
                                switch (Params.VAT)
                                {
                                    case 0: // Без НДС
                                        if (log != null)
                                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1105>");
                                        res_Add = AddFreeField(ZagolovokHeight + ad_lines + 11, 0, 1, PrintMode, 0, "<1105>");
                                        break;
                                    case 1: // НДС 0%
                                        if (log != null)
                                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1104>");
                                        res_Add = AddFreeField(ZagolovokHeight + ad_lines + 11, 0, 1, PrintMode, 0, "<1104>");
                                        break;
                                    case 2: // НДС 10%
                                        if (log != null)
                                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1103>");
                                        res_Add = AddFreeField(ZagolovokHeight + ad_lines + 11, 0, 1, PrintMode, 0, "<1103>");
                                        break;
                                    case 3: // НДС 18%
                                        if (log != null)
                                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1002>");
                                        res_Add = AddFreeField(ZagolovokHeight + ad_lines + 11, 0, 1, PrintMode, 0, "<1102>");
                                        break;
                                }
                                if (versionKKM == 0)
                                    vat++;
                            }
                            if (res_Add == 0)
                            {
                                //1055 применяемая система налогообложения
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1055> 1");
                                res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 11, 0, 1, PrintMode, 0, "<1055> 0"); //00 - система налогообложения (СНО) по умолчанию
                                                                                                                               //vat++;
                            }

                            if (res_Add == 0)//ПРИНЯТО:
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + string.Format(Properties.Resource.InCash, string.Format("{0:F2}", Params.Pay_Summa)));
                                res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 12, 0, 1, PrintMode, 0, string.Format(Properties.Resource.InCash, string.Format("{0:F2}", Params.Pay_Summa)));
                            }
                            if (res_Add == 0)//ВЫДАНО СДАЧИ:
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + string.Format(Properties.Resource.ChangesIssued, string.Format("{0:F2}", Params.Pay_SummaSdachi)));
                                res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 13, 0, 1, PrintMode, 0, string.Format(Properties.Resource.ChangesIssued, string.Format("{0:F2}", Params.Pay_SummaSdachi)));
                            }
                            //1001 - признак автоматического режима
                            //if (res_Add == 0)
                            //{
                            //    if (log != null)
                            //        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1001> 1");
                            //    res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14, 0, 1, 3, 0, string.Format(Properties.Resources.InCash, string.Format("{0:F2}", Params.Pay_Summa)));
                            //    vat++;
                            //}
                            /*if (res_Add == 0)
                            {
                                //1030 Наименование товара
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1030> Оплата парковки");
                                res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14, 0, 1, 3, 0, "<1030> Оплата парковки");
                                vat++;
                            }*/

                            //if (res_Add == 0)
                            //{
                            //    //1008 email или телефон
                            //    if (log != null)
                            //        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1008> " + "test@r-p-s.ru");
                            //    res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14, 0, 1, 3, 0, "<1008> " + "test@r-p-s.ru");
                            //    vat++;
                            //}
                            if (res_Add == 0)
                            {
                                //1038 Номер смены
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1038> Номер смены "/* + (resources.LastZNumber + 1).ToString()*/);
                                if (versionKKM == 0)
                                    res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14, 0, 1, PrintMode, 0, "Номер смены"/* + (resources.LastZNumber + 1).ToString()*/);
                                res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14, 30, 1, PrintMode, 0, "<1038>"/* + (resources.LastZNumber + 1).ToString()*/);
                                if (versionKKM == 0)
                                    vat++;
                            }
                            //if (res_Add == 0)
                            //{
                            //    //1055 применяемая система налогообложения
                            //    if (log != null)
                            //        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1055> 1");
                            //    res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14, 0, 1, PrintMode, 0, "<1055> 0");
                            //    //vat++;
                            //}
                            if (res_Add == 0)
                            {
                                //1042 номер чека за смену
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1042> Номер чека за смену");
                                if (versionKKM == 0)
                                    res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14, 0, 1, PrintMode, 0, "Номер чека за смену");
                                res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14, 30, 1, PrintMode, 0, "<1042>");
                                if (versionKKM == 0)
                                    vat++;
                            }
                            if (res_Add == 0 && versionKKM == 0)
                            {
                                //1115 адрес проверки
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + "<1115>");
                                res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14, 0, 1, PrintMode, 0, "<1115>");
                                vat++;
                            }

                            if (res_Add == 0)
                                for (int i = 0; i < FooterHeight; i++)
                                {

                                    if (log != null)
                                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Footer[i]);
                                    res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14 + i, 0, 1, PrintMode, 0, Footer[i]);
                                    if (res_Add != 0)
                                        break;
                                }
                            if (res_Add == 0)
                                if (Params.BankSlip.Count > 0)
                                {
                                    res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14 + FooterHeight, 0, 1, PrintMode, 0, "---------------------------------------");
                                }
                            for (int i = 0; i < Params.BankSlip.Count; i++)
                            {

                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: AddFreeField " + Params.BankSlip[i]);
                                res_Add = AddFreeField(ZagolovokHeight + ad_lines + vat + 14 + FooterHeight + i + 1, 0, 1, PrintMode, 0, Params.BankSlip[i]);
                                if (res_Add != 0)
                                    break;
                            }

                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: PrintFiscalReceipt ");
                            PrintFiscalReceipt();

                            if (res.resultCode == ErrorCode.Sucsess)
                                res.boolResult = true;
                            else
                            {
                                res.boolResult = false;
                                CloseFreeDoc();
                                res.resultDescription = Properties.Resource.PrintChequeError + res.resultCode.ToString() + " " + FR_ERROR_TO_STRING(res.resultCode);
                                ResultDescription = res.resultDescription;
                            }
                        }
                        catch (Exception exc)
                        {

                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: Exception " + exc.ToString());

                            CloseFreeDoc();
                            res.boolResult = false;
                        }

                        _serialPort.Close();
                        OpenDLL(op_name, psw, comNum, Oem);
                        Thread.Sleep(100);
                    }
                    busy = false;
                    return res;

                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printCheck: Exception " + exc.ToString());
                res.resultCode = ErrorCode.NotConnect;
                busy = false;
                return res;
            }
        }

        public void ESC_a_n(int n)
        {
            byte[] nosymbols = new byte[] { 0x1b, 0x61, 0x01 }; //пока печатаем для тестов, вообще нужно включить в 0
            SendDirectCommand(nosymbols);
            mDelay2();
        }

        int OpenFDocC()
        {
            int rescode = 0;
            List<byte> cmd = CreateCmd();
            string s;
            dt = DateTime.Now;
            s = "70";
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            cmd = AddEnd(cmd);
            ba = cmd.ToArray();
            SendCmd();
            if (cn < 0x21) cn = 0x21;
            return rescode;
        }
        int CloseFDocC()
        {
            int rescode = 0;
            List<byte> cmd = new List<byte>();
            cmd.Add(esc);
            cmd.Add(esc);
            ba = cmd.ToArray();
            _serialPort.Write(ba, 0, ba.Length);
            return rescode;
        }
        int CutFDoc()
        {
            int rescode = 0;
            List<byte> cmd = new List<byte>();
            cmd.Add(esc);
            cmd.Add((byte)'i');
            ba = cmd.ToArray();
            _serialPort.Write(ba, 0, ba.Length);
            return rescode;
        }
        int PrintOEMDocC(string Info, long InfoLen)
        {
            int rescode = 0;
            List<byte> cmd = new List<byte>();
            foreach (byte c in Encode(Info))
                cmd.Add(c);
            ba = cmd.ToArray();
            _serialPort.Write(ba, 0, ba.Length);
            return rescode;
        }
        protected override ResultT printNotCheck(List<string> Items)
        {
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printNotCheck: KKM busy ");
                    res.resultCode = ErrorCode.NotConnect;
                    return res;
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                lock (locker)
                {
                    byte PaperStatus;
                    bool PaperInOutput = true;
                    try
                    {
                        OpenDLL(op_name, psw, comNum, Oem);
                        Thread.Sleep(100);
                    }
                    catch { }
                    while (PaperInOutput)
                    {
                        GetSerialNum();
                        //GetStatusNoPlus(5);
                        PaperStatus = (byte)GetPrnByte(5);
                        //PaperStatus = (byte)GetStatusNum(6);
                        BitVector32 hs = new BitVector32(PaperStatus);
                        PaperInOutput = hs[myBit5];
                        if (!PaperInOutput  /*|| ((DateTime.Now - now).TotalSeconds > 5)*/)
                            break;
                        Thread.Sleep(100);
                    }

                    CloseDLL();
                    Thread.Sleep(100);
                    _serialPort.Close();
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();

                    res.resultCode = setResultCode(OpenFDocC());
                    //res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));

                    foreach (string Item in Items)
                    {
                        if (res.resultCode == ErrorCode.Sucsess)
                            res.resultCode = setResultCode(PrintOEMDocC(Item, Item.Length));
                        res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));

                        if (res.resultCode != ErrorCode.Sucsess)
                        {
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printNotCheck: resultCode="
                                    + res.resultCode.ToString());
                            break;
                        }
                    }
                    if (Items.Count < 15 && QRTypeString == null)
                    {
                        for (int i = 0; i < 15 - Items.Count; i++)
                        {
                            res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));

                            if (res.resultCode != ErrorCode.Sucsess)
                            {
                                if (log != null)
                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printNotCheck: resultCode="
                                        + res.resultCode.ToString());
                                break;
                            }
                        }
                    }

                    //может QR тут печатать???
                    if (QRDateString != null && QRDateString != "" && QRTypeString == null)
                    {
                        Thread.Sleep(100);
                        QRcodeStore(QRDateString);
                        Thread.Sleep(100);
                        QRcodePrintPrim();
                        Thread.Sleep(100);
                        res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));
                    }
                    if (QRDateString != null && QRDateString != "" && QRTypeString == "Ticket")
                    {
                        Thread.Sleep(100);
                        QRcodeStore(QRDateString);
                        Thread.Sleep(100);
                        QRcodePrintPrim();
                        Thread.Sleep(100);
                        res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));
                    }
                    if (QRDateString != null && QRDateString != "" && QRTypeString == "Code128")
                    {
                        Thread.Sleep(100);
                        PrintBarcode(QRDateString);
                        Thread.Sleep(100);
                        //QRcodePrintPrim();
                        Thread.Sleep(100);
                        res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));
                    }

                    if (res.resultCode == ErrorCode.Sucsess)
                        res.resultCode = setResultCode(CutFDoc());

                    if (res.resultCode == ErrorCode.Sucsess)
                        res.resultCode = setResultCode(CloseFDocC());
                    if (res.resultCode == ErrorCode.Sucsess)
                        res.boolResult = true;
                    else
                        res.boolResult = false;

                    _serialPort.Close();
                    Thread.Sleep(100);
                    OpenDLL(op_name, psw, comNum, Oem);
                    Thread.Sleep(100);
                    //setPrezenter(1, 1, 1);
                    busy = false;
                    return res;
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: printNotCheck: Exception " + exc.ToString());
                res.resultCode = ErrorCode.NotConnect;
                busy = false;
                return res;
            }
        }

        public void SendDirectCommand(byte[] cmd)
        {
            _serialPort.Write(cmd, 0, cmd.Length);
        }
        public void mDelay2()
        {
            Thread.Sleep(50);
        }
        public byte[] CombineByteArrayAndString(byte[] a, string bs)
        {
            byte[] b = Encoding.GetEncoding("CP866").GetBytes(bs);
            return CombineByteArray(a, b);
        }
        public byte[] CombineByteArray(byte[] a, byte[] b)
        {
            byte[] c = new byte[a.Length + b.Length];
            System.Buffer.BlockCopy(a, 0, c, 0, a.Length);
            System.Buffer.BlockCopy(b, 0, c, a.Length, b.Length);
            return c;
        }


        public void PrintBarcode(string Info)
        {
            if (true)
            {
                //Задание цифр для штрихкода Select printing position of HRI characters Below the bar code 
                byte[] nosymbols = new byte[] { 0x1d, 0x48, 0x02 }; //пока печатаем для тестов, вообще нужно включить в 0
                SendDirectCommand(nosymbols);
                mDelay2();

                //высота
                byte h = 0x70;
                byte[] setheight = new byte[] { 0x1d, 0x68, h }; //15-32,5%
                SendDirectCommand(setheight);
                mDelay2();

                //font 
                byte f = 0x01;
                byte[] setfont = new byte[] { 0x1d, 0x66, f }; //Фонт "00" – Фонт А (12х24); "01" – Фонт В (9х17)
                SendDirectCommand(setfont);
                mDelay2();

                //Ширина % для Code128 = 2-6
                byte w = (byte)ticketBarCodeWidth;// 0x01;
                byte[] setwidth = new byte[] { 0x1d, 0x77, w };
                SendDirectCommand(setwidth);
                mDelay2();

                //печать
                byte[] printdata = new byte[] { 0x1d, 0x6b }; //здесь Print bar code 
                string data = "{A" + Info; //0x7B, 0x41, +19-digits...
                //string data = "1234567890"; //19-digits...
                byte dataLength = (byte)(data.Length + 2);
                printdata = CombineByteArray(printdata, new byte[] { 0x49 });
                printdata = CombineByteArray(printdata, new byte[] { dataLength });
                printdata = CombineByteArray(printdata, new byte[] { 0x7B, 0x41 });
                printdata = CombineByteArrayAndString(printdata, data);
                SendDirectCommand(printdata);
                mDelay2();

            }
            if (false)
            {
                List<byte> cmd = CreateCmd();
                string s;
                dt = DateTime.Now;
                s = "1A";//Печать штрих-кода
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = "49";//Тип штрих-кода "49" - CODE128
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = "02";//Печать цифрового кода "02" – под штрих-кодом;
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = "01";//Фонт "00" – Фонт А (12х24); "01" – Фонт В (9х17)
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = "50";//Высота штрих-кода "00" - "FF"
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = "02";//Ширина штрих-кода "02" - "06" 
                cmd.Add(fs);

                foreach (char c in s)
                    cmd.Add((byte)c);
                foreach (byte c in Encode(Info))
                    cmd.Add(c);
                cmd.Add(fs);

                cmd = AddEnd(cmd);
                ba = cmd.ToArray();

                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: SendCmd: PrintBarcode");
                ToLog("T->", ba, ba.Length);
                SendCmd();
                ToLog("R->", rb, rd);
                if (cn < 0x21) cn = 0x21;
                //return rescode;

                //ba = cmd.ToArray();
                //_serialPort.Write(ba, 0, ba.Length);
            }
            if (false)
            {
                _serialPort.Close();
                Thread.Sleep(100);
                OpenDLL(op_name, psw, comNum, Oem);
                Thread.Sleep(1000);
                res.resultCode = setResultCode(OpenFDoc());
                Thread.Sleep(100);
                for (int i = 0; i < 15; i++)
                {
                    res.resultCode = setResultCode(PrintOEMDoc("\r\n", 2));

                }

                int ress = PrintBarcodeFDoc(1, 2, 50, 0, 2, 11, "11111111111");
                Thread.Sleep(1000);
                for (int i = 0; i < 15; i++)
                {
                    res.resultCode = setResultCode(PrintOEMDoc("\r\n", 2));

                }
                ress = PrintBarcodeFDoc(73, 2, 50, 1, 2, 19, Info);
                res.resultCode = setResultCode(ress);
                //res.resultCode = setResultCode(PrintBarcodeFDoc(73, 2, 50, 1, 2, 19, Info));
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: QRPaint=" + res.resultCode.ToString());
                Thread.Sleep(1000);
                for (int i = 0; i < 15; i++)
                {
                    res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));

                }

                res.resultCode = setResultCode(CloseFDoc());
                Thread.Sleep(100);
                CloseDLL();
                Thread.Sleep(100);
                _serialPort.Close();
                _serialPort.Open();
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();

            }

        }

        public void PrintBarcode2Check(string Info)
        {
            if (true)
            {
                //Задание цифр для штрихкода Select printing position of HRI characters Below the bar code 
                byte[] nosymbols = new byte[] { 0x1d, 0x48, 0x02 }; //пока печатаем для тестов, вообще нужно включить в 0
                SendDirectCommand(nosymbols);
                mDelay2();

                //высота
                byte h = 0x70;
                byte[] setheight = new byte[] { 0x1d, 0x68, h }; //15-32,5%
                SendDirectCommand(setheight);
                mDelay2();

                //font 
                byte f = 0x01;
                byte[] setfont = new byte[] { 0x1d, 0x66, f }; //Фонт "00" – Фонт А (12х24); "01" – Фонт В (9х17)
                SendDirectCommand(setfont);
                mDelay2();

                //Ширина % для Code128 = 2-6
                byte w = (byte)ticketBarCodeWidth;// 0x01;
                byte[] setwidth = new byte[] { 0x1d, 0x77, w };
                SendDirectCommand(setwidth);
                mDelay2();

                //печать
                byte[] printdata = new byte[] { 0x1d, 0x6b }; //здесь Print bar code 
                string data = Info; //19-digits...
                //string data = "1234567890"; //19-digits...
                byte dataLength = (byte)(data.Length + 2);
                printdata = CombineByteArray(printdata, new byte[] { 0x49 });
                printdata = CombineByteArray(printdata, new byte[] { dataLength });
                printdata = CombineByteArray(printdata, new byte[] { 0x7B, 0x41 });
                printdata = CombineByteArrayAndString(printdata, data);
                SendDirectCommand(printdata);
                mDelay2();

            }
            else if (false)
            {
                List<byte> cmd = CreateCmd();
                string s;
                dt = DateTime.Now;
                s = "1A";//Печать штрих-кода
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = "49";//Тип штрих-кода "49" - CODE128
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = "02";//Печать цифрового кода "02" – под штрих-кодом;
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = "01";//Фонт "00" – Фонт А (12х24); "01" – Фонт В (9х17)
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = "50";//Высота штрих-кода "00" - "FF"
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);
                s = "02";//Ширина штрих-кода "02" - "06" 
                foreach (char c in s)
                    cmd.Add((byte)c);
                cmd.Add(fs);

                foreach (byte c in Encode(Info))
                    cmd.Add(c);
                cmd.Add(fs);

                cmd = AddEnd(cmd);
                ba = cmd.ToArray();

                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: SendCmd: PrintBarcode2Check");
                ToLog("T->", ba, ba.Length);
                SendCmd();
                ToLog("R->", rb, rd);
                if (cn < 0x21) cn = 0x21;
                //return rescode;

                //ba = cmd.ToArray();
                //_serialPort.Write(ba, 0, ba.Length);
            }
            else if (false)
            {
                _serialPort.Close();
                Thread.Sleep(100);
                OpenDLL(op_name, psw, comNum, Oem);
                Thread.Sleep(1000);
                res.resultCode = setResultCode(OpenFDoc());
                Thread.Sleep(100);
                for (int i = 0; i < 15; i++)
                {
                    res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));

                }

                int ress = PrintBarcodeFDoc(1, 2, 50, 0, 2, 11, "11111111111");
                Thread.Sleep(1000);
                for (int i = 0; i < 15; i++)
                {
                    res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));

                }
                ress = PrintBarcodeFDoc(73, 2, 50, 1, 2, 19, Info);
                res.resultCode = setResultCode(ress);
                //res.resultCode = setResultCode(PrintBarcodeFDoc(73, 2, 50, 1, 2, 19, Info));
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: QRPaint=" + res.resultCode.ToString());
                Thread.Sleep(1000);
                for (int i = 0; i < 15; i++)
                {
                    res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));

                }

                res.resultCode = setResultCode(CloseFDoc());
                Thread.Sleep(100);
                CloseDLL();
                Thread.Sleep(100);
                _serialPort.Close();
                _serialPort.Open();
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();

            }
        }

        public void QRcodeStore(string Info)
        {
            {
                //List<byte> cmd = CreateCmd();
                //string s;
                //dt = DateTime.Now;
                //s = "1E";//Построение QR-кода
                //foreach (char c in s)
                //    cmd.Add((byte)c);
                //cmd.Add(fs);
                //s = "24";//Параметры кода 1-UTF8 (для кириллицы), 0, 0-MQR, 1-строчные (прописные), 00-уровень безопасности
                //foreach (char c in s)
                //    cmd.Add((byte)c);
                //cmd.Add(fs);
                //s = "02";//Версия кода (1-18 (дес))
                //foreach (char c in s)
                //    cmd.Add((byte)c);
                //cmd.Add(fs);
                //s = "0";//
                //foreach (char c in s)
                //    cmd.Add((byte)c);
                //cmd.Add(fs);
                //s = "QR:";
                //foreach (char c in s)
                //    cmd.Add((byte)c);
                //foreach (byte c in Encode(Info))
                //    cmd.Add(c);
                //cmd.Add(fs);

                //cmd = AddEnd(cmd);
                //ba = cmd.ToArray();

                //if (log != null)
                //    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: SendCmd: QRcodeStore");
                //SendCmd();
                //if (cn < 0x21) cn = 0x21;
                ////return rescode;

                ////ba = cmd.ToArray();
                ////_serialPort.Write(ba, 0, ba.Length);
            }

            {
                _serialPort.Close();
                Thread.Sleep(100);
                OpenDLL(op_name, psw, comNum, Oem);
                Thread.Sleep(100);
                string QR = "QR:" + Info;
                if (QRTypeString == null) res.resultCode = setResultCode(QRPaint(0, 4, Info));
                else res.resultCode = setResultCode(QRPaint(0, 4, Info));
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: QRPaint=" + res.resultCode.ToString());
                CloseDLL();
                Thread.Sleep(100);
                _serialPort.Close();
                _serialPort.Open();
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();

            }

        }

        //cnjg

        public void QRcodePrintPrim()
        {
            List<byte> cmd = CreateCmd();
            string s;
            dt = DateTime.Now;
            s = "1F";//Печать QR-кода
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = "03";//Размер одного квадрата QR-кода по ширине(в точках печати принтера) 01-04
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            s = "02";//Размер одного квадрата QR-кода по высоте (в точках печати принтера) 01-05
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);
            if (QRTypeString == null)
                s = "01";//Выравнивание 00-02
            else s = "02";//Выравнивание 00-02
            foreach (char c in s)
                cmd.Add((byte)c);
            cmd.Add(fs);

            cmd = AddEnd(cmd);
            ba = cmd.ToArray();
            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: SendCmd: QRcodePrintPrim");
            SendCmd();
            if (cn < 0x21) cn = 0x21;
            //return rescode;
        }

        protected override ResultT printBankSlip(List<string> Items)
        {
            lock (locker)
            {
                byte PaperStatus;
                bool PaperInOutput = true;
                while (PaperInOutput)
                {
                    GetSerialNum();
                    //GetStatusNoPlus(5);
                    PaperStatus = (byte)GetPrnByte(5);
                    //PaperStatus = (byte)GetStatusNum(6);
                    BitVector32 hs = new BitVector32(PaperStatus);
                    PaperInOutput = hs[myBit5];
                    if (!PaperInOutput  /*|| ((DateTime.Now - now).TotalSeconds > 5)*/)
                        break;
                    Thread.Sleep(100);
                }
                CloseDLL();
                _serialPort.Close();
                _serialPort.Open();
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();

                res.resultCode = setResultCode(OpenFDocC());
                res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));

                foreach (string Item in Items)
                {
                    if (res.resultCode == ErrorCode.Sucsess)
                        res.resultCode = setResultCode(PrintOEMDocC(Item, Item.Length));
                    res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));

                    if (res.resultCode != ErrorCode.Sucsess)
                        break;
                }
                if (Items.Count < 15)
                {
                    for (int i = 0; i < 15 - Items.Count; i++)
                    {
                        res.resultCode = setResultCode(PrintOEMDocC("\r\n", 2));

                        if (res.resultCode != ErrorCode.Sucsess)
                            break;
                    }
                }
                if (res.resultCode == ErrorCode.Sucsess)
                    res.resultCode = setResultCode(CutFDoc());

                if (res.resultCode == ErrorCode.Sucsess)
                    res.resultCode = setResultCode(CloseFDocC());
                if (res.resultCode == ErrorCode.Sucsess)
                    res.boolResult = true;
                else
                    res.boolResult = false;

                _serialPort.Close();
                OpenDLL(op_name, psw, comNum, Oem);
                setPrezenter(1, 1, 1);
                return res;
            }
        }
        protected override ResultT setInterfaceParam(string BaudRate, byte Is5Wires, byte IsDateTime)
        {
            return res;
            /*lock (locker)
            {
                res.resultCode = setResultCode(SetInterfaceParam(BaudRate, Is5Wires, IsDateTime));
                return res;
            }*/
        }
        protected override ResultT zReportByNum(int ZNo)
        {
            lock (locker)
            {
                byte PaperStatus;
                bool PaperInOutput = true;

                while (PaperInOutput)
                {
                    GetSerialNum();
                    PaperStatus = (byte)GetPrnByte(5);
                    BitVector32 hs = new BitVector32(PaperStatus);
                    PaperInOutput = hs[myBit5];
                    if (!PaperInOutput)
                        break;
                    Thread.Sleep(100);
                }

                res.resultCode = setResultCode(EJZreport(ZNo));
                return res;
            }
        }
        protected override ResultT setPrezenter(byte IsRetrak, byte IsPrezenter, byte IsSet)
        {
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    //if (log != null)
                    //    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: setPrezenter: KKM busy ");
                    //res.resultCode = ErrorCode.NotConnect;
                    return res;
                }
                Thread.Sleep(50);
                //if (log != null)
                //    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: setPrezenter: KKM busy countS="
                //        + countS.ToString());

            }
            busy = true;
            try
            {
                lock (locker)
                {
                    isRetrak = IsRetrak;
                    if (IsSet == 0 && IsPrezenter == isPrezenter)
                    {
                        res.resultCode = ErrorCode.Sucsess;
                        busy = false;
                        return res;
                    }
                    else
                    {
                        if (IsSet != 1 && IsPrezenter != isPrezenter)
                        {
                            SetPrezenter(IsPrezenter, IsPrezenter, 2);
                            isPrezenter = IsPrezenter;
                            //if (log != null)
                            //    log.ToLog(1, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: setPrezenter: "
                            //+ IsPrezenter.ToString() + IsPrezenter.ToString() + " 2");
                        }
                        Thread.Sleep(50);

                        res.resultCode = setResultCode(SetPrezenter(IsRetrak, IsPrezenter, IsSet));
                        //if (log != null)
                        //    log.ToLog(1, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: setPrezenter: "
                        //+ IsRetrak.ToString() + IsPrezenter.ToString() + IsSet.ToString());

                        busy = false;
                        return res;
                    }
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: setPrezenter: Exception " + exc.ToString());
                res.resultCode = ErrorCode.NotConnect;
                busy = false;
                return res;
            }
        }
        bool busy = false;
        int countSbusyMax = 220; // *50ms

        //Получаем сумму в фискальном регистраторе
        int Get_Summa_FR()
        {
            try
            {
                sum_kassa = 0;
                //res.resultCode = setResultCode(GetEReport(0x37, 0));
                res.resultCode = setResultCode(GetEReport(0x34, 0));
                if (res.resultCode == ErrorCode.Sucsess)
                {
                    if (versionKKM == 0)
                        sum_kassa = GetFldInt(23);
                    //Возвращаемые значения для $37: 15-Сумма в кассе, 23-Нарастающий Итог коррекция Возврат расхода
                    //для 0x34: 23 - Сумма в кассе
                    else if (versionKKM == 1)
                        sum_kassa = GetFldInt(31);
                    //sum_kassa = GetFldInt(31);
                }
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": Get_Summa_FR: resultCode="
                    //+ Environment.NewLine + res.ToString()
                    + Environment.NewLine + "sum_kassa = " + sum_kassa.ToString()
                    );

                return sum_kassa;
            }
            catch
            {
                return 0;
            }
        }

        protected override bool _EReport()
        {
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    //if (log != null)
                    //    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: _eReport: KKM busy ");
                    //res.resultCode = ErrorCode.NotConnect;
                    return false;
                }
                Thread.Sleep(50);
                //if (log != null)
                //    log.ToLog(1, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: _eReport: KKM busy countS="
                //        + countS.ToString());

            }
            busy = true;
            bool resEReport = EReport_();
            Thread.Sleep(20);
            busy = false;
            return resEReport;
        }

        /*
        protected override bool _FNGetStatus()
        {
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    //if (log != null)
                    //    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: _eReport: KKM busy ");
                    //res.resultCode = ErrorCode.NotConnect;
                    return false;
                }
                Thread.Sleep(50);
                //if (log != null)
                //    log.ToLog(1, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": PrimFAKKM: _eReport: KKM busy countS="
                //        + countS.ToString());

            }
            busy = true;
            bool resFNGetStatus = false;
            if (FNGetStatus() == 0) resFNGetStatus = true;
            Thread.Sleep(20);
            busy = false;
            return resFNGetStatus;
        }
        */

        protected override ResultT inkasstion()
        {
            lock (locker)
            {
                try
                {
                    {
                        int SummaFR = 0;
                        //SummaFR = Get_Summa_FR();
                        if (EReport_())
                            SummaFR = sum_kassa;
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": inkasstion: SummaFR=" + SummaFR.ToString()
                            + Environment.NewLine + "inkasationSum=" + inkasstionSum.ToString() + " коп");
                        if (/*inkasstionSum != 0 && */inkasstionSum < SummaFR)
                            SummaFR = inkasstionSum;
                        inkasstionSum = SummaFR;
                        if (res.resultCode == ErrorCode.Sucsess)
                            res.resultCode = setResultCode(FromCash(SummaFR));//ИНКАССАЦИЯ

                        if (res.resultCode != 0)
                        {
                            res.resultDescription = string.Format(Properties.Resource.EncashmentError, res.ToString(), FR_ERROR_TO_STRING(res.resultCode));
                            ResultDescription = res.resultDescription;
                        }
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": inkasstion: resultCode="
                            + Environment.NewLine + res.ToString()
                            );
                    }
                }
                catch (Exception err)
                {
                    res.resultCode = ErrorCode.OtherError;
                    res.resultDescription = string.Format(Properties.Resource.EncashmentError, err.Message, "");
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": inkasstion: resultCode="
                        + Environment.NewLine + res.ToString()
                        );
                }
                return res;
            }
        }

        protected override ResultT toCash()
        {
            lock (locker)
            {
                try
                {
                    {

                        if (res.resultCode == ErrorCode.Sucsess)
                            res.resultCode = setResultCode(ToCash(sumTokassa));//sumTokassa

                        if (res.resultCode != 0)
                        {
                            res.resultDescription = string.Format(Properties.Resource.EncashmentError, res.ToString(), FR_ERROR_TO_STRING(res.resultCode));
                            ResultDescription = res.resultDescription;
                        }
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": sumTokassa: resultCode="
                            + Environment.NewLine + res.ToString()
                            );
                    }
                }
                catch (Exception err)
                {
                    res.resultCode = ErrorCode.OtherError;
                    res.resultDescription = string.Format(Properties.Resource.EncashmentError, err.Message, "");
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": sumTokassa: resultCode="
                        + Environment.NewLine + res.ToString()
                        );
                }
                return res;
            }
        }

    }
}
