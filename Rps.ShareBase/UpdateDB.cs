﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rps.ShareBase
{
    public interface IUpdateDb
    {
        int NumberPacket { get; }
        int NumberItemInPacket { get; }
        void Update(SqlConnection connect);
    }

    public class InfoVersionDb
    {
        public int Packet { get; set; }
        public int Item { get; set; }
    }

    public class UpdateDbFromScript : IUpdateDb
    {
        private const char SymbolSplit = '_';
        private string NameFile = string.Empty;
        public int NumberPacket { get; set; }
        public int NumberItemInPacket { get; set; }

        public static UpdateDbFromScript Create(string NameFile)
        {
            UpdateDbFromScript result = null;
            if (!string.IsNullOrEmpty(NameFile) && !string.IsNullOrWhiteSpace(NameFile) && System.IO.File.Exists(NameFile))
            {
                string fname = System.IO.Path.GetFileName(NameFile);
                string[] dats = fname.Split(UpdateDbFromScript.SymbolSplit);
                try
                {
                    int packet = Convert.ToInt32(dats[0]);
                    int item = Convert.ToInt32(dats[1]);
                    result = new UpdateDbFromScript();
                    result.NameFile = NameFile;
                    result.NumberPacket = packet;
                    result.NumberItemInPacket = item;
                }
                catch (Exception ex)
                {
                }
            }
            return result;
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(NameFile) && !string.IsNullOrWhiteSpace(NameFile) && System.IO.File.Exists(NameFile))
            {
                UtilsBase.ExecuteNonQueryScript(NameFile, connect, null);
            }
            else
            {
                throw new Exception("Not found file script " + NameFile);
            }
        }
    }

    public class UpdateDbEngine
    {
        public static string NameTable = "DbVersion";
        public static string FieldPacket = "CurrentPacket";
        public static string FieldItem = "CurrentItem";
        private List<IUpdateDb> updates = new List<IUpdateDb>();
        private Guid idupdate;

        public UpdateDbEngine(Guid idupdate)
        {
            this.idupdate = idupdate;
            string folder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Scripts");
            FillUpdaterCode();
            FillUpdaterScript(folder);
        }



        public void Register(IUpdateDb updater)
        {
            var find = (from t in updates
                        where t.NumberPacket == updater.NumberPacket && t.NumberItemInPacket == updater.NumberItemInPacket
                        select t).FirstOrDefault();
            if (find != null) { }
            else
            {
                updates.Add(updater);
            }
        }

        public void FillUpdaterCode()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            if (assemblies != null && assemblies.Length > 0)
            {
                foreach (var assem in assemblies)
                {
                    var types = assem.GetTypes();
                    if (types != null && types.Length > 0)
                    {
                        foreach (Type tp in types)
                        {
                            var res = tp.GetInterface(typeof(IUpdateDb).Name, true);
                            if (res != null)
                            {
                                try
                                {
                                    IUpdateDb updater = Activator.CreateInstance(tp) as IUpdateDb;
                                    if (updater != null)
                                    {
                                        this.Register(updater);
                                    }
                                }
                                catch { }
                            }
                        }
                    }
                }
            }
        }

        public void FillUpdaterScript(string folder)
        {
            var files = System.IO.Directory.GetFiles(folder, "*.sql", System.IO.SearchOption.TopDirectoryOnly);
            if (files != null && files.Length > 0)
            {
                foreach (string namefile in files)
                {
                    var updater = UpdateDbFromScript.Create(namefile);
                    if (updater != null)
                    {
                        Register(updater);
                    }
                }
            }
        }

        public static InfoVersionDb GetCurrentDbVersion(SqlConnection connect)
        {
            InfoVersionDb result = null;
            string qr = UtilsBase.MergeString(Sql.Sel, " Top 1 ", Sql.allfield, Sql.from, NameTable);
            DataRow row = UtilsBase.FillRow(qr, connect, null);
            if (row!=null)
            {
                try
                {
                    result = new InfoVersionDb();
                    result.Packet = ConvertField.GetInt(row, FieldPacket).Value;
                    result.Item = ConvertField.GetInt(row, FieldItem).Value;
                }
                catch(Exception ex)
                {
                    string aa = ex.Message;
                }
            }
            return result;
        }

        public void SetDbVersion(int packet,int item, SqlConnection connect)
        {
            UtilsBase.SetOneRow(NameTable, "Id", connect, null, FieldPacket, packet,FieldItem,item);
        }

        public void UpdateToCurrent(SqlConnection connect)
        {
            try
            {
                string zap = "select top 1 DbVer from SystemVersion where Status=@up";
                var res = UtilsBase.ExecuteScalar(zap, connect, null, "@up", 3);
                if (res != null && res != System.DBNull.Value)
                {
                    int ver = System.Convert.ToInt32(res);
                    UpdateToVersion(connect, ver);
                }
                UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 4 },
                        "Id=@id", new object[] { "@id", idupdate }, connect);
            }
            catch (Exception ex)
            {
                SaveErrorDevice(connect, ex.ToString());
            }


        }

        public void SaveErrorDevice(SqlConnection connect, string message)
        {
            UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 3, "Error", message },
                           "Id=@id", new object[] { "@id", idupdate }, connect);
        }

        public void UpdateToVersion(SqlConnection connect, int NumberPacket)
        {
            InfoVersionDb curversion = GetCurrentDbVersion(connect);
            //var updater = from t in updates where t > curversion && t <= ToVersion orderby t ascending select t;
            //Нужно найти все пакеты между текущими и последним который нужен
            //1 Выбираем эелементы из текущего пакеты но с большим номером item
            var updater1 = from t in updates
                          where t.NumberPacket == curversion.Packet && t.NumberItemInPacket > curversion.Item
                          orderby t.NumberItemInPacket
                          select t ;
            //2 Выбираем эелементы из пакетов с большим номером чем текущий но меньший или равен нужному
            var updater2 = from t in updates where t.NumberPacket > curversion.Packet && t.NumberPacket < NumberPacket
                           orderby t.NumberPacket,t.NumberItemInPacket
                           select t;

            foreach (IUpdateDb vers in updater1)
            {
                vers.Update(connect);
                SetDbVersion(vers.NumberPacket,vers.NumberItemInPacket, connect);
            }
            foreach (IUpdateDb vers in updater2)
            {
                vers.Update(connect);
                SetDbVersion(vers.NumberPacket, vers.NumberItemInPacket, connect);
            }
        }
    }
}
