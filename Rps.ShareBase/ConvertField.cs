﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rps.ShareBase
{
    public static class ConvertField
    {
        public static int? GetInt(this DataRow row,string field)
        {
            int? result = null;
            try
            {
                result = Convert.ToInt32(row[field]);
            }
            catch(Exception ex)
            {

            }
            return result;
        }
        public static string GetString(this DataRow row, string field)
        {
            string result = string.Empty;
            try
            {
                result = Convert.ToString(row[field]);
            }
            catch { }
            return result;
        }
        public static bool? GetBool(this DataRow row, string field)
        {
            bool? result = null;
            try
            {
                result = Convert.ToBoolean(row[field]);
            }
            catch { }
            return result;
        }
        public static long? GetLong(this DataRow row, string field)
        {
            long? result = null;
            try
            {
                result = Convert.ToInt64(row[field]);
            }
            catch { }
            return result;
        }
        public static Guid? GetGuid(this DataRow row,string field)
        {
            Guid? result = null;
            try
            {
                result = Guid.Parse(row[field].ToString());
            }
            catch { }
            return result;
        }
        public static DateTime? GetDateTime(this DataRow row, string field)
        {
            DateTime? result = null;
            try
            {
                result = Convert.ToDateTime(row[field]);
            }
            catch { }
            return result;
        }

        public static object GetObject(this DataRow row, string field)
        {
            object result = null;
            try
            {
                result = row[field];
            }
            catch { }
            return result;
        }
    }
}
