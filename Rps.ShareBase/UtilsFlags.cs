﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rps.ShareBase
{
    public class UtilsFlags
    {
        public static bool IsCloseProgramm = false;

        public static bool UpdaterClosed = false;
        public static bool ConnectClosed = false;
        public static bool CurisClosed = false;
        public static bool RasposClosed = false;

        public static List<Action> lstClosed = new List<Action>();
    }
}
