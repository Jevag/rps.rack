﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Rps.ShareBase
{
    public class Sql
    {
        public const string Sel = "select ";
        public const string from = " from ";
        public const string allfield = "* ";
        public const string wh = " where ";
        public const string ljoin = " left join ";
        public const string ijoin = " inner join ";
        public const string joinon = " on ";
        public const string equal = "=";
        public const string more = ">";
        public const string less = "<";
        public const string notequal = "!=";
        public const string asname = " as ";
        public const string moreequal = ">=";
        public const string lessequal = "<=";
        public const string include = " in ";
        public const string like = " like ";
        public const string not = " not ";
        public const string and = " and ";
        public const string or = " or ";
        public const string openskob = "(";
        public const string closeskob = ")";
        public const string top1 = " Top 1 ";
    }

    public class UtilsBase
    {
        /// <summary>
        /// Создание команды
        /// </summary>
        /// <param name="query">SQL Запрос команды</param>
        /// <param name="dats">Список в формате название параметра, знечение</param>
        /// <returns>Команда</returns>
        public static SqlCommand CreateCommand(string query, params object[] dats)
        {
            SqlCommand comm = new SqlCommand();
            comm.CommandText = query;
            ForListParametrs(dats, (string nameparam, object valparam) =>
            {
                SqlParameter pr = new SqlParameter(nameparam, (dynamic)valparam);
                comm.Parameters.Add(pr);
            });
            return comm;
        }
        /// <summary>
        /// Добавление рдной записи в БД
        /// </summary>
        /// <param name="NameTable">Название таблицы</param>
        /// <param name="connect">Открытое соединение с БД</param>
        /// <param name="transact">Транзакция</param>
        /// <param name="dats">Список в формате название поля в таблице, знечение</param>
        /// <returns>Если возможно то идентификатор записи в БД</returns>
        public static int? InsertCommand(string NameTable, SqlConnection connect, SqlTransaction transact, params object[] dats)
        {
            int? result = null;
            using (SqlCommand command = new SqlCommand())
            {
                StringBuilder strins = new StringBuilder();
                StringBuilder strvalues = new StringBuilder();
                int indexparam = 0;
                ForListParametrs(dats, (string namefield, object valparam) =>
                {
                    if (indexparam == 0)
                    {
                        strins.Append(namefield);
                        string namepr = "@pr" + indexparam.ToString();
                        strvalues.Append(namepr);
                        SqlParameter pr = new SqlParameter(namepr, (dynamic)valparam);
                        command.Parameters.Add(pr);
                        indexparam++;
                    }
                    else
                    {
                        strins.Append(",");
                        strins.Append(namefield);
                        string namepr = "@pr" + indexparam.ToString();
                        strvalues.Append(",");
                        strvalues.Append(namepr);
                        SqlParameter pr = new SqlParameter(namepr, (dynamic)valparam);
                        command.Parameters.Add(pr);
                        indexparam++;
                    }
                });

                string strcommand = "insert into " + NameTable + "(" + strins.ToString() + ") values (" + strvalues.ToString() + ") select SCOPE_IDENTITY()";
                command.CommandText = strcommand;
                command.Connection = connect;
                command.Transaction = transact;
                object res = command.ExecuteScalar();
                if (res != null && res != System.DBNull.Value)
                {
                    result = Convert.ToInt32(res);
                }
            }

            return result;

        }
        /// <summary>
        /// Обновление значений в таблице
        /// </summary>
        /// <param name="NameTable">Название таблицы</param>
        /// <param name="dats">Список в формате название поля значение</param>
        /// <param name="where">Строка условия обновления</param>
        /// <param name="paramwhere">Список в формате название параметра значение параемтра</param>
        /// <param name="connect">Открытое соединение с БД</param>
        /// <param name="transact">Транзакция</param> 
        public static void UpdateCommand(string NameTable, object[] dats, string where, object[] paramwhere, SqlConnection connect, SqlTransaction transact = null)
        {
            using (SqlCommand command = new SqlCommand())
            {
                StringBuilder strbuild = new StringBuilder();

                int indexparam = 0;
                ForListParametrs(dats, (string namefield, object valparam) =>
                {
                    if (indexparam == 0)
                    {
                        strbuild.Append(namefield);
                        strbuild.Append("=");
                        string namepr = "@prset" + indexparam.ToString();
                        strbuild.Append(namepr);
                        SqlParameter pr = new SqlParameter(namepr, (dynamic)valparam);
                        command.Parameters.Add(pr);
                        indexparam++;
                    }
                    else
                    {
                        strbuild.Append(",");
                        strbuild.Append(namefield);
                        strbuild.Append("=");
                        string namepr = "@prset" + indexparam.ToString();
                        strbuild.Append(namepr);
                        SqlParameter pr = new SqlParameter(namepr, (dynamic)valparam);
                        command.Parameters.Add(pr);
                        indexparam++;
                    }
                });

                if (!string.IsNullOrEmpty(where) && !string.IsNullOrWhiteSpace(where))
                {
                    strbuild.Append(" where ");
                    strbuild.Append(where);
                    ForListParametrs(paramwhere, (string nameparam, object valparam) =>
                    {
                        SqlParameter pr = new SqlParameter(nameparam, (dynamic)valparam);
                        command.Parameters.Add(pr);
                    });
                }


                command.CommandText = "update [" + NameTable + "] set " + strbuild.ToString();
                command.Connection = connect;
                command.Transaction = transact;
                command.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// Установка одной строки в таблице по заданному условию
        /// </summary>
        /// <param name="NameTable">Название таблицы</param>
        /// <param name="KeyField">Ключ таблицы(первияный ключ)</param>
        /// <param name="setdats">Данные которые нужно установит</param>
        /// <param name="where">строка условия по которому ищут наличе той нужной строки</param>
        /// <param name="whereprs">Параметры для условия список формата название параемтра в where и его значение</param>
        /// <param name="connect">Открытое подключение к БД</param>
        /// <param name="transact">Транзакция</param>
        /// <returns>Значение первичного ключа у строи</returns>
        public static object SetOne(string NameTable, string KeyField, object[] setdats, string where, object[] whereprs, SqlConnection connect, SqlTransaction transact = null)
        {
            object result = null;

            string sel = "select top 1 " + KeyField + " from " + NameTable;
            if (!string.IsNullOrEmpty(where) && !string.IsNullOrWhiteSpace(where))
            {
                sel = sel + " where " + where;
            }

            object res = null;
            using (var comm = CreateCommand(sel, whereprs))
            {
                comm.Connection = connect;
                comm.Transaction = transact;
                res = comm.ExecuteScalar();
            }
            //Нужно пройти по setdats и исключить для Update поле ключа если оно есть
            var sdats = ListExceptKey(setdats, KeyField);

            if (res != null && res != System.DBNull.Value)
            {
                UpdateCommand(NameTable, sdats.ToArray(), KeyField + "=@prwh1", new object[] { "@prwh1", (dynamic)res }, connect, transact);
            }
            else
            {
                result = InsertCommand(NameTable, connect, transact, setdats);
            }

            return result;
        }
        /// <summary>
        /// Удаляем строки из БД по условию
        /// </summary>
        /// <param name="NameTable">Название таблицы</param>
        /// <param name="where">Строка условия</param>
        /// <param name="paramwhere">Список параметров для условия</param>
        /// <param name="connect">Подлюучение к БД</param>
        /// <param name="transact">Транзакция</param>
        public static void DeleteCommand(string NameTable, string where, object[] paramwhere, SqlConnection connect, SqlTransaction transact = null)
        {
            string commstr = "delete from " + NameTable;
            if (!string.IsNullOrEmpty(where) && !string.IsNullOrWhiteSpace(where))
            {
                commstr = commstr + " where " + where;
                using (var scommand = CreateCommand(commstr, paramwhere))
                {
                    scommand.Connection = connect;
                    scommand.Transaction = transact;
                    scommand.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// Устанавливает коллекцию
        /// </summary>
        /// <param name="NameTable">Название таблицы</param>
        /// <param name="PrimKeyField">Название поля первичного ключа</param>
        /// <param name="PrimKeyProperty">Название свойства в объекта первичного ключа</param>
        /// <param name="setdats">Список объектов которые нужно установить</param>
        /// <param name="maps">Описание маппироания объекта на поля в таблице</param>
        /// <param name="where">Строка условия выбора текущей коллекции из БД</param>
        /// <param name="whereprs">Значения параметров для условия в формает название параметра значение параметра</param>
        /// <param name="connect">Подлючение к БД</param>
        /// <param name="transact">Транзвакция</param>
        /// <returns>Строка ошибки или если нет ошиби то пустая строка</returns>
        public static string SetCollect(string NameTable, string PrimKeyField, string PrimKeyProperty, List<object> setdats, List<Map> maps, string where, object[] whereprs, SqlConnection connect, SqlTransaction transact = null)
        {
            string error = string.Empty;
            //Список из БД по заданному условию
            #region Берем текущие данные из БД
            SortedList<MultiKey, DataRow> basesort = new SortedList<MultiKey, DataRow>();
            StringBuilder strbuild = new StringBuilder();
            strbuild.Append("select ");
            bool isfirst = true;
            foreach (var item in maps)
            {
                if (isfirst)
                {
                    strbuild.Append(item.Field); isfirst = false;
                }
                else
                {
                    strbuild.Append(","); strbuild.Append(item.Field);
                }
            }
            strbuild.Append(" from "); strbuild.Append(NameTable);
            if (!string.IsNullOrEmpty(where) && !string.IsNullOrWhiteSpace(where))
            {
                strbuild.Append(where);
            }
            DataTable table = new DataTable();
            using (SqlDataAdapter adap = new SqlDataAdapter())
            {
                using (SqlCommand comm = CreateCommand(strbuild.ToString(), whereprs))
                {
                    adap.SelectCommand = comm;
                    adap.Fill(table);
                }
            }
            var keys = from t in maps where t.IsKey select t;
            foreach (DataRow row in table.Rows)
            {
                bool isadd = true;
                MultiKey key = new MultiKey();
                try
                {
                    foreach (var fkey in keys)
                    {
                        IComparable comp = row[fkey.Field] as IComparable;
                        if (comp != null)
                        {
                            key.AddKey(comp);
                        }
                        else
                        {
                            isadd = false;
                            break;
                        }
                    }
                }
                catch
                {
                    isadd = false;
                    break;
                }
                if (isadd)
                {
                    if (!basesort.ContainsKey(key))
                    {
                        basesort.Add(key, row);
                    }
                }
            }
            #endregion Берем текущие данные из БД
            //Текущие данные которые нужно установить сортируем
            #region Сортируем данные которые нужно установить
            SortedList<MultiKey, object> sortset = new SortedList<MultiKey, object>();
            foreach (var data in setdats)
            {
                bool isadd = true;
                MultiKey key = new MultiKey();
                try
                {
                    foreach (var fkey in keys)
                    {
                        IComparable comp = data.GetValue(fkey.Property) as IComparable;
                        if (comp != null)
                        {
                            key.AddKey(comp);
                        }
                        else
                        {
                            isadd = false;
                            break;
                        }
                    }
                }
                catch
                {
                    isadd = false;
                    break;
                }
                if (isadd)
                {
                    if (!sortset.ContainsKey(key))
                    {
                        sortset.Add(key, data);
                    }
                }
            }
            #endregion Сортируем данные которые нужно установить
            //Проходим по спиcку который нужно установить и пытаемся найти им соответсвеи в 
            //списке из БД если находим то обновляем если нет то добавляем
            SqlTransaction curtransact = transact;
            bool isCommit = false;
            if (curtransact == null)
            {
                curtransact = connect.BeginTransaction();
                isCommit = true;
            }
            try
            {
                foreach (var item in sortset)
                {
                    List<object> lst = new List<object>();
                    foreach (var fl in maps)
                    {
                        object dt = item.Value.GetValue(fl.Property);
                        lst.Add(fl.Field); lst.Add(dt);
                    }

                    if (basesort.ContainsKey(item.Key))
                    {
                        //Update
                        object key = item.Value.GetValue(PrimKeyProperty);
                        string wh = PrimKeyField + "=@prkey1";
                        UpdateCommand(NameTable, lst.ToArray(), wh, new object[] { "@prkey1", key }, connect, curtransact);
                    }
                    else
                    {
                        //Insert
                        InsertCommand(NameTable, connect, curtransact, lst);
                    }
                }
                //Проходим по списку из БД и пытаемся найти ему соответсвеи в устанавливаемых
                //если не находим то удаляем
                foreach (var itwm in basesort)
                {
                    if (!sortset.ContainsKey(itwm.Key))
                    {
                        object key = itwm.Value[PrimKeyField];
                        string wh = PrimKeyField + "=@prkey1";
                        DeleteCommand(NameTable, wh, new object[] { "@prkey1", key }, connect, curtransact);
                    }
                }
                if (isCommit)
                {
                    curtransact.Commit();
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
                if (isCommit)
                {
                    curtransact.Rollback();
                }
            }
            finally
            {
                if (isCommit && curtransact != null)
                {
                    curtransact.Dispose();
                }
            }
            return error;
        }

        /// <summary>
        /// Выполняет запрос на выборку данных и заполняет таблицу
        /// </summary>
        /// <param name="query">Запрос на выборку</param>
        /// <param name="dats">Параметры запроса в формате название параметра, знечение</param>
        /// <returns>Результирующая таблица</returns>
        public static DataTable FillTable(string query, SqlConnection connect, SqlTransaction transact, params object[] dats)
        {
            //try
            //{
                DataTable table = new DataTable();
                using (SqlDataAdapter adap = new SqlDataAdapter())
                {
                    using (SqlCommand comsel = CreateCommand(query, dats))
                    {
                        comsel.Connection = connect;
                        comsel.Transaction = transact;
                        adap.SelectCommand = comsel;
                        adap.Fill(table);
                    }
                }
                return table;
            //}
            //catch (Exception exx)
            //{
            //    return null;
            //}
        }

        public static DataRow FillRow(string query,SqlConnection connect, SqlTransaction transact, params object[] dats)
        {
            DataRow row = null;
            DataTable table = new DataTable();
            using (SqlDataAdapter adap = new SqlDataAdapter())
            {
                using (SqlCommand comsel = CreateCommand(query, dats))
                {
                    comsel.Connection = connect;
                    comsel.Transaction = transact;
                    adap.SelectCommand = comsel;
                    adap.Fill(table);
                    if(table!=null && table.Rows!=null && table.Rows.Count>0)
                    {
                        row = table.Rows[0];
                    }
                }
            }
            return row;
        }

        public static bool HasRow(string NameTable, string TestField, SqlConnection connect, SqlTransaction transact, params object[] dats)
        {
            bool result = false;
            using (SqlCommand comm = new SqlCommand())
            {
                StringBuilder strbuild = new StringBuilder();
                strbuild.Append("select Top 1 " + TestField + " from " + NameTable);
                bool isfirst = true;
                ForListParametrs(dats, (string field, object data) =>
                {
                    if (isfirst)
                    {
                        strbuild.Append(" where " + field + "=@" + field);
                        comm.Parameters.Add(new SqlParameter("@" + field, (dynamic)data));
                    }
                });
                comm.CommandText = strbuild.ToString();
                object res = comm.ExecuteScalar();
                if (res != null && res != System.DBNull.Value)
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// Идет по списку праматеров 
        /// </summary>
        /// <param name="dats">Спиок формата 1 - название параметра 2 значение параметра</param>
        /// <param name="action">Действие которое вызывется и ему передается название параметра и его значение</param>
        private static void ForListParametrs(object[] dats, Action<string, object> action)
        {
            if (dats != null && dats.Length > 0)
            {
                int index = 0;
                bool flag = true;
                while (flag)
                {
                    if (index < dats.Length)
                    {
                        string fl = Convert.ToString(dats[index]);
                        index++;
                        if (fl != null)
                        {
                            if (index < dats.Length)
                            {
                                object dt = dats[index];
                                index++;
                                action(fl, dt);
                            }
                        }
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }
        }

        private static List<object> ListExceptKey(object[] dats,string KeyField)
        {
            List<object> result = new List<object>();

            if (dats != null && dats.Length > 0)
            {
                int index = 0;
                bool flag = true;
                while (flag)
                {
                    if (index < dats.Length)
                    {
                        string fl = Convert.ToString(dats[index]);
                        index++;
                        if (fl != null)
                        {
                            if (index < dats.Length)
                            {
                                object dt = dats[index];
                                index++;
                                if (!fl.Equals(KeyField))
                                {
                                    result.Add(fl);
                                    result.Add(dt);
                                }
                            }
                        }
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Формироание строки
        /// </summary>
        /// <param name="prefix">Префикс перед каждой строкой</param>
        /// <param name="suffix">Суффикс в конце каждой строки</param>
        /// <param name="beetwen">Строка которая вставляется между</param>
        /// <param name="start">Строка которая ставиться в самом начале</param>
        /// <param name="end">Строка которая тсавиться в самом конце</param>
        /// <param name="Dats">Список строк</param>
        /// <returns></returns>
        public static string FormatString(string prefix, string suffix, string beetwen, string start, string end, params string[] Dats)
        {
            StringBuilder str = new StringBuilder();
            if (!string.IsNullOrEmpty(start) && !string.IsNullOrWhiteSpace(start))
            {
                str.Append(start);
            }
            if (Dats != null && Dats.Length > 0)
            {
                bool isfirst = true;
                foreach (var item in Dats)
                {
                    if (!string.IsNullOrEmpty(item) && !string.IsNullOrWhiteSpace(item))
                    {
                        if (!isfirst)
                        {
                            if (!string.IsNullOrEmpty(beetwen) && !string.IsNullOrWhiteSpace(beetwen))
                            {
                                str.Append(beetwen);
                            }
                        }

                        if (!string.IsNullOrEmpty(prefix) && !string.IsNullOrWhiteSpace(prefix))
                        {
                            str.Append(prefix);
                        }

                        str.Append(item);

                        if (!string.IsNullOrEmpty(suffix) && !string.IsNullOrWhiteSpace(suffix))
                        {
                            str.Append(suffix);
                        }
                        isfirst = false;
                    }
                }
            }
            return str.ToString();
        }

        /// <summary>
        /// Соединение всех строк
        /// </summary>
        /// <param name="lines">Список строк</param>
        /// <returns>Объединенная строка</returns>
        public static string MergeString(params string[] lines)
        {
            string result = string.Empty;
            if (lines != null && lines.Length > 0)
            {
                StringBuilder str = new StringBuilder();
                foreach (var line in lines)
                {
                    str.Append(line);
                }
                result = str.ToString();
            }
            return result;
        }

        public static object ExecuteScalar(string query, SqlConnection connect, SqlTransaction transact, params object[] dats)
        {
            object result = null;
            using (SqlCommand comsel = CreateCommand(query, dats))
            {
                comsel.Connection = connect;
                comsel.Transaction = transact;
                var res = comsel.ExecuteScalar();
                if(res!=null && res!=System.DBNull.Value)
                {
                    result = res;
                }
            }
            return result;
        }

        public static void ExecuteNonQueryScript(string namefile, SqlConnection connect, SqlTransaction transact, params object[] dats)
        {
            string query = System.IO.File.ReadAllText(namefile);
            using (var command = CreateCommand(query, dats))
            {
                command.Connection = connect;
                command.Transaction = transact;
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Выполнение запроса без ответа
        /// </summary>
        /// <param name="query">Строка запроса</param>
        /// <param name="connect">Подключение к БД</param>
        /// <param name="transact">Транзакция</param>
        /// <param name="dats">Список параметров в формате название параемтра значение</param>
        public static void ExecuteNonQuery(string query,SqlConnection connect,SqlTransaction transact, params object[] dats)
        {
            using (var command = CreateCommand(query, dats))
            {
                command.Connection = connect;
                command.Transaction = transact;
                command.ExecuteNonQuery();
            }
        }

        public static void SetOneRow(string NameTable, string keyfield, SqlConnection connect, SqlTransaction transact, params object[] dats)
        {
            string qr = MergeString(Sql.Sel, Sql.top1, keyfield, Sql.from, NameTable);
            var res = ExecuteScalar(qr, connect, transact);
            if (res != null && res != System.DBNull.Value)
            {
                UpdateCommand(NameTable, dats, string.Empty, null, connect, transact);
            }
            else
            {
                InsertCommand(NameTable, connect, transact, dats.ToArray());
            }
        }

        public static SqlConnection CreateConnectFromRegitry()
        {
            return new SqlConnection(RegistryRead());
            /*
            SqlConnection connect = null;
            RegistryKey currRegistryKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RPS");

            if (currRegistryKey.GetValue("conlocalReg") != null)
            {

                string conlocalReg = currRegistryKey.GetValue("conlocalReg").ToString();
                if(!string.IsNullOrEmpty(conlocalReg) && !string.IsNullOrWhiteSpace(conlocalReg))
                {
                    Rps.Crypto.CryptoDecrypto cryp = new Rps.Crypto.CryptoDecrypto();
                    string constr = cryp.Decrypto(conlocalReg);
                    if(!string.IsNullOrEmpty(constr) && !string.IsNullOrWhiteSpace(constr))
                    {
                        connect = new SqlConnection(constr);
                    }
                }
            }
            return connect;
            */
        }

        public static string RegistryRead()
        {
            string constr = string.Empty;
            Rps.Crypto.CryptoDecrypto cryp = new Rps.Crypto.CryptoDecrypto();
            RegistryKey currRegistryKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RPS");
            if (currRegistryKey.GetValue("conlocalReg") != null)
            {
                var conlocalReg = currRegistryKey.GetValue("conlocalReg").ToString();
                string conlocal = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(conlocalReg));
                if (conlocal == null)
                {
                    SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conlocalReg);
                    conlocal = conlocalReg;
                    string Password = cb.Password;
                    string Password1 = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(Password));
                    if (Password1 == null)
                    {
                        Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                        cb.Password = Password;
                        //currRegistryKey.SetValue("conlocalReg", cb.ToString());
                    }
                    else
                    {
                        cb.Password = Password1;
                        conlocal = cb.ToString();
                    }
                }
                constr = conlocal;

            }
            return constr;
        }

        public static string GetStringConnectFromRegistry()
        {
            string constr = string.Empty;
            RegistryKey currRegistryKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RPS");

            if (currRegistryKey.GetValue("conlocalReg") != null)
            {

                string conlocalReg = currRegistryKey.GetValue("conlocalReg").ToString();
                if (!string.IsNullOrEmpty(conlocalReg) && !string.IsNullOrWhiteSpace(conlocalReg))
                {
                    Rps.Crypto.CryptoDecrypto cryp = new Rps.Crypto.CryptoDecrypto();
                    constr = cryp.Decrypto(conlocalReg);
                }
            }
            return constr;
        }

        public static void SaveConnectionFromConfigToRegistry(string keyconnect)
        {
            var hasconstr = (from t in ConfigurationManager.ConnectionStrings.OfType<ConnectionStringSettings>()
                            where t.Name == keyconnect
                            select t).FirstOrDefault();
            if (hasconstr != null)
            {
                string connect = hasconstr.ConnectionString;
                if (!string.IsNullOrEmpty(connect) && !string.IsNullOrWhiteSpace(connect))
                {
                    Rps.Crypto.CryptoDecrypto cryp = new Rps.Crypto.CryptoDecrypto();
                    RegistryKey currRegistryKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RPS");
                    byte[] encrypted = cryp.EncryptStringToBytes_Aes(connect);
                    currRegistryKey.SetValue("conlocalReg", cryp.ByteAToString(encrypted));

                    string section = "connectionStrings";
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    // Get the section to unprotect.
                    ConfigurationSection Section = config.GetSection(section);
                    var conlocalA = config.ConnectionStrings.ConnectionStrings[keyconnect];
                    conlocalA.ConnectionString = string.Empty;

                    ConfigurationManager.RefreshSection("connectionStrings");
                    Section.SectionInformation.ForceSave = true;
                    config.Save(ConfigurationSaveMode.Full, true);
                }
            }
        }

        /*
                 void ConfToReestr()
        {
            ToLog(2, "Cashier: ConfToReestr");
            #region // проверка, есть ли данные в конфиге/реестре
            //Берем строки соединения для синхронизатора
            string section = "connectionStrings";
            Configuration config =
            ConfigurationManager.OpenExeConfiguration(
            ConfigurationUserLevel.None);

            // Get the section to unprotect.
            ConfigurationSection Section =
                config.GetSection(section);

            string conlocalReg;

            // строка conlocal
            conlocal = ConfigurationManager.ConnectionStrings["DeviceConnect"].ConnectionString;
            if (conlocal == conlocal0 | conlocal == null)
            {
                if (currRegistryKey.GetValue("conlocalReg") != null)
                {

                    conlocalReg = currRegistryKey.GetValue("conlocalReg").ToString();
                    byte[] encrypted = StringToByteA(conlocalReg);
                    conlocal = DecryptStringFromBytes_Aes(encrypted);
                }
            }
            else
            {
                byte[] encrypted = EncryptStringToBytes_Aes(conlocal);
                currRegistryKey.SetValue("conlocalReg", ByteAToString(encrypted));
            }

            string conserverReg;
            // строка conserver
            conserver = ConfigurationManager.ConnectionStrings["ServerConnect"].ConnectionString;
            if (conserver == conserver0 | conserver == null)
            {
                if (currRegistryKey.GetValue("conserverReg") != null)
                {
                    conserverReg = currRegistryKey.GetValue("conserverReg").ToString();
                    byte[] encrypted = StringToByteA(conserverReg);
                    conserver = DecryptStringFromBytes_Aes(encrypted);
                }
            }
            else
            {
                byte[] encrypted = EncryptStringToBytes_Aes(conserver);
                currRegistryKey.SetValue("conserverReg", ByteAToString(encrypted));
            }

            string EntitiesReg;
            // строка Entities
            EntitiesCS = ConfigurationManager.ConnectionStrings["Entities"].ConnectionString;
            if (EntitiesCS == Entities0 | EntitiesCS == null)
            {
                if (currRegistryKey.GetValue("EntitiesReg") != null)
                {
                    EntitiesReg = currRegistryKey.GetValue("EntitiesReg").ToString();
                    byte[] encrypted = StringToByteA(EntitiesReg);
                    EntitiesCS = DecryptStringFromBytes_Aes(encrypted);
                }
            }
            else
            {
                byte[] encrypted = EncryptStringToBytes_Aes(EntitiesCS);
                currRegistryKey.SetValue("EntitiesReg", ByteAToString(encrypted));
            }

            // сохраняем новый пустой конфиг
            var conlocalA = config.ConnectionStrings.ConnectionStrings["DeviceConnect"];
            conlocalA.ConnectionString = conlocal0;
            var conserverA = config.ConnectionStrings.ConnectionStrings["ServerConnect"];
            conserverA.ConnectionString = conserver0;
            var EntitiesA = config.ConnectionStrings.ConnectionStrings["Entities"];
            EntitiesA.ConnectionString = Entities0;

            ConfigurationManager.RefreshSection("connectionStrings");
            Section.SectionInformation.ForceSave = true;
            config.Save(ConfigurationSaveMode.Full, true);

            EntitiesA.ConnectionString = EntitiesCS;
            Entities.EntitiesCS = EntitiesA.ToString();
            #endregion
        
        */
    }

    public class Map
    {
        /// <summary>
        /// Название свойства в обекте
        /// </summary>
        public string Property { get; set; }
        /// <summary>
        /// Название поля в БД
        /// </summary>
        public string Field { get; set; }
        /// <summary>
        /// Является ли данное поле ключом при сравнении
        /// </summary>
        public bool IsKey { get; set; }

        /// <summary>
        /// Соданием маппирования
        /// </summary>
        /// <param name="property">Название свойства в объекте</param>
        /// <param name="field">Название поля в БД</param>
        /// <param name="iskey">Флаг является ли ключом при сравнении</param>
        public Map(string property, string field, bool iskey = false)
        {
            this.Property = property;
            this.Field = field;
            this.IsKey = iskey;
        }
    }

    public class MultiKey : IComparable, IComparable<MultiKey>
    {
        private List<IComparable> keys = new List<IComparable>();

        public int CompareTo(MultiKey other)
        {
            int result = -1;
            if (this.keys.Count == other.keys.Count)
            {
                for (int i = 0; i < this.keys.Count; i++)
                {
                    result = this.keys[i].CompareTo(other.keys[i]);
                    if (result != 0)
                    {
                        break;
                    }
                }
            }
            return result;
        }

        public int CompareTo(object obj)
        {
            int result = -1;
            MultiKey testobj = obj as MultiKey;
            if (testobj != null)
            {
                result = CompareTo(testobj);
            }
            return result;
        }

        public void AddKey(IComparable compare)
        {
            keys.Add(compare);
        }
    }

    public static class ReflectionWork
    {
        /// <summary>
        /// Функция возвращающая значение в заданном свойстве
        /// </summary>
        /// <param name="NameField">Название свойства если оно вложеннное то передвать через точку</param>
        /// <param name="from">Объект у которого брать</param>
        /// <returns>Значение свойства</returns>
        public static object GetValue(this object from, string NameField)
        {
            object result = null;
            try
            {
                if (NameField != null && !NameField.Trim().Equals(string.Empty))
                {
                    Type currenttype = from.GetType();
                    object currentobject = from;
                    string[] paths = NameField.Split('.');
                    for (int i = 0; i < paths.Length; i++)
                    {
                        string currentprop = paths[i];
                        PropertyInfo propinfo = currenttype.GetProperty(currentprop, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                        currentobject = currenttype.InvokeMember(currentprop,
                             BindingFlags.Public | BindingFlags.NonPublic |
                             BindingFlags.Instance | BindingFlags.GetField | BindingFlags.GetProperty, null, currentobject, null);
                        if (currentobject != null)
                        {
                            currenttype = currentobject.GetType();
                        }
                        else
                        {
                            break;
                        }
                    }
                    result = currentobject;
                }
            }
            catch { }
            return result;
        }

        /// <summary>
        /// Функция которая устанавливает значение у заданного свойства
        /// </summary>
        /// <param name="NameField">Название свойства если оно вложеннное то передвать через точку</param>
        /// <param name="DataObject">Данные которые необходимо установить</param>
        /// <param name="ContainObject">Объект у которого необходимо установть эти данные</param>
        public static void SetValue(this object ContainObject, string NameField, object DataObject)
        {
            Type currenttype = ContainObject.GetType();
            object currentobject = ContainObject;
            string[] paths = NameField.Split('.');
            string endNameProp = NameField;
            //foreach (string currentprop in paths)
            for (int i = 0; i < paths.Length; i++)
            {
                string currentprop = paths[i];
                if (i == paths.Length - 1)
                {
                    currenttype.InvokeMember(currentprop,
                                             BindingFlags.Public | BindingFlags.NonPublic |
                                             BindingFlags.Instance | BindingFlags.SetField |
                                              BindingFlags.SetProperty, null,
                                              currentobject, new object[] { DataObject });
                }
                else
                {
                    //PropertyInfo propinfo = currenttype.GetProperty(currentprop, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    currentobject = currenttype.InvokeMember(currentprop,
                         BindingFlags.Public | BindingFlags.NonPublic |
                         BindingFlags.Instance | BindingFlags.GetField | BindingFlags.GetProperty, null, currentobject, null);
                    currenttype = currentobject.GetType();
                }
            }
        }

        /// <summary>
        /// Проверяет наличе свойства
        /// у заданного типа
        /// </summary>
        /// <param name="TpObject">Тип у которого проверяем</param>
        /// <param name="NameProperty">Название свойства</param>
        /// <returns>true-есть свойства false-нет свойства</returns>
        public static bool HasProperty(this Type TpObject, string NameProperty)
        {
            bool find = false;
            Type currenttype = TpObject;
            string[] paths = NameProperty.Split('.');
            string endNameProp = NameProperty;
            for (int i = 0; i < paths.Length; i++)
            {
                string currentprop = paths[i];
                PropertyInfo propinfo = currenttype.GetProperty(currentprop, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                currenttype = currenttype.BaseType;
                if (i == paths.Length - 1)
                {
                    if (propinfo != null)
                    {
                        find = true;
                    }
                }
            }
            return find;
        }
    }
}
