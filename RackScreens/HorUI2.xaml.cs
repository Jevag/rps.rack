﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace RackScreens
{
    /// <summary>
    /// Логика взаимодействия для HorUI2.xaml
    /// </summary>
    public partial class HorUI2 : UserControl, INotifyPropertyChanged
    {
        public string MyMsg;
        public int Mypic;

        private string _idCounter;
        public string IdCounter
        {
            get { return _idCounter; }
            set
            {
                if (value != _idCounter)
                {
                    _idCounter = value;
                    OnPropertyChanged("IdCounter");
                }
            }
        }

        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            var handler = System.Threading.Interlocked.CompareExchange(ref PropertyChanged, null, null);
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion


        public string QRCodeString { get; set; }

        public HorUI2()
        {
            InitializeComponent();
            lblRPS1.SetBinding(ContentProperty, new Binding("IdCounter"));
            DataContext = this;
        }

        //свой таймер сделать для языка...
        DispatcherTimer DTTimer;

        public string Lang { get; set; }

        public void ShowMe(string Text1, string Text2, bool isText2, bool isPicture, int picIndex)
        {
            if (picIndex != 100)
            {
                viewMain3.Visibility = Visibility.Collapsed;
                MyMsg = Text1;

                if (isText2)
                {
                    Grid.SetRowSpan(viewMain1, 1);
                    Grid.SetRowSpan(viewMain2, 1);
                    txtAddText.Text = Text2;
                    rectStripe.Visibility = Visibility.Visible;
                }
                else
                {
                    Grid.SetRowSpan(viewMain1, 2);
                    Grid.SetRowSpan(viewMain2, 2);
                    txtAddText.Text = "";
                    rectStripe.Visibility = Visibility.Collapsed;
                }

                if (isPicture)
                {
                    Mypic = picIndex;

                    txtMainText1.Text = Text1;
                    viewMain1.Visibility = Visibility.Visible;
                    viewMain2.Visibility = Visibility.Collapsed;

                    switch (picIndex)
                    {
                        case 1:
                            picMain.Source = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\1.png"));
                            picMain.Width = 136;
                            picMain.Height = 137;
                            break;
                        case 2:
                            picMain.Source = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\2.png"));
                            picMain.Width = 139;
                            picMain.Height = 138;
                            break;
                        case 3:
                            picMain.Source = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\3.png"));
                            picMain.Width = 139;
                            picMain.Height = 138;
                            break;
                        case 4:
                            picMain.Source = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\4.png"));
                            picMain.Width = 158;
                            picMain.Height = 138;
                            break;
                        case 5:
                            picMain.Source = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\5.png"));
                            picMain.Width = 87;
                            picMain.Height = 186;
                            break;
                        case 51:
                            picMain.Source = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\51.png"));
                            picMain.Width = 87;
                            picMain.Height = 186;
                            break;
                        case 6:
                            picMain.Source = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\6.png"));
                            picMain.Width = 148;
                            picMain.Height = 138;
                            break;
                        case 7:
                            picMain.Source = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\7.png"));
                            picMain.Width = 139;
                            picMain.Height = 138;
                            break;
                        case 8:
                            picMain.Source = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\8.png"));
                            picMain.Width = 139;
                            picMain.Height = 138;
                            break;
                        case 9:
                            picMain.Source = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\9.png"));
                            picMain.Width = 139;
                            picMain.Height = 138;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Mypic = 0;

                    txtMainText2.Text = Text1;
                    viewMain1.Visibility = Visibility.Collapsed;
                    viewMain2.Visibility = Visibility.Visible;
                }
            }
            else //показ QR
            {
                txtMainText3.Text = Text1;

                viewMain1.Visibility = Visibility.Collapsed;
                viewMain2.Visibility = Visibility.Collapsed;
                viewMain3.Visibility = Visibility.Visible;

                if (isText2)
                {
                    Grid.SetRowSpan(viewMain3, 1);
                    txtAddText.Text = Text2;
                    rectStripe.Visibility = Visibility.Visible;
                }
                else
                {
                    Grid.SetRowSpan(viewMain3, 2);
                    txtAddText.Text = "";
                    rectStripe.Visibility = Visibility.Collapsed;
                }

                //рисуем QR
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(QRCodeString, QRCodeGenerator.ECCLevel.H);
                XamlQRCode qrCode = new XamlQRCode(qrCodeData);
                DrawingImage qrCodeAsXaml = qrCode.GetGraphic(20);
                picQR.Source = qrCodeAsXaml;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            DTTimer = new DispatcherTimer();
            DTTimer.Tick += DTTimer_Tick;
            DTTimer.Interval = new TimeSpan(0, 0, 0, 1);
            DTTimer.Start();
            //Lang = "zh-Hans";
            //Lang = "ru-RU";
        }

        private void DTTimer_Tick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            try
            {
                //throw new NotImplementedException();
                CultureInfo ci = new CultureInfo(Lang);
                DateTime dt = DateTime.Now;
                if (Lang != "ru-RU")
                {
                    lblDT1.Content = dt.ToString("MMMM-dd-yyyy HH:mm:ss", ci);
                }
                else
                {
                    lblDT1.Content = dt.ToString("dd MMMM yyyy HH:mm:ss", ci);
                }
            }
            catch (Exception ex) { }
        }
    }
}
