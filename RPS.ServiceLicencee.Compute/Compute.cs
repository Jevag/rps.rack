﻿using RPS.ServerLicencee.Share;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RPS.ServiceLicencee.Compute
{
    public class Compute
    {

        public byte[] GetLicencee(SqlConnection connect, string code)
        {
            byte[] data = null;
            string select = "select Top 1 Data from License where SlaveCode='" + code + "'";
            using (SqlCommand command = new SqlCommand(select, connect))
            {
                object res = command.ExecuteScalar();
                if (res != null && res != System.DBNull.Value)
                {
                    data = (byte[])res;
                }
            }
            return data;
        }

        public bool ComputeHash(SqlConnection connect,string code)
        {
            bool result = false;
            var licen = GetLicencee(connect,code);
            string key = code;
            if (licen != null && licen.Length > 0 && !string.IsNullOrEmpty(key) && !string.IsNullOrWhiteSpace(key))
            {
                LicenseeData licdata = LicenseeData.DeSerializable(licen);
                if (licdata.ToDate >= DateTime.Now)
                {
                    RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
                    RSA.FromXmlString(licdata.PublicKey);
                   
                    SHA1Managed Sha1 = new SHA1Managed();
                    byte[] checkinghash = Sha1.ComputeHash(Encoding.UTF8.GetBytes(key));
                    if (RSA.VerifyHash(checkinghash, CryptoConfig.MapNameToOID("SHA1"), licdata.Signature))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            return result;
        }
    }
}
