﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Resources;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CommonClassLibrary;

namespace Communications
{
    public class GPB : CommonBankModule
    {
        #region DllFunctions
        //Вспомогательные функции
        [DllImport("c:\\GCS\\EMVGSSL\\emvgatessl.dll", EntryPoint = "egGetVersion", CallingConvention = CallingConvention.StdCall)]
        private static extern int egGetVersion();
        [DllImport("c:\\GCS\\EMVGSSL\\emvgatessl.dll", EntryPoint = "egGetVersionDescription", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr egGetVersionDescription();
        [DllImport("c:\\GCS\\EMVGSSL\\emvgatessl.dll", EntryPoint = "egGetLastError", CallingConvention = CallingConvention.StdCall)]
        private static extern int egGetLastError(int nIdInst);
        [DllImport("c:\\GCS\\EMVGSSL\\emvgatessl.dll", EntryPoint = "egGetErrorDescription", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr egGetErrorDescription(int nErrCode);
        /*[DllImport("emvgatessl.dll", EntryPoint = "egDlgGetRecNumber", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr egDlgGetRecNumber();*/

        //Основные функции
        [DllImport("c:\\GCS\\EMVGSSL\\emvgatessl.dll", EntryPoint = "egInitInstance", CallingConvention = CallingConvention.StdCall)]
        private static extern int egInitInstance(byte[] pszCfgFileName);
        [DllImport("c:\\GCS\\EMVGSSL\\emvgatessl.dll", EntryPoint = "egReleaseInstance", CallingConvention = CallingConvention.StdCall)]
        private static extern bool egReleaseInstance(int nIdInst);
        [DllImport("c:\\GCS\\EMVGSSL\\emvgatessl.dll", EntryPoint = "egAuthRequest", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr egAuthRequest(int nIdInst, int nProtId, byte[] pRequest);
        [DllImport("c:\\GCS\\EMVGSSL\\emvgatessl.dll", EntryPoint = "egAuthRequestAsync", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr egAuthRequestAsync(int nIdInst, int nProtId, byte[] pRequest);
        [DllImport("c:\\GCS\\EMVGSSL\\emvgatessl.dll", EntryPoint = "egGetAuthReceipt", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr egGetAuthReceipt(int nIdInst);
        [DllImport("c:\\GCS\\EMVGSSL\\emvgatessl.dll", EntryPoint = "egGetAuthResult", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr egGetAuthResult(int nIdInst);
        [DllImport("c:\\GCS\\EMVGSSL\\emvgatessl.dll", EntryPoint = "egGetOpStatus", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr egGetOpStatus(int nIdInst, bool bIsCansel);
        /*
        [DllImport("emvgatessl.dll", EntryPoint = "egMfAuthenticate", CallingConvention = CallingConvention.StdCall)]
        private static extern int egMfAuthenticate(byte nSector, byte KeyType, uint KeyID);
        [DllImport("emvgatessl.dll", EntryPoint = "egMfRead", CallingConvention = CallingConvention.StdCall)]
        private static extern int egMfRead(byte nSector, byte nBlockId, ref byte[] pBuffer, uint nSizeBuffer);
        [DllImport("emvgatessl.dll", EntryPoint = "egMfWrite", CallingConvention = CallingConvention.StdCall)]
        private static extern int egMfWrite(byte nSector, byte nBlockId, ref byte[] pBuffer, uint nSizeBuffer);
        [DllImport("emvgatessl.dll", EntryPoint = "egMfEndTr", CallingConvention = CallingConvention.StdCall)]
        private static extern int egMfEndTr();
        */
        #endregion
        bool run = false;
        Thread StatusThread;
        Thread StatusPoolThread;
        const int nProtId = 15;
        byte[] pszConfig = Encoding.ASCII.GetBytes("C:\\GCS\\EMVGSSL\\emvgate.cfg\0");
        int nSessId;
        string status = "";
        enum BankModuleCommandT
        {
            Unknown,
            Purchase = 1,
            Cancell,
            //Return,
            Verification = 4,
            WorkKey,
            //ReAuthorization=9,
            //ReAuthorizationClose,
            EchoTest = 24,
            TechnicalCancellation = 27,
            //ChequeByNum = 32,
            //ShortReport,
            //LongReport,
            LoadSW = 35,
            LoadParams,
            //LastCheque=42,
            TerminalInfo = 51,
            CleanCache = 56,
            //Revert = 59
        }
        BankModuleCommandT bankModuleCommand = BankModuleCommandT.Unknown;

        string OldStatus;
        public GPB(Log Log)
        {
            log = Log;
            type = BM_TYPE.GPB;
            toLog("Start");
        }
        ErrorCode SetResultCode(int Ret)
        {
            switch (Ret)
            {
                case 0:
                case 3:
                case 20:
                case 959:
                    return ErrorCode.Sucsess;
                case 2:
                    return ErrorCode.AtmPartial;
                case 5:
                case 902:
                case 903:
                case 913:
                case 915:
                case 967:
                case 968:
                    //case 981:
                    return ErrorCode.SysytemError;
                case 10:
                case 957:
                    return ErrorCode.ResponseError;
                case 100:
                case 103:
                case 104:
                case 111:
                case 305:
                case 904:
                case 905:
                case 997:
                    return ErrorCode.CardNotSupported;
                case 101:
                    return ErrorCode.CardIsDelayed;
                case 105:
                    return ErrorCode.CallSecurity1;
                case 106:
                    return ErrorCode.CardIsBlocked;
                case 107:
                    return ErrorCode.CallIssuer2;
                case 109:
                case 118:
                case 119:
                case 120:
                case 121:
                case 123:
                case 914:
                case 955:
                case 960:
                case 965:
                    return ErrorCode.RequestDeniedByBank;
                case 110:
                case 116:
                    return ErrorCode.CardNotEnoughMoney;
                case 117:
                    return ErrorCode.IncorrectPINCode;
                case 125:
                case 127:
                    return ErrorCode.CardIsDamaged;
                case 126:
                case 128:
                case 920:
                    return ErrorCode.ErrorPINCodeHandling;
                case 200:
                    return ErrorCode.CardIsInvalid;
                case 201:
                    return ErrorCode.InvalidChequeNumber;
                case 202:
                    return ErrorCode.TransactionNotFound;
                case 203:
                case 223:
                    return ErrorCode.InvalidSumm;
                case 204:
                case 224:
                    return ErrorCode.InvalidOriginalSumm;
                case 205:
                case 225:
                case 226:
                    return ErrorCode.InvalidRRN;
                case 206:
                case 231:
                case 973:
                    return ErrorCode.InvalidPIN;
                case 208:
                case 228:
                    return ErrorCode.InvalidCVV;
                case 209:
                case 229:
                case 304:
                case 988:
                    return ErrorCode.InvalidCurrency;
                case 210:
                case 227:
                case 230:
                    return ErrorCode.InvalidAuth;
                case 212:
                case 232:
                case 401:
                    return ErrorCode.InvalidCard;
                case 213:
                    return ErrorCode.InvalidCashierCard;
                case 214:
                    return ErrorCode.InvalidManualCard;
                case 215:
                    return ErrorCode.InvalidCardType;
                case 216:
                    return ErrorCode.InvalidOperation;
                case 221:
                    return ErrorCode.ChequeNumberNeeded;
                case 222:
                    return ErrorCode.ErrorFormatTrack;
                case 233:
                case 244:
                case 411:
                    return ErrorCode.CardNotReaded;
                case 234:
                    return ErrorCode.ErrorChipCard;

                case 235:
                    return ErrorCode.IncorrectSumm;
                case 236:
                    return ErrorCode.CancellNotLast;
                case 237:
                    return ErrorCode.InvalidCancell;
                case 238:
                    return ErrorCode.InvalidCardType;
                case 239:
                    return ErrorCode.SomeElseCard;
                //case 240: Ошибка TMSID
                case 241:
                case 303:
                    return ErrorCode.ShiftClaseNeeded;
                //case 242: Не определен скрипт
                //case 243: Обновление неуспешно
                //case 301: Отказ выбора виртуального терминала
                case 302:
                    return ErrorCode.CancellError;
                //case 306: Конфигурация не загружена
                //case 313: Фулбэк запрещен
                //case 402: Транзакция не поддерживается(ошибка конфигурации)
                //case 403: Ошибка настройки точки доступа (ошибка конфигурации)
                //case 404: Ошибка формата ответа хоста.
                case 410:
                case 971:
                    return ErrorCode.ErrorLoadWorkKey;
                case 530:
                    return ErrorCode.AsyncStart;
                case 907:
                case 908:
                    return ErrorCode.BankNotAvailable;
                case 909:
                case 910:
                    return ErrorCode.BankEmitentNotAvailable;
                case 911:
                    return ErrorCode.PaymentSystemNotAvailable;
                case 912:
                    return ErrorCode.PaymentSystemNotResponse;
                case 923:
                    //case 969:
                    return ErrorCode.RequestTimeOut;
                case 958:
                    //case 999:
                    return ErrorCode.ConnectionError;
                case 963:
                    return ErrorCode.PINCountLimited;
                //case 940 Специальные условия.
                //case 952 Ошибка сверки
                //case 953 Ошибка MAC
                //case 954 Ошибка переполнения памяти
                //case 956 Ошибка конфигурации
                //case 961 ПОДПИСЬ НЕ ПРОВЕРЕНА, НЕВЕРНАЯ ПОДПИСЬ
                //case 962 СБОЙ ДАННЫХ ЭМИТЕНТА
                //case 964 ОПЕРАЦИЯ ЗАПРЕЩЕНА
                //case 966 Ошибка POS-терминала.
                //case 970 Ошибка COM-порта.
                //case 971 Ошибка получения рабочего ключа.
                //case 972 Ошибка формирования чека.
                //case 974 Ошибка чтения Track2.
                case 975:
                    return ErrorCode.SessionClosed;
                case 976:
                    return ErrorCode.SessionOpened;
                //case 977 Отказ от продолжения.
                case 978:
                case 996:
                    return ErrorCode.ProcessingError;
                //case 979 Существуют отложенные транзакции.
                //case 980 ККМ не подтвердила операцию.
                //case 982 Ошибка LRC.
                //case 983 Рассинхронизация запроса и ответа.
                //case 984 Ошибка файловой системы.
                //case 985 Ошибка в формате запроса
                //case 986 Запрещенный BIN
                //case 987 Запрещенный Сервис-Код
                //case 989 Ручной ввод запрещен
                //case 990 Неверная длина номера карты
                //case 991 Повторный запрос отмены
                //case 992 Нет отменяемой операции
                //case 993 Нет журнала продаж
                //case 994 Нет ответа от ККМ
                //case 995 Ошибка соединения по X25
                //case 998 Терминал не зарегистрирован в системе.

                case 1000:
                    return ErrorCode.OtherError1000;

                default:
                    return ErrorCode.OtherError;
            }
        }
        public override bool Open()
        {
            toLog("Start");
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Open: busy ");
                    resultCode = ErrorCode.OtherError;
                    return false;
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                lock (locker)
                {
                    int nRet = 0;
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Open InitInstance");
                    egReleaseInstance(nSessId);
                    nSessId = 0;
                    nSessId = egInitInstance(pszConfig);
                    if (nSessId > 0)
                    {
                        egReleaseInstance(nSessId);
                        nSessId = 0;
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Open InitInstance Success");
                        resultCode = ErrorCode.Sucsess;
                        resultDescription = "";
                        deviceEnabled = true;

                        versionDescription = GetVersionDescription();

                        if (StatusThread == null || !StatusThread.IsAlive)
                        {
                            StatusThread = new Thread(PoolThread);
                            StatusThread.Name = "BankModuleStatusThread";
                            StatusThread.IsBackground = true;
                            run = true;
                            StatusThread.Start();
                        }
                        //#region PoolThreadGPBBackgroundWorker.Init
                        //run = true;
                        //PoolThreadGPBBackgroundWorker = new BackgroundWorker();
                        //InitializePoolThreadGPBBackgroundWorker();
                        //PoolThreadGPBBackgroundWorker.RunWorkerAsync();
                        //#endregion PoolThreadGPBBackgroundWorker.Init


                        StatusPoolThread = new Thread(PoolThreadCheck);
                        StatusPoolThread.Name = "BankModulePoolThreadCheck";
                        StatusPoolThread.IsBackground = true;
                        StatusPoolThread.Start();

                        busy = false;
                        return true;
                    }
                    else
                    {
                        deviceEnabled = false;
                        //Это ошибка в процессе обработки запроса!
                        nRet = GetLastError();
                        resultCode = SetResultCode(nRet);
                        if (nRet > 0)
                        {
                            resultDescription = GetErrorDescription(nRet);
                        }
                        egReleaseInstance(nSessId);
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Open InitInstance resultCode=" + nRet.ToString() + " resultDescription=" + resultDescription);
                        busy = false;
                        return false;
                    }
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Open: Exception " + exc.ToString());
                resultCode = ErrorCode.OtherError;
                busy = false;
                return false;
            }
        }
        public override bool Close()
        {

            toLog("Start");

            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Close: busy ");
                    resultCode = ErrorCode.OtherError;
                    return false;
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                lock (locker)
                {
                    run = false;
                    egReleaseInstance(nSessId);
                    nSessId = 0;
                    deviceEnabled = false;
                    busy = false;
                    return true;
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Close: Exception " + exc.ToString());
                resultCode = ErrorCode.OtherError;
                busy = false;
                return false;
            }
        }
        public override bool StartSession()
        {
            nSessId = egInitInstance(pszConfig);
            //int nRet = GetLastError();
            //resultCode = SetResultCode(nRet);
            //resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
            //if (resultCode != ErrorCode.Sucsess)
            //{
            //    if (log != null)
            //        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.StartSession resultDescription=" + resultDescription);
            //}
            return (nSessId > 0);
        }
        public override bool StopSession()
        {
            if (egReleaseInstance(nSessId))
            {
                nSessId = 0;
                return true;

            }
            else
            {
                return false;
            }
        }
        /*public override int GetVersion()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetVersion");
            lock (locker)
            {
                return egGetVersion();
            }
        }*/
        public override string GetVersionDescription()
        {
            lock (locker)
            {
                StopSession();
                StartSession();
                IntPtr pBuff = IntPtr.Zero;
                pBuff = Marshal.AllocHGlobal(100);
                /*unsafe
                {
                    byte* src = (byte*)pBuff.ToPointer();

                    for (int i = 0; i < 100; i++)
                    {
                        *src++ = 0;
                    }
                }*/
                IntPtr p = egGetVersionDescription();
                string str = Marshal.PtrToStringAnsi(p);
                Marshal.FreeHGlobal(pBuff);
                versionDescription = str;
                StopSession();
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetVersionDescription VersionDescription=" + str);
                return str;
            }
        }
        public override int GetLastError()
        {
            //lock (locker)
            {
                rawResultCode = egGetLastError(nSessId);
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetLastError rawResultCode=" + rawResultCode.ToString());
                return rawResultCode;
            }
        }

        public string GetErrorDescription(int nRet)
        {
            //lock (locker)
            {
                IntPtr pBuff = IntPtr.Zero;
                pBuff = Marshal.AllocHGlobal(255);
                IntPtr p = egGetErrorDescription(nRet);
                string str = Marshal.PtrToStringAnsi(p);
                Marshal.FreeHGlobal(pBuff);
                //Marshal.FreeHGlobal(p);
                resultDescription = str;
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetErrorDescription res=" + str);
                return str;
            }
        }
        string GetAuthReceipt()
        {
            //lock (locker)
            {
                if (deviceEnabled)
                {
                    IntPtr pBuff = IntPtr.Zero;
                    pBuff = Marshal.AllocHGlobal(1024);
                    IntPtr p = egGetAuthReceipt(nSessId);
                    string str = Marshal.PtrToStringAnsi(p);
                    Marshal.FreeHGlobal(pBuff);
                    //Marshal.FreeHGlobal(p);
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetAuthReceipt AuthReceipt=" + str);
                    return str;
                }
                return "";
            }
        }
        string GetAuthResult()
        {
            //lock (locker)
            {
                if (deviceEnabled)
                {
                    IntPtr pBuff = IntPtr.Zero;
                    pBuff = Marshal.AllocHGlobal(1024);
                    IntPtr p = egGetAuthResult(nSessId);
                    string str = Marshal.PtrToStringAnsi(p);
                    Marshal.FreeHGlobal(pBuff);
                    //Marshal.FreeHGlobal(p);
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetAuthResult AuthResult=" + str);
                    return str;
                }
                return "";
            }
        }
        string AuthRequestAsync(string Request)
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.AuthRequestAsync");
            //lock (locker)
            {
                if (deviceEnabled)
                {
                    byte[] bRequest = Encoding.ASCII.GetBytes(Request + "\0");
                    IntPtr p;
                    //IntPtr p = IntPtr.Zero;
                    //p = Marshal.AllocHGlobal(1024);
                    IntPtr pBuff = IntPtr.Zero;
                    pBuff = Marshal.AllocHGlobal(1024);
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.AuthRequestAsync nSessId="
                            + nSessId.ToString() + ", nProtId=" + nProtId.ToString() + ", Request=" + Request);
                    string str = "";
                    try
                    {
                        p = egAuthRequestAsync(nSessId, nProtId, bRequest);
                        if (p != null)
                        {
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.AuthRequestAsync ok, p=" + p.ToString());
                            str = Marshal.PtrToStringAnsi(p);
                        }
                        else
                        {
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.AuthRequestAsync, p=null");
                        }
                    }
                    catch (Exception ee)
                    {
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.AuthRequestAsync Exception=" + ee.ToString());
                    }

                    Marshal.FreeHGlobal(pBuff);
                    //Marshal.FreeHGlobal(p);
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff")
                            + ": GPB: GPB.AuthRequestAsync res size=" + str.Length.ToString()
                            + ", res=" + str);
                    return str;
                }
                return "";
            }
        }
        string AuthRequestOperation(string Request)
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.AuthRequest=" + Request);
            lock (locker)
            {
                if (deviceEnabled)
                {
                    byte[] bRequest = Encoding.ASCII.GetBytes(Request + "\0");
                    IntPtr pBuff = IntPtr.Zero;
                    pBuff = Marshal.AllocHGlobal(1024);
                    IntPtr p = egAuthRequest(nSessId, nProtId, bRequest);
                    string str = Marshal.PtrToStringAnsi(p);
                    //Marshal.FreeHGlobal(p);
                    Marshal.FreeHGlobal(pBuff);
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.AuthRequest res=" + str);
                    return str;
                }
                return "";
            }
        }
        public override string GetOpStatus(bool IsCancel = false)
        {
            int countS = 0;
            while (busy)
            {
                countS++;
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetOpStatus: countS=" + countS.ToString());

                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetOpStatus: busy ");
                    resultCode = ErrorCode.OtherError;
                    return "";
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                lock (locker)
                {
                    if (deviceEnabled)
                    {
                        IntPtr p;
                        IntPtr pBuff = IntPtr.Zero;
                        pBuff = Marshal.AllocHGlobal(1024);
                        //IntPtr p = IntPtr.Zero;
                        //p = Marshal.AllocHGlobal(1024);
                        if (IsCancel && !isCancelled)
                        {
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetOpStatus IsCancel=" + IsCancel.ToString());
                            p = egGetOpStatus(nSessId, true);
                        }
                        else
                        {
                            p = egGetOpStatus(nSessId, false);
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetOpStatus IsCancel=" + IsCancel.ToString()
                                    + ", res=" + Marshal.PtrToStringAnsi(p));
                        }

                        string str = "";
                        str = Marshal.PtrToStringAnsi(p);
                        if (IsCancel && !isCancelled && str != "")
                        {
                            if ((str.Substring(0, 2) == "#0" || (str.Substring(0, 3) != "&0 " && str.Substring(0, 3) != "&3 "
                                && str.Substring(0, 4) != "&20 " && str.Substring(0, 4) != "&959")) && (str.Substring(0, 4) != "&969"))
                                isCancelled = true;
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetOpStatus IsCancel=" + IsCancel.ToString()
                                    + ", isCancelled = true, res =" + str);
                        }
                        //Marshal.FreeHGlobal(p);
                        Marshal.FreeHGlobal(pBuff);
                        if (OldStatus != str)
                        {
                            OldStatus = str;
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetOpStatus IsCancel=" + IsCancel.ToString() + ", res =" + str);
                        }

                        int ercod = egGetLastError(nSessId);
                        //if (ercod > 0)
                        //{
                        string ers = GetErrorDescription(ercod);
                        if (log != null)
                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetOpStatus ErrorDescription=" + ers);
                        //}

                        if (IsCancel == true)//если isCancel=true уже после запуска GetOpStatus(isCancel), то выполним отмену в следующем цикле
                            isCancel = false;
                        busy = false;
                        return str;
                    }
                    if (IsCancel == true)//если isCancel=true уже после запуска GetOpStatus(isCancel), то выполним отмену в следующем цикле
                        isCancel = false;
                    busy = false;
                    return "";
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.GetOpStatus: Exception " + exc.ToString());
                resultCode = ErrorCode.OtherError;
                busy = false;
                isCancel = false;
                return "";
            }
        }

        private bool PurchaseAsync()
        {
            int countS = 0;
            while (busy)
            {
                countS++;
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync: countS=" + countS.ToString());
                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync: busy ");
                    resultCode = ErrorCode.OtherError;
                    return false;
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                resultCode = ErrorCode.AsyncStart;
                isCancelled = false;
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync");
                if (StopSession())
                {
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync.StopSession ok");
                }
                else
                {
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync.StopSession false");
                }
                if (resultCode == ErrorCode.OtherError)
                {
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync.StopSession ErrorCode=OtherError");
                    busy = false;
                    return false;
                }
                if (!StartSession())
                {
                    int nRet = GetLastError();
                    resultCode = SetResultCode(nRet);
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync Error StartSession");
                    busy = false;
                    return false;
                }
                else
                {
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync.StartSession nSessId=" + nSessId.ToString());
                }
                if (resultCode == ErrorCode.OtherError)
                {
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync.StartSession ErrorCode=OtherError");
                    busy = false;
                    return false;
                }
                string Request = KKMNumber.ToString() + " " + ((int)BankModuleCommandT.Purchase).ToString() + " " + ((int)Math.Round((AmountDue * 100), 0)).ToString() + " " + ChequeNumber.ToString();
                string Result = AuthRequestAsync(Request);
                if (Result == "530 START")
                {
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync started");
                    AuthorizationStarted = false;
                    busy = false;
                    return true;
                }
                else
                {
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync Error res=" + Result);
                    busy = false;
                    return false;
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PurchaseAsync: Exception " + exc.ToString());
                resultCode = ErrorCode.OtherError;
                busy = false;
                return false;
            }
        }
        public override bool Purchase()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Purchase");
            cardWasInserted = false;
            cardInserted = false;
            bankModuleCommand = BankModuleCommandT.Purchase;
            CommandStatus = CommandStatusT.Run;

            return true;
        }
        public override bool Cancel(int ChequeNumber, decimal AmountDue)
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Cancel ChequeNumber=" + ChequeNumber.ToString() + " AmountDue=" + AmountDue.ToString());
            lock (locker)
            {
                StopSession();
                StartSession();
                string res = AuthRequestOperation(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.Cancell).ToString() + " " + ((int)Math.Round((AmountDue * 100), 0)).ToString() + " " + ChequeNumber.ToString());
                int nRet = GetLastError();
                resultCode = SetResultCode(nRet);
                if (resultCode != ErrorCode.Sucsess)
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                else
                {
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                    //res = GetAuthResult();
                    AuthResult = "";
                    res = GetAuthReceipt();
                    AuthReceipt = res;
                }
                StopSession();
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Cancel res=" + AuthReceipt);
                return true;
            }
        }
        public override bool Verification()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Verification");
            int countS = 0;
            while (busy)
            {
                countS++;
                if (countS > countSbusyMax)
                {
                    if (log != null)
                        log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Verification: busy ");
                    resultCode = ErrorCode.OtherError;
                    return false;
                }
                Thread.Sleep(50);
            }
            busy = true;
            try
            {
                lock (locker)
                {
                    StopSession();
                    StartSession();
                    string res = AuthRequestOperation(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.Verification).ToString());
                    int nRet = GetLastError();
                    resultCode = SetResultCode(nRet);
                    if (resultCode != ErrorCode.Sucsess)
                        resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                    else
                    {
                        resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                        //res = GetAuthResult();
                        AuthResult = "";
                        res = GetAuthReceipt();
                        AuthReceipt = res;
                    }
                    StopSession();
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Verification res=" + AuthReceipt);
                    busy = false;
                    return true;
                }
            }
            catch (Exception exc)
            {
                if (log != null)
                    log.ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Verification: Exception " + exc.ToString());
                resultCode = ErrorCode.OtherError;
                busy = false;
                return false;
            }
        }
        public override bool WorkKey()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.WorkKey");
            lock (locker)
            {
                StopSession();
                StartSession();
                string res = AuthRequestOperation(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.WorkKey).ToString());
                int nRet = GetLastError();
                resultCode = SetResultCode(nRet);
                if (resultCode != ErrorCode.Sucsess)
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                else
                {
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                    //res = GetAuthResult();
                    AuthResult = "";
                    res = GetAuthReceipt();
                    AuthReceipt = res;
                }
                StopSession();
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.WorkKey res=" + AuthReceipt);
                return true;
            }
        }
        /*public override bool ChequeByNum(int chequeNum)
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.ChequeByNum chequeNum=" + chequeNum.ToString());
            lock (locker)
            {
                string res = AuthRequest(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.ChequeByNum).ToString() + " 0 " + chequeNum.ToString());
                int nRet = GetLastError();
                resultCode = SetResultCode(nRet);
                if (resultCode != ErrorCode.Sucsess)
                    resultDescription = GetErrorDescription(nRet);
                else
                {
                    res = GetAuthResult();
                    AuthResult = res;
                    res = GetAuthReceipt();
                    AuthReceipt = res;
                }
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.ChequeByNum res=" + AuthReceipt);
                return true;
            }
        }*/
        /*public override bool LongReport()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.LongReport");
            lock (locker)
            {
                string res = AuthRequest(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.LongReport).ToString());
                res = GetAuthResult();
                AuthResult = res;
                res = GetAuthReceipt();
                AuthReceipt = res;
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.LongReport res=" + AuthReceipt);
                return true;
            }
        }*/
        /*public override bool ShortReport()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.ShortReport");
            lock (locker)
            {
                string res = AuthRequest(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.ShortReport).ToString());
                res = GetAuthResult();
                AuthResult = res;
                res = GetAuthReceipt();
                AuthReceipt = res;
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.ShortReport res=" + AuthReceipt);
                return true;
            }
        }*/
        public override bool TerminalInfo()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.TerminalInfo");
            lock (locker)
            {
                StopSession();
                StartSession();
                string res = AuthRequestOperation(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.TerminalInfo).ToString());
                int nRet = GetLastError();
                resultCode = SetResultCode(nRet);
                if (resultCode != ErrorCode.Sucsess)
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                else
                {
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                    res = GetAuthResult();
                    AuthResult = res;
                    res = GetAuthReceipt();
                    AuthReceipt = res;
                }
                StopSession();
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.TerminalInfo res=" + AuthReceipt);
                return true;
            }
        }
        public override bool TechnicalCancelation()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.TechnicalCancellation");
            lock (locker)
            {
                isCancelled = false;
                StopSession();
                StartSession();
                string res = AuthRequestOperation(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.TechnicalCancellation).ToString());
                res = GetAuthResult();
                int nRet = GetLastError();
                resultCode = SetResultCode(nRet);
                resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                AuthResult = res;
                res = GetAuthReceipt();
                AuthReceipt = res;
                StopSession();
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.TechnicalCancellation res=" + resultDescription);
                return true;
            }
        }
        public override bool LoadSW()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.LoadSW");
            lock (locker)
            {
                StopSession();
                StartSession();
                string res = AuthRequestOperation(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.LoadSW).ToString());
                int nRet = GetLastError();
                resultCode = SetResultCode(nRet);
                if (resultCode != ErrorCode.Sucsess)
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                else
                {
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                    res = GetAuthResult();
                    AuthResult = res;
                    res = GetAuthReceipt();
                    AuthReceipt = res;
                }
                StopSession();
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.LoadSW res=" + resultDescription);
                return true;
            }
        }
        public override bool LoadParams()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.LoadParams");
            lock (locker)
            {
                StopSession();
                StartSession();
                string res = AuthRequestOperation(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.LoadParams).ToString());
                int nRet = GetLastError();
                resultCode = SetResultCode(nRet);
                if (resultCode != ErrorCode.Sucsess)
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                else
                {
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                    res = GetAuthResult();
                    AuthResult = res;
                    res = GetAuthReceipt();
                    AuthReceipt = res;
                }
                StopSession();
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.LoadParams res=" + resultDescription);
                return true;
            }
        }
        public override bool EchoTest()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.EchoTest");
            lock (locker)
            {
                StopSession();
                StartSession();
                string res = AuthRequestOperation(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.EchoTest).ToString());
                int nRet = GetLastError();
                resultCode = SetResultCode(nRet);
                if (resultCode != ErrorCode.Sucsess)
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                else
                {
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                    res = GetAuthResult();
                    AuthResult = res;
                    res = GetAuthReceipt();
                    AuthReceipt = res;
                }
                StopSession();
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.EchoTest res=" + resultDescription);
                return true;
            }
        }
        public override bool CleanCache()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.CleanCache");
            lock (locker)
            {
                StartSession();
                string res = AuthRequestOperation(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.CleanCache).ToString());
                int nRet = GetLastError();
                resultCode = SetResultCode(nRet);
                if (resultCode != ErrorCode.Sucsess)
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                else
                {
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                    res = GetAuthResult();
                    AuthResult = res;
                    res = GetAuthReceipt();
                    AuthReceipt = res;
                }
                StopSession();
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.CleanCache res=" + resultDescription);
                return true;
            }
        }
        /*public override bool Revert()
        {
            if (log != null)
                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Revert");
            lock (locker)
            {
                StartSession();
                string res = AuthRequestOperation(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.Revert).ToString());
                int nRet = GetLastError();
                resultCode = SetResultCode(nRet);
                if (resultCode != ErrorCode.Sucsess)
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                else
                {
                    resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                    res = GetAuthResult();
                    AuthResult = res;
                    res = GetAuthReceipt();
                    AuthReceipt = res;
                }
                StopSession();
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.Revert res=" + resultDescription);
                return true;
            }
        }*/
        protected internal void PoolThreadCheck()
        {
            try
            {
                int timeCheck = 0;
                while (run)
                {
                    Thread.Sleep(100);
                    if (threadCheck == true) //не зависло
                    {
                        threadCheck = false;
                        timeCheck = 0;
                    }
                    else//завис..
                    {
                        if (timeCheck < 130) // 13сек
                                             //if (timeCheck < 60) // 6сек
                        {
                            timeCheck++;
                        }
                        else
                        {
                            timeCheck = 0;
                            //run = false;
                            //StatusThread.Abort();
                            //Thread.Sleep(50);
                            //Close();
                            //authRequest = "Ошибка таймаута.";
                            resultCode = ErrorCode.OtherError;
                            ////resultCode = ErrorCode.RequestTimeOut;
                            //deviceEnabled = false;
                            if (log != null)
                                log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: GPB.PoolThread висит");

                        }
                    }
                }
            }
            catch { }
        }
        protected internal bool threadCheck = false;

        //BackgroundWorker PoolThreadGPBBackgroundWorker;
        //private void InitializePoolThreadGPBBackgroundWorker()
        //{
        //    PoolThreadGPBBackgroundWorker.DoWork +=
        //        new DoWorkEventHandler(
        //            PoolThreadGPBBackgroundWorker_DoWork);
        //    PoolThreadGPBBackgroundWorker.ProgressChanged +=
        //        new ProgressChangedEventHandler(
        //    PoolThreadGPBBackgroundWorker_StatusChanged);
        //}
        //private void PoolThreadGPBBackgroundWorker_StatusChanged(object sender, ProgressChangedEventArgs e)
        //{
        //}

        //private void PoolThreadGPBBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        //protected internal void PoolThread()
        private void PoolThread()
        {
            try
            {
                int count = 0;
                string res = "";
                string result = "";
                string olds = "";
                while (run)
                {
                    runing = true;
                    Thread.Sleep(50);
                    threadCheck = true;
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: PoolThread_1, CommandStatus=" + CommandStatus
                        + ", bankModuleCommand=" + bankModuleCommand + ", status=" + status);
                    switch (CommandStatus)
                    {
                        case CommandStatusT.Run:
                            #region CommandStatusT.Run
                            switch (bankModuleCommand)
                            {
                                case BankModuleCommandT.Purchase:
                                    CommandStatus = CommandStatusT.Running;
                                    if (!deviceEnabled)
                                        try
                                        {
                                            Open();
                                        }
                                        catch
                                        {
                                            resultCode = ErrorCode.OtherError;
                                        }
                                    if (deviceEnabled)
                                    {
                                        //lock (locker)
                                        {
                                            try
                                            {
                                                if (!PurchaseAsync())
                                                    CommandStatus = CommandStatusT.Completed;
                                                if (log != null)
                                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff")
                                                        + ": GPB: PoolThread, PurchaseAsync: CommandStatus = " + CommandStatus.ToString());
                                            }
                                            catch (Exception ee)
                                            {
                                                if (log != null)
                                                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff")
                                                        + ": GPB: PoolThread, Exception " + ee.ToString());
                                            }
                                            count = 200;        // *300ms
                                        }
                                    }
                                    else
                                        resultCode = ErrorCode.OtherError;
                                    break;
                                default:
                                    break;
                            }
                            #endregion CommandStatusT.Run
                            break;

                        case CommandStatusT.Running:
                            #region CommandStatusT.Running
                            //lock (locker)
                            {
                                status = GetOpStatus(isCancel);
                                _status = status;
                                if (olds != status)
                                {
                                    if (log != null)
                                        log.ToLog(1, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: "
                                            + string.Format("GPB status changed from {0} to {1}", olds, status));
                                }
                                if (status != "")
                                {
                                    switch (status[0])
                                    {
                                        case '#':
                                            switch (status[1])
                                            {
                                                case '0':
                                                    result = status.Substring(3);
                                                    //result = status;
                                                    authRequest = result;
                                                    AuthorizationStarted = false;
                                                    TransactionStarted = false;
                                                    cardInserted = false;
                                                    break;
                                                case '1':
                                                    result = status.Substring(3);
                                                    authRequest = result;
                                                    if (cardWasInserted)
                                                        AuthorizationStarted = true;
                                                    //TransactionStarted = false;
                                                    if ((status.Contains("КАРТА ОБНАРУЖЕНА") || status.Contains("СОЕДИНЕНИЕ")
                                                        || status.Contains("СВЯЗЬ С ПЦ") || status.Contains("ОБРАБОТКА ДАННЫХ")
                                                        || status.Contains("ЗАБЕРИТЕ  КАРТУ"))
                                                        && !cardInserted)
                                                    {
                                                        cardInserted = true;
                                                        cardWasInserted = true;
                                                    }
                                                    if (status.Contains("ОЖИДАЙТЕ, ИДЕТ ОБМЕН С БАНКОМ") && !cardInserted)
                                                    {
                                                        cardInserted = true;
                                                        cardWasInserted = true;
                                                    }
                                                    if (status.Contains("ОЖИДАНИЕ ВВОДА ПИН"))
                                                    {
                                                        if (!cardInserted)
                                                        {
                                                            cardInserted = true;
                                                            cardWasInserted = true;
                                                        }
                                                        if (!TransactionStarted)
                                                            TransactionStarted = true;
                                                    }
                                                    break;
                                                case '2':
                                                    result = status.Substring(3);
                                                    authRequest = result;
                                                    if (!AuthorizationStarted)
                                                        AuthorizationStarted = true;
                                                    if (!TransactionStarted)
                                                        TransactionStarted = true;
                                                    //if (!cardInserted)
                                                    //    cardInserted = true;
                                                    break;
                                                case '3':
                                                    authRequest = "";
                                                    if (!AuthorizationStarted)
                                                        AuthorizationStarted = true;
                                                    if (!TransactionStarted)
                                                        TransactionStarted = true;
                                                    if (cardInserted)
                                                        cardInserted = false;
                                                    break;
                                            }
                                            break;
                                        case '&':
                                            authRequest = "";
                                            cardInserted = false;
                                            resultDescription = status.Substring(1);
                                            if (AuthorizationStarted)
                                                AuthorizationStarted = false;
                                            if (TransactionStarted)
                                                TransactionStarted = false;
                                            if (status.Contains("&0 \"OK\""))
                                            {
                                                cardInserted = true;
                                                cardWasInserted = true;
                                            }
                                            /*int nRet = GetLastError();
                                            resultCode = SetResultCode(nRet);
                                            if (resultCode != ErrorCode.Sucsess)
                                                resultDescription = nRet.ToString() + " " + GetErrorDescription(nRet);
                                            else
                                            {
                                                resultDescription = "";
                                                string res = GetAuthResult();
                                                AuthResult = res;
                                                res = GetAuthReceipt();
                                                AuthReceipt = res;
                                            }*/
                                            string[] parse = resultDescription.Split(' ');
                                            try
                                            {
                                                resultCode = SetResultCode(Convert.ToInt16(parse[0]));
                                            }
                                            catch { resultCode = SetResultCode(1000); }

                                            /*for (int i = 0; i < parse.Count(); i++)
                                            {
                                                if (i == 1)
                                                    resultDescription = parse[i];
                                                else
                                                    resultDescription = " " + parse[i];
                                            }*/

                                            if (resultCode == ErrorCode.Sucsess)
                                            {
                                                res = GetAuthResult();
                                                AuthResult = res;
                                                res = GetAuthReceipt();
                                                AuthReceipt = res;
                                            }
                                            StopSession();
                                            CommandStatus = CommandStatusT.Completed;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                if (olds != status)
                                {
                                    olds = status;
                                }
                            }
                            break;
                        #endregion
                        case CommandStatusT.Completed:
                            if (AuthorizationStarted)
                                AuthorizationStarted = false;
                            //lock (locker)
                            {
                                if (count < 1)
                                {
                                    if (deviceEnabled)
                                    {
                                        count = 200;        // *300ms
                                        //AuthRequest(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.EchoTest).ToString());
                                        //int nRet = GetLastError();
                                        //checkResultCode = SetResultCode(nRet);
                                        //if (checkResultCode != ErrorCode.Sucsess)
                                        //    checkResultDescription = GetErrorDescription(nRet);
                                        //else
                                        //    checkResultDescription = "";
                                        EchoTest();
                                        if (log != null)
                                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff")
                                                + ": GPB: PoolThread, EchoTest: CommandStatus=" + CommandStatus
                                                + ", bankModuleCommand=" + bankModuleCommand + ", status=" + status);

                                    }
                                    else
                                        count = 40;        // *300ms
                                }
                                else if (isVerification)
                                {
                                    if (deviceEnabled)
                                    {
                                        if (Verification()) isVerification = false;
                                        if (log != null)
                                            log.ToLog(1, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff")
                                                + ": GPB: PoolThread, Verification: CommandStatus=" + CommandStatus
                                                + ", bankModuleCommand=" + bankModuleCommand + ", status=" + status);
                                        count = 40;        // *300ms
                                    }
                                }

                                count--;
                            }
                            break;
                        case CommandStatusT.Undeifned:
                            #region CommandStatusT.NotRun
                            if (AuthorizationStarted)
                                AuthorizationStarted = false;
                            //if (resultDescription != "")
                            //    resultDescription = "";
                            //lock (locker)
                            {
                                if (count < 1)
                                {
                                    if (deviceEnabled)
                                    {
                                        count = 200;        // *300ms
                                        //AuthRequest(KKMNumber.ToString() + " " + ((int)BankModuleCommandT.EchoTest).ToString());
                                        //int nRet = GetLastError();
                                        //checkResultCode = SetResultCode(nRet);
                                        //if (checkResultCode != ErrorCode.Sucsess)
                                        //    checkResultDescription = GetErrorDescription(nRet);
                                        //else
                                        //    checkResultDescription = "";
                                        EchoTest();
                                        if (log != null)
                                            log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff")
                                                + ": GPB: PoolThread, EchoTest: CommandStatus=" + CommandStatus
                                                + ", bankModuleCommand=" + bankModuleCommand + ", status=" + status);
                                    }
                                    else
                                        count = 40;        // *300ms
                                }
                                else if (isVerification)
                                {
                                    if (deviceEnabled)
                                    {
                                        if (Verification()) isVerification = false;
                                        if (log != null)
                                            log.ToLog(1, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff")
                                                + ": GPB: PoolThread, Verification: CommandStatus=" + CommandStatus
                                                + ", bankModuleCommand=" + bankModuleCommand + ", status=" + status);
                                        count = 40;        // *300ms
                                    }
                                }

                                count--;
                            }
                            break;
                        #endregion CommandStatusT.NotRun   
                        default:
                            break;
                    }
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: PoolThread_2, CommandStatus=" + CommandStatus
                        + ", bankModuleCommand=" + bankModuleCommand + ", status=" + status);

                    runing = false;
                    Thread.Sleep(200);
                }
            }

            catch (Exception ee)
            {
                if (log != null)
                    log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + ": GPB: PoolThread.Aborted, Exception " + ee.ToString());
                if (Thread.CurrentThread.ThreadState == ThreadState.Aborted)
                    Thread.ResetAbort();
            }
        }
        //public bool busy = false;
        int countSbusyMax = 900; //*50мс

    }
}
