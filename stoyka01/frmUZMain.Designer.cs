﻿namespace stoyka01
{
    partial class frmUZMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblCardId = new System.Windows.Forms.Label();
            this.lblUZCardID = new System.Windows.Forms.Label();
            this.lblDT = new System.Windows.Forms.Label();
            this.lblUZEntryTime = new System.Windows.Forms.Label();
            this.lblUZSumOnCard = new System.Windows.Forms.Label();
            this.lblTP = new System.Windows.Forms.Label();
            this.lblUZTP = new System.Windows.Forms.Label();
            this.lblTS = new System.Windows.Forms.Label();
            this.lblUZTS = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblUZZadolg = new System.Windows.Forms.Label();
            this.lblDolg = new System.Windows.Forms.Label();
            this.lblDisc = new System.Windows.Forms.Label();
            this.cboUZDiscount = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblMsg = new System.Windows.Forms.Label();
            this.cmdApply = new System.Windows.Forms.Button();
            this.picMsgPicture = new System.Windows.Forms.PictureBox();
            this.tmrDataUpdate = new System.Windows.Forms.Timer(this.components);
            this.lblDiscountDescription = new System.Windows.Forms.Label();
            this.lblManualAmount = new System.Windows.Forms.Label();
            this.txtManualAmount = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.picMsgPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCardId
            // 
            this.lblCardId.AutoSize = true;
            this.lblCardId.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCardId.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblCardId.Location = new System.Drawing.Point(22, 19);
            this.lblCardId.Name = "lblCardId";
            this.lblCardId.Size = new System.Drawing.Size(101, 25);
            this.lblCardId.TabIndex = 3;
            this.lblCardId.Text = "ID karta:";
            // 
            // lblUZCardID
            // 
            this.lblUZCardID.AutoSize = true;
            this.lblUZCardID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblUZCardID.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUZCardID.Location = new System.Drawing.Point(126, 19);
            this.lblUZCardID.Name = "lblUZCardID";
            this.lblUZCardID.Size = new System.Drawing.Size(129, 25);
            this.lblUZCardID.TabIndex = 4;
            this.lblUZCardID.Text = "228867567";
            // 
            // lblDT
            // 
            this.lblDT.AutoSize = true;
            this.lblDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDT.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDT.Location = new System.Drawing.Point(265, 19);
            this.lblDT.Name = "lblDT";
            this.lblDT.Size = new System.Drawing.Size(134, 25);
            this.lblDT.TabIndex = 5;
            this.lblDT.Text = "kirish vaqti:";
            // 
            // lblUZEntryTime
            // 
            this.lblUZEntryTime.AutoSize = true;
            this.lblUZEntryTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblUZEntryTime.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUZEntryTime.Location = new System.Drawing.Point(441, 19);
            this.lblUZEntryTime.Name = "lblUZEntryTime";
            this.lblUZEntryTime.Size = new System.Drawing.Size(129, 25);
            this.lblUZEntryTime.TabIndex = 6;
            this.lblUZEntryTime.Text = "228867567";
            // 
            // lblUZSumOnCard
            // 
            this.lblUZSumOnCard.AutoSize = true;
            this.lblUZSumOnCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblUZSumOnCard.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUZSumOnCard.Location = new System.Drawing.Point(983, 19);
            this.lblUZSumOnCard.Name = "lblUZSumOnCard";
            this.lblUZSumOnCard.Size = new System.Drawing.Size(77, 25);
            this.lblUZSumOnCard.TabIndex = 8;
            this.lblUZSumOnCard.Text = "80000";
            this.lblUZSumOnCard.Visible = false;
            // 
            // lblTP
            // 
            this.lblTP.AutoSize = true;
            this.lblTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTP.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTP.Location = new System.Drawing.Point(22, 68);
            this.lblTP.Name = "lblTP";
            this.lblTP.Size = new System.Drawing.Size(125, 25);
            this.lblTP.TabIndex = 9;
            this.lblTP.Text = "tarif rejasi:";
            // 
            // lblUZTP
            // 
            this.lblUZTP.AutoSize = true;
            this.lblUZTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblUZTP.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUZTP.Location = new System.Drawing.Point(256, 68);
            this.lblUZTP.Name = "lblUZTP";
            this.lblUZTP.Size = new System.Drawing.Size(38, 25);
            this.lblUZTP.TabIndex = 10;
            this.lblUZTP.Text = "15";
            // 
            // lblTS
            // 
            this.lblTS.AutoSize = true;
            this.lblTS.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTS.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTS.Location = new System.Drawing.Point(21, 105);
            this.lblTS.Name = "lblTS";
            this.lblTS.Size = new System.Drawing.Size(181, 25);
            this.lblTS.TabIndex = 11;
            this.lblTS.Text = "tariflar shkalasi:";
            // 
            // lblUZTS
            // 
            this.lblUZTS.AutoSize = true;
            this.lblUZTS.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblUZTS.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUZTS.Location = new System.Drawing.Point(258, 105);
            this.lblUZTS.Name = "lblUZTS";
            this.lblUZTS.Size = new System.Drawing.Size(38, 25);
            this.lblUZTS.TabIndex = 12;
            this.lblUZTS.Text = "15";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(801, 255);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 25);
            this.label6.TabIndex = 13;
            // 
            // lblUZZadolg
            // 
            this.lblUZZadolg.AutoSize = true;
            this.lblUZZadolg.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblUZZadolg.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUZZadolg.Location = new System.Drawing.Point(259, 147);
            this.lblUZZadolg.Name = "lblUZZadolg";
            this.lblUZZadolg.Size = new System.Drawing.Size(77, 25);
            this.lblUZZadolg.TabIndex = 14;
            this.lblUZZadolg.Text = "80000";
            // 
            // lblDolg
            // 
            this.lblDolg.AutoSize = true;
            this.lblDolg.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDolg.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDolg.Location = new System.Drawing.Point(22, 147);
            this.lblDolg.Name = "lblDolg";
            this.lblDolg.Size = new System.Drawing.Size(123, 25);
            this.lblDolg.TabIndex = 15;
            this.lblDolg.Text = "qarzdorlik:";
            // 
            // lblDisc
            // 
            this.lblDisc.AutoSize = true;
            this.lblDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDisc.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDisc.Location = new System.Drawing.Point(24, 202);
            this.lblDisc.Name = "lblDisc";
            this.lblDisc.Size = new System.Drawing.Size(219, 25);
            this.lblDisc.TabIndex = 16;
            this.lblDisc.Text = "chegirmani tanlang:";
            // 
            // cboUZDiscount
            // 
            this.cboUZDiscount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUZDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cboUZDiscount.FormattingEnabled = true;
            this.cboUZDiscount.Location = new System.Drawing.Point(270, 193);
            this.cboUZDiscount.Name = "cboUZDiscount";
            this.cboUZDiscount.Size = new System.Drawing.Size(435, 39);
            this.cboUZDiscount.TabIndex = 17;
            this.cboUZDiscount.SelectedIndexChanged += new System.EventHandler(this.cboUZDiscount_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(34, 303);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(792, 25);
            this.label10.TabIndex = 18;
            this.label10.Text = "____________________________________________________________";
            this.label10.Visible = false;
            // 
            // lblMsg
            // 
            this.lblMsg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMsg.AutoSize = true;
            this.lblMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMsg.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblMsg.Location = new System.Drawing.Point(194, 390);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(511, 25);
            this.lblMsg.TabIndex = 20;
            this.lblMsg.Text = "Для проезда необходимо подъехать к стойке";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmdApply
            // 
            this.cmdApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdApply.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cmdApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmdApply.Location = new System.Drawing.Point(711, 191);
            this.cmdApply.Name = "cmdApply";
            this.cmdApply.Size = new System.Drawing.Size(142, 41);
            this.cmdApply.TabIndex = 21;
            this.cmdApply.Text = "topshirmoq";
            this.cmdApply.UseVisualStyleBackColor = false;
            this.cmdApply.Click += new System.EventHandler(this.cmdApply_Click);
            // 
            // picMsgPicture
            // 
            this.picMsgPicture.Location = new System.Drawing.Point(7, 340);
            this.picMsgPicture.Name = "picMsgPicture";
            this.picMsgPicture.Size = new System.Drawing.Size(136, 136);
            this.picMsgPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picMsgPicture.TabIndex = 22;
            this.picMsgPicture.TabStop = false;
            // 
            // tmrDataUpdate
            // 
            this.tmrDataUpdate.Enabled = true;
            this.tmrDataUpdate.Tick += new System.EventHandler(this.tmrDataUpdate_Tick);
            // 
            // lblDiscountDescription
            // 
            this.lblDiscountDescription.AutoSize = true;
            this.lblDiscountDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDiscountDescription.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDiscountDescription.Location = new System.Drawing.Point(265, 243);
            this.lblDiscountDescription.Name = "lblDiscountDescription";
            this.lblDiscountDescription.Size = new System.Drawing.Size(219, 25);
            this.lblDiscountDescription.TabIndex = 23;
            this.lblDiscountDescription.Text = "chegirmani tanlang:";
            // 
            // lblManualAmount
            // 
            this.lblManualAmount.AutoSize = true;
            this.lblManualAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblManualAmount.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblManualAmount.Location = new System.Drawing.Point(36, 287);
            this.lblManualAmount.Name = "lblManualAmount";
            this.lblManualAmount.Size = new System.Drawing.Size(266, 25);
            this.lblManualAmount.TabIndex = 24;
            this.lblManualAmount.Text = "Введите сумму скидки:";
            this.lblManualAmount.Visible = false;
            // 
            // txtManualAmount
            // 
            this.txtManualAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.txtManualAmount.Location = new System.Drawing.Point(308, 278);
            this.txtManualAmount.MaxLength = 6;
            this.txtManualAmount.Name = "txtManualAmount";
            this.txtManualAmount.Size = new System.Drawing.Size(110, 38);
            this.txtManualAmount.TabIndex = 25;
            this.txtManualAmount.Visible = false;
            this.txtManualAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtManualAmount_KeyPress);
            // 
            // frmUZMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GrayText;
            this.ClientSize = new System.Drawing.Size(864, 487);
            this.Controls.Add(this.txtManualAmount);
            this.Controls.Add(this.lblManualAmount);
            this.Controls.Add(this.lblDiscountDescription);
            this.Controls.Add(this.picMsgPicture);
            this.Controls.Add(this.cmdApply);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cboUZDiscount);
            this.Controls.Add(this.lblDisc);
            this.Controls.Add(this.lblDolg);
            this.Controls.Add(this.lblUZZadolg);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblUZTS);
            this.Controls.Add(this.lblTS);
            this.Controls.Add(this.lblUZTP);
            this.Controls.Add(this.lblTP);
            this.Controls.Add(this.lblUZSumOnCard);
            this.Controls.Add(this.lblUZEntryTime);
            this.Controls.Add(this.lblDT);
            this.Controls.Add(this.lblUZCardID);
            this.Controls.Add(this.lblCardId);
            this.Name = "frmUZMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "operator ish stantsiyasi";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUZMain_FormClosing);
            this.Load += new System.EventHandler(this.frmUZMain_Load);
            this.Resize += new System.EventHandler(this.frmUZMain_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.picMsgPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCardId;
        private System.Windows.Forms.Label lblUZCardID;
        private System.Windows.Forms.Label lblDT;
        private System.Windows.Forms.Label lblUZEntryTime;
        private System.Windows.Forms.Label lblUZSumOnCard;
        private System.Windows.Forms.Label lblTP;
        private System.Windows.Forms.Label lblUZTP;
        private System.Windows.Forms.Label lblTS;
        private System.Windows.Forms.Label lblUZTS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblUZZadolg;
        private System.Windows.Forms.Label lblDolg;
        private System.Windows.Forms.Label lblDisc;
        private System.Windows.Forms.ComboBox cboUZDiscount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Button cmdApply;
        private System.Windows.Forms.PictureBox picMsgPicture;
        private System.Windows.Forms.Timer tmrDataUpdate;
        private System.Windows.Forms.Label lblDiscountDescription;
        private System.Windows.Forms.Label lblManualAmount;
        private System.Windows.Forms.TextBox txtManualAmount;
    }
}