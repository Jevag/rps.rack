﻿using Newtonsoft.Json;
using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace stoyka01
{
    public class IskraKKM
    {
        public struct IskraParams
        {
            public string external_id;
            public string terminal_id;
            public string adr;
            public string place;
            public string cashier_name;
            public string cashier_inn;
            public int taxation;
            public double price;
            public int tax;
            public string card_id;
        }

        public struct TokenResult
        {
            public bool ret;
            public string token;
        }

        public int tkn_expired = 0; //таймаут на получение токена

        public RequestTokenRoot req;

        public ChequeRoot cheque;

        public string token;
        public string cheque_guid;

        //используем тестовые учетные записи
        public string lgn = "test";
        public string pwd = "@Test2021@";

        public string cheque_result;

        public string cheque_request; //запрос на чек (json)
        public string cheque_responce; ////ответ на чек (json)

        public string cheque_qr;

        public async Task<bool> ChequeRequest(IskraParams prms, DateTime dt, string ep) //***
        {
            HttpClient httpClient = new HttpClient();
            try
            {
                string postdata = CreateChequeTest(prms,dt);
                cheque_request = postdata;
                Logging.ToLog(postdata);
                string endpoint = ep;

                httpClient.DefaultRequestHeaders.Add("Token", token);
                //httpClient.DefaultRequestHeaders.Add("SIGN", signheader);
                StringContent content = new StringContent(postdata, Encoding.UTF8, "application/json");
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                HttpResponseMessage Response = await httpClient.PostAsync(endpoint, content);
                string data = await Response.Content.ReadAsStringAsync();
                Logging.ToLog(data);
                cheque_responce = data;
                ResponseChequeRoot root = JsonConvert.DeserializeObject<ResponseChequeRoot>(data);
                if (root.response_error.result == 0)
                {
                    ResponseCheque respc = JsonConvert.DeserializeObject<ResponseCheque>(root.response.ToString());
                    cheque_guid = respc.uuid;
                    return true;
                }
                else
                {
                    cheque_guid = root.response_error.comment.ToString();
                    return false;
                }
            }
            catch (HttpRequestException hre)
            {
                cheque_guid = "Ошибка http соединения";
                return false;
            }
            catch (TaskCanceledException)
            {
                cheque_guid = "Ошибка асинхронной задачи";
                return false;
            }
            catch (Exception ex)
            {
                cheque_guid = "Другая ошибка " + ex.Message;
                return false;
            }
            finally
            {
                if (httpClient != null)
                {
                    httpClient.Dispose();
                    httpClient = null;
                }
            }
        }

        public async Task<bool> TokenRequest(string ep) //***
        {
            HttpClient httpClient = new HttpClient();
            try
            {
            //string nonce = ServiceClass.GetUnixTimestamp(DateTime.Now).ToString();
            string postdata = GetTokenJson();
            Logging.ToLog(postdata);

            StringContent content = new StringContent(postdata, Encoding.UTF8, "application/json");
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            HttpResponseMessage Response = await httpClient.PostAsync(ep, content);
            string data = await Response.Content.ReadAsStringAsync();
            ResponseTokenRoot root = JsonConvert.DeserializeObject<ResponseTokenRoot>(data);
                Logging.ToLog(data);
                //TokenResult t = new TokenResult();
                //t.ret = true;
                //return root.response.token;
                token = root.response.token;
                return true;
            }
            
            catch (HttpRequestException hre)
            {
                //GlobalClass.LastErr = "Невозможно соединиться с биржей Gate IO";
                //return "HttpRequestException";
                return false;
            }
            catch (TaskCanceledException)
            {
                //GlobalClass.LastErr = "Ошибка асинхронной задачи";
                //return "TaskCanceledException";
                return false;
            }
            catch (Exception ex)
            {
                //Debug.WriteLine("OtherException");
                //GlobalClass.LastErr = "Другая ошибка";
                //return "OtherException" + ex.Message;
                return false;
            }
            finally
            {
                if (httpClient != null)
                {
                    httpClient.Dispose();
                    httpClient = null;
                }
            }            
        }

        public string GetTokenJson()
        {
            req = new RequestTokenRoot();
            req.request_type = "get_token";
            RequestToken reqbody = new RequestToken();
            reqbody.login = lgn;
            reqbody.password = pwd;
            req.request = reqbody;
            //Debug.Print(JsonConvert.SerializeObject(req));

            return JsonConvert.SerializeObject(req);
        }

        public string CreateChequeTest(IskraParams prms, DateTime dt)
        {
            cheque = new ChequeRoot();
            cheque.request_type = "cheque_create";
            ChequeRequest creq = new ChequeRequest();
            creq.external_id = prms.external_id;
            creq.terminal_id = prms.terminal_id; //номер ккм (c 
            creq.terminal_type = 1;
            //"timestamp":"01.12.19 13:45:00",
            //Debug.Print(DateTime.Now.ToString("dd.MM.yy HH:mm:ss"));
            creq.timestamp = (dt.ToString("dd.MM.yy HH:mm:ss"));
            creq.cheque_content = new ChequeContent();
            creq.cheque_content.calc_address = prms.adr;
            creq.cheque_content.calc_place = prms.place;
            creq.cheque_content.cashier_name = prms.cashier_name;
            creq.cheque_content.CashierInn = prms.cashier_inn;
            creq.cheque_content.calc_sign = 1;
            creq.cheque_content.taxation = prms.taxation;
            creq.cheque_content.agent_data = null;
            creq.cheque_content.provider_data = null;
            creq.cheque_content.positions = new List<Position>();

            Position pos = new Position();
            pos.name = "Оплата парковки"; //пока хардкод
            pos.price = prms.price;
            pos.quantity = 1; //1 у нас
            pos.amount = prms.price;
            pos.measure_unit = null; //
            pos.tax = prms.tax; //берем из настроек стойки
            pos.calc_method_sign = 4; //полный расчет
            pos.calc_subject_sign = 4; //услуга
            pos.ktn = null;
            pos.add_props = "Номер карты: " + prms.card_id;
            pos.excise = null;
            pos.country_code = null;
            pos.declaration_number = null;
            pos.agent_data = null; //пока оставляем null???
            creq.cheque_content.positions.Add(pos);

            creq.cheque_content.total = prms.price;
            creq.cheque_content.add_user_props = null;
            creq.cheque_content.customer = null; //пока оставляем null (реквизиты при продаже чего-то юр.лицам или ИП)
            creq.cheque_content.payments = new List<IskraPayment>();

            IskraPayment payment = new IskraPayment();
            payment.payment_type = 2; //для нас!
            payment.amount = prms.price;
            creq.cheque_content.payments.Add(payment);

            creq.cheque_content.correction = false; //
            creq.cheque_content.correction_type = 0;
            creq.cheque_content.correction_basis = null;

            cheque.request = creq;

            return JsonConvert.SerializeObject(cheque);
        }

        public async Task<bool> GetChequeStatus(string ep, string uuid) //***
        {
            HttpClient httpClient = new HttpClient();
            try
            {
                ChequeStatusRequestRoot root = new ChequeStatusRequestRoot();
                root.request_type = "get_cheque_status";
                root.request = new ChequeStatusRequest();
                root.request.uuid = uuid;

                string postdata = JsonConvert.SerializeObject(root);
                cheque_request = postdata;
                Logging.ToLog(postdata);
                string endpoint = ep;

                //httpClient.DefaultRequestHeaders.Add("Token", await TokenRequest(ep));
                httpClient.DefaultRequestHeaders.Add("Token", token);
                //httpClient.DefaultRequestHeaders.Add("SIGN", signheader);
                StringContent content = new StringContent(postdata, Encoding.UTF8, "application/json");
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                HttpResponseMessage Response = await httpClient.PostAsync(endpoint, content);
                string data = await Response.Content.ReadAsStringAsync();
                cheque_responce = data;
                Logging.ToLog(data);
                //return true;

                ChequeStatus resp = JsonConvert.DeserializeObject<ChequeStatus>(data);


                if (resp.response_error.result == 0)
                {
                    cheque_result = resp.response.cheque_info.qr_value;
                    return true;
                }
                else
                {
                    cheque_result = resp.response_error.comment.ToString();
                    return false;
                }
            }
            catch (HttpRequestException hre)
            {
                cheque_result = "Http err";
                return false;
            }
            catch (TaskCanceledException)
            {
                cheque_result = "Ошибка асинхронной задачи";
                return false;
            }
            catch (Exception ex)
            {
                cheque_result = "Другая ошибка " + ex.Message;
                return false;
            }
            finally
            {
                if (httpClient != null)
                {
                    httpClient.Dispose();
                    httpClient = null;
                }
            }
        }

        public async Task<bool> GetQRCheque(IskraParams prms, string ep, DateTime dt)
        {
            bool x = await ChequeRequest(prms,dt, ep);
            if (x)
            {
                Thread.Sleep(1000);
                bool y = await GetChequeStatus(ep, cheque_guid);
                if (y) return true;
                else return false;
            }
            else
            {
                return false;
            }
        }

        public void AddToCL(DateTime dt, IskraParams prms)
        {
            List<object> CL = new List<object>();

            CL.Add("dt");
            CL.Add(dt);

            CL.Add("adr");
            CL.Add(prms.adr);

            CL.Add("card_id");
            CL.Add(prms.card_id);

            CL.Add("cashier_inn");
            CL.Add(prms.cashier_inn);

            CL.Add("cashier_name");
            CL.Add(prms.cashier_name);

            CL.Add("external_id");
            CL.Add(prms.external_id);

            CL.Add("place");
            CL.Add(prms.place);

            CL.Add("price");
            CL.Add(prms.price);

            CL.Add("tax");
            CL.Add(prms.tax);

            CL.Add("taxation");
            CL.Add(prms.taxation);

            CL.Add("terminal_id");
            CL.Add(prms.terminal_id);

            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                UtilsBase.InsertCommand("WebIskraCL", connect, null, CL.ToArray());
            }
        }

        public async void PrintingFromCL(string ep)
        {
            bool res = false;
            DateTime dt;
            IskraParams prms = new IskraParams();
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                try
                {
                    DataTable CL = new DataTable();
                    CL = UtilsBase.FillTable("select * from WebIskraCL", connect, null, null); // where ISNULL(IsDeleted,0)!=1                                       

                    if (CL != null && CL.Rows.Count != 0)
                    {
                    DataRow dad = CL.Rows[0];
                        dt =(DateTime) dad.GetDateTime("dt");
                        prms.adr = dad.GetString("adr");
                        prms.card_id = dad.GetString("card_id");
                    prms.cashier_inn = dad.GetString("cashier_inn");
                    prms.cashier_name = dad.GetString("cashier_name");
                    prms.external_id = dad.GetString("external_id");
                    prms.place = dad.GetString("place");
                    prms.price = Convert.ToDouble(dad.GetInt("price"));
                    prms.tax =(int) dad.GetInt("tax");
                    prms.taxation = (int)dad.GetInt("taxation");
                    prms.terminal_id = dad.GetString("terminal_id");
                        bool x = await ChequeRequest(prms, dt, ep);
                    if (x)
                    {
                        res = true;
                    }
                    }
                    else
                    {

                    }
                }
                catch (Exception e)
                {
                    Logging.ToLog("Exception AuthUser " + e);
                }
            }

            if (res) //удаляем запись!
            {
                DelFromCL();
            }
        }

        public void DelFromCL()
        {
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                try
                {
                    DataTable cls = UtilsBase.FillTable("select * from WebIskraCL", connect, null, null);
                    if (cls != null && cls.Rows.Count != 0)
                    {
                        string sql = "DELETE FROM WebIskraCL";
                        SqlCommand cmd = new SqlCommand(sql, connect);
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {

                    }
                }
                catch (Exception e)
                {
                    //ToLog("Exception GetDat Setting. " + e.ToString());
                }
            }
        }
    }
}
