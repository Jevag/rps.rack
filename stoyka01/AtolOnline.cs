﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace stoyka01
{
    public class AtolOnline
    {
        AtolAuthResponse AuthResponse;

        public async Task<string> GetToken(string lgn, string pwd) //Успех - возвращает токен, нет - возвращает
        {
            try
            {
                string data="";
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync("https://online.atol.ru/possystem/v4/getToken?login=" + lgn + "&pass=" + pwd);

                HttpContent responseContent = response.Content;
                using (StreamReader reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
                {
                    data = await reader.ReadToEndAsync();

                    AuthResponse = JsonConvert.DeserializeObject<AtolAuthResponse>(data);

                    if (AuthResponse.error == null)
                    {
                        return AuthResponse.token;
                    }
                    else
                    {
                        return "error"; //Потом развернуть
                    }

                }
            }
            catch (Exception ex)
            {
                return "internal_error";
            }
        }
    }
}
