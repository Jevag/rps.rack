﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stoyka01
{
    public partial class frmUZLogin : Form
    {
        public frmUZLogin()
        {
            InitializeComponent();
        }

        private void txtPhone_TextChanged(object sender, EventArgs e)
        {
            if (txtPhone.Text.Length == 7)
            {
                txtPassword.Focus();
            }
        }

        private void frmUZLogin_Leave(object sender, EventArgs e)
        {
            //Logging.ToLog("lost_focus");
        }

        private void frmUZLogin_Activated(object sender, EventArgs e)
        {
            //Logging.ToLog("get_focus");
        }

        private void txtRegion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }

        }

        private void txtPhonePrefix_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtRegion_TextChanged(object sender, EventArgs e)
        {
            if (txtRegion.Text.Length == 3)
            {
                txtPhonePrefix.Focus();
            }
        }

        private void txtPhonePrefix_TextChanged(object sender, EventArgs e)
        {
            if (txtPhonePrefix.Text.Length == 3)
            {
                txtPhone.Focus();
            }
        }

        void EntryVoid()
        {
            GlobalClass.UZLogin = txtPhonePrefix.Text + txtPhone.Text;
            GlobalClass.UZPass = txtPassword.Text;

            if (Tashkent.Authorize(GlobalClass.UZLogin, GlobalClass.UZPass))
            {
                //shaxsiy hisobingizga kirish bloklangan
                if (Tashkent.CheckLK(GlobalClass.UZ_Id))
                {
                    if (Tashkent.SelectDiscounts(GlobalClass.UZ_Id))
                    {
                        //Пошли далее
                        GlobalClass.UZOperatorShow = 2;
                        frmUZMain frmUZ = new frmUZMain();
                        frmUZ.Show();
                        frmUZ.Left = GlobalClass.UZFormX;
                        frmUZ.Top = GlobalClass.UZFormY;
                        this.Hide();
                    }
                    else
                    {
                        //MessageBox.Show("kompaniyada chegirmalar yo'q");
                        frmUZMsg msg = new frmUZMsg(GlobalClass.CurrentLangDictionary[10915], this.Location.X + this.Size.Width / 2, this.Location.Y + this.Size.Height / 2);
                        msg.Show();
                    }
                }
                else
                {
                    //MessageBox.Show("Shaxsiy hisobingizga kirish bloklangan");
                    frmUZMsg msg = new frmUZMsg(GlobalClass.CurrentLangDictionary[10916], this.Location.X + this.Size.Width / 2, this.Location.Y + this.Size.Height / 2);
                    msg.Show();
                }
            }
            else
            {
                //MessageBox.Show("Tizimda foydalanuvchi topilmadi");


                frmUZMsg msg = new frmUZMsg(GlobalClass.CurrentLangDictionary[10917], this.Location.X + this.Size.Width / 2, this.Location.Y + this.Size.Height / 2);
                msg.Show();
                //MessageBoxEx.Show(this.Handle, "Tizimda foydalanuvchi topilmadi");
            }
        }

        private void cmdEntry_Click(object sender, EventArgs e)
        {
            EntryVoid();
        }

        private void frmUZLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            GlobalClass.UZOperatorShow = 0; //следить по таймеру???
        }

        private void frmUZLogin_Load(object sender, EventArgs e)
        {
            this.Text = GlobalClass.CurrentLangDictionary[10911];
            lblPhone.Text= GlobalClass.CurrentLangDictionary[10912];
            lblPass.Text= GlobalClass.CurrentLangDictionary[10913];
            cmdEntry.Text = GlobalClass.CurrentLangDictionary[10914];
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EntryVoid();
            }
        }

        private void frmUZLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EntryVoid();
            }
        }
    }
}
