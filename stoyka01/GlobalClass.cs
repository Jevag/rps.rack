﻿using CalculatorTS;
using RPS_Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using static stoyka01.Enums;

namespace stoyka01
{
    public static class GlobalClass
    {
        public static bool CardidReverseDec = false;

        public static string MTSBarcode = "";
        public static DateTime MTSDateTime;

        public static bool UpdateHost = false;
        public static int IpListIndex = 0;

        public static bool PayPhysicalCard = false; //переменная, учитывающая что нужно выдать физику

        public static bool PayEntranceCard = false;

        public static int PaySummEntrance = 100;

        public static int TarifIdEntrance = 1;
        public static int TarifPlanIdEntrance = 1;

        public static bool CreateSinglePlateEntrance = false;
        public static bool CreateSinglePlateExit = false; //Удалять карту после проезда

        public static bool SinglePlate = false;
        public static bool LevenSingle = false;

        public static string BlockedGroupList="";


        public struct BlockedGroups
        {
            public int Index;
            public Guid Id;
            public byte IdFC;
            public string mName;
            public bool Blocked;
        }

        public static List<BlockedGroups> blGroups;

        public static bool BlockedGroupExist = false;


        public static bool PayFromOutScanner = false;

        public static string DomainNameForQR = "";

        public static bool HomelessRack = false;

        public static bool BMres = false;

        public static string UnitellerTID = "";
        public static string UnitellerIP = "";
        public static string UnitellerPort="";

        public static bool ThruLPNum=false;

        //используем для выезда тоже!!!
        public static string QR_Res = "";
        public static string QR_Plate = ""; //номер жопы!


        public static string QR_CardIdCardR=""; //постоянный - regular
        public static string QR_CardIdCardS = ""; //разовый - single

        public static bool AbonVn121 = false; //значит, что прилетела блокировка абонемента с внутр ридера!!!

        public static bool QRVerificationEntrance = false;
        public static bool QRVerificationExit = false;

        public static int QRVerificationResult = -1;

        public static bool QRVerificationEntranceR = false; //для постоянников
        public static bool QRVerificationExitR = false;
        public static bool QRVerificationExitRC = false;

        public static bool NeedQREntranceAlarm = false; //а был ли аларм?
        public static bool NeedQRExitAlarm = false; //а был ли аларм?


        public static bool UpdRegTicket = false;

        //public static bool NippelLastEtap = false;
        public static bool RegTicket = false;

        public static bool RegTicketAnalysis = false;

        public static bool eFrom304 = false;


        public static int BankSumMax = 1000;

        public static bool QRCheckSended = false;

        public static string KKMQR;

        public static int KKMType;

        public static bool BTNRPSUSB = true;

        public static int RPSButtonDelay = 20;

        public static DateTime? BlockTime;

        public static bool BarcodeSetting = false; //настройка из Setting

        public static DateTime BarcodeAvailableFrom;
        public static DateTime DiscountAvailableFrom;

        public static bool SendPrinterDensity150=false;

        public static bool SeosActivated=false;
        public static bool SeosCardDetected=false;
        public static string SeosCardNum = "";

        public static string strDateRfid = "";

        public static bool FirstRfidPicture = false;

        public static int rfidhid_counter = 0;
        public static string RFIDHIDTag = "";

        public static bool OperatorRFIDHID = false;
        public static int OperatorRFIDHIDDelay = 0;

        public static int HSize;
        public static int VSize;

        public static int UZValue=0;

        public static int tmrExlog = 0;
        public static int tmrbadmln = 0;

        public static bool ExtendedLog = false;

        public static bool PairLoops = false;

        public static bool UpDoorInverse = false;

        public static bool StopByHID_Abon = false;

        public static bool VirtCardLog = false;

        public static string Crypted;

        public static int TicketScannerType = 0;
        public static int TicketPrinterType = 0;

        public static bool DoNotPaidTransact = false;
        public static int NotPaidSumOnCard = 0;

        //управление по управлению удаленными проездами
        public static bool RemoteRackPost = false;
        public static bool RemoteRackRaz = false;

        public static string RemoteRackPost1 = "";
        public static string RemoteRackPost2 = "";
        public static string RemoteRackPost3 = "";
        public static string RemoteRackPost4 = "";
        public static string RemoteRackPost5 = "";

        public static string RemoteRackRaz1 = "";
        public static string RemoteRackRaz2 = "";
        public static string RemoteRackRaz3 = "";
        public static string RemoteRackRaz4 = "";
        public static string RemoteRackRaz5 = "";

        public static string PPTimeout = "";
        public static string PPThres = "";
        public static string PPDelta = "";

        public static bool IskraChequeResult = false;

        public static bool VirtKKMReady = false;

        //AbonementBlockAllow
        public static bool AbonementBlockAllow = false;

        public static bool WebTokenCheck = false;
        public static bool WebGuidCheck = false;
        public static bool WebQRCheck = false;

        public static bool WebShowCheck = false;

        public static bool WebIskra  = true; //будем пробовать активировать бм, если включена искра

        //webiskra
        public static string WebIskraLogin = "";
        public static string WebIskraPassword = "";
        public static bool OutTicketScannerWorks = false;

        public static int WebIskraTax = 0;
        public static int WebIskraTaxation = 0;

        public static string WebIskraINN = "";
        public static string WebIskraFIO = "";
        public static string WebIskraAdr = "";
        public static string WebIskraPlace = "";
        public static string WebIskraEP = "https://195.190.114.238:8281/kktcloud/api/kktcloud?shop=1";

        public static long FireAlarmCounter = 0;
        public static int FireAlarm = 0; //0-2
        public static int FireAlarmDelay = 100;
        public static bool FireAlarmMiu = false;

        public static bool ZetonLoopABasket = false;

        public static bool Npl_Run = false;

        public static bool RequestR661 = false;

        public static bool NoSystem=false;

        public static bool BlockByLoopB = false;

        public static bool UseDbonly = false;

        public static bool EnableRfidReset = false;

        public static bool CompanyControl = false;

        public static bool FromEtap50 = false;

        

        public static bool WhiteScreenUpdTrigger = false;
        //
        public static string GeneredQRCode = "";

        public static bool QRIsGenered = false;

        public static bool WhiteScreen = false;

        public static bool QRAllowEntry = false;

        public static bool PingFromLoopA = false;

        public static int BigSize = 0;

        public static string BarrierState;
        public static string oldBarrierState;
        public static string BarrierState2;
        public static string oldBarrierState2;

        public static bool AllowPropusk = true;

        public static bool AirportMode = false;

        public static bool IgnoreBarierTransactions = false;
        public static bool IgnoreFreeTransactions = false;

        public static bool BarrierIsOpened = false;

        public static bool NeedFreeTransact = false;

        public static bool BMVerifyRestart = false;

        public static int etap = 0;               // этап проезда (статус стойки) 

        #region uzbek
        public static bool UZDiscountApproved = false;

        public static string UZLogin = "";
        public static string UZPass = "";

        public static Guid UZ_Id;

        public static bool UZOperator = false; //включенная функция

        public static int UZLoginX = 1024;
        public static int UZLoginY = 768;

        public static int UZFormX = 1024;
        public static int UZFormY = 768;

        public static byte UZOperatorShow = 0; //0 - hide, 1 - окно логина, 2 - главное окно

        public struct UZData
        {
            public string UZCardId;
            public string UZEntryTime;
            public int UZSumOnCard;
            public string UZTP;
            public string UZTS;
            public string UZZadolg;
            public string MsgPicture;
            public string Msg;
        }

        public static bool UZOutApproved; //приложите карту еще раз

        public static UZData UZShowData;

        public static bool UZCardInserted = false;

        public static bool UZLockPaymentScreen = false;

        #endregion


        public static bool TKVP_Incremented = false;

        public static bool BarcodeAltimeter = false;

        public static int FreeBarrierReverse = 100;

        public static int MlnVertPers = 100;

        public static int MlnHorPers = 100;

        public static int MlnDistorsion = 0;

        public static byte MlnCamera = 1; //1 или 2 - для показа ROI

        public static int PrinterDensity = 200;

        public static bool PenaltyCardBarcodeCheck = false;

        public static bool InvertHatch = false;

        public static bool isPPTransact = false;

        public static byte[] RPSButtonMifareId;

        public static bool RPSButtonStarted = false;

        public static int RPSButtonStart = 0;

        public static int ParkpassType = 0;

        public static bool RPSButtonOnTouch = false;

        public static bool RPSButtonEnabled = false;

        public static int RPSButtonByte4 = 0;
        public static int RPSButtonCS = 0;

        public static int oldCapacity;

        public static bool RZoneUpdInfo = false;

        /*
        public struct BudapestDirection
        {
            public DateTime ActualTime;
            public int Direction; //int
        }

        public static BudapestDirection RZoneDirection;
        */
        public static bool EnableAltimetrChecking = false;

        public static bool EnableAltimetrRazForExit= false;

        public static int AltimetrControlTS0 = 1;
        public static int AltimetrControlTS1 = 1; //реально осталось
        public static int AltimetrControlTS2 = 1; //реально осталось
        public static int AltimetrControlTS3 = 1; //реально осталось

        public static bool RZoneActive = false;
        public static bool RZoneDisableExit = false;

        public static bool RZonePlusSended = false;
        public static bool RZoneMinusSended = false;

        public static bool Play121 = false;
        public static bool Play122 = false;

        public static int AbonementBlockDaysLeft=0; //реально осталось

        public static bool AbonementBlock = false;
        public static bool AbonementBlockAlert = false;
        public static int AbonementBlockAlertDays = 1;

        public static bool LevenRegion = false;

        public static bool CL_Enabled = true; //CL_Remove!


        public static bool Bobina = false;

        public static bool WithoutCardPay = false;

        public static string ParkPassObjectId = "";

        public static bool EnabledTicketRecognition = false;

        //public static bool BarrierTOAvai=false;

        //public static bool BarrierDnStarted = false;
        //public static bool BarrierUpStarted = false;

        //public static int BarrierTOCounter=0;
        public static bool FromPenaltyBarcode = false;

        public static bool IsPayDiscount=false;

        public static bool isExternal = false;

        public static bool FindCardForTicketRecognitionOK;
        public static bool WithoutCardRecognitionOK;
        public static int TicketCollectorTimer;

        public static bool NoCardEntrance = false;

        public struct BarrierQueque
        {
            public DateTime ReleaseTime;
            public string Direction; //up or down
        }

        //public static Queue<BarrierQueque> br_que;

        public static List<BarrierQueque> br_list;

        public static int ProductionNumber;

        public static string LastScanningNumber;

        public static bool KKMReady = false;

        public static int PlaceControl; //замены настройки...
        public static bool DeniedForRaz;
        public static bool DeniedForAll;
        public static bool DeniedForRazIf;
        public static bool DeniedForPostIf;

        public static bool PingSended;

        //new slave
        public static byte v_ClimateMode=0;
        public static byte TempChannel = 0;
        public static bool Cooler = false;
        public static bool Heater = false;
        public static float ClimateTMax = 50;
        public static float ClimateTMin = -5;
        public static float ClimateHumidity = 90;

        public static int Slave3In1A;
        public static int Slave3In2A;
        public static int Slave3In3A;
        public static int Slave3In4A;

        public static int Slave3In1B;
        public static int Slave3In2B;
        public static int Slave3In3B;
        public static int Slave3In4B;

        public static int Slave3Out1A;
        public static int Slave3Out2A;
        public static int Slave3Out3A;
        public static int Slave3Out4A;

        public static int Slave3Out1B;
        public static int Slave3Out2B;
        public static int Slave3Out3B;
        public static int Slave3Out4B;

        public static int Slave3In1H;
        public static int Slave3In2H;
        public static int Slave3In3H;
        public static int Slave3In4H;
        public static int Slave3In5H;
        public static int Slave3In6H;
        public static int Slave3In7H;
        public static int Slave3In8H;
        public static int Slave3In9H;
        public static int Slave3In10H;

        public static int Slave3Out1H;
        public static int Slave3Out2H;
        public static int Slave3Out3H;
        public static int Slave3Out4H;
        public static int Slave3Out5H;
        public static int Slave3Out6H;
        public static int Slave3Out7H;
        public static int Slave3Out8H;
        public static int Slave3Out9H;
        public static int Slave3Out10H;

        public static int Slave3Relay1;
        public static int Slave3Relay2;
        public static int Slave3Relay3;
        public static int Slave3Relay4;
        public static int Slave3Relay5;
        public static int Slave3Relay6;
        public static int Slave3Relay7;
        public static int Slave3Relay8;
        public static int Slave3Relay9;
        public static int Slave3Relay10;

        public static bool Slave3Relay1Restart=false;
        public static bool Slave3Relay2Restart = false;
        public static bool Slave3Relay3Restart = false;
        public static bool Slave3Relay4Restart = false;
        public static bool Slave3Relay5Restart = false;
        public static bool Slave3Relay6Restart = false;
        public static bool Slave3Relay7Restart = false;
        public static bool Slave3Relay8Restart = false;
        public static bool Slave3Relay9Restart = false;
        public static bool Slave3Relay10Restart = false;

        public static int ReadOutPower = 0;
        public static int ReadInPower = 0;
        public static int DispenserPower = 0;
        public static int DisplayPower = 0;
        public static int DisplayTwoPower = 0;
        public static int HopperPower = 0;
        public static int RFIDPower = 0;
        public static int CollectorPower = 0;
        public static int BankModulePower = 0;
        public static int TicketPrinterPower = 0;
        public static int TicketScannerPower = 0;
        public static int KKMPower = 0;

        public static bool ParkpassDisableRaz = false;

        public static Dictionary<string, int> CurrentABInputs; //0-3 a;
        public static Dictionary<string, int> CurrentABOutputs; //4-7 b

        public static Dictionary<string, int> CurrentHInputs;
        public static Dictionary<string, int> CurrentHOutputs;

        public static Dictionary<string, bool> InDiskretBusy;

        public static bool BarrierClosing;
        public static bool BarrierOpening;

        public static bool BarrierClosing2;
        public static bool BarrierOpening2;

        public static bool LightIsGreen;
        public static bool LightIsRed;

        public static bool SlaveGetData;

        //слепки команд
        public static bool OutA1;
        public static bool OutA2;
        public static bool OutA3;
        public static bool OutA4;

        public static bool OutB1;
        public static bool OutB2;
        public static bool OutB3;
        public static bool OutB4;

        public static bool OutH1;
        public static bool OutH2;
        public static bool OutH3;
        public static bool OutH4;
        public static bool OutH5;
        public static bool OutH6;
        public static bool OutH7;
        public static bool OutH8;
        public static bool OutH9;
        public static bool OutH10;

        //end new slave

        public static bool ComboReInit;

        public static string CurrentCultureInfo;

        public static byte TPBefore; //тариф до
        public static byte TSBefore; //тариф до


        //public static bool PereezdDolg;

        public static string DefaultCardKey;

        public static bool ZulikZeton = false;

        public static bool ZulikCard = false;
        public static string oldCardNumber = "";

        public static bool ZulikPoiman = false;
        public static int ZulikTimer = 0;
        public static bool ParkingIsPayed;
        public static bool FirstTimePay = false;
        public static bool FromEtap2 = false;
        public static bool FromEtap4 = false;
        public static bool FromEtap34 = false;

        public static bool CheckIsPrinted = false;

        //public static bool IsBadKKM = false;

        public static string oldZetonCardId;

        public static bool ZPrevEtap = false;
        public static bool ZNextEtap = false;

        public static bool StayBeforeCheck = false;

        public static bool CheckTakingStarted;
        public static bool CheckTaken;
        public static string PrintingStatus;

        public static int ZadolgPP;

        public static bool FromPP;
        public static bool FromPP2;

        public static bool TransactFromPP;

        public static bool PPClean;
        public static bool PPActivated;

        public static bool IsRegularEntrance;

        public static string CurrentSoundPlay;
        public static int CurrentSoundDelay=0;
        public static int SoundCounter;

        #region Logs
        public static string logPath = @"LogStoyka.txt";
        public static string[] logs = new string[220];
        public static int[] logLevel = new int[220];             // уровень лога для текущей записи

        public static int logNew = 0;             // номер посылки последней записи  в лог
        public static int logOld = 0;             // номер записанной в файл посылки
        public static int logTyp = 2;         // тип лога =1 - локальны в файл, =2 - в базу
        public static CommonClassLibrary.Log log;      // лог типа 2 - наверное нельзя!!!
        public static int logIndex = 0;

        public static long fileL = 0;               //размер лога
        public static long fileLS = 0;               //размер лога синхр

        #endregion

        #region Cards
        public static ToTariffPlan.DateCard _DateCard;       // данные с карты считанные и пишущиеся
        public static ToTariffPlan.DateCard _DateCard2;       // данные с карты после калькулятора (копия _DateCard) 
        public static ToTariffPlan.DateCard Save_DateCard;       // данные с карты сохраненные
        public static ToTariffPlan.DateCard _VirtualCard;

        public static byte[] cardIdCard = new byte[8];                // номер карты клиента
        public static long CardIdCard;
        public static string CardIdCardS;

        public static DateTime datetime0 = new DateTime(2000, 1, 1, 0, 0, 0, 0);  // НУЛЕВОЕ время

        public static byte[] keyA = new byte[6];               // ключ 6 байт
        public static byte[] keyX = new byte[6];               // ключ 6 байт
        public static byte[] keyY = new byte[6];               // ключ 6 байт

        public static DateTime DateSaveCard;      // Время последней записи на карту
        public static int CardsOnline = 0;    // признак проверки обновления данных с карт по базе

        public static byte[] rBlock = new byte[48];               // чтение сектора карты

        public static byte[] rBlockPP = new byte[48];

        public static byte[] wBlock = new byte[48];               // запись сектора карты

        public static byte[] wBlockPP = new byte[48];

        public static byte ZonaDoCard = 0;        // зона на карте для записи транзакции 

        #endregion

        #region hardware_and_etaps_and_strings
        public static int etapReadCard = 0;    // этап ридера карт
        public static string comReadIn;         // новая команда (в очередь)
        public static string _comReadIn;        // старая команда (выполняемая), comToReadOut.Text - последняя на выполнение

        public static string statDispensText;     //
        public static string statDispensLanguage; //
        public static string statReadInText;     //
        public static string statReadInLanguage; //
        public static string statReadOutText;     //
        public static string statReadOutLanguage; //
        public static string statusCardLanguage; //
        public static string statSlaveLanguage; //
        public static string statReadBarcodeText;     //

        public static string comDispens;         // новая команда (в очередь)
        public static string _comDispens;        // старая команда (выполняемая), comDispens - последняя на выполнение
        public static string nameDispens;
        public static int etapDispensCard = 0;    // этап выдачи карт

        public static string comReadOut;         // новая команда (в очередь)
        public static string _comReadOut;        // старая команда (выполняемая), comToReadOut.Text - последняя на выполнение

        public static string statDispensN;        // новый () статус диспенсера
        public static string statReadInN;         // новый () статус ридера внутри (разового клиента)
        public static string statReadOutN;        // новый () статус ридера снаружи (постоянного клиента)        
        public static int statDiskretN;           // новый loopA=1, ИК=2, loopB=4, кнопка "НАЖМИТЕ"=8,  

        // старый статус. Пишем из нового статуса в alarmR();       // запись тревог в БД
        public static string _statDispens;        // статус диспенсера карт
        public static string _statReadIn;         // статус ридера карт внутри (разового клиента)
        public static string _statReadOut;        // статус ридера карт снаружи (постоянного клиента)
        public static int _statDiskret;           // статус loopA=1, ИК=2, loopB=4, кнопка "НАЖМИТЕ"=8,  
        // кнопка "ВЫЗОВ"=16, датчик верхней двери=32,  датчик нижней двери=64
        public static string _statusCard = "";       // статус карт есть/мало/нет

        // старый статус. Пишем из нового статуса в log()
        public static string l_statDispens;        // статус диспенсера карт
        public static string l_statReadIn;         // статус ридера карт внутри (разового клиента)
        public static string l_statReadOut;        // статус ридера карт снаружи (постоянного клиента)
        public static int l_statDiskret;           // статус loopA=1, ИК=2, loopB=4, кнопка "НАЖМИТЕ"=8,  
                                                   // кнопка "ВЫЗОВ"=16, датчик верхней двери=32,  датчик нижней двери=64
        public static string l_statusCard = "";       // статус карт есть/мало/нет


        // новый статус. Пишем из ComboBox и statDiskret в log()

        // кнопка "ВЫЗОВ"=16, датчик верхней двери=32,  датчик нижней двери=64, ...=128
        public static string statusCard = "";       // новый статус карт есть/мало/нет

        public static string statLoopA = "";              //
        public static string statLoopB = "";              //
        public static string statIR = "";
        public static string statLoopD = "";
        public static string statIR2 = "";
        public static string statButPush = "";            // кнопка "НАЖМИТЕ"
        public static string statButCall = "";            // кнопка "ВЫЗОВ"
        public static string statDoorUp = "";             //
        public static string statDoorDn = "";             //
        public static string statPower = "";             //
        // новый статус.

        public static string statLoopAN = "";              //
        public static string statLoopBN = "";              //
        public static string statIRN = "";                 //
        public static string statButPushN = "";            // кнопка "НАЖМИТЕ"
        public static string statButCallN = "";            // кнопка "ВЫЗОВ"
        public static string statDoorUpN = "";             //
        public static string statDoorDnN = "";             //

        public static string _statOut;                     //
        public static string _statLoopA = "";              //
        public static string _statLoopB = "";              //
        public static string _statIR = "";                 //
        public static string _statButPush = "";            // кнопка "НАЖМИТЕ"
        public static string _statButCall = "";            // кнопка "ВЫЗОВ"
        public static string _statDoorUp = "";             //
        public static string _statDoorDn = "";             //


        public static string statSlave;          // статус Slave текстовый
        public static string _statSlave;          // статус Slave текстовый старый
        public static string l_statSlave;          // статус Slave текстовый старый в log()

        public static string statTablo;          // статус Tablo текстовый
        public static string statTabloTwo;          // статус Tablo Two текстовый

        public static string statWiegand; // статус Slave текстовый

        public static bool NoZetons;

        public static int timerbadZeton=0;

        //Команды въездной стойки
        public static string _statZeton;
        public static string _comToZeton;
        //Команды выездной стойки
        public static string _statZeton2;
        public static string _comToZeton2;

        public static string _oldstatZeton;
        public static string _oldstatZeton2;

        public static string _oldcomToZeton;
        public static string _oldcomToZeton2;

        public static int ZetonReadState = 0; //0-под ридером, 1 - прочитан, 2 - записан

        public static int timernotZeton = 0;

        public static int timernotZetonH = 0; //для хоппера отдельный жетон

        public static string statHopper;

        public static int isSlave = 0;                // Slave работает = 1, нет = 0
        public static int isReadIn = 0;               // ридер IN работает = 1, нет = 0
        public static int isReadOut = 0;              // ридер OUT работает = 1, нет = 0
        public static int isDispens = 0;              // Диспенсер работает = 1, нет = 0
        public static int isCollector = 0;
        public static int isBarcode = 0;               // Штрихкод работает = 1, нет = 0
        public static int isTablo = 0;                // Tablo работает = 1, нет = 0
        public static int isWiegand = 0; //Виганд работает =1, нет =0
        public static int isHopper = 0;

        #endregion

        #region integer_timers
        public static int timer_riderOut = 0;         // таймер обмена с ридером * 100 мс
        public static int timer_riderIn = 0;          // таймер обмена с ридером * 100 мс
        public static int timer_dispens = 0;          // таймер обмена с диспенсером * 100 мс
        public static int timer_slave = 0;            // таймер обмена с Slave-контроллером * 100 мс
        public static int timer_diskret = 0;            // таймер обмена с Slave Diskret
        public static int timer_noCard = 0;          // таймер "нет карт" * 100 мс
        public static int timer_Tablo = 0;            // таймер обмена с Tablo-контроллером * 100 мс
        public static int timer_TabloTwo = 0;            // таймер обмена с Tablo-контроллером * 100 мс

        public static int timer_diskretT = 0;            // таймер обмена с Tablo Diskret
        public static int timer_diskretT_Two = 0;

        public static int timer_TPrint = 0;
        public static int timer_TAlarm = 0;
        public static int timer_TScan = 0;
        public static int timer_TScanCommand = 0;

        public static int timer_checkStat = 0;        // таймер проверки статуса * 100 мс

        public static int timer_kart_v;           // таймер карты в губах * 100 мс
        public static int timer_barier;           // таймер импульса управления шлаг * 100 мс
        public static int timer_barier2;           // таймер импульса управления шлаг * 100 мс
        public static int timer_display;          // таймер задержки сообщений на дисплее * 100 мс
        public static int timer_notLoopA;         // таймер не loopA * 100 мс
        public static int timer_notLoopB;         // таймер не loopB * 100 мс
        public static int timer_notIr;            // таймер не ИК * 100 мс
        public static int timer_Wiegand; //таймер виганд * 100 мс
        public static int timer_LoopA = 1000;         // таймер loopA * 100 мс
        public static int timer_LoopB = 1000;         // таймер loopB * 100 мс
        public static int timer_IR = 1000;         // таймер ИК * 100 мс
        public static int _timer_LoopA = 1;
        public static int _timer_LoopB = 1;
        public static int _timer_IR = 1;
        #endregion

        #region Константы
        public const int time_display = 40;        // время задержки сообщений на дисплее * 100 мс
        public const int time_checkStat = 2;        // время таймер проверки статуса * 100 мс
        public const int time_slave = 3;           // таймаут для statSlave = "не отвечает"  * 100 мс
        public const int time_Tablo = 3;           // таймаут для statTablo = "не отвечает"  * 100 мс
        public const int time_diskret = 5;        // время таймера обмена с Slave * 20 мс 
        public const int _Npricep = 1;             // число прицепов
        public const int timeSync = 20;          // время таймер синхронизации БД * 100 мс
        public const int time_noCard = 50;         // время ожидания карты * 100 мс

        #endregion

        #region DB
        public static Entities db;

        public static string conlocal = "";
        public static string conserver = "";
        public static string EntitiesCS = "";

        public static string conlocal0 = "Data Source=; Initial Catalog=rpsMDBG; User Id=sa; Password=";
        public static string conserver0 = "Data Source=; Initial Catalog=rpsMDBG; User Id=sa; Password=";
        public static string Entities0 = "metadata=res://*/RPS_Shared.csdl|res://*/RPS_Shared.ssdl|res://*/RPS_Shared.msl;provider=System.Data.SqlClient;provider connection string=\"data source=;initial catalog=rpsMDBG;persist security info=True;user id=sa;password=;MultipleActiveResultSets=True;App=EntityFramework\"";
        public static string Password;

        public static bool RunBdOk = false;
        #endregion

        #region Recognition
        //public static int PlateRecognizeMode;
        public static string RfidPlate;
        public static bool CheckTag;

        public static byte statBarcodeVirt = 0;

        public static string LastPlate="";
        public static string LevenPlate = "";

        public static bool ViinexRequestSended = false;
        public static bool RecognizeNeed = true;
        public static int ViinexAttempts = 0;
        public static int ViinexAttempts2 = 0;
        public static int ViinexAttempts3 = 0;

        public static bool IsRecognized = false;

        public static bool PenaltyCardRecognition = false;

        public static bool PenaltyCardPassWithoutSync = false;

        public static bool PenaltyLastRecountSync = false;

        public static bool isPlateControlDenyTransact = false;

        //public static int plateRecognizeMode;

        public static PlateControlStatus isPlateControl;

        public static bool IsNeedReset = true;
        public static bool BarcodeFromRecognize = false;

        public static bool NotR_Main = false;
        public static bool R_Main = false;

        public static bool NotR_BL = false;
        public static bool NotR_IO = false;
        public static bool NotR_PK = false;
        public static bool NotR_QR = false;

        public static bool R_BL = false;
        public static bool R_IO = false;
        public static bool R_PK = false;
        public static bool R_PK2 = false;
        public static bool R_PK3 = false;
        public static bool R_QR = false;

        public static string qridVirtual = "";

        public static TimeSpan DateRaspos;        // дата распознавания
        public static int DateRasposMax = 10;    // 10 sek


        public static int SnapshotsLoopA = 0;

        public static bool ServerAcceptPicture = false; //Сервер принял картинку
        public static bool ServerSendPlate = false; //Сервер отправил номер
        public static bool ClientSendPlate = false;
        public static bool OperatorImageDownloaded = false;

        public static bool UseLoopAServer = false;

        public static bool TransportTCP = false;
        public static bool TransportUDP = false;

        public static bool TransportTCP2 = false;
        public static bool TransportUDP2 = false;


        public static bool Snap1 = false;
        public static bool Snap2 = false;

        public static decimal RoiLeft;
        public static decimal RoiTop;
        public static decimal RoiRight;
        public static decimal RoiBottom;

        #endregion

        #region Lang
        public static string[] language = null;
        public static string[] Language = null;
        public static string language1 = "RU";
        public static string language2 = "RU";
        public static string language3 = "RU";
        #endregion

        #region Alarms
        public static bool OneAlarmEnabled = false; //NewAlarmE no=не писать Не стандартные проезды в тревоги, yes=писать

        public static int OneAlarmTime = 1;

        //public static int NewAlarmMax = 18;
        //public static int[] timerAlarm = new int[20]; // таймеров больше или равно NewAlarmMax+1
        //public static int timeNewAlarm = 40;  // время индикации Не стандартные проезды в тревоги

        public static bool Alarm281Enabled = false;
        public static bool Alarm282Enabled = false;
        public static bool Alarm283Enabled = false;
        public static bool Alarm284Enabled = false;
        public static bool Alarm285Enabled = false;
        public static bool Alarm286Enabled = false;
        public static bool Alarm287Enabled = false;
        public static bool Alarm288Enabled = false;
        public static bool Alarm289Enabled = false;
        public static bool Alarm290Enabled = false;
        public static bool Alarm291Enabled = false;
        public static bool Alarm292Enabled = false;
        public static bool Alarm293Enabled = false;

        public static bool Alarm170Enabled = false;

        public static bool Alarm171Enabled = false;
        public static bool Alarm172Enabled = false;

        public static int timeAlarm = 30;  //задержку перед записью ошибки в БД, *100мс
        public static int timerAlarm101 = 0;
        public static int timerAlarm102 = 0;
        public static int timerAlarm103 = 0;
        public static int timerAlarm104 = 0;
        public static int timerAlarm105 = 0;
        public static int timerAlarm106 = 0;
        public static int timerAlarm107 = 0;
        public static int timerAlarm108 = 0;
        public static int timerAlarm109 = 0;
        public static int timerAlarm110 = 0;
        public static int timerAlarm111 = 0;
        public static int timerAlarm112 = 0;
        public static int timerAlarm113 = 0;

        public static int timerAlarm116 = 0;
        public static int timerAlarm117 = 0;
        public static int timerAlarm118 = 0;
        public static int timerAlarm336 = 0;
        public static int timerAlarm244 = 0;
        public static int timerAlarm154 = 0;

        //по старинке
        public static int timerAlarm121 = 0;
        public static int timerAlarm122 = 0;
        public static int timerAlarm123 = 0;
        public static int timerAlarm124 = 0;
        public static int timerAlarm125 = 0;
        public static int timerAlarm126 = 0;
        public static int timerAlarm127 = 0;
        public static int timerAlarm128 = 0;
        public static int timerAlarm129 = 0;
        public static int timerAlarm130 = 0;
        public static int timerAlarm131 = 0;
        public static int timerAlarm132 = 0;
        public static int timerAlarm133 = 0;
        public static int timerAlarm134 = 0;
        public static int timerAlarm135 = 0;
        public static int timerAlarm136 = 0;
        public static int timerAlarm137 = 0;
        public static int timerAlarm138 = 0;

        public static int timerAlarm191 = 0;
        public static int timerAlarm192 = 0;

        //детка, я сегодня гусь

        public static bool Alarm152Genered = false;
        public static bool Alarm155Genered = false;
        public static bool Alarm166Genered = false;

        public static bool Alarm167Genered = false; //жетон застрял...

        #endregion

        public static bool ZetonAway; //жетон покинул жетонопровод

        #region Hardware
        public static int comDiskret;             // команда в дискрет шлаг.вниз=1, шлаг.вверх=2, светофор GRIN=4. светофор RED=8, подсвет кнопки нажмите=16, 
        //... реверс =1024+2048
        public static int _comDiskret;            // старая команда
        #endregion

        #region Other
        public static string IP = "";
        public static List<string> IpList;

        public static Aes myAes;

        public static float level;                    // уровень звука

        public static bool Dop; //флажок карты в корзину в отладке
        public static int Itog;                   // итог, рассчитаный калькулятором = баланс - задолженность


        public static Guid DeviceIdG;

        public static bool LicenseWork = false;
        public static bool _LicenseWork = false;
        public static bool LicenseWorkDebug = false;
        public static DateTime TimeLicenseWorkDebug;

        public static int isBarCodeUsed = 0;      // =0-нет QRid,=1-есть, =2-есть в BarCodeInUse, =3-в BarCodeUsed ("Used") == false|null, =4-иначе, =5 - просрочен
        public static int _isBarCodeUsed = 0;     // последний в данном проезде
        #endregion



        #region New_Variables

        public static bool discount_start = true;

        public static bool isCheckDiscount = false;

        // public static byte RackOrientation=0; //0-обычная; 1-вертикальная
        public struct Discount
        {
            public string t;
            public string T;
            public int S;
            public string fn;
            public string fp;
            public DateTime dt; //из t и T
        }

        /*
         Генерируем BCTransaction В
которой заполняем: idBCM,
CompanyId, IdTP+IdTS ( Если есть из
BCM), UsageTime (now), CreatedAt
(now), DiscountAmount Если есть
в BCM)
IdTPForCount+IdTSForCount ( Из
BCM), CardId, Used=True,
ActivatedAt (now),
ParckingEnterTime, IsDeleted=0, FP
+BarCodeInUse
         */

        public struct CheckDiscountTransact
        {
            public string idBCM; //GUID???
            public string CompanyId; //GUID???
            public int? IdTP;
            public int? IdTS;
            public DateTime UsageTime;
            public DateTime CreatedAt;
            public int? DiscountAmount;
            public Guid? IdTPForCount;
            public Guid? IdTSForCount;
            public long CardId;
            public bool Used;
            public DateTime ActivatedAt;
            public DateTime ParkingEnterTime;
            public bool IsDeleted;
            public string FP_ExternalId;
        }

        public static CheckDiscountTransact CheckDiscount;

        public static Guid? discount_guid;
        public static Guid? discount_guid2;

        public static int RackSource = 0;

        public struct RackSettings
        {
            public string id;
            public string device_id;
            public string name;
            public string value;
            public int type;
        }

        public static List<RackSettings> RSet;

        public static int UpdaterY = 0;    // признак обновлятора

        public static List<RackSettings> DefSet;

        public struct LangSettings
        {
            public int id;
            public int DeviceType; //1
            public int InterfaceType; //1 и 2
            public int MaxLength;
            public string RU;
            public string EN;
            public string ZH;
            public string ES;
            public string PT;
            public string HI;
            public string AR;
            public string IT;
            public string DE;
            public string FR;
            public string IN;
            public string KZ;
            public string UZ;
            public string Econom_RU;
            public string Econom_EN;
            public int EconomColor;
            public int EconomPicture;
        }

        public static List<LangSettings> LangSet; //загруженный набор

        public static List<LangSettings> LangDefSet; //дефолтный набор

        public static int CurrentLanguage;
        public static int SecondLanguage;

        public static Dictionary<int, string> CurrentLangDictionary;
        public static Dictionary<int, string> SecondLangDictionary;

        //ViinexSettings
        public static string GetURI;
        public static string ViinexLicenseKey;
        public static string CameraURI;
        public static string CameraLogin;
        public static string CameraPassword;
        public static string CameraSkip;
        public static string CameraPreProcess;
        public static string CameraPostProcess;
        public static string CameraIP;

        public static string CameraType;

        public static bool IsAcquireEvents;
        public static bool IsAcquireVideo;

        public static int ViinexConfidence;
        public static string ViinexSettingsPath;
        public static byte ViinexTransliter; //0-латиница; 1-кириллица
        public static byte MlnTransliter; //0-латиница; 1-кириллица

        //ViinexSettings #2
        //public static string GetURI2;
        //public static string ViinexLicenseKey2;
        public static string CameraURI2;
        public static string CameraLogin2;
        public static string CameraPassword2;
        public static string CameraSkip2;
        public static string CameraPreProcess2;
        public static string CameraPostProcess2;
        public static string CameraIP2;

        public static int BadPing1 = 0;
        public static int BadPing2 = 0;

        public static string CameraType2;

        public static bool IsAcquireEvents2;
        public static bool IsAcquireVideo2;

        public static int ViinexConfidence2;
        public static string ViinexSettingsPath2;
        public static byte ViinexTransliter2; //0-латиница; 1-кириллица

        public static bool Camera2Enabled;

        public static bool GetSnapshots;

        public static bool SnapshotRequested;

        public static int TwoLevelRequest; //0-не работает,1- слушаем, 2- отправляем
        public static string TwoLevelSendIP;
        public static string TwoLevelReceiveIP;

        public static int TimeBudget;

        //LoopANotification
        public static string LoopANotificationUri;

        public static int SavePhoto = 0;
        public static string SavePhotoDir = "";
        public static int SavePhotoTimeout = 500;

        public static bool TrainReverse;

        //Control + Levenshtein
        public static bool ControlBL;
        public static bool ControlIO;
        public static bool LevenBL;
        public static bool LevenIO;
        public static bool LevenPK;
        public static bool LevenQR;

        public static int RFIDType; //0-Wiegand; 1-TCP/IP Тип RFID (ISBC-TCP или RPS-wiegand)
        public static int AntennaNumber; //1-4
        public static string RFIDRackType; //Host или Slave
        public static string RFIDTransponderIP;
        public static string RFIDTransponderPort;
        public static string RFIDHostIP;

        //РФИД метки с антенн
        public static string A1RfidLabel;
        public static string A2RfidLabel;
        public static string A3RfidLabel;
        public static string A4RfidLabel;

        public static int ScrOrientation;

        public static string RackMode = "";
        public static string RackType = "";

        public static TimeSpan proTimeT;

        public static bool PK_Opened = false;

        public static int RfidSleep = 0;

        public static bool FreewayRed = false;

        public static DateTime? LoopABusyTime = null;

        //Настройки Малленома
        //#1
        public static double MlnPointX1_1 = 0;
        public static double MlnPointY1_1 = 0;

        public static double MlnPointX2_1 = 0;
        public static double MlnPointY2_1 = 0;

        public static double MlnPointX3_1 = 0;
        public static double MlnPointY3_1 = 0;

        public static double MlnPointX4_1 = 0;
        public static double MlnPointY4_1 = 0;

        public static double MlnPointX5_1 = 0;
        public static double MlnPointY5_1 = 0;

        public static double MlnPointX6_1 = 0;
        public static double MlnPointY6_1 = 0;

        public static double MlnPointX7_1 = 0;
        public static double MlnPointY7_1 = 0;

        public static double MlnPointX8_1 = 0;
        public static double MlnPointY8_1 = 0;

        public static double MlnMinSizeW_1 = 0;
        public static double MlnMinSizeH_1 = 0;

        public static double MlnMaxSizeW_1 = 0;
        public static double MlnMaxSizeH_1 = 0;

        //#2
        public static double MlnPointX1_2 = 0;
        public static double MlnPointY1_2 = 0;

        public static double MlnPointX2_2 = 0;
        public static double MlnPointY2_2 = 0;

        public static double MlnPointX3_2 = 0;
        public static double MlnPointY3_2 = 0;

        public static double MlnPointX4_2 = 0;
        public static double MlnPointY4_2 = 0;

        public static double MlnPointX5_2 = 0;
        public static double MlnPointY5_2 = 0;

        public static double MlnPointX6_2 = 0;
        public static double MlnPointY6_2 = 0;

        public static double MlnPointX7_2 = 0;
        public static double MlnPointY7_2 = 0;

        public static double MlnPointX8_2 = 0;
        public static double MlnPointY8_2 = 0;

        public static string MlnMinSizeW_2 = "";
        public static string MlnMinSizeH_2 = "";

        public static string MlnMaxSizeW_2 = "";
        public static string MlnMaxSizeH_2 = "";

        public static int MlnConfidence_1 = 0;
        public static int MlnConfidence_2 = 0;

        public static double MlnPreprocess_1 = 0;
        public static double MlnPreprocess_2 = 0;

        public static string MlnConString_1 = "";
        public static string MlnConString_2 = "";

        public static double MlnAngle_1 = 0;
        public static double MlnAngle_2 = 0;

        public static bool MlnUseCamera2 = false;

        public static bool CheckZoneRecognition = false;
        public static bool CheckZoneRFID = false;
        public static bool CheckZoneHID = false;

        public static double RecognitionTime = 0;

        public static bool ScreenBlockedByHost = false;

        public static bool ParkPassExists = false;
        public static int ParkPassPort = 4;

        public static int WaitForPayment;

        public static int BMCounter;

        #endregion

        #region Zetons
        public static bool ExitSensor1 = false;
        public static bool ExitSensor2 = false;
        public static bool ExitSensor3 = false;

        public static bool EntrySensor1 = false;
        public static bool EntrySensor2 = false;
        public static bool EntrySensor3 = false;

        public static bool DelayedDrop = false;
        public static int DelayedDropCounter = 0;

        public static bool DelayedPass = false;
        public static int DelayedPassCounter = 0;

        public static int HopperAttempts=0;

        #endregion


        #region KKM Settings

        public static bool From444 = false;
        public static bool From3441 = false;

        public static string KKMPassword="";
        public static string KKMModePassword="";
        public static string CashierNumber="";
        public static int? ChequeWork=0;
        public static bool ShiftOpened = false;

        public static int? ShiftNumber = 1;        // № смены

        public static int CurrentTransactionNumber = 1;   // № чека в смене

        public static DateTime? LastShiftOpenTime;


        public static int? VAT;

        public static int KKMFiskalVersion;

        public static bool KKMExists;
        public static int ComKKM;

        public static bool PrinterExists;

        public static bool AutoSmena;

        public static DateTime AutoSmenaTime;

        public static bool AutoSver;

        public static DateTime AutoSverTime;

        public static bool isBadKKM = false;

        //deviceCMMModel - сделать оттуда настройки наши!!!
        //В гетсеттингс
        #endregion

        #region ParkPass

        public static string PPCardNumber;

        public static string PPZone; //въезд-выезд

        public static byte ParkPassExitResult;

        #endregion

        public static int WiegandType = 0;

        public static int SlaveType = 0;

        public static int NewVoice=0;

        public static bool CompanySession = false;

        public static int CompanyTSId;
        public static int CompanyTPId;

        public static bool DoCompanyTransaction = false;

        public static bool ShowCompanyScreen = false;

        public static double CompanySessionAmount=0;

        public static int? MaxDebtAmount;

        public static int ReverseMode=1;

        public static string RemoteReverseIP = "";

        //public static int RemoteReverseCarCount = 1; //вот эта переменная...

        public static int CarsInRamp=0;
        public static int oldCarsInRamp = 0;

        public static bool RemotePlusSended = false;
        public static bool RemoteMinusSended = false;

        public static bool FullEntrance = false; //проезд...

        public static int tmrNippelNotB;
        public static int tmrNippelNotAll;

        public static int Zadolg = 0;
        //public static int ZadolgPer = 0;

        public static int tmrAbonBlock;

        public static bool TwoFactorsIdentity;

        public static bool TicketPrinterWorks = false;
        public static bool TicketScannerWorks = false;

        public static string e2Phrase = "";
        public static bool Fr4 = false;

        public static bool ControlLoopBRev = false;

        public static bool QRDiscount = false;

        //работает след образом.
        //если машина заехала через эту стойку, то на ней каринрамп +1
        //на соседнюю стойку эта шлет команду "минус", на той становится -1 и блокируется выезд (экран) +
        //переключение в режим нипеля происходит

        //когда проезжает машина по нипельной стойке, на ней счетчик минусуется плюс шлет на исходную команду с минусом!

        //Итого имеем две команды и логику достижения нуля, или ухода от нуля
    }
}
