using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Security.Cryptography;
using System.Net;
using System.ComponentModel;
using System.Data.SqlClient;
using RPS.ServiceLicencee.Compute;
using Communications;
using RPS_Shared;
using EventWeb;
using System.ServiceProcess;
using System.Text;
using System.Media;
using NAudio.Wave;
using Microsoft.Win32;
using CommonClassLibrary;
using Rps.ShareBase;
using System.Collections.Generic;
using System.Security.Principal;
using System.Diagnostics;
using System.Drawing;
using Newtonsoft.Json;
using System.Reflection;
using Dapper;
using System.Threading.Tasks;
using CalculatorTS;
using static stoyka01.GlobalClass;
using System.Text.RegularExpressions;
using RPS.TariffCalc;
using System.Web;

namespace stoyka01
{
    public partial class Form1 : Form
    {
            //my_new
        bool BarrierDownProceed = false;
        int BarrierDownTimer = 0;
        bool BarrierDefender = false;
        bool BarrierDownTransact = false;

        string oldstatDispensText = "";

        int showgosscreen = 0;

        bool QRButtonPressed=false;
        int ststTemp = 0;

        bool IgnoreChecking=false;
        bool BarcodeFromRecognize = false;

        int ButtonBlinker = 0;

        int AddFreeTime = 0;
        int LoopAAlarmsTime = 0;
        //old

        private int CardMoveCounter = 0;
        private bool CardMoved = false;
        private bool CardFromDebug = false;

        string Ver = "RPS™ Rack v. 1.102";
        string Build = "140218";

        int tmrPriglots = 0;

        
        private bool StopUpdate = true;

        //Для банк модуля в транзакцию
        decimal Paid = 0;
        int PaymentTransactionTypeID = 0; //Тип транзакции банк
        //bool BankCardAccepted = false; -не делаем

        #region переменные
        private int TabloNew = 0;       //0=старое табло, 1=табло ИНФОТАБ-3/24/5, 2=китай табло UDP

        private int OldTabloValue = 0;

        private bool ControlLoopB = true; // false=Нет контроля отката с loopB, true=есть контроль

        private bool NewAlarmE = false; //NewAlarmE no=не писать Не стандартные проезды в тревоги, yes=писать
        private int NewAlarmMax = 18;
        private int[] timerAlarm = new int[20]; // таймеров больше или равно NewAlarmMax+1
        private int timeNewAlarm = 40;  // время индикации Не стандартные проезды в тревоги

        private int timeAlarm = 30;  //задержку перед записью ошибки в БД, *100мс
        private int timerAlarm101 = 0;
        private int timerAlarm102 = 0;
        private int timerAlarm103 = 0;
        private int timerAlarm104 = 0;
        private int timerAlarm105 = 0;
        private int timerAlarm106 = 0;
        private int timerAlarm107 = 0;
        private int timerAlarm108 = 0;
        private int timerAlarm109 = 0;
        private int timerAlarm110 = 0;
        private int timerAlarm111 = 0;
        private int timerAlarm112 = 0;
        private int timerAlarm113 = 0;

        private int timerAlarm116 = 0;
        private int timerAlarm117 = 0;
        private int timerAlarm118 = 0;
        private int timerAlarm336 = 0;
        private int timerAlarm244 = 0;
        private int timerAlarm154 = 0;

        private bool Alarm152Genered = false;
        private bool Alarm155Genered = false;

        public int hid_timer = 0;

        private bool IsNeedReset = true;
        private string ViinexFile = "C:\\RPV\\1.jpg";
        private int numerViinex = 0;    // номер запроса
        private int numerViinexMax = 9; // номер запроса max
        private int Viinex = 0;         // 0=нет Viinex, 1=есть Viinex

        private int timerRasposPlate = 0; //таймер распознавания *100мс
        private int timeRasposPlate = 60; //максимальное время распознавания
        private int etapRasp = 0;       //этап распознавания Viinex
        //private string CameraLogin = "";    //
        //private string CameraPass = "";    //
        private int timerSnapshot = 0;   // таймер записи файла-картинки * 100 мс
        private int сameraTime = 60;    // ожидание записи файла-картинки * 100 мс

        bool RSUpdateTrigger = false;
        bool ViinexRequestSended = false;

        private int newInterface = 1;       // 0=старый, 1=новый 

        float SizeFontLanguage1 = 60;
        float SizeFontLanguage2 = 32;
        private int CardExitWrite = 1;          // перезапись разовых карт на выезде при отладке 1 = да, 0 = нет

        private int PlaseControlRazov = 0;   // 0 - нет контроля, 1 - есть

        internal bool IsRunAsAdmin()
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(id);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private int PlaseCompanyControlY = 0;   // 0 - нет контроля, 1 - есть
        private int guestCompany = 0;                   // занято гостями
        Guid CompanyIdGu;

        int i = 0;
        private int listC = 0;
        private int Altimetr1 = 0;              // высотомер1 =0 - свободен, =1 - занят
        private int Altimetr2 = 0;              // высотомер2 =0 - свободен, =1 - занят
        private int Altimetr3 = 0;              // высотомер3 =0 - свободен, =1 - занят

        private int isBankModule = 0;          // BankModule работает = 1, нет = 0 
        private int etapBankModule = 0;        // этап BankModule
        private int _etapBankModule = 0;        // этап BankModule old
        private int timerBankModule = 0;          // таймер BankModule * 100 мс
        const int timeBankModule = 600;           // время BankModule * 100 мс
        DateTime LastBankModuleTime = new DateTime(2000, 1, 1, 0, 0, 0, 0);     //  время начала сеанса работы  BankModule
        CommonBankModule BankModule;
        private bool WorkKeyLoaded;
        private int CurrentTransactionNumber = 1;   // № чека в смене
        DateTime WorkKeyCheckTime = new DateTime(2000, 1, 1, 0, 0, 0, 0);     //  
        private int ShiftNumber = 1;        // № смены
        BankModuleStatusT BankModuleStatus;
        private bool BankModuleStatusChanged = false;
        private DateTime BankCardErrorDateTime;
        private string TerminalStatusLabel;
        private int BankSumMax = 1000;


        int viinexTimeout = 2000;

        private int isPlaseCompanyControl = 0;     // результат проверки мест для компании: 0 -есть место, 1-нет мест для компании,

        // Обмен с ридером штрихкода  
        private int etapBarcode = 0;            // этап ридера внешнего
        private int timer_riderBarcode;            // таймер обмена с ридером * 10 мс
        private string _comReadBarcode = "";        // старая команда
        private string comReadBarcode;        // новая команда
        private string nameBarcode;     // имя СОМ порта
        private string qrid = "";                          //  QRcode
        private string _qrid = "";                          //  QRcode последний
        private string _qridL = "";                          //  QRcode последний в лог
        private string _statBarcode = "";           // статус Штрихкода для логики
        private string statBarcode = "";           // статус Штрихкода для обмена
        private string _statBarcodeW = "";      // статус Штрихкода от web
        private string _statBarcodeL = "";           // статус Штрихкода для лога
        private string statBarcodeL = "";           // статус Штрихкода для лога
        private string _comReadBarcodeL = "";        // старая команда для лога
        private string comReadBarcodeL = "";        // новая команда для лога

        private int isBarCodeUsed = 0;      // =0-нет QRid,=1-есть, =2-есть в BarCodeInUse, =3-в BarCodeUsed ("Used") == false|null, =4-иначе, =5 - просрочен
        private int _isBarCodeUsed = 0;     // последний в данном проезде
        RT200Barcode RT200Barcode = new RT200Barcode();

        Guid Transactions1Gu;

        string[] logs = new string[logBuf + 20];
        int[] logLevel = new int[logBuf + 20];             // уровень лога для текущей записи

        int logNew = 0;             // номер посылки последней записи  в лог
        int logOld = 0;             // номер записанной в файл посылки
        int logTyp = 2;         // тип лога =1 - локальны в файл, =2 - в базу
        protected CommonClassLibrary.Log log;      // лог типа 2
        int logNastroySelectedIndex = 0;

        private int DisplayZon = 0;     // число свободных мест на табло
        private DateTime LastDisplayTime = new DateTime(2000, 1, 1, 0, 0, 0, 0);     //  время последнего вывода на табло
        private int DisplayTime = 1;          // период вывода на табло в секундах 
        private DateTime LastSignatureTimeT = new DateTime(2000, 1, 1, 0, 0, 0, 0);     //  время последнего чтения SignatureTablo

        

        private bool BaseImportRun;     // статус синхронизации

        private int TimerСhecker;       // таймер программы проверки
        private int etapСhecker;       // этап программы проверки
        private int _etapСhecker;       // этап программы проверки старый
        private int TimeСhecker = 200;       // тайм программы проверки * 100 мс
        private int etapEtapСhecker;       // подэтап программы проверки

        private bool RunBdOk = false;
        private int TimerRunBd;         // попытка связи с БД
        private int TimeRunBd = 250;    // *20мс
        private bool GetDatOk = true;

        int viinexConfidence = 98;

        private int UpdateE = 0;    // признак (этап) запроса на обновление
        private int CardsOnline = 0;    // признак проверки обновления данных с карт по базе
        bool UpdateNeeded = false;
        SqlConnection upddb;

        private bool LicenseWork = false;
        private bool _LicenseWork = false;
        private bool LicenseWorkDebug = false;
        DateTime TimeLicenseWorkDebug;

        string[] language = null;
        string[] Language = null;
        string language1 = "RU";
        string language2 = "RU";
        string language3 = "RU";

        string ServerUrlS;
        string IP = "";

        private bool TalkInit = true;
        private int InvertCall = 0;     // инверсия кнопки "Вызов"
        private int ButtonPushPort = 1; // порт шлагбаума = 1, порт двери = 2
        private int Reverse = 1;        // reverse = 1, двухуровневая стойка = 0
        private int InvertAltimetr = 0;     // инверсия IN Altimetr

        private static EventWebServer webServer = null;

        EventWebClient CheckEventWebClient, CardReaderEventWebClient, TalkOperatorEventWebClient;
        BackgroundWorker CheckEventServerBackgroundWorker, CardReaderEventSenderBackgroundWorker, TalkOperatorEventSenderBackgroundWorker, SinglePassageEventSenderBackgroundWorker, LogsWriterBackgroundWorker;
        bool EventWebClientBackgroundWorkerInitialised = false, CardReaderEventSenderBackgroundWorkerInitialised = false, TalkOperatorEventSenderBackgroundWorkerInitialised = false, SinglePassageEventSenderBackgroundWorkerInitialised = false;
        bool EventWebServer;

        public OperatorTalk Talk;
        public SinglePassage SinglePassageEventSender;

        private string conlocal = "";
        private string conserver = "";
        private string EntitiesCS = "";

        private string conlocal0 = "Data Source=; Initial Catalog=rpsMDBG; User Id=sa; Password=";
        private string conserver0 = "Data Source=; Initial Catalog=rpsMDBG; User Id=sa; Password=";
        private string Entities0 = "metadata=res://*/RPS_Shared.csdl|res://*/RPS_Shared.ssdl|res://*/RPS_Shared.msl;provider=System.Data.SqlClient;provider connection string=\"data source=;initial catalog=rpsMDBG;persist security info=True;user id=sa;password=;MultipleActiveResultSets=True;App=EntityFramework\"";

        public DeviceModel device;

        bool BCOneMsg = false;


        string Password;

        public Aes myAes;

        private string StatTalk = "";       // статус связи с оператором
        private int timerTalk = 0;          // таймер вызова оператора * 100 мс
        const int timeTalk = 200;           // время вызова оператора * 100 мс

        private DateTime? timeExit = null;
        private DateTime timeEntry;

        private int etap = 0;               // этап проезда (статус стойки) 
        private int _etap = 0;              // этап проезда (статус стойки) для лога
        private int etapOut = 0;            // этап ридера внешнего
        private int etapSlave = 0;          // этап Slave
        private int etapDispens = 0;        // этап диспенсера
        private int etapIn = 0;             // этап ридера внутри
        private int etapOtboy = 0;          // этап проезда с отбоем стрелы 
        private int _etapOtboy = 0;          // этап проезда с отбоем стрелы для лога
        private int etapF = 0;        // этап сканера штрих
        private int etapTablo = 0;        // этап Tablo
        private int etapWiegand = 0; //этап Wiegand
        public Entities db;
        public string ConnectionString;
        public DeviceModel DeviceModel; // 
        public DeviceRackModel DeviceRackModel; // 
        public bool GrEnabled = false;

        public string HIDCardId = "";
        public bool hid_success = false;
        public bool hid_blocked = false;
        Guid? ClientGroupIdGu;
        Guid DeviceIdG;
        Guid? ZoneIdCGu;
        Guid? TarifiTSgu;
        Guid? TarifiTPgu;
        private byte cardBlocked = 0;

        public bool DelayRfidTablo = false;

        internal enum TimeType : int
        {
            Minutes = 1,
            Hours = 2,
            Days = 3,
            Weeeks = 4,
            Months = 5
        }

        private long fileL = 0;               //размер лога
        private long fileLS = 0;               //размер лога синхр

        #region статус
        // старый статус. Пишем из нового статуса в alarmR();       // запись тревог в БД
        private string _statDispens;        // статус диспенсера карт
        private string _statReadIn;         // статус ридера карт внутри (разового клиента)
        private string _statReadOut;        // статус ридера карт снаружи (постоянного клиента)
        private int _statDiskret;           // статус loopA=1, ИК=2, loopB=4, кнопка "НАЖМИТЕ"=8,  
        // кнопка "ВЫЗОВ"=16, датчик верхней двери=32,  датчик нижней двери=64
        private string _statusCard = "";       // статус карт есть/мало/нет

        // старый статус. Пишем из нового статуса в log()
        private string l_statDispens;        // статус диспенсера карт
        private string l_statReadIn;         // статус ридера карт внутри (разового клиента)
        private string l_statReadOut;        // статус ридера карт снаружи (постоянного клиента)
        private int l_statDiskret;           // статус loopA=1, ИК=2, loopB=4, кнопка "НАЖМИТЕ"=8,  
        // кнопка "ВЫЗОВ"=16, датчик верхней двери=32,  датчик нижней двери=64
        private string l_statusCard = "";       // статус карт есть/мало/нет


        // новый статус. Пишем из ComboBox и statDiskret в log()
        private string statDispensN;        // новый () статус диспенсера
        private string statReadInN;         // новый () статус ридера внутри (разового клиента)
        private string statReadOutN;        // новый () статус ридера снаружи (постоянного клиента)        
        private int statDiskretN;           // новый loopA=1, ИК=2, loopB=4, кнопка "НАЖМИТЕ"=8,  
        // кнопка "ВЫЗОВ"=16, датчик верхней двери=32,  датчик нижней двери=64, ...=128
        private string statusCard = "";       // новый статус карт есть/мало/нет

        private string statDispensText;     //
        private string statDispensLanguage; //
        private string statReadInText;     //
        private string statReadInLanguage; //
        private string statReadOutText;     //
        private string statReadOutLanguage; //
        private string statusCardLanguage; //
        private string statSlaveLanguage; //
        private string statReadBarcodeText;     //


        private string statSlave;          // статус Slave текстовый
        private string _statSlave;          // статус Slave текстовый старый
        private string l_statSlave;          // статус Slave текстовый старый в log()

        private string statTablo;          // статус Tablo текстовый


        private string statWiegand; // статус Slave текстовый

        string statLoopA = "";              //
        string statLoopB = "";              //
        string statIR = "";                 //
        string statButPush = "";            // кнопка "НАЖМИТЕ"
        string statButCall = "";            // кнопка "ВЫЗОВ"
        string statDoorUp = "";             //
        string statDoorDn = "";             //
        // новый статус.

        string statLoopAN = "";              //
        string statLoopBN = "";              //
        string statIRN = "";                 //
        string statButPushN = "";            // кнопка "НАЖМИТЕ"
        string statButCallN = "";            // кнопка "ВЫЗОВ"
        string statDoorUpN = "";             //
        string statDoorDnN = "";             //
        // старый статус.
        string _statOut;                     //
        string _statLoopA = "";              //
        string _statLoopB = "";              //
        string _statIR = "";                 //
        string _statButPush = "";            // кнопка "НАЖМИТЕ"
        string _statButCall = "";            // кнопка "ВЫЗОВ"
        string _statDoorUp = "";             //
        string _statDoorDn = "";             //

        private int RackTypeIndex = 1; // изменение типа стойки с сервера
        private int _RackTypeIndex = 0; // для лога

        private int RackModeIndex = 1; // изменение типа стойки с сервера
        private int _RackModeIndex = 0; // для лога
        #endregion


        private bool CollectorInits = false;


        private int stat_barier;            // последняя команда в шлагбаум шлаг.вниз=1, шлаг.вверх=2,

        private int timer_riderOut = 0;         // таймер обмена с ридером * 100 мс
        private int timer_riderIn = 0;          // таймер обмена с ридером * 100 мс
        private int timer_dispens = 0;          // таймер обмена с диспенсером * 100 мс
        private int timer_slave = 0;            // таймер обмена с Slave-контроллером * 100 мс
        private int timer_diskret = 0;            // таймер обмена с Slave Diskret
        private int timer_noCard = 0;          // таймер "нет карт" * 100 мс
        private int timer_Tablo = 0;            // таймер обмена с Tablo-контроллером * 100 мс
        private int timer_diskretT = 0;            // таймер обмена с Tablo Diskret

        private int timer_checkStat = 0;        // таймер проверки статуса * 100 мс

        private int timer_kart_v;           // таймер карты в губах * 100 мс
        private int timer_barier;           // таймер импульса управления шлаг * 100 мс
        private int timer_display;          // таймер задержки сообщений на дисплее * 100 мс
        private int timer_notLoopA;         // таймер не loopA * 100 мс
        private int timer_notLoopB;         // таймер не loopB * 100 мс
        private int timer_notIr;            // таймер не ИК * 100 мс
        private int timer_Wiegand; //таймер виганд * 100 мс
        private int timer_LoopA = 1000;         // таймер loopA * 100 мс
        private int timer_LoopB = 1000;         // таймер loopB * 100 мс
        private int timer_IR = 1000;         // таймер ИК * 100 мс
        private int _timer_LoopA = 1;
        private int _timer_LoopB = 1;
        private int _timer_IR = 1;

        bool rfid_success = false;
        bool rfid_blocked = false;
        bool rfid_blocked2 = false;

        private int isControl;             // результат проверки 0 == пропуск, 1-блокировка в базе, 2-нет в базе
        private int isControlGroup;        // результат проверки 0 == пропуск, 1-нет group в базе

        string oldLabel = "";
        byte oldcheckstate = 0;




        private TimeSpan tt;                // преобразование datetime и int
        int ststA;                          // для проверки статуса
        int _ststA;                          // для проверки статуса

        private int isSlave = 0;                // Slave работает = 1, нет = 0
        private int isReadIn = 0;               // ридер IN работает = 1, нет = 0
        private int isReadOut = 0;              // ридер OUT работает = 1, нет = 0
        private int isDispens = 0;              // Диспенсер работает = 1, нет = 0
        private int isBarcode = 0;               // Штрихкод работает = 1, нет = 0
        private int isTablo = 0;                // Tablo работает = 1, нет = 0
        private int isWiegand = 0; //Виганд работает =1, нет =0
        private int Itog;                   // итог, рассчитаный калькулятором = баланс - задолженность

        private int Npricep;                // число прицепов

        private int PropuskAvto = 0;            // число пропусков автомобиля НЕ МЕНЯТЬ!! на этапах=3...14 штатного и 6...14 блокировать (команда с сервера если...)
        private int _propuskAvto = 0;            // число пропусков автомобиля старое
        private int propuskAvtoCanc = 0;
        private int PropuskAvtoY = 1;            // пропуск автомобиля 1 - есть, 0 - нет

        #region данные с карты
        private byte[] cardIdCard = new byte[8];                // номер карты клиента
        private long CardIdCard;
        private string CardIdCardS;

        private byte ZonaDoCard = 0;        // зона на карте для записи транзакции  
        private DateTime DateSaveCard;      // Время последней записи на карту
        ToTariffPlan.DateCard _DateCard;       // данные с карты считанные и пишущиеся
        ToTariffPlan.DateCard _DateCard2;       // данные с карты после калькулятора (копия _DateCard) 
        ToTariffPlan.DateCard Save_DateCard;       // данные с карты сохраненные

        private bool NotR_Main = false;
        private bool R_Main = false;

        private bool NotR_BL = false;
        private bool NotR_IO = false;
        private bool NotR_PK = false;
        private bool NotR_QR = false;

        private bool R_BL = false;
        private bool R_IO = false;
        private bool R_PK = false;
        private bool R_PK2 = false;
        private bool R_PK3 = false;
        private bool R_QR = false;


        float SizeFontTimef = 12;

        private byte[] rBlock = new byte[48];               // чтение сектора карты

        private byte[] wBlock = new byte[48];               // запись сектора карты
        #endregion данные с карты

        private string comReadOut;         // новая команда (в очередь)
        private string _comReadOut;        // старая команда (выполняемая), comToReadOut.Text - последняя на выполнение
        private string nameOut;
        private int txOutSize = 0;
        private int rxOutSize = 0;
        Communications.MT625Reader OutReader = new Communications.MT625Reader();
        private byte[] txOut = new byte[256];               // передача в ридер внешний
        private byte[] rxOut = new byte[256];               // прием из ридер внешний

        private string comReadIn;         // новая команда (в очередь)
        private string _comReadIn;        // старая команда (выполняемая), comToReadOut.Text - последняя на выполнение
        private string nameIn;
        private int txInSize = 0;
        private int rxInSize = 0;
        Communications.MT625Reader InReader = new Communications.MT625Reader();
        private byte[] txIn = new byte[256];               // передача в ридер внутри
        private byte[] rxIn = new byte[256];               // прием из ридер внутри
        private int etapReadCard = 0;    // этап ридера карт

       
        private byte NumerSector;           // номер сектора

        private string nameSlave;
        // реальный статус пишем из устройств в ComboBox и statDiskret в obmen() 
        private int statDiskret;            // статус loopA=1, ИК=2, loopB=4, кнопка "НАЖМИТЕ"=8,  
        // кнопка "ВЫЗОВ"=16, датчик верхней двери=32,  датчик нижней двери=64, кнопка "НАЖМИТЕ"=128 (тип2),
        // Altimetr1=256, Altimetr2=512, Altimetr3=1024 ... реверс =2048

        Communications.Slave Slave = new Communications.Slave();
        private int reversTx = 0;            // передача "реверс занят" другой стойке нет=0, да =1  
        private int reversRx = 0;            // прием "реверс занят" от другой стойке нет=0, да =1
        private int _reversRx = 0;            // прием "реверс занят" до того
        private int _reversTx = 0;            // передача "реверс занят" до того


        private string SignatureSlave;          // SignatureSlave текстовый 
        private DateTime LastSignatureTime = new DateTime(2000, 1, 1, 0, 0, 0, 0);     //  время последнего чтения SignatureSlave
        private int SignatureTime = 10000;          // период опроса SignatureSlave в секундах 2,7часов
        private string versionSlave = "";          // версия ПО Slave текстовый 

        private string nameTablo;
        private string nameWiegand;

        string camUrl = "";
        string viinexUrl = "";

        // реальный статус пишем из устройств в ComboBox и statDiskret в obmen() 
        private int statDiskretTablo;            // статус loopA=1, ИК=2, loopB=4, кнопка "НАЖМИТЕ"=8,  
        // кнопка "ВЫЗОВ"=16, датчик верхней двери=32,  датчик нижней двери=64, ...
        private int comDiskretTablo;             // команда в дискрет шлаг.вниз=1, шлаг.вверх=2, светофор GRIN=4. светофор RED=8, подсвет кнопки нажмите=16
        private int _comDiskretTablo;            // старая команда
        Communications.Tablo Tablo = new Communications.Tablo();
        Communications.Tablo1 Tablo1 = new Communications.Tablo1();
        private int TabloAddress = 0;

        private string SignatureTablo;          // SignatureTablo текстовый 
        private DateTime LastSignatureTimeTablo = new DateTime(2000, 1, 1, 0, 0, 0, 0);     //  время последнего чтения SignatureTablo
        private int SignatureTimeTablo = 10000;          // период опроса SignatureTablo в секундах 2,7часов
        private string versionTablo = "";          // версия ПО Tablo текстовый 

        private string comDispens;         // новая команда (в очередь)
        private string _comDispens;        // старая команда (выполняемая), comDispens - последняя на выполнение
        private string nameDispens;
        private int etapDispensCard = 0;    // этап выдачи карт
        private int comDispensNum = 0;    // номер повторения команды при застревании карт
        DispenserMT166 Dispens = new Communications.DispenserMT166();

        OmniKey5427 omni = new Communications.OmniKey5427();


        //Коллектор
        Communications.MT163 Collector = new Communications.MT163();
        //private int etapCollector = 0;
        private string nameCollector;

        //Кит
        KYT2064 Dispens2 = new KYT2064();
        private string nameDispens2;

        private int firstDispens2 = 0;

        private int checkItog = 0;          // 2 клика по итогу для дебага

        WiegandAdapter Wiegand = new WiegandAdapter();

        CalculatorTS.ToTariffPlan CalculatorTS; // = new CalculatorTS.ToTariffPlan();

        public Int16 Type = 0;

        private int timer100mc = 0;
        private int timerSync = 20;
        DateTime Timer100mc;

        string timePro1;                // хронометраж
        string timePro2;
        string timePro3;
        string timePro4 = " ";

        private string oldKey = "";     // старый ключ для лога смены ключа

        private int test = 0;              // для проверки
        private string testS = "0";

        byte rfid_checkstate = 0;

       
        #endregion

        #region Константы
        const int time_display = 20;        // время задержки сообщений на дисплее * 100 мс
        const int time_checkStat = 2;        // время таймер проверки статуса * 100 мс
        const int time_slave = 3;           // таймаут для statSlave = "не отвечает"  * 100 мс
        const int time_Tablo = 3;           // таймаут для statTablo = "не отвечает"  * 100 мс
        const int time_diskret = 5;        // время таймера обмена с Slave * 20 мс 
        const int _Npricep = 1;             // число прицепов
        const int timeSync = 20;          // время таймер синхронизации БД * 100 мс
        const int time_noCard = 50;         // время ожидания карты * 100 мс

        public DateTime datetime0 = new DateTime(2000, 1, 1, 0, 0, 0, 0);  // НУЛЕВОЕ время
        #endregion

        private string logPath = @"LogStoyka.txt";
        const int logBuf = 200;

        SoundPlayer Audio = new SoundPlayer("Alarm08.wav");
        WaveInEvent waveIn = new WaveInEvent();
        private PaimentMethodT Paiments;

        TabloUDP Tablo2 = new TabloUDP();

        byte WiegandTransactionType = 1; //1 - проезд штат, 13-16 ошибки

        public Form1()
        {

        }


        //Инициализация Viinex
    

        // Protect the connectionStrings section.
      
        // Unprotect the connectionStrings section.
  
    
     
        private void Form1_Load(object sender, EventArgs e)
        {



        }

     
     
        #region Распознавание

        public Dictionary<int, string> PlateControlStatusString
        {
            get
            {
                var map = new Dictionary<int, string>
                {
                    {0, "Нет распознавания"},

                    {1, "Не распознан"},

                    {2, "Нет в черном списке"},

                    {3, "В черном списке"},

                    {4, "Черный список. Оператор"},

                    {5, "Черный список, не распознан, пропуск"},

                    {6, "Въезд-выезд, пропуск"},

                    {7, "Въезд-выезд, не распознан или не совпал"},

                    {8, "Въезд-выезд,  Оператор"},

                    {9, "Въезд-выезд,  не распознан, пропуск"},

                    {10, "ПК, пропуск"},

                    {11, "ПК, не прошел проверку"},

                    {12, "Штрихкод,  есть"},

                    {13, "Штрихкод,  нет"},

                    {14, "Штрихкод,  устарел"}
                };

                return map;
            }
        }

        private string _prevLastPlate = ""; // последний номер на предыдущем кругу

        private string _lastPlate = "";

        private DateTime? LastPlateTime = null;

        

        private string LastPlate // последний номер
        {
            get { return _lastPlate; }

            set
            {
                if (value != "")
                {
                    LastPlateTime = DateTime.Now;
                }

                if (value == "")
                {
                    _prevLastPlate = "";
                }
                else if (_lastPlate != value)
                {
                    _prevLastPlate = _lastPlate;
                }

                _lastPlate = value;
            }
        }

        private byte[] LastFrame = new byte[0];

        private byte[] FirstFrame = new byte[0];

        private string _LastPlate = ""; // последний распознаный гос. номер для лога

        CalculatorTS.ToTariffPlan.DateCard _VirtualCard;


        

        //Вернуться сюда и сделать чтение уже с моей таблицы!!!

        private int PlateRecognizeMode
        {
            //Высчитываем Value исходя из настроек
            get
            {
                return plateRecognizeMode;
            }
        }


        private string LastMsg;

        private bool ViinexStarted = false;

        private DateTime? StartRecognize = null;

        private DateTime? StopRecognize = null;

        private bool IsRecognized = false;

        private int recognizeCycle = 1;

        private Viinex viinex;

        private bool RecognizeNeed = true;
        private int ViinexAttempts = 0;
        private int ViinexAttempts2 = 0;
        private int ViinexAttempts3 = 0;

        private bool IsNeedRecognize
        {
            get
            {
                var need = PlateRecognizeMode > 0 && LastPlate == "" && etap == 2;

                return need;
            }
        }

        public static void LogToNLOG(params object[] args)
        {
#if DEBUG

#endif
        }

        

        // проверка распознаного гос. номера (
        // 1-въезд (черный список); 
        // 2-выезд (вьезд - выезд)
        // 3 въезд (постоянный по номеру);
        // 4-въезд (по штрихкоду)
      
      

      
        #endregion

        #region //RFID LOgic
      

      
        #endregion


        //Пустая!
        private void postCard3()                                // подготовка запись на карту постоянного клиента
        {
            // калькулятор

        }

        //Раз они пустые тут - убрать?
        private void cleaStatIn()                   // чистка статуса диспенсера и ридера внутри (разовый)
        {
            //statDispensText = "неопределен";
            //statReadInText = "неопределен";
        }

        private void cleaStatOut()                  // чистка статуса ридера внешнего (постоянный)
        {
            //statReadOutText = "неопределен";
        }

    

        #region ДЕБАГ КНОПКИ
        private void loopA_Click(object sender, EventArgs e)
        {
           
        }

        private void IR_Click(object sender, EventArgs e)
        {
           
        }

        private void loopB_Click(object sender, EventArgs e)
        {

        }

        private void PUSH_MouseDown(object sender, MouseEventArgs e)
        {
          
        }

        private void PUSH_MouseUp(object sender, MouseEventArgs e)
        {
           
        }
        #endregion


        //И тут события...
        private void buttonExit_Click(object sender, EventArgs e)
        {
           
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void to_klient_Click(object sender, EventArgs e)
        {
           
        }

        private void ButtonMinimiz_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonMax_Click(object sender, EventArgs e)
        {
           
        }

        private void buttonDebugExit_Click(object sender, EventArgs e)
        {
            
        }

        private void IdCard_TextChanged(object sender, EventArgs e)
        {
           

        }

        private void BalanceC_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void numericUpDnZonaC_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void numericUpDnGroupC_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void numericUpDnIdTcC_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void numericUpDnIdTp_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void sItog_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void labelPropuskAvto_Click(object sender, EventArgs e)
        {
           
        }

        private void textBoxKey_TextChanged(object sender, EventArgs e)
        {

        }

        private void OutGetCard_Click(object sender, EventArgs e)
        {
 
        }

        private void labelGetId_Click(object sender, EventArgs e)
        {

        }

        private void labelOutKeyA_Click(object sender, EventArgs e)
        {
           
        }

        private void labelOutKeyB_Click(object sender, EventArgs e)
        {

        }

        private void labelOutRead_Click(object sender, EventArgs e)
        {

        }

        private void labelOutWrite_Click(object sender, EventArgs e)
        {

        }

        private void labelDispensGetStatus_Click(object sender, EventArgs e)
        {
            
        }

        private void labelFromBoxToRead_Click(object sender, EventArgs e)
        {
            
        }

        private void labelToBezelAndHold_Click(object sender, EventArgs e)
        {
            
        }

        private void labelToBezelAndNotHold_Click(object sender, EventArgs e)
        {
            
        }

        private void labelToRecycleBox_Click(object sender, EventArgs e)
        {
            
        }

        private void labelGetIdIn_Click(object sender, EventArgs e)
        {

        }

        private void sItog_DoubleClick(object sender, EventArgs e)
        {
            
        }

        //
        #region сохранение настроек
        private void numericUpDnIdStoyky_ValueChanged(object sender, EventArgs e)
        {
            //DeviceRackModel.IdStoyky = (int) numericUpDnIdStoyky.Value;
            //db.SaveChanges();
        }


        #endregion сохранение настроек

        #region сохранение дополнительных настроек
        //!!!
        private void labelKeySave_Click(object sender, EventArgs e)
        {
           
        }

        private void comboBoxTSRaz_SelectedIndexChanged(object sender, EventArgs e)
        {
           

        }

        private void comboBoxTPRaz_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxGrupRaz_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void reader_int_Click(object sender, EventArgs e)
        {

        }

        #region Altimetr


        private void numericUpDn1AltimetrScheduleId_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void numericUpDn2AltimetrScheduleId_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void numericUpDn3AltimetrScheduleId_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void numericUpDn1AltimetrTariffId_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void numericUpDn2AltimetrTariffId_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void numericUpDn3AltimetrTariffId_ValueChanged(object sender, EventArgs e)
        {
            
        }

        #endregion Altimetr

        #endregion сохранение дополнительных настроек


        //Еще события
        private void label_barrierUp_Click(object sender, EventArgs e)
        {
           
        }

        private void label_barrierDn_Click(object sender, EventArgs e)
        {
            
        }

        private void label_lightGrin_Click(object sender, EventArgs e)
        {
            
        }

        private void label_lightRed_Click(object sender, EventArgs e)
        {
            
        }

        private void label_Call_MouseDown(object sender, MouseEventArgs e)
        {
        }

        private void label_Call_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void labelTopMost_Click(object sender, EventArgs e)
        {
           
        }


        private void textBoxPass_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void tabPageDebug_Click(object sender, EventArgs e)
        {
            

        }

        private void labelGrup_Click(object sender, EventArgs e)
        {

        }

        private void buttonClearDevice_Click(object sender, EventArgs e)
        {
            
        }

        private void comboBoxClientTypCard_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void statReadOut_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void statReadIn_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void statDispens_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void labelNameSave_Click(object sender, EventArgs e)
        {
            
        }

      
        private void labelTimerСhecker_Click(object sender, EventArgs e)
        {
            TimerСhecker = TimeСhecker;
        }

        //Касается программы проверки
       //Events

        private void buttonReversON_Click(object sender, EventArgs e)
        {
           
        }

        private void buttonReversOFF_Click(object sender, EventArgs e)
        {
         

        }

        private void buttonStartDecoding_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonBankModuleOpen_Click(object sender, EventArgs e)
        {
            
           
        }

        private void buttonBankModulePay_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonBankModuleClose_Click(object sender, EventArgs e)
        {
            

        }

        private void buttonBankModuleCancell_Click(object sender, EventArgs e)
        {
            

        }

        private void buttonVerification_Click(object sender, EventArgs e)
        {

            

        }

        private void buttonEchoTest_Click(object sender, EventArgs e)
        {

            

        }

        private void buttonWorkKey_Click(object sender, EventArgs e)
        {


        }

        private void buttonTerminalInfo_Click(object sender, EventArgs e)
        {

        }

        private void buttonLoadSW_Click(object sender, EventArgs e)
        {

        }

        private void buttonLoadParams_Click(object sender, EventArgs e)
        {

        }

        private void buttonStopDecoding_Click(object sender, EventArgs e)
        {
            
        }

        private void labelFromBezel_Click(object sender, EventArgs e)
        {
            
        }

        private void labelGetVersion_Click(object sender, EventArgs e)
        {
            
        }

        

        private void labelRecycleToRead_Click(object sender, EventArgs e)
        {
            //
        }

        /*
        public class ViinexRec
        {
            public string confidence { get; set; }
            public string plate_text { get; set; }
            //public List<ViinexRec2> plate_rect { get; set; }
        }
        */

        private void chkSlave_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void chkReaderOut_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void chkReaderIn_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void chkDispenser_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void chkBarcode_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void chkDisplay_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkRFID_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void chkBankModule_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void udComSlave_ValueChanged(object sender, EventArgs e)
        {
          
        }

        private void udComReadOut_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void udComReadIn_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void udComDispenser_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void udComBarcode_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void udComDisplay_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void udComRFID_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void cboRackMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cboRackType_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void udZoneBefore_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void udZoneAfter_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void cboZoneBefore_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void cboZoneAfter_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboDisplayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cboPortDiscret1_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void cboPortDiscret2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cboPortDiscret3_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void chkViinex_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void udTimeCardV_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void udTimeBarrier_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void udTimeNotLoopA_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void udTimeNotIr_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void udLoopA_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void udLoopB_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void udIR_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void chkLoopA_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void chkLoopB_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void chkIR_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void chkPUSH_CheckedChanged(object sender, EventArgs e)
        {
          
        }

        private void udNumberSector_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void udTarifIdRazov0_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void udTarifPlanIdRazov_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void udGrupRaz_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void chkPlaceControlRazov_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void chkControlLoopB_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void chkNewAlarm_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void cboRedNotCard_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cbologNastroy_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void chkDebug_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void chkSlaveDebug_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void chkReaderOutDebug_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void chkReaderInDebug_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void chkDispensDebug_CheckedChanged(object sender, EventArgs e)
        {

          
        }

        private void chkBarcodeDebug_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void chkItog_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void optBlackListNone_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void optBlackListAllow_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void optBlackListNotAllow_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void optBlackListOperator_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void optInOutNone_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void optInOutAllow_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void optInOutNotAllow_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void optInOutOperator_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void chkRegularPlate_CheckedChanged(object sender, EventArgs e)
        {
           //
        }

        private void chkBarcodePlate_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void udAddFreeTime_ValueChanged(object sender, EventArgs e)
        {
           //
        }

        private void chkCollector_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void udComCollector_ValueChanged(object sender, EventArgs e)
        {
           //
        }

        private void lblStatusCollector_Click(object sender, EventArgs e)
        {
           //
        }

        private void lblCollectorEject_Click(object sender, EventArgs e)
        {
            //
        }

        private void lblGetVersionCollector_Click(object sender, EventArgs e)
        {
            //
        }

        private void lblCollectorRetain_Click(object sender, EventArgs e)
        {
            //
        }

        private void lblCollectorFront_Click(object sender, EventArgs e)
        {
            //
        }

        private void cboRackMode_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboRackMode_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cboRackType_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboRackType_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cboZoneBefore_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;           
        }

        private void cboZoneBefore_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cboZoneAfter_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboZoneAfter_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cboDisplayType_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboDisplayType_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cboPortDiscret1_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboPortDiscret1_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cboPortDiscret2_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboPortDiscret2_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cboPortDiscret3_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboPortDiscret3_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void tabMainSettings_Click(object sender, EventArgs e)
        {

        }

        //Это убираем совсем
        private void txtLicenseDebug_TextChanged(object sender, EventArgs e)
        {
            /*
            if (txtLicenseDebug.Text == "xxz1225")
            {
                TimeSpan ddd = TimeSpan.FromSeconds(100000);
                TimeLicenseWorkDebug = DateTime.Now + ddd;
                LicenseWorkDebug = true;
                txtLicenseDebug.Text = "";
            }
            if (txtLicenseDebug.Text == "xxz122qr")
            {
                TimeSpan ddd = TimeSpan.FromSeconds(1000000);
                TimeLicenseWorkDebug = DateTime.Now + ddd;
                LicenseWorkDebug = true;
                txtLicenseDebug.Text = "";
            }
            */
        }

        private void lblStClear_Click(object sender, EventArgs e)
        {
            
        }

        private void cboDispenserTypeId_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        private void cboDispenserTypeId_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboDispenserTypeId_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void chkCardsOnline_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void tabAddSettings_Click(object sender, EventArgs e)
        {

        }

        private void cboReadOutTypeId_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        private void cboReadOutTypeId_DropDown(object sender, EventArgs e)
        {
            //StopUpdate = true;
        }

        private void cboReadOutTypeId_DropDownClosed(object sender, EventArgs e)
        {
            //StopUpdate = false;
        }

        private void lblOmniGetId_Click(object sender, EventArgs e)
        {
           //
        }

        private void tmrOmnikey_Tick(object sender, EventArgs e)
        {
           //
        }

        private void lblCollectorTimeoutOff_Click(object sender, EventArgs e)
        {
            //
        }

        private void lblDispensGetStatus2_Click(object sender, EventArgs e)
        {
           //
        }

        private void lblFromBoxToReader2_Click(object sender, EventArgs e)
        {
           //
        }

        private void lblToBezelAndHold2_Click(object sender, EventArgs e)
        {
            //
        }

        private void lblToBezelAndNotHold2_Click(object sender, EventArgs e)
        {
            //
        }

        private void lblToRecycleBox2_Click(object sender, EventArgs e)
        {
            //
        }

        private void lblFromBezel2_Click(object sender, EventArgs e)
        {
            //
        }

        private void lblRecycleToRead2_Click(object sender, EventArgs e)
        {
            //
        }

        private void lblGetVersion2_Click(object sender, EventArgs e)
        {
            //
        }

        private void chkBarcodeFromButton_CheckedChanged(object sender, EventArgs e)
        {
           //
        }

        private void chkDisableRazovButton_CheckedChanged(object sender, EventArgs e)
        {
           //
        }

        private void cmdViinexSettingsPath_Click(object sender, EventArgs e)
        {
           //
        }

        private void txtViinexSettingsPath_TextChanged(object sender, EventArgs e)
        {
          //
        }

        private void txtCameraURI_TextChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtCameraLogin_TextChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtCameraPassword_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void txtCameraSkip_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void txtViinexConfidence_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void txtCameraPreProcess_TextChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtCameraPostProcess_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void txtViinexLicenseKey_TextChanged(object sender, EventArgs e)
        {
            //
        }

        private void cmdUpdateViinexSettings_Click(object sender, EventArgs e)
        {
            //
        }

        private void cboAltimetrModeId_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        private void cbo2AltimetrModeId_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        private void cbo3AltimetrModeId_SelectedIndexChanged(object sender, EventArgs e)
        {
           //
        }

        private void cboAltimetrModeId_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboAltimetrModeId_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cbo2AltimetrModeId_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cbo2AltimetrModeId_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cbo3AltimetrModeId_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cbo3AltimetrModeId_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cboAltimetrSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        private void cboAltimetrSchedule_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboAltimetrSchedule_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cbo2AltimetrSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
           //
        }

        private void cbo2AltimetrSchedule_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cbo2AltimetrSchedule_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cbo3AltimetrSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
           //
        }

        private void cbo3AltimetrSchedule_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cbo3AltimetrSchedule_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void tabControlService_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboAltimetrTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        private void cboAltimetrTariff_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboAltimetrTariff_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cbo2AltimetrTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        private void cbo2AltimetrTariff_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cbo2AltimetrTariff_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cbo3AltimetrTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cbo3AltimetrTariff_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void cbo3AltimetrTariff_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cbologNastroy_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cbologNastroy_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void comboBoxAlarmW_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private async void tmrLoopANotification_Tick(object sender, EventArgs e)
        {
            //
        }

        private void chkEnableLoopANotification_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtLoopANotificationUri_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void txtLoopANotificationTimeout_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void optNoSavePhoto_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void optSavePhotoAlways_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void optSavePhotoIfNotRecognized_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void cmdSavePhotoDir_Click(object sender, EventArgs e)
        {
            //
        }

        private void txtSavePhotoDir_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void txtSavePhotoTimeout_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void chkInvertCall_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void chkPlaceCompanyControl_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void optLat_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void optCyr_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void comboBoxTSRaz_DropDown(object sender, EventArgs e)
        {
            //StopUpdate = true;
        }

        private void comboBoxTSRaz_DropDownClosed(object sender, EventArgs e)
        {
            //StopUpdate = false;
        }

        private void comboBoxTPRaz_DropDown(object sender, EventArgs e)
        {
            //StopUpdate = true;
        }

        private void comboBoxTPRaz_DropDownClosed(object sender, EventArgs e)
        {
            //StopUpdate = false;
        }

        private void comboBoxGrupRaz_DropDown(object sender, EventArgs e)
        {
            //StopUpdate = true;
        }

        private void comboBoxGrupRaz_DropDownClosed(object sender, EventArgs e)
        {
            //StopUpdate = false;
        }


        private void cboTabloNew_SelectedIndexChanged(object sender, EventArgs e)
        {
           //
        }

        private void cboTabloNew_DropDown(object sender, EventArgs e)
        {
            StopUpdate = true;
        }

        private void cboTabloNew_DropDownClosed(object sender, EventArgs e)
        {
            StopUpdate = false;
        }

        private void udTabloAddress_ValueChanged(object sender, EventArgs e)
        {
           //
        }


        private void udKKMNumber_ValueChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtUDPTabloIP_TextChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtUDPTabloPort_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void udUDPTabloColor_ValueChanged(object sender, EventArgs e)
        {
            //
        }

        private void udUDPTabloRed_ValueChanged(object sender, EventArgs e)
        {
            //
        }

        private void udUDPTabloYellow_ValueChanged(object sender, EventArgs e)
        {
           //
        }

        private void udUDPTabloGreen_ValueChanged(object sender, EventArgs e)
        {
           //
        }

        private void cmdSetTablo3Value_Click(object sender, EventArgs e)
        {
            //
        }

        private void optCameraOnvif_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void optCameraRTSP_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void chkAcquireVideo_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void chkAcquireEvents_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtTablo3_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTablo1_TextChanged(object sender, EventArgs e)
        {

        }

        private void optInOutReverse_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void optTwoLevelReverse_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtViinexGetURI_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void txtCameraURI2_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void optCameraOnvif2_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void optCameraRTSP2_CheckedChanged(object sender, EventArgs e)
        {
           //
        }

        private void chkAcquireVideo2_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void chkAcquireEvents2_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtCameraLogin2_TextChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtCameraPassword2_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void optLat2_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void optCyr2_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtCameraSkip2_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void txtViinexConfidence2_TextChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtCameraPreProcess2_TextChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtCameraPostProcess2_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void chkCamera2Enabled_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void txtTwoLevelSendIP_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void optTwoLevelDisabled_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void optTwoLevelReceive_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void optTwoLevelSend_CheckedChanged(object sender, EventArgs e)
        {
           //
        }

        private void chkGetSnapshots_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void chkTrainReverse_CheckedChanged(object sender, EventArgs e)
        {
           //
        }

        private void txtTimeBudget_TextChanged(object sender, EventArgs e)
        {
           //
        }

        private void chkControlBL_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void chkControlIO_CheckedChanged(object sender, EventArgs e)
        {
           //
        }

        private void chkLevenBL_CheckedChanged(object sender, EventArgs e)
        {
           //
        }

        private void chkLevenIO_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void chkLevenPK_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void chkLevenQR_CheckedChanged(object sender, EventArgs e)
        {
            //
        }

        private void tmrButtonBlinking_Tick(object sender, EventArgs e)
        {
          //
        }

    }

   
}
