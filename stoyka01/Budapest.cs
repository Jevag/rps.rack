﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using static stoyka01.GlobalClass;

namespace stoyka01
{
    public class Budapest
    {
        public struct RZoneTable
        {
            public Guid? DeviceId;
            public string DeviceIP;
            public Guid? RZoneId; //Поменяли RZoneName
            public int? Direction;
        }

        public struct RZoneCapacity
        {
            public int RZoneStatus;
            public DateTime UpdTime;
        }

        public static RZoneCapacity CurrCapacity;

        public static List<RZoneTable> RZoneList;

        public static RZoneTable RZoneDevice;

        public static Guid? CurrentZoneId;

        //Старт стойки
        public static void InitNullCapacity()
        {
            CurrCapacity = new RZoneCapacity();
            CurrCapacity.RZoneStatus = 0;
            CurrCapacity.UpdTime = new DateTime(2020, 1, 1);
        }

        //при отправке и получении http запроса (при отъезде уменьшать!)
        public static void ManualUpdateCapacity(bool inverse) //инверсия = отъезд!
        {
            Logging.ToLog("ManualUpdatecapacity");
            switch (RZoneDevice.Direction)
            {               
                case 0:
                    if (!inverse)
                        CurrCapacity.RZoneStatus += 1;
                    else
                        CurrCapacity.RZoneStatus -= 1;
                    break;
                case 1:
                    if (!inverse)
                        CurrCapacity.RZoneStatus -= 1;
                    else
                        CurrCapacity.RZoneStatus += 1;
                    break;
                case 2:
                    //уменьшаем по модулю. Ноль не трогаем, его не может быть.
                    if (!inverse)
                    {
                        if (CurrCapacity.RZoneStatus > 0)
                        {
                            CurrCapacity.RZoneStatus -= 1;
                        }
                        else if (CurrCapacity.RZoneStatus < 0)
                        {
                            CurrCapacity.RZoneStatus += 1;
                        }
                    }
                    else //увеличиваем по модулю?
                    {
                        if (CurrCapacity.RZoneStatus > 0)
                        {
                            CurrCapacity.RZoneStatus += 1;
                        }
                        else if (CurrCapacity.RZoneStatus < 0)
                        {
                            CurrCapacity.RZoneStatus -= 1;
                        }
                    }
                    break;
                default:
                    break;
            }
            CurrCapacity.UpdTime = DateTime.Now;
            //Logging.ToLog(CurrCapacity.UpdTime.ToString() + " " + CurrCapacity.RZoneStatus.ToString());
        }

        public static void DBUpdateCapacity()
        {
            using (SqlConnection connect = new SqlConnection(conlocal))
            {
                connect.Open();
                DataTable dat =
                    UtilsBase.FillTable("select * from RZoneStatus where Id ='" + RZoneDevice.RZoneId + "'", connect, null, null);
                if (dat != null && dat.Rows.Count > 0)
                {
                    bool Founded = false;
                    foreach (DataRow dad in dat.Rows)
                    {
                        if (dad.GetBool("IsDeleted") != true)
                        {
                            Founded = true;
                            //CurrentZoneId = dad.GetGuid("Id");
                            DateTime? dt = dad.GetDateTime("Sync");
                            int? capacity = dad.GetInt("RZoneStatus"); //из бд
                            //Logging.ToLog("cpc" + capacity.ToString());
                            if (CurrCapacity.UpdTime <= (DateTime)dt)
                            {
                                CurrCapacity.UpdTime = (DateTime)dt;
                                CurrCapacity.RZoneStatus = (int)capacity;
                            }
                            //Logging.ToLog(CurrCapacity.UpdTime.ToString() + " " + CurrCapacity.RZoneStatus.ToString());

                        }
                    }
                    if (!Founded)
                    {
                        Logging.ToLog("В таблице RZoneStatus не найден Guid реверсной зоны");
                    }
                }
                else
                {
                    Logging.ToLog("В таблице RZoneStatus не найден Guid реверсной зоны");
                }
            }  
        }

        //получение ИД зоны из RZone по DeviceId стойки
        public static void GetRZoneId()
        {
            using (SqlConnection connect = new SqlConnection(conlocal))
            {
                connect.Open();
                DataTable dat =
                    UtilsBase.FillTable("select * from RZone where DeviceId ='" + DeviceIdG + "'", connect, null, null);
                if (dat != null && dat.Rows.Count > 0)
                {
                    bool Founded = false;
                    foreach (DataRow dad in dat.Rows)
                    {
                        if (dad.GetBool("IsDeleted") != true)
                        {
                            Founded = true;
                            RZoneDevice = new RZoneTable();
                            RZoneDevice.DeviceId = DeviceIdG;
                            RZoneDevice.DeviceIP = dad.GetString("DeviceIP");
                            RZoneDevice.Direction = dad.GetInt("DeviceDirection");
                            RZoneDevice.RZoneId = dad.GetGuid("RZoneId");
                            Logging.ToLog(RZoneDevice.DeviceId.ToString() + Environment.NewLine + RZoneDevice.DeviceIP + Environment.NewLine + RZoneDevice.Direction.ToString() + Environment.NewLine + RZoneDevice.RZoneId.ToString());

                        }
                    }
                    if (!Founded)
                    {
                        Logging.ToLog("В таблице RZone не найден Id данного устройства");
                    }
                }
                else
                {
                    Logging.ToLog("В таблице RZone не найден Id данного устройства");
                }
            }
        }

        //получение списка остальных устройств (по Id зоны)

        //check!
        public static void GetRZoneList()
        {
            using (SqlConnection connect = new SqlConnection(conlocal))
            {
                connect.Open();
                DataTable dat =
                    UtilsBase.FillTable("select * from RZone where RZoneId ='" + RZoneDevice.RZoneId + "' ", connect, null, null);
                if (dat != null && dat.Rows.Count > 0)
                {
                    RZoneList = new List<RZoneTable>();
                    bool Founded = false;
                    foreach (DataRow dad in dat.Rows)
                    {
                        if (dad.GetBool("IsDeleted") != true)
                        {
                            Founded = true;
                            RZoneTable rt = new RZoneTable();
                            rt.DeviceId = dad.GetGuid("DeviceId");
                            rt.DeviceIP = dad.GetString("DeviceIP");
                            rt.Direction = dad.GetInt("DeviceDirection");
                            rt.RZoneId = dad.GetGuid("RZoneId");
                            Logging.ToLog(rt.DeviceId.ToString() + Environment.NewLine + rt.DeviceIP + Environment.NewLine + rt.Direction.ToString() + Environment.NewLine + rt.RZoneId.ToString());
                            RZoneList.Add(rt);
                        }
                    }
                    if (!Founded)
                    {
                        Logging.ToLog("В таблице RZone не найдены устройства с данным ID зоны");
                    }
                }
                else
                {
                    Logging.ToLog("В таблице RZone не найдены устройства с данным ID зоны");
                }
            }
        }

        //получаем ID нашей текущей зоны по устройству
        public static void GetCurrZoneId()
        {
            using (SqlConnection connect = new SqlConnection(conlocal))
            {
                connect.Open();
                DataTable dat =
                    UtilsBase.FillTable("select * from RZoneStatus where Id ='" + RZoneDevice.RZoneId + "'", connect, null, null);
                if (dat != null && dat.Rows.Count > 0)
                {
                    bool Founded = false;
                    foreach (DataRow dad in dat.Rows)
                    {
                        if (dad.GetBool("IsDeleted") != true)
                        {
                            Founded = true;
                            CurrentZoneId = dad.GetGuid("Id");
                            Logging.ToLog("Guid реверсной зоны " + CurrentZoneId.ToString());

                        }
                    }
                    if (!Founded)
                    {
                        Logging.ToLog("В таблице RZoneStatus не найден Guid реверсной зоны");
                    }
                }
                else
                {
                    Logging.ToLog("В таблице RZoneStatus не найден Guid реверсной зоны");
                }
            }
        }

        //создаем транзакцию
        public static void CreateTransaction(bool reverse)
        {
                List<object> lst = new List<object>();
                lst.Add("Id");
                Guid guID = Guid.NewGuid();
                lst.Add(guID);
                
                //lst.Add("tt");
                //lst.Add(DateTime.Now);

                lst.Add("DeviceID");
                lst.Add(DeviceIdG);

                lst.Add("RZoneId");
                lst.Add(CurrentZoneId);

                lst.Add("DeviceIP");
                lst.Add(RZoneDevice.DeviceIP);

            //lst.Add("Reverse");
            //lst.Add(reverse);
            if (!reverse)
            {
                lst.Add("Direction");
                lst.Add(RZoneDevice.Direction);
            }
            else
            {
                lst.Add("Direction");
                lst.Add(2);
            }

            try
            {
                using (SqlConnection connect = new SqlConnection(conlocal))
                {
                    connect.Open();
                    UtilsBase.InsertCommand("RZoneTransaction", connect, null, lst.ToArray());
                }
            }
            catch (Exception ex)
            {
                Logging.ToLog("Ошибка транзакции реверсивной зоны " + ex.Message);
            }
        }

        public static void CreateTransaction1(int dir)
        {
            List<object> lst = new List<object>();
            lst.Add("Id");
            Guid guID = Guid.NewGuid();
            lst.Add(guID);

            //lst.Add("tt");
            //lst.Add(DateTime.Now);

            lst.Add("DeviceID");
            lst.Add(DeviceIdG);

            lst.Add("RZoneId");
            lst.Add(CurrentZoneId);

            lst.Add("DeviceIP");
            lst.Add(RZoneDevice.DeviceIP);

            //lst.Add("Reverse");
            //lst.Add(reverse);
            if (dir==0)
            {
                lst.Add("Direction");
                lst.Add(1);
            }
            else
            {
                lst.Add("Direction");
                lst.Add(0);
            }

            try
            {
                using (SqlConnection connect = new SqlConnection(conlocal))
                {
                    connect.Open();
                    UtilsBase.InsertCommand("RZoneTransaction", connect, null, lst.ToArray());
                }
            }
            catch (Exception ex)
            {
                Logging.ToLog("Ошибка транзакции реверсивной зоны " + ex.Message);
            }
        }


        public static void SendRZoneRequest2() //Добавим сюда гуид стойки! ммп
        {
            //ManualUpdateCapacity(inverse);
            if (CurrCapacity.RZoneStatus > 0)
            {
                CurrCapacity.RZoneStatus -= 1;
            }
            else if (CurrCapacity.RZoneStatus < 0)
            {
                CurrCapacity.RZoneStatus += 1;
            }

            try
            {
                foreach (RZoneTable Rz in RZoneList) //Исключить наше устройство!
                {
                    if (Rz.DeviceId != DeviceIdG)
                    {
                        try
                        {
                            HttpClient client = new HttpClient();
                            Guid id = Guid.NewGuid();
                            client.GetAsync("http://" + Rz.DeviceIP + ":2222/RZoneRequest/g=" + id.ToString() + "&rackid=" + DeviceIdG.ToString() + "&cpc=" + CurrCapacity.RZoneStatus.ToString() + "n");

                        }
                        catch (Exception ex)
                        {
                            Logging.ToLog(ex.Message);
                        }
                        Logging.ToLog("SendTo: " + Rz.DeviceIP);
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.ToLog(ex.Message);
            }
        }

        //Отправка сообщений между уровнями стойки для HTTP реверса!
        public static void SendRZoneRequest(bool inverse) //Добавим сюда гуид стойки! ммп
        {
            ManualUpdateCapacity(inverse);
            try
            {
                foreach (RZoneTable Rz in RZoneList) //Исключить наше устройство!
                {
                    if (Rz.DeviceId != DeviceIdG)
                    {
                        try
                        {
                            HttpClient client = new HttpClient();
                            Guid id = Guid.NewGuid();
                            client.GetAsync("http://" + Rz.DeviceIP + ":2222/RZoneRequest/g=" + id.ToString() + "&rackid=" + DeviceIdG.ToString());

                        }
                        catch (Exception ex)
                        {
                            Logging.ToLog(ex.Message);
                        }
                        Logging.ToLog("SendTo: " + Rz.DeviceIP);
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.ToLog(ex.Message);
            }
        }

        public static void SendRZoneRequestV(bool inverse) //Добавим сюда гуид стойки! ммп
        {
            ManualUpdateCapacity(inverse);
            try
            {
                foreach (RZoneTable Rz in RZoneList) //Исключить наше устройство!
                {
                    if (Rz.DeviceId != DeviceIdG)
                    {
                        try
                        {
                            HttpClient client = new HttpClient();
                            Guid id = Guid.NewGuid();
                            client.GetAsync("http://" + Rz.DeviceIP + ":2222/RZoneRequest/g=" + id.ToString() + "&rackid=" + DeviceIdG.ToString() + "&cpc=" + CurrCapacity.RZoneStatus.ToString() + "n");

                        }
                        catch (Exception ex)
                        {
                            Logging.ToLog(ex.Message);
                        }
                        Logging.ToLog("SendTo: " + Rz.DeviceIP);
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.ToLog(ex.Message);
            }
        }

        public static void RZoneFullRequest2()
        {
            //ChangeBDTo2(CurrCapacity.RZoneStatus);
            SendRZoneRequest2();
            //Logging.ToLog("mzd");
            //Thread.Sleep(200);
            CreateTransaction(true);
        }

        public static void ChangeBDTo2(int rzs)
        {
           // Logging.ToLog("cbdto2 " + rzs.ToString());
            //CurrentZoneId = new Guid();
            string cz = CurrentZoneId.ToString();
            List<object> RZS_list = new List<object>();
            RZS_list.Add("RZoneStatus"); //1
            RZS_list.Add(rzs);
            try
            {
                using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
                {
                    connect.Open();
                    UtilsBase.UpdateCommand("RZoneStatus", RZS_list.ToArray(), "Id ='" + cz + "'", null, connect);
                }
            }
            catch (Exception ex) { }
        }

        public static void RZoneFullRequest(bool rev)
        {
            SendRZoneRequest(rev);
            CreateTransaction(rev);
        }

        public static void RZoneFullRequest1(bool rev)
        {
            SendRZoneRequest(rev);
            CreateTransaction1(0);
        }

        public static void RZoneFullRequest1R(bool rev)
        {
            SendRZoneRequest(rev);
            CreateTransaction1(1);
        }

        public static void RZoneFullRequestV(bool rev)
        {
            SendRZoneRequestV(rev);
            CreateTransaction(rev);
        }

        //вешаем также на раз в секунду!
        //??????
        public static bool BlockFromType() //true-> block
        {
            
            if (RZoneDevice.Direction == 0)
            {
                if (CurrCapacity.RZoneStatus >= 0) //блокируем при отрицательном
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (RZoneDevice.Direction == 1)
            {
                if (CurrCapacity.RZoneStatus <= 0) //блокируем при положительном
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
            
        }
    }
}
