﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stoyka01
{
    public partial class frmSeos : Form
    {

        int tmrCounter = 0;

        private KeyHandler ghk0;
        private KeyHandler ghk1;
        private KeyHandler ghk2;
        private KeyHandler ghk3;
        private KeyHandler ghk4;
        private KeyHandler ghk5;
        private KeyHandler ghk6;
        private KeyHandler ghk7;
        private KeyHandler ghk8;
        private KeyHandler ghk9;

        //public static string CardNum = "";

        public frmSeos()
        {
            InitializeComponent();
            ghk0 = new KeyHandler(Keys.D0, this);
            ghk0.Register();

            ghk1 = new KeyHandler(Keys.D1, this);
            ghk1.Register();

            ghk2 = new KeyHandler(Keys.D2, this);
            ghk2.Register();

            ghk3 = new KeyHandler(Keys.D3, this);
            ghk3.Register();

            ghk4 = new KeyHandler(Keys.D4, this);
            ghk4.Register();

            ghk5 = new KeyHandler(Keys.D5, this);
            ghk5.Register();

            ghk6 = new KeyHandler(Keys.D6, this);
            ghk6.Register();

            ghk7 = new KeyHandler(Keys.D7, this);
            ghk7.Register();

            ghk8 = new KeyHandler(Keys.D8, this);
            ghk8.Register();

            ghk9 = new KeyHandler(Keys.D9, this);
            ghk9.Register();

        }

        private void frmSeos_FormClosed(object sender, FormClosedEventArgs e)
        {
            GlobalClass.SeosActivated = false;

            ghk0.Unregister();
            ghk0 = null;

            ghk1.Unregister();
            ghk1 = null;

            ghk2.Unregister();
            ghk2 = null;

            ghk3.Unregister();
            ghk3 = null;

            ghk4.Unregister();
            ghk4 = null;

            ghk5.Unregister();
            ghk5 = null;

            ghk6.Unregister();
            ghk6 = null;

            ghk7.Unregister();
            ghk7 = null;

            ghk8.Unregister();
            ghk8 = null;

            ghk9.Unregister();
            ghk9 = null;
        }

        private void tmrDetect_Tick(object sender, EventArgs e)
        {
            tmrCounter += 1;
            if (tmrCounter == 2)
            {
                if (GlobalClass.SeosCardNum.Length >= 3 && GlobalClass.SeosCardNum.Length <= 7)
                {
                    //GlobalClass.SeosActivated = true;
                    Logging.ToLog("Обнаружен номер Seos карты " + GlobalClass.SeosCardNum);
                    GlobalClass.SeosCardDetected = true;

                }
                tmrCounter = 0;
            }
        }

        protected override void WndProc(ref Message m)
        {
            //if (m.Msg == Constants.WM_HOTKEY_MSG_ID)
            //    HandleHotkey0();
            //base.WndProc(ref m);
            try
            {
                if (m.WParam.ToInt32() == ghk0.GetHashCode())
                {
                    GlobalClass.SeosCardNum += "0";
                }
                if (m.WParam.ToInt32() == ghk1.GetHashCode())
                {
                    GlobalClass.SeosCardNum += "1";
                }
                if (m.WParam.ToInt32() == ghk2.GetHashCode())
                {
                    GlobalClass.SeosCardNum += "2";
                }
                if (m.WParam.ToInt32() == ghk3.GetHashCode())
                {
                    GlobalClass.SeosCardNum += "3";
                }
                if (m.WParam.ToInt32() == ghk4.GetHashCode())
                {
                    GlobalClass.SeosCardNum += "4";
                }
                if (m.WParam.ToInt32() == ghk5.GetHashCode())
                {
                    GlobalClass.SeosCardNum += "5";
                }
                if (m.WParam.ToInt32() == ghk6.GetHashCode())
                {
                    GlobalClass.SeosCardNum += "6";
                }
                if (m.WParam.ToInt32() == ghk7.GetHashCode())
                {
                    GlobalClass.SeosCardNum += "7";
                }
                if (m.WParam.ToInt32() == ghk8.GetHashCode())
                {
                    GlobalClass.SeosCardNum += "8";
                }
                if (m.WParam.ToInt32() == ghk9.GetHashCode())
                {
                    GlobalClass.SeosCardNum += "9";
                }
            }
            catch (Exception ex) { }

            base.WndProc(ref m);
        }

        private void frmSeos_Load(object sender, EventArgs e)
        {
            GlobalClass.SeosActivated = true;
            Logging.ToLog("Форма Сеос Активирована!");
        }
    }
}
