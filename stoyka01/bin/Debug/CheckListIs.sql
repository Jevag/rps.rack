USE [rpsMDBG]
GO

/****** Object:  Table [dbo].[CheckListIs]    Script Date: 05.04.2019 13:46:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CheckListIs](
	[ID] [uniqueidentifier] NOT NULL PRIMARY KEY,
	[DeviceID] [uniqueidentifier] NULL,
	[Paid] [int],
	[CardQuery] [int],
	[ResDiff] [int],
	[Pay_Summa] [int],
	[Pay_Query] [int],
	[Pay_SummaSdachi] [int],
	[VAT] [int],
	[DocType] [int],
	[EntryTime] [datetime],
	[ExitTime] [datetime] NULL,
	[CardId] [varchar](1024),
	[What] [varchar](1024),
	[AddStrings] [varchar](max),
	[BankSlip] [varchar](max) NULL,
	[TypeClose] [int],
	[Delivery] [bit],
	[_IsDeleted] [bit] NULL,
	[Sync] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

