USE [rpsMDBG]
GO

/****** Object:  Table [dbo].[DeviceTranslation]    Script Date: 20.05.2019 13:46:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DeviceTranslation](
	[ID] [int] NOT NULL PRIMARY KEY,
	[DeviceType] [int] NULL,
	[InterfaceType] [int] NULL,
	[MaxLength] [int] NULL,
	[RU] [nvarchar](max) NULL,
	[EN] [nvarchar](max) NULL,
	[ZH] [nvarchar](max) NULL,
	[ES] [nvarchar](max) NULL,
	[PT] [nvarchar](max) NULL,
	[HI] [nvarchar](max) NULL,
	[AR] [nvarchar](max) NULL,
	[IT] [nvarchar](max) NULL,
	[DE] [nvarchar](max) NULL,
	[FR] [nvarchar](max) NULL,
	[IN] [nvarchar](max) NULL,
	[KZ] [nvarchar](max) NULL,
	[UZ] [nvarchar](max) NULL,
	[EconomColor] [int] NULL,
	[EconomPicture] [int] NULL,
	[Sync] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

CREATE TRIGGER [dbo].[SyncTranslation]
ON [dbo].[DeviceTranslation]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [DeviceTranslation] set Sync=@dt	where ID in (select ID from inserted)
	update SyncTables set [DateTime]=@dt where TableName='DeviceTranslation'
end
