USE [rpsMDBG]
GO

/****** Object:  Table [dbo].[RZoneTransaction]    Script Date: 01.10.2020 17:50:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RZoneTransaction](
	[Id] [uniqueidentifier] NOT NULL,
	[DeviceId] [uniqueidentifier] NULL,
	[RZoneId] [uniqueidentifier] NULL,
	[DeviceIP] [nvarchar](50) NULL,
	[Direction] [int] NULL,
	[Sync] [datetime] NULL
) ON [PRIMARY]
GO


