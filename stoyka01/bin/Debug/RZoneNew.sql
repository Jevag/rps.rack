	SET ANSI_NULLS ON
	GO

	SET QUOTED_IDENTIFIER ON
	GO

	-- STEP 1. Check if table [RZone] is already created. If NO, then create it
	IF OBJECT_ID(N'RZone','U') IS NULL
	BEGIN
		PRINT 'STEP 1. Table [RZone] is not exist. Create it'
		CREATE TABLE [dbo].[RZone](
			[Id] [uniqueidentifier] NOT NULL,
			[DeviceId] [uniqueidentifier] NULL,
			[DeviceIP] [nvarchar](50) NULL,
			[RZoneId] [uniqueidentifier] NULL,
			--[RZoneName] [nvarchar](50) NULL,
			[DeviceDirection] [int] NULL,
			[Sync] [datetime] NULL,
			[IsDeleted] [bit] NULL,
			CONSTRAINT PK_RZone_Id PRIMARY KEY CLUSTERED (Id)
		) ON [PRIMARY]
	END
	ELSE
	BEGIN
		PRINT 'STEP 1. Table [RZone] is already created';
	END
	GO

	-- STEP 2. Check if table [RZoneStatus] is already created. If NO, then create it
	IF OBJECT_ID(N'RZoneStatus','U') IS NULL
	BEGIN
		PRINT 'STEP 2. Table [RZoneStatus] is not exist. Create it'
		CREATE TABLE [dbo].[RZoneStatus](
			[Id] [uniqueidentifier] NOT NULL,
			[RZoneName] [nvarchar](50) NULL,
			[RZoneStatus] [int] NULL,
			[Sync] [datetime] NULL,
			[IsDeleted] [bit] NULL,
			CONSTRAINT PK_RZoneStatus_Id PRIMARY KEY CLUSTERED (Id)
		) ON [PRIMARY]
	END
	ELSE
	BEGIN
		PRINT 'STEP 2. Table [RZoneStatus] is already created';
	END
	GO

	--STEP 3. Check if table [RZoneTransaction] is already created. If NO, then create it
	IF OBJECT_ID(N'RZoneTransaction','U') IS NULL
	BEGIN
		PRINT 'STEP 3. Table [RZoneTransaction] is not exist. Create it'
		CREATE TABLE [dbo].[RZoneTransaction](
			[Id] [uniqueidentifier] NOT NULL,
			[DeviceId] [uniqueidentifier] NULL,
			[RZoneId] [uniqueidentifier] NULL,
			[DeviceIP] [nvarchar](50) NULL,
			[Direction] [int] NULL,
			[Sync] [datetime] NULL,
			CONSTRAINT PK_RZoneTransaction_Id PRIMARY KEY CLUSTERED (Id)
		) ON [PRIMARY]
	END
	ELSE
	BEGIN
		PRINT 'STEP 3. Table [RZoneTransaction] is already created';
	END
	GO

	-- STEP 4. Check if trigger [SyncRZoneTransaction] is already created. If YES, then drop it. Always create new trigger
	IF EXISTS (SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[dbo].[SyncRZoneTransaction]') AND [type] = 'TR')
	BEGIN
		PRINT 'STEP 4. Trigger [SyncRZoneTransaction] is already exist. Drop it';
		DROP TRIGGER [dbo].[SyncRZoneTransaction];
	END
	ELSE
		PRINT 'STEP 4. Trigger [SyncRZoneTransaction] is not exist';
	GO

	-- STEP 5. Create new trigger [SyncRZoneTransaction]
	PRINT 'STEP 5. Create trigger [SyncRZoneTransaction]'
	GO
	CREATE TRIGGER [dbo].[SyncRZoneTransaction]
	ON [dbo].[RZoneTransaction]
	AFTER INSERT, UPDATE
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @dt datetime;
		DECLARE @level int;

		SET @dt=GetDate();

		UPDATE [RZoneTransaction] SET Sync=@dt	WHERE Id in (SELECT Id FROM inserted)
		UPDATE [SyncTables] SET [DateTime]=@dt WHERE TableName='RZoneTransaction'
		
		IF(EXISTS(SELECT TOP 1 CurrentLevel FROM ConfigSync))
		BEGIN
			SET @level=(SELECT TOP 1 CurrentLevel FROM ConfigSync);
			if(@level = 2)
			BEGIN
			  INSERT INTO Command(Status,TypePacket,DataPacket,DateCreate)
			  SELECT 1, 'SetRZoneTransactions', Id, GETDATE() FROM inserted
			END
		END
	END
	GO

	-- STEP 6. Add new rows to [SyncTables]
	PRINT 'STEP 6. Add new rows to [SyncTables] if needed'
	GO

	IF(EXISTS(SELECT TOP 1 TableName FROM [SyncTables] WHERE TableName = 'RZone'))
		PRINT 'STEP 6.1. Row for [RZone] is already existed'
	ELSE
	BEGIN
		INSERT INTO [SyncTables]
			   ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField]
			   ,[SyncFrom], [IsRemove], [UslovieRemove], [OrderSync2], [SyncFrom2])
		 VALUES
			   ('RZone', GETDATE(), NULL, 120, 'Id', 'Sync', 2, NULL, NULL, NULL, NULL)
		PRINT 'STEP 6.1. Create row for [RZone]'
	END
	GO

	IF(EXISTS(SELECT TOP 1 TableName FROM [SyncTables] WHERE TableName = 'RZoneStatus'))
		PRINT 'STEP 6.2. Row for [RZoneStatus] is already existed'
	ELSE
	BEGIN
		INSERT INTO [SyncTables]
			   ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField]
			   ,[SyncFrom], [IsRemove], [UslovieRemove], [OrderSync2], [SyncFrom2])
		 VALUES
			   ('RZoneStatus', GETDATE(), NULL, 120, 'Id', 'Sync', 2, NULL, NULL, NULL, NULL)
		PRINT 'STEP 6.2. Create row for [RZoneStatus]'
	END
	GO

	IF(EXISTS(SELECT TOP 1 TableName FROM [SyncTables] WHERE TableName = 'RZoneTransaction'))
		PRINT 'STEP 6.3. Row for [RZoneTransaction] is already existed'
	ELSE
	BEGIN
		INSERT INTO [SyncTables]
			   ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField]
			   ,[SyncFrom], [IsRemove], [UslovieRemove], [OrderSync2], [SyncFrom2])
		 VALUES
			   ('RZoneTransaction', GETDATE(), NULL, 120, 'Id', 'Sync', 1, 1, NULL, NULL, NULL)
		PRINT 'STEP 6.3. Create row for [RZoneTransaction]'
	END
	GO

	-- STEP 7. Check if trigger [SyncRZone] is already created. If YES, then drop it. Always create new trigger
	IF EXISTS (SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[dbo].[SyncRZone]') AND [type] = 'TR')
	BEGIN
		PRINT 'STEP 7. Trigger [SyncRZone] is already exist. Drop it';
		DROP TRIGGER [dbo].[SyncRZone];
	END
	ELSE
		PRINT 'STEP 7. Trigger [SyncRZone] is not exist';
	GO

	-- STEP 8. Create new trigger [SyncRZone]
	PRINT 'STEP 8. Create trigger [SyncRZone]'
	GO
	CREATE TRIGGER [dbo].[SyncRZone]
	ON [dbo].[RZone]
	AFTER INSERT, UPDATE
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @dt datetime;

		SET @dt=GetDate();
		
		UPDATE [RZone] SET Sync=@dt	WHERE ID in (SELECT ID FROM inserted)
		UPDATE [SyncTables] SET [DateTime]=@dt WHERE TableName='RZone'
	END
	GO

	-- STEP 9. Check if trigger [SyncRZoneStatus] is already created. If YES, then drop it. Always create new trigger
	IF EXISTS (SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'[dbo].[SyncRZoneStatus]') AND [type] = 'TR')
	BEGIN
		PRINT 'STEP 9. Trigger [SyncRZoneStatus] is already exist. Drop it';
		DROP TRIGGER [dbo].[SyncRZoneStatus];
	END
	ELSE
		PRINT 'STEP 9. Trigger [SyncRZoneStatus] is not exist';
	GO

	-- STEP 10. Create new trigger [SyncRZoneStatus]
	PRINT 'STEP 10. Create trigger [SyncRZoneStatus]'
	GO
	CREATE TRIGGER [dbo].[SyncRZoneStatus]
	ON [dbo].[RZoneStatus]
	AFTER INSERT, UPDATE
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @dt datetime;

		SET @dt=GetDate();
		
		UPDATE [RZoneStatus] SET Sync=@dt	WHERE ID in (SELECT ID FROM inserted)
		UPDATE [SyncTables] SET [DateTime]=@dt WHERE TableName='RZoneStatus'
	END
	GO
