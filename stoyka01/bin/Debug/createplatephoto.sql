USE [rpsMDBG]
GO

/****** Object:  Table [dbo].[PlatePhoto]    Script Date: 23.10.2017 15:48:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PlatePhoto](
	[ID] [uniqueidentifier] NOT NULL PRIMARY KEY,
	[Photo] [image] NULL,
	[TransactionID] [uniqueidentifier] NULL,
	[FTPLink] [varchar](250) NOT NULL,
	[Sync] [datetime] NULL,
	[IsDeleted] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

CREATE TRIGGER [dbo].[SyncPlatePhoto]
ON [dbo].[PlatePhoto]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [PlatePhoto] set Sync=@dt	where ID in (select ID from inserted)
	update SyncTables set [DateTime]=@dt where TableName='PlatePhoto'
end