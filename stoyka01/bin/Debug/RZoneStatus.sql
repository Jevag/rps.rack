USE [rpsMDBG]
GO

/****** Object:  Table [dbo].[RZoneStatus]    Script Date: 01.10.2020 17:48:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RZoneStatus](
	[Id] [uniqueidentifier] NOT NULL,
	[RZoneName] [nvarchar](50) NULL,
	[RZoneStatus] [int] NULL,
	[Sync] [datetime] NULL,
	[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO


