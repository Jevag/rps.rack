USE [rpsMDBG]
GO

/****** Object:  Table [dbo].[WebIskraCL]    Script Date: 19.01.2022 16:26:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WebIskraCL](
	[dt] [datetime] NOT NULL,
	[adr] [nvarchar](100) NULL,
	[card_id] [nvarchar](10) NULL,
	[cashier_inn] [nvarchar](50) NULL,
	[cashier_name] [nvarchar](50) NULL,
	[external_id] [nvarchar](10) NULL,
	[place] [nvarchar](50) NULL,
	[price] [float] NULL,
	[tax] [tinyint] NULL,
	[taxation] [tinyint] NULL,
	[terminal_id] [int] NULL
) ON [PRIMARY]
GO

