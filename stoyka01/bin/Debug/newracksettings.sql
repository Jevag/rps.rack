USE [rpsMDBG]
GO

/****** Object:  Table [dbo].[RackSettings]    Script Date: 11.08.2017 13:46:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DeviceRackSettingsTypes](
	ID int NOT NULL PRIMARY KEY,
	[Value] [varchar](50) NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[DeviceRackSettings](
	[ID] [uniqueidentifier] NOT NULL PRIMARY KEY,
	[DeviceID] [uniqueidentifier] NULL,
	[Name] [varchar](max) NULL,
	[Value] [varchar](1024) NULL,
	[ValueType] [int] FOREIGN KEY REFERENCES [DeviceRackSettingsTypes](ID),
	[Sync] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

CREATE TRIGGER [dbo].[SyncRackSettings]
ON [dbo].[DeviceRackSettings]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [DeviceRackSettings] set Sync=@dt	where DeviceID in (select DeviceID from inserted)
	update SyncTables set [DateTime]=@dt where TableName='DeviceRackSettings'
end
