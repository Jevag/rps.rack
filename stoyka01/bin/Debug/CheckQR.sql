USE [rpsMDBG]
GO

/****** Object:  Table [dbo].[CheckQR]    Script Date: 25.04.2022 11:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CheckQR](
	[Id] [uniqueidentifier] NOT NULL,
	[DeviceId] [uniqueidentifier] NULL,
	[PaidTime] [datetime] NULL,
	[BillSum] [int] NULL,
	[BillNumber] [int] NULL,
	[BillQR] [nvarchar](max) NULL,
	[Sync] [datetime] NULL,
	[_IsDeleted] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

