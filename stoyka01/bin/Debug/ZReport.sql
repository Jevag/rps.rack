USE [rpsMDBG]
GO

/****** Object:  Table [dbo].[ZReport]    Script Date: 15.07.2020 16:18:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZReport](
	[ID] [uniqueidentifier] NOT NULL,
	[DeviceID] [uniqueidentifier] NULL,
	[Z_ID] [int] NULL,
	[Start] [datetime] NULL,
	[Finish] [datetime] NULL,
	[CashAccepted] [int] NULL,
	[CreditCardAccepted] [int] NULL,
	[DelivaryBoxCash1] [int] NULL,
	[DelivaryBoxCash2] [int] NULL,
	[DelivaryBoxCoins1] [int] NULL,
	[DelivaryBoxCoins2] [int] NULL,
	[RecycleBox] [int] NULL,
	[OperatorID] [uniqueidentifier] NULL,
	[Sync] [datetime] NULL,
	[_IsDeleted] [bit] NULL,
	[Type_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZReport] ADD  DEFAULT (NULL) FOR [Type_ID]
GO

