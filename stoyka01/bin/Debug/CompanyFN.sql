USE [rpsMDBG]
GO

/****** Object:  Table [dbo].[CompanyFN]    Script Date: 25.02.2020 13:46:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CompanyFN](
	[ID] [uniqueidentifier] NOT NULL PRIMARY KEY,
	[CompanyId] [uniqueidentifier] NULL,
	[FNnumber] [bigint] NULL,
	[Comment] [nvarchar](max) NULL,
	[Sync] [datetime] NULL,
	[IsDeleted] [bit]NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

