﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rps.ShareBase;

namespace stoyka01
{
    public class StaticDic
    {
        public static Dictionary<string, List<string>> LedDictionary;

        public static Dictionary<int, LedFields> LedDictionaryEx;

        public struct LedFields
        {
            public int id;
            public string RU;
            public string EN;
            public List<string> Words_RU;
            public List<string> Words_EN;
            public int color;
            public int picture;

        }

        public static void InitDBPhrases()
        {
            LedDictionaryEx = new Dictionary<int, LedFields>();
            //делаем запрос к БД!
            //разбиваем фразы
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                //try
                //{
                    DataTable dat = UtilsBase.FillTable("select * from DeviceTranslation where InterfaceType ='1'", connect, null, null);
                    if (dat != null && dat.Rows.Count > 0)
                    {
                        foreach (DataRow da in dat.Rows)
                        {
                            //Debug.Print(da.GetString("RU"));

                            LedFields field = new LedFields();
                            field.id =(int) da.GetInt("ID");
                            field.color= (int)da.GetInt("EconomColor");
                            field.picture = (int)da.GetInt("EconomPicture");
                            field.RU= da.GetString("RU");
                            field.EN = da.GetString("EN");

                            //Debug.Print(field.RU);

                            field.Words_RU = new List<string>();
                            string[] arr_ru1 = field.RU.Split('|');
                            for (int i = 0; i < arr_ru1.Length; i++)
                            {
                                if (arr_ru1[i].IndexOf('-') > 0)
                                {
                                    string[] arr_ru2 = arr_ru1[i].Split('-');
                                    for (int j = 0; j < arr_ru2.Length; j++)
                                    {
                                        field.Words_RU.Add(arr_ru2[j]);
                                    }
                                }
                                else
                                {
                                    field.Words_RU.Add(arr_ru1[i]);
                                }
                            }

                            field.Words_EN = new List<string>();
                            string[] arr_en1 = field.EN.Split('|');
                            for (int i = 0; i < arr_en1.Length; i++)
                            {
                                if (arr_en1[i].IndexOf('-') > 0)
                                {
                                    string[] arr_en2 = arr_en1[i].Split('-');
                                    for (int j = 0; j < arr_en2.Length; j++)
                                    {
                                        field.Words_EN.Add(arr_en2[j]);
                                    }
                                }
                                else
                                {
                                    field.Words_EN.Add(arr_en1[i]);
                                }
                            }

                            LedDictionaryEx.Add(field.id, field);

                        }
                    }
                //}
                //catch { }
            }
            Debug.Print("ass");
        }

        public static void InitTestPhrases()
        {
            LedDictionary = new Dictionary<string, List<string>>();

            List<string> L1 = new List<string>();
            //Освободите	проезд	встречному	авто
            L1.Add("Освободите");
            L1.Add("проезд");
            L1.Add("встречному");
            L1.Add("авто");
            L1.Reverse();

            LedDictionary.Add("ОСВОБОДИТЕ ПРОЕЗД", L1);

            //Пожалуйста	проезжайте
            List<string> L2 = new List<string>();
            L2.Add("Пожалуйста");
            L2.Add("проезжайте");
            L2.Reverse();

            LedDictionary.Add("ПОЖАЛУЙСТА ПРОЕЗЖАЙТЕ", L2);

            List<string> L3 = new List<string>();
            //Проезд	закрыт	обратитесь	к оператору
            L3.Add("Проезд");
            L3.Add("закрыт");
            L3.Add("обратитесь");
            L3.Add("к оператору");
            L3.Reverse();

            LedDictionary.Add("ПРОЕЗД ЗАКРЫТ", L3);

            List<string> L4 = new List<string>();
            //Для проезда	необходимо	подъехать	к стойке
            L4.Add("Для проезда");
            L4.Add("необходимо");
            L4.Add("подъехать");
            L4.Add("к стойке");
            L4.Reverse();

            LedDictionary.Add("Для проезда", L4);

            List<string> L5 = new List<string>();
            //Терминал	временно	не	обслуживает
            L5.Add("Терминал");
            L5.Add("временно");
            L5.Add("не");
            L5.Add("обслуживает");
            L5.Reverse();

            LedDictionary.Add("ТЕРМИНАЛ ВРЕМЕННО", L5);

            List<string> L6 = new List<string>();
            //Нажмите	кнопку	или	приложите	карту
            L6.Add("Нажмите");
            L6.Add("кнопку");
            L6.Add("или");
            L6.Add("приложите");
            L6.Add("карту");
            L6.Reverse();

            LedDictionary.Add("НАЖМИТЕ КНОПКУ ИЛИ", L6);

            List<string> L7 = new List<string>();
            //приложите	карту
            L7.Add("Приложите");
            L7.Add("карту");
            L7.Reverse();

            LedDictionary.Add("ПРИЛОЖИТЕ КАРТУ", L7);

            List<string> L8 = new List<string>();
            //приложите	карту
            L8.Add("Нажмите");
            L8.Add("кнопку");
            L8.Reverse();

            LedDictionary.Add("НАЖМИТЕ КНОПКУ", L8);

            List<string> L9 = new List<string>();
            //приложите	карту	выдача	карт	невозможна
            L9.Add("приложите");
            L9.Add("карту");
            L9.Add("выдача");
            L9.Add("карт");
            L9.Add("невозможна");
            L9.Reverse();

            LedDictionary.Add("Выдача карт невозможна", L9);

            List<string> L10 = new List<string>();
            //вставьте	или	приложите	карту
            L10.Add("вставьте");
            L10.Add("или");
            L10.Add("приложите");
            L10.Add("карту");
            L10.Reverse();

            LedDictionary.Add("ВСТАВТЕ ИЛИ", L10);

            List<string> L11 = new List<string>();
            //вставьте	или	приложите	карту
            L11.Add("вставьте");
            L11.Add("парковочную");
            L11.Add("карту");
            L11.Reverse();

            LedDictionary.Add("ВСТАВТЕ КАРТУ", L11);

            List<string> L12 = new List<string>();
            //пожалуйста	подождите
            L12.Add("пожалуйста");
            L12.Add("подождите");
            L12.Reverse();

            LedDictionary.Add("ПОДОЖДИТЕ", L12);

            List<string> L13 = new List<string>();
            //возьмите	парковочную	карту
            L13.Add("возьмите");
            L13.Add("парковочную");
            L13.Add("карту");
            L13.Reverse();

            LedDictionary.Add("ВОЗЬМИТЕ", L13);

            List<string> L14 = new List<string>();
            //возьмите	парковочную	карту
            L14.Add("необходимо");
            L14.Add("доплатить");
            L14.Add("100 руб");
            L14.Add("возьмите");
            L14.Add("парковочную");
            L14.Add("карту");
            L14.Reverse();

            LedDictionary.Add("НЕОБХОДИМО ДОПЛАТИТЬ", L14);

            //проезд	запрещен	по гос.	номеру
            List<string> L15 = new List<string>();
            L15.Add("проезд");
            L15.Add("запрещен");
            L15.Add("по гос.");
            L15.Add("номеру");
            L15.Reverse();

            LedDictionary.Add("По гос.номеру", L15);

            //проезд	запрещен	Не совпал	гос. Номер
            List<string> L16 = new List<string>();
            L16.Add("проезд");
            L16.Add("запрещен");
            L16.Add("Не совпал");
            L16.Add("гос. Номер");
            L16.Reverse();

            LedDictionary.Add("Не совпал гос.номер", L16);

            //проезд	запрещен	карта	заблокирована
            List<string> L17 = new List<string>();
            L17.Add("проезд");
            L17.Add("запрещен");
            L17.Add("карта");
            L17.Add("в блоке");
            L17.Reverse();

            LedDictionary.Add("Карта заблокирована", L17);

            //проезд	запрещен	нет карты	в базе
            List<string> L18 = new List<string>();
            L18.Add("проезд");
            L18.Add("запрещен");
            L18.Add("нет карты");
            L18.Add("в базе");
            L18.Reverse();

            LedDictionary.Add("Нет карты в базе", L18);

            //проезд	запрещен	вашей	группе	клиентов
            List<string> L19 = new List<string>();
            L19.Add("проезд");
            L19.Add("запрещен");
            L19.Add("вашей");
            L19.Add("группе");
            L19.Add("клиентов");
            L19.Reverse();

            LedDictionary.Add("группе клиентов", L19);

            //повторный	проезд	запрещен	несоответсвие	зоны
            List<string> L20 = new List<string>();
            L20.Add("повторный");
            L20.Add("проезд");
            L20.Add("запрещен");
            L20.Add("неверная");
            L20.Add("зона");
            L20.Reverse();

            LedDictionary.Add("ПОВТОРНЫЙ ПРОЕЗД ЗАПРЕЩЕН", L20);

            //ошибка	чтения	карты
            List<string> L21 = new List<string>();
            L21.Add("ошибка");
            L21.Add("чтения");
            L21.Add("карты");
            L21.Reverse();

            LedDictionary.Add("ОШИБКА ЧТЕНИЯ КАРТЫ", L21);

            //ошибка	записи	карты
            List<string> L22 = new List<string>();
            L22.Add("ошибка");
            L22.Add("записи");
            L22.Add("карты");
            L22.Reverse();

            LedDictionary.Add("ОШИБКА ЗАПИСИ КАРТЫ", L22);

            List<string> L23 = new List<string>();
            L23.Add("не");
            L23.Add("системная");
            L23.Add("карта");
            L23.Reverse();

            LedDictionary.Add("НЕ СИСТЕМНАЯ КАРТА", L23);

            List<string> L24 = new List<string>();
            L24.Add("вставьте");
            L24.Add("карту");
            L24.Add("в карто-");
            L24.Add("приемник");
            L24.Reverse();

            LedDictionary.Add("В КАРТОПРИЕМНИК", L24);

            List<string> L25 = new List<string>();
            L25.Add("Приложите");
            L25.Add("карту");
            L25.Add("Выдача");
            L25.Add("карт");
            L25.Add("невозможна");
            L25.Add("кончились");
            L25.Add("карты");
            L25.Reverse();

            LedDictionary.Add("Выдача карт невозможна кончились", L25);

        }
    }
}
