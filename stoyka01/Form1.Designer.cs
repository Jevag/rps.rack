﻿namespace stoyka01
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.klient = new System.Windows.Forms.Panel();
            this.debug = new System.Windows.Forms.Panel();
            this.CardReadP = new System.Windows.Forms.Panel();
            this.labelDateExitEstimated = new System.Windows.Forms.Label();
            this.comboBoxClientTypCard = new System.Windows.Forms.ComboBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label26 = new System.Windows.Forms.Label();
            this.numericUpDnIdTp = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.BalanceC = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.numericUpDnZonaC = new System.Windows.Forms.NumericUpDown();
            this.numericUpDnIdTcC = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.numericUpDnGroupC = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.IdCard = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.labelBarcodeD = new System.Windows.Forms.Label();
            this.labelPropuskAvto = new System.Windows.Forms.Label();
            this.labelStatSlave = new System.Windows.Forms.Label();
            this.labelReaderOut = new System.Windows.Forms.Label();
            this.labelDispens = new System.Windows.Forms.Label();
            this.labelReaderIn = new System.Windows.Forms.Label();
            this.labelSlave = new System.Windows.Forms.Label();
            this.sItog = new System.Windows.Forms.TextBox();
            this.labelItog = new System.Windows.Forms.Label();
            this.statReadOut = new System.Windows.Forms.ComboBox();
            this.statReadIn = new System.Windows.Forms.ComboBox();
            this.statDispens = new System.Windows.Forms.ComboBox();
            this.comToReadOut = new System.Windows.Forms.Label();
            this.comToDispens = new System.Windows.Forms.Label();
            this.comToReadIn = new System.Windows.Forms.Label();
            this.buttonDebugExit = new System.Windows.Forms.Button();
            this.labelEtap = new System.Windows.Forms.Label();
            this.reader_int = new System.Windows.Forms.Label();
            this.reader_out = new System.Windows.Forms.Label();
            this.DISPENS = new System.Windows.Forms.Label();
            this.PUSH = new System.Windows.Forms.Label();
            this.trafficLight = new System.Windows.Forms.Label();
            this.barrier = new System.Windows.Forms.Label();
            this.IR = new System.Windows.Forms.Label();
            this.loopB = new System.Windows.Forms.Label();
            this.loopA = new System.Windows.Forms.Label();
            this.labelMessage7 = new System.Windows.Forms.Label();
            this.labelMessage6 = new System.Windows.Forms.Label();
            this.labelMessage5 = new System.Windows.Forms.Label();
            this.labelMessage4 = new System.Windows.Forms.Label();
            this.comboBoxDebugUI = new System.Windows.Forms.ComboBox();
            this.RPS_1 = new System.Windows.Forms.Label();
            this.labelMessage2 = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.Label();
            this.labelMessage1 = new System.Windows.Forms.Label();
            this.labelMessage3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelMessage0 = new System.Windows.Forms.Label();
            this.texmenu = new System.Windows.Forms.Panel();
            this.tabControlService = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabDiag = new System.Windows.Forms.TabControl();
            this.tabDiagStatus = new System.Windows.Forms.TabPage();
            this.labelStatDevice = new System.Windows.Forms.Label();
            this.labelStstusOut = new System.Windows.Forms.Label();
            this.tabDiagReaderOut = new System.Windows.Forms.TabPage();
            this.grOmnikey5427OutReader = new System.Windows.Forms.GroupBox();
            this.lblOmniGetId = new System.Windows.Forms.Label();
            this.lblOmniCardID = new System.Windows.Forms.Label();
            this.lblOmniStatus = new System.Windows.Forms.Label();
            this.lblOmniCard = new System.Windows.Forms.Label();
            this.lblDeviceState = new System.Windows.Forms.Label();
            this.lblEnableWrite = new System.Windows.Forms.Label();
            this.lblEnableRead = new System.Windows.Forms.Label();
            this.lblOmniEnabled = new System.Windows.Forms.Label();
            this.grMT625OutReader = new System.Windows.Forms.GroupBox();
            this.labelOutNewComand = new System.Windows.Forms.Label();
            this.labelRxOut = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.labelOutWrite = new System.Windows.Forms.Label();
            this.labelOutRead = new System.Windows.Forms.Label();
            this.labelOutKeyBok = new System.Windows.Forms.Label();
            this.labelOutKeyB = new System.Windows.Forms.Label();
            this.labelOutKeyAok = new System.Windows.Forms.Label();
            this.labelOutKeyA = new System.Windows.Forms.Label();
            this.labelOutIdCard = new System.Windows.Forms.Label();
            this.labelGetId = new System.Windows.Forms.Label();
            this.noCardOut = new System.Windows.Forms.Label();
            this.OutGetCard = new System.Windows.Forms.Label();
            this.labelComOut = new System.Windows.Forms.Label();
            this.tabDiagReaderIn = new System.Windows.Forms.TabPage();
            this.labelInNewComand = new System.Windows.Forms.Label();
            this.labeletapReadCard = new System.Windows.Forms.Label();
            this.labelGetIdIn = new System.Windows.Forms.Label();
            this.InGetCard = new System.Windows.Forms.Label();
            this.labelInIdCard = new System.Windows.Forms.Label();
            this.noCardIn = new System.Windows.Forms.Label();
            this.labelRxIn = new System.Windows.Forms.Label();
            this.labelComIn = new System.Windows.Forms.Label();
            this.tabDiagDispenser = new System.Windows.Forms.TabPage();
            this.grMT166 = new System.Windows.Forms.GroupBox();
            this.grKYT2064 = new System.Windows.Forms.GroupBox();
            this.lblRecycleToRead2 = new System.Windows.Forms.Label();
            this.lblGetVersion2 = new System.Windows.Forms.Label();
            this.lblFromBezel2 = new System.Windows.Forms.Label();
            this.lblToRecycleBox2 = new System.Windows.Forms.Label();
            this.lblToBezelAndNotHold2 = new System.Windows.Forms.Label();
            this.lblToBezelAndHold2 = new System.Windows.Forms.Label();
            this.lblFromBoxToReader2 = new System.Windows.Forms.Label();
            this.lblDispensGetStatus2 = new System.Windows.Forms.Label();
            this.lblStFeedOutSol = new System.Windows.Forms.Label();
            this.lblStAutoWait = new System.Windows.Forms.Label();
            this.lblStStack2Wait = new System.Windows.Forms.Label();
            this.lblStStack1Wait = new System.Windows.Forms.Label();
            this.lblStStop = new System.Windows.Forms.Label();
            this.lblStFeedOut = new System.Windows.Forms.Label();
            this.lblStFeedIn = new System.Windows.Forms.Label();
            this.lblStCapture = new System.Windows.Forms.Label();
            this.lblStAutoIssue = new System.Windows.Forms.Label();
            this.lblStStacker2Out = new System.Windows.Forms.Label();
            this.lblDispenser2Status = new System.Windows.Forms.Label();
            this.lblEtapDisp2 = new System.Windows.Forms.Label();
            this.lblStStacker1Out = new System.Windows.Forms.Label();
            this.lblStStatus = new System.Windows.Forms.Label();
            this.lblStClear = new System.Windows.Forms.Label();
            this.lblComDispens2 = new System.Windows.Forms.Label();
            this.lblRecycleToRead = new System.Windows.Forms.Label();
            this.labelRxDisp = new System.Windows.Forms.Label();
            this.labelVersionDispens = new System.Windows.Forms.Label();
            this.lblGetVersion = new System.Windows.Forms.Label();
            this.lblFromBezel = new System.Windows.Forms.Label();
            this.labelDispensStatus = new System.Windows.Forms.Label();
            this.labeletapDispensCard = new System.Windows.Forms.Label();
            this.lblToRecycleBox = new System.Windows.Forms.Label();
            this.lblToBezelAndNotHold = new System.Windows.Forms.Label();
            this.lblToBezelAndHold = new System.Windows.Forms.Label();
            this.lblFromBoxToRead = new System.Windows.Forms.Label();
            this.lblDispensGetStatus = new System.Windows.Forms.Label();
            this.labelComDispens = new System.Windows.Forms.Label();
            this.tabDiagBarcode = new System.Windows.Forms.TabPage();
            this.labelQRcode = new System.Windows.Forms.Label();
            this.buttonStopDecoding = new System.Windows.Forms.Button();
            this.buttonStartDecoding = new System.Windows.Forms.Button();
            this.labelComBarcode = new System.Windows.Forms.Label();
            this.tabDiagSlave = new System.Windows.Forms.TabPage();
            this.buttonReversOFF = new System.Windows.Forms.Button();
            this.buttonReversON = new System.Windows.Forms.Button();
            this.labelVersionSlave = new System.Windows.Forms.Label();
            this.label_Call = new System.Windows.Forms.Label();
            this.labelStatusOut2 = new System.Windows.Forms.Label();
            this.label_lightRed = new System.Windows.Forms.Label();
            this.label_lightGrin = new System.Windows.Forms.Label();
            this.label_barrierDn = new System.Windows.Forms.Label();
            this.label_barrierUp = new System.Windows.Forms.Label();
            this.labelComSlave = new System.Windows.Forms.Label();
            this.tabDiagTablo = new System.Windows.Forms.TabPage();
            this.grTablo3 = new System.Windows.Forms.GroupBox();
            this.label79 = new System.Windows.Forms.Label();
            this.udUDPTabloGreen = new System.Windows.Forms.NumericUpDown();
            this.udUDPTabloYellow = new System.Windows.Forms.NumericUpDown();
            this.label78 = new System.Windows.Forms.Label();
            this.udUDPTabloRed = new System.Windows.Forms.NumericUpDown();
            this.label77 = new System.Windows.Forms.Label();
            this.cmdSetTablo3Value = new System.Windows.Forms.Button();
            this.txtTablo3 = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.udUDPTabloColor = new System.Windows.Forms.NumericUpDown();
            this.label74 = new System.Windows.Forms.Label();
            this.txtUDPTabloPort = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.txtUDPTabloIP = new System.Windows.Forms.TextBox();
            this.grTablo2 = new System.Windows.Forms.GroupBox();
            this.txtTablo2 = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.udTabloAddress = new System.Windows.Forms.NumericUpDown();
            this.label41 = new System.Windows.Forms.Label();
            this.cboDisplayType = new System.Windows.Forms.ComboBox();
            this.grTablo1 = new System.Windows.Forms.GroupBox();
            this.txtTablo1 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.labelComTablo = new System.Windows.Forms.Label();
            this.tabDiagWebServer = new System.Windows.Forms.TabPage();
            this.buttonViinexFolder = new System.Windows.Forms.Button();
            this.CameraBox = new System.Windows.Forms.GroupBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.textBoxCameraIP = new System.Windows.Forms.TextBox();
            this.labelLastPlate = new System.Windows.Forms.Label();
            this.labelviinex = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBoxViinexFile = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.labelWebServer = new System.Windows.Forms.Label();
            this.tabDiagBankModule = new System.Windows.Forms.TabPage();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.buttonVerification = new System.Windows.Forms.Button();
            this.buttonEchoTest = new System.Windows.Forms.Button();
            this.buttonWorkKey = new System.Windows.Forms.Button();
            this.buttonTerminalInfo = new System.Windows.Forms.Button();
            this.buttonLoadSW = new System.Windows.Forms.Button();
            this.buttonLoadParams = new System.Windows.Forms.Button();
            this.buttonBankModuleCancell = new System.Windows.Forms.Button();
            this.buttonBankModuleClose = new System.Windows.Forms.Button();
            this.buttonBankModulePay = new System.Windows.Forms.Button();
            this.buttonBankModuleOpen = new System.Windows.Forms.Button();
            this.labelBankModuleStatus = new System.Windows.Forms.Label();
            this.tabDiagRFID = new System.Windows.Forms.TabPage();
            this.labelWiegandResult = new System.Windows.Forms.Label();
            this.labelWiegandStat = new System.Windows.Forms.Label();
            this.labelComWiegand = new System.Windows.Forms.Label();
            this.tabDiagCollector = new System.Windows.Forms.TabPage();
            this.lblCollectorTimeoutOff = new System.Windows.Forms.Label();
            this.lblCollectorBack = new System.Windows.Forms.Label();
            this.lblVersionCollector = new System.Windows.Forms.Label();
            this.lblGetVersionCollector = new System.Windows.Forms.Label();
            this.lblCollectorFront = new System.Windows.Forms.Label();
            this.lblCollectorStatus = new System.Windows.Forms.Label();
            this.lblEtapCollector = new System.Windows.Forms.Label();
            this.lblCollectorRetain = new System.Windows.Forms.Label();
            this.lblCollectorEject = new System.Windows.Forms.Label();
            this.lblStatusCollector = new System.Windows.Forms.Label();
            this.lblComCollector = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabMainSettings = new System.Windows.Forms.TabPage();
            this.label71 = new System.Windows.Forms.Label();
            this.udKKMNumber = new System.Windows.Forms.NumericUpDown();
            this.cboTabloNew = new System.Windows.Forms.ComboBox();
            this.cboReadOutTypeId = new System.Windows.Forms.ComboBox();
            this.cboDispenserTypeId = new System.Windows.Forms.ComboBox();
            this.cboRackMode = new System.Windows.Forms.ComboBox();
            this.udComCollector = new System.Windows.Forms.NumericUpDown();
            this.chkCollector = new System.Windows.Forms.CheckBox();
            this.udComRFID = new System.Windows.Forms.NumericUpDown();
            this.chkRFID = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.udZoneBefore = new System.Windows.Forms.NumericUpDown();
            this.udZoneAfter = new System.Windows.Forms.NumericUpDown();
            this.chkBankModule = new System.Windows.Forms.CheckBox();
            this.chkSlave = new System.Windows.Forms.CheckBox();
            this.cboPortDiscret3 = new System.Windows.Forms.ComboBox();
            this.chkReaderOut = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.chkReaderIn = new System.Windows.Forms.CheckBox();
            this.chkDispenser = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.chkBarcode = new System.Windows.Forms.CheckBox();
            this.udComDisplay = new System.Windows.Forms.NumericUpDown();
            this.chkDisplay = new System.Windows.Forms.CheckBox();
            this.cboZoneAfter = new System.Windows.Forms.ComboBox();
            this.udComSlave = new System.Windows.Forms.NumericUpDown();
            this.cboZoneBefore = new System.Windows.Forms.ComboBox();
            this.udComReadOut = new System.Windows.Forms.NumericUpDown();
            this.cboPortDiscret2 = new System.Windows.Forms.ComboBox();
            this.udComReadIn = new System.Windows.Forms.NumericUpDown();
            this.cboPortDiscret1 = new System.Windows.Forms.ComboBox();
            this.udComDispenser = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.udComBarcode = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cboRackType = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.txtTimeBudget = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.chkGetSnapshots = new System.Windows.Forms.CheckBox();
            this.grTwoLevelRackRecognition = new System.Windows.Forms.GroupBox();
            this.label88 = new System.Windows.Forms.Label();
            this.txtTwoLevelSendIP = new System.Windows.Forms.TextBox();
            this.optTwoLevelSend = new System.Windows.Forms.RadioButton();
            this.optTwoLevelReceive = new System.Windows.Forms.RadioButton();
            this.optTwoLevelDisabled = new System.Windows.Forms.RadioButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtViinexGetURI = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkAcquireEvents = new System.Windows.Forms.CheckBox();
            this.chkAcquireVideo = new System.Windows.Forms.CheckBox();
            this.grCameraType = new System.Windows.Forms.GroupBox();
            this.optCameraOnvif = new System.Windows.Forms.RadioButton();
            this.optCameraRTSP = new System.Windows.Forms.RadioButton();
            this.txtViinexConfidence = new System.Windows.Forms.TextBox();
            this.grTrans = new System.Windows.Forms.GroupBox();
            this.optLat = new System.Windows.Forms.RadioButton();
            this.optCyr = new System.Windows.Forms.RadioButton();
            this.label67 = new System.Windows.Forms.Label();
            this.txtCameraPostProcess = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.txtCameraPreProcess = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.txtCameraSkip = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.txtCameraPassword = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.txtCameraLogin = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.txtCameraURI = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chkCamera2Enabled = new System.Windows.Forms.CheckBox();
            this.label80 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.chkAcquireEvents2 = new System.Windows.Forms.CheckBox();
            this.chkAcquireVideo2 = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.optCameraOnvif2 = new System.Windows.Forms.RadioButton();
            this.optCameraRTSP2 = new System.Windows.Forms.RadioButton();
            this.txtViinexConfidence2 = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.optLat2 = new System.Windows.Forms.RadioButton();
            this.optCyr2 = new System.Windows.Forms.RadioButton();
            this.label81 = new System.Windows.Forms.Label();
            this.txtCameraPostProcess2 = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.txtCameraPreProcess2 = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.txtCameraSkip2 = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.txtCameraPassword2 = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.txtCameraLogin2 = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.txtCameraURI2 = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.grSavePhotoLocal = new System.Windows.Forms.GroupBox();
            this.txtSavePhotoTimeout = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.cmdSavePhotoDir = new System.Windows.Forms.Button();
            this.label69 = new System.Windows.Forms.Label();
            this.txtSavePhotoDir = new System.Windows.Forms.TextBox();
            this.optSavePhotoIfNotRecognized = new System.Windows.Forms.RadioButton();
            this.optNoSavePhoto = new System.Windows.Forms.RadioButton();
            this.optSavePhotoAlways = new System.Windows.Forms.RadioButton();
            this.grEnableLoopANotification = new System.Windows.Forms.GroupBox();
            this.txtLoopANotificationTimeout = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.txtLoopANotificationUri = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.chkEnableLoopANotification = new System.Windows.Forms.CheckBox();
            this.grViinexMain = new System.Windows.Forms.GroupBox();
            this.cmdViinexSettingsPath = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.txtViinexSettingsPath = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.chkViinex = new System.Windows.Forms.CheckBox();
            this.cmdUpdateViinexSettings = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.txtViinexLicenseKey = new System.Windows.Forms.TextBox();
            this.pnlVision = new System.Windows.Forms.Panel();
            this.chkControlIO = new System.Windows.Forms.CheckBox();
            this.chkControlBL = new System.Windows.Forms.CheckBox();
            this.chkLevenIO = new System.Windows.Forms.CheckBox();
            this.chkLevenBL = new System.Windows.Forms.CheckBox();
            this.chkLevenQR = new System.Windows.Forms.CheckBox();
            this.chkLevenPK = new System.Windows.Forms.CheckBox();
            this.groupBoxBlackList = new System.Windows.Forms.GroupBox();
            this.optBlackListOperator = new System.Windows.Forms.RadioButton();
            this.optBlackListNone = new System.Windows.Forms.RadioButton();
            this.optBlackListAllow = new System.Windows.Forms.RadioButton();
            this.optBlackListNotAllow = new System.Windows.Forms.RadioButton();
            this.chkBarcodePlate = new System.Windows.Forms.CheckBox();
            this.label53 = new System.Windows.Forms.Label();
            this.chkRegularPlate = new System.Windows.Forms.CheckBox();
            this.label56 = new System.Windows.Forms.Label();
            this.groupBoxInOut = new System.Windows.Forms.GroupBox();
            this.optInOutOperator = new System.Windows.Forms.RadioButton();
            this.optInOutNone = new System.Windows.Forms.RadioButton();
            this.optInOutAllow = new System.Windows.Forms.RadioButton();
            this.optInOutNotAllow = new System.Windows.Forms.RadioButton();
            this.label51 = new System.Windows.Forms.Label();
            this.tabAddSettings = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.udLoopAAlarmsTime = new System.Windows.Forms.NumericUpDown();
            this.label57 = new System.Windows.Forms.Label();
            this.chkTrainReverse = new System.Windows.Forms.CheckBox();
            this.grReverseMode = new System.Windows.Forms.GroupBox();
            this.optTwoLevelReverse = new System.Windows.Forms.RadioButton();
            this.optInOutReverse = new System.Windows.Forms.RadioButton();
            this.chkPlaceCompanyControl = new System.Windows.Forms.CheckBox();
            this.chkBarcodeFromButton = new System.Windows.Forms.CheckBox();
            this.chkDisableRazovButton = new System.Windows.Forms.CheckBox();
            this.chkCardsOnline = new System.Windows.Forms.CheckBox();
            this.udAddFreeTime = new System.Windows.Forms.NumericUpDown();
            this.label37 = new System.Windows.Forms.Label();
            this.chkControlLoopB = new System.Windows.Forms.CheckBox();
            this.chkNewAlarm = new System.Windows.Forms.CheckBox();
            this.chkPlaceControlRazov = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkInvertCall = new System.Windows.Forms.CheckBox();
            this.chkPUSH = new System.Windows.Forms.CheckBox();
            this.chkIR = new System.Windows.Forms.CheckBox();
            this.chkLoopB = new System.Windows.Forms.CheckBox();
            this.chkLoopA = new System.Windows.Forms.CheckBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.cbo3AltimetrTariff = new System.Windows.Forms.ComboBox();
            this.cbo2AltimetrTariff = new System.Windows.Forms.ComboBox();
            this.cboAltimetrTariff = new System.Windows.Forms.ComboBox();
            this.numericUpDn3AltimetrTariffId = new System.Windows.Forms.NumericUpDown();
            this.numericUpDn2AltimetrTariffId = new System.Windows.Forms.NumericUpDown();
            this.numericUpDn1AltimetrTariffId = new System.Windows.Forms.NumericUpDown();
            this.cbo3AltimetrSchedule = new System.Windows.Forms.ComboBox();
            this.cbo2AltimetrSchedule = new System.Windows.Forms.ComboBox();
            this.cboAltimetrSchedule = new System.Windows.Forms.ComboBox();
            this.numericUpDn3AltimetrScheduleId = new System.Windows.Forms.NumericUpDown();
            this.numericUpDn2AltimetrScheduleId = new System.Windows.Forms.NumericUpDown();
            this.numericUpDn1AltimetrScheduleId = new System.Windows.Forms.NumericUpDown();
            this.cbo3AltimetrModeId = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.cbo2AltimetrModeId = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.cboAltimetrModeId = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.udIR = new System.Windows.Forms.NumericUpDown();
            this.label38 = new System.Windows.Forms.Label();
            this.udLoopB = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.udLoopA = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxGrupRaz = new System.Windows.Forms.ComboBox();
            this.comboBoxTPRaz = new System.Windows.Forms.ComboBox();
            this.comboBoxTSRaz = new System.Windows.Forms.ComboBox();
            this.cboRedNotCard = new System.Windows.Forms.ComboBox();
            this.udGrupRaz = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.cbologNastroy = new System.Windows.Forms.ComboBox();
            this.udTarifPlanIdRazov = new System.Windows.Forms.NumericUpDown();
            this.label32 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.udTimeNotIr = new System.Windows.Forms.NumericUpDown();
            this.udTimeNotLoopA = new System.Windows.Forms.NumericUpDown();
            this.udTarifIdRazov0 = new System.Windows.Forms.NumericUpDown();
            this.udNumberSector = new System.Windows.Forms.NumericUpDown();
            this.udTimeBarrier = new System.Windows.Forms.NumericUpDown();
            this.udTimeCardV = new System.Windows.Forms.NumericUpDown();
            this.tabCheck = new System.Windows.Forms.TabPage();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonRun = new System.Windows.Forms.Button();
            this.labelAudioIn = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.labelTimerСhecker = new System.Windows.Forms.Label();
            this.tabDebug = new System.Windows.Forms.TabPage();
            this.checkBoxDop = new System.Windows.Forms.CheckBox();
            this.cboDop = new System.Windows.Forms.ComboBox();
            this.SoftVersion = new System.Windows.Forms.Label();
            this.panelDebugH = new System.Windows.Forms.Panel();
            this.textBoxUITest = new System.Windows.Forms.TextBox();
            this.labelCardExitWrite = new System.Windows.Forms.Label();
            this.txtCardKey = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.txtLicenseDebug = new System.Windows.Forms.TextBox();
            this.labelLanguage = new System.Windows.Forms.Label();
            this.labelSignatureSlave = new System.Windows.Forms.Label();
            this.buttonClearDevice = new System.Windows.Forms.Button();
            this.labelTopMost = new System.Windows.Forms.Label();
            this.lblGroupFromFile = new System.Windows.Forms.Label();
            this.cboHronom = new System.Windows.Forms.ComboBox();
            this.cboAlarmW = new System.Windows.Forms.ComboBox();
            this.cboClientType = new System.Windows.Forms.ComboBox();
            this.labelNameSave = new System.Windows.Forms.Label();
            this.ServerUrl = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.lblSaveKey = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.chkItog = new System.Windows.Forms.CheckBox();
            this.label27 = new System.Windows.Forms.Label();
            this.chkBarcodeDebug = new System.Windows.Forms.CheckBox();
            this.chkDispensDebug = new System.Windows.Forms.CheckBox();
            this.chkReaderInDebug = new System.Windows.Forms.CheckBox();
            this.chkReaderOutDebug = new System.Windows.Forms.CheckBox();
            this.chkSlaveDebug = new System.Windows.Forms.CheckBox();
            this.chkDebug = new System.Windows.Forms.CheckBox();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.labelLicenseWork = new System.Windows.Forms.Label();
            this.labelErrBD = new System.Windows.Forms.Label();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonMax = new System.Windows.Forms.Button();
            this.timer_serv = new System.Windows.Forms.Label();
            this.ButtonMinimiz = new System.Windows.Forms.Button();
            this.to_klient = new System.Windows.Forms.Button();
            this.service = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timer50 = new System.Windows.Forms.Timer(this.components);
            this.Com1 = new System.Windows.Forms.OpenFileDialog();
            this.tmrOmnikey = new System.Windows.Forms.Timer(this.components);
            this.tmrLoopANotification = new System.Windows.Forms.Timer(this.components);
            this.fold1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tmrButtonBlinking = new System.Windows.Forms.Timer(this.components);
            this.tmrCameraAvailable = new System.Windows.Forms.Timer(this.components);
            this.klient.SuspendLayout();
            this.debug.SuspendLayout();
            this.CardReadP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDnIdTp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDnZonaC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDnIdTcC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDnGroupC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.texmenu.SuspendLayout();
            this.tabControlService.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabDiag.SuspendLayout();
            this.tabDiagStatus.SuspendLayout();
            this.tabDiagReaderOut.SuspendLayout();
            this.grOmnikey5427OutReader.SuspendLayout();
            this.grMT625OutReader.SuspendLayout();
            this.tabDiagReaderIn.SuspendLayout();
            this.tabDiagDispenser.SuspendLayout();
            this.grMT166.SuspendLayout();
            this.grKYT2064.SuspendLayout();
            this.tabDiagBarcode.SuspendLayout();
            this.tabDiagSlave.SuspendLayout();
            this.tabDiagTablo.SuspendLayout();
            this.grTablo3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udUDPTabloGreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udUDPTabloYellow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udUDPTabloRed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udUDPTabloColor)).BeginInit();
            this.grTablo2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udTabloAddress)).BeginInit();
            this.grTablo1.SuspendLayout();
            this.tabDiagWebServer.SuspendLayout();
            this.tabDiagBankModule.SuspendLayout();
            this.tabDiagRFID.SuspendLayout();
            this.tabDiagCollector.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabMainSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udKKMNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComCollector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComRFID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udZoneBefore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udZoneAfter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComSlave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComReadOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComReadIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComDispenser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComBarcode)).BeginInit();
            this.tabPage15.SuspendLayout();
            this.grTwoLevelRackRecognition.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.grCameraType.SuspendLayout();
            this.grTrans.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.grSavePhotoLocal.SuspendLayout();
            this.grEnableLoopANotification.SuspendLayout();
            this.grViinexMain.SuspendLayout();
            this.pnlVision.SuspendLayout();
            this.groupBoxBlackList.SuspendLayout();
            this.groupBoxInOut.SuspendLayout();
            this.tabAddSettings.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udLoopAAlarmsTime)).BeginInit();
            this.grReverseMode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udAddFreeTime)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn3AltimetrTariffId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn2AltimetrTariffId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn1AltimetrTariffId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn3AltimetrScheduleId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn2AltimetrScheduleId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn1AltimetrScheduleId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udIR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udLoopB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udLoopA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udGrupRaz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTarifPlanIdRazov)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTimeNotIr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTimeNotLoopA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTarifIdRazov0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNumberSector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTimeBarrier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTimeCardV)).BeginInit();
            this.tabCheck.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabDebug.SuspendLayout();
            this.panelDebugH.SuspendLayout();
            this.SuspendLayout();
            // 
            // klient
            // 
            this.klient.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.klient.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("klient.BackgroundImage")));
            this.klient.Controls.Add(this.debug);
            this.klient.Controls.Add(this.labelMessage7);
            this.klient.Controls.Add(this.labelMessage6);
            this.klient.Controls.Add(this.labelMessage5);
            this.klient.Controls.Add(this.labelMessage4);
            this.klient.Controls.Add(this.comboBoxDebugUI);
            this.klient.Controls.Add(this.RPS_1);
            this.klient.Controls.Add(this.labelMessage2);
            this.klient.Controls.Add(this.time);
            this.klient.Controls.Add(this.labelMessage1);
            this.klient.Controls.Add(this.labelMessage3);
            this.klient.Controls.Add(this.pictureBox1);
            this.klient.Controls.Add(this.labelMessage0);
            this.klient.Controls.Add(this.texmenu);
            this.klient.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.klient.Location = new System.Drawing.Point(0, 0);
            this.klient.Name = "klient";
            this.klient.Size = new System.Drawing.Size(800, 720);
            this.klient.TabIndex = 0;
            // 
            // debug
            // 
            this.debug.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.debug.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.debug.Controls.Add(this.CardReadP);
            this.debug.Controls.Add(this.labelBarcodeD);
            this.debug.Controls.Add(this.labelPropuskAvto);
            this.debug.Controls.Add(this.labelStatSlave);
            this.debug.Controls.Add(this.labelReaderOut);
            this.debug.Controls.Add(this.labelDispens);
            this.debug.Controls.Add(this.labelReaderIn);
            this.debug.Controls.Add(this.labelSlave);
            this.debug.Controls.Add(this.sItog);
            this.debug.Controls.Add(this.labelItog);
            this.debug.Controls.Add(this.statReadOut);
            this.debug.Controls.Add(this.statReadIn);
            this.debug.Controls.Add(this.statDispens);
            this.debug.Controls.Add(this.comToReadOut);
            this.debug.Controls.Add(this.comToDispens);
            this.debug.Controls.Add(this.comToReadIn);
            this.debug.Controls.Add(this.buttonDebugExit);
            this.debug.Controls.Add(this.labelEtap);
            this.debug.Controls.Add(this.reader_int);
            this.debug.Controls.Add(this.reader_out);
            this.debug.Controls.Add(this.DISPENS);
            this.debug.Controls.Add(this.PUSH);
            this.debug.Controls.Add(this.trafficLight);
            this.debug.Controls.Add(this.barrier);
            this.debug.Controls.Add(this.IR);
            this.debug.Controls.Add(this.loopB);
            this.debug.Controls.Add(this.loopA);
            this.debug.Location = new System.Drawing.Point(17, 386);
            this.debug.Name = "debug";
            this.debug.Size = new System.Drawing.Size(798, 173);
            this.debug.TabIndex = 3;
            // 
            // CardReadP
            // 
            this.CardReadP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CardReadP.Controls.Add(this.labelDateExitEstimated);
            this.CardReadP.Controls.Add(this.comboBoxClientTypCard);
            this.CardReadP.Controls.Add(this.dateTimePicker2);
            this.CardReadP.Controls.Add(this.label26);
            this.CardReadP.Controls.Add(this.numericUpDnIdTp);
            this.CardReadP.Controls.Add(this.label23);
            this.CardReadP.Controls.Add(this.BalanceC);
            this.CardReadP.Controls.Add(this.label22);
            this.CardReadP.Controls.Add(this.label21);
            this.CardReadP.Controls.Add(this.dateTimePicker1);
            this.CardReadP.Controls.Add(this.numericUpDnZonaC);
            this.CardReadP.Controls.Add(this.numericUpDnIdTcC);
            this.CardReadP.Controls.Add(this.label19);
            this.CardReadP.Controls.Add(this.numericUpDnGroupC);
            this.CardReadP.Controls.Add(this.label20);
            this.CardReadP.Controls.Add(this.label18);
            this.CardReadP.Controls.Add(this.IdCard);
            this.CardReadP.Controls.Add(this.label17);
            this.CardReadP.Location = new System.Drawing.Point(279, 3);
            this.CardReadP.Name = "CardReadP";
            this.CardReadP.Size = new System.Drawing.Size(289, 159);
            this.CardReadP.TabIndex = 20;
            this.CardReadP.Visible = false;
            // 
            // labelDateExitEstimated
            // 
            this.labelDateExitEstimated.AutoSize = true;
            this.labelDateExitEstimated.Location = new System.Drawing.Point(3, 140);
            this.labelDateExitEstimated.Name = "labelDateExitEstimated";
            this.labelDateExitEstimated.Size = new System.Drawing.Size(55, 13);
            this.labelDateExitEstimated.TabIndex = 17;
            this.labelDateExitEstimated.Text = "Выезд до";
            // 
            // comboBoxClientTypCard
            // 
            this.comboBoxClientTypCard.Items.AddRange(new object[] {
            "Разовый",
            "Постоянный",
            "...",
            "Вездеход"});
            this.comboBoxClientTypCard.Location = new System.Drawing.Point(204, 87);
            this.comboBoxClientTypCard.Name = "comboBoxClientTypCard";
            this.comboBoxClientTypCard.Size = new System.Drawing.Size(74, 21);
            this.comboBoxClientTypCard.TabIndex = 16;
            this.comboBoxClientTypCard.Text = "Тип ";
            this.comboBoxClientTypCard.SelectedIndexChanged += new System.EventHandler(this.comboBoxClientTypCard_SelectedIndexChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "dd MMMM  yyyy    HH:mm:ss";
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(89, 37);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(190, 20);
            this.dateTimePicker2.TabIndex = 15;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(3, 37);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(80, 13);
            this.label26.TabIndex = 14;
            this.label26.Text = "Время оплаты";
            // 
            // numericUpDnIdTp
            // 
            this.numericUpDnIdTp.Location = new System.Drawing.Point(139, 115);
            this.numericUpDnIdTp.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDnIdTp.Name = "numericUpDnIdTp";
            this.numericUpDnIdTp.Size = new System.Drawing.Size(37, 20);
            this.numericUpDnIdTp.TabIndex = 13;
            this.numericUpDnIdTp.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDnIdTp.ValueChanged += new System.EventHandler(this.numericUpDnIdTp_ValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(97, 118);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(36, 13);
            this.label23.TabIndex = 12;
            this.label23.Text = "ID ТП";
            // 
            // BalanceC
            // 
            this.BalanceC.Location = new System.Drawing.Point(209, 61);
            this.BalanceC.MaxLength = 10;
            this.BalanceC.Name = "BalanceC";
            this.BalanceC.Size = new System.Drawing.Size(69, 20);
            this.BalanceC.TabIndex = 11;
            this.BalanceC.TextChanged += new System.EventHandler(this.BalanceC_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(166, 64);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Сумма";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 15);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(80, 13);
            this.label21.TabIndex = 9;
            this.label21.Text = "Время въезда";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd MMMM  yyyy    HH:mm:ss";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(89, 10);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(190, 20);
            this.dateTimePicker1.TabIndex = 8;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // numericUpDnZonaC
            // 
            this.numericUpDnZonaC.Location = new System.Drawing.Point(139, 87);
            this.numericUpDnZonaC.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDnZonaC.Name = "numericUpDnZonaC";
            this.numericUpDnZonaC.Size = new System.Drawing.Size(37, 20);
            this.numericUpDnZonaC.TabIndex = 7;
            this.numericUpDnZonaC.ValueChanged += new System.EventHandler(this.numericUpDnZonaC_ValueChanged);
            // 
            // numericUpDnIdTcC
            // 
            this.numericUpDnIdTcC.Location = new System.Drawing.Point(52, 114);
            this.numericUpDnIdTcC.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDnIdTcC.Name = "numericUpDnIdTcC";
            this.numericUpDnIdTcC.Size = new System.Drawing.Size(37, 20);
            this.numericUpDnIdTcC.TabIndex = 6;
            this.numericUpDnIdTcC.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDnIdTcC.ValueChanged += new System.EventHandler(this.numericUpDnIdTcC_ValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 118);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "ID TC";
            // 
            // numericUpDnGroupC
            // 
            this.numericUpDnGroupC.Location = new System.Drawing.Point(52, 88);
            this.numericUpDnGroupC.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDnGroupC.Name = "numericUpDnGroupC";
            this.numericUpDnGroupC.Size = new System.Drawing.Size(37, 20);
            this.numericUpDnGroupC.TabIndex = 3;
            this.numericUpDnGroupC.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDnGroupC.ValueChanged += new System.EventHandler(this.numericUpDnGroupC_ValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(101, 90);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "Зона";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 90);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Группа";
            // 
            // IdCard
            // 
            this.IdCard.Location = new System.Drawing.Point(52, 63);
            this.IdCard.MaxLength = 17;
            this.IdCard.Name = "IdCard";
            this.IdCard.Size = new System.Drawing.Size(108, 20);
            this.IdCard.TabIndex = 1;
            this.IdCard.TextChanged += new System.EventHandler(this.IdCard_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 66);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "ID карты";
            // 
            // labelBarcodeD
            // 
            this.labelBarcodeD.Location = new System.Drawing.Point(665, 55);
            this.labelBarcodeD.Name = "labelBarcodeD";
            this.labelBarcodeD.Size = new System.Drawing.Size(105, 107);
            this.labelBarcodeD.TabIndex = 29;
            this.labelBarcodeD.Text = "Barcode";
            // 
            // labelPropuskAvto
            // 
            this.labelPropuskAvto.AutoSize = true;
            this.labelPropuskAvto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelPropuskAvto.Location = new System.Drawing.Point(71, 116);
            this.labelPropuskAvto.Name = "labelPropuskAvto";
            this.labelPropuskAvto.Size = new System.Drawing.Size(87, 15);
            this.labelPropuskAvto.TabIndex = 28;
            this.labelPropuskAvto.Text = "Пропуск авто 0";
            this.labelPropuskAvto.Click += new System.EventHandler(this.labelPropuskAvto_Click);
            // 
            // labelStatSlave
            // 
            this.labelStatSlave.AutoSize = true;
            this.labelStatSlave.Location = new System.Drawing.Point(125, 144);
            this.labelStatSlave.Name = "labelStatSlave";
            this.labelStatSlave.Size = new System.Drawing.Size(112, 13);
            this.labelStatSlave.TabIndex = 27;
            this.labelStatSlave.Text = "статус не определен";
            // 
            // labelReaderOut
            // 
            this.labelReaderOut.AutoSize = true;
            this.labelReaderOut.Location = new System.Drawing.Point(564, 145);
            this.labelReaderOut.Name = "labelReaderOut";
            this.labelReaderOut.Size = new System.Drawing.Size(70, 13);
            this.labelReaderOut.TabIndex = 26;
            this.labelReaderOut.Text = "Не работает";
            // 
            // labelDispens
            // 
            this.labelDispens.AutoSize = true;
            this.labelDispens.Location = new System.Drawing.Point(390, 144);
            this.labelDispens.Name = "labelDispens";
            this.labelDispens.Size = new System.Drawing.Size(70, 13);
            this.labelDispens.TabIndex = 25;
            this.labelDispens.Text = "Не работает";
            // 
            // labelReaderIn
            // 
            this.labelReaderIn.AutoSize = true;
            this.labelReaderIn.Location = new System.Drawing.Point(243, 144);
            this.labelReaderIn.Name = "labelReaderIn";
            this.labelReaderIn.Size = new System.Drawing.Size(70, 13);
            this.labelReaderIn.TabIndex = 24;
            this.labelReaderIn.Text = "Не работает";
            // 
            // labelSlave
            // 
            this.labelSlave.AutoSize = true;
            this.labelSlave.Location = new System.Drawing.Point(21, 145);
            this.labelSlave.Name = "labelSlave";
            this.labelSlave.Size = new System.Drawing.Size(83, 13);
            this.labelSlave.TabIndex = 23;
            this.labelSlave.Text = "Slave работает";
            // 
            // sItog
            // 
            this.sItog.Location = new System.Drawing.Point(119, 81);
            this.sItog.Name = "sItog";
            this.sItog.Size = new System.Drawing.Size(69, 20);
            this.sItog.TabIndex = 22;
            this.sItog.TextChanged += new System.EventHandler(this.sItog_TextChanged);
            this.sItog.DoubleClick += new System.EventHandler(this.sItog_DoubleClick);
            // 
            // labelItog
            // 
            this.labelItog.AutoSize = true;
            this.labelItog.Location = new System.Drawing.Point(194, 84);
            this.labelItog.Name = "labelItog";
            this.labelItog.Size = new System.Drawing.Size(31, 13);
            this.labelItog.TabIndex = 21;
            this.labelItog.Text = "Итог";
            // 
            // statReadOut
            // 
            this.statReadOut.FormattingEnabled = true;
            this.statReadOut.Items.AddRange(new object[] {
            "неопределен",
            "не отвечает",
            "нет карты",
            "есть карта",
            "карта прочитана",
            "карта записана",
            "ошибка чтения",
            "ошибка записи",
            "чужая карта"});
            this.statReadOut.Location = new System.Drawing.Point(538, 113);
            this.statReadOut.Name = "statReadOut";
            this.statReadOut.Size = new System.Drawing.Size(121, 21);
            this.statReadOut.TabIndex = 19;
            this.statReadOut.SelectedIndexChanged += new System.EventHandler(this.statReadOut_SelectedIndexChanged);
            // 
            // statReadIn
            // 
            this.statReadIn.FormattingEnabled = true;
            this.statReadIn.Items.AddRange(new object[] {
            "неопределен",
            "не отвечает",
            "нет карты",
            "есть карта",
            "карта прочитана",
            "карта записана",
            "ошибка чтения",
            "ошибка записи",
            "чужая карта"});
            this.statReadIn.Location = new System.Drawing.Point(212, 113);
            this.statReadIn.Name = "statReadIn";
            this.statReadIn.Size = new System.Drawing.Size(121, 21);
            this.statReadIn.TabIndex = 18;
            this.statReadIn.SelectedIndexChanged += new System.EventHandler(this.statReadIn_SelectedIndexChanged);
            // 
            // statDispens
            // 
            this.statDispens.FormattingEnabled = true;
            this.statDispens.Items.AddRange(new object[] {
            "неопределен",
            "не отвечает",
            "нет карт",
            "карта под ридером",
            "карта в губах",
            "нет карты в тракте",
            "карта застряла"});
            this.statDispens.Location = new System.Drawing.Point(339, 113);
            this.statDispens.Name = "statDispens";
            this.statDispens.Size = new System.Drawing.Size(121, 21);
            this.statDispens.TabIndex = 17;
            this.statDispens.SelectedIndexChanged += new System.EventHandler(this.statDispens_SelectedIndexChanged);
            // 
            // comToReadOut
            // 
            this.comToReadOut.AutoSize = true;
            this.comToReadOut.Location = new System.Drawing.Point(564, 85);
            this.comToReadOut.Name = "comToReadOut";
            this.comToReadOut.Size = new System.Drawing.Size(57, 13);
            this.comToReadOut.TabIndex = 16;
            this.comToReadOut.Text = "ожидание";
            // 
            // comToDispens
            // 
            this.comToDispens.AutoSize = true;
            this.comToDispens.Location = new System.Drawing.Point(390, 84);
            this.comToDispens.Name = "comToDispens";
            this.comToDispens.Size = new System.Drawing.Size(57, 13);
            this.comToDispens.TabIndex = 14;
            this.comToDispens.Text = "ожидание";
            // 
            // comToReadIn
            // 
            this.comToReadIn.AutoSize = true;
            this.comToReadIn.Location = new System.Drawing.Point(243, 84);
            this.comToReadIn.Name = "comToReadIn";
            this.comToReadIn.Size = new System.Drawing.Size(57, 13);
            this.comToReadIn.TabIndex = 15;
            this.comToReadIn.Text = "ожидание";
            // 
            // buttonDebugExit
            // 
            this.buttonDebugExit.Location = new System.Drawing.Point(759, 3);
            this.buttonDebugExit.Name = "buttonDebugExit";
            this.buttonDebugExit.Size = new System.Drawing.Size(23, 23);
            this.buttonDebugExit.TabIndex = 11;
            this.buttonDebugExit.Text = "Х";
            this.buttonDebugExit.UseVisualStyleBackColor = true;
            this.buttonDebugExit.Click += new System.EventHandler(this.buttonDebugExit_Click);
            // 
            // labelEtap
            // 
            this.labelEtap.AutoSize = true;
            this.labelEtap.Location = new System.Drawing.Point(22, 116);
            this.labelEtap.Name = "labelEtap";
            this.labelEtap.Size = new System.Drawing.Size(31, 13);
            this.labelEtap.TabIndex = 10;
            this.labelEtap.Text = "Этап";
            // 
            // reader_int
            // 
            this.reader_int.AutoSize = true;
            this.reader_int.Location = new System.Drawing.Point(243, 51);
            this.reader_int.Name = "reader_int";
            this.reader_int.Size = new System.Drawing.Size(74, 13);
            this.reader_int.TabIndex = 7;
            this.reader_int.Text = "ридер внутри";
            this.reader_int.Click += new System.EventHandler(this.reader_int_Click);
            // 
            // reader_out
            // 
            this.reader_out.AutoSize = true;
            this.reader_out.Location = new System.Drawing.Point(564, 54);
            this.reader_out.Name = "reader_out";
            this.reader_out.Size = new System.Drawing.Size(84, 13);
            this.reader_out.TabIndex = 8;
            this.reader_out.Text = "ридер внешний";
            // 
            // DISPENS
            // 
            this.DISPENS.AutoSize = true;
            this.DISPENS.Location = new System.Drawing.Point(390, 51);
            this.DISPENS.Name = "DISPENS";
            this.DISPENS.Size = new System.Drawing.Size(61, 13);
            this.DISPENS.TabIndex = 6;
            this.DISPENS.Text = "диспенсер";
            // 
            // PUSH
            // 
            this.PUSH.AutoSize = true;
            this.PUSH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PUSH.Location = new System.Drawing.Point(122, 84);
            this.PUSH.Name = "PUSH";
            this.PUSH.Size = new System.Drawing.Size(66, 15);
            this.PUSH.TabIndex = 5;
            this.PUSH.Text = "НАЖМИТЕ";
            this.PUSH.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PUSH_MouseDown);
            this.PUSH.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PUSH_MouseUp);
            // 
            // trafficLight
            // 
            this.trafficLight.AutoSize = true;
            this.trafficLight.Location = new System.Drawing.Point(122, 54);
            this.trafficLight.Name = "trafficLight";
            this.trafficLight.Size = new System.Drawing.Size(55, 13);
            this.trafficLight.TabIndex = 4;
            this.trafficLight.Text = "traffic light";
            // 
            // barrier
            // 
            this.barrier.AutoSize = true;
            this.barrier.Location = new System.Drawing.Point(122, 21);
            this.barrier.Name = "barrier";
            this.barrier.Size = new System.Drawing.Size(36, 13);
            this.barrier.TabIndex = 3;
            this.barrier.Text = "barrier";
            // 
            // IR
            // 
            this.IR.AutoSize = true;
            this.IR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IR.Location = new System.Drawing.Point(21, 54);
            this.IR.Name = "IR";
            this.IR.Size = new System.Drawing.Size(75, 15);
            this.IR.TabIndex = 2;
            this.IR.Text = "ИК свободен";
            this.IR.Click += new System.EventHandler(this.IR_Click);
            // 
            // loopB
            // 
            this.loopB.AutoSize = true;
            this.loopB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.loopB.Location = new System.Drawing.Point(21, 84);
            this.loopB.Name = "loopB";
            this.loopB.Size = new System.Drawing.Size(87, 15);
            this.loopB.TabIndex = 1;
            this.loopB.Text = "loopB свободна";
            this.loopB.Click += new System.EventHandler(this.loopB_Click);
            // 
            // loopA
            // 
            this.loopA.AutoSize = true;
            this.loopA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.loopA.Location = new System.Drawing.Point(21, 21);
            this.loopA.Name = "loopA";
            this.loopA.Size = new System.Drawing.Size(87, 15);
            this.loopA.TabIndex = 0;
            this.loopA.Text = "loopA свободна";
            this.loopA.Click += new System.EventHandler(this.loopA_Click);
            // 
            // labelMessage7
            // 
            this.labelMessage7.AutoSize = true;
            this.labelMessage7.BackColor = System.Drawing.Color.Transparent;
            this.labelMessage7.Location = new System.Drawing.Point(485, 577);
            this.labelMessage7.Name = "labelMessage7";
            this.labelMessage7.Size = new System.Drawing.Size(78, 13);
            this.labelMessage7.TabIndex = 15;
            this.labelMessage7.Text = "labelMessage7";
            // 
            // labelMessage6
            // 
            this.labelMessage6.AutoSize = true;
            this.labelMessage6.BackColor = System.Drawing.Color.Transparent;
            this.labelMessage6.Location = new System.Drawing.Point(330, 577);
            this.labelMessage6.Name = "labelMessage6";
            this.labelMessage6.Size = new System.Drawing.Size(78, 13);
            this.labelMessage6.TabIndex = 14;
            this.labelMessage6.Text = "labelMessage6";
            // 
            // labelMessage5
            // 
            this.labelMessage5.AutoSize = true;
            this.labelMessage5.BackColor = System.Drawing.Color.Transparent;
            this.labelMessage5.Location = new System.Drawing.Point(107, 577);
            this.labelMessage5.Name = "labelMessage5";
            this.labelMessage5.Size = new System.Drawing.Size(78, 13);
            this.labelMessage5.TabIndex = 13;
            this.labelMessage5.Text = "labelMessage5";
            // 
            // labelMessage4
            // 
            this.labelMessage4.AutoSize = true;
            this.labelMessage4.BackColor = System.Drawing.Color.Transparent;
            this.labelMessage4.Location = new System.Drawing.Point(14, 578);
            this.labelMessage4.Name = "labelMessage4";
            this.labelMessage4.Size = new System.Drawing.Size(78, 13);
            this.labelMessage4.TabIndex = 12;
            this.labelMessage4.Text = "labelMessage4";
            // 
            // comboBoxDebugUI
            // 
            this.comboBoxDebugUI.FormattingEnabled = true;
            this.comboBoxDebugUI.Location = new System.Drawing.Point(543, 9);
            this.comboBoxDebugUI.Name = "comboBoxDebugUI";
            this.comboBoxDebugUI.Size = new System.Drawing.Size(247, 21);
            this.comboBoxDebugUI.TabIndex = 11;
            // 
            // RPS_1
            // 
            this.RPS_1.AutoSize = true;
            this.RPS_1.BackColor = System.Drawing.Color.Transparent;
            this.RPS_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RPS_1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.RPS_1.Location = new System.Drawing.Point(37, 33);
            this.RPS_1.Name = "RPS_1";
            this.RPS_1.Size = new System.Drawing.Size(36, 17);
            this.RPS_1.TabIndex = 6;
            this.RPS_1.Text = "RPS";
            // 
            // labelMessage2
            // 
            this.labelMessage2.Location = new System.Drawing.Point(0, 0);
            this.labelMessage2.Name = "labelMessage2";
            this.labelMessage2.Size = new System.Drawing.Size(100, 23);
            this.labelMessage2.TabIndex = 16;
            // 
            // time
            // 
            this.time.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.time.AutoSize = true;
            this.time.BackColor = System.Drawing.Color.Transparent;
            this.time.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.time.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.time.Location = new System.Drawing.Point(566, 33);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(171, 17);
            this.time.TabIndex = 4;
            this.time.Text = "15 июня 2009 г. 13:45:30";
            // 
            // labelMessage1
            // 
            this.labelMessage1.Location = new System.Drawing.Point(0, 0);
            this.labelMessage1.Name = "labelMessage1";
            this.labelMessage1.Size = new System.Drawing.Size(100, 23);
            this.labelMessage1.TabIndex = 17;
            // 
            // labelMessage3
            // 
            this.labelMessage3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMessage3.BackColor = System.Drawing.Color.Transparent;
            this.labelMessage3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelMessage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.labelMessage3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelMessage3.Location = new System.Drawing.Point(5, 519);
            this.labelMessage3.Name = "labelMessage3";
            this.labelMessage3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelMessage3.Size = new System.Drawing.Size(780, 40);
            this.labelMessage3.TabIndex = 9;
            this.labelMessage3.Text = "Message3";
            this.labelMessage3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(330, 45);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 140);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // labelMessage0
            // 
            this.labelMessage0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMessage0.BackColor = System.Drawing.Color.Transparent;
            this.labelMessage0.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelMessage0.Location = new System.Drawing.Point(10, 60);
            this.labelMessage0.Name = "labelMessage0";
            this.labelMessage0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelMessage0.Size = new System.Drawing.Size(780, 40);
            this.labelMessage0.TabIndex = 8;
            this.labelMessage0.Text = "Message0";
            this.labelMessage0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // texmenu
            // 
            this.texmenu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.texmenu.Controls.Add(this.tabControlService);
            this.texmenu.Controls.Add(this.labelLicenseWork);
            this.texmenu.Controls.Add(this.labelErrBD);
            this.texmenu.Controls.Add(this.buttonExit);
            this.texmenu.Controls.Add(this.buttonMax);
            this.texmenu.Controls.Add(this.timer_serv);
            this.texmenu.Controls.Add(this.ButtonMinimiz);
            this.texmenu.Controls.Add(this.to_klient);
            this.texmenu.Controls.Add(this.service);
            this.texmenu.Location = new System.Drawing.Point(3, 33);
            this.texmenu.Name = "texmenu";
            this.texmenu.Size = new System.Drawing.Size(800, 600);
            this.texmenu.TabIndex = 1;
            this.texmenu.Visible = false;
            // 
            // tabControlService
            // 
            this.tabControlService.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlService.Controls.Add(this.tabPage1);
            this.tabControlService.Controls.Add(this.tabPage2);
            this.tabControlService.Controls.Add(this.tabAddSettings);
            this.tabControlService.Controls.Add(this.tabCheck);
            this.tabControlService.Controls.Add(this.tabDebug);
            this.tabControlService.Location = new System.Drawing.Point(16, 28);
            this.tabControlService.Name = "tabControlService";
            this.tabControlService.SelectedIndex = 0;
            this.tabControlService.Size = new System.Drawing.Size(790, 565);
            this.tabControlService.TabIndex = 6;
            this.tabControlService.SelectedIndexChanged += new System.EventHandler(this.tabControlService_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tabDiag);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(782, 539);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ДИАГНОСТИКА";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabDiag
            // 
            this.tabDiag.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabDiag.Controls.Add(this.tabDiagStatus);
            this.tabDiag.Controls.Add(this.tabDiagReaderOut);
            this.tabDiag.Controls.Add(this.tabDiagReaderIn);
            this.tabDiag.Controls.Add(this.tabDiagDispenser);
            this.tabDiag.Controls.Add(this.tabDiagBarcode);
            this.tabDiag.Controls.Add(this.tabDiagSlave);
            this.tabDiag.Controls.Add(this.tabDiagTablo);
            this.tabDiag.Controls.Add(this.tabDiagWebServer);
            this.tabDiag.Controls.Add(this.tabDiagBankModule);
            this.tabDiag.Controls.Add(this.tabDiagRFID);
            this.tabDiag.Controls.Add(this.tabDiagCollector);
            this.tabDiag.Location = new System.Drawing.Point(1, 1);
            this.tabDiag.Name = "tabDiag";
            this.tabDiag.SelectedIndex = 0;
            this.tabDiag.Size = new System.Drawing.Size(780, 574);
            this.tabDiag.TabIndex = 0;
            // 
            // tabDiagStatus
            // 
            this.tabDiagStatus.Controls.Add(this.labelStatDevice);
            this.tabDiagStatus.Controls.Add(this.labelStstusOut);
            this.tabDiagStatus.Location = new System.Drawing.Point(4, 22);
            this.tabDiagStatus.Name = "tabDiagStatus";
            this.tabDiagStatus.Padding = new System.Windows.Forms.Padding(3);
            this.tabDiagStatus.Size = new System.Drawing.Size(772, 548);
            this.tabDiagStatus.TabIndex = 0;
            this.tabDiagStatus.Text = "Статус";
            this.tabDiagStatus.UseVisualStyleBackColor = true;
            // 
            // labelStatDevice
            // 
            this.labelStatDevice.AutoSize = true;
            this.labelStatDevice.Location = new System.Drawing.Point(252, 37);
            this.labelStatDevice.Name = "labelStatDevice";
            this.labelStatDevice.Size = new System.Drawing.Size(156, 78);
            this.labelStatDevice.TabIndex = 1;
            this.labelStatDevice.Text = "Статус внутренних устройств\r\nSlave Дискрет ок\r\nРидер внешний нет карт\r\nРидер внут" +
    "ренни нет карт\r\nДиспенсер нет карт в тракте\r\nКарт мало";
            // 
            // labelStstusOut
            // 
            this.labelStstusOut.AutoSize = true;
            this.labelStstusOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.labelStstusOut.Location = new System.Drawing.Point(31, 37);
            this.labelStstusOut.Name = "labelStstusOut";
            this.labelStstusOut.Size = new System.Drawing.Size(158, 104);
            this.labelStstusOut.TabIndex = 0;
            this.labelStstusOut.Text = "Статус внешних устройств\r\nloopA свободна\r\nloopB свободна\r\nИК свободен\r\nкнопка НАЖ" +
    "МИТЕ не нажата\r\nкнопка ВЫЗОВ не нажата\r\nверхняя дверь закрыта\r\nнижняя дверь закр" +
    "ыта";
            // 
            // tabDiagReaderOut
            // 
            this.tabDiagReaderOut.Controls.Add(this.grOmnikey5427OutReader);
            this.tabDiagReaderOut.Controls.Add(this.grMT625OutReader);
            this.tabDiagReaderOut.Location = new System.Drawing.Point(4, 22);
            this.tabDiagReaderOut.Name = "tabDiagReaderOut";
            this.tabDiagReaderOut.Padding = new System.Windows.Forms.Padding(3);
            this.tabDiagReaderOut.Size = new System.Drawing.Size(772, 548);
            this.tabDiagReaderOut.TabIndex = 1;
            this.tabDiagReaderOut.Text = "Ридер внешний";
            this.tabDiagReaderOut.UseVisualStyleBackColor = true;
            // 
            // grOmnikey5427OutReader
            // 
            this.grOmnikey5427OutReader.Controls.Add(this.lblOmniGetId);
            this.grOmnikey5427OutReader.Controls.Add(this.lblOmniCardID);
            this.grOmnikey5427OutReader.Controls.Add(this.lblOmniStatus);
            this.grOmnikey5427OutReader.Controls.Add(this.lblOmniCard);
            this.grOmnikey5427OutReader.Controls.Add(this.lblDeviceState);
            this.grOmnikey5427OutReader.Controls.Add(this.lblEnableWrite);
            this.grOmnikey5427OutReader.Controls.Add(this.lblEnableRead);
            this.grOmnikey5427OutReader.Controls.Add(this.lblOmniEnabled);
            this.grOmnikey5427OutReader.Location = new System.Drawing.Point(6, 21);
            this.grOmnikey5427OutReader.Name = "grOmnikey5427OutReader";
            this.grOmnikey5427OutReader.Size = new System.Drawing.Size(765, 479);
            this.grOmnikey5427OutReader.TabIndex = 17;
            this.grOmnikey5427OutReader.TabStop = false;
            this.grOmnikey5427OutReader.Text = "Omnikey 5427";
            // 
            // lblOmniGetId
            // 
            this.lblOmniGetId.AutoSize = true;
            this.lblOmniGetId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOmniGetId.Location = new System.Drawing.Point(22, 75);
            this.lblOmniGetId.Name = "lblOmniGetId";
            this.lblOmniGetId.Size = new System.Drawing.Size(45, 15);
            this.lblOmniGetId.TabIndex = 29;
            this.lblOmniGetId.Text = "ID Card";
            this.lblOmniGetId.Click += new System.EventHandler(this.lblOmniGetId_Click);
            // 
            // lblOmniCardID
            // 
            this.lblOmniCardID.AutoSize = true;
            this.lblOmniCardID.Location = new System.Drawing.Point(169, 75);
            this.lblOmniCardID.Name = "lblOmniCardID";
            this.lblOmniCardID.Size = new System.Drawing.Size(61, 13);
            this.lblOmniCardID.TabIndex = 28;
            this.lblOmniCardID.Text = "_________";
            // 
            // lblOmniStatus
            // 
            this.lblOmniStatus.AutoSize = true;
            this.lblOmniStatus.Location = new System.Drawing.Point(300, 46);
            this.lblOmniStatus.Name = "lblOmniStatus";
            this.lblOmniStatus.Size = new System.Drawing.Size(60, 13);
            this.lblOmniStatus.TabIndex = 27;
            this.lblOmniStatus.Text = "состояние";
            // 
            // lblOmniCard
            // 
            this.lblOmniCard.AutoSize = true;
            this.lblOmniCard.Location = new System.Drawing.Point(169, 46);
            this.lblOmniCard.Name = "lblOmniCard";
            this.lblOmniCard.Size = new System.Drawing.Size(50, 13);
            this.lblOmniCard.TabIndex = 26;
            this.lblOmniCard.Text = "нет карт";
            // 
            // lblDeviceState
            // 
            this.lblDeviceState.AutoSize = true;
            this.lblDeviceState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDeviceState.Location = new System.Drawing.Point(21, 48);
            this.lblDeviceState.Name = "lblDeviceState";
            this.lblDeviceState.Size = new System.Drawing.Size(63, 15);
            this.lblDeviceState.TabIndex = 25;
            this.lblDeviceState.Text = "Состояние";
            // 
            // lblEnableWrite
            // 
            this.lblEnableWrite.AutoSize = true;
            this.lblEnableWrite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEnableWrite.Location = new System.Drawing.Point(18, 209);
            this.lblEnableWrite.Name = "lblEnableWrite";
            this.lblEnableWrite.Size = new System.Drawing.Size(46, 15);
            this.lblEnableWrite.TabIndex = 24;
            this.lblEnableWrite.Text = "Запись";
            // 
            // lblEnableRead
            // 
            this.lblEnableRead.AutoSize = true;
            this.lblEnableRead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEnableRead.Location = new System.Drawing.Point(18, 179);
            this.lblEnableRead.Name = "lblEnableRead";
            this.lblEnableRead.Size = new System.Drawing.Size(46, 15);
            this.lblEnableRead.TabIndex = 23;
            this.lblEnableRead.Text = "Чтение";
            // 
            // lblOmniEnabled
            // 
            this.lblOmniEnabled.AutoSize = true;
            this.lblOmniEnabled.Location = new System.Drawing.Point(18, 28);
            this.lblOmniEnabled.Name = "lblOmniEnabled";
            this.lblOmniEnabled.Size = new System.Drawing.Size(78, 13);
            this.lblOmniEnabled.TabIndex = 14;
            this.lblOmniEnabled.Text = "No Connection";
            // 
            // grMT625OutReader
            // 
            this.grMT625OutReader.Controls.Add(this.labelOutNewComand);
            this.grMT625OutReader.Controls.Add(this.labelRxOut);
            this.grMT625OutReader.Controls.Add(this.label30);
            this.grMT625OutReader.Controls.Add(this.labelOutWrite);
            this.grMT625OutReader.Controls.Add(this.labelOutRead);
            this.grMT625OutReader.Controls.Add(this.labelOutKeyBok);
            this.grMT625OutReader.Controls.Add(this.labelOutKeyB);
            this.grMT625OutReader.Controls.Add(this.labelOutKeyAok);
            this.grMT625OutReader.Controls.Add(this.labelOutKeyA);
            this.grMT625OutReader.Controls.Add(this.labelOutIdCard);
            this.grMT625OutReader.Controls.Add(this.labelGetId);
            this.grMT625OutReader.Controls.Add(this.noCardOut);
            this.grMT625OutReader.Controls.Add(this.OutGetCard);
            this.grMT625OutReader.Controls.Add(this.labelComOut);
            this.grMT625OutReader.Location = new System.Drawing.Point(13, 19);
            this.grMT625OutReader.Name = "grMT625OutReader";
            this.grMT625OutReader.Size = new System.Drawing.Size(764, 480);
            this.grMT625OutReader.TabIndex = 14;
            this.grMT625OutReader.TabStop = false;
            this.grMT625OutReader.Text = "MT625";
            // 
            // labelOutNewComand
            // 
            this.labelOutNewComand.AutoSize = true;
            this.labelOutNewComand.Location = new System.Drawing.Point(97, 191);
            this.labelOutNewComand.Name = "labelOutNewComand";
            this.labelOutNewComand.Size = new System.Drawing.Size(68, 13);
            this.labelOutNewComand.TabIndex = 27;
            this.labelOutNewComand.Text = "NewComand";
            // 
            // labelRxOut
            // 
            this.labelRxOut.AutoSize = true;
            this.labelRxOut.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRxOut.Location = new System.Drawing.Point(253, 145);
            this.labelRxOut.MaximumSize = new System.Drawing.Size(350, 100);
            this.labelRxOut.MinimumSize = new System.Drawing.Size(100, 100);
            this.labelRxOut.Name = "labelRxOut";
            this.labelRxOut.Size = new System.Drawing.Size(100, 100);
            this.labelRxOut.TabIndex = 26;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(298, 117);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(103, 13);
            this.label30.TabIndex = 25;
            this.label30.Text = "Принято от ридера";
            // 
            // labelOutWrite
            // 
            this.labelOutWrite.AutoSize = true;
            this.labelOutWrite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelOutWrite.Location = new System.Drawing.Point(18, 209);
            this.labelOutWrite.Name = "labelOutWrite";
            this.labelOutWrite.Size = new System.Drawing.Size(46, 15);
            this.labelOutWrite.TabIndex = 24;
            this.labelOutWrite.Text = "Запись";
            // 
            // labelOutRead
            // 
            this.labelOutRead.AutoSize = true;
            this.labelOutRead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelOutRead.Location = new System.Drawing.Point(18, 179);
            this.labelOutRead.Name = "labelOutRead";
            this.labelOutRead.Size = new System.Drawing.Size(46, 15);
            this.labelOutRead.TabIndex = 23;
            this.labelOutRead.Text = "Чтение";
            // 
            // labelOutKeyBok
            // 
            this.labelOutKeyBok.AutoSize = true;
            this.labelOutKeyBok.Location = new System.Drawing.Point(105, 146);
            this.labelOutKeyBok.Name = "labelOutKeyBok";
            this.labelOutKeyBok.Size = new System.Drawing.Size(34, 13);
            this.labelOutKeyBok.TabIndex = 22;
            this.labelOutKeyBok.Text = "key B";
            // 
            // labelOutKeyB
            // 
            this.labelOutKeyB.AutoSize = true;
            this.labelOutKeyB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelOutKeyB.Location = new System.Drawing.Point(18, 146);
            this.labelOutKeyB.Name = "labelOutKeyB";
            this.labelOutKeyB.Size = new System.Drawing.Size(45, 15);
            this.labelOutKeyB.TabIndex = 21;
            this.labelOutKeyB.Text = "Ключ В";
            // 
            // labelOutKeyAok
            // 
            this.labelOutKeyAok.AutoSize = true;
            this.labelOutKeyAok.Location = new System.Drawing.Point(105, 115);
            this.labelOutKeyAok.Name = "labelOutKeyAok";
            this.labelOutKeyAok.Size = new System.Drawing.Size(37, 13);
            this.labelOutKeyAok.TabIndex = 20;
            this.labelOutKeyAok.Text = "key A ";
            // 
            // labelOutKeyA
            // 
            this.labelOutKeyA.AutoSize = true;
            this.labelOutKeyA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelOutKeyA.Location = new System.Drawing.Point(18, 115);
            this.labelOutKeyA.Name = "labelOutKeyA";
            this.labelOutKeyA.Size = new System.Drawing.Size(45, 15);
            this.labelOutKeyA.TabIndex = 19;
            this.labelOutKeyA.Text = "Ключ А";
            // 
            // labelOutIdCard
            // 
            this.labelOutIdCard.AutoSize = true;
            this.labelOutIdCard.Location = new System.Drawing.Point(103, 83);
            this.labelOutIdCard.Name = "labelOutIdCard";
            this.labelOutIdCard.Size = new System.Drawing.Size(61, 13);
            this.labelOutIdCard.TabIndex = 18;
            this.labelOutIdCard.Text = "_________";
            // 
            // labelGetId
            // 
            this.labelGetId.AutoSize = true;
            this.labelGetId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelGetId.Location = new System.Drawing.Point(18, 81);
            this.labelGetId.Name = "labelGetId";
            this.labelGetId.Size = new System.Drawing.Size(45, 15);
            this.labelGetId.TabIndex = 17;
            this.labelGetId.Text = "ID Card";
            this.labelGetId.Click += new System.EventHandler(this.labelGetId_Click);
            // 
            // noCardOut
            // 
            this.noCardOut.AutoSize = true;
            this.noCardOut.Location = new System.Drawing.Point(102, 53);
            this.noCardOut.Name = "noCardOut";
            this.noCardOut.Size = new System.Drawing.Size(50, 13);
            this.noCardOut.TabIndex = 16;
            this.noCardOut.Text = "нет карт";
            // 
            // OutGetCard
            // 
            this.OutGetCard.AutoSize = true;
            this.OutGetCard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.OutGetCard.Location = new System.Drawing.Point(18, 53);
            this.OutGetCard.Name = "OutGetCard";
            this.OutGetCard.Size = new System.Drawing.Size(48, 15);
            this.OutGetCard.TabIndex = 15;
            this.OutGetCard.Text = "GetCard";
            // 
            // labelComOut
            // 
            this.labelComOut.AutoSize = true;
            this.labelComOut.Location = new System.Drawing.Point(18, 28);
            this.labelComOut.Name = "labelComOut";
            this.labelComOut.Size = new System.Drawing.Size(31, 13);
            this.labelComOut.TabIndex = 14;
            this.labelComOut.Text = "COM";
            // 
            // tabDiagReaderIn
            // 
            this.tabDiagReaderIn.Controls.Add(this.labelInNewComand);
            this.tabDiagReaderIn.Controls.Add(this.labeletapReadCard);
            this.tabDiagReaderIn.Controls.Add(this.labelGetIdIn);
            this.tabDiagReaderIn.Controls.Add(this.InGetCard);
            this.tabDiagReaderIn.Controls.Add(this.labelInIdCard);
            this.tabDiagReaderIn.Controls.Add(this.noCardIn);
            this.tabDiagReaderIn.Controls.Add(this.labelRxIn);
            this.tabDiagReaderIn.Controls.Add(this.labelComIn);
            this.tabDiagReaderIn.Location = new System.Drawing.Point(4, 22);
            this.tabDiagReaderIn.Name = "tabDiagReaderIn";
            this.tabDiagReaderIn.Padding = new System.Windows.Forms.Padding(3);
            this.tabDiagReaderIn.Size = new System.Drawing.Size(772, 548);
            this.tabDiagReaderIn.TabIndex = 2;
            this.tabDiagReaderIn.Text = "Ридер внутренний";
            this.tabDiagReaderIn.UseVisualStyleBackColor = true;
            // 
            // labelInNewComand
            // 
            this.labelInNewComand.AutoSize = true;
            this.labelInNewComand.Location = new System.Drawing.Point(115, 176);
            this.labelInNewComand.Name = "labelInNewComand";
            this.labelInNewComand.Size = new System.Drawing.Size(68, 13);
            this.labelInNewComand.TabIndex = 8;
            this.labelInNewComand.Text = "NewComand";
            // 
            // labeletapReadCard
            // 
            this.labeletapReadCard.AutoSize = true;
            this.labeletapReadCard.Location = new System.Drawing.Point(360, 28);
            this.labeletapReadCard.Name = "labeletapReadCard";
            this.labeletapReadCard.Size = new System.Drawing.Size(51, 13);
            this.labeletapReadCard.TabIndex = 7;
            this.labeletapReadCard.Text = "этап рид";
            // 
            // labelGetIdIn
            // 
            this.labelGetIdIn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelGetIdIn.Location = new System.Drawing.Point(14, 85);
            this.labelGetIdIn.Name = "labelGetIdIn";
            this.labelGetIdIn.Size = new System.Drawing.Size(50, 15);
            this.labelGetIdIn.TabIndex = 6;
            this.labelGetIdIn.Text = "GetId";
            this.labelGetIdIn.Click += new System.EventHandler(this.labelGetIdIn_Click);
            // 
            // InGetCard
            // 
            this.InGetCard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InGetCard.Location = new System.Drawing.Point(12, 49);
            this.InGetCard.Name = "InGetCard";
            this.InGetCard.Size = new System.Drawing.Size(50, 15);
            this.InGetCard.TabIndex = 5;
            this.InGetCard.Text = "GetCard";
            // 
            // labelInIdCard
            // 
            this.labelInIdCard.AutoSize = true;
            this.labelInIdCard.Location = new System.Drawing.Point(112, 85);
            this.labelInIdCard.Name = "labelInIdCard";
            this.labelInIdCard.Size = new System.Drawing.Size(31, 13);
            this.labelInIdCard.TabIndex = 4;
            this.labelInIdCard.Text = "--------";
            // 
            // noCardIn
            // 
            this.noCardIn.AutoSize = true;
            this.noCardIn.Location = new System.Drawing.Point(109, 49);
            this.noCardIn.Name = "noCardIn";
            this.noCardIn.Size = new System.Drawing.Size(50, 13);
            this.noCardIn.TabIndex = 3;
            this.noCardIn.Text = "нет карт";
            // 
            // labelRxIn
            // 
            this.labelRxIn.AutoSize = true;
            this.labelRxIn.Location = new System.Drawing.Point(317, 85);
            this.labelRxIn.Name = "labelRxIn";
            this.labelRxIn.Size = new System.Drawing.Size(91, 13);
            this.labelRxIn.TabIndex = 2;
            this.labelRxIn.Text = "______________";
            // 
            // labelComIn
            // 
            this.labelComIn.AutoSize = true;
            this.labelComIn.Location = new System.Drawing.Point(27, 19);
            this.labelComIn.Name = "labelComIn";
            this.labelComIn.Size = new System.Drawing.Size(28, 13);
            this.labelComIn.TabIndex = 1;
            this.labelComIn.Text = "Com";
            // 
            // tabDiagDispenser
            // 
            this.tabDiagDispenser.Controls.Add(this.grMT166);
            this.tabDiagDispenser.Location = new System.Drawing.Point(4, 22);
            this.tabDiagDispenser.Name = "tabDiagDispenser";
            this.tabDiagDispenser.Size = new System.Drawing.Size(772, 548);
            this.tabDiagDispenser.TabIndex = 3;
            this.tabDiagDispenser.Text = "Диспенсер";
            this.tabDiagDispenser.UseVisualStyleBackColor = true;
            // 
            // grMT166
            // 
            this.grMT166.Controls.Add(this.grKYT2064);
            this.grMT166.Controls.Add(this.lblRecycleToRead);
            this.grMT166.Controls.Add(this.labelRxDisp);
            this.grMT166.Controls.Add(this.labelVersionDispens);
            this.grMT166.Controls.Add(this.lblGetVersion);
            this.grMT166.Controls.Add(this.lblFromBezel);
            this.grMT166.Controls.Add(this.labelDispensStatus);
            this.grMT166.Controls.Add(this.labeletapDispensCard);
            this.grMT166.Controls.Add(this.lblToRecycleBox);
            this.grMT166.Controls.Add(this.lblToBezelAndNotHold);
            this.grMT166.Controls.Add(this.lblToBezelAndHold);
            this.grMT166.Controls.Add(this.lblFromBoxToRead);
            this.grMT166.Controls.Add(this.lblDispensGetStatus);
            this.grMT166.Controls.Add(this.labelComDispens);
            this.grMT166.Location = new System.Drawing.Point(21, 11);
            this.grMT166.Name = "grMT166";
            this.grMT166.Size = new System.Drawing.Size(746, 383);
            this.grMT166.TabIndex = 13;
            this.grMT166.TabStop = false;
            this.grMT166.Text = "MT166";
            // 
            // grKYT2064
            // 
            this.grKYT2064.Controls.Add(this.lblRecycleToRead2);
            this.grKYT2064.Controls.Add(this.lblGetVersion2);
            this.grKYT2064.Controls.Add(this.lblFromBezel2);
            this.grKYT2064.Controls.Add(this.lblToRecycleBox2);
            this.grKYT2064.Controls.Add(this.lblToBezelAndNotHold2);
            this.grKYT2064.Controls.Add(this.lblToBezelAndHold2);
            this.grKYT2064.Controls.Add(this.lblFromBoxToReader2);
            this.grKYT2064.Controls.Add(this.lblDispensGetStatus2);
            this.grKYT2064.Controls.Add(this.lblStFeedOutSol);
            this.grKYT2064.Controls.Add(this.lblStAutoWait);
            this.grKYT2064.Controls.Add(this.lblStStack2Wait);
            this.grKYT2064.Controls.Add(this.lblStStack1Wait);
            this.grKYT2064.Controls.Add(this.lblStStop);
            this.grKYT2064.Controls.Add(this.lblStFeedOut);
            this.grKYT2064.Controls.Add(this.lblStFeedIn);
            this.grKYT2064.Controls.Add(this.lblStCapture);
            this.grKYT2064.Controls.Add(this.lblStAutoIssue);
            this.grKYT2064.Controls.Add(this.lblStStacker2Out);
            this.grKYT2064.Controls.Add(this.lblDispenser2Status);
            this.grKYT2064.Controls.Add(this.lblEtapDisp2);
            this.grKYT2064.Controls.Add(this.lblStStacker1Out);
            this.grKYT2064.Controls.Add(this.lblStStatus);
            this.grKYT2064.Controls.Add(this.lblStClear);
            this.grKYT2064.Controls.Add(this.lblComDispens2);
            this.grKYT2064.Location = new System.Drawing.Point(0, 1);
            this.grKYT2064.Name = "grKYT2064";
            this.grKYT2064.Size = new System.Drawing.Size(746, 389);
            this.grKYT2064.TabIndex = 44;
            this.grKYT2064.TabStop = false;
            this.grKYT2064.Text = "KYT2064";
            // 
            // lblRecycleToRead2
            // 
            this.lblRecycleToRead2.AutoSize = true;
            this.lblRecycleToRead2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRecycleToRead2.Location = new System.Drawing.Point(15, 268);
            this.lblRecycleToRead2.Name = "lblRecycleToRead2";
            this.lblRecycleToRead2.Size = new System.Drawing.Size(94, 15);
            this.lblRecycleToRead2.TabIndex = 65;
            this.lblRecycleToRead2.Text = "Опять под ридер";
            this.lblRecycleToRead2.Click += new System.EventHandler(this.lblRecycleToRead2_Click);
            // 
            // lblGetVersion2
            // 
            this.lblGetVersion2.AutoSize = true;
            this.lblGetVersion2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGetVersion2.Location = new System.Drawing.Point(15, 300);
            this.lblGetVersion2.Name = "lblGetVersion2";
            this.lblGetVersion2.Size = new System.Drawing.Size(61, 15);
            this.lblGetVersion2.TabIndex = 64;
            this.lblGetVersion2.Text = "GetVersion";
            this.lblGetVersion2.Click += new System.EventHandler(this.lblGetVersion2_Click);
            // 
            // lblFromBezel2
            // 
            this.lblFromBezel2.AutoSize = true;
            this.lblFromBezel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFromBezel2.Location = new System.Drawing.Point(15, 235);
            this.lblFromBezel2.Name = "lblFromBezel2";
            this.lblFromBezel2.Size = new System.Drawing.Size(80, 15);
            this.lblFromBezel2.TabIndex = 63;
            this.lblFromBezel2.Text = "Втянуть карту";
            this.lblFromBezel2.Click += new System.EventHandler(this.lblFromBezel2_Click);
            // 
            // lblToRecycleBox2
            // 
            this.lblToRecycleBox2.AutoSize = true;
            this.lblToRecycleBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblToRecycleBox2.Location = new System.Drawing.Point(15, 201);
            this.lblToRecycleBox2.Name = "lblToRecycleBox2";
            this.lblToRecycleBox2.Size = new System.Drawing.Size(97, 15);
            this.lblToRecycleBox2.TabIndex = 62;
            this.lblToRecycleBox2.Text = "Карту в отказник";
            this.lblToRecycleBox2.Click += new System.EventHandler(this.lblToRecycleBox2_Click);
            // 
            // lblToBezelAndNotHold2
            // 
            this.lblToBezelAndNotHold2.AutoSize = true;
            this.lblToBezelAndNotHold2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblToBezelAndNotHold2.Location = new System.Drawing.Point(15, 164);
            this.lblToBezelAndNotHold2.Name = "lblToBezelAndNotHold2";
            this.lblToBezelAndNotHold2.Size = new System.Drawing.Size(96, 15);
            this.lblToBezelAndNotHold2.TabIndex = 61;
            this.lblToBezelAndNotHold2.Text = "Выбросить карту";
            this.lblToBezelAndNotHold2.Click += new System.EventHandler(this.lblToBezelAndNotHold2_Click);
            // 
            // lblToBezelAndHold2
            // 
            this.lblToBezelAndHold2.AutoSize = true;
            this.lblToBezelAndHold2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblToBezelAndHold2.Location = new System.Drawing.Point(15, 129);
            this.lblToBezelAndHold2.Name = "lblToBezelAndHold2";
            this.lblToBezelAndHold2.Size = new System.Drawing.Size(83, 15);
            this.lblToBezelAndHold2.TabIndex = 60;
            this.lblToBezelAndHold2.Text = "Выдать в губы";
            this.lblToBezelAndHold2.Click += new System.EventHandler(this.lblToBezelAndHold2_Click);
            // 
            // lblFromBoxToReader2
            // 
            this.lblFromBoxToReader2.AutoSize = true;
            this.lblFromBoxToReader2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFromBoxToReader2.Location = new System.Drawing.Point(15, 90);
            this.lblFromBoxToReader2.Name = "lblFromBoxToReader2";
            this.lblFromBoxToReader2.Size = new System.Drawing.Size(101, 15);
            this.lblFromBoxToReader2.TabIndex = 59;
            this.lblFromBoxToReader2.Text = "Выдать под ридер";
            this.lblFromBoxToReader2.Click += new System.EventHandler(this.lblFromBoxToReader2_Click);
            // 
            // lblDispensGetStatus2
            // 
            this.lblDispensGetStatus2.AutoSize = true;
            this.lblDispensGetStatus2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDispensGetStatus2.Location = new System.Drawing.Point(15, 54);
            this.lblDispensGetStatus2.Name = "lblDispensGetStatus2";
            this.lblDispensGetStatus2.Size = new System.Drawing.Size(106, 15);
            this.lblDispensGetStatus2.TabIndex = 58;
            this.lblDispensGetStatus2.Text = "Статус диспенсера";
            this.lblDispensGetStatus2.Click += new System.EventHandler(this.lblDispensGetStatus2_Click);
            // 
            // lblStFeedOutSol
            // 
            this.lblStFeedOutSol.AutoSize = true;
            this.lblStFeedOutSol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStFeedOutSol.Location = new System.Drawing.Point(446, 189);
            this.lblStFeedOutSol.Name = "lblStFeedOutSol";
            this.lblStFeedOutSol.Size = new System.Drawing.Size(71, 15);
            this.lblStFeedOutSol.TabIndex = 57;
            this.lblStFeedOutSol.Text = "Feed Out Sol";
            this.lblStFeedOutSol.Visible = false;
            // 
            // lblStAutoWait
            // 
            this.lblStAutoWait.AutoSize = true;
            this.lblStAutoWait.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStAutoWait.Location = new System.Drawing.Point(446, 153);
            this.lblStAutoWait.Name = "lblStAutoWait";
            this.lblStAutoWait.Size = new System.Drawing.Size(56, 15);
            this.lblStAutoWait.TabIndex = 56;
            this.lblStAutoWait.Text = "Auto Wait";
            this.lblStAutoWait.Visible = false;
            // 
            // lblStStack2Wait
            // 
            this.lblStStack2Wait.AutoSize = true;
            this.lblStStack2Wait.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStStack2Wait.Location = new System.Drawing.Point(446, 119);
            this.lblStStack2Wait.Name = "lblStStack2Wait";
            this.lblStStack2Wait.Size = new System.Drawing.Size(75, 15);
            this.lblStStack2Wait.TabIndex = 55;
            this.lblStStack2Wait.Text = "Stack#2 Wait";
            this.lblStStack2Wait.Visible = false;
            // 
            // lblStStack1Wait
            // 
            this.lblStStack1Wait.AutoSize = true;
            this.lblStStack1Wait.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStStack1Wait.Location = new System.Drawing.Point(446, 87);
            this.lblStStack1Wait.Name = "lblStStack1Wait";
            this.lblStStack1Wait.Size = new System.Drawing.Size(75, 15);
            this.lblStStack1Wait.TabIndex = 54;
            this.lblStStack1Wait.Text = "Stack#1 Wait";
            this.lblStStack1Wait.Visible = false;
            // 
            // lblStStop
            // 
            this.lblStStop.AutoSize = true;
            this.lblStStop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStStop.Location = new System.Drawing.Point(446, 225);
            this.lblStStop.Name = "lblStStop";
            this.lblStStop.Size = new System.Drawing.Size(31, 15);
            this.lblStStop.TabIndex = 53;
            this.lblStStop.Text = "Stop";
            this.lblStStop.Visible = false;
            // 
            // lblStFeedOut
            // 
            this.lblStFeedOut.AutoSize = true;
            this.lblStFeedOut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStFeedOut.Location = new System.Drawing.Point(335, 330);
            this.lblStFeedOut.Name = "lblStFeedOut";
            this.lblStFeedOut.Size = new System.Drawing.Size(53, 15);
            this.lblStFeedOut.TabIndex = 52;
            this.lblStFeedOut.Text = "Feed Out";
            this.lblStFeedOut.Visible = false;
            // 
            // lblStFeedIn
            // 
            this.lblStFeedIn.AutoSize = true;
            this.lblStFeedIn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStFeedIn.Location = new System.Drawing.Point(334, 295);
            this.lblStFeedIn.Name = "lblStFeedIn";
            this.lblStFeedIn.Size = new System.Drawing.Size(45, 15);
            this.lblStFeedIn.TabIndex = 51;
            this.lblStFeedIn.Text = "Feed In";
            this.lblStFeedIn.Visible = false;
            // 
            // lblStCapture
            // 
            this.lblStCapture.AutoSize = true;
            this.lblStCapture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStCapture.Location = new System.Drawing.Point(334, 259);
            this.lblStCapture.Name = "lblStCapture";
            this.lblStCapture.Size = new System.Drawing.Size(46, 15);
            this.lblStCapture.TabIndex = 50;
            this.lblStCapture.Text = "Capture";
            this.lblStCapture.Visible = false;
            // 
            // lblStAutoIssue
            // 
            this.lblStAutoIssue.AutoSize = true;
            this.lblStAutoIssue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStAutoIssue.Location = new System.Drawing.Point(332, 225);
            this.lblStAutoIssue.Name = "lblStAutoIssue";
            this.lblStAutoIssue.Size = new System.Drawing.Size(59, 15);
            this.lblStAutoIssue.TabIndex = 49;
            this.lblStAutoIssue.Text = "Auto Issue";
            this.lblStAutoIssue.Visible = false;
            // 
            // lblStStacker2Out
            // 
            this.lblStStacker2Out.AutoSize = true;
            this.lblStStacker2Out.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStStacker2Out.Location = new System.Drawing.Point(332, 189);
            this.lblStStacker2Out.Name = "lblStStacker2Out";
            this.lblStStacker2Out.Size = new System.Drawing.Size(79, 15);
            this.lblStStacker2Out.TabIndex = 48;
            this.lblStStacker2Out.Text = "Stacker#2 Out";
            this.lblStStacker2Out.Visible = false;
            // 
            // lblDispenser2Status
            // 
            this.lblDispenser2Status.AutoSize = true;
            this.lblDispenser2Status.Location = new System.Drawing.Point(330, 66);
            this.lblDispenser2Status.Name = "lblDispenser2Status";
            this.lblDispenser2Status.Size = new System.Drawing.Size(41, 13);
            this.lblDispenser2Status.TabIndex = 47;
            this.lblDispenser2Status.Text = "Статус";
            // 
            // lblEtapDisp2
            // 
            this.lblEtapDisp2.AutoSize = true;
            this.lblEtapDisp2.Location = new System.Drawing.Point(330, 34);
            this.lblEtapDisp2.Name = "lblEtapDisp2";
            this.lblEtapDisp2.Size = new System.Drawing.Size(57, 13);
            this.lblEtapDisp2.TabIndex = 46;
            this.lblEtapDisp2.Text = "этап дисп";
            // 
            // lblStStacker1Out
            // 
            this.lblStStacker1Out.AutoSize = true;
            this.lblStStacker1Out.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStStacker1Out.Location = new System.Drawing.Point(332, 153);
            this.lblStStacker1Out.Name = "lblStStacker1Out";
            this.lblStStacker1Out.Size = new System.Drawing.Size(79, 15);
            this.lblStStacker1Out.TabIndex = 45;
            this.lblStStacker1Out.Text = "Stacker#1 Out";
            this.lblStStacker1Out.Visible = false;
            // 
            // lblStStatus
            // 
            this.lblStStatus.AutoSize = true;
            this.lblStStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStStatus.Location = new System.Drawing.Point(332, 119);
            this.lblStStatus.Name = "lblStStatus";
            this.lblStStatus.Size = new System.Drawing.Size(43, 15);
            this.lblStStatus.TabIndex = 44;
            this.lblStStatus.Text = "Статус";
            this.lblStStatus.Visible = false;
            // 
            // lblStClear
            // 
            this.lblStClear.AutoSize = true;
            this.lblStClear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStClear.Location = new System.Drawing.Point(476, 34);
            this.lblStClear.Name = "lblStClear";
            this.lblStClear.Size = new System.Drawing.Size(56, 15);
            this.lblStClear.TabIndex = 43;
            this.lblStClear.Text = "Очистить";
            this.lblStClear.Click += new System.EventHandler(this.lblStClear_Click);
            // 
            // lblComDispens2
            // 
            this.lblComDispens2.AutoSize = true;
            this.lblComDispens2.Location = new System.Drawing.Point(13, 25);
            this.lblComDispens2.Name = "lblComDispens2";
            this.lblComDispens2.Size = new System.Drawing.Size(28, 13);
            this.lblComDispens2.TabIndex = 42;
            this.lblComDispens2.Text = "Com";
            // 
            // lblRecycleToRead
            // 
            this.lblRecycleToRead.AutoSize = true;
            this.lblRecycleToRead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRecycleToRead.Location = new System.Drawing.Point(23, 262);
            this.lblRecycleToRead.Name = "lblRecycleToRead";
            this.lblRecycleToRead.Size = new System.Drawing.Size(94, 15);
            this.lblRecycleToRead.TabIndex = 25;
            this.lblRecycleToRead.Text = "Опять под ридер";
            this.lblRecycleToRead.Click += new System.EventHandler(this.labelRecycleToRead_Click);
            // 
            // labelRxDisp
            // 
            this.labelRxDisp.AutoSize = true;
            this.labelRxDisp.Location = new System.Drawing.Point(369, 294);
            this.labelRxDisp.Name = "labelRxDisp";
            this.labelRxDisp.Size = new System.Drawing.Size(41, 13);
            this.labelRxDisp.TabIndex = 24;
            this.labelRxDisp.Text = "label47";
            // 
            // labelVersionDispens
            // 
            this.labelVersionDispens.AutoSize = true;
            this.labelVersionDispens.Location = new System.Drawing.Point(134, 294);
            this.labelVersionDispens.Name = "labelVersionDispens";
            this.labelVersionDispens.Size = new System.Drawing.Size(42, 13);
            this.labelVersionDispens.TabIndex = 23;
            this.labelVersionDispens.Text = "Version";
            // 
            // lblGetVersion
            // 
            this.lblGetVersion.AutoSize = true;
            this.lblGetVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGetVersion.Location = new System.Drawing.Point(23, 294);
            this.lblGetVersion.Name = "lblGetVersion";
            this.lblGetVersion.Size = new System.Drawing.Size(61, 15);
            this.lblGetVersion.TabIndex = 22;
            this.lblGetVersion.Text = "GetVersion";
            this.lblGetVersion.Click += new System.EventHandler(this.labelGetVersion_Click);
            // 
            // lblFromBezel
            // 
            this.lblFromBezel.AutoSize = true;
            this.lblFromBezel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFromBezel.Location = new System.Drawing.Point(23, 229);
            this.lblFromBezel.Name = "lblFromBezel";
            this.lblFromBezel.Size = new System.Drawing.Size(80, 15);
            this.lblFromBezel.TabIndex = 21;
            this.lblFromBezel.Text = "Втянуть карту";
            this.lblFromBezel.Click += new System.EventHandler(this.labelFromBezel_Click);
            // 
            // labelDispensStatus
            // 
            this.labelDispensStatus.AutoSize = true;
            this.labelDispensStatus.Location = new System.Drawing.Point(337, 48);
            this.labelDispensStatus.Name = "labelDispensStatus";
            this.labelDispensStatus.Size = new System.Drawing.Size(37, 13);
            this.labelDispensStatus.TabIndex = 20;
            this.labelDispensStatus.Text = "Ответ";
            // 
            // labeletapDispensCard
            // 
            this.labeletapDispensCard.AutoSize = true;
            this.labeletapDispensCard.Location = new System.Drawing.Point(345, 28);
            this.labeletapDispensCard.Name = "labeletapDispensCard";
            this.labeletapDispensCard.Size = new System.Drawing.Size(57, 13);
            this.labeletapDispensCard.TabIndex = 19;
            this.labeletapDispensCard.Text = "этап дисп";
            // 
            // lblToRecycleBox
            // 
            this.lblToRecycleBox.AutoSize = true;
            this.lblToRecycleBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblToRecycleBox.Location = new System.Drawing.Point(23, 195);
            this.lblToRecycleBox.Name = "lblToRecycleBox";
            this.lblToRecycleBox.Size = new System.Drawing.Size(97, 15);
            this.lblToRecycleBox.TabIndex = 18;
            this.lblToRecycleBox.Text = "Карту в отказник";
            this.lblToRecycleBox.Click += new System.EventHandler(this.labelToRecycleBox_Click);
            // 
            // lblToBezelAndNotHold
            // 
            this.lblToBezelAndNotHold.AutoSize = true;
            this.lblToBezelAndNotHold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblToBezelAndNotHold.Location = new System.Drawing.Point(23, 158);
            this.lblToBezelAndNotHold.Name = "lblToBezelAndNotHold";
            this.lblToBezelAndNotHold.Size = new System.Drawing.Size(96, 15);
            this.lblToBezelAndNotHold.TabIndex = 17;
            this.lblToBezelAndNotHold.Text = "Выбросить карту";
            this.lblToBezelAndNotHold.Click += new System.EventHandler(this.labelToBezelAndNotHold_Click);
            // 
            // lblToBezelAndHold
            // 
            this.lblToBezelAndHold.AutoSize = true;
            this.lblToBezelAndHold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblToBezelAndHold.Location = new System.Drawing.Point(23, 123);
            this.lblToBezelAndHold.Name = "lblToBezelAndHold";
            this.lblToBezelAndHold.Size = new System.Drawing.Size(83, 15);
            this.lblToBezelAndHold.TabIndex = 16;
            this.lblToBezelAndHold.Text = "Выдать в губы";
            this.lblToBezelAndHold.Click += new System.EventHandler(this.labelToBezelAndHold_Click);
            // 
            // lblFromBoxToRead
            // 
            this.lblFromBoxToRead.AutoSize = true;
            this.lblFromBoxToRead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFromBoxToRead.Location = new System.Drawing.Point(23, 84);
            this.lblFromBoxToRead.Name = "lblFromBoxToRead";
            this.lblFromBoxToRead.Size = new System.Drawing.Size(101, 15);
            this.lblFromBoxToRead.TabIndex = 15;
            this.lblFromBoxToRead.Text = "Выдать под ридер";
            this.lblFromBoxToRead.Click += new System.EventHandler(this.labelFromBoxToRead_Click);
            // 
            // lblDispensGetStatus
            // 
            this.lblDispensGetStatus.AutoSize = true;
            this.lblDispensGetStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDispensGetStatus.Location = new System.Drawing.Point(23, 48);
            this.lblDispensGetStatus.Name = "lblDispensGetStatus";
            this.lblDispensGetStatus.Size = new System.Drawing.Size(106, 15);
            this.lblDispensGetStatus.TabIndex = 14;
            this.lblDispensGetStatus.Text = "Статус диспенсера";
            this.lblDispensGetStatus.Click += new System.EventHandler(this.labelDispensGetStatus_Click);
            // 
            // labelComDispens
            // 
            this.labelComDispens.AutoSize = true;
            this.labelComDispens.Location = new System.Drawing.Point(20, 19);
            this.labelComDispens.Name = "labelComDispens";
            this.labelComDispens.Size = new System.Drawing.Size(28, 13);
            this.labelComDispens.TabIndex = 13;
            this.labelComDispens.Text = "Com";
            // 
            // tabDiagBarcode
            // 
            this.tabDiagBarcode.Controls.Add(this.labelQRcode);
            this.tabDiagBarcode.Controls.Add(this.buttonStopDecoding);
            this.tabDiagBarcode.Controls.Add(this.buttonStartDecoding);
            this.tabDiagBarcode.Controls.Add(this.labelComBarcode);
            this.tabDiagBarcode.Location = new System.Drawing.Point(4, 22);
            this.tabDiagBarcode.Name = "tabDiagBarcode";
            this.tabDiagBarcode.Size = new System.Drawing.Size(772, 548);
            this.tabDiagBarcode.TabIndex = 4;
            this.tabDiagBarcode.Text = "Штрихкод";
            this.tabDiagBarcode.UseVisualStyleBackColor = true;
            // 
            // labelQRcode
            // 
            this.labelQRcode.AutoSize = true;
            this.labelQRcode.Location = new System.Drawing.Point(247, 79);
            this.labelQRcode.Name = "labelQRcode";
            this.labelQRcode.Size = new System.Drawing.Size(47, 13);
            this.labelQRcode.TabIndex = 3;
            this.labelQRcode.Text = "QRcode";
            // 
            // buttonStopDecoding
            // 
            this.buttonStopDecoding.Location = new System.Drawing.Point(42, 124);
            this.buttonStopDecoding.Name = "buttonStopDecoding";
            this.buttonStopDecoding.Size = new System.Drawing.Size(88, 23);
            this.buttonStopDecoding.TabIndex = 2;
            this.buttonStopDecoding.Text = "StopDecoding";
            this.buttonStopDecoding.UseVisualStyleBackColor = true;
            this.buttonStopDecoding.Click += new System.EventHandler(this.buttonStopDecoding_Click);
            // 
            // buttonStartDecoding
            // 
            this.buttonStartDecoding.Location = new System.Drawing.Point(42, 70);
            this.buttonStartDecoding.Name = "buttonStartDecoding";
            this.buttonStartDecoding.Size = new System.Drawing.Size(88, 23);
            this.buttonStartDecoding.TabIndex = 1;
            this.buttonStartDecoding.Text = "StartDecoding";
            this.buttonStartDecoding.UseVisualStyleBackColor = true;
            this.buttonStartDecoding.Click += new System.EventHandler(this.buttonStartDecoding_Click);
            // 
            // labelComBarcode
            // 
            this.labelComBarcode.AutoSize = true;
            this.labelComBarcode.Location = new System.Drawing.Point(39, 27);
            this.labelComBarcode.Name = "labelComBarcode";
            this.labelComBarcode.Size = new System.Drawing.Size(31, 13);
            this.labelComBarcode.TabIndex = 0;
            this.labelComBarcode.Text = "COM";
            // 
            // tabDiagSlave
            // 
            this.tabDiagSlave.Controls.Add(this.buttonReversOFF);
            this.tabDiagSlave.Controls.Add(this.buttonReversON);
            this.tabDiagSlave.Controls.Add(this.labelVersionSlave);
            this.tabDiagSlave.Controls.Add(this.label_Call);
            this.tabDiagSlave.Controls.Add(this.labelStatusOut2);
            this.tabDiagSlave.Controls.Add(this.label_lightRed);
            this.tabDiagSlave.Controls.Add(this.label_lightGrin);
            this.tabDiagSlave.Controls.Add(this.label_barrierDn);
            this.tabDiagSlave.Controls.Add(this.label_barrierUp);
            this.tabDiagSlave.Controls.Add(this.labelComSlave);
            this.tabDiagSlave.Location = new System.Drawing.Point(4, 22);
            this.tabDiagSlave.Name = "tabDiagSlave";
            this.tabDiagSlave.Size = new System.Drawing.Size(772, 548);
            this.tabDiagSlave.TabIndex = 5;
            this.tabDiagSlave.Text = "Slave";
            this.tabDiagSlave.UseVisualStyleBackColor = true;
            // 
            // buttonReversOFF
            // 
            this.buttonReversOFF.Location = new System.Drawing.Point(29, 295);
            this.buttonReversOFF.Name = "buttonReversOFF";
            this.buttonReversOFF.Size = new System.Drawing.Size(101, 23);
            this.buttonReversOFF.TabIndex = 9;
            this.buttonReversOFF.Text = "Revers out OFF";
            this.buttonReversOFF.UseVisualStyleBackColor = true;
            this.buttonReversOFF.Click += new System.EventHandler(this.buttonReversOFF_Click);
            // 
            // buttonReversON
            // 
            this.buttonReversON.Location = new System.Drawing.Point(29, 256);
            this.buttonReversON.Name = "buttonReversON";
            this.buttonReversON.Size = new System.Drawing.Size(101, 23);
            this.buttonReversON.TabIndex = 8;
            this.buttonReversON.Text = "Revers out ON";
            this.buttonReversON.UseVisualStyleBackColor = true;
            this.buttonReversON.Click += new System.EventHandler(this.buttonReversON_Click);
            // 
            // labelVersionSlave
            // 
            this.labelVersionSlave.AutoSize = true;
            this.labelVersionSlave.Location = new System.Drawing.Point(220, 21);
            this.labelVersionSlave.Name = "labelVersionSlave";
            this.labelVersionSlave.Size = new System.Drawing.Size(69, 13);
            this.labelVersionSlave.TabIndex = 7;
            this.labelVersionSlave.Text = "VersionSlave";
            // 
            // label_Call
            // 
            this.label_Call.AutoSize = true;
            this.label_Call.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_Call.Location = new System.Drawing.Point(29, 206);
            this.label_Call.Name = "label_Call";
            this.label_Call.Size = new System.Drawing.Size(88, 15);
            this.label_Call.TabIndex = 6;
            this.label_Call.Text = "Кнопка ВЫЗОВ";
            this.label_Call.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label_Call_MouseDown);
            this.label_Call.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label_Call_MouseUp);
            // 
            // labelStatusOut2
            // 
            this.labelStatusOut2.AutoSize = true;
            this.labelStatusOut2.Location = new System.Drawing.Point(217, 55);
            this.labelStatusOut2.Name = "labelStatusOut2";
            this.labelStatusOut2.Size = new System.Drawing.Size(158, 143);
            this.labelStatusOut2.TabIndex = 5;
            this.labelStatusOut2.Text = "Статус внешних устройств\r\nloopA свободна\r\nloopB свободна\r\nИК свободен\r\nкнопка НАЖ" +
    "МИТЕ не нажата\r\nкнопка ВЫЗОВ не нажата\r\nверхняя дверь закрыта\r\nнижняя дверь закр" +
    "ыта\r\nшлагбаум\r\nсветофор\r\nRevers in";
            // 
            // label_lightRed
            // 
            this.label_lightRed.AutoSize = true;
            this.label_lightRed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_lightRed.Location = new System.Drawing.Point(29, 154);
            this.label_lightRed.Name = "label_lightRed";
            this.label_lightRed.Size = new System.Drawing.Size(106, 15);
            this.label_lightRed.TabIndex = 4;
            this.label_lightRed.Text = "Светофор красный";
            this.label_lightRed.Click += new System.EventHandler(this.label_lightRed_Click);
            // 
            // label_lightGrin
            // 
            this.label_lightGrin.AutoSize = true;
            this.label_lightGrin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_lightGrin.Location = new System.Drawing.Point(29, 122);
            this.label_lightGrin.Name = "label_lightGrin";
            this.label_lightGrin.Size = new System.Drawing.Size(106, 15);
            this.label_lightGrin.TabIndex = 3;
            this.label_lightGrin.Text = "Светофор зеленый";
            this.label_lightGrin.Click += new System.EventHandler(this.label_lightGrin_Click);
            // 
            // label_barrierDn
            // 
            this.label_barrierDn.AutoSize = true;
            this.label_barrierDn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_barrierDn.Location = new System.Drawing.Point(29, 86);
            this.label_barrierDn.Name = "label_barrierDn";
            this.label_barrierDn.Size = new System.Drawing.Size(106, 15);
            this.label_barrierDn.TabIndex = 2;
            this.label_barrierDn.Text = "Закрыть шлагбаум";
            this.label_barrierDn.Click += new System.EventHandler(this.label_barrierDn_Click);
            // 
            // label_barrierUp
            // 
            this.label_barrierUp.AutoSize = true;
            this.label_barrierUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_barrierUp.Location = new System.Drawing.Point(29, 54);
            this.label_barrierUp.Name = "label_barrierUp";
            this.label_barrierUp.Size = new System.Drawing.Size(106, 15);
            this.label_barrierUp.TabIndex = 1;
            this.label_barrierUp.Text = "Открыть шлагбаум";
            this.label_barrierUp.Click += new System.EventHandler(this.label_barrierUp_Click);
            // 
            // labelComSlave
            // 
            this.labelComSlave.AutoSize = true;
            this.labelComSlave.Location = new System.Drawing.Point(26, 22);
            this.labelComSlave.Name = "labelComSlave";
            this.labelComSlave.Size = new System.Drawing.Size(31, 13);
            this.labelComSlave.TabIndex = 0;
            this.labelComSlave.Text = "COM";
            // 
            // tabDiagTablo
            // 
            this.tabDiagTablo.Controls.Add(this.grTablo3);
            this.tabDiagTablo.Controls.Add(this.grTablo2);
            this.tabDiagTablo.Controls.Add(this.grTablo1);
            this.tabDiagTablo.Location = new System.Drawing.Point(4, 22);
            this.tabDiagTablo.Name = "tabDiagTablo";
            this.tabDiagTablo.Size = new System.Drawing.Size(772, 548);
            this.tabDiagTablo.TabIndex = 7;
            this.tabDiagTablo.Text = "Табло";
            this.tabDiagTablo.UseVisualStyleBackColor = true;
            // 
            // grTablo3
            // 
            this.grTablo3.Controls.Add(this.label79);
            this.grTablo3.Controls.Add(this.udUDPTabloGreen);
            this.grTablo3.Controls.Add(this.udUDPTabloYellow);
            this.grTablo3.Controls.Add(this.label78);
            this.grTablo3.Controls.Add(this.udUDPTabloRed);
            this.grTablo3.Controls.Add(this.label77);
            this.grTablo3.Controls.Add(this.cmdSetTablo3Value);
            this.grTablo3.Controls.Add(this.txtTablo3);
            this.grTablo3.Controls.Add(this.label76);
            this.grTablo3.Controls.Add(this.udUDPTabloColor);
            this.grTablo3.Controls.Add(this.label74);
            this.grTablo3.Controls.Add(this.txtUDPTabloPort);
            this.grTablo3.Controls.Add(this.label73);
            this.grTablo3.Controls.Add(this.label72);
            this.grTablo3.Controls.Add(this.txtUDPTabloIP);
            this.grTablo3.Location = new System.Drawing.Point(419, 169);
            this.grTablo3.Name = "grTablo3";
            this.grTablo3.Size = new System.Drawing.Size(324, 233);
            this.grTablo3.TabIndex = 5;
            this.grTablo3.TabStop = false;
            this.grTablo3.Text = "Табло #3 (UDP китайское)";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(14, 194);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(70, 13);
            this.label79.TabIndex = 111;
            this.label79.Text = "Зеленый до:";
            // 
            // udUDPTabloGreen
            // 
            this.udUDPTabloGreen.Location = new System.Drawing.Point(88, 192);
            this.udUDPTabloGreen.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.udUDPTabloGreen.Name = "udUDPTabloGreen";
            this.udUDPTabloGreen.Size = new System.Drawing.Size(52, 20);
            this.udUDPTabloGreen.TabIndex = 110;
            this.udUDPTabloGreen.Value = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.udUDPTabloGreen.ValueChanged += new System.EventHandler(this.udUDPTabloGreen_ValueChanged);
            // 
            // udUDPTabloYellow
            // 
            this.udUDPTabloYellow.Location = new System.Drawing.Point(88, 159);
            this.udUDPTabloYellow.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.udUDPTabloYellow.Name = "udUDPTabloYellow";
            this.udUDPTabloYellow.Size = new System.Drawing.Size(52, 20);
            this.udUDPTabloYellow.TabIndex = 109;
            this.udUDPTabloYellow.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.udUDPTabloYellow.ValueChanged += new System.EventHandler(this.udUDPTabloYellow_ValueChanged);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(13, 161);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(67, 13);
            this.label78.TabIndex = 108;
            this.label78.Text = "Желтый до:";
            // 
            // udUDPTabloRed
            // 
            this.udUDPTabloRed.Location = new System.Drawing.Point(88, 126);
            this.udUDPTabloRed.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.udUDPTabloRed.Name = "udUDPTabloRed";
            this.udUDPTabloRed.Size = new System.Drawing.Size(52, 20);
            this.udUDPTabloRed.TabIndex = 107;
            this.udUDPTabloRed.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.udUDPTabloRed.ValueChanged += new System.EventHandler(this.udUDPTabloRed_ValueChanged);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(13, 128);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(70, 13);
            this.label77.TabIndex = 106;
            this.label77.Text = "Красный до:";
            // 
            // cmdSetTablo3Value
            // 
            this.cmdSetTablo3Value.Location = new System.Drawing.Point(166, 78);
            this.cmdSetTablo3Value.Name = "cmdSetTablo3Value";
            this.cmdSetTablo3Value.Size = new System.Drawing.Size(100, 26);
            this.cmdSetTablo3Value.TabIndex = 105;
            this.cmdSetTablo3Value.Text = "Задать";
            this.cmdSetTablo3Value.UseVisualStyleBackColor = true;
            this.cmdSetTablo3Value.Click += new System.EventHandler(this.cmdSetTablo3Value_Click);
            // 
            // txtTablo3
            // 
            this.txtTablo3.Location = new System.Drawing.Point(115, 83);
            this.txtTablo3.MaxLength = 3;
            this.txtTablo3.Name = "txtTablo3";
            this.txtTablo3.Size = new System.Drawing.Size(40, 20);
            this.txtTablo3.TabIndex = 104;
            this.txtTablo3.Text = "999";
            this.txtTablo3.TextChanged += new System.EventHandler(this.txtTablo3_TextChanged);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(13, 84);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(96, 13);
            this.label76.TabIndex = 103;
            this.label76.Text = "Задать значение:";
            // 
            // udUDPTabloColor
            // 
            this.udUDPTabloColor.Location = new System.Drawing.Point(259, 38);
            this.udUDPTabloColor.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.udUDPTabloColor.Name = "udUDPTabloColor";
            this.udUDPTabloColor.Size = new System.Drawing.Size(52, 20);
            this.udUDPTabloColor.TabIndex = 102;
            this.udUDPTabloColor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udUDPTabloColor.Visible = false;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(221, 39);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(32, 13);
            this.label74.TabIndex = 101;
            this.label74.Text = "Цвет";
            this.label74.Visible = false;
            // 
            // txtUDPTabloPort
            // 
            this.txtUDPTabloPort.Location = new System.Drawing.Point(169, 37);
            this.txtUDPTabloPort.Name = "txtUDPTabloPort";
            this.txtUDPTabloPort.Size = new System.Drawing.Size(47, 20);
            this.txtUDPTabloPort.TabIndex = 100;
            this.txtUDPTabloPort.TextChanged += new System.EventHandler(this.txtUDPTabloPort_TextChanged);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(137, 39);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(26, 13);
            this.label73.TabIndex = 99;
            this.label73.Text = "Port";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(13, 40);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(17, 13);
            this.label72.TabIndex = 98;
            this.label72.Text = "IP";
            // 
            // txtUDPTabloIP
            // 
            this.txtUDPTabloIP.Location = new System.Drawing.Point(31, 37);
            this.txtUDPTabloIP.Name = "txtUDPTabloIP";
            this.txtUDPTabloIP.Size = new System.Drawing.Size(100, 20);
            this.txtUDPTabloIP.TabIndex = 97;
            this.txtUDPTabloIP.TextChanged += new System.EventHandler(this.txtUDPTabloIP_TextChanged);
            // 
            // grTablo2
            // 
            this.grTablo2.Controls.Add(this.txtTablo2);
            this.grTablo2.Controls.Add(this.label75);
            this.grTablo2.Controls.Add(this.label28);
            this.grTablo2.Controls.Add(this.udTabloAddress);
            this.grTablo2.Controls.Add(this.label41);
            this.grTablo2.Controls.Add(this.cboDisplayType);
            this.grTablo2.Location = new System.Drawing.Point(419, 19);
            this.grTablo2.Name = "grTablo2";
            this.grTablo2.Size = new System.Drawing.Size(324, 139);
            this.grTablo2.TabIndex = 4;
            this.grTablo2.TabStop = false;
            this.grTablo2.Text = "Табло #2 (ИНФОТАБ 3/24-3)";
            // 
            // txtTablo2
            // 
            this.txtTablo2.Location = new System.Drawing.Point(94, 101);
            this.txtTablo2.MaxLength = 4;
            this.txtTablo2.Name = "txtTablo2";
            this.txtTablo2.Size = new System.Drawing.Size(40, 20);
            this.txtTablo2.TabIndex = 95;
            this.txtTablo2.Text = "1234";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(28, 104);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(55, 13);
            this.label75.TabIndex = 94;
            this.label75.Text = "Значение";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(101, 35);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(52, 13);
            this.label28.TabIndex = 93;
            this.label28.Text = "Разряды";
            // 
            // udTabloAddress
            // 
            this.udTabloAddress.Location = new System.Drawing.Point(26, 55);
            this.udTabloAddress.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udTabloAddress.Name = "udTabloAddress";
            this.udTabloAddress.Size = new System.Drawing.Size(52, 20);
            this.udTabloAddress.TabIndex = 92;
            this.udTabloAddress.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.udTabloAddress.ValueChanged += new System.EventHandler(this.udTabloAddress_ValueChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(25, 35);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(38, 13);
            this.label41.TabIndex = 91;
            this.label41.Text = "Адрес";
            // 
            // cboDisplayType
            // 
            this.cboDisplayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDisplayType.FormattingEnabled = true;
            this.cboDisplayType.Items.AddRange(new object[] {
            "0",
            "00",
            "000",
            "0000"});
            this.cboDisplayType.Location = new System.Drawing.Point(95, 53);
            this.cboDisplayType.Name = "cboDisplayType";
            this.cboDisplayType.Size = new System.Drawing.Size(105, 21);
            this.cboDisplayType.TabIndex = 90;
            this.cboDisplayType.SelectedIndexChanged += new System.EventHandler(this.cboDisplayType_SelectedIndexChanged);
            // 
            // grTablo1
            // 
            this.grTablo1.Controls.Add(this.txtTablo1);
            this.grTablo1.Controls.Add(this.label40);
            this.grTablo1.Controls.Add(this.labelComTablo);
            this.grTablo1.Location = new System.Drawing.Point(21, 19);
            this.grTablo1.Name = "grTablo1";
            this.grTablo1.Size = new System.Drawing.Size(324, 121);
            this.grTablo1.TabIndex = 3;
            this.grTablo1.TabStop = false;
            this.grTablo1.Text = "Табло #1 (RPS Tablo)";
            // 
            // txtTablo1
            // 
            this.txtTablo1.Location = new System.Drawing.Point(89, 91);
            this.txtTablo1.MaxLength = 4;
            this.txtTablo1.Name = "txtTablo1";
            this.txtTablo1.Size = new System.Drawing.Size(40, 20);
            this.txtTablo1.TabIndex = 5;
            this.txtTablo1.Text = "1234";
            this.txtTablo1.TextChanged += new System.EventHandler(this.txtTablo1_TextChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(23, 94);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(55, 13);
            this.label40.TabIndex = 4;
            this.label40.Text = "Значение";
            // 
            // labelComTablo
            // 
            this.labelComTablo.AutoSize = true;
            this.labelComTablo.Location = new System.Drawing.Point(20, 34);
            this.labelComTablo.Name = "labelComTablo";
            this.labelComTablo.Size = new System.Drawing.Size(34, 13);
            this.labelComTablo.TabIndex = 3;
            this.labelComTablo.Text = "COM ";
            // 
            // tabDiagWebServer
            // 
            this.tabDiagWebServer.Controls.Add(this.buttonViinexFolder);
            this.tabDiagWebServer.Controls.Add(this.CameraBox);
            this.tabDiagWebServer.Controls.Add(this.label50);
            this.tabDiagWebServer.Controls.Add(this.label49);
            this.tabDiagWebServer.Controls.Add(this.textBoxCameraIP);
            this.tabDiagWebServer.Controls.Add(this.labelLastPlate);
            this.tabDiagWebServer.Controls.Add(this.labelviinex);
            this.tabDiagWebServer.Controls.Add(this.label48);
            this.tabDiagWebServer.Controls.Add(this.label47);
            this.tabDiagWebServer.Controls.Add(this.textBoxViinexFile);
            this.tabDiagWebServer.Controls.Add(this.button1);
            this.tabDiagWebServer.Controls.Add(this.labelWebServer);
            this.tabDiagWebServer.Location = new System.Drawing.Point(4, 22);
            this.tabDiagWebServer.Name = "tabDiagWebServer";
            this.tabDiagWebServer.Size = new System.Drawing.Size(772, 548);
            this.tabDiagWebServer.TabIndex = 6;
            this.tabDiagWebServer.Text = "WebServer";
            this.tabDiagWebServer.UseVisualStyleBackColor = true;
            // 
            // buttonViinexFolder
            // 
            this.buttonViinexFolder.Location = new System.Drawing.Point(0, 0);
            this.buttonViinexFolder.Name = "buttonViinexFolder";
            this.buttonViinexFolder.Size = new System.Drawing.Size(75, 23);
            this.buttonViinexFolder.TabIndex = 0;
            // 
            // CameraBox
            // 
            this.CameraBox.AutoSize = true;
            this.CameraBox.Location = new System.Drawing.Point(475, 37);
            this.CameraBox.Name = "CameraBox";
            this.CameraBox.Size = new System.Drawing.Size(285, 210);
            this.CameraBox.TabIndex = 10;
            this.CameraBox.TabStop = false;
            this.CameraBox.Text = "groupBox1";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(7, 185);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(23, 13);
            this.label50.TabIndex = 9;
            this.label50.Text = "File";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(7, 213);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(53, 13);
            this.label49.TabIndex = 8;
            this.label49.Text = "CameraIP";
            // 
            // textBoxCameraIP
            // 
            this.textBoxCameraIP.Location = new System.Drawing.Point(66, 208);
            this.textBoxCameraIP.Name = "textBoxCameraIP";
            this.textBoxCameraIP.Size = new System.Drawing.Size(164, 20);
            this.textBoxCameraIP.TabIndex = 7;
            // 
            // labelLastPlate
            // 
            this.labelLastPlate.AutoSize = true;
            this.labelLastPlate.Location = new System.Drawing.Point(116, 240);
            this.labelLastPlate.Name = "labelLastPlate";
            this.labelLastPlate.Size = new System.Drawing.Size(39, 13);
            this.labelLastPlate.TabIndex = 6;
            this.labelLastPlate.Text = "номер";
            // 
            // labelviinex
            // 
            this.labelviinex.AutoSize = true;
            this.labelviinex.Location = new System.Drawing.Point(244, 213);
            this.labelviinex.Name = "labelviinex";
            this.labelviinex.Size = new System.Drawing.Size(37, 13);
            this.labelviinex.TabIndex = 5;
            this.labelviinex.Text = "viinex ";
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(3, 256);
            this.label48.Multiline = true;
            this.label48.Name = "label48";
            this.label48.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.label48.Size = new System.Drawing.Size(743, 228);
            this.label48.TabIndex = 4;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(3, 240);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(37, 13);
            this.label47.TabIndex = 3;
            this.label47.Text = "Ответ";
            // 
            // textBoxViinexFile
            // 
            this.textBoxViinexFile.Location = new System.Drawing.Point(36, 182);
            this.textBoxViinexFile.Name = "textBoxViinexFile";
            this.textBoxViinexFile.Size = new System.Drawing.Size(224, 20);
            this.textBoxViinexFile.TabIndex = 2;
            this.textBoxViinexFile.Text = "C:\\RPV\\1.jpg";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            // 
            // labelWebServer
            // 
            this.labelWebServer.AutoSize = true;
            this.labelWebServer.Location = new System.Drawing.Point(7, 5);
            this.labelWebServer.Name = "labelWebServer";
            this.labelWebServer.Size = new System.Drawing.Size(55, 13);
            this.labelWebServer.TabIndex = 0;
            this.labelWebServer.Text = "Запросы:\r\n";
            // 
            // tabDiagBankModule
            // 
            this.tabDiagBankModule.Controls.Add(this.listBox1);
            this.tabDiagBankModule.Controls.Add(this.buttonVerification);
            this.tabDiagBankModule.Controls.Add(this.buttonEchoTest);
            this.tabDiagBankModule.Controls.Add(this.buttonWorkKey);
            this.tabDiagBankModule.Controls.Add(this.buttonTerminalInfo);
            this.tabDiagBankModule.Controls.Add(this.buttonLoadSW);
            this.tabDiagBankModule.Controls.Add(this.buttonLoadParams);
            this.tabDiagBankModule.Controls.Add(this.buttonBankModuleCancell);
            this.tabDiagBankModule.Controls.Add(this.buttonBankModuleClose);
            this.tabDiagBankModule.Controls.Add(this.buttonBankModulePay);
            this.tabDiagBankModule.Controls.Add(this.buttonBankModuleOpen);
            this.tabDiagBankModule.Controls.Add(this.labelBankModuleStatus);
            this.tabDiagBankModule.Location = new System.Drawing.Point(4, 22);
            this.tabDiagBankModule.Name = "tabDiagBankModule";
            this.tabDiagBankModule.Size = new System.Drawing.Size(772, 548);
            this.tabDiagBankModule.TabIndex = 8;
            this.tabDiagBankModule.Text = "BankModule";
            this.tabDiagBankModule.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "123",
            "456"});
            this.listBox1.Location = new System.Drawing.Point(503, 56);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(241, 303);
            this.listBox1.TabIndex = 11;
            // 
            // buttonVerification
            // 
            this.buttonVerification.Location = new System.Drawing.Point(295, 332);
            this.buttonVerification.Name = "buttonVerification";
            this.buttonVerification.Size = new System.Drawing.Size(127, 23);
            this.buttonVerification.TabIndex = 10;
            this.buttonVerification.Text = "Сверка итогов";
            this.buttonVerification.UseVisualStyleBackColor = true;
            this.buttonVerification.Click += new System.EventHandler(this.buttonVerification_Click);
            // 
            // buttonEchoTest
            // 
            this.buttonEchoTest.Location = new System.Drawing.Point(295, 276);
            this.buttonEchoTest.Name = "buttonEchoTest";
            this.buttonEchoTest.Size = new System.Drawing.Size(127, 23);
            this.buttonEchoTest.TabIndex = 9;
            this.buttonEchoTest.Text = "Эхо-тест";
            this.buttonEchoTest.UseVisualStyleBackColor = true;
            this.buttonEchoTest.Click += new System.EventHandler(this.buttonEchoTest_Click);
            // 
            // buttonWorkKey
            // 
            this.buttonWorkKey.Location = new System.Drawing.Point(295, 219);
            this.buttonWorkKey.Name = "buttonWorkKey";
            this.buttonWorkKey.Size = new System.Drawing.Size(127, 36);
            this.buttonWorkKey.TabIndex = 8;
            this.buttonWorkKey.Text = "Загрузка рабочего ключа";
            this.buttonWorkKey.UseVisualStyleBackColor = true;
            this.buttonWorkKey.Click += new System.EventHandler(this.buttonWorkKey_Click);
            // 
            // buttonTerminalInfo
            // 
            this.buttonTerminalInfo.Location = new System.Drawing.Point(295, 162);
            this.buttonTerminalInfo.Name = "buttonTerminalInfo";
            this.buttonTerminalInfo.Size = new System.Drawing.Size(127, 23);
            this.buttonTerminalInfo.TabIndex = 7;
            this.buttonTerminalInfo.Text = "Инфо терминала";
            this.buttonTerminalInfo.UseVisualStyleBackColor = true;
            this.buttonTerminalInfo.Click += new System.EventHandler(this.buttonTerminalInfo_Click);
            // 
            // buttonLoadSW
            // 
            this.buttonLoadSW.Location = new System.Drawing.Point(295, 108);
            this.buttonLoadSW.Name = "buttonLoadSW";
            this.buttonLoadSW.Size = new System.Drawing.Size(127, 23);
            this.buttonLoadSW.TabIndex = 6;
            this.buttonLoadSW.Text = "Загрузка ПО";
            this.buttonLoadSW.UseVisualStyleBackColor = true;
            this.buttonLoadSW.Click += new System.EventHandler(this.buttonLoadSW_Click);
            // 
            // buttonLoadParams
            // 
            this.buttonLoadParams.Location = new System.Drawing.Point(295, 56);
            this.buttonLoadParams.Name = "buttonLoadParams";
            this.buttonLoadParams.Size = new System.Drawing.Size(127, 23);
            this.buttonLoadParams.TabIndex = 5;
            this.buttonLoadParams.Text = "Загрузка параметров";
            this.buttonLoadParams.UseVisualStyleBackColor = true;
            this.buttonLoadParams.Click += new System.EventHandler(this.buttonLoadParams_Click);
            // 
            // buttonBankModuleCancell
            // 
            this.buttonBankModuleCancell.Location = new System.Drawing.Point(33, 220);
            this.buttonBankModuleCancell.Name = "buttonBankModuleCancell";
            this.buttonBankModuleCancell.Size = new System.Drawing.Size(75, 23);
            this.buttonBankModuleCancell.TabIndex = 4;
            this.buttonBankModuleCancell.Text = "Cancell";
            this.buttonBankModuleCancell.UseVisualStyleBackColor = true;
            this.buttonBankModuleCancell.Click += new System.EventHandler(this.buttonBankModuleCancell_Click);
            // 
            // buttonBankModuleClose
            // 
            this.buttonBankModuleClose.Location = new System.Drawing.Point(33, 163);
            this.buttonBankModuleClose.Name = "buttonBankModuleClose";
            this.buttonBankModuleClose.Size = new System.Drawing.Size(75, 23);
            this.buttonBankModuleClose.TabIndex = 3;
            this.buttonBankModuleClose.Text = "Close";
            this.buttonBankModuleClose.UseVisualStyleBackColor = true;
            this.buttonBankModuleClose.Click += new System.EventHandler(this.buttonBankModuleClose_Click);
            // 
            // buttonBankModulePay
            // 
            this.buttonBankModulePay.Location = new System.Drawing.Point(33, 109);
            this.buttonBankModulePay.Name = "buttonBankModulePay";
            this.buttonBankModulePay.Size = new System.Drawing.Size(75, 23);
            this.buttonBankModulePay.TabIndex = 2;
            this.buttonBankModulePay.Text = "Pay 1";
            this.buttonBankModulePay.UseVisualStyleBackColor = true;
            this.buttonBankModulePay.Click += new System.EventHandler(this.buttonBankModulePay_Click);
            // 
            // buttonBankModuleOpen
            // 
            this.buttonBankModuleOpen.Location = new System.Drawing.Point(33, 57);
            this.buttonBankModuleOpen.Name = "buttonBankModuleOpen";
            this.buttonBankModuleOpen.Size = new System.Drawing.Size(75, 23);
            this.buttonBankModuleOpen.TabIndex = 1;
            this.buttonBankModuleOpen.Text = "Open";
            this.buttonBankModuleOpen.UseVisualStyleBackColor = true;
            this.buttonBankModuleOpen.Click += new System.EventHandler(this.buttonBankModuleOpen_Click);
            // 
            // labelBankModuleStatus
            // 
            this.labelBankModuleStatus.AutoSize = true;
            this.labelBankModuleStatus.Location = new System.Drawing.Point(40, 17);
            this.labelBankModuleStatus.Name = "labelBankModuleStatus";
            this.labelBankModuleStatus.Size = new System.Drawing.Size(100, 13);
            this.labelBankModuleStatus.TabIndex = 0;
            this.labelBankModuleStatus.Text = "BankModule Status";
            // 
            // tabDiagRFID
            // 
            this.tabDiagRFID.Controls.Add(this.labelWiegandResult);
            this.tabDiagRFID.Controls.Add(this.labelWiegandStat);
            this.tabDiagRFID.Controls.Add(this.labelComWiegand);
            this.tabDiagRFID.Location = new System.Drawing.Point(4, 22);
            this.tabDiagRFID.Name = "tabDiagRFID";
            this.tabDiagRFID.Padding = new System.Windows.Forms.Padding(3);
            this.tabDiagRFID.Size = new System.Drawing.Size(772, 548);
            this.tabDiagRFID.TabIndex = 9;
            this.tabDiagRFID.Text = "RFID";
            this.tabDiagRFID.UseVisualStyleBackColor = true;
            // 
            // labelWiegandResult
            // 
            this.labelWiegandResult.AutoSize = true;
            this.labelWiegandResult.Location = new System.Drawing.Point(50, 110);
            this.labelWiegandResult.Name = "labelWiegandResult";
            this.labelWiegandResult.Size = new System.Drawing.Size(16, 13);
            this.labelWiegandResult.TabIndex = 6;
            this.labelWiegandResult.Text = "---";
            // 
            // labelWiegandStat
            // 
            this.labelWiegandStat.AutoSize = true;
            this.labelWiegandStat.Location = new System.Drawing.Point(50, 75);
            this.labelWiegandStat.Name = "labelWiegandStat";
            this.labelWiegandStat.Size = new System.Drawing.Size(16, 13);
            this.labelWiegandStat.TabIndex = 5;
            this.labelWiegandStat.Text = "---";
            // 
            // labelComWiegand
            // 
            this.labelComWiegand.AutoSize = true;
            this.labelComWiegand.Location = new System.Drawing.Point(50, 40);
            this.labelComWiegand.Name = "labelComWiegand";
            this.labelComWiegand.Size = new System.Drawing.Size(75, 13);
            this.labelComWiegand.TabIndex = 4;
            this.labelComWiegand.Text = "Unknown Port";
            // 
            // tabDiagCollector
            // 
            this.tabDiagCollector.Controls.Add(this.lblCollectorTimeoutOff);
            this.tabDiagCollector.Controls.Add(this.lblCollectorBack);
            this.tabDiagCollector.Controls.Add(this.lblVersionCollector);
            this.tabDiagCollector.Controls.Add(this.lblGetVersionCollector);
            this.tabDiagCollector.Controls.Add(this.lblCollectorFront);
            this.tabDiagCollector.Controls.Add(this.lblCollectorStatus);
            this.tabDiagCollector.Controls.Add(this.lblEtapCollector);
            this.tabDiagCollector.Controls.Add(this.lblCollectorRetain);
            this.tabDiagCollector.Controls.Add(this.lblCollectorEject);
            this.tabDiagCollector.Controls.Add(this.lblStatusCollector);
            this.tabDiagCollector.Controls.Add(this.lblComCollector);
            this.tabDiagCollector.Location = new System.Drawing.Point(4, 22);
            this.tabDiagCollector.Name = "tabDiagCollector";
            this.tabDiagCollector.Size = new System.Drawing.Size(772, 548);
            this.tabDiagCollector.TabIndex = 10;
            this.tabDiagCollector.Text = "Коллектор";
            this.tabDiagCollector.UseVisualStyleBackColor = true;
            // 
            // lblCollectorTimeoutOff
            // 
            this.lblCollectorTimeoutOff.AutoSize = true;
            this.lblCollectorTimeoutOff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCollectorTimeoutOff.Location = new System.Drawing.Point(21, 322);
            this.lblCollectorTimeoutOff.Name = "lblCollectorTimeoutOff";
            this.lblCollectorTimeoutOff.Size = new System.Drawing.Size(64, 15);
            this.lblCollectorTimeoutOff.TabIndex = 26;
            this.lblCollectorTimeoutOff.Text = "Timeout Off";
            this.lblCollectorTimeoutOff.Click += new System.EventHandler(this.lblCollectorTimeoutOff_Click);
            // 
            // lblCollectorBack
            // 
            this.lblCollectorBack.AutoSize = true;
            this.lblCollectorBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCollectorBack.Location = new System.Drawing.Point(21, 178);
            this.lblCollectorBack.Name = "lblCollectorBack";
            this.lblCollectorBack.Size = new System.Drawing.Size(77, 15);
            this.lblCollectorBack.TabIndex = 25;
            this.lblCollectorBack.Text = "Захват сзади";
            // 
            // lblVersionCollector
            // 
            this.lblVersionCollector.AutoSize = true;
            this.lblVersionCollector.Location = new System.Drawing.Point(132, 286);
            this.lblVersionCollector.Name = "lblVersionCollector";
            this.lblVersionCollector.Size = new System.Drawing.Size(42, 13);
            this.lblVersionCollector.TabIndex = 23;
            this.lblVersionCollector.Text = "Version";
            // 
            // lblGetVersionCollector
            // 
            this.lblGetVersionCollector.AutoSize = true;
            this.lblGetVersionCollector.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGetVersionCollector.Location = new System.Drawing.Point(21, 286);
            this.lblGetVersionCollector.Name = "lblGetVersionCollector";
            this.lblGetVersionCollector.Size = new System.Drawing.Size(61, 15);
            this.lblGetVersionCollector.TabIndex = 22;
            this.lblGetVersionCollector.Text = "GetVersion";
            this.lblGetVersionCollector.Click += new System.EventHandler(this.lblGetVersionCollector_Click);
            // 
            // lblCollectorFront
            // 
            this.lblCollectorFront.AutoSize = true;
            this.lblCollectorFront.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCollectorFront.Location = new System.Drawing.Point(21, 142);
            this.lblCollectorFront.Name = "lblCollectorFront";
            this.lblCollectorFront.Size = new System.Drawing.Size(89, 15);
            this.lblCollectorFront.TabIndex = 21;
            this.lblCollectorFront.Text = "Захват спереди";
            this.lblCollectorFront.Click += new System.EventHandler(this.lblCollectorFront_Click);
            // 
            // lblCollectorStatus
            // 
            this.lblCollectorStatus.AutoSize = true;
            this.lblCollectorStatus.Location = new System.Drawing.Point(335, 40);
            this.lblCollectorStatus.Name = "lblCollectorStatus";
            this.lblCollectorStatus.Size = new System.Drawing.Size(37, 13);
            this.lblCollectorStatus.TabIndex = 20;
            this.lblCollectorStatus.Text = "Ответ";
            // 
            // lblEtapCollector
            // 
            this.lblEtapCollector.AutoSize = true;
            this.lblEtapCollector.Location = new System.Drawing.Point(343, 20);
            this.lblEtapCollector.Name = "lblEtapCollector";
            this.lblEtapCollector.Size = new System.Drawing.Size(57, 13);
            this.lblEtapCollector.TabIndex = 19;
            this.lblEtapCollector.Text = "этап дисп";
            // 
            // lblCollectorRetain
            // 
            this.lblCollectorRetain.AutoSize = true;
            this.lblCollectorRetain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCollectorRetain.Location = new System.Drawing.Point(21, 106);
            this.lblCollectorRetain.Name = "lblCollectorRetain";
            this.lblCollectorRetain.Size = new System.Drawing.Size(97, 15);
            this.lblCollectorRetain.TabIndex = 18;
            this.lblCollectorRetain.Text = "Карту в отказник";
            this.lblCollectorRetain.Click += new System.EventHandler(this.lblCollectorRetain_Click);
            // 
            // lblCollectorEject
            // 
            this.lblCollectorEject.AutoSize = true;
            this.lblCollectorEject.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCollectorEject.Location = new System.Drawing.Point(21, 72);
            this.lblCollectorEject.Name = "lblCollectorEject";
            this.lblCollectorEject.Size = new System.Drawing.Size(83, 15);
            this.lblCollectorEject.TabIndex = 16;
            this.lblCollectorEject.Text = "Выдать в губы";
            this.lblCollectorEject.Click += new System.EventHandler(this.lblCollectorEject_Click);
            // 
            // lblStatusCollector
            // 
            this.lblStatusCollector.AutoSize = true;
            this.lblStatusCollector.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStatusCollector.Location = new System.Drawing.Point(21, 40);
            this.lblStatusCollector.Name = "lblStatusCollector";
            this.lblStatusCollector.Size = new System.Drawing.Size(105, 15);
            this.lblStatusCollector.TabIndex = 14;
            this.lblStatusCollector.Text = "Статус коллектора";
            this.lblStatusCollector.Click += new System.EventHandler(this.lblStatusCollector_Click);
            // 
            // lblComCollector
            // 
            this.lblComCollector.AutoSize = true;
            this.lblComCollector.Location = new System.Drawing.Point(18, 11);
            this.lblComCollector.Name = "lblComCollector";
            this.lblComCollector.Size = new System.Drawing.Size(28, 13);
            this.lblComCollector.TabIndex = 13;
            this.lblComCollector.Text = "Com";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(782, 539);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "НАСТРОЙКИ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabMainSettings);
            this.tabControl2.Controls.Add(this.tabPage15);
            this.tabControl2.Location = new System.Drawing.Point(1, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(783, 542);
            this.tabControl2.TabIndex = 39;
            // 
            // tabMainSettings
            // 
            this.tabMainSettings.Controls.Add(this.label71);
            this.tabMainSettings.Controls.Add(this.udKKMNumber);
            this.tabMainSettings.Controls.Add(this.cboTabloNew);
            this.tabMainSettings.Controls.Add(this.cboReadOutTypeId);
            this.tabMainSettings.Controls.Add(this.cboDispenserTypeId);
            this.tabMainSettings.Controls.Add(this.cboRackMode);
            this.tabMainSettings.Controls.Add(this.udComCollector);
            this.tabMainSettings.Controls.Add(this.chkCollector);
            this.tabMainSettings.Controls.Add(this.udComRFID);
            this.tabMainSettings.Controls.Add(this.chkRFID);
            this.tabMainSettings.Controls.Add(this.label1);
            this.tabMainSettings.Controls.Add(this.udZoneBefore);
            this.tabMainSettings.Controls.Add(this.udZoneAfter);
            this.tabMainSettings.Controls.Add(this.chkBankModule);
            this.tabMainSettings.Controls.Add(this.chkSlave);
            this.tabMainSettings.Controls.Add(this.cboPortDiscret3);
            this.tabMainSettings.Controls.Add(this.chkReaderOut);
            this.tabMainSettings.Controls.Add(this.label39);
            this.tabMainSettings.Controls.Add(this.chkReaderIn);
            this.tabMainSettings.Controls.Add(this.chkDispenser);
            this.tabMainSettings.Controls.Add(this.label31);
            this.tabMainSettings.Controls.Add(this.chkBarcode);
            this.tabMainSettings.Controls.Add(this.udComDisplay);
            this.tabMainSettings.Controls.Add(this.chkDisplay);
            this.tabMainSettings.Controls.Add(this.cboZoneAfter);
            this.tabMainSettings.Controls.Add(this.udComSlave);
            this.tabMainSettings.Controls.Add(this.cboZoneBefore);
            this.tabMainSettings.Controls.Add(this.udComReadOut);
            this.tabMainSettings.Controls.Add(this.cboPortDiscret2);
            this.tabMainSettings.Controls.Add(this.udComReadIn);
            this.tabMainSettings.Controls.Add(this.cboPortDiscret1);
            this.tabMainSettings.Controls.Add(this.udComDispenser);
            this.tabMainSettings.Controls.Add(this.label16);
            this.tabMainSettings.Controls.Add(this.udComBarcode);
            this.tabMainSettings.Controls.Add(this.label15);
            this.tabMainSettings.Controls.Add(this.label14);
            this.tabMainSettings.Controls.Add(this.label3);
            this.tabMainSettings.Controls.Add(this.label11);
            this.tabMainSettings.Controls.Add(this.cboRackType);
            this.tabMainSettings.Controls.Add(this.label8);
            this.tabMainSettings.Controls.Add(this.label6);
            this.tabMainSettings.Controls.Add(this.label7);
            this.tabMainSettings.Location = new System.Drawing.Point(4, 22);
            this.tabMainSettings.Name = "tabMainSettings";
            this.tabMainSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabMainSettings.Size = new System.Drawing.Size(775, 516);
            this.tabMainSettings.TabIndex = 0;
            this.tabMainSettings.Text = "Основные";
            this.tabMainSettings.UseVisualStyleBackColor = true;
            this.tabMainSettings.Click += new System.EventHandler(this.tabMainSettings_Click);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(411, 303);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(44, 13);
            this.label71.TabIndex = 90;
            this.label71.Text = "ККМ №";
            // 
            // udKKMNumber
            // 
            this.udKKMNumber.Location = new System.Drawing.Point(459, 300);
            this.udKKMNumber.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.udKKMNumber.Name = "udKKMNumber";
            this.udKKMNumber.Size = new System.Drawing.Size(84, 20);
            this.udKKMNumber.TabIndex = 88;
            this.udKKMNumber.ValueChanged += new System.EventHandler(this.udKKMNumber_ValueChanged);
            // 
            // cboTabloNew
            // 
            this.cboTabloNew.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTabloNew.FormattingEnabled = true;
            this.cboTabloNew.Items.AddRange(new object[] {
            "0",
            "00",
            "000",
            "0000"});
            this.cboTabloNew.Location = new System.Drawing.Point(442, 184);
            this.cboTabloNew.Name = "cboTabloNew";
            this.cboTabloNew.Size = new System.Drawing.Size(121, 21);
            this.cboTabloNew.TabIndex = 86;
            this.cboTabloNew.DropDown += new System.EventHandler(this.cboTabloNew_DropDown);
            this.cboTabloNew.SelectedIndexChanged += new System.EventHandler(this.cboTabloNew_SelectedIndexChanged);
            this.cboTabloNew.DropDownClosed += new System.EventHandler(this.cboTabloNew_DropDownClosed);
            // 
            // cboReadOutTypeId
            // 
            this.cboReadOutTypeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReadOutTypeId.FormattingEnabled = true;
            this.cboReadOutTypeId.Items.AddRange(new object[] {
            "0",
            "00",
            "000",
            "0000"});
            this.cboReadOutTypeId.Location = new System.Drawing.Point(473, 65);
            this.cboReadOutTypeId.Name = "cboReadOutTypeId";
            this.cboReadOutTypeId.Size = new System.Drawing.Size(121, 21);
            this.cboReadOutTypeId.TabIndex = 85;
            this.cboReadOutTypeId.DropDown += new System.EventHandler(this.cboReadOutTypeId_DropDown);
            this.cboReadOutTypeId.SelectedIndexChanged += new System.EventHandler(this.cboReadOutTypeId_SelectedIndexChanged);
            this.cboReadOutTypeId.DropDownClosed += new System.EventHandler(this.cboReadOutTypeId_DropDownClosed);
            // 
            // cboDispenserTypeId
            // 
            this.cboDispenserTypeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDispenserTypeId.FormattingEnabled = true;
            this.cboDispenserTypeId.Items.AddRange(new object[] {
            "0",
            "00",
            "000",
            "0000"});
            this.cboDispenserTypeId.Location = new System.Drawing.Point(473, 123);
            this.cboDispenserTypeId.Name = "cboDispenserTypeId";
            this.cboDispenserTypeId.Size = new System.Drawing.Size(121, 21);
            this.cboDispenserTypeId.TabIndex = 84;
            this.cboDispenserTypeId.DropDown += new System.EventHandler(this.cboDispenserTypeId_DropDown);
            this.cboDispenserTypeId.SelectedIndexChanged += new System.EventHandler(this.cboDispenserTypeId_SelectedIndexChanged);
            this.cboDispenserTypeId.DropDownClosed += new System.EventHandler(this.cboDispenserTypeId_DropDownClosed);
            // 
            // cboRackMode
            // 
            this.cboRackMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRackMode.FormattingEnabled = true;
            this.cboRackMode.Location = new System.Drawing.Point(12, 32);
            this.cboRackMode.Name = "cboRackMode";
            this.cboRackMode.Size = new System.Drawing.Size(146, 21);
            this.cboRackMode.TabIndex = 81;
            this.cboRackMode.DropDown += new System.EventHandler(this.cboRackMode_DropDown);
            this.cboRackMode.SelectedIndexChanged += new System.EventHandler(this.cboRackMode_SelectedIndexChanged);
            this.cboRackMode.DropDownClosed += new System.EventHandler(this.cboRackMode_DropDownClosed);
            // 
            // udComCollector
            // 
            this.udComCollector.Location = new System.Drawing.Point(396, 270);
            this.udComCollector.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udComCollector.Name = "udComCollector";
            this.udComCollector.Size = new System.Drawing.Size(40, 20);
            this.udComCollector.TabIndex = 80;
            this.udComCollector.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.udComCollector.ValueChanged += new System.EventHandler(this.udComCollector_ValueChanged);
            // 
            // chkCollector
            // 
            this.chkCollector.AutoSize = true;
            this.chkCollector.Location = new System.Drawing.Point(272, 272);
            this.chkCollector.Name = "chkCollector";
            this.chkCollector.Size = new System.Drawing.Size(80, 17);
            this.chkCollector.TabIndex = 79;
            this.chkCollector.Text = "Коллектор";
            this.chkCollector.UseVisualStyleBackColor = true;
            this.chkCollector.CheckedChanged += new System.EventHandler(this.chkCollector_CheckedChanged);
            // 
            // udComRFID
            // 
            this.udComRFID.Location = new System.Drawing.Point(396, 243);
            this.udComRFID.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udComRFID.Name = "udComRFID";
            this.udComRFID.Size = new System.Drawing.Size(40, 20);
            this.udComRFID.TabIndex = 78;
            this.udComRFID.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.udComRFID.ValueChanged += new System.EventHandler(this.udComRFID_ValueChanged);
            // 
            // chkRFID
            // 
            this.chkRFID.AutoSize = true;
            this.chkRFID.Location = new System.Drawing.Point(272, 245);
            this.chkRFID.Name = "chkRFID";
            this.chkRFID.Size = new System.Drawing.Size(51, 17);
            this.chkRFID.TabIndex = 77;
            this.chkRFID.Text = "RFID";
            this.chkRFID.UseVisualStyleBackColor = true;
            this.chkRFID.CheckedChanged += new System.EventHandler(this.chkRFID_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Режим стойки";
            // 
            // udZoneBefore
            // 
            this.udZoneBefore.Location = new System.Drawing.Point(12, 180);
            this.udZoneBefore.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udZoneBefore.Name = "udZoneBefore";
            this.udZoneBefore.Size = new System.Drawing.Size(40, 20);
            this.udZoneBefore.TabIndex = 4;
            this.udZoneBefore.ValueChanged += new System.EventHandler(this.udZoneBefore_ValueChanged);
            // 
            // udZoneAfter
            // 
            this.udZoneAfter.Location = new System.Drawing.Point(12, 230);
            this.udZoneAfter.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udZoneAfter.Name = "udZoneAfter";
            this.udZoneAfter.Size = new System.Drawing.Size(40, 20);
            this.udZoneAfter.TabIndex = 5;
            this.udZoneAfter.ValueChanged += new System.EventHandler(this.udZoneAfter_ValueChanged);
            // 
            // chkBankModule
            // 
            this.chkBankModule.AutoSize = true;
            this.chkBankModule.Location = new System.Drawing.Point(272, 302);
            this.chkBankModule.Name = "chkBankModule";
            this.chkBankModule.Size = new System.Drawing.Size(86, 17);
            this.chkBankModule.TabIndex = 35;
            this.chkBankModule.Text = "BankModule";
            this.chkBankModule.UseVisualStyleBackColor = true;
            this.chkBankModule.CheckedChanged += new System.EventHandler(this.chkBankModule_CheckedChanged);
            // 
            // chkSlave
            // 
            this.chkSlave.AutoSize = true;
            this.chkSlave.Checked = true;
            this.chkSlave.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSlave.Location = new System.Drawing.Point(272, 35);
            this.chkSlave.Name = "chkSlave";
            this.chkSlave.Size = new System.Drawing.Size(115, 17);
            this.chkSlave.TabIndex = 10;
            this.chkSlave.Text = "Slave контроллер";
            this.chkSlave.UseVisualStyleBackColor = true;
            this.chkSlave.CheckedChanged += new System.EventHandler(this.chkSlave_CheckedChanged);
            // 
            // cboPortDiscret3
            // 
            this.cboPortDiscret3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPortDiscret3.FormattingEnabled = true;
            this.cboPortDiscret3.Items.AddRange(new object[] {
            " ",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "H",
            "J",
            "K"});
            this.cboPortDiscret3.Location = new System.Drawing.Point(395, 419);
            this.cboPortDiscret3.Name = "cboPortDiscret3";
            this.cboPortDiscret3.Size = new System.Drawing.Size(40, 21);
            this.cboPortDiscret3.TabIndex = 34;
            this.cboPortDiscret3.DropDown += new System.EventHandler(this.cboPortDiscret3_DropDown);
            this.cboPortDiscret3.SelectedIndexChanged += new System.EventHandler(this.cboPortDiscret3_SelectedIndexChanged);
            this.cboPortDiscret3.DropDownClosed += new System.EventHandler(this.cboPortDiscret3_DropDownClosed);
            // 
            // chkReaderOut
            // 
            this.chkReaderOut.AutoSize = true;
            this.chkReaderOut.Checked = true;
            this.chkReaderOut.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkReaderOut.Location = new System.Drawing.Point(272, 65);
            this.chkReaderOut.Name = "chkReaderOut";
            this.chkReaderOut.Size = new System.Drawing.Size(104, 17);
            this.chkReaderOut.TabIndex = 12;
            this.chkReaderOut.Text = "Ридер внешний";
            this.chkReaderOut.UseVisualStyleBackColor = true;
            this.chkReaderOut.CheckedChanged += new System.EventHandler(this.chkReaderOut_CheckedChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(274, 422);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(115, 13);
            this.label39.TabIndex = 33;
            this.label39.Text = "Высотомеры, реверс";
            // 
            // chkReaderIn
            // 
            this.chkReaderIn.AutoSize = true;
            this.chkReaderIn.Location = new System.Drawing.Point(272, 95);
            this.chkReaderIn.Name = "chkReaderIn";
            this.chkReaderIn.Size = new System.Drawing.Size(118, 17);
            this.chkReaderIn.TabIndex = 13;
            this.chkReaderIn.Text = "Ридер внутренний";
            this.chkReaderIn.UseVisualStyleBackColor = true;
            this.chkReaderIn.CheckedChanged += new System.EventHandler(this.chkReaderIn_CheckedChanged);
            // 
            // chkDispenser
            // 
            this.chkDispenser.AutoSize = true;
            this.chkDispenser.Location = new System.Drawing.Point(272, 125);
            this.chkDispenser.Name = "chkDispenser";
            this.chkDispenser.Size = new System.Drawing.Size(83, 17);
            this.chkDispenser.TabIndex = 14;
            this.chkDispenser.Text = "Диспенсер";
            this.chkDispenser.UseVisualStyleBackColor = true;
            this.chkDispenser.CheckedChanged += new System.EventHandler(this.chkDispenser_CheckedChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(470, 15);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(26, 13);
            this.label31.TabIndex = 31;
            this.label31.Text = "Тип";
            // 
            // chkBarcode
            // 
            this.chkBarcode.AutoSize = true;
            this.chkBarcode.Location = new System.Drawing.Point(272, 155);
            this.chkBarcode.Name = "chkBarcode";
            this.chkBarcode.Size = new System.Drawing.Size(75, 17);
            this.chkBarcode.TabIndex = 15;
            this.chkBarcode.Text = "Штрихкод";
            this.chkBarcode.UseVisualStyleBackColor = true;
            this.chkBarcode.CheckedChanged += new System.EventHandler(this.chkBarcode_CheckedChanged);
            // 
            // udComDisplay
            // 
            this.udComDisplay.Location = new System.Drawing.Point(395, 185);
            this.udComDisplay.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udComDisplay.Name = "udComDisplay";
            this.udComDisplay.Size = new System.Drawing.Size(40, 20);
            this.udComDisplay.TabIndex = 30;
            this.udComDisplay.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.udComDisplay.ValueChanged += new System.EventHandler(this.udComDisplay_ValueChanged);
            // 
            // chkDisplay
            // 
            this.chkDisplay.AutoSize = true;
            this.chkDisplay.Location = new System.Drawing.Point(272, 185);
            this.chkDisplay.Name = "chkDisplay";
            this.chkDisplay.Size = new System.Drawing.Size(57, 17);
            this.chkDisplay.TabIndex = 16;
            this.chkDisplay.Text = "Табло";
            this.chkDisplay.UseVisualStyleBackColor = true;
            this.chkDisplay.CheckedChanged += new System.EventHandler(this.chkDisplay_CheckedChanged);
            // 
            // cboZoneAfter
            // 
            this.cboZoneAfter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboZoneAfter.FormattingEnabled = true;
            this.cboZoneAfter.Location = new System.Drawing.Point(58, 230);
            this.cboZoneAfter.Name = "cboZoneAfter";
            this.cboZoneAfter.Size = new System.Drawing.Size(121, 21);
            this.cboZoneAfter.TabIndex = 29;
            this.cboZoneAfter.DropDown += new System.EventHandler(this.cboZoneAfter_DropDown);
            this.cboZoneAfter.SelectedIndexChanged += new System.EventHandler(this.cboZoneAfter_SelectedIndexChanged);
            this.cboZoneAfter.DropDownClosed += new System.EventHandler(this.cboZoneAfter_DropDownClosed);
            // 
            // udComSlave
            // 
            this.udComSlave.Location = new System.Drawing.Point(395, 35);
            this.udComSlave.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udComSlave.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udComSlave.Name = "udComSlave";
            this.udComSlave.Size = new System.Drawing.Size(40, 20);
            this.udComSlave.TabIndex = 18;
            this.udComSlave.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udComSlave.ValueChanged += new System.EventHandler(this.udComSlave_ValueChanged);
            // 
            // cboZoneBefore
            // 
            this.cboZoneBefore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboZoneBefore.FormattingEnabled = true;
            this.cboZoneBefore.Location = new System.Drawing.Point(58, 180);
            this.cboZoneBefore.Name = "cboZoneBefore";
            this.cboZoneBefore.Size = new System.Drawing.Size(121, 21);
            this.cboZoneBefore.TabIndex = 28;
            this.cboZoneBefore.DropDown += new System.EventHandler(this.cboZoneBefore_DropDown);
            this.cboZoneBefore.SelectedIndexChanged += new System.EventHandler(this.cboZoneBefore_SelectedIndexChanged);
            this.cboZoneBefore.DropDownClosed += new System.EventHandler(this.cboZoneBefore_DropDownClosed);
            // 
            // udComReadOut
            // 
            this.udComReadOut.Location = new System.Drawing.Point(395, 65);
            this.udComReadOut.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udComReadOut.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udComReadOut.Name = "udComReadOut";
            this.udComReadOut.Size = new System.Drawing.Size(40, 20);
            this.udComReadOut.TabIndex = 19;
            this.udComReadOut.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.udComReadOut.ValueChanged += new System.EventHandler(this.udComReadOut_ValueChanged);
            // 
            // cboPortDiscret2
            // 
            this.cboPortDiscret2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPortDiscret2.FormattingEnabled = true;
            this.cboPortDiscret2.Items.AddRange(new object[] {
            " ",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "H",
            "J",
            "K"});
            this.cboPortDiscret2.Location = new System.Drawing.Point(395, 384);
            this.cboPortDiscret2.Name = "cboPortDiscret2";
            this.cboPortDiscret2.Size = new System.Drawing.Size(40, 21);
            this.cboPortDiscret2.TabIndex = 27;
            this.cboPortDiscret2.DropDown += new System.EventHandler(this.cboPortDiscret2_DropDown);
            this.cboPortDiscret2.SelectedIndexChanged += new System.EventHandler(this.cboPortDiscret2_SelectedIndexChanged);
            this.cboPortDiscret2.DropDownClosed += new System.EventHandler(this.cboPortDiscret2_DropDownClosed);
            // 
            // udComReadIn
            // 
            this.udComReadIn.Location = new System.Drawing.Point(395, 95);
            this.udComReadIn.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udComReadIn.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udComReadIn.Name = "udComReadIn";
            this.udComReadIn.Size = new System.Drawing.Size(40, 20);
            this.udComReadIn.TabIndex = 20;
            this.udComReadIn.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.udComReadIn.ValueChanged += new System.EventHandler(this.udComReadIn_ValueChanged);
            // 
            // cboPortDiscret1
            // 
            this.cboPortDiscret1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPortDiscret1.FormattingEnabled = true;
            this.cboPortDiscret1.Items.AddRange(new object[] {
            " ",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "H",
            "J",
            "K"});
            this.cboPortDiscret1.Location = new System.Drawing.Point(395, 349);
            this.cboPortDiscret1.Name = "cboPortDiscret1";
            this.cboPortDiscret1.Size = new System.Drawing.Size(40, 21);
            this.cboPortDiscret1.TabIndex = 26;
            this.cboPortDiscret1.DropDown += new System.EventHandler(this.cboPortDiscret1_DropDown);
            this.cboPortDiscret1.SelectedIndexChanged += new System.EventHandler(this.cboPortDiscret1_SelectedIndexChanged);
            this.cboPortDiscret1.DropDownClosed += new System.EventHandler(this.cboPortDiscret1_DropDownClosed);
            // 
            // udComDispenser
            // 
            this.udComDispenser.Location = new System.Drawing.Point(395, 125);
            this.udComDispenser.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udComDispenser.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udComDispenser.Name = "udComDispenser";
            this.udComDispenser.Size = new System.Drawing.Size(40, 20);
            this.udComDispenser.TabIndex = 21;
            this.udComDispenser.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.udComDispenser.ValueChanged += new System.EventHandler(this.udComDispenser_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(292, 387);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "Вызов, двери";
            // 
            // udComBarcode
            // 
            this.udComBarcode.Location = new System.Drawing.Point(395, 155);
            this.udComBarcode.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udComBarcode.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udComBarcode.Name = "udComBarcode";
            this.udComBarcode.Size = new System.Drawing.Size(40, 20);
            this.udComBarcode.TabIndex = 22;
            this.udComBarcode.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.udComBarcode.ValueChanged += new System.EventHandler(this.udComBarcode_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(292, 355);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "loop, шлагбаум";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(295, 329);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(134, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Порты Slave дискретные";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Тип стойки";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(392, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "COM порт";
            // 
            // cboRackType
            // 
            this.cboRackType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRackType.FormattingEnabled = true;
            this.cboRackType.Location = new System.Drawing.Point(12, 82);
            this.cboRackType.Name = "cboRackType";
            this.cboRackType.Size = new System.Drawing.Size(146, 21);
            this.cboRackType.TabIndex = 2;
            this.cboRackType.DropDown += new System.EventHandler(this.cboRackType_DropDown);
            this.cboRackType.SelectedIndexChanged += new System.EventHandler(this.cboRackType_SelectedIndexChanged);
            this.cboRackType.DropDownClosed += new System.EventHandler(this.cboRackType_DropDownClosed);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(272, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Оборудование";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 165);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Зона до";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 215);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Зона после";
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.txtTimeBudget);
            this.tabPage15.Controls.Add(this.label89);
            this.tabPage15.Controls.Add(this.chkGetSnapshots);
            this.tabPage15.Controls.Add(this.grTwoLevelRackRecognition);
            this.tabPage15.Controls.Add(this.tabControl1);
            this.tabPage15.Controls.Add(this.grSavePhotoLocal);
            this.tabPage15.Controls.Add(this.grEnableLoopANotification);
            this.tabPage15.Controls.Add(this.grViinexMain);
            this.tabPage15.Controls.Add(this.cmdUpdateViinexSettings);
            this.tabPage15.Controls.Add(this.label66);
            this.tabPage15.Controls.Add(this.txtViinexLicenseKey);
            this.tabPage15.Controls.Add(this.pnlVision);
            this.tabPage15.Controls.Add(this.label51);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage15.Size = new System.Drawing.Size(775, 516);
            this.tabPage15.TabIndex = 1;
            this.tabPage15.Text = "Распознавание";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // txtTimeBudget
            // 
            this.txtTimeBudget.Location = new System.Drawing.Point(285, 337);
            this.txtTimeBudget.Name = "txtTimeBudget";
            this.txtTimeBudget.Size = new System.Drawing.Size(44, 20);
            this.txtTimeBudget.TabIndex = 152;
            this.txtTimeBudget.Text = "30";
            this.txtTimeBudget.TextChanged += new System.EventHandler(this.txtTimeBudget_TextChanged);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(191, 340);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(88, 13);
            this.label89.TabIndex = 151;
            this.label89.Text = "Таймбюджет, с:";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkGetSnapshots
            // 
            this.chkGetSnapshots.AutoSize = true;
            this.chkGetSnapshots.Location = new System.Drawing.Point(12, 340);
            this.chkGetSnapshots.Name = "chkGetSnapshots";
            this.chkGetSnapshots.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkGetSnapshots.Size = new System.Drawing.Size(114, 17);
            this.chkGetSnapshots.TabIndex = 147;
            this.chkGetSnapshots.Text = "Сохранять кадры";
            this.chkGetSnapshots.UseVisualStyleBackColor = true;
            this.chkGetSnapshots.CheckedChanged += new System.EventHandler(this.chkGetSnapshots_CheckedChanged);
            // 
            // grTwoLevelRackRecognition
            // 
            this.grTwoLevelRackRecognition.Controls.Add(this.label88);
            this.grTwoLevelRackRecognition.Controls.Add(this.txtTwoLevelSendIP);
            this.grTwoLevelRackRecognition.Controls.Add(this.optTwoLevelSend);
            this.grTwoLevelRackRecognition.Controls.Add(this.optTwoLevelReceive);
            this.grTwoLevelRackRecognition.Controls.Add(this.optTwoLevelDisabled);
            this.grTwoLevelRackRecognition.Location = new System.Drawing.Point(355, 399);
            this.grTwoLevelRackRecognition.Name = "grTwoLevelRackRecognition";
            this.grTwoLevelRackRecognition.Size = new System.Drawing.Size(407, 100);
            this.grTwoLevelRackRecognition.TabIndex = 149;
            this.grTwoLevelRackRecognition.TabStop = false;
            this.grTwoLevelRackRecognition.Text = "Для двухуровневой стойки";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(206, 69);
            this.label88.Margin = new System.Windows.Forms.Padding(0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(35, 13);
            this.label88.TabIndex = 146;
            this.label88.Text = "на IP:";
            this.label88.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtTwoLevelSendIP
            // 
            this.txtTwoLevelSendIP.Location = new System.Drawing.Point(248, 66);
            this.txtTwoLevelSendIP.Name = "txtTwoLevelSendIP";
            this.txtTwoLevelSendIP.Size = new System.Drawing.Size(145, 20);
            this.txtTwoLevelSendIP.TabIndex = 145;
            this.txtTwoLevelSendIP.Text = "192.168.1.1";
            this.txtTwoLevelSendIP.TextChanged += new System.EventHandler(this.txtTwoLevelSendIP_TextChanged);
            // 
            // optTwoLevelSend
            // 
            this.optTwoLevelSend.AutoSize = true;
            this.optTwoLevelSend.Location = new System.Drawing.Point(262, 41);
            this.optTwoLevelSend.Name = "optTwoLevelSend";
            this.optTwoLevelSend.Size = new System.Drawing.Size(120, 17);
            this.optTwoLevelSend.TabIndex = 89;
            this.optTwoLevelSend.Text = "Отправлять номер";
            this.optTwoLevelSend.UseVisualStyleBackColor = true;
            this.optTwoLevelSend.CheckedChanged += new System.EventHandler(this.optTwoLevelSend_CheckedChanged);
            // 
            // optTwoLevelReceive
            // 
            this.optTwoLevelReceive.AutoSize = true;
            this.optTwoLevelReceive.Location = new System.Drawing.Point(98, 41);
            this.optTwoLevelReceive.Name = "optTwoLevelReceive";
            this.optTwoLevelReceive.Size = new System.Drawing.Size(158, 17);
            this.optTwoLevelReceive.TabIndex = 88;
            this.optTwoLevelReceive.Text = "Получать номер (слушать)";
            this.optTwoLevelReceive.UseVisualStyleBackColor = true;
            this.optTwoLevelReceive.CheckedChanged += new System.EventHandler(this.optTwoLevelReceive_CheckedChanged);
            // 
            // optTwoLevelDisabled
            // 
            this.optTwoLevelDisabled.AutoSize = true;
            this.optTwoLevelDisabled.Checked = true;
            this.optTwoLevelDisabled.Location = new System.Drawing.Point(11, 41);
            this.optTwoLevelDisabled.Name = "optTwoLevelDisabled";
            this.optTwoLevelDisabled.Size = new System.Drawing.Size(81, 17);
            this.optTwoLevelDisabled.TabIndex = 87;
            this.optTwoLevelDisabled.TabStop = true;
            this.optTwoLevelDisabled.Text = "Отключено";
            this.optTwoLevelDisabled.UseVisualStyleBackColor = true;
            this.optTwoLevelDisabled.CheckedChanged += new System.EventHandler(this.optTwoLevelDisabled_CheckedChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(351, 66);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(415, 303);
            this.tabControl1.TabIndex = 148;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(407, 277);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Камера 1";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtViinexGetURI);
            this.groupBox1.Controls.Add(this.label59);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.grCameraType);
            this.groupBox1.Controls.Add(this.txtViinexConfidence);
            this.groupBox1.Controls.Add(this.grTrans);
            this.groupBox1.Controls.Add(this.label67);
            this.groupBox1.Controls.Add(this.txtCameraPostProcess);
            this.groupBox1.Controls.Add(this.label65);
            this.groupBox1.Controls.Add(this.txtCameraPreProcess);
            this.groupBox1.Controls.Add(this.label62);
            this.groupBox1.Controls.Add(this.txtCameraSkip);
            this.groupBox1.Controls.Add(this.label55);
            this.groupBox1.Controls.Add(this.txtCameraPassword);
            this.groupBox1.Controls.Add(this.label63);
            this.groupBox1.Controls.Add(this.txtCameraLogin);
            this.groupBox1.Controls.Add(this.label64);
            this.groupBox1.Controls.Add(this.txtCameraURI);
            this.groupBox1.Controls.Add(this.label60);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(415, 263);
            this.groupBox1.TabIndex = 145;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Настройки камеры #1";
            // 
            // txtViinexGetURI
            // 
            this.txtViinexGetURI.Location = new System.Drawing.Point(118, 23);
            this.txtViinexGetURI.Name = "txtViinexGetURI";
            this.txtViinexGetURI.Size = new System.Drawing.Size(281, 20);
            this.txtViinexGetURI.TabIndex = 162;
            this.txtViinexGetURI.TextChanged += new System.EventHandler(this.txtViinexGetURI_TextChanged);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(11, 26);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(97, 13);
            this.label59.TabIndex = 161;
            this.label59.Text = "URL Get запроса:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkAcquireEvents);
            this.groupBox4.Controls.Add(this.chkAcquireVideo);
            this.groupBox4.Location = new System.Drawing.Point(181, 81);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(218, 55);
            this.groupBox4.TabIndex = 160;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Запрет событий (для ONVIF камер)";
            // 
            // chkAcquireEvents
            // 
            this.chkAcquireEvents.AutoSize = true;
            this.chkAcquireEvents.Location = new System.Drawing.Point(103, 22);
            this.chkAcquireEvents.Name = "chkAcquireEvents";
            this.chkAcquireEvents.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkAcquireEvents.Size = new System.Drawing.Size(59, 17);
            this.chkAcquireEvents.TabIndex = 141;
            this.chkAcquireEvents.Text = "Events";
            this.chkAcquireEvents.UseVisualStyleBackColor = true;
            this.chkAcquireEvents.CheckedChanged += new System.EventHandler(this.chkAcquireEvents_CheckedChanged);
            // 
            // chkAcquireVideo
            // 
            this.chkAcquireVideo.AutoSize = true;
            this.chkAcquireVideo.Location = new System.Drawing.Point(27, 22);
            this.chkAcquireVideo.Name = "chkAcquireVideo";
            this.chkAcquireVideo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkAcquireVideo.Size = new System.Drawing.Size(53, 17);
            this.chkAcquireVideo.TabIndex = 140;
            this.chkAcquireVideo.Text = "Video";
            this.chkAcquireVideo.UseVisualStyleBackColor = true;
            this.chkAcquireVideo.CheckedChanged += new System.EventHandler(this.chkAcquireVideo_CheckedChanged);
            // 
            // grCameraType
            // 
            this.grCameraType.Controls.Add(this.optCameraOnvif);
            this.grCameraType.Controls.Add(this.optCameraRTSP);
            this.grCameraType.Location = new System.Drawing.Point(29, 80);
            this.grCameraType.Name = "grCameraType";
            this.grCameraType.Size = new System.Drawing.Size(146, 55);
            this.grCameraType.TabIndex = 159;
            this.grCameraType.TabStop = false;
            this.grCameraType.Text = "Тип камеры";
            // 
            // optCameraOnvif
            // 
            this.optCameraOnvif.AutoSize = true;
            this.optCameraOnvif.Checked = true;
            this.optCameraOnvif.Location = new System.Drawing.Point(15, 21);
            this.optCameraOnvif.Name = "optCameraOnvif";
            this.optCameraOnvif.Size = new System.Drawing.Size(50, 17);
            this.optCameraOnvif.TabIndex = 3;
            this.optCameraOnvif.TabStop = true;
            this.optCameraOnvif.Text = "Onvif";
            this.optCameraOnvif.UseVisualStyleBackColor = true;
            this.optCameraOnvif.CheckedChanged += new System.EventHandler(this.optCameraOnvif_CheckedChanged);
            // 
            // optCameraRTSP
            // 
            this.optCameraRTSP.AutoSize = true;
            this.optCameraRTSP.Location = new System.Drawing.Point(87, 21);
            this.optCameraRTSP.Name = "optCameraRTSP";
            this.optCameraRTSP.Size = new System.Drawing.Size(54, 17);
            this.optCameraRTSP.TabIndex = 2;
            this.optCameraRTSP.Text = "RTSP";
            this.optCameraRTSP.UseVisualStyleBackColor = true;
            this.optCameraRTSP.CheckedChanged += new System.EventHandler(this.optCameraRTSP_CheckedChanged);
            // 
            // txtViinexConfidence
            // 
            this.txtViinexConfidence.Location = new System.Drawing.Point(268, 210);
            this.txtViinexConfidence.Name = "txtViinexConfidence";
            this.txtViinexConfidence.Size = new System.Drawing.Size(35, 20);
            this.txtViinexConfidence.TabIndex = 158;
            this.txtViinexConfidence.Text = "2";
            this.txtViinexConfidence.TextChanged += new System.EventHandler(this.txtViinexConfidence_TextChanged);
            // 
            // grTrans
            // 
            this.grTrans.Controls.Add(this.optLat);
            this.grTrans.Controls.Add(this.optCyr);
            this.grTrans.Location = new System.Drawing.Point(234, 141);
            this.grTrans.Name = "grTrans";
            this.grTrans.Size = new System.Drawing.Size(163, 55);
            this.grTrans.TabIndex = 157;
            this.grTrans.TabStop = false;
            this.grTrans.Text = "Транслитерация";
            // 
            // optLat
            // 
            this.optLat.AutoSize = true;
            this.optLat.Location = new System.Drawing.Point(15, 22);
            this.optLat.Name = "optLat";
            this.optLat.Size = new System.Drawing.Size(56, 17);
            this.optLat.TabIndex = 158;
            this.optLat.Text = "Латин";
            this.optLat.UseVisualStyleBackColor = true;
            this.optLat.CheckedChanged += new System.EventHandler(this.optLat_CheckedChanged);
            // 
            // optCyr
            // 
            this.optCyr.AutoSize = true;
            this.optCyr.Checked = true;
            this.optCyr.Location = new System.Drawing.Point(84, 22);
            this.optCyr.Name = "optCyr";
            this.optCyr.Size = new System.Drawing.Size(73, 17);
            this.optCyr.TabIndex = 157;
            this.optCyr.TabStop = true;
            this.optCyr.Text = "Кириллич";
            this.optCyr.UseVisualStyleBackColor = true;
            this.optCyr.CheckedChanged += new System.EventHandler(this.optCyr_CheckedChanged);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(199, 213);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(61, 13);
            this.label67.TabIndex = 155;
            this.label67.Text = "Confidence";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraPostProcess
            // 
            this.txtCameraPostProcess.Location = new System.Drawing.Point(267, 237);
            this.txtCameraPostProcess.Name = "txtCameraPostProcess";
            this.txtCameraPostProcess.Size = new System.Drawing.Size(47, 20);
            this.txtCameraPostProcess.TabIndex = 154;
            this.txtCameraPostProcess.Text = "1500";
            this.txtCameraPostProcess.TextChanged += new System.EventHandler(this.txtCameraPostProcess_TextChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(175, 240);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(86, 13);
            this.label65.TabIndex = 153;
            this.label65.Text = "PostProcess, мс";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraPreProcess
            // 
            this.txtCameraPreProcess.Location = new System.Drawing.Point(118, 237);
            this.txtCameraPreProcess.Name = "txtCameraPreProcess";
            this.txtCameraPreProcess.Size = new System.Drawing.Size(44, 20);
            this.txtCameraPreProcess.TabIndex = 152;
            this.txtCameraPreProcess.Text = "1500";
            this.txtCameraPreProcess.TextChanged += new System.EventHandler(this.txtCameraPreProcess_TextChanged);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(35, 240);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(80, 13);
            this.label62.TabIndex = 151;
            this.label62.Text = "Preprocess, мс";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraSkip
            // 
            this.txtCameraSkip.Location = new System.Drawing.Point(118, 210);
            this.txtCameraSkip.Name = "txtCameraSkip";
            this.txtCameraSkip.Size = new System.Drawing.Size(44, 20);
            this.txtCameraSkip.TabIndex = 150;
            this.txtCameraSkip.Text = "2000";
            this.txtCameraSkip.TextChanged += new System.EventHandler(this.txtCameraSkip_TextChanged);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(42, 213);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(70, 13);
            this.label55.TabIndex = 149;
            this.label55.Text = "Skip,кадров:";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraPassword
            // 
            this.txtCameraPassword.Location = new System.Drawing.Point(118, 176);
            this.txtCameraPassword.Name = "txtCameraPassword";
            this.txtCameraPassword.Size = new System.Drawing.Size(100, 20);
            this.txtCameraPassword.TabIndex = 148;
            this.txtCameraPassword.Text = "root";
            this.txtCameraPassword.UseSystemPasswordChar = true;
            this.txtCameraPassword.TextChanged += new System.EventHandler(this.txtCameraPassword_TextChanged);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(45, 179);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(53, 13);
            this.label63.TabIndex = 147;
            this.label63.Text = "Password";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraLogin
            // 
            this.txtCameraLogin.Location = new System.Drawing.Point(118, 142);
            this.txtCameraLogin.Name = "txtCameraLogin";
            this.txtCameraLogin.Size = new System.Drawing.Size(100, 20);
            this.txtCameraLogin.TabIndex = 146;
            this.txtCameraLogin.Text = "root";
            this.txtCameraLogin.TextChanged += new System.EventHandler(this.txtCameraLogin_TextChanged);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(65, 145);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(33, 13);
            this.label64.TabIndex = 145;
            this.label64.Text = "Login";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraURI
            // 
            this.txtCameraURI.Location = new System.Drawing.Point(118, 50);
            this.txtCameraURI.Name = "txtCameraURI";
            this.txtCameraURI.Size = new System.Drawing.Size(281, 20);
            this.txtCameraURI.TabIndex = 144;
            this.txtCameraURI.TextChanged += new System.EventHandler(this.txtCameraURI_TextChanged);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(15, 57);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(75, 13);
            this.label60.TabIndex = 143;
            this.label60.Text = "URL камеры:";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(407, 277);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Камера 2";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chkCamera2Enabled);
            this.groupBox5.Controls.Add(this.label80);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.txtViinexConfidence2);
            this.groupBox5.Controls.Add(this.groupBox8);
            this.groupBox5.Controls.Add(this.label81);
            this.groupBox5.Controls.Add(this.txtCameraPostProcess2);
            this.groupBox5.Controls.Add(this.label82);
            this.groupBox5.Controls.Add(this.txtCameraPreProcess2);
            this.groupBox5.Controls.Add(this.label83);
            this.groupBox5.Controls.Add(this.txtCameraSkip2);
            this.groupBox5.Controls.Add(this.label84);
            this.groupBox5.Controls.Add(this.txtCameraPassword2);
            this.groupBox5.Controls.Add(this.label85);
            this.groupBox5.Controls.Add(this.txtCameraLogin2);
            this.groupBox5.Controls.Add(this.label86);
            this.groupBox5.Controls.Add(this.txtCameraURI2);
            this.groupBox5.Controls.Add(this.label87);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(415, 263);
            this.groupBox5.TabIndex = 146;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Настройки камеры #2";
            // 
            // chkCamera2Enabled
            // 
            this.chkCamera2Enabled.AutoSize = true;
            this.chkCamera2Enabled.Location = new System.Drawing.Point(211, -1);
            this.chkCamera2Enabled.Name = "chkCamera2Enabled";
            this.chkCamera2Enabled.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkCamera2Enabled.Size = new System.Drawing.Size(178, 17);
            this.chkCamera2Enabled.TabIndex = 165;
            this.chkCamera2Enabled.Text = "Использовать вторую камеру";
            this.chkCamera2Enabled.UseVisualStyleBackColor = true;
            this.chkCamera2Enabled.CheckedChanged += new System.EventHandler(this.chkCamera2Enabled_CheckedChanged);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(11, 26);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(97, 13);
            this.label80.TabIndex = 161;
            this.label80.Text = "URL Get запроса:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.chkAcquireEvents2);
            this.groupBox6.Controls.Add(this.chkAcquireVideo2);
            this.groupBox6.Location = new System.Drawing.Point(181, 81);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(218, 55);
            this.groupBox6.TabIndex = 160;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Запрет событий (для ONVIF камер)";
            // 
            // chkAcquireEvents2
            // 
            this.chkAcquireEvents2.AutoSize = true;
            this.chkAcquireEvents2.Location = new System.Drawing.Point(103, 22);
            this.chkAcquireEvents2.Name = "chkAcquireEvents2";
            this.chkAcquireEvents2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkAcquireEvents2.Size = new System.Drawing.Size(59, 17);
            this.chkAcquireEvents2.TabIndex = 141;
            this.chkAcquireEvents2.Text = "Events";
            this.chkAcquireEvents2.UseVisualStyleBackColor = true;
            this.chkAcquireEvents2.CheckedChanged += new System.EventHandler(this.chkAcquireEvents2_CheckedChanged);
            // 
            // chkAcquireVideo2
            // 
            this.chkAcquireVideo2.AutoSize = true;
            this.chkAcquireVideo2.Location = new System.Drawing.Point(27, 22);
            this.chkAcquireVideo2.Name = "chkAcquireVideo2";
            this.chkAcquireVideo2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkAcquireVideo2.Size = new System.Drawing.Size(53, 17);
            this.chkAcquireVideo2.TabIndex = 140;
            this.chkAcquireVideo2.Text = "Video";
            this.chkAcquireVideo2.UseVisualStyleBackColor = true;
            this.chkAcquireVideo2.CheckedChanged += new System.EventHandler(this.chkAcquireVideo2_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.optCameraOnvif2);
            this.groupBox7.Controls.Add(this.optCameraRTSP2);
            this.groupBox7.Location = new System.Drawing.Point(29, 80);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(146, 55);
            this.groupBox7.TabIndex = 159;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Тип камеры";
            // 
            // optCameraOnvif2
            // 
            this.optCameraOnvif2.AutoSize = true;
            this.optCameraOnvif2.Checked = true;
            this.optCameraOnvif2.Location = new System.Drawing.Point(15, 21);
            this.optCameraOnvif2.Name = "optCameraOnvif2";
            this.optCameraOnvif2.Size = new System.Drawing.Size(50, 17);
            this.optCameraOnvif2.TabIndex = 3;
            this.optCameraOnvif2.TabStop = true;
            this.optCameraOnvif2.Text = "Onvif";
            this.optCameraOnvif2.UseVisualStyleBackColor = true;
            this.optCameraOnvif2.CheckedChanged += new System.EventHandler(this.optCameraOnvif2_CheckedChanged);
            // 
            // optCameraRTSP2
            // 
            this.optCameraRTSP2.AutoSize = true;
            this.optCameraRTSP2.Location = new System.Drawing.Point(87, 21);
            this.optCameraRTSP2.Name = "optCameraRTSP2";
            this.optCameraRTSP2.Size = new System.Drawing.Size(54, 17);
            this.optCameraRTSP2.TabIndex = 2;
            this.optCameraRTSP2.Text = "RTSP";
            this.optCameraRTSP2.UseVisualStyleBackColor = true;
            this.optCameraRTSP2.CheckedChanged += new System.EventHandler(this.optCameraRTSP2_CheckedChanged);
            // 
            // txtViinexConfidence2
            // 
            this.txtViinexConfidence2.Location = new System.Drawing.Point(268, 210);
            this.txtViinexConfidence2.Name = "txtViinexConfidence2";
            this.txtViinexConfidence2.Size = new System.Drawing.Size(35, 20);
            this.txtViinexConfidence2.TabIndex = 158;
            this.txtViinexConfidence2.Text = "2";
            this.txtViinexConfidence2.TextChanged += new System.EventHandler(this.txtViinexConfidence2_TextChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.optLat2);
            this.groupBox8.Controls.Add(this.optCyr2);
            this.groupBox8.Location = new System.Drawing.Point(234, 141);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(163, 55);
            this.groupBox8.TabIndex = 157;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Транслитерация";
            // 
            // optLat2
            // 
            this.optLat2.AutoSize = true;
            this.optLat2.Location = new System.Drawing.Point(15, 22);
            this.optLat2.Name = "optLat2";
            this.optLat2.Size = new System.Drawing.Size(56, 17);
            this.optLat2.TabIndex = 158;
            this.optLat2.Text = "Латин";
            this.optLat2.UseVisualStyleBackColor = true;
            this.optLat2.CheckedChanged += new System.EventHandler(this.optLat2_CheckedChanged);
            // 
            // optCyr2
            // 
            this.optCyr2.AutoSize = true;
            this.optCyr2.Checked = true;
            this.optCyr2.Location = new System.Drawing.Point(84, 22);
            this.optCyr2.Name = "optCyr2";
            this.optCyr2.Size = new System.Drawing.Size(73, 17);
            this.optCyr2.TabIndex = 157;
            this.optCyr2.TabStop = true;
            this.optCyr2.Text = "Кириллич";
            this.optCyr2.UseVisualStyleBackColor = true;
            this.optCyr2.CheckedChanged += new System.EventHandler(this.optCyr2_CheckedChanged);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(199, 213);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(61, 13);
            this.label81.TabIndex = 155;
            this.label81.Text = "Confidence";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraPostProcess2
            // 
            this.txtCameraPostProcess2.Location = new System.Drawing.Point(267, 237);
            this.txtCameraPostProcess2.Name = "txtCameraPostProcess2";
            this.txtCameraPostProcess2.Size = new System.Drawing.Size(47, 20);
            this.txtCameraPostProcess2.TabIndex = 154;
            this.txtCameraPostProcess2.Text = "1500";
            this.txtCameraPostProcess2.TextChanged += new System.EventHandler(this.txtCameraPostProcess2_TextChanged);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(175, 240);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(86, 13);
            this.label82.TabIndex = 153;
            this.label82.Text = "PostProcess, мс";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraPreProcess2
            // 
            this.txtCameraPreProcess2.Location = new System.Drawing.Point(118, 237);
            this.txtCameraPreProcess2.Name = "txtCameraPreProcess2";
            this.txtCameraPreProcess2.Size = new System.Drawing.Size(44, 20);
            this.txtCameraPreProcess2.TabIndex = 152;
            this.txtCameraPreProcess2.Text = "1500";
            this.txtCameraPreProcess2.TextChanged += new System.EventHandler(this.txtCameraPreProcess2_TextChanged);
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(35, 240);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(80, 13);
            this.label83.TabIndex = 151;
            this.label83.Text = "Preprocess, мс";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraSkip2
            // 
            this.txtCameraSkip2.Location = new System.Drawing.Point(118, 210);
            this.txtCameraSkip2.Name = "txtCameraSkip2";
            this.txtCameraSkip2.Size = new System.Drawing.Size(44, 20);
            this.txtCameraSkip2.TabIndex = 150;
            this.txtCameraSkip2.Text = "2000";
            this.txtCameraSkip2.TextChanged += new System.EventHandler(this.txtCameraSkip2_TextChanged);
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(41, 213);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(73, 13);
            this.label84.TabIndex = 149;
            this.label84.Text = "Skip, кадров:";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraPassword2
            // 
            this.txtCameraPassword2.Location = new System.Drawing.Point(118, 176);
            this.txtCameraPassword2.Name = "txtCameraPassword2";
            this.txtCameraPassword2.Size = new System.Drawing.Size(100, 20);
            this.txtCameraPassword2.TabIndex = 148;
            this.txtCameraPassword2.Text = "root";
            this.txtCameraPassword2.UseSystemPasswordChar = true;
            this.txtCameraPassword2.TextChanged += new System.EventHandler(this.txtCameraPassword2_TextChanged);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(45, 179);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(53, 13);
            this.label85.TabIndex = 147;
            this.label85.Text = "Password";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraLogin2
            // 
            this.txtCameraLogin2.Location = new System.Drawing.Point(118, 142);
            this.txtCameraLogin2.Name = "txtCameraLogin2";
            this.txtCameraLogin2.Size = new System.Drawing.Size(100, 20);
            this.txtCameraLogin2.TabIndex = 146;
            this.txtCameraLogin2.Text = "root";
            this.txtCameraLogin2.TextChanged += new System.EventHandler(this.txtCameraLogin2_TextChanged);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(65, 145);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(33, 13);
            this.label86.TabIndex = 145;
            this.label86.Text = "Login";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCameraURI2
            // 
            this.txtCameraURI2.Location = new System.Drawing.Point(118, 54);
            this.txtCameraURI2.Name = "txtCameraURI2";
            this.txtCameraURI2.Size = new System.Drawing.Size(281, 20);
            this.txtCameraURI2.TabIndex = 144;
            this.txtCameraURI2.TextChanged += new System.EventHandler(this.txtCameraURI2_TextChanged);
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(31, 57);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(75, 13);
            this.label87.TabIndex = 143;
            this.label87.Text = "URL камеры:";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grSavePhotoLocal
            // 
            this.grSavePhotoLocal.Controls.Add(this.txtSavePhotoTimeout);
            this.grSavePhotoLocal.Controls.Add(this.label70);
            this.grSavePhotoLocal.Controls.Add(this.cmdSavePhotoDir);
            this.grSavePhotoLocal.Controls.Add(this.label69);
            this.grSavePhotoLocal.Controls.Add(this.txtSavePhotoDir);
            this.grSavePhotoLocal.Controls.Add(this.optSavePhotoIfNotRecognized);
            this.grSavePhotoLocal.Controls.Add(this.optNoSavePhoto);
            this.grSavePhotoLocal.Controls.Add(this.optSavePhotoAlways);
            this.grSavePhotoLocal.Location = new System.Drawing.Point(9, 174);
            this.grSavePhotoLocal.Name = "grSavePhotoLocal";
            this.grSavePhotoLocal.Size = new System.Drawing.Size(54, 138);
            this.grSavePhotoLocal.TabIndex = 147;
            this.grSavePhotoLocal.TabStop = false;
            this.grSavePhotoLocal.Text = "Сохранение фото с камеры в БД";
            this.grSavePhotoLocal.Visible = false;
            // 
            // txtSavePhotoTimeout
            // 
            this.txtSavePhotoTimeout.Location = new System.Drawing.Point(307, 97);
            this.txtSavePhotoTimeout.Name = "txtSavePhotoTimeout";
            this.txtSavePhotoTimeout.Size = new System.Drawing.Size(36, 20);
            this.txtSavePhotoTimeout.TabIndex = 155;
            this.txtSavePhotoTimeout.Text = "500";
            this.txtSavePhotoTimeout.TextChanged += new System.EventHandler(this.txtSavePhotoTimeout_TextChanged);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(17, 100);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(284, 13);
            this.label70.TabIndex = 154;
            this.label70.Text = "Время снятия кадра, от времени занятия петли А, мс:";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmdSavePhotoDir
            // 
            this.cmdSavePhotoDir.Location = new System.Drawing.Point(370, 58);
            this.cmdSavePhotoDir.Name = "cmdSavePhotoDir";
            this.cmdSavePhotoDir.Size = new System.Drawing.Size(32, 23);
            this.cmdSavePhotoDir.TabIndex = 146;
            this.cmdSavePhotoDir.Text = "...";
            this.cmdSavePhotoDir.UseVisualStyleBackColor = true;
            this.cmdSavePhotoDir.Click += new System.EventHandler(this.cmdSavePhotoDir_Click);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(15, 64);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(133, 13);
            this.label69.TabIndex = 145;
            this.label69.Text = "Путь к локальной папке:";
            // 
            // txtSavePhotoDir
            // 
            this.txtSavePhotoDir.Location = new System.Drawing.Point(154, 60);
            this.txtSavePhotoDir.Name = "txtSavePhotoDir";
            this.txtSavePhotoDir.Size = new System.Drawing.Size(210, 20);
            this.txtSavePhotoDir.TabIndex = 144;
            this.txtSavePhotoDir.Text = "c:\\Rack\\";
            this.txtSavePhotoDir.TextChanged += new System.EventHandler(this.txtSavePhotoDir_TextChanged);
            // 
            // optSavePhotoIfNotRecognized
            // 
            this.optSavePhotoIfNotRecognized.AutoSize = true;
            this.optSavePhotoIfNotRecognized.Location = new System.Drawing.Point(244, 27);
            this.optSavePhotoIfNotRecognized.Name = "optSavePhotoIfNotRecognized";
            this.optSavePhotoIfNotRecognized.Size = new System.Drawing.Size(157, 17);
            this.optSavePhotoIfNotRecognized.TabIndex = 2;
            this.optSavePhotoIfNotRecognized.Text = "Если номер не распознан";
            this.optSavePhotoIfNotRecognized.UseVisualStyleBackColor = true;
            this.optSavePhotoIfNotRecognized.CheckedChanged += new System.EventHandler(this.optSavePhotoIfNotRecognized_CheckedChanged);
            // 
            // optNoSavePhoto
            // 
            this.optNoSavePhoto.AutoSize = true;
            this.optNoSavePhoto.Checked = true;
            this.optNoSavePhoto.Location = new System.Drawing.Point(14, 28);
            this.optNoSavePhoto.Name = "optNoSavePhoto";
            this.optNoSavePhoto.Size = new System.Drawing.Size(81, 17);
            this.optNoSavePhoto.TabIndex = 1;
            this.optNoSavePhoto.TabStop = true;
            this.optNoSavePhoto.Text = "Отключено";
            this.optNoSavePhoto.UseVisualStyleBackColor = true;
            this.optNoSavePhoto.CheckedChanged += new System.EventHandler(this.optNoSavePhoto_CheckedChanged);
            // 
            // optSavePhotoAlways
            // 
            this.optSavePhotoAlways.AutoSize = true;
            this.optSavePhotoAlways.Location = new System.Drawing.Point(114, 28);
            this.optSavePhotoAlways.Name = "optSavePhotoAlways";
            this.optSavePhotoAlways.Size = new System.Drawing.Size(116, 17);
            this.optSavePhotoAlways.TabIndex = 0;
            this.optSavePhotoAlways.Text = "Сохранять всегда";
            this.optSavePhotoAlways.UseVisualStyleBackColor = true;
            this.optSavePhotoAlways.CheckedChanged += new System.EventHandler(this.optSavePhotoAlways_CheckedChanged);
            // 
            // grEnableLoopANotification
            // 
            this.grEnableLoopANotification.Controls.Add(this.txtLoopANotificationTimeout);
            this.grEnableLoopANotification.Controls.Add(this.label68);
            this.grEnableLoopANotification.Controls.Add(this.txtLoopANotificationUri);
            this.grEnableLoopANotification.Controls.Add(this.label52);
            this.grEnableLoopANotification.Controls.Add(this.chkEnableLoopANotification);
            this.grEnableLoopANotification.Location = new System.Drawing.Point(9, 395);
            this.grEnableLoopANotification.Name = "grEnableLoopANotification";
            this.grEnableLoopANotification.Size = new System.Drawing.Size(333, 107);
            this.grEnableLoopANotification.TabIndex = 146;
            this.grEnableLoopANotification.TabStop = false;
            this.grEnableLoopANotification.Text = "Уведомление удаленного сервера о занятии петли A";
            // 
            // txtLoopANotificationTimeout
            // 
            this.txtLoopANotificationTimeout.Location = new System.Drawing.Point(150, 79);
            this.txtLoopANotificationTimeout.Name = "txtLoopANotificationTimeout";
            this.txtLoopANotificationTimeout.Size = new System.Drawing.Size(36, 20);
            this.txtLoopANotificationTimeout.TabIndex = 154;
            this.txtLoopANotificationTimeout.Text = "500";
            this.txtLoopANotificationTimeout.TextChanged += new System.EventHandler(this.txtLoopANotificationTimeout_TextChanged);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(24, 82);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(118, 13);
            this.label68.TabIndex = 153;
            this.label68.Text = "Таймаут запроса, мс:";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLoopANotificationUri
            // 
            this.txtLoopANotificationUri.Location = new System.Drawing.Point(93, 52);
            this.txtLoopANotificationUri.Name = "txtLoopANotificationUri";
            this.txtLoopANotificationUri.Size = new System.Drawing.Size(229, 20);
            this.txtLoopANotificationUri.TabIndex = 147;
            this.txtLoopANotificationUri.Text = "http://youruri.ru";
            this.txtLoopANotificationUri.TextChanged += new System.EventHandler(this.txtLoopANotificationUri_TextChanged);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(10, 55);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(77, 13);
            this.label52.TabIndex = 146;
            this.label52.Text = "URL запроса:";
            // 
            // chkEnableLoopANotification
            // 
            this.chkEnableLoopANotification.AutoSize = true;
            this.chkEnableLoopANotification.Location = new System.Drawing.Point(10, 23);
            this.chkEnableLoopANotification.Name = "chkEnableLoopANotification";
            this.chkEnableLoopANotification.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkEnableLoopANotification.Size = new System.Drawing.Size(145, 17);
            this.chkEnableLoopANotification.TabIndex = 140;
            this.chkEnableLoopANotification.Text = "Включить уведомление";
            this.chkEnableLoopANotification.UseVisualStyleBackColor = true;
            this.chkEnableLoopANotification.CheckedChanged += new System.EventHandler(this.chkEnableLoopANotification_CheckedChanged);
            // 
            // grViinexMain
            // 
            this.grViinexMain.Controls.Add(this.cmdViinexSettingsPath);
            this.grViinexMain.Controls.Add(this.label54);
            this.grViinexMain.Controls.Add(this.txtViinexSettingsPath);
            this.grViinexMain.Controls.Add(this.label61);
            this.grViinexMain.Controls.Add(this.chkViinex);
            this.grViinexMain.Location = new System.Drawing.Point(351, 7);
            this.grViinexMain.Name = "grViinexMain";
            this.grViinexMain.Size = new System.Drawing.Size(410, 62);
            this.grViinexMain.TabIndex = 144;
            this.grViinexMain.TabStop = false;
            this.grViinexMain.Text = "Основные настройки Viinex";
            // 
            // cmdViinexSettingsPath
            // 
            this.cmdViinexSettingsPath.Location = new System.Drawing.Point(370, 34);
            this.cmdViinexSettingsPath.Name = "cmdViinexSettingsPath";
            this.cmdViinexSettingsPath.Size = new System.Drawing.Size(32, 23);
            this.cmdViinexSettingsPath.TabIndex = 143;
            this.cmdViinexSettingsPath.Text = "...";
            this.cmdViinexSettingsPath.UseVisualStyleBackColor = true;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(15, 40);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(135, 13);
            this.label54.TabIndex = 142;
            this.label54.Text = "Путь к настройкам Viinex";
            // 
            // txtViinexSettingsPath
            // 
            this.txtViinexSettingsPath.Location = new System.Drawing.Point(168, 36);
            this.txtViinexSettingsPath.Name = "txtViinexSettingsPath";
            this.txtViinexSettingsPath.Size = new System.Drawing.Size(196, 20);
            this.txtViinexSettingsPath.TabIndex = 141;
            this.txtViinexSettingsPath.TextChanged += new System.EventHandler(this.txtViinexSettingsPath_TextChanged);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(115, 17);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(35, 13);
            this.label61.TabIndex = 140;
            this.label61.Text = "Viinex";
            // 
            // chkViinex
            // 
            this.chkViinex.AutoSize = true;
            this.chkViinex.Location = new System.Drawing.Point(168, 17);
            this.chkViinex.Name = "chkViinex";
            this.chkViinex.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkViinex.Size = new System.Drawing.Size(15, 14);
            this.chkViinex.TabIndex = 139;
            this.chkViinex.UseVisualStyleBackColor = true;
            this.chkViinex.CheckedChanged += new System.EventHandler(this.chkViinex_CheckedChanged);
            // 
            // cmdUpdateViinexSettings
            // 
            this.cmdUpdateViinexSettings.Location = new System.Drawing.Point(589, 372);
            this.cmdUpdateViinexSettings.Name = "cmdUpdateViinexSettings";
            this.cmdUpdateViinexSettings.Size = new System.Drawing.Size(178, 20);
            this.cmdUpdateViinexSettings.TabIndex = 143;
            this.cmdUpdateViinexSettings.Text = "Применить настройки Viinex";
            this.cmdUpdateViinexSettings.UseVisualStyleBackColor = true;
            this.cmdUpdateViinexSettings.Click += new System.EventHandler(this.cmdUpdateViinexSettings_Click);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(9, 374);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(92, 13);
            this.label66.TabIndex = 140;
            this.label66.Text = "Лиц. ключ Viinex:";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtViinexLicenseKey
            // 
            this.txtViinexLicenseKey.Location = new System.Drawing.Point(104, 372);
            this.txtViinexLicenseKey.Name = "txtViinexLicenseKey";
            this.txtViinexLicenseKey.Size = new System.Drawing.Size(479, 20);
            this.txtViinexLicenseKey.TabIndex = 139;
            this.txtViinexLicenseKey.TextChanged += new System.EventHandler(this.txtViinexLicenseKey_TextChanged);
            // 
            // pnlVision
            // 
            this.pnlVision.Controls.Add(this.chkControlIO);
            this.pnlVision.Controls.Add(this.chkControlBL);
            this.pnlVision.Controls.Add(this.chkLevenIO);
            this.pnlVision.Controls.Add(this.chkLevenBL);
            this.pnlVision.Controls.Add(this.chkLevenQR);
            this.pnlVision.Controls.Add(this.chkLevenPK);
            this.pnlVision.Controls.Add(this.groupBoxBlackList);
            this.pnlVision.Controls.Add(this.chkBarcodePlate);
            this.pnlVision.Controls.Add(this.label53);
            this.pnlVision.Controls.Add(this.chkRegularPlate);
            this.pnlVision.Controls.Add(this.label56);
            this.pnlVision.Controls.Add(this.groupBoxInOut);
            this.pnlVision.Location = new System.Drawing.Point(104, 27);
            this.pnlVision.Name = "pnlVision";
            this.pnlVision.Size = new System.Drawing.Size(235, 299);
            this.pnlVision.TabIndex = 117;
            // 
            // chkControlIO
            // 
            this.chkControlIO.AutoSize = true;
            this.chkControlIO.Location = new System.Drawing.Point(10, 181);
            this.chkControlIO.Name = "chkControlIO";
            this.chkControlIO.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkControlIO.Size = new System.Drawing.Size(112, 17);
            this.chkControlIO.TabIndex = 96;
            this.chkControlIO.Text = "Контроль ПК В-В";
            this.chkControlIO.UseVisualStyleBackColor = true;
            this.chkControlIO.CheckedChanged += new System.EventHandler(this.chkControlIO_CheckedChanged);
            // 
            // chkControlBL
            // 
            this.chkControlBL.AutoSize = true;
            this.chkControlBL.Location = new System.Drawing.Point(8, 58);
            this.chkControlBL.Name = "chkControlBL";
            this.chkControlBL.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkControlBL.Size = new System.Drawing.Size(110, 17);
            this.chkControlBL.TabIndex = 95;
            this.chkControlBL.Text = "Контроль ПК ЧС";
            this.chkControlBL.UseVisualStyleBackColor = true;
            this.chkControlBL.CheckedChanged += new System.EventHandler(this.chkControlBL_CheckedChanged);
            // 
            // chkLevenIO
            // 
            this.chkLevenIO.AutoSize = true;
            this.chkLevenIO.Location = new System.Drawing.Point(11, 209);
            this.chkLevenIO.Name = "chkLevenIO";
            this.chkLevenIO.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkLevenIO.Size = new System.Drawing.Size(109, 17);
            this.chkLevenIO.TabIndex = 94;
            this.chkLevenIO.Text = "Левенштейн В-В";
            this.chkLevenIO.UseVisualStyleBackColor = true;
            this.chkLevenIO.CheckedChanged += new System.EventHandler(this.chkLevenIO_CheckedChanged);
            // 
            // chkLevenBL
            // 
            this.chkLevenBL.AutoSize = true;
            this.chkLevenBL.Location = new System.Drawing.Point(11, 85);
            this.chkLevenBL.Name = "chkLevenBL";
            this.chkLevenBL.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkLevenBL.Size = new System.Drawing.Size(107, 17);
            this.chkLevenBL.TabIndex = 93;
            this.chkLevenBL.Text = "Левенштейн ЧС";
            this.chkLevenBL.UseVisualStyleBackColor = true;
            this.chkLevenBL.CheckedChanged += new System.EventHandler(this.chkLevenBL_CheckedChanged);
            // 
            // chkLevenQR
            // 
            this.chkLevenQR.AutoSize = true;
            this.chkLevenQR.Location = new System.Drawing.Point(116, 276);
            this.chkLevenQR.Name = "chkLevenQR";
            this.chkLevenQR.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkLevenQR.Size = new System.Drawing.Size(108, 17);
            this.chkLevenQR.TabIndex = 92;
            this.chkLevenQR.Text = "Левенштейн QR";
            this.chkLevenQR.UseVisualStyleBackColor = true;
            this.chkLevenQR.CheckedChanged += new System.EventHandler(this.chkLevenQR_CheckedChanged);
            // 
            // chkLevenPK
            // 
            this.chkLevenPK.AutoSize = true;
            this.chkLevenPK.Location = new System.Drawing.Point(114, 244);
            this.chkLevenPK.Name = "chkLevenPK";
            this.chkLevenPK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkLevenPK.Size = new System.Drawing.Size(107, 17);
            this.chkLevenPK.TabIndex = 91;
            this.chkLevenPK.Text = "Левенштейн ПК";
            this.chkLevenPK.UseVisualStyleBackColor = true;
            this.chkLevenPK.CheckedChanged += new System.EventHandler(this.chkLevenPK_CheckedChanged);
            // 
            // groupBoxBlackList
            // 
            this.groupBoxBlackList.Controls.Add(this.optBlackListOperator);
            this.groupBoxBlackList.Controls.Add(this.optBlackListNone);
            this.groupBoxBlackList.Controls.Add(this.optBlackListAllow);
            this.groupBoxBlackList.Controls.Add(this.optBlackListNotAllow);
            this.groupBoxBlackList.Location = new System.Drawing.Point(124, 3);
            this.groupBoxBlackList.Name = "groupBoxBlackList";
            this.groupBoxBlackList.Size = new System.Drawing.Size(93, 112);
            this.groupBoxBlackList.TabIndex = 87;
            this.groupBoxBlackList.TabStop = false;
            // 
            // optBlackListOperator
            // 
            this.optBlackListOperator.AutoSize = true;
            this.optBlackListOperator.Location = new System.Drawing.Point(6, 82);
            this.optBlackListOperator.Name = "optBlackListOperator";
            this.optBlackListOperator.Size = new System.Drawing.Size(74, 17);
            this.optBlackListOperator.TabIndex = 86;
            this.optBlackListOperator.Text = "Оператор";
            this.optBlackListOperator.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.optBlackListOperator.UseVisualStyleBackColor = true;
            this.optBlackListOperator.CheckedChanged += new System.EventHandler(this.optBlackListOperator_CheckedChanged);
            // 
            // optBlackListNone
            // 
            this.optBlackListNone.AutoSize = true;
            this.optBlackListNone.Checked = true;
            this.optBlackListNone.Location = new System.Drawing.Point(6, 12);
            this.optBlackListNone.Name = "optBlackListNone";
            this.optBlackListNone.Size = new System.Drawing.Size(81, 17);
            this.optBlackListNone.TabIndex = 86;
            this.optBlackListNone.TabStop = true;
            this.optBlackListNone.Text = "Отключено";
            this.optBlackListNone.UseVisualStyleBackColor = true;
            this.optBlackListNone.CheckedChanged += new System.EventHandler(this.optBlackListNone_CheckedChanged);
            // 
            // optBlackListAllow
            // 
            this.optBlackListAllow.AutoSize = true;
            this.optBlackListAllow.Location = new System.Drawing.Point(6, 35);
            this.optBlackListAllow.Name = "optBlackListAllow";
            this.optBlackListAllow.Size = new System.Drawing.Size(84, 17);
            this.optBlackListAllow.TabIndex = 86;
            this.optBlackListAllow.Text = "Пропустить";
            this.optBlackListAllow.UseVisualStyleBackColor = true;
            this.optBlackListAllow.CheckedChanged += new System.EventHandler(this.optBlackListAllow_CheckedChanged);
            // 
            // optBlackListNotAllow
            // 
            this.optBlackListNotAllow.AutoSize = true;
            this.optBlackListNotAllow.Location = new System.Drawing.Point(6, 58);
            this.optBlackListNotAllow.Name = "optBlackListNotAllow";
            this.optBlackListNotAllow.Size = new System.Drawing.Size(78, 17);
            this.optBlackListNotAllow.TabIndex = 86;
            this.optBlackListNotAllow.Text = "Запретить";
            this.optBlackListNotAllow.UseVisualStyleBackColor = true;
            this.optBlackListNotAllow.CheckedChanged += new System.EventHandler(this.optBlackListNotAllow_CheckedChanged);
            // 
            // chkBarcodePlate
            // 
            this.chkBarcodePlate.AutoSize = true;
            this.chkBarcodePlate.Location = new System.Drawing.Point(15, 276);
            this.chkBarcodePlate.Name = "chkBarcodePlate";
            this.chkBarcodePlate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkBarcodePlate.Size = new System.Drawing.Size(95, 17);
            this.chkBarcodePlate.TabIndex = 90;
            this.chkBarcodePlate.Text = "Приглашения";
            this.chkBarcodePlate.UseVisualStyleBackColor = true;
            this.chkBarcodePlate.CheckedChanged += new System.EventHandler(this.chkBarcodePlate_CheckedChanged);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(20, 15);
            this.label53.Margin = new System.Windows.Forms.Padding(0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(86, 13);
            this.label53.TabIndex = 84;
            this.label53.Text = "Черный список";
            this.label53.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // chkRegularPlate
            // 
            this.chkRegularPlate.AutoSize = true;
            this.chkRegularPlate.Location = new System.Drawing.Point(15, 245);
            this.chkRegularPlate.Name = "chkRegularPlate";
            this.chkRegularPlate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkRegularPlate.Size = new System.Drawing.Size(82, 17);
            this.chkRegularPlate.TabIndex = 89;
            this.chkRegularPlate.Text = "Проезд ПК";
            this.chkRegularPlate.UseVisualStyleBackColor = true;
            this.chkRegularPlate.CheckedChanged += new System.EventHandler(this.chkRegularPlate_CheckedChanged);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(23, 133);
            this.label56.Margin = new System.Windows.Forms.Padding(0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(75, 13);
            this.label56.TabIndex = 84;
            this.label56.Text = "Въезд-Выезд";
            this.label56.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBoxInOut
            // 
            this.groupBoxInOut.Controls.Add(this.optInOutOperator);
            this.groupBoxInOut.Controls.Add(this.optInOutNone);
            this.groupBoxInOut.Controls.Add(this.optInOutAllow);
            this.groupBoxInOut.Controls.Add(this.optInOutNotAllow);
            this.groupBoxInOut.Location = new System.Drawing.Point(125, 121);
            this.groupBoxInOut.Name = "groupBoxInOut";
            this.groupBoxInOut.Size = new System.Drawing.Size(92, 113);
            this.groupBoxInOut.TabIndex = 88;
            this.groupBoxInOut.TabStop = false;
            // 
            // optInOutOperator
            // 
            this.optInOutOperator.AutoSize = true;
            this.optInOutOperator.Location = new System.Drawing.Point(6, 84);
            this.optInOutOperator.Name = "optInOutOperator";
            this.optInOutOperator.Size = new System.Drawing.Size(74, 17);
            this.optInOutOperator.TabIndex = 86;
            this.optInOutOperator.Text = "Оператор";
            this.optInOutOperator.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.optInOutOperator.UseVisualStyleBackColor = true;
            this.optInOutOperator.CheckedChanged += new System.EventHandler(this.optInOutOperator_CheckedChanged);
            // 
            // optInOutNone
            // 
            this.optInOutNone.AutoSize = true;
            this.optInOutNone.Checked = true;
            this.optInOutNone.Location = new System.Drawing.Point(6, 14);
            this.optInOutNone.Name = "optInOutNone";
            this.optInOutNone.Size = new System.Drawing.Size(81, 17);
            this.optInOutNone.TabIndex = 86;
            this.optInOutNone.TabStop = true;
            this.optInOutNone.Text = "Отключено";
            this.optInOutNone.UseVisualStyleBackColor = true;
            this.optInOutNone.CheckedChanged += new System.EventHandler(this.optInOutNone_CheckedChanged);
            // 
            // optInOutAllow
            // 
            this.optInOutAllow.AutoSize = true;
            this.optInOutAllow.Location = new System.Drawing.Point(6, 37);
            this.optInOutAllow.Name = "optInOutAllow";
            this.optInOutAllow.Size = new System.Drawing.Size(84, 17);
            this.optInOutAllow.TabIndex = 86;
            this.optInOutAllow.Text = "Пропустить";
            this.optInOutAllow.UseVisualStyleBackColor = true;
            this.optInOutAllow.CheckedChanged += new System.EventHandler(this.optInOutAllow_CheckedChanged);
            // 
            // optInOutNotAllow
            // 
            this.optInOutNotAllow.AutoSize = true;
            this.optInOutNotAllow.Location = new System.Drawing.Point(6, 60);
            this.optInOutNotAllow.Name = "optInOutNotAllow";
            this.optInOutNotAllow.Size = new System.Drawing.Size(78, 17);
            this.optInOutNotAllow.TabIndex = 86;
            this.optInOutNotAllow.Text = "Запретить";
            this.optInOutNotAllow.UseVisualStyleBackColor = true;
            this.optInOutNotAllow.CheckedChanged += new System.EventHandler(this.optInOutNotAllow_CheckedChanged);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 143);
            this.label51.Margin = new System.Windows.Forms.Padding(0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(86, 26);
            this.label51.TabIndex = 116;
            this.label51.Text = "Распознавание\r\n гос. номера";
            this.label51.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tabAddSettings
            // 
            this.tabAddSettings.Controls.Add(this.groupBox3);
            this.tabAddSettings.Controls.Add(this.groupBox2);
            this.tabAddSettings.Controls.Add(this.label46);
            this.tabAddSettings.Controls.Add(this.label45);
            this.tabAddSettings.Controls.Add(this.cbo3AltimetrTariff);
            this.tabAddSettings.Controls.Add(this.cbo2AltimetrTariff);
            this.tabAddSettings.Controls.Add(this.cboAltimetrTariff);
            this.tabAddSettings.Controls.Add(this.numericUpDn3AltimetrTariffId);
            this.tabAddSettings.Controls.Add(this.numericUpDn2AltimetrTariffId);
            this.tabAddSettings.Controls.Add(this.numericUpDn1AltimetrTariffId);
            this.tabAddSettings.Controls.Add(this.cbo3AltimetrSchedule);
            this.tabAddSettings.Controls.Add(this.cbo2AltimetrSchedule);
            this.tabAddSettings.Controls.Add(this.cboAltimetrSchedule);
            this.tabAddSettings.Controls.Add(this.numericUpDn3AltimetrScheduleId);
            this.tabAddSettings.Controls.Add(this.numericUpDn2AltimetrScheduleId);
            this.tabAddSettings.Controls.Add(this.numericUpDn1AltimetrScheduleId);
            this.tabAddSettings.Controls.Add(this.cbo3AltimetrModeId);
            this.tabAddSettings.Controls.Add(this.label44);
            this.tabAddSettings.Controls.Add(this.cbo2AltimetrModeId);
            this.tabAddSettings.Controls.Add(this.label43);
            this.tabAddSettings.Controls.Add(this.cboAltimetrModeId);
            this.tabAddSettings.Controls.Add(this.label42);
            this.tabAddSettings.Controls.Add(this.udIR);
            this.tabAddSettings.Controls.Add(this.label38);
            this.tabAddSettings.Controls.Add(this.udLoopB);
            this.tabAddSettings.Controls.Add(this.label4);
            this.tabAddSettings.Controls.Add(this.udLoopA);
            this.tabAddSettings.Controls.Add(this.label2);
            this.tabAddSettings.Controls.Add(this.label5);
            this.tabAddSettings.Controls.Add(this.comboBoxGrupRaz);
            this.tabAddSettings.Controls.Add(this.comboBoxTPRaz);
            this.tabAddSettings.Controls.Add(this.comboBoxTSRaz);
            this.tabAddSettings.Controls.Add(this.cboRedNotCard);
            this.tabAddSettings.Controls.Add(this.udGrupRaz);
            this.tabAddSettings.Controls.Add(this.label35);
            this.tabAddSettings.Controls.Add(this.label34);
            this.tabAddSettings.Controls.Add(this.cbologNastroy);
            this.tabAddSettings.Controls.Add(this.udTarifPlanIdRazov);
            this.tabAddSettings.Controls.Add(this.label32);
            this.tabAddSettings.Controls.Add(this.label25);
            this.tabAddSettings.Controls.Add(this.label24);
            this.tabAddSettings.Controls.Add(this.label13);
            this.tabAddSettings.Controls.Add(this.label12);
            this.tabAddSettings.Controls.Add(this.label10);
            this.tabAddSettings.Controls.Add(this.label9);
            this.tabAddSettings.Controls.Add(this.udTimeNotIr);
            this.tabAddSettings.Controls.Add(this.udTimeNotLoopA);
            this.tabAddSettings.Controls.Add(this.udTarifIdRazov0);
            this.tabAddSettings.Controls.Add(this.udNumberSector);
            this.tabAddSettings.Controls.Add(this.udTimeBarrier);
            this.tabAddSettings.Controls.Add(this.udTimeCardV);
            this.tabAddSettings.Location = new System.Drawing.Point(4, 22);
            this.tabAddSettings.Name = "tabAddSettings";
            this.tabAddSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabAddSettings.Size = new System.Drawing.Size(782, 539);
            this.tabAddSettings.TabIndex = 2;
            this.tabAddSettings.Text = "ДОП НАСТРОЙКИ";
            this.tabAddSettings.UseVisualStyleBackColor = true;
            this.tabAddSettings.Click += new System.EventHandler(this.tabAddSettings_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.udLoopAAlarmsTime);
            this.groupBox3.Controls.Add(this.label57);
            this.groupBox3.Controls.Add(this.chkTrainReverse);
            this.groupBox3.Controls.Add(this.grReverseMode);
            this.groupBox3.Controls.Add(this.chkPlaceCompanyControl);
            this.groupBox3.Controls.Add(this.chkBarcodeFromButton);
            this.groupBox3.Controls.Add(this.chkDisableRazovButton);
            this.groupBox3.Controls.Add(this.chkCardsOnline);
            this.groupBox3.Controls.Add(this.udAddFreeTime);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.chkControlLoopB);
            this.groupBox3.Controls.Add(this.chkNewAlarm);
            this.groupBox3.Controls.Add(this.chkPlaceControlRazov);
            this.groupBox3.Location = new System.Drawing.Point(312, 11);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(316, 323);
            this.groupBox3.TabIndex = 85;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Разное";
            // 
            // udLoopAAlarmsTime
            // 
            this.udLoopAAlarmsTime.Location = new System.Drawing.Point(252, 186);
            this.udLoopAAlarmsTime.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udLoopAAlarmsTime.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.udLoopAAlarmsTime.Name = "udLoopAAlarmsTime";
            this.udLoopAAlarmsTime.Size = new System.Drawing.Size(50, 20);
            this.udLoopAAlarmsTime.TabIndex = 95;
            this.udLoopAAlarmsTime.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(10, 188);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(216, 13);
            this.label57.TabIndex = 94;
            this.label57.Text = "Время закрытия алармов (петля А), мин:";
            // 
            // chkTrainReverse
            // 
            this.chkTrainReverse.AutoSize = true;
            this.chkTrainReverse.Location = new System.Drawing.Point(14, 294);
            this.chkTrainReverse.Name = "chkTrainReverse";
            this.chkTrainReverse.Size = new System.Drawing.Size(228, 17);
            this.chkTrainReverse.TabIndex = 93;
            this.chkTrainReverse.Text = "Реверс стрелы при проезде паровозом";
            this.chkTrainReverse.UseVisualStyleBackColor = true;
            this.chkTrainReverse.CheckedChanged += new System.EventHandler(this.chkTrainReverse_CheckedChanged);
            // 
            // grReverseMode
            // 
            this.grReverseMode.Controls.Add(this.optTwoLevelReverse);
            this.grReverseMode.Controls.Add(this.optInOutReverse);
            this.grReverseMode.Location = new System.Drawing.Point(14, 216);
            this.grReverseMode.Name = "grReverseMode";
            this.grReverseMode.Size = new System.Drawing.Size(230, 69);
            this.grReverseMode.TabIndex = 92;
            this.grReverseMode.TabStop = false;
            this.grReverseMode.Text = "Режим реверса";
            // 
            // optTwoLevelReverse
            // 
            this.optTwoLevelReverse.AutoSize = true;
            this.optTwoLevelReverse.Location = new System.Drawing.Point(120, 27);
            this.optTwoLevelReverse.Name = "optTwoLevelReverse";
            this.optTwoLevelReverse.Size = new System.Drawing.Size(103, 17);
            this.optTwoLevelReverse.TabIndex = 1;
            this.optTwoLevelReverse.Text = "Двухуровневая";
            this.optTwoLevelReverse.UseVisualStyleBackColor = true;
            this.optTwoLevelReverse.CheckedChanged += new System.EventHandler(this.optTwoLevelReverse_CheckedChanged);
            // 
            // optInOutReverse
            // 
            this.optInOutReverse.AutoSize = true;
            this.optInOutReverse.Checked = true;
            this.optInOutReverse.Location = new System.Drawing.Point(14, 27);
            this.optInOutReverse.Name = "optInOutReverse";
            this.optInOutReverse.Size = new System.Drawing.Size(94, 17);
            this.optInOutReverse.TabIndex = 0;
            this.optInOutReverse.TabStop = true;
            this.optInOutReverse.Text = "Въезд/выезд";
            this.optInOutReverse.UseVisualStyleBackColor = true;
            this.optInOutReverse.CheckedChanged += new System.EventHandler(this.optInOutReverse_CheckedChanged);
            // 
            // chkPlaceCompanyControl
            // 
            this.chkPlaceCompanyControl.AutoSize = true;
            this.chkPlaceCompanyControl.Checked = true;
            this.chkPlaceCompanyControl.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPlaceCompanyControl.Location = new System.Drawing.Point(159, 52);
            this.chkPlaceCompanyControl.Name = "chkPlaceCompanyControl";
            this.chkPlaceCompanyControl.Size = new System.Drawing.Size(155, 17);
            this.chkPlaceCompanyControl.TabIndex = 91;
            this.chkPlaceCompanyControl.Text = "Контроль мест компаний";
            this.chkPlaceCompanyControl.UseVisualStyleBackColor = true;
            this.chkPlaceCompanyControl.CheckedChanged += new System.EventHandler(this.chkPlaceCompanyControl_CheckedChanged);
            // 
            // chkBarcodeFromButton
            // 
            this.chkBarcodeFromButton.AutoSize = true;
            this.chkBarcodeFromButton.Location = new System.Drawing.Point(173, 19);
            this.chkBarcodeFromButton.Name = "chkBarcodeFromButton";
            this.chkBarcodeFromButton.Size = new System.Drawing.Size(129, 17);
            this.chkBarcodeFromButton.TabIndex = 90;
            this.chkBarcodeFromButton.Text = "Штрихкод по кнопке";
            this.chkBarcodeFromButton.UseVisualStyleBackColor = true;
            this.chkBarcodeFromButton.CheckedChanged += new System.EventHandler(this.chkBarcodeFromButton_CheckedChanged);
            // 
            // chkDisableRazovButton
            // 
            this.chkDisableRazovButton.AutoSize = true;
            this.chkDisableRazovButton.Location = new System.Drawing.Point(11, 19);
            this.chkDisableRazovButton.Name = "chkDisableRazovButton";
            this.chkDisableRazovButton.Size = new System.Drawing.Size(159, 17);
            this.chkDisableRazovButton.TabIndex = 89;
            this.chkDisableRazovButton.Text = "Разовые карты по кнопке";
            this.chkDisableRazovButton.UseVisualStyleBackColor = true;
            this.chkDisableRazovButton.CheckedChanged += new System.EventHandler(this.chkDisableRazovButton_CheckedChanged);
            // 
            // chkCardsOnline
            // 
            this.chkCardsOnline.AutoSize = true;
            this.chkCardsOnline.Location = new System.Drawing.Point(11, 134);
            this.chkCardsOnline.Name = "chkCardsOnline";
            this.chkCardsOnline.Size = new System.Drawing.Size(86, 17);
            this.chkCardsOnline.TabIndex = 88;
            this.chkCardsOnline.Text = "Cards Online";
            this.chkCardsOnline.UseVisualStyleBackColor = true;
            this.chkCardsOnline.CheckedChanged += new System.EventHandler(this.chkCardsOnline_CheckedChanged);
            // 
            // udAddFreeTime
            // 
            this.udAddFreeTime.Location = new System.Drawing.Point(251, 158);
            this.udAddFreeTime.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udAddFreeTime.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.udAddFreeTime.Name = "udAddFreeTime";
            this.udAddFreeTime.Size = new System.Drawing.Size(50, 20);
            this.udAddFreeTime.TabIndex = 87;
            this.udAddFreeTime.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.udAddFreeTime.ValueChanged += new System.EventHandler(this.udAddFreeTime_ValueChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(9, 160);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(237, 13);
            this.label37.TabIndex = 86;
            this.label37.Text = "Дополнит. бесплатное время парковки, мин:";
            // 
            // chkControlLoopB
            // 
            this.chkControlLoopB.AutoSize = true;
            this.chkControlLoopB.Location = new System.Drawing.Point(11, 80);
            this.chkControlLoopB.Name = "chkControlLoopB";
            this.chkControlLoopB.Size = new System.Drawing.Size(164, 17);
            this.chkControlLoopB.TabIndex = 85;
            this.chkControlLoopB.Text = "Контроль проезда по loopB";
            this.chkControlLoopB.UseVisualStyleBackColor = true;
            this.chkControlLoopB.CheckedChanged += new System.EventHandler(this.chkControlLoopB_CheckedChanged);
            // 
            // chkNewAlarm
            // 
            this.chkNewAlarm.AutoSize = true;
            this.chkNewAlarm.Location = new System.Drawing.Point(11, 107);
            this.chkNewAlarm.Name = "chkNewAlarm";
            this.chkNewAlarm.Size = new System.Drawing.Size(77, 17);
            this.chkNewAlarm.TabIndex = 84;
            this.chkNewAlarm.Text = "New Alarm";
            this.chkNewAlarm.UseVisualStyleBackColor = true;
            this.chkNewAlarm.CheckedChanged += new System.EventHandler(this.chkNewAlarm_CheckedChanged);
            // 
            // chkPlaceControlRazov
            // 
            this.chkPlaceControlRazov.AutoSize = true;
            this.chkPlaceControlRazov.Checked = true;
            this.chkPlaceControlRazov.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPlaceControlRazov.Location = new System.Drawing.Point(11, 52);
            this.chkPlaceControlRazov.Name = "chkPlaceControlRazov";
            this.chkPlaceControlRazov.Size = new System.Drawing.Size(148, 17);
            this.chkPlaceControlRazov.TabIndex = 83;
            this.chkPlaceControlRazov.Text = "Контроль мест разовых";
            this.chkPlaceControlRazov.UseVisualStyleBackColor = true;
            this.chkPlaceControlRazov.CheckedChanged += new System.EventHandler(this.chkPlaceControlRazov_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkInvertCall);
            this.groupBox2.Controls.Add(this.chkPUSH);
            this.groupBox2.Controls.Add(this.chkIR);
            this.groupBox2.Controls.Add(this.chkLoopB);
            this.groupBox2.Controls.Add(this.chkLoopA);
            this.groupBox2.Location = new System.Drawing.Point(634, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(137, 144);
            this.groupBox2.TabIndex = 84;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Инверсия";
            // 
            // chkInvertCall
            // 
            this.chkInvertCall.AutoSize = true;
            this.chkInvertCall.Checked = true;
            this.chkInvertCall.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInvertCall.Location = new System.Drawing.Point(17, 116);
            this.chkInvertCall.Name = "chkInvertCall";
            this.chkInvertCall.Size = new System.Drawing.Size(98, 17);
            this.chkInvertCall.TabIndex = 88;
            this.chkInvertCall.Text = "Инверт вызов";
            this.chkInvertCall.UseVisualStyleBackColor = true;
            this.chkInvertCall.CheckedChanged += new System.EventHandler(this.chkInvertCall_CheckedChanged);
            // 
            // chkPUSH
            // 
            this.chkPUSH.AutoSize = true;
            this.chkPUSH.Location = new System.Drawing.Point(17, 90);
            this.chkPUSH.Name = "chkPUSH";
            this.chkPUSH.Size = new System.Drawing.Size(83, 17);
            this.chkPUSH.TabIndex = 87;
            this.chkPUSH.Text = "\"Нажмите\"";
            this.chkPUSH.UseVisualStyleBackColor = true;
            this.chkPUSH.CheckedChanged += new System.EventHandler(this.chkPUSH_CheckedChanged);
            // 
            // chkIR
            // 
            this.chkIR.AutoSize = true;
            this.chkIR.Location = new System.Drawing.Point(17, 65);
            this.chkIR.Name = "chkIR";
            this.chkIR.Size = new System.Drawing.Size(41, 17);
            this.chkIR.TabIndex = 86;
            this.chkIR.Text = "ИК";
            this.chkIR.UseVisualStyleBackColor = true;
            this.chkIR.CheckedChanged += new System.EventHandler(this.chkIR_CheckedChanged);
            // 
            // chkLoopB
            // 
            this.chkLoopB.AutoSize = true;
            this.chkLoopB.Checked = true;
            this.chkLoopB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLoopB.Location = new System.Drawing.Point(17, 42);
            this.chkLoopB.Name = "chkLoopB";
            this.chkLoopB.Size = new System.Drawing.Size(53, 17);
            this.chkLoopB.TabIndex = 85;
            this.chkLoopB.Text = "loopB";
            this.chkLoopB.UseVisualStyleBackColor = true;
            this.chkLoopB.CheckedChanged += new System.EventHandler(this.chkLoopB_CheckedChanged);
            // 
            // chkLoopA
            // 
            this.chkLoopA.AutoSize = true;
            this.chkLoopA.Checked = true;
            this.chkLoopA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLoopA.Location = new System.Drawing.Point(17, 19);
            this.chkLoopA.Name = "chkLoopA";
            this.chkLoopA.Size = new System.Drawing.Size(53, 17);
            this.chkLoopA.TabIndex = 84;
            this.chkLoopA.Text = "loopA";
            this.chkLoopA.UseVisualStyleBackColor = true;
            this.chkLoopA.CheckedChanged += new System.EventHandler(this.chkLoopA_CheckedChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(482, 340);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(22, 13);
            this.label46.TabIndex = 70;
            this.label46.Text = "ТП";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(239, 340);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(21, 13);
            this.label45.TabIndex = 69;
            this.label45.Text = "ТС";
            // 
            // cbo3AltimetrTariff
            // 
            this.cbo3AltimetrTariff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo3AltimetrTariff.FormattingEnabled = true;
            this.cbo3AltimetrTariff.Location = new System.Drawing.Point(537, 450);
            this.cbo3AltimetrTariff.Name = "cbo3AltimetrTariff";
            this.cbo3AltimetrTariff.Size = new System.Drawing.Size(185, 21);
            this.cbo3AltimetrTariff.TabIndex = 68;
            this.cbo3AltimetrTariff.DropDown += new System.EventHandler(this.cbo3AltimetrTariff_DropDown);
            this.cbo3AltimetrTariff.SelectedIndexChanged += new System.EventHandler(this.cbo3AltimetrTariff_SelectedIndexChanged);
            this.cbo3AltimetrTariff.DropDownClosed += new System.EventHandler(this.cbo3AltimetrTariff_DropDownClosed);
            // 
            // cbo2AltimetrTariff
            // 
            this.cbo2AltimetrTariff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo2AltimetrTariff.FormattingEnabled = true;
            this.cbo2AltimetrTariff.Location = new System.Drawing.Point(537, 405);
            this.cbo2AltimetrTariff.Name = "cbo2AltimetrTariff";
            this.cbo2AltimetrTariff.Size = new System.Drawing.Size(185, 21);
            this.cbo2AltimetrTariff.TabIndex = 67;
            this.cbo2AltimetrTariff.DropDown += new System.EventHandler(this.cbo2AltimetrTariff_DropDown);
            this.cbo2AltimetrTariff.SelectedIndexChanged += new System.EventHandler(this.cbo2AltimetrTariff_SelectedIndexChanged);
            this.cbo2AltimetrTariff.DropDownClosed += new System.EventHandler(this.cbo2AltimetrTariff_DropDownClosed);
            // 
            // cboAltimetrTariff
            // 
            this.cboAltimetrTariff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAltimetrTariff.FormattingEnabled = true;
            this.cboAltimetrTariff.Location = new System.Drawing.Point(537, 360);
            this.cboAltimetrTariff.Name = "cboAltimetrTariff";
            this.cboAltimetrTariff.Size = new System.Drawing.Size(185, 21);
            this.cboAltimetrTariff.TabIndex = 66;
            this.cboAltimetrTariff.DropDown += new System.EventHandler(this.cboAltimetrTariff_DropDown);
            this.cboAltimetrTariff.SelectedIndexChanged += new System.EventHandler(this.cboAltimetrTariff_SelectedIndexChanged);
            this.cboAltimetrTariff.DropDownClosed += new System.EventHandler(this.cboAltimetrTariff_DropDownClosed);
            // 
            // numericUpDn3AltimetrTariffId
            // 
            this.numericUpDn3AltimetrTariffId.Location = new System.Drawing.Point(490, 450);
            this.numericUpDn3AltimetrTariffId.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDn3AltimetrTariffId.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn3AltimetrTariffId.Name = "numericUpDn3AltimetrTariffId";
            this.numericUpDn3AltimetrTariffId.Size = new System.Drawing.Size(41, 20);
            this.numericUpDn3AltimetrTariffId.TabIndex = 65;
            this.numericUpDn3AltimetrTariffId.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn3AltimetrTariffId.ValueChanged += new System.EventHandler(this.numericUpDn3AltimetrTariffId_ValueChanged);
            // 
            // numericUpDn2AltimetrTariffId
            // 
            this.numericUpDn2AltimetrTariffId.Location = new System.Drawing.Point(490, 405);
            this.numericUpDn2AltimetrTariffId.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDn2AltimetrTariffId.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn2AltimetrTariffId.Name = "numericUpDn2AltimetrTariffId";
            this.numericUpDn2AltimetrTariffId.Size = new System.Drawing.Size(41, 20);
            this.numericUpDn2AltimetrTariffId.TabIndex = 64;
            this.numericUpDn2AltimetrTariffId.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn2AltimetrTariffId.ValueChanged += new System.EventHandler(this.numericUpDn2AltimetrTariffId_ValueChanged);
            // 
            // numericUpDn1AltimetrTariffId
            // 
            this.numericUpDn1AltimetrTariffId.Location = new System.Drawing.Point(490, 360);
            this.numericUpDn1AltimetrTariffId.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDn1AltimetrTariffId.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn1AltimetrTariffId.Name = "numericUpDn1AltimetrTariffId";
            this.numericUpDn1AltimetrTariffId.Size = new System.Drawing.Size(41, 20);
            this.numericUpDn1AltimetrTariffId.TabIndex = 63;
            this.numericUpDn1AltimetrTariffId.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn1AltimetrTariffId.ValueChanged += new System.EventHandler(this.numericUpDn1AltimetrTariffId_ValueChanged);
            // 
            // cbo3AltimetrSchedule
            // 
            this.cbo3AltimetrSchedule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo3AltimetrSchedule.FormattingEnabled = true;
            this.cbo3AltimetrSchedule.Location = new System.Drawing.Point(294, 450);
            this.cbo3AltimetrSchedule.Name = "cbo3AltimetrSchedule";
            this.cbo3AltimetrSchedule.Size = new System.Drawing.Size(185, 21);
            this.cbo3AltimetrSchedule.TabIndex = 62;
            this.cbo3AltimetrSchedule.DropDown += new System.EventHandler(this.cbo3AltimetrSchedule_DropDown);
            this.cbo3AltimetrSchedule.SelectedIndexChanged += new System.EventHandler(this.cbo3AltimetrSchedule_SelectedIndexChanged);
            this.cbo3AltimetrSchedule.DropDownClosed += new System.EventHandler(this.cbo3AltimetrSchedule_DropDownClosed);
            // 
            // cbo2AltimetrSchedule
            // 
            this.cbo2AltimetrSchedule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo2AltimetrSchedule.FormattingEnabled = true;
            this.cbo2AltimetrSchedule.Location = new System.Drawing.Point(294, 405);
            this.cbo2AltimetrSchedule.Name = "cbo2AltimetrSchedule";
            this.cbo2AltimetrSchedule.Size = new System.Drawing.Size(185, 21);
            this.cbo2AltimetrSchedule.TabIndex = 61;
            this.cbo2AltimetrSchedule.DropDown += new System.EventHandler(this.cbo2AltimetrSchedule_DropDown);
            this.cbo2AltimetrSchedule.SelectedIndexChanged += new System.EventHandler(this.cbo2AltimetrSchedule_SelectedIndexChanged);
            this.cbo2AltimetrSchedule.DropDownClosed += new System.EventHandler(this.cbo2AltimetrSchedule_DropDownClosed);
            // 
            // cboAltimetrSchedule
            // 
            this.cboAltimetrSchedule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAltimetrSchedule.FormattingEnabled = true;
            this.cboAltimetrSchedule.Location = new System.Drawing.Point(294, 360);
            this.cboAltimetrSchedule.Name = "cboAltimetrSchedule";
            this.cboAltimetrSchedule.Size = new System.Drawing.Size(185, 21);
            this.cboAltimetrSchedule.TabIndex = 60;
            this.cboAltimetrSchedule.DropDown += new System.EventHandler(this.cboAltimetrSchedule_DropDown);
            this.cboAltimetrSchedule.SelectedIndexChanged += new System.EventHandler(this.cboAltimetrSchedule_SelectedIndexChanged);
            this.cboAltimetrSchedule.DropDownClosed += new System.EventHandler(this.cboAltimetrSchedule_DropDownClosed);
            // 
            // numericUpDn3AltimetrScheduleId
            // 
            this.numericUpDn3AltimetrScheduleId.Location = new System.Drawing.Point(247, 450);
            this.numericUpDn3AltimetrScheduleId.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDn3AltimetrScheduleId.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn3AltimetrScheduleId.Name = "numericUpDn3AltimetrScheduleId";
            this.numericUpDn3AltimetrScheduleId.Size = new System.Drawing.Size(41, 20);
            this.numericUpDn3AltimetrScheduleId.TabIndex = 59;
            this.numericUpDn3AltimetrScheduleId.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn3AltimetrScheduleId.ValueChanged += new System.EventHandler(this.numericUpDn3AltimetrScheduleId_ValueChanged);
            // 
            // numericUpDn2AltimetrScheduleId
            // 
            this.numericUpDn2AltimetrScheduleId.Location = new System.Drawing.Point(247, 405);
            this.numericUpDn2AltimetrScheduleId.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDn2AltimetrScheduleId.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn2AltimetrScheduleId.Name = "numericUpDn2AltimetrScheduleId";
            this.numericUpDn2AltimetrScheduleId.Size = new System.Drawing.Size(41, 20);
            this.numericUpDn2AltimetrScheduleId.TabIndex = 58;
            this.numericUpDn2AltimetrScheduleId.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn2AltimetrScheduleId.ValueChanged += new System.EventHandler(this.numericUpDn2AltimetrScheduleId_ValueChanged);
            // 
            // numericUpDn1AltimetrScheduleId
            // 
            this.numericUpDn1AltimetrScheduleId.Location = new System.Drawing.Point(247, 360);
            this.numericUpDn1AltimetrScheduleId.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDn1AltimetrScheduleId.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn1AltimetrScheduleId.Name = "numericUpDn1AltimetrScheduleId";
            this.numericUpDn1AltimetrScheduleId.Size = new System.Drawing.Size(41, 20);
            this.numericUpDn1AltimetrScheduleId.TabIndex = 57;
            this.numericUpDn1AltimetrScheduleId.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDn1AltimetrScheduleId.ValueChanged += new System.EventHandler(this.numericUpDn1AltimetrScheduleId_ValueChanged);
            // 
            // cbo3AltimetrModeId
            // 
            this.cbo3AltimetrModeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo3AltimetrModeId.FormattingEnabled = true;
            this.cbo3AltimetrModeId.Items.AddRange(new object[] {
            "Не используется",
            "Определение тарифа",
            "Пропуск транспортного средства"});
            this.cbo3AltimetrModeId.Location = new System.Drawing.Point(20, 450);
            this.cbo3AltimetrModeId.Name = "cbo3AltimetrModeId";
            this.cbo3AltimetrModeId.Size = new System.Drawing.Size(200, 21);
            this.cbo3AltimetrModeId.TabIndex = 56;
            this.cbo3AltimetrModeId.DropDown += new System.EventHandler(this.cbo3AltimetrModeId_DropDown);
            this.cbo3AltimetrModeId.SelectedIndexChanged += new System.EventHandler(this.cbo3AltimetrModeId_SelectedIndexChanged);
            this.cbo3AltimetrModeId.DropDownClosed += new System.EventHandler(this.cbo3AltimetrModeId_DropDownClosed);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(20, 430);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(71, 13);
            this.label44.TabIndex = 55;
            this.label44.Text = "Высотомер3";
            // 
            // cbo2AltimetrModeId
            // 
            this.cbo2AltimetrModeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo2AltimetrModeId.FormattingEnabled = true;
            this.cbo2AltimetrModeId.Items.AddRange(new object[] {
            "Не используется",
            "Определение тарифа",
            "Пропуск транспортного средства"});
            this.cbo2AltimetrModeId.Location = new System.Drawing.Point(20, 405);
            this.cbo2AltimetrModeId.Name = "cbo2AltimetrModeId";
            this.cbo2AltimetrModeId.Size = new System.Drawing.Size(200, 21);
            this.cbo2AltimetrModeId.TabIndex = 54;
            this.cbo2AltimetrModeId.DropDown += new System.EventHandler(this.cbo2AltimetrModeId_DropDown);
            this.cbo2AltimetrModeId.SelectedIndexChanged += new System.EventHandler(this.cbo2AltimetrModeId_SelectedIndexChanged);
            this.cbo2AltimetrModeId.DropDownClosed += new System.EventHandler(this.cbo2AltimetrModeId_DropDownClosed);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(17, 385);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(71, 13);
            this.label43.TabIndex = 53;
            this.label43.Text = "Высотомер2";
            // 
            // cboAltimetrModeId
            // 
            this.cboAltimetrModeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAltimetrModeId.FormattingEnabled = true;
            this.cboAltimetrModeId.Items.AddRange(new object[] {
            "Не используется",
            "Определение тарифа",
            "Пропуск транспортного средства"});
            this.cboAltimetrModeId.Location = new System.Drawing.Point(20, 360);
            this.cboAltimetrModeId.Name = "cboAltimetrModeId";
            this.cboAltimetrModeId.Size = new System.Drawing.Size(200, 21);
            this.cboAltimetrModeId.TabIndex = 52;
            this.cboAltimetrModeId.DropDown += new System.EventHandler(this.cboAltimetrModeId_DropDown);
            this.cboAltimetrModeId.SelectedIndexChanged += new System.EventHandler(this.cboAltimetrModeId_SelectedIndexChanged);
            this.cboAltimetrModeId.DropDownClosed += new System.EventHandler(this.cboAltimetrModeId_DropDownClosed);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(17, 340);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(71, 13);
            this.label42.TabIndex = 51;
            this.label42.Text = "Высотомер1";
            // 
            // udIR
            // 
            this.udIR.Location = new System.Drawing.Point(224, 130);
            this.udIR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udIR.Name = "udIR";
            this.udIR.Size = new System.Drawing.Size(50, 20);
            this.udIR.TabIndex = 50;
            this.udIR.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.udIR.ValueChanged += new System.EventHandler(this.udIR_ValueChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(221, 114);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(22, 13);
            this.label38.TabIndex = 49;
            this.label38.Text = "ИК";
            // 
            // udLoopB
            // 
            this.udLoopB.Location = new System.Drawing.Point(224, 91);
            this.udLoopB.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udLoopB.Name = "udLoopB";
            this.udLoopB.Size = new System.Drawing.Size(50, 20);
            this.udLoopB.TabIndex = 48;
            this.udLoopB.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.udLoopB.ValueChanged += new System.EventHandler(this.udLoopB_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(221, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 47;
            this.label4.Text = "loopB";
            // 
            // udLoopA
            // 
            this.udLoopA.Location = new System.Drawing.Point(224, 52);
            this.udLoopA.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udLoopA.Name = "udLoopA";
            this.udLoopA.Size = new System.Drawing.Size(50, 20);
            this.udLoopA.TabIndex = 46;
            this.udLoopA.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.udLoopA.ValueChanged += new System.EventHandler(this.udLoopA_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(214, 23);
            this.label2.MaximumSize = new System.Drawing.Size(130, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 26);
            this.label2.TabIndex = 45;
            this.label2.Text = "Время до тревог\r\nсек - loopA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(626, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 13);
            this.label5.TabIndex = 42;
            this.label5.Text = "Нет карт - красный/зеленый";
            // 
            // comboBoxGrupRaz
            // 
            this.comboBoxGrupRaz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGrupRaz.FormattingEnabled = true;
            this.comboBoxGrupRaz.Location = new System.Drawing.Point(70, 308);
            this.comboBoxGrupRaz.Name = "comboBoxGrupRaz";
            this.comboBoxGrupRaz.Size = new System.Drawing.Size(185, 21);
            this.comboBoxGrupRaz.TabIndex = 41;
            this.comboBoxGrupRaz.DropDown += new System.EventHandler(this.comboBoxGrupRaz_DropDown);
            this.comboBoxGrupRaz.SelectedIndexChanged += new System.EventHandler(this.comboBoxGrupRaz_SelectedIndexChanged);
            this.comboBoxGrupRaz.DropDownClosed += new System.EventHandler(this.comboBoxGrupRaz_DropDownClosed);
            // 
            // comboBoxTPRaz
            // 
            this.comboBoxTPRaz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTPRaz.FormattingEnabled = true;
            this.comboBoxTPRaz.Location = new System.Drawing.Point(70, 263);
            this.comboBoxTPRaz.Name = "comboBoxTPRaz";
            this.comboBoxTPRaz.Size = new System.Drawing.Size(183, 21);
            this.comboBoxTPRaz.TabIndex = 40;
            this.comboBoxTPRaz.DropDown += new System.EventHandler(this.comboBoxTPRaz_DropDown);
            this.comboBoxTPRaz.SelectedIndexChanged += new System.EventHandler(this.comboBoxTPRaz_SelectedIndexChanged);
            this.comboBoxTPRaz.DropDownClosed += new System.EventHandler(this.comboBoxTPRaz_DropDownClosed);
            // 
            // comboBoxTSRaz
            // 
            this.comboBoxTSRaz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTSRaz.FormattingEnabled = true;
            this.comboBoxTSRaz.Location = new System.Drawing.Point(70, 215);
            this.comboBoxTSRaz.Name = "comboBoxTSRaz";
            this.comboBoxTSRaz.Size = new System.Drawing.Size(184, 21);
            this.comboBoxTSRaz.TabIndex = 39;
            this.comboBoxTSRaz.DropDown += new System.EventHandler(this.comboBoxTSRaz_DropDown);
            this.comboBoxTSRaz.SelectedIndexChanged += new System.EventHandler(this.comboBoxTSRaz_SelectedIndexChanged);
            this.comboBoxTSRaz.DropDownClosed += new System.EventHandler(this.comboBoxTSRaz_DropDownClosed);
            // 
            // cboRedNotCard
            // 
            this.cboRedNotCard.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRedNotCard.FormattingEnabled = true;
            this.cboRedNotCard.Location = new System.Drawing.Point(634, 168);
            this.cboRedNotCard.Name = "cboRedNotCard";
            this.cboRedNotCard.Size = new System.Drawing.Size(124, 21);
            this.cboRedNotCard.TabIndex = 38;
            this.cboRedNotCard.SelectedIndexChanged += new System.EventHandler(this.cboRedNotCard_SelectedIndexChanged);
            // 
            // udGrupRaz
            // 
            this.udGrupRaz.Location = new System.Drawing.Point(23, 307);
            this.udGrupRaz.Name = "udGrupRaz";
            this.udGrupRaz.Size = new System.Drawing.Size(40, 20);
            this.udGrupRaz.TabIndex = 37;
            this.udGrupRaz.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udGrupRaz.ValueChanged += new System.EventHandler(this.udGrupRaz_ValueChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(20, 290);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(92, 13);
            this.label35.TabIndex = 36;
            this.label35.Text = "Группа разового";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(634, 198);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(88, 13);
            this.label34.TabIndex = 35;
            this.label34.Text = "Настройка лога";
            // 
            // cbologNastroy
            // 
            this.cbologNastroy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbologNastroy.FormattingEnabled = true;
            this.cbologNastroy.Location = new System.Drawing.Point(637, 216);
            this.cbologNastroy.Name = "cbologNastroy";
            this.cbologNastroy.Size = new System.Drawing.Size(121, 21);
            this.cbologNastroy.TabIndex = 34;
            this.cbologNastroy.DropDown += new System.EventHandler(this.cbologNastroy_DropDown);
            this.cbologNastroy.SelectedIndexChanged += new System.EventHandler(this.cbologNastroy_SelectedIndexChanged);
            this.cbologNastroy.DropDownClosed += new System.EventHandler(this.cbologNastroy_DropDownClosed);
            // 
            // udTarifPlanIdRazov
            // 
            this.udTarifPlanIdRazov.Location = new System.Drawing.Point(23, 263);
            this.udTarifPlanIdRazov.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udTarifPlanIdRazov.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udTarifPlanIdRazov.Name = "udTarifPlanIdRazov";
            this.udTarifPlanIdRazov.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.udTarifPlanIdRazov.Size = new System.Drawing.Size(40, 20);
            this.udTarifPlanIdRazov.TabIndex = 30;
            this.udTarifPlanIdRazov.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udTarifPlanIdRazov.ValueChanged += new System.EventHandler(this.udTarifPlanIdRazov_ValueChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(20, 245);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(83, 13);
            this.label32.TabIndex = 29;
            this.label32.Text = "ID ТП разовый";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(135, 91);
            this.label25.MaximumSize = new System.Drawing.Size(130, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(76, 26);
            this.label25.TabIndex = 11;
            this.label25.Text = "Время не ИК (х100мсек)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(125, 25);
            this.label24.MaximumSize = new System.Drawing.Size(130, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(88, 26);
            this.label24.TabIndex = 9;
            this.label24.Text = "Время не loopA (х100мсек)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(20, 199);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "ID ТС разовый";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 149);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Номер сектора";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(2, 78);
            this.label10.MaximumSize = new System.Drawing.Size(135, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(135, 39);
            this.label10.TabIndex = 3;
            this.label10.Text = "Длительность импульса \r\nуправления шлагбаумом\r\n (х100мсек.)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 25);
            this.label9.MaximumSize = new System.Drawing.Size(135, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 26);
            this.label9.TabIndex = 1;
            this.label9.Text = "Время карты \r\nв губах (сек.)";
            // 
            // udTimeNotIr
            // 
            this.udTimeNotIr.Location = new System.Drawing.Point(138, 120);
            this.udTimeNotIr.Name = "udTimeNotIr";
            this.udTimeNotIr.Size = new System.Drawing.Size(41, 20);
            this.udTimeNotIr.TabIndex = 12;
            this.udTimeNotIr.ValueChanged += new System.EventHandler(this.udTimeNotIr_ValueChanged);
            // 
            // udTimeNotLoopA
            // 
            this.udTimeNotLoopA.Location = new System.Drawing.Point(138, 54);
            this.udTimeNotLoopA.Name = "udTimeNotLoopA";
            this.udTimeNotLoopA.Size = new System.Drawing.Size(41, 20);
            this.udTimeNotLoopA.TabIndex = 10;
            this.udTimeNotLoopA.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.udTimeNotLoopA.ValueChanged += new System.EventHandler(this.udTimeNotLoopA_ValueChanged);
            // 
            // udTarifIdRazov0
            // 
            this.udTarifIdRazov0.Location = new System.Drawing.Point(23, 215);
            this.udTarifIdRazov0.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udTarifIdRazov0.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udTarifIdRazov0.Name = "udTarifIdRazov0";
            this.udTarifIdRazov0.Size = new System.Drawing.Size(40, 20);
            this.udTarifIdRazov0.TabIndex = 8;
            this.udTarifIdRazov0.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udTarifIdRazov0.ValueChanged += new System.EventHandler(this.udTarifIdRazov0_ValueChanged);
            // 
            // udNumberSector
            // 
            this.udNumberSector.Location = new System.Drawing.Point(23, 165);
            this.udNumberSector.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.udNumberSector.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNumberSector.Name = "udNumberSector";
            this.udNumberSector.Size = new System.Drawing.Size(40, 20);
            this.udNumberSector.TabIndex = 6;
            this.udNumberSector.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNumberSector.ValueChanged += new System.EventHandler(this.udNumberSector_ValueChanged);
            // 
            // udTimeBarrier
            // 
            this.udTimeBarrier.Location = new System.Drawing.Point(23, 120);
            this.udTimeBarrier.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udTimeBarrier.Name = "udTimeBarrier";
            this.udTimeBarrier.Size = new System.Drawing.Size(40, 20);
            this.udTimeBarrier.TabIndex = 4;
            this.udTimeBarrier.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.udTimeBarrier.ValueChanged += new System.EventHandler(this.udTimeBarrier_ValueChanged);
            // 
            // udTimeCardV
            // 
            this.udTimeCardV.Location = new System.Drawing.Point(23, 54);
            this.udTimeCardV.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.udTimeCardV.Name = "udTimeCardV";
            this.udTimeCardV.Size = new System.Drawing.Size(40, 20);
            this.udTimeCardV.TabIndex = 2;
            this.udTimeCardV.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.udTimeCardV.ValueChanged += new System.EventHandler(this.udTimeCardV_ValueChanged);
            // 
            // tabCheck
            // 
            this.tabCheck.Controls.Add(this.buttonStart);
            this.tabCheck.Controls.Add(this.buttonRun);
            this.tabCheck.Controls.Add(this.labelAudioIn);
            this.tabCheck.Controls.Add(this.dataGridView1);
            this.tabCheck.Controls.Add(this.labelTimerСhecker);
            this.tabCheck.Location = new System.Drawing.Point(4, 22);
            this.tabCheck.Name = "tabCheck";
            this.tabCheck.Size = new System.Drawing.Size(782, 539);
            this.tabCheck.TabIndex = 5;
            this.tabCheck.Text = "ПРОГРАММА ПРОВЕРКИ";
            this.tabCheck.UseVisualStyleBackColor = true;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(0, 0);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 0;
            // 
            // buttonRun
            // 
            this.buttonRun.Location = new System.Drawing.Point(0, 0);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(75, 23);
            this.buttonRun.TabIndex = 1;
            // 
            // labelAudioIn
            // 
            this.labelAudioIn.AutoSize = true;
            this.labelAudioIn.Location = new System.Drawing.Point(392, 24);
            this.labelAudioIn.Name = "labelAudioIn";
            this.labelAudioIn.Size = new System.Drawing.Size(45, 13);
            this.labelAudioIn.TabIndex = 3;
            this.labelAudioIn.Text = "Level In";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 150);
            this.dataGridView1.TabIndex = 4;
            // 
            // labelTimerСhecker
            // 
            this.labelTimerСhecker.AutoSize = true;
            this.labelTimerСhecker.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.labelTimerСhecker.Location = new System.Drawing.Point(599, 24);
            this.labelTimerСhecker.Name = "labelTimerСhecker";
            this.labelTimerСhecker.Size = new System.Drawing.Size(42, 26);
            this.labelTimerСhecker.TabIndex = 1;
            this.labelTimerСhecker.Text = "15 ";
            this.labelTimerСhecker.Click += new System.EventHandler(this.labelTimerСhecker_Click);
            // 
            // tabDebug
            // 
            this.tabDebug.Controls.Add(this.checkBoxDop);
            this.tabDebug.Controls.Add(this.cboDop);
            this.tabDebug.Controls.Add(this.SoftVersion);
            this.tabDebug.Controls.Add(this.panelDebugH);
            this.tabDebug.Controls.Add(this.chkItog);
            this.tabDebug.Controls.Add(this.label27);
            this.tabDebug.Controls.Add(this.chkBarcodeDebug);
            this.tabDebug.Controls.Add(this.chkDispensDebug);
            this.tabDebug.Controls.Add(this.chkReaderInDebug);
            this.tabDebug.Controls.Add(this.chkReaderOutDebug);
            this.tabDebug.Controls.Add(this.chkSlaveDebug);
            this.tabDebug.Controls.Add(this.chkDebug);
            this.tabDebug.Controls.Add(this.txtPass);
            this.tabDebug.Location = new System.Drawing.Point(4, 22);
            this.tabDebug.Name = "tabDebug";
            this.tabDebug.Padding = new System.Windows.Forms.Padding(3);
            this.tabDebug.Size = new System.Drawing.Size(782, 539);
            this.tabDebug.TabIndex = 3;
            this.tabDebug.Text = "ОТЛАДКА";
            this.tabDebug.UseVisualStyleBackColor = true;
            this.tabDebug.Click += new System.EventHandler(this.tabPageDebug_Click);
            // 
            // checkBoxDop
            // 
            this.checkBoxDop.AutoSize = true;
            this.checkBoxDop.Checked = true;
            this.checkBoxDop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDop.Location = new System.Drawing.Point(605, 439);
            this.checkBoxDop.Name = "checkBoxDop";
            this.checkBoxDop.Size = new System.Drawing.Size(111, 17);
            this.checkBoxDop.TabIndex = 52;
            this.checkBoxDop.Text = "Карты в корзину";
            this.checkBoxDop.UseVisualStyleBackColor = true;
            // 
            // cboDop
            // 
            this.cboDop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDop.FormattingEnabled = true;
            this.cboDop.Items.AddRange(new object[] {
            "Нет доп функций",
            "Инициализация",
            "Деинициализация"});
            this.cboDop.Location = new System.Drawing.Point(605, 396);
            this.cboDop.Name = "cboDop";
            this.cboDop.Size = new System.Drawing.Size(121, 21);
            this.cboDop.TabIndex = 51;
            // 
            // SoftVersion
            // 
            this.SoftVersion.AutoSize = true;
            this.SoftVersion.Location = new System.Drawing.Point(627, 303);
            this.SoftVersion.Name = "SoftVersion";
            this.SoftVersion.Size = new System.Drawing.Size(91, 13);
            this.SoftVersion.TabIndex = 49;
            this.SoftVersion.Text = "Stoyka01-160402";
            // 
            // panelDebugH
            // 
            this.panelDebugH.Controls.Add(this.textBoxUITest);
            this.panelDebugH.Controls.Add(this.labelCardExitWrite);
            this.panelDebugH.Controls.Add(this.txtCardKey);
            this.panelDebugH.Controls.Add(this.textBoxName);
            this.panelDebugH.Controls.Add(this.txtLicenseDebug);
            this.panelDebugH.Controls.Add(this.labelLanguage);
            this.panelDebugH.Controls.Add(this.labelSignatureSlave);
            this.panelDebugH.Controls.Add(this.buttonClearDevice);
            this.panelDebugH.Controls.Add(this.labelTopMost);
            this.panelDebugH.Controls.Add(this.lblGroupFromFile);
            this.panelDebugH.Controls.Add(this.cboHronom);
            this.panelDebugH.Controls.Add(this.cboAlarmW);
            this.panelDebugH.Controls.Add(this.cboClientType);
            this.panelDebugH.Controls.Add(this.labelNameSave);
            this.panelDebugH.Controls.Add(this.ServerUrl);
            this.panelDebugH.Controls.Add(this.label33);
            this.panelDebugH.Controls.Add(this.lblSaveKey);
            this.panelDebugH.Controls.Add(this.label29);
            this.panelDebugH.Controls.Add(this.label36);
            this.panelDebugH.Location = new System.Drawing.Point(41, 34);
            this.panelDebugH.Name = "panelDebugH";
            this.panelDebugH.Size = new System.Drawing.Size(380, 400);
            this.panelDebugH.TabIndex = 0;
            this.panelDebugH.Visible = false;
            // 
            // textBoxUITest
            // 
            this.textBoxUITest.Location = new System.Drawing.Point(0, 0);
            this.textBoxUITest.Name = "textBoxUITest";
            this.textBoxUITest.Size = new System.Drawing.Size(100, 20);
            this.textBoxUITest.TabIndex = 0;
            // 
            // labelCardExitWrite
            // 
            this.labelCardExitWrite.Location = new System.Drawing.Point(0, 0);
            this.labelCardExitWrite.Name = "labelCardExitWrite";
            this.labelCardExitWrite.Size = new System.Drawing.Size(100, 23);
            this.labelCardExitWrite.TabIndex = 1;
            // 
            // txtCardKey
            // 
            this.txtCardKey.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCardKey.Location = new System.Drawing.Point(21, 66);
            this.txtCardKey.MaxLength = 12;
            this.txtCardKey.Name = "txtCardKey";
            this.txtCardKey.Size = new System.Drawing.Size(96, 20);
            this.txtCardKey.TabIndex = 40;
            this.txtCardKey.Text = "FFFFFFFFFFFF";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(199, 320);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(178, 20);
            this.textBoxName.TabIndex = 55;
            // 
            // txtLicenseDebug
            // 
            this.txtLicenseDebug.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLicenseDebug.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.txtLicenseDebug.Location = new System.Drawing.Point(179, 215);
            this.txtLicenseDebug.Name = "txtLicenseDebug";
            this.txtLicenseDebug.PasswordChar = '*';
            this.txtLicenseDebug.Size = new System.Drawing.Size(120, 19);
            this.txtLicenseDebug.TabIndex = 54;
            this.txtLicenseDebug.TextChanged += new System.EventHandler(this.txtLicenseDebug_TextChanged);
            // 
            // labelLanguage
            // 
            this.labelLanguage.AutoSize = true;
            this.labelLanguage.Location = new System.Drawing.Point(23, 270);
            this.labelLanguage.Name = "labelLanguage";
            this.labelLanguage.Size = new System.Drawing.Size(57, 13);
            this.labelLanguage.TabIndex = 53;
            this.labelLanguage.Text = "language1";
            // 
            // labelSignatureSlave
            // 
            this.labelSignatureSlave.AutoSize = true;
            this.labelSignatureSlave.Location = new System.Drawing.Point(21, 246);
            this.labelSignatureSlave.Name = "labelSignatureSlave";
            this.labelSignatureSlave.Size = new System.Drawing.Size(79, 13);
            this.labelSignatureSlave.TabIndex = 52;
            this.labelSignatureSlave.Text = "SignatureSlave";
            // 
            // buttonClearDevice
            // 
            this.buttonClearDevice.Location = new System.Drawing.Point(21, 215);
            this.buttonClearDevice.Name = "buttonClearDevice";
            this.buttonClearDevice.Size = new System.Drawing.Size(75, 23);
            this.buttonClearDevice.TabIndex = 51;
            this.buttonClearDevice.Text = "Чистка базы";
            this.buttonClearDevice.UseVisualStyleBackColor = true;
            this.buttonClearDevice.Click += new System.EventHandler(this.buttonClearDevice_Click);
            // 
            // labelTopMost
            // 
            this.labelTopMost.AutoSize = true;
            this.labelTopMost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelTopMost.Location = new System.Drawing.Point(21, 137);
            this.labelTopMost.Name = "labelTopMost";
            this.labelTopMost.Size = new System.Drawing.Size(127, 15);
            this.labelTopMost.TabIndex = 50;
            this.labelTopMost.Text = "Делать окно не поверх";
            this.labelTopMost.Click += new System.EventHandler(this.labelTopMost_Click);
            // 
            // lblGroupFromFile
            // 
            this.lblGroupFromFile.AutoSize = true;
            this.lblGroupFromFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGroupFromFile.Location = new System.Drawing.Point(21, 100);
            this.lblGroupFromFile.Name = "lblGroupFromFile";
            this.lblGroupFromFile.Size = new System.Drawing.Size(96, 15);
            this.lblGroupFromFile.TabIndex = 49;
            this.lblGroupFromFile.Text = "Группы из файла";
            this.lblGroupFromFile.Click += new System.EventHandler(this.labelGrup_Click);
            // 
            // cboHronom
            // 
            this.cboHronom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHronom.FormattingEnabled = true;
            this.cboHronom.Items.AddRange(new object[] {
            "Не писать хронометраж",
            "Писать хронометраж"});
            this.cboHronom.Location = new System.Drawing.Point(179, 66);
            this.cboHronom.Name = "cboHronom";
            this.cboHronom.Size = new System.Drawing.Size(121, 21);
            this.cboHronom.TabIndex = 47;
            // 
            // cboAlarmW
            // 
            this.cboAlarmW.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAlarmW.FormattingEnabled = true;
            this.cboAlarmW.Items.AddRange(new object[] {
            "Тревоги писать",
            "Не писать тревоги"});
            this.cboAlarmW.Location = new System.Drawing.Point(179, 27);
            this.cboAlarmW.Name = "cboAlarmW";
            this.cboAlarmW.Size = new System.Drawing.Size(121, 21);
            this.cboAlarmW.TabIndex = 46;
            // 
            // cboClientType
            // 
            this.cboClientType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboClientType.FormattingEnabled = true;
            this.cboClientType.Items.AddRange(new object[] {
            "Разовый",
            "Постоянный",
            "...",
            "Вездеход"});
            this.cboClientType.Location = new System.Drawing.Point(179, 104);
            this.cboClientType.Name = "cboClientType";
            this.cboClientType.Size = new System.Drawing.Size(121, 21);
            this.cboClientType.TabIndex = 45;
            // 
            // labelNameSave
            // 
            this.labelNameSave.AutoSize = true;
            this.labelNameSave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNameSave.Location = new System.Drawing.Point(257, 292);
            this.labelNameSave.Name = "labelNameSave";
            this.labelNameSave.Size = new System.Drawing.Size(92, 15);
            this.labelNameSave.TabIndex = 44;
            this.labelNameSave.Text = "сохранить Name";
            this.labelNameSave.Click += new System.EventHandler(this.labelNameSave_Click);
            // 
            // ServerUrl
            // 
            this.ServerUrl.Location = new System.Drawing.Point(26, 319);
            this.ServerUrl.Name = "ServerUrl";
            this.ServerUrl.Size = new System.Drawing.Size(153, 20);
            this.ServerUrl.TabIndex = 43;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(21, 294);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(77, 13);
            this.label33.TabIndex = 42;
            this.label33.Text = "URL сервера:";
            // 
            // lblSaveKey
            // 
            this.lblSaveKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSaveKey.Location = new System.Drawing.Point(56, 46);
            this.lblSaveKey.Name = "lblSaveKey";
            this.lblSaveKey.Size = new System.Drawing.Size(61, 20);
            this.lblSaveKey.TabIndex = 41;
            this.lblSaveKey.Text = "сохранить";
            this.lblSaveKey.Click += new System.EventHandler(this.labelKeySave_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(18, 47);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(33, 13);
            this.label29.TabIndex = 39;
            this.label29.Text = "Ключ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(39, 16);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(59, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "Всё для ...";
            // 
            // chkItog
            // 
            this.chkItog.AutoSize = true;
            this.chkItog.Location = new System.Drawing.Point(629, 230);
            this.chkItog.Name = "chkItog";
            this.chkItog.Size = new System.Drawing.Size(97, 17);
            this.chkItog.TabIndex = 48;
            this.chkItog.Text = "Итог по клику";
            this.chkItog.UseVisualStyleBackColor = true;
            this.chkItog.CheckedChanged += new System.EventHandler(this.chkItog_CheckedChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(617, 57);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(121, 26);
            this.label27.TabIndex = 47;
            this.label27.Text = "Имитация, если\r\nустройство отключено";
            // 
            // chkBarcodeDebug
            // 
            this.chkBarcodeDebug.AutoSize = true;
            this.chkBarcodeDebug.Location = new System.Drawing.Point(630, 182);
            this.chkBarcodeDebug.Name = "chkBarcodeDebug";
            this.chkBarcodeDebug.Size = new System.Drawing.Size(75, 17);
            this.chkBarcodeDebug.TabIndex = 46;
            this.chkBarcodeDebug.Text = "Штрихкод";
            this.chkBarcodeDebug.UseVisualStyleBackColor = true;
            this.chkBarcodeDebug.CheckedChanged += new System.EventHandler(this.chkBarcodeDebug_CheckedChanged);
            // 
            // chkDispensDebug
            // 
            this.chkDispensDebug.AutoSize = true;
            this.chkDispensDebug.Location = new System.Drawing.Point(630, 158);
            this.chkDispensDebug.Name = "chkDispensDebug";
            this.chkDispensDebug.Size = new System.Drawing.Size(83, 17);
            this.chkDispensDebug.TabIndex = 45;
            this.chkDispensDebug.Text = "Диспенсер";
            this.chkDispensDebug.UseVisualStyleBackColor = true;
            this.chkDispensDebug.CheckedChanged += new System.EventHandler(this.chkDispensDebug_CheckedChanged);
            // 
            // chkReaderInDebug
            // 
            this.chkReaderInDebug.AutoSize = true;
            this.chkReaderInDebug.Location = new System.Drawing.Point(630, 134);
            this.chkReaderInDebug.Name = "chkReaderInDebug";
            this.chkReaderInDebug.Size = new System.Drawing.Size(94, 17);
            this.chkReaderInDebug.TabIndex = 44;
            this.chkReaderInDebug.Text = "Ридер внутри";
            this.chkReaderInDebug.UseVisualStyleBackColor = true;
            this.chkReaderInDebug.CheckedChanged += new System.EventHandler(this.chkReaderInDebug_CheckedChanged);
            // 
            // chkReaderOutDebug
            // 
            this.chkReaderOutDebug.AutoSize = true;
            this.chkReaderOutDebug.Location = new System.Drawing.Point(630, 110);
            this.chkReaderOutDebug.Name = "chkReaderOutDebug";
            this.chkReaderOutDebug.Size = new System.Drawing.Size(104, 17);
            this.chkReaderOutDebug.TabIndex = 43;
            this.chkReaderOutDebug.Text = "Ридер внешний";
            this.chkReaderOutDebug.UseVisualStyleBackColor = true;
            this.chkReaderOutDebug.CheckedChanged += new System.EventHandler(this.chkReaderOutDebug_CheckedChanged);
            // 
            // chkSlaveDebug
            // 
            this.chkSlaveDebug.AutoSize = true;
            this.chkSlaveDebug.Location = new System.Drawing.Point(630, 86);
            this.chkSlaveDebug.Name = "chkSlaveDebug";
            this.chkSlaveDebug.Size = new System.Drawing.Size(53, 17);
            this.chkSlaveDebug.TabIndex = 42;
            this.chkSlaveDebug.Text = "Slave";
            this.chkSlaveDebug.UseVisualStyleBackColor = true;
            this.chkSlaveDebug.CheckedChanged += new System.EventHandler(this.chkSlaveDebug_CheckedChanged);
            // 
            // chkDebug
            // 
            this.chkDebug.AutoSize = true;
            this.chkDebug.Location = new System.Drawing.Point(630, 34);
            this.chkDebug.Name = "chkDebug";
            this.chkDebug.Size = new System.Drawing.Size(69, 17);
            this.chkDebug.TabIndex = 41;
            this.chkDebug.Text = "Отладка";
            this.chkDebug.UseVisualStyleBackColor = true;
            this.chkDebug.CheckedChanged += new System.EventHandler(this.chkDebug_CheckedChanged);
            // 
            // txtPass
            // 
            this.txtPass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPass.Location = new System.Drawing.Point(626, 269);
            this.txtPass.MaxLength = 12;
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(100, 13);
            this.txtPass.TabIndex = 40;
            this.txtPass.TextChanged += new System.EventHandler(this.textBoxPass_TextChanged);
            // 
            // labelLicenseWork
            // 
            this.labelLicenseWork.AutoSize = true;
            this.labelLicenseWork.Location = new System.Drawing.Point(467, 12);
            this.labelLicenseWork.Name = "labelLicenseWork";
            this.labelLicenseWork.Size = new System.Drawing.Size(76, 13);
            this.labelLicenseWork.TabIndex = 10;
            this.labelLicenseWork.Text = "LicenseWork?";
            // 
            // labelErrBD
            // 
            this.labelErrBD.AutoSize = true;
            this.labelErrBD.Location = new System.Drawing.Point(257, 12);
            this.labelErrBD.Name = "labelErrBD";
            this.labelErrBD.Size = new System.Drawing.Size(0, 13);
            this.labelErrBD.TabIndex = 9;
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(734, 8);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(38, 30);
            this.buttonExit.TabIndex = 8;
            this.buttonExit.Text = "X";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonMax
            // 
            this.buttonMax.AutoSize = true;
            this.buttonMax.Location = new System.Drawing.Point(690, 8);
            this.buttonMax.Name = "buttonMax";
            this.buttonMax.Size = new System.Drawing.Size(38, 30);
            this.buttonMax.TabIndex = 7;
            this.buttonMax.Text = "<>";
            this.buttonMax.UseVisualStyleBackColor = true;
            this.buttonMax.Click += new System.EventHandler(this.buttonMax_Click);
            // 
            // timer_serv
            // 
            this.timer_serv.AutoSize = true;
            this.timer_serv.Location = new System.Drawing.Point(93, 12);
            this.timer_serv.Name = "timer_serv";
            this.timer_serv.Size = new System.Drawing.Size(55, 13);
            this.timer_serv.TabIndex = 4;
            this.timer_serv.Text = "timer_serv";
            // 
            // ButtonMinimiz
            // 
            this.ButtonMinimiz.AutoSize = true;
            this.ButtonMinimiz.Location = new System.Drawing.Point(644, 8);
            this.ButtonMinimiz.Name = "ButtonMinimiz";
            this.ButtonMinimiz.Size = new System.Drawing.Size(38, 30);
            this.ButtonMinimiz.TabIndex = 2;
            this.ButtonMinimiz.Text = "__";
            this.ButtonMinimiz.UseVisualStyleBackColor = true;
            this.ButtonMinimiz.Click += new System.EventHandler(this.ButtonMinimiz_Click);
            // 
            // to_klient
            // 
            this.to_klient.AutoSize = true;
            this.to_klient.Location = new System.Drawing.Point(598, 8);
            this.to_klient.Name = "to_klient";
            this.to_klient.Size = new System.Drawing.Size(38, 30);
            this.to_klient.TabIndex = 1;
            this.to_klient.Text = "<--";
            this.to_klient.UseVisualStyleBackColor = true;
            this.to_klient.Click += new System.EventHandler(this.to_klient_Click);
            // 
            // service
            // 
            this.service.AutoSize = true;
            this.service.Location = new System.Drawing.Point(11, 12);
            this.service.Name = "service";
            this.service.Size = new System.Drawing.Size(53, 13);
            this.service.TabIndex = 0;
            this.service.Text = "СЕРВИС ";
            // 
            // Column1
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Column1.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "Шаг";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 40;
            // 
            // Column2
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Column2.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column2.Frozen = true;
            this.Column2.HeaderText = "Действие";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 650;
            // 
            // Column3
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Column3.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "Итог";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Width = 50;
            // 
            // Com1
            // 
            this.Com1.Filter = "*.json|Json Files";
            // 
            // tmrOmnikey
            // 
            this.tmrOmnikey.Enabled = true;
            this.tmrOmnikey.Interval = 10000;
            this.tmrOmnikey.Tick += new System.EventHandler(this.tmrOmnikey_Tick);
            // 
            // tmrLoopANotification
            // 
            this.tmrLoopANotification.Enabled = true;
            this.tmrLoopANotification.Interval = 500;
            this.tmrLoopANotification.Tick += new System.EventHandler(this.tmrLoopANotification_Tick);
            // 
            // fold1
            // 
            this.fold1.SelectedPath = "c:\\";
            // 
            // tmrButtonBlinking
            // 
            this.tmrButtonBlinking.Interval = 500;
            this.tmrButtonBlinking.Tick += new System.EventHandler(this.tmrButtonBlinking_Tick);
            // 
            // tmrCameraAvailable
            // 
            this.tmrCameraAvailable.Enabled = true;
            this.tmrCameraAvailable.Interval = 30000;
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(788, 588);
            this.ControlBox = false;
            this.Controls.Add(this.klient);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "stoyka";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.klient.ResumeLayout(false);
            this.klient.PerformLayout();
            this.debug.ResumeLayout(false);
            this.debug.PerformLayout();
            this.CardReadP.ResumeLayout(false);
            this.CardReadP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDnIdTp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDnZonaC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDnIdTcC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDnGroupC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.texmenu.ResumeLayout(false);
            this.texmenu.PerformLayout();
            this.tabControlService.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabDiag.ResumeLayout(false);
            this.tabDiagStatus.ResumeLayout(false);
            this.tabDiagStatus.PerformLayout();
            this.tabDiagReaderOut.ResumeLayout(false);
            this.grOmnikey5427OutReader.ResumeLayout(false);
            this.grOmnikey5427OutReader.PerformLayout();
            this.grMT625OutReader.ResumeLayout(false);
            this.grMT625OutReader.PerformLayout();
            this.tabDiagReaderIn.ResumeLayout(false);
            this.tabDiagReaderIn.PerformLayout();
            this.tabDiagDispenser.ResumeLayout(false);
            this.grMT166.ResumeLayout(false);
            this.grMT166.PerformLayout();
            this.grKYT2064.ResumeLayout(false);
            this.grKYT2064.PerformLayout();
            this.tabDiagBarcode.ResumeLayout(false);
            this.tabDiagBarcode.PerformLayout();
            this.tabDiagSlave.ResumeLayout(false);
            this.tabDiagSlave.PerformLayout();
            this.tabDiagTablo.ResumeLayout(false);
            this.grTablo3.ResumeLayout(false);
            this.grTablo3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udUDPTabloGreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udUDPTabloYellow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udUDPTabloRed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udUDPTabloColor)).EndInit();
            this.grTablo2.ResumeLayout(false);
            this.grTablo2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udTabloAddress)).EndInit();
            this.grTablo1.ResumeLayout(false);
            this.grTablo1.PerformLayout();
            this.tabDiagWebServer.ResumeLayout(false);
            this.tabDiagWebServer.PerformLayout();
            this.tabDiagBankModule.ResumeLayout(false);
            this.tabDiagBankModule.PerformLayout();
            this.tabDiagRFID.ResumeLayout(false);
            this.tabDiagRFID.PerformLayout();
            this.tabDiagCollector.ResumeLayout(false);
            this.tabDiagCollector.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabMainSettings.ResumeLayout(false);
            this.tabMainSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udKKMNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComCollector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComRFID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udZoneBefore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udZoneAfter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComSlave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComReadOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComReadIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComDispenser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComBarcode)).EndInit();
            this.tabPage15.ResumeLayout(false);
            this.tabPage15.PerformLayout();
            this.grTwoLevelRackRecognition.ResumeLayout(false);
            this.grTwoLevelRackRecognition.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.grCameraType.ResumeLayout(false);
            this.grCameraType.PerformLayout();
            this.grTrans.ResumeLayout(false);
            this.grTrans.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.grSavePhotoLocal.ResumeLayout(false);
            this.grSavePhotoLocal.PerformLayout();
            this.grEnableLoopANotification.ResumeLayout(false);
            this.grEnableLoopANotification.PerformLayout();
            this.grViinexMain.ResumeLayout(false);
            this.grViinexMain.PerformLayout();
            this.pnlVision.ResumeLayout(false);
            this.pnlVision.PerformLayout();
            this.groupBoxBlackList.ResumeLayout(false);
            this.groupBoxBlackList.PerformLayout();
            this.groupBoxInOut.ResumeLayout(false);
            this.groupBoxInOut.PerformLayout();
            this.tabAddSettings.ResumeLayout(false);
            this.tabAddSettings.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udLoopAAlarmsTime)).EndInit();
            this.grReverseMode.ResumeLayout(false);
            this.grReverseMode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udAddFreeTime)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn3AltimetrTariffId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn2AltimetrTariffId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn1AltimetrTariffId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn3AltimetrScheduleId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn2AltimetrScheduleId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDn1AltimetrScheduleId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udIR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udLoopB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udLoopA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udGrupRaz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTarifPlanIdRazov)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTimeNotIr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTimeNotLoopA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTarifIdRazov0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNumberSector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTimeBarrier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTimeCardV)).EndInit();
            this.tabCheck.ResumeLayout(false);
            this.tabCheck.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabDebug.ResumeLayout(false);
            this.tabDebug.PerformLayout();
            this.panelDebugH.ResumeLayout(false);
            this.panelDebugH.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel klient;
        private System.Windows.Forms.Panel texmenu;
        private System.Windows.Forms.Label labelMessage1;
        private System.Windows.Forms.Button to_klient;
        private System.Windows.Forms.Label service;
        private System.Windows.Forms.Label RPS_1;
        private System.Windows.Forms.Label labelMessage2;
        private System.Windows.Forms.Label time;
        private System.Windows.Forms.Panel debug;
        private System.Windows.Forms.Label trafficLight;
        private System.Windows.Forms.Label barrier;
        private System.Windows.Forms.Label IR;
        private System.Windows.Forms.Label loopB;
        private System.Windows.Forms.Label loopA;
        private System.Windows.Forms.Button ButtonMinimiz;
        private System.Windows.Forms.Label reader_out;
        private System.Windows.Forms.Label reader_int;
        private System.Windows.Forms.Label DISPENS;
        private System.Windows.Forms.Label PUSH;
        private System.Windows.Forms.Timer timer50;
        private System.Windows.Forms.Label timer_serv;
        private System.Windows.Forms.Label labelEtap;
        private System.Windows.Forms.Button buttonDebugExit;
        private System.Windows.Forms.Label comToReadOut;
        private System.Windows.Forms.Label comToReadIn;
        private System.Windows.Forms.Label comToDispens;
        private System.Windows.Forms.ComboBox statReadOut;
        private System.Windows.Forms.ComboBox statReadIn;
        private System.Windows.Forms.ComboBox statDispens;
        private System.Windows.Forms.Panel CardReadP;
        private System.Windows.Forms.TextBox IdCard;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numericUpDnGroupC;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown numericUpDnIdTcC;
        private System.Windows.Forms.NumericUpDown numericUpDnZonaC;
        private System.Windows.Forms.TextBox BalanceC;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.NumericUpDown numericUpDnIdTp;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox sItog;
        private System.Windows.Forms.Label labelItog;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label labelSlave;
        private System.Windows.Forms.Label labelReaderOut;
        private System.Windows.Forms.Label labelDispens;
        private System.Windows.Forms.Label labelReaderIn;
        private System.Windows.Forms.Label labelStatSlave;
        private System.Windows.Forms.Label labelPropuskAvto;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonMax;
        private System.Windows.Forms.TabControl tabControlService;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabDiag;
        private System.Windows.Forms.TabPage tabDiagStatus;
        private System.Windows.Forms.Label labelStstusOut;
        private System.Windows.Forms.TabPage tabDiagReaderOut;
        private System.Windows.Forms.TabPage tabDiagReaderIn;
        private System.Windows.Forms.Label labelGetIdIn;
        private System.Windows.Forms.Label InGetCard;
        private System.Windows.Forms.Label labelInIdCard;
        private System.Windows.Forms.Label noCardIn;
        private System.Windows.Forms.Label labelRxIn;
        private System.Windows.Forms.Label labelComIn;
        private System.Windows.Forms.TabPage tabDiagDispenser;
        private System.Windows.Forms.TabPage tabDiagBarcode;
        private System.Windows.Forms.TabPage tabDiagSlave;
        private System.Windows.Forms.Label labelComSlave;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cboPortDiscret2;
        private System.Windows.Forms.ComboBox cboPortDiscret1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboRackType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown udComBarcode;
        private System.Windows.Forms.NumericUpDown udComDispenser;
        private System.Windows.Forms.NumericUpDown udComReadIn;
        private System.Windows.Forms.NumericUpDown udComReadOut;
        private System.Windows.Forms.NumericUpDown udComSlave;
        private System.Windows.Forms.CheckBox chkDisplay;
        private System.Windows.Forms.CheckBox chkBarcode;
        private System.Windows.Forms.CheckBox chkDispenser;
        private System.Windows.Forms.CheckBox chkReaderIn;
        private System.Windows.Forms.CheckBox chkReaderOut;
        private System.Windows.Forms.CheckBox chkSlave;
        private System.Windows.Forms.NumericUpDown udZoneAfter;
        private System.Windows.Forms.NumericUpDown udZoneBefore;
        private System.Windows.Forms.TabPage tabAddSettings;
        private System.Windows.Forms.NumericUpDown udGrupRaz;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox cbologNastroy;
        private System.Windows.Forms.NumericUpDown udTarifPlanIdRazov;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown udTimeNotIr;
        private System.Windows.Forms.NumericUpDown udTimeNotLoopA;
        private System.Windows.Forms.NumericUpDown udTarifIdRazov0;
        private System.Windows.Forms.NumericUpDown udNumberSector;
        private System.Windows.Forms.NumericUpDown udTimeBarrier;
        private System.Windows.Forms.NumericUpDown udTimeCardV;
        public System.Windows.Forms.TabPage tabDebug;
        private System.Windows.Forms.Panel panelDebugH;
        private System.Windows.Forms.ComboBox cboClientType;
        private System.Windows.Forms.Label labelNameSave;
        private System.Windows.Forms.TextBox ServerUrl;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label lblSaveKey;
        private System.Windows.Forms.TextBox txtCardKey;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.CheckBox chkItog;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.CheckBox chkBarcodeDebug;
        private System.Windows.Forms.CheckBox chkDispensDebug;
        private System.Windows.Forms.CheckBox chkReaderInDebug;
        private System.Windows.Forms.CheckBox chkReaderOutDebug;
        private System.Windows.Forms.CheckBox chkSlaveDebug;
        private System.Windows.Forms.CheckBox chkDebug;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.ComboBox cboAlarmW;
        private System.Windows.Forms.ComboBox cboHronom;
        private System.Windows.Forms.Label lblGroupFromFile;
        private System.Windows.Forms.ComboBox cboRedNotCard;
        private System.Windows.Forms.ComboBox comboBoxClientTypCard;
        private System.Windows.Forms.Label labelStatDevice;
        private System.Windows.Forms.ComboBox cboZoneBefore;
        private System.Windows.Forms.ComboBox cboZoneAfter;
        private System.Windows.Forms.ComboBox comboBoxTPRaz;
        private System.Windows.Forms.ComboBox comboBoxTSRaz;
        private System.Windows.Forms.ComboBox comboBoxGrupRaz;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_barrierUp;
        private System.Windows.Forms.Label label_barrierDn;
        private System.Windows.Forms.Label label_lightRed;
        private System.Windows.Forms.Label label_lightGrin;
        private System.Windows.Forms.Label label_Call;
        private System.Windows.Forms.Label labelStatusOut2;
        private System.Windows.Forms.Label labelTopMost;
        private System.Windows.Forms.TabPage tabDiagWebServer;
        private System.Windows.Forms.Label labelWebServer;
        private System.Windows.Forms.Button buttonClearDevice;
        private System.Windows.Forms.Label labelSignatureSlave;
        private System.Windows.Forms.Label labelVersionSlave;
        private System.Windows.Forms.Label labelLanguage;
        private System.Windows.Forms.TextBox txtLicenseDebug;
        private System.Windows.Forms.NumericUpDown udIR;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.NumericUpDown udLoopB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown udLoopA;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label SoftVersion;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labeletapReadCard;
        private System.Windows.Forms.Label labelErrBD;
        private System.Windows.Forms.TabPage tabCheck;
        private System.Windows.Forms.Label labelLicenseWork;
        private System.Windows.Forms.Label labelTimerСhecker;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Label labelDateExitEstimated;
        private System.Windows.Forms.Label labelAudioIn;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.NumericUpDown udComDisplay;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cboPortDiscret3;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TabPage tabDiagTablo;
        private System.Windows.Forms.Label labelComBarcode;
        private System.Windows.Forms.Button buttonStartDecoding;
        private System.Windows.Forms.Button buttonStopDecoding;
        private System.Windows.Forms.Button buttonReversON;
        private System.Windows.Forms.Button buttonReversOFF;
        private System.Windows.Forms.Label labelQRcode;
        private System.Windows.Forms.Label labelBarcodeD;
        private System.Windows.Forms.CheckBox chkBankModule;
        private System.Windows.Forms.TabPage tabDiagBankModule;
        private System.Windows.Forms.Label labelBankModuleStatus;
        private System.Windows.Forms.Button buttonBankModuleOpen;
        private System.Windows.Forms.Button buttonBankModulePay;
        private System.Windows.Forms.Button buttonBankModuleClose;
        private System.Windows.Forms.Button buttonBankModuleCancell;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button buttonVerification;
        private System.Windows.Forms.Button buttonEchoTest;
        private System.Windows.Forms.Button buttonWorkKey;
        private System.Windows.Forms.Button buttonTerminalInfo;
        private System.Windows.Forms.Button buttonLoadSW;
        private System.Windows.Forms.Button buttonLoadParams;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox cbo3AltimetrTariff;
        private System.Windows.Forms.ComboBox cbo2AltimetrTariff;
        private System.Windows.Forms.ComboBox cboAltimetrTariff;
        private System.Windows.Forms.NumericUpDown numericUpDn3AltimetrTariffId;
        private System.Windows.Forms.NumericUpDown numericUpDn2AltimetrTariffId;
        private System.Windows.Forms.NumericUpDown numericUpDn1AltimetrTariffId;
        private System.Windows.Forms.ComboBox cbo3AltimetrSchedule;
        private System.Windows.Forms.ComboBox cbo2AltimetrSchedule;
        private System.Windows.Forms.ComboBox cboAltimetrSchedule;
        private System.Windows.Forms.NumericUpDown numericUpDn3AltimetrScheduleId;
        private System.Windows.Forms.NumericUpDown numericUpDn2AltimetrScheduleId;
        private System.Windows.Forms.NumericUpDown numericUpDn1AltimetrScheduleId;
        private System.Windows.Forms.ComboBox cbo3AltimetrModeId;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox cbo2AltimetrModeId;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox cboAltimetrModeId;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label labelInNewComand;
        private System.Windows.Forms.Label labelCardExitWrite;
        private System.Windows.Forms.Label labelMessage0;
        private System.Windows.Forms.Label labelMessage3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxUITest;
        private System.Windows.Forms.ComboBox comboBoxDebugUI;
        private System.Windows.Forms.TextBox textBoxViinexFile;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox label48;
        private System.Windows.Forms.Label labelviinex;
        private System.Windows.Forms.Label labelLastPlate;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBoxCameraIP;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox CameraBox;
        private System.Windows.Forms.Label labelMessage7;
        private System.Windows.Forms.Label labelMessage6;
        private System.Windows.Forms.Label labelMessage5;
        private System.Windows.Forms.Label labelMessage4;
        private System.Windows.Forms.Button buttonViinexFolder;
        private System.Windows.Forms.OpenFileDialog Com1;
        private System.Windows.Forms.ComboBox cboDop;
        private System.Windows.Forms.CheckBox checkBoxDop;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabMainSettings;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.Panel pnlVision;
        private System.Windows.Forms.GroupBox groupBoxBlackList;
        private System.Windows.Forms.RadioButton optBlackListOperator;
        private System.Windows.Forms.RadioButton optBlackListNone;
        private System.Windows.Forms.RadioButton optBlackListAllow;
        private System.Windows.Forms.RadioButton optBlackListNotAllow;
        private System.Windows.Forms.CheckBox chkBarcodePlate;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.CheckBox chkRegularPlate;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.GroupBox groupBoxInOut;
        private System.Windows.Forms.RadioButton optInOutOperator;
        private System.Windows.Forms.RadioButton optInOutNone;
        private System.Windows.Forms.RadioButton optInOutAllow;
        private System.Windows.Forms.RadioButton optInOutNotAllow;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TabPage tabDiagRFID;
        private System.Windows.Forms.Label labelWiegandResult;
        private System.Windows.Forms.Label labelWiegandStat;
        private System.Windows.Forms.Label labelComWiegand;
        private System.Windows.Forms.NumericUpDown udComRFID;
        private System.Windows.Forms.CheckBox chkRFID;
        private System.Windows.Forms.NumericUpDown udComCollector;
        private System.Windows.Forms.CheckBox chkCollector;
        private System.Windows.Forms.TabPage tabDiagCollector;
        private System.Windows.Forms.Label lblCollectorBack;
        private System.Windows.Forms.Label lblVersionCollector;
        private System.Windows.Forms.Label lblGetVersionCollector;
        private System.Windows.Forms.Label lblCollectorFront;
        private System.Windows.Forms.Label lblCollectorStatus;
        private System.Windows.Forms.Label lblEtapCollector;
        private System.Windows.Forms.Label lblCollectorRetain;
        private System.Windows.Forms.Label lblCollectorEject;
        private System.Windows.Forms.Label lblStatusCollector;
        private System.Windows.Forms.Label lblComCollector;
        private System.Windows.Forms.ComboBox cboRackMode;
        private System.Windows.Forms.ComboBox cboDispenserTypeId;
        private System.Windows.Forms.GroupBox grMT166;
        private System.Windows.Forms.Label lblRecycleToRead;
        private System.Windows.Forms.Label labelRxDisp;
        private System.Windows.Forms.Label labelVersionDispens;
        private System.Windows.Forms.Label lblGetVersion;
        private System.Windows.Forms.Label lblFromBezel;
        private System.Windows.Forms.Label labelDispensStatus;
        private System.Windows.Forms.Label labeletapDispensCard;
        private System.Windows.Forms.Label lblToRecycleBox;
        private System.Windows.Forms.Label lblToBezelAndNotHold;
        private System.Windows.Forms.Label lblToBezelAndHold;
        private System.Windows.Forms.Label lblFromBoxToRead;
        private System.Windows.Forms.Label lblDispensGetStatus;
        private System.Windows.Forms.Label labelComDispens;
        private System.Windows.Forms.ComboBox cboReadOutTypeId;
        private System.Windows.Forms.GroupBox grMT625OutReader;
        private System.Windows.Forms.Label labelOutNewComand;
        private System.Windows.Forms.Label labelRxOut;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label labelOutWrite;
        private System.Windows.Forms.Label labelOutRead;
        private System.Windows.Forms.Label labelOutKeyBok;
        private System.Windows.Forms.Label labelOutKeyB;
        private System.Windows.Forms.Label labelOutKeyAok;
        private System.Windows.Forms.Label labelOutKeyA;
        private System.Windows.Forms.Label labelOutIdCard;
        private System.Windows.Forms.Label labelGetId;
        private System.Windows.Forms.Label noCardOut;
        private System.Windows.Forms.Label OutGetCard;
        private System.Windows.Forms.Label labelComOut;
        private System.Windows.Forms.GroupBox grOmnikey5427OutReader;
        private System.Windows.Forms.Label lblOmniGetId;
        private System.Windows.Forms.Label lblOmniCardID;
        private System.Windows.Forms.Label lblOmniStatus;
        private System.Windows.Forms.Label lblOmniCard;
        private System.Windows.Forms.Label lblDeviceState;
        private System.Windows.Forms.Label lblEnableWrite;
        private System.Windows.Forms.Label lblEnableRead;
        private System.Windows.Forms.Label lblOmniEnabled;
        private System.Windows.Forms.Timer tmrOmnikey;
        private System.Windows.Forms.Label lblCollectorTimeoutOff;
        private System.Windows.Forms.GroupBox grKYT2064;
        private System.Windows.Forms.Label lblRecycleToRead2;
        private System.Windows.Forms.Label lblGetVersion2;
        private System.Windows.Forms.Label lblFromBezel2;
        private System.Windows.Forms.Label lblToRecycleBox2;
        private System.Windows.Forms.Label lblToBezelAndNotHold2;
        private System.Windows.Forms.Label lblToBezelAndHold2;
        private System.Windows.Forms.Label lblFromBoxToReader2;
        private System.Windows.Forms.Label lblDispensGetStatus2;
        private System.Windows.Forms.Label lblStFeedOutSol;
        private System.Windows.Forms.Label lblStAutoWait;
        private System.Windows.Forms.Label lblStStack2Wait;
        private System.Windows.Forms.Label lblStStack1Wait;
        private System.Windows.Forms.Label lblStStop;
        private System.Windows.Forms.Label lblStFeedOut;
        private System.Windows.Forms.Label lblStFeedIn;
        private System.Windows.Forms.Label lblStCapture;
        private System.Windows.Forms.Label lblStAutoIssue;
        private System.Windows.Forms.Label lblStStacker2Out;
        private System.Windows.Forms.Label lblDispenser2Status;
        private System.Windows.Forms.Label lblEtapDisp2;
        private System.Windows.Forms.Label lblStStacker1Out;
        private System.Windows.Forms.Label lblStStatus;
        private System.Windows.Forms.Label lblStClear;
        private System.Windows.Forms.Label lblComDispens2;
        private System.Windows.Forms.TextBox txtViinexLicenseKey;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button cmdUpdateViinexSettings;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox txtCameraPostProcess;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txtCameraPreProcess;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtCameraSkip;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox txtCameraPassword;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox txtCameraLogin;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox txtCameraURI;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.GroupBox grViinexMain;
        private System.Windows.Forms.Button cmdViinexSettingsPath;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtViinexSettingsPath;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.CheckBox chkViinex;
        private System.Windows.Forms.GroupBox grEnableLoopANotification;
        private System.Windows.Forms.CheckBox chkEnableLoopANotification;
        private System.Windows.Forms.TextBox txtLoopANotificationUri;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtLoopANotificationTimeout;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.GroupBox grSavePhotoLocal;
        private System.Windows.Forms.RadioButton optSavePhotoIfNotRecognized;
        private System.Windows.Forms.RadioButton optNoSavePhoto;
        private System.Windows.Forms.RadioButton optSavePhotoAlways;
        private System.Windows.Forms.Button cmdSavePhotoDir;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox txtSavePhotoDir;
        private System.Windows.Forms.TextBox txtSavePhotoTimeout;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Timer tmrLoopANotification;
        private System.Windows.Forms.FolderBrowserDialog fold1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkInvertCall;
        private System.Windows.Forms.CheckBox chkPUSH;
        private System.Windows.Forms.CheckBox chkIR;
        private System.Windows.Forms.CheckBox chkLoopB;
        private System.Windows.Forms.CheckBox chkLoopA;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkPlaceCompanyControl;
        private System.Windows.Forms.CheckBox chkBarcodeFromButton;
        private System.Windows.Forms.CheckBox chkDisableRazovButton;
        private System.Windows.Forms.CheckBox chkCardsOnline;
        private System.Windows.Forms.NumericUpDown udAddFreeTime;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox chkControlLoopB;
        private System.Windows.Forms.CheckBox chkNewAlarm;
        private System.Windows.Forms.CheckBox chkPlaceControlRazov;
        private System.Windows.Forms.GroupBox grTrans;
        private System.Windows.Forms.RadioButton optLat;
        private System.Windows.Forms.RadioButton optCyr;
        private System.Windows.Forms.TextBox txtViinexConfidence;
        private System.Windows.Forms.ComboBox cboTabloNew;
        private System.Windows.Forms.NumericUpDown udKKMNumber;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.GroupBox grTablo1;
        private System.Windows.Forms.GroupBox grTablo2;
        private System.Windows.Forms.TextBox txtTablo1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label labelComTablo;
        private System.Windows.Forms.GroupBox grTablo3;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown udTabloAddress;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox cboDisplayType;
        private System.Windows.Forms.NumericUpDown udUDPTabloColor;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox txtUDPTabloPort;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox txtUDPTabloIP;
        private System.Windows.Forms.TextBox txtTablo3;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox txtTablo2;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Button cmdSetTablo3Value;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.NumericUpDown udUDPTabloGreen;
        private System.Windows.Forms.NumericUpDown udUDPTabloYellow;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.NumericUpDown udUDPTabloRed;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.GroupBox grCameraType;
        private System.Windows.Forms.RadioButton optCameraOnvif;
        private System.Windows.Forms.RadioButton optCameraRTSP;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox chkAcquireEvents;
        private System.Windows.Forms.CheckBox chkAcquireVideo;
        private System.Windows.Forms.GroupBox grReverseMode;
        private System.Windows.Forms.RadioButton optTwoLevelReverse;
        private System.Windows.Forms.RadioButton optInOutReverse;
        private System.Windows.Forms.TextBox txtViinexGetURI;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox chkAcquireEvents2;
        private System.Windows.Forms.CheckBox chkAcquireVideo2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton optCameraOnvif2;
        private System.Windows.Forms.RadioButton optCameraRTSP2;
        private System.Windows.Forms.TextBox txtViinexConfidence2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton optLat2;
        private System.Windows.Forms.RadioButton optCyr2;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox txtCameraPostProcess2;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox txtCameraPreProcess2;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox txtCameraSkip2;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox txtCameraPassword2;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox txtCameraLogin2;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox txtCameraURI2;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.CheckBox chkCamera2Enabled;
        private System.Windows.Forms.GroupBox grTwoLevelRackRecognition;
        private System.Windows.Forms.RadioButton optTwoLevelDisabled;
        private System.Windows.Forms.RadioButton optTwoLevelSend;
        private System.Windows.Forms.RadioButton optTwoLevelReceive;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox txtTwoLevelSendIP;
        private System.Windows.Forms.CheckBox chkGetSnapshots;
        private System.Windows.Forms.CheckBox chkTrainReverse;
        private System.Windows.Forms.TextBox txtTimeBudget;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.CheckBox chkLevenIO;
        private System.Windows.Forms.CheckBox chkLevenBL;
        private System.Windows.Forms.CheckBox chkLevenQR;
        private System.Windows.Forms.CheckBox chkLevenPK;
        private System.Windows.Forms.CheckBox chkControlIO;
        private System.Windows.Forms.CheckBox chkControlBL;
        private System.Windows.Forms.Timer tmrButtonBlinking;
        private System.Windows.Forms.Timer tmrCameraAvailable;
        private System.Windows.Forms.NumericUpDown udLoopAAlarmsTime;
        private System.Windows.Forms.Label label57;
    }
}

