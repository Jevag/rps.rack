﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static stoyka01.GlobalClass;

namespace stoyka01
{
    public class Logging
    {
        public static void ToLog(string What) // запись в лог строки
        {
            //string[] logs = null;
            //int logNew = 0;             // номер посылки последней записи  в лог
            //int logOld = 0;             // номер записанной в файл посылки

            logs[logNew] = "";

            DateTime d = DateTime.Now;
            string ds = d.ToString("yyyy/MM/dd HH:mm:ss.fff");
            logs[logNew] += Environment.NewLine + ds + "  " + What;

            if (logNew < 200)     // номера от 0 до logBuf (500) включительно
            {
                if ((logNew + 1) != logOld) logNew++;
                else
                {
                    // пропала N-ая..
                }
            }
            else
            {
                if (logOld != 0) logNew = 0;
                else
                {
                    // пропала 0-вая..
                }
            }

            logLevel[logNew] = 0;       // для следующей записи
        }

        public static void ToLogEvent(string What) // запись в лог событий
        {
            if (logIndex != 0)          // писать всё
            {
                logLevel[logNew] = 1;
                ToLog(What);
            }
        }

        public static void ToLogD(string What) // запись в лог Debug
        {
            if (logIndex == 2)          // писать Debug
            {
                logLevel[logNew] = 1;
                ToLog(What);
            }
        }

        public static void logCardVirt(string ss)      // запись данных с карты
        {
                try
                {
                    DateTime timeEntry = datetime0 + TimeSpan.FromSeconds(_DateCard.ParkingEnterTime);
                    string What = ss + Environment.NewLine +
                        "CardId  " + CardIdCardS.ToString() + Environment.NewLine +
                        "ParkingEnterTime  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.ParkingEnterTime)).ToString("F") + Environment.NewLine +
                        "DateSaveCard  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.DateSaveCard)).ToString("F") + Environment.NewLine +
                        "Nulltime1  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.Nulltime1)).ToString("F") + Environment.NewLine +
                        "Nulltime2  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.Nulltime2)).ToString("F") + Environment.NewLine +
                        "Nulltime3  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.Nulltime3)).ToString("F") + Environment.NewLine +
                        "LastPaymentTime  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.LastPaymentTime)).ToString("F") + Environment.NewLine +
                        "LastRecountTime  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.LastRecountTime)).ToString("F") + Environment.NewLine +
                        "TVP  " + _DateCard.TVP.ToString() + Environment.NewLine +
                        "ClientGroupidFC  " + _DateCard.ClientGroupidFC.ToString() + Environment.NewLine +
                        "ClientTypidFC  " + _DateCard.ClientTypidFC.ToString() + Environment.NewLine +
                        "SumOnCard  " + _DateCard.SumOnCard.ToString() + Environment.NewLine +
                        "TKVP  " + _DateCard.TKVP.ToString() + Environment.NewLine +
                        "TPidFC  " + _DateCard.TPidFC.ToString() + Environment.NewLine +
                        "TSidFC  " + _DateCard.TSidFC.ToString() + Environment.NewLine +
                        "ZoneidFC  " + _DateCard.ZoneidFC.ToString() + Environment.NewLine;

                    //logLevel[logNew] = 1;
                    ToLog(What);
                }
                catch (Exception ee)
                
                {
                    Console.WriteLine("Exception запись в лог событий: " + ee.Message);
                }
                finally
                {
                    Console.WriteLine("Executing finally block.");
                }
                
        }

        public static void logCard(string ss)      // запись данных с карты
        {
            if (logIndex != 0)          // писать всё
            {
                try
                {
                    DateTime timeEntry = datetime0 + TimeSpan.FromSeconds(_DateCard.ParkingEnterTime);
                    string What = ss + Environment.NewLine +
                        "CardId  " + CardIdCardS.ToString() + Environment.NewLine +
                        "ParkingEnterTime  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.ParkingEnterTime)).ToString("F") + Environment.NewLine +
                        "DateSaveCard  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.DateSaveCard)).ToString("F") + Environment.NewLine +
                        "Nulltime1  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.Nulltime1)).ToString("F") + Environment.NewLine +
                        "Nulltime2  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.Nulltime2)).ToString("F") + Environment.NewLine +
                        "Nulltime3  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.Nulltime3)).ToString("F") + Environment.NewLine +
                        "LastPaymentTime  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.LastPaymentTime)).ToString("F") + Environment.NewLine +
                        "LastRecountTime  " + (datetime0 + TimeSpan.FromSeconds(_DateCard.LastRecountTime)).ToString("F") + Environment.NewLine +
                        "TVP  " + _DateCard.TVP.ToString() + Environment.NewLine +
                        "ClientGroupidFC  " + _DateCard.ClientGroupidFC.ToString() + Environment.NewLine +
                        "ClientTypidFC  " + _DateCard.ClientTypidFC.ToString() + Environment.NewLine +
                        "SumOnCard  " + _DateCard.SumOnCard.ToString() + Environment.NewLine +
                        "TKVP  " + _DateCard.TKVP.ToString() + Environment.NewLine +
                        "TPidFC  " + _DateCard.TPidFC.ToString() + Environment.NewLine +
                        "TSidFC  " + _DateCard.TSidFC.ToString() + Environment.NewLine +
                        "ZoneidFC  " + _DateCard.ZoneidFC.ToString() + Environment.NewLine;

                    logLevel[logNew] = 1;
                    ToLog(What);
                }
                catch (Exception ee)
                {
                    Console.WriteLine("Exception запись в лог событий: " + ee.Message);
                }
                finally
                {
                    Console.WriteLine("Executing finally block.");
                }
            }
        }

        public static void ToLog_(string What) // запись в лог строки c ---------
        {
            try
            {
                ToLog(new string('-', 30));
                ToLog(What);
            }
            catch (Exception ee)
            {
                Console.WriteLine("Exception запись в лог: " + ee.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
        }

        public static void logControl()                       // контроль размера лога
        {
            DateTime d = DateTime.Now;
            string dsf = d.ToString("yyMMddHHmmss");

            try
            {
                FileInfo files = new FileInfo("LogSync.txt");
                fileLS = files.Length;
                if (fileLS > 1046528)            // 1048576 1046528
                {
                    File.Move("LogSync.txt", "LogSync" + dsf + ".txt");
                }
            }
            catch (Exception e)
            {

            }

        }
    }
}
