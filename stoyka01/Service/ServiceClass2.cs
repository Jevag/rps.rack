﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stoyka01
{
    public class ServiceClass2
    {
        public static string ReverseString(string arg)
        {
            return new string(arg.ToCharArray().Reverse().ToArray());
        }


        //99523c70-5e00-bef2-6379-496aa4afce49
        public static bool IsValidGuid2(string qr)
        {
            Logging.ToLog(qr.Length.ToString());
            if (qr.Length == 36)
            {
                Logging.ToLog(qr.Length.ToString());
                string[] arr = qr.Split('-');
                Logging.ToLog(arr.Length.ToString());
                if (arr.Length == 5)
                {
                    Logging.ToLog(arr[0].Length.ToString() + " " + arr[1].Length.ToString() +
                        arr[2].Length.ToString() + arr[3].Length.ToString() + arr[4].Length.ToString());

                    if (arr[0].Length == 8 && arr[1].Length == 4 && arr[2].Length == 4 && arr[3].Length == 4 && arr[4].Length == 12)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool IsValidGuid(string qr)
        {
            Guid GuidResult = Guid.NewGuid();
            if (Guid.TryParse(qr, out GuidResult))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsValidCheckDiscount(string qr)
        {
            //t=20180116T2216&s=267.00&fn=8710000100454146&i=84766&fp=2339627080&n=1
            if (!qr.Contains("t=")) return false;
            if (!qr.Contains("&s=")) return false;
            if (!qr.Contains("&fn=")) return false;
            if (!qr.Contains("&i=")) return false;
            if (!qr.Contains("&fp=")) return false;
            if (!qr.Contains("&n=")) return false;
            return true;
        }

        public static GlobalClass.Discount StringToDiscount(string qr)
        {
            GlobalClass.Discount discount = new GlobalClass.Discount();
            string[] arr1 = qr.Split('&');
            for (int i = 0; i < arr1.Length; i++)
            {
                string body = arr1[i];
                string[] arr2 = body.Split('=');
                if (arr2[0] == "t")
                {
                    //string date_body = arr2[0].Replace("t", "");
                    string[] arr3 = arr2[1].Split('T');
                    discount.t = arr3[0];
                    discount.T = arr3[1];
                }
                else if (arr2[0] == "s")
                {
                    //discount.S = arr2[1];
                    string[] arr4 = arr2[1].Split('.');
                    discount.S = Convert.ToInt32(arr4[0]);
                }
                else if (arr2[0] == "fn")
                {
                    discount.fn = arr2[1];
                }
                else if (arr2[0] == "fp")
                {
                    discount.fp = arr2[1];
                }
            }
            int year = Convert.ToInt32(discount.t.Substring(0, 4));
            int mn = Convert.ToInt32(discount.t.Substring(4, 2));
            int day = Convert.ToInt32(discount.t.Substring(6, 2));
            int h = Convert.ToInt32(discount.T.Substring(0, 2));
            int m = Convert.ToInt32(discount.T.Substring(2, 2));
            discount.dt = new DateTime(year, mn, day, h, m, 0);

            return discount;
        }
    }
}
