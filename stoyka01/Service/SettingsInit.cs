﻿using Rps.ShareBase;
using RPS_Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static stoyka01.GlobalClass;

namespace stoyka01
{
    public class SettingsInit
    {
        
        public static void CreateZoneColumns()
        {
            CreateColumn("CheckTag", "Cards");
            CreateColumn("FreeSpaceTemp", "ZoneModel");
            CreateColumn("CapacityTemp", "ZoneModel");
            CreateColumn("FreeSpaceSeason", "ZoneModel");
            CreateColumn("CapacitySeason", "ZoneModel");
            CreateColumn("SpaceType", "DisplayZone");
        }

        public static void CreateBarcodeColumns()
        {
            CreateColumn("MinDiscount", "BarCodeModel");
            CreateColumn("MaxDiscount", "BarCodeModel");
            CreateColumnEx("DiscountType", "BarCodeModel","tinyint");
            CreateColumn("BillUsageTime", "BarCodeModel");
            CreateColumnEx("Blocked", "BarCodeModel", "bit");
            CreateColumnEx("ExternalId", "BarCodeInUse", "nvarchar (50)");
            CreateColumnEx("ExternalId", "BCTransaction", "nvarchar (50)");
        }

        public static void CreateTransactionTypes()
        {
            CreateField("PassageTransactionType", "Id='21'", new List<object> { "Name", "Активация скидки по чеку", "Id", 21 });
            CreateField("PassageTransactionType", "Id='22'", new List<object> { "Name", "Активация скидки по штрихкоду на выезде", "Id", 22 });
        }

        public static void UpdateCompanyModel()
        {

        }

        public static void CreateField(string tablename, string field_request, List<object> paramslist)
        {
            string query = "select * from " + tablename + " where " + field_request;
            //string create_query=

            using (SqlConnection connection = new SqlConnection(conlocal))
            {
                connection.Open();

                DataTable dat = UtilsBase.FillTable(query, connection, null, null);
                if (dat != null && dat.Rows.Count > 0)
                {
                    return;
                }
                else
                {
                    UtilsBase.InsertCommand(tablename, connection, null, paramslist.ToArray());
                }
            }

        }

        public static void CreateColumn(string colname, string tablename)
        {
            try
            {
                string query = "select * from " + tablename;

                //string query = "select * from Cards";

                string createquery = "alter table " + tablename + " add " + colname + " int null";

                using (SqlConnection connection = new SqlConnection(conlocal))
                {
                    connection.Open();

                    DataTable dat = UtilsBase.FillTable(query, connection, null, null);
                    DataColumnCollection columns = dat.Columns;

                    if (!columns.Contains(colname))
                    {
                        SqlCommand cmd = new SqlCommand(createquery, connection);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex) { }
        }

        public static void CreateColumnEx(string colname, string tablename, string typename)
        {
            try
            {
                string query = "select * from " + tablename;

                //string query = "select * from Cards";

                string createquery = "alter table " + tablename + " add " + colname + " " + typename;

                using (SqlConnection connection = new SqlConnection(conlocal))
                {
                    connection.Open();

                    DataTable dat = UtilsBase.FillTable(query, connection, null, null);
                    DataColumnCollection columns = dat.Columns;

                    if (!columns.Contains(colname))
                    {
                        SqlCommand cmd = new SqlCommand(createquery, connection);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex) { }
        }

        public static bool IsColumnExists(string colname, string tablename)
        {
            //if (row.Table.Columns.Contains("US_OTHERFRIEND"))

            //string exists = "IF COL_LENGTH('" + tablename + "', '" + colname + "') IS NOT NULL";

            //Debug.Print(exists);
            //string exists = "IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tablename + "' AND COLUMN_NAME = '" + colname + "')";
            // try
            //  {

           string query= "select * from " + tablename;

            //string query = "select * from Cards";

            string CreateQuery = "alter table Cards add Noobs int null";

            using (SqlConnection connection = new SqlConnection(conlocal))
                {
                    connection.Open();


                DataTable dat = UtilsBase.FillTable(query,connection, null, null);
                /*
                DataTable dat = new DataTable();

                SqlCommand cmd = new SqlCommand(query, connection);

                // create data adapter
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                // this will query your database and return the result to your datatable
                da.Fill(dat);
                */

                DataColumnCollection columns = dat.Columns;
                
                    if (columns.Contains(colname))
                    {
                    return true;
                    }
                    else
                    {
                    return false;
                    }

                //da.Dispose();

                /*
                    //Проверка
                    SqlCommand ex1 = new SqlCommand(exists, connection);
                int ex1value = (int)ex1.ExecuteScalar();
                    if (ex1value == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                    */
            }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logging.ToLog("МУКУ АМ!");
        //        return false;
        //    }
        }

        public static void InsertTypesTable()
        {
            string fields = "(ID, Value)";
            string values1 = "('0', 'bool')";
            string sqlQuery1 = "INSERT INTO DeviceRackSettingsTypes" + " " + fields + " VALUES " + values1;
            string exists1 = "SELECT COUNT(*) FROM DeviceRackSettingsTypes WHERE ID ='0'";

            string values2 = "('1', 'string')";
            string sqlQuery2 = "INSERT INTO DeviceRackSettingsTypes" + " " + fields + " VALUES " + values2;
            string exists2 = "SELECT COUNT(*) FROM DeviceRackSettingsTypes where ID = '1'";
            string values3 = "('2', 'int')";
            string sqlQuery3 = "INSERT INTO DeviceRackSettingsTypes" + " " + fields + " VALUES " + values3;
            string exists3 = "SELECT COUNT(*) FROM DeviceRackSettingsTypes where ID = '2'";
            string values4 = "('3', 'double')";
            string sqlQuery4 = "INSERT INTO DeviceRackSettingsTypes" + " " + fields + " VALUES " + values4;
            string exists4 = "SELECT COUNT(*) FROM DeviceRackSettingsTypes where ID = '3'";
            string values5 = "('4', 'guid')";
            string sqlQuery5 = "INSERT INTO DeviceRackSettingsTypes" + " " + fields + " VALUES " + values5;
            string exists5 = "SELECT COUNT(*) FROM DeviceRackSettingsTypes where ID = '4'";

            try
            {
               // атик
                using (SqlConnection connection = new SqlConnection(conlocal))
                {
                    connection.Open();
                    //Проверка
                    SqlCommand ex1 = new SqlCommand(exists1, connection);
                    int ex1value = (int)ex1.ExecuteScalar();
                    if (ex1value == 0)
                    {
                        SqlCommand command1 = new SqlCommand(sqlQuery1, connection);
                        command1.ExecuteNonQuery();
                    }

                    SqlCommand ex2 = new SqlCommand(exists2, connection);
                    int ex2value = (int)ex2.ExecuteScalar();
                    if (ex2value == 0)
                    {
                        SqlCommand command2 = new SqlCommand(sqlQuery2, connection);
                        command2.ExecuteNonQuery();
                    }

                    SqlCommand ex3 = new SqlCommand(exists3, connection);
                    int ex3value = (int)ex3.ExecuteScalar();
                    if (ex3value == 0)
                    {
                        SqlCommand command3 = new SqlCommand(sqlQuery3, connection);
                        command3.ExecuteNonQuery();
                    }

                    SqlCommand ex4 = new SqlCommand(exists4, connection);
                    int ex4value = (int)ex4.ExecuteScalar();
                    if (ex4value == 0)
                    {
                        SqlCommand command4 = new SqlCommand(sqlQuery4, connection);
                        command4.ExecuteNonQuery();
                    }

                    SqlCommand ex5 = new SqlCommand(exists5, connection);
                    int ex5value = (int)ex5.ExecuteScalar();
                    if (ex5value == 0)
                    {
                        SqlCommand command5 = new SqlCommand(sqlQuery5, connection);
                        command5.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.ToLog("Ошибка заполнения таблицы DeviceRackSettingsTypes " + conlocal);
            }
        }

        public static bool IsTableExists()
        {
            Int32 newProdID = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[DeviceRackSettings]')";
            using (SqlConnection conn = new SqlConnection(conlocal))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                try
                {
                    conn.Open();
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                   Logging.ToLog(ex.Message);
                   return true;
                }
            }
        }

        public static bool KKMTableExists()
        {
            Int32 newProdID = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[CheckListIs]')";
            using (SqlConnection conn = new SqlConnection(conlocal))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                try
                {
                    conn.Open();
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog(ex.Message);
                    return true;
                }
            }
        }

        public static bool IskraTableExists()
        {
            Int32 newProdID = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[WebIskraCL]')";
            using (SqlConnection conn = new SqlConnection(conlocal))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                try
                {
                    conn.Open();
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog(ex.Message);
                    return true;
                }
            }
        }

        public static void CreateIskraTable()
        {
            //run script if table isn't exists
            if (!IskraTableExists())
            {
                //Create Table
                string script = File.ReadAllText(Application.StartupPath + "\\WebIskraCL.sql");

                // split script on GO command
                IEnumerable<string> queries = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                try
                {
                    using (SqlConnection conn = new SqlConnection(conlocal))
                    {

                        conn.Open();
                        foreach (string query in queries)
                        {
                            if (query.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(query, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка выполнения скрипта создания таблиц " + conlocal);
                }

                Thread.Sleep(500);
                //Надо заполнить табличку
            }
        }

        public static void CreateKKMTable()
        {
            //run script if table isn't exists
            if (!KKMTableExists())
            {
                //Create Table
                string script = File.ReadAllText(Application.StartupPath + "\\CheckListIs.sql");

                // split script on GO command
                IEnumerable<string> queries = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                try
                {
                    using (SqlConnection conn = new SqlConnection(conlocal))
                    {

                        conn.Open();
                        foreach (string query in queries)
                        {
                            if (query.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(query, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка выполнения скрипта создания таблиц " + conlocal);
                }

                Thread.Sleep(500);
                //Надо заполнить табличку
            }
        }

        public static bool CompanyFNTableExists()
        {
            Int32 newProdID = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[CompanyFN]')";
            using (SqlConnection conn = new SqlConnection(conlocal))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                try
                {
                    conn.Open();
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog(ex.Message);
                    return true;
                }
            }
        }

        public static void CreateCompanyFNTable()
        {
            //run script if table isn't exists
            if (!CompanyFNTableExists())
            {
                //Create Table
                string script = File.ReadAllText(Application.StartupPath + "\\CompanyFN.sql");

                // split script on GO command
                IEnumerable<string> queries = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                try
                {
                    using (SqlConnection conn = new SqlConnection(conlocal))
                    {

                        conn.Open();
                        foreach (string query in queries)
                        {
                            if (query.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(query, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка выполнения скрипта создания таблиц " + conlocal);
                }

                Thread.Sleep(200);
                //Надо заполнить табличку
            }
        }

        public static bool ZReportTableExists()
        {
            Int32 newProdID = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[ZReport]')";
            using (SqlConnection conn = new SqlConnection(conlocal))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                try
                {
                    conn.Open();
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog(ex.Message);
                    return true;
                }
            }
        }

        public static void CreateZReport()
        {
            //run script if table isn't exists
            if (!ZReportTableExists())
            {
                //Create Table
                string script = File.ReadAllText(Application.StartupPath + "\\ZReport.sql");

                // split script on GO command
                IEnumerable<string> queries = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                try
                {
                    using (SqlConnection conn = new SqlConnection(conlocal))
                    {

                        conn.Open();
                        foreach (string query in queries)
                        {
                            if (query.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(query, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка выполнения скрипта создания таблиц " + conlocal);
                }

                Thread.Sleep(500);
                //Надо заполнить табличку
            }
        }

        public static bool CheckQRExists()
        {
            Int32 newProdID = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[CheckQR]')";
            using (SqlConnection conn = new SqlConnection(conlocal))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                try
                {
                    conn.Open();
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog(ex.Message);
                    return true;
                }
            }
        }

        public static void CreateCheckQR()
        {
            //run script if table isn't exists
            if (!CheckQRExists())
            {
                //Create Table
                string script = File.ReadAllText(Application.StartupPath + "\\CheckQR.sql");

                // split script on GO command
                IEnumerable<string> queries = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                try
                {
                    using (SqlConnection conn = new SqlConnection(conlocal))
                    {

                        conn.Open();
                        foreach (string query in queries)
                        {
                            if (query.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(query, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка выполнения скрипта создания таблиц " + conlocal);
                }

                Thread.Sleep(500);
                //Надо заполнить табличку
            }
        }


        //New Tables 30.09.2020
        //RZoneTransaction
        /*
        public static bool RZoneTransactionExists()
        {
            Int32 newProdID = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[RZoneTransaction]')";
            using (SqlConnection conn = new SqlConnection(conlocal))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                try
                {
                    conn.Open();
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog(ex.Message);
                    return true;
                }
            }
        }

        public static void CreateRZoneTransaction()
        {
            //run script if table isn't exists
            if (!RZoneTransactionExists())
            {
                //Create Table
                string script = File.ReadAllText(Application.StartupPath + "\\RZoneTransaction.sql");

                // split script on GO command
                IEnumerable<string> queries = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                try
                {
                    using (SqlConnection conn = new SqlConnection(conlocal))
                    {

                        conn.Open();
                        foreach (string query in queries)
                        {
                            if (query.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(query, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка выполнения скрипта создания таблиц " + conlocal);
                }

                Thread.Sleep(500);
                //Надо заполнить табличку
            }
        }

        public static bool RZoneStatusExists()
        {
            Int32 newProdID = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[RZoneStatus]')";
            using (SqlConnection conn = new SqlConnection(conlocal))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                try
                {
                    conn.Open();
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog(ex.Message);
                    return true;
                }
            }
        }

        public static void CreateRZoneStatus()
        {
            //run script if table isn't exists
            if (!RZoneStatusExists())
            {
                //Create Table
                string script = File.ReadAllText(Application.StartupPath + "\\RZoneStatus.sql");

                // split script on GO command
                IEnumerable<string> queries = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                try
                {
                    using (SqlConnection conn = new SqlConnection(conlocal))
                    {

                        conn.Open();
                        foreach (string query in queries)
                        {
                            if (query.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(query, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка выполнения скрипта создания таблиц " + conlocal);
                }

                Thread.Sleep(500);
                //Надо заполнить табличку
            }
        }

        public static bool RZoneExists()
        {
            Int32 newProdID = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[RZone]')";
            using (SqlConnection conn = new SqlConnection(conlocal))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                try
                {
                    conn.Open();
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog(ex.Message);
                    return true;
                }
            }
        }

        public static void CreateRZone()
        {
            //run script if table isn't exists
            if (!RZoneExists())
            {
                //Create Table
                string script = File.ReadAllText(Application.StartupPath + "\\RZone.sql");

                // split script on GO command
                IEnumerable<string> queries = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                try
                {
                    using (SqlConnection conn = new SqlConnection(conlocal))
                    {

                        conn.Open();
                        foreach (string query in queries)
                        {
                            if (query.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(query, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка выполнения скрипта создания таблиц " + conlocal);
                }

                Thread.Sleep(500);
                //Надо заполнить табличку
            }
        }
        */
        public static void CreateRZoneNew()
        {
                //Create Table
                string script = File.ReadAllText(Application.StartupPath + "\\RZoneNew.sql");

                // split script on GO command
                IEnumerable<string> queries = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                try
                {
                    using (SqlConnection conn = new SqlConnection(conlocal))
                    {

                        conn.Open();
                        foreach (string query in queries)
                        {
                            if (query.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(query, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка выполнения скрипта создания таблиц " + conlocal);
                }

                Thread.Sleep(500);
                //Надо заполнить табличку
        }

        public static void FillDefRS(string DeviceId)
        {
            DefSet = new List<RackSettings>();

            RackSettings RS;
            Guid gd;
            //ZoneBeforeId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ZoneBeforeId";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //ZoneAfterId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ZoneAfterId";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //DispenserTypeId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DispenserTypeId";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            //BarcodeTypeId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BarcodeTypeId";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            //BankModuleTypeId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BankModuleTypeId";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            //MaxBusyTimeAntennaA
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MaxBusyTimeAntennaA";
            RS.value = "100";
            RS.type = 2;
            DefSet.Add(RS);

            //MaxBusyTimeAntennaB
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MaxBusyTimeAntennaB";
            RS.value = "100";
            RS.type = 2;
            DefSet.Add(RS);

            //MaxBusyTimeIR
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MaxBusyTimeIR";
            RS.value = "100";
            RS.type = 2;
            DefSet.Add(RS);

            //DisplayTypeId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DisplayTypeId";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //DisplayTypeIdTwo
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DisplayTwoTypeId";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //RackId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RackId";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //SlaveExists
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SlaveExists";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //ReaderOutExists
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ReaderOutExists";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //ReaderOutTypeId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ReaderOutTypeId";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            //ReaderInExists
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ReaderInExists";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //ReaderInTypeId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ReaderInTypeId";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            //DispenserExists
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DispenserExists";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //FeederExists
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "FeederExists";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //FeederTypeId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "FeederTypeId";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            //BarcodeExists
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BarcodeExists";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //BankModuleExists
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BankModuleExists";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //DisplayExists
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DisplayExists";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DisplayTwoExists";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //HopperExists
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "HopperExists";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //ComHopper
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComHopper";
            RS.value = "10";
            RS.type = 2;
            DefSet.Add(RS);

            //ComDisplay
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComDisplay";
            RS.value = "9";
            RS.type = 2;
            DefSet.Add(RS);

            //ComDisplayTwo
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComDisplayTwo";
            RS.value = "10";
            RS.type = 2;
            DefSet.Add(RS);

            //ComSlave
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComSlave";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //ComReadOut
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComReadOut";
            RS.value = "4";
            RS.type = 2;
            DefSet.Add(RS);

            //ComReadIn
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComReadIn";
            RS.value = "3";
            RS.type = 2;
            DefSet.Add(RS);

            //ComDispenser
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComDispenser";
            RS.value = "2";
            RS.type = 2;
            DefSet.Add(RS);

            //ComFeeder
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComFeeder";
            RS.value = "13";
            RS.type = 2;
            DefSet.Add(RS);

            //ComBarcode
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComBarcode";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //PortDiscret1
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PortDiscret1";
            RS.value = "2";
            RS.type = 2;
            DefSet.Add(RS);

            //PortDiscret2
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PortDiscret2";
            RS.value = "7";
            RS.type = 2;
            DefSet.Add(RS);

            //PortDiscret3
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PortDiscret3";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //PortDiscret4
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PortDiscret4";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RegularPlate"; //True/False
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BarcodePlate"; //True/False
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //AddFreeTime
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AddFreeTime";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //TimeCardV
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimeCardV";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //TimeBarrier
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimeBarrier";
            RS.value = "10";
            RS.type = 2;
            DefSet.Add(RS);

            //NumberSector
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "NumberSector";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //TarifIdScheduleId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TarifIdScheduleId";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //TarifPlanId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TarifPlanId";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //ClientGroupidToC
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ClientGroupidToC";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //TimeNotLoopA
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimeNotLoopA";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //TimeNotIr
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimeNotIr";
            RS.value = "3";
            RS.type = 2;
            DefSet.Add(RS);

            //LoopA
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LoopA";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //LoopB
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LoopB";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //IR
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "IR";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //PUSH
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PUSH";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //Debug
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Debug";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //SlaveDebug
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SlaveDebug";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //ReaderOutDebug
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ReaderOutDebug";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //ReaderInDebug
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ReaderInDebug";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //DispenserDebug
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DispenserDebug";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //BarcodeDebug
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BarcodeDebug";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //checkBoxItog
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "checkBoxItog";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //logNastroy
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "logNastroy";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //CardKey
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CardKey";
            RS.value = "E7A894330E7615209EDB559E83157ADA";
            RS.type = 1;
            DefSet.Add(RS);

            //TransitTypeId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TransitTypeId";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //RackWorkingModeId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RackWorkingModeId";
            RS.value = "3";
            RS.type = 2;
            DefSet.Add(RS);

            //ZoneId
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ZoneId";
            RS.value = "17d66717-64a1-419a-85e0-0be834a99001";
            RS.type = 4;
            DefSet.Add(RS);

            //ServerURL
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ServerURL";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            //ComRFID
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComRFID";
            RS.value = "5";
            RS.type = 2;
            DefSet.Add(RS);

            //RFIDExists
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RFIDExists";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //CollectorExists
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CollectorExists";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //ComCollector
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComCollector";
            RS.value = "9";
            RS.type = 2;
            DefSet.Add(RS);

            //Language1
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Language1";
            RS.value = "RU";
            RS.type = 1;
            DefSet.Add(RS);

            //SizeFontLanguage1
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SizeFontLanguage1";
            RS.value = "60";
            RS.type = 3;
            DefSet.Add(RS);

            //Language2
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Language2";
            RS.value = "EN";
            RS.type = 1;
            DefSet.Add(RS);

            //SizeFontLanguage2
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SizeFontLanguage2";
            RS.value = "32";
            RS.type = 3;
            DefSet.Add(RS);

            //Language3
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Language3";
            RS.value = "RU";
            RS.type = 1;
            DefSet.Add(RS);

            //SizeFontTime
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SizeFontTime";
            RS.value = "12";
            RS.type = 3;
            DefSet.Add(RS);

            //InvertCall
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "InvertCall";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //InvertAltimetr
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "InvertAltimetr";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //ButtonPushPort
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ButtonPushPort";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //UpdaterY
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UpdaterY";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //CardsOnline
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CardsOnline";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //LogTyp
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LogTyp";
            RS.value = "2";
            RS.type = 2;
            DefSet.Add(RS);

            //Reverse
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Reverse";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //PlaceCompanyControl
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PlaceCompanyControl";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //PlaceControlRazov
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PlaceControlRazov";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //PropuskAvtoY
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PropuskAvtoY";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //TabloNew
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TabloNew";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //TabloNewTwo
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TabloNewTwo";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //TabloAddress
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TabloAddress";
            RS.value = "2";
            RS.type = 2;
            DefSet.Add(RS);

            //NewAlarmE
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "NewAlarmE";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //TimeNewAlarm
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimeNewAlarm";
            RS.value = "4";
            RS.type = 2;
            DefSet.Add(RS);

            //CardExitWrite
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CardExitWrite";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //GreenNoCard
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "GreenNoCard";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //ControlLoopB
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ControlLoopB";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //TopMost
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TopMost";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //13.10.2017

            //DisableRazovButton
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DisableRazovButton";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //BarcodeFromButton
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BarcodeFromButton";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //17.10.2017-новое распознавание

            //Viinex
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Viinex";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Mallenom";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);


            //ViinexSettingsPath
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ViinexSettingsPath";
            RS.value = "c:\\Program Files\\Viinex\\etc\\conf.d\\lpr_ru.json";
            RS.type = 1;
            DefSet.Add(RS);

            //ViinexGetURI
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ViinexGetURI";
            RS.value = "http://localhost:8880/v1/svc/alprcam1";
            RS.type = 1;
            DefSet.Add(RS);

            //CameraURI
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CameraURI";
            RS.value = "192.168.1.1";
            RS.type = 1;
            DefSet.Add(RS);

            //CameraLogin
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CameraLogin";
            RS.value = "root";
            RS.type = 1;
            DefSet.Add(RS);

            //CameraPassword
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CameraPassword";
            RS.value = "root";
            RS.type = 1;
            DefSet.Add(RS);

            //CameraSkip
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CameraSkip";
            RS.value = "non_idr";
            RS.type = 2;
            DefSet.Add(RS);

            //CameraPreProcess
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PreProcess";
            RS.value = "1500";
            RS.type = 3;
            DefSet.Add(RS);

            //CameraPostProcess
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PostProcess";
            RS.value = "1500";
            RS.type = 3;
            DefSet.Add(RS);

            //ViinexConfidence
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ViinexConfidence";
            RS.value = "98";
            RS.type = 2;
            DefSet.Add(RS);

            //ViinexLicenseKey
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ViinexLicenseKey";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            //23.10.2017-Post запрос на занятие петли A

            //EnableLoopANotification
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "EnableLoopANotification";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            //LoopANotificationUri
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LoopANotificationUri";
            RS.value = "http://youruri.ru";
            RS.type = 1;
            DefSet.Add(RS);

            //LoopANotificationTimeout
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LoopANotificationTimeout";
            RS.value = "500";
            RS.type = 2;
            DefSet.Add(RS);

            //23.10.2017-Локальное сохранение фотографий
            //SavePhoto
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SavePhoto";
            RS.value = "0"; //0-не сохраняем, 1- всегда, 2 - если распознали
            RS.type = 2;
            DefSet.Add(RS);

            //SavePhotoDir
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SavePhotoDir";
            RS.value = "c:\\Rack\\";
            RS.type = 1;
            DefSet.Add(RS);

            //txtSavePhotoTimeout
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SavePhotoTimeout";
            RS.value = "500";
            RS.type = 2;
            DefSet.Add(RS);

            //02.11.2017 - кириллица/латиница
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ViinexTransliter";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnTransliter";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //17.11.2017 - BankSumMax - потеряшка
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BankSumMax";
            RS.value = "1000";
            RS.type = 2;
            DefSet.Add(RS);

            //27.11.2017 - Настройки нового табло
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UDPTabloIP";
            RS.value = "192.168.0.7";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UDPTabloPort";
            RS.value = "10006";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UDPTabloColor";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //29.11.2017 - Дополнительные настройки нового табло
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UDPTabloRed";
            RS.value = "10";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UDPTabloYellow";
            RS.value = "100";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UDPTabloGreen";
            RS.value = "999";
            RS.type = 2;
            DefSet.Add(RS);

            //29.11.2017 - Дополнительные настройки Viinex
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CameraType";
            RS.value = "Onvif";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AcquireVideo";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AcquireEvents";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //05.01.2018 Настройки Viinex для второй камеры
            // кириллица/латиница
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ViinexTransliter2";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //Дополнительные настройки Viinex
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CameraType2";
            RS.value = "Onvif";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AcquireVideo2";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AcquireEvents2";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //CameraURI
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CameraURI2";
            RS.value = "192.168.1.1";
            RS.type = 1;
            DefSet.Add(RS);

            //CameraLogin
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CameraLogin2";
            RS.value = "root";
            RS.type = 1;
            DefSet.Add(RS);

            //CameraPassword
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CameraPassword2";
            RS.value = "root";
            RS.type = 1;
            DefSet.Add(RS);

            //CameraSkip
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CameraSkip2";
            RS.value = "non_idr";
            RS.type = 2;
            DefSet.Add(RS);

            //CameraPreProcess
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PreProcess2";
            RS.value = "1500";
            RS.type = 3;
            DefSet.Add(RS);

            //CameraPostProcess
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PostProcess2";
            RS.value = "1500";
            RS.type = 3;
            DefSet.Add(RS);

            //ViinexConfidence
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ViinexConfidence2";
            RS.value = "98";
            RS.type = 2;
            DefSet.Add(RS);

            

            //Камера 2 включена или нет - оставим!!!

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Camera2Enabled";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            //13.01.2017
            //0- отключен; 1- получать; 2-отправлять
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TwoLevelRequest";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //txtTwoLevelSendIP
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TwoLevelSendIP";
            RS.value = "192.168.1.1";
            RS.type = 1;
            DefSet.Add(RS);

            //txtTwoLevelReceiveIP
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TwoLevelReceiveIP";
            RS.value = "192.168.1.1";
            RS.type = 1;
            DefSet.Add(RS);

            //18.01.2018
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "GetSnapshots";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            //19.01.2018
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TrainReverse";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            //24.01.2018
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimeBudget";
            RS.value = "30";
            RS.type = 2;
            DefSet.Add(RS);

            //09.02.2018
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ControlBL";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ControlIO";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ControlBL";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LevenBL";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LevenIO";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LevenPK";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LevenQR";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            //LoopAAlarmsTime
            //14.02.2018
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LoopAAlarmsTime";
            RS.value = "15";
            RS.type = 2;
            DefSet.Add(RS);
            //New RFID Settings 24.02.2018
            //RFIDType
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RfidType";
            RS.value = "RPS-Wiegand";
            RS.type = 2;
            DefSet.Add(RS);

            //AntennaNumber
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AntennaNumber";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //RFIDRackType
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RFIDRackType";
            RS.value = "Host";
            RS.type = 1;
            DefSet.Add(RS);

            //RFIDTransponderIP
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RFIDTransponderIP";
            RS.value = "192.168.10.10";
            RS.type = 1;
            DefSet.Add(RS);

            //RFIDTransponderPort
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RFIDTransponderPort";
            RS.value = "10001";
            RS.type = 1;
            DefSet.Add(RS);

            //RFIDHostIP
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RFIDHostIP";
            RS.value = "192.168.10.10";
            RS.type = 1;
            DefSet.Add(RS);

            //RFIDTCPA1
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RfidTCPA1";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            //RFIDTCPA2
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RfidTCPA2";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            //RFIDTCPA3
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RfidTCPA3";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            //RFIDTCPA4
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RfidTCPA4";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            //RackSource
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RackSource";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //12.04.2018
            //FreewayRed
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "FreewayRed";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            //11.02.2021
            //Настройки Малленома

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnVertPers";
            RS.value = "100";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnHorPers";
            RS.value = "100";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnDistorsion";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //#1
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX1_1";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY1_1";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            //2
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX2_1";
            RS.value = "0.5";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY2_1";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            //3
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX3_1";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY3_1";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            //4
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX4_1";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY4_1";
            RS.value = "0.5";
            RS.type = 2;
            DefSet.Add(RS);

            //5
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX5_1";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY5_1";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            //6
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX6_1";
            RS.value = "0.5";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY6_1";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            //7
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX7_1";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY7_1";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            //8
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX8_1";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY8_1";
            RS.value = "0.5";
            RS.type = 2;
            DefSet.Add(RS);

            
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnMinSizeW_1";
            RS.value = "0.070000000298023224";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnMinSizeH_1";
            RS.value = "0.030999999493360519";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnMaxSizeW_1";
            RS.value = "0.17000000178813934";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnMaxSizeH_1";
            RS.value = "0.075000002980232239";
            RS.type = 2;
            DefSet.Add(RS);

            //#2
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX1_2";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY1_2";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            //2
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX2_2";
            RS.value = "0.5";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY2_2";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            //3
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX3_2";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY3_2";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            //4
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX4_2";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY4_2";
            RS.value = "0.5";
            RS.type = 2;
            DefSet.Add(RS);

            //5
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX5_2";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY5_2";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            //6
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX6_2";
            RS.value = "0.5";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY6_2";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            //7
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX7_2";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY7_2";
            RS.value = "0.95";
            RS.type = 2;
            DefSet.Add(RS);

            //8
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointX8_2";
            RS.value = "0.05";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPointY8_2";
            RS.value = "0.5";
            RS.type = 2;
            DefSet.Add(RS);


            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnMinSizeW_2";
            RS.value = "0.070000000298023224";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnMinSizeH_2";
            RS.value = "0.030999999493360519";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnMaxSizeW_2";
            RS.value = "0.17000000178813934";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "0.075000002980232239";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnConfidence_1";
            RS.value = "90";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPreprocess_1";
            RS.value = "1500";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnConfidence_2";
            RS.value = "90";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnPreprocess_2";
            RS.value = "1500";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnConString_1";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnConString_2";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnAngle_1";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnAngle_2";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "MlnUseCamera2";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //17.04.2018
            //SnapshotsLoopA
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SnapshotsLoopA";
            RS.value = "5";
            RS.type = 2;
            DefSet.Add(RS);
            //04.05.2018
            //UseLoopAServer
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UseLoopAServer";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);
            //18.05.2018
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "EnableOperator";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            //Новые настройки распознавания 21.05.2018
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "NotRAllow";
            RS.value = "true";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "NotRDeny";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BlackList";
            RS.value = "true";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "InOut";
            RS.value = "true";
            RS.type = 0;
            DefSet.Add(RS);

            //29.05.2018-ROI
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RoiLeft";
            RS.value = "0,01";
            RS.type = 3;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RoiTop";
            RS.value = "0,01";
            RS.type = 3;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RoiRight";
            RS.value = "0,09";
            RS.type = 3;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RoiBottom";
            RS.value = "0,09";
            RS.type = 3;
            DefSet.Add(RS);

            //Transport TCP
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TransportTCP";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TransportUDP";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TransportTCP2";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TransportUDP2";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //камеры для снапшотов
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Snap1";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Snap2";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //контроль зон
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CheckZoneRecognition";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CheckZoneRFID";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //ParkPass
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ParkPassExists";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComParkPass";
            RS.value = "4";
            RS.type = 2;
            DefSet.Add(RS);

            //ККМ

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CashierNumber"; //Номер кассы вестимо (или ККМ-ки?)
            RS.value = "18";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "KKMPassword";
            RS.value = "AERF";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "KKMModePassword";
            RS.value = "AERF";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "KKMFiskalVersion";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ChequeWork";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ShiftOpened";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);


            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ShiftNumber";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CurrentTransactionNumber";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LastShiftOpenTime";
            RS.value = "Null";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "VAT";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "KKMExists";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComKKM";
            RS.value = "20";
            RS.type = 2;
            DefSet.Add(RS);

            //пока не делаем для него...
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PrinterExists";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AutoSmena";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AutoSmenaTime";
            RS.value = "19:25:34"; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "OneAlarmTime";
            RS.value = "5";
            RS.type = 2;
            DefSet.Add(RS);

            //новый голос...
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "NewVoice";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "StayBeforeCheck";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            //TarifIdScheduleIdPP
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TarifIdScheduleIdPP";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //TarifPlanIdPP
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TarifPlanIdPP";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //ClientGroupidToC
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ClientGroupidToCPP";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "HopperType";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CompanySession";
            RS.value = "false";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DefaultCardKey";
            RS.value = "FFFFFFFFFFFF"; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CheckZoneHID";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ReverseMode";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteReverseIP";
            RS.value = "192.168.1.1"; //string
            RS.type = 1;
            DefSet.Add(RS);

            //RemoteReverseCarCount
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteReverseCarCount";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TwoFactorsIdentity";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PenaltyCardRecognition";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);
            
            //PenaltyCardPassWithoutSync
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PenaltyCardPassWithoutSync";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PenaltyCardPassWithoutSync";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PereezdDolg";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SlaveType";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ParkpassType";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CurrentLanguage";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SecondLanguage";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);
            
            //slave3 in AB
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In1A";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In2A";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In3A";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In4A";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In1B";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In2B";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In3B";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In4B";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //slave3 out AB
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out1A";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out2A";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out3A";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out4A";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out1B";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out2B";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out3B";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out4B";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //H-In
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In1H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In2H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In3H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In4H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In5H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In6H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In7H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In8H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In9H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3In10H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //H-Out
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out1H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out2H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out3H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out4H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out5H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out6H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out7H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out8H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out9H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Out10H";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //slave relays
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Relay1";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Relay2";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Relay3";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Relay4";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Relay5";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Relay6";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Relay7";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Relay8";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Relay9";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Slave3Relay10";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);
                

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "KKMTag1214";
            RS.value = "04";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebCameraIP";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebCameraLogin";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebCameraPassword";
            RS.value = "";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimerEntrySol1";
            RS.value = "60";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimerEntrySol2";
            RS.value = "60";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimerExitSol1";
            RS.value = "60";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimerExitSol2";
            RS.value = "60";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimerExitSol3";
            RS.value = "60";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WaitForPayment";
            RS.value = "20";
            RS.type = 2;
            DefSet.Add(RS);

            //SoundText
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPressButtonCard";
            RS.value = "\\sound\\card_button.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPressButtonTag";
            RS.value = "\\sound\\tag_button.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPressButtonTicket";
            RS.value = "\\sound\\ticket_button.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPressButtonCardOR";
            RS.value = "\\sound\\card_button_or.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPressButtonTagOR";
            RS.value = "\\sound\\tag_button_or.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPressButtonTicketOR";
            RS.value = "\\sound\\ticket_button_or.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundTakeCard";
            RS.value = "\\sound\\card_take.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundTakeTag";
            RS.value = "\\sound\\tag_take.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundTakeTicket";
            RS.value = "\\sound\\ticket_take.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPleaseDrive";
            RS.value = "\\sound\\please_drive.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundBlackList";
            RS.value = "\\sound\\black_list.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundCardBlocked";
            RS.value = "\\sound\\card_blocked.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundWrongZone";
            RS.value = "\\sound\\wrong_zone.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundWrongGroup";
            RS.value = "\\sound\\wrong_group.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundNeedMoney";
            RS.value = "\\sound\\need_money.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPaypass";
            RS.value = "\\sound\\paypass.wav";
            RS.type = 1;
            DefSet.Add(RS);

            //Sound Timeouts
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPressButtonDelay";
            RS.value = "5";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPressButtonORDelay";
            RS.value = "5";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundTakeDelay";
            RS.value = "5";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPleaseDriveDelay";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundBlackListDelay";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundCardBlockedDelay";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundWrongZoneDelay";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundWrongGroupDelay";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundNeedMoneyDelay";
            RS.value = "8";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPaypassDelay";
            RS.value = "8";
            RS.type = 2;
            DefSet.Add(RS);

            //еще звуки (выезд)
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPutCard";
            RS.value = "\\sound\\put_card.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPutTag";
            RS.value = "\\sound\\put_tag.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPutTicket";
            RS.value = "\\sound\\put_ticket.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPutCardOr";
            RS.value = "\\sound\\put_card_or.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPutTagOr";
            RS.value = "\\sound\\put_tag_or.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPutTicketOr";
            RS.value = "\\sound\\put_ticket_or.wav";
            RS.type = 1;
            DefSet.Add(RS);
            
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPost";
            RS.value = "\\sound\\post.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundGiveWay";
            RS.value = "\\sound\\give_way.wav";
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPostDelay";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundGiveWayDelay";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPutDelay";
            RS.value = "5";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SoundPutOrDelay";
            RS.value = "5";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "KKMDebug";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TPTSCheck";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketPrinter";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComTicketPrinter";
            RS.value = "3";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketRetract";
            RS.value = "True";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketRelease";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);


            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketScanner";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComTicketScanner";
            RS.value = "5";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CarsInRamp";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Tablo4Color";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Tablo4Bright";
            RS.value = "50";
            RS.type = 2;
            DefSet.Add(RS);

            //control_upd
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PlaceControl";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DeniedForRaz";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DeniedForAll";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DeniedForRazIf";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DeniedForPostIf";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ProductionNumber";
            RS.value = "9999";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LogDays";
            RS.value = "7";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "EnableBarrierTO";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BarrierTO";
            RS.value = "6";
            RS.type = 2;
            DefSet.Add(RS);

            //chkNoCardEntrance
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "NoCardEntrance";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketExitRecognition";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketExitRecognitionLeven";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketExitRecognitionTimer";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketCollectorTimer";
            RS.value = "100";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BankType";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //ControlLoopBRev
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ControlLoopBRev";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "QRDiscount";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);
            //chkQRDiscount

            //public static bool EnabledTicketRecognition = false;
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "EnabledTicketRecognition";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BarcodeType";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ParkpassDisableRaz";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //cboReadOutPower
            //cboReadInPower
            //cboDispenserPower
            //cboDisplayPower
            //cboDisplayTwoPower
            //cboHopperPower
            //cboRFIDPower
            //cboCollectorPower
            //cboBankModulePower
            //cboTicketPrinterPower
            //cboTicketScannerPower
            //cboKKMPower

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ReadOutPower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ReadInPower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DispenserPower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DisplayPower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "DisplayTwoPower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "HopperPower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RFIDPower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CollectorPower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BankModulePower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketPrinterPower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketScannerPower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "KKMPower";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);


            //всякое
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ClimateMode";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TempChannel";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Cooler";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Heater";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ClimateTMax";
            RS.value = "50";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ClimateTMin";
            RS.value = "-5";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ClimateHumidity";
            RS.value = "90";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Bobina";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "Econom";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ComEconom";
            RS.value = "5";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AutoSver";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AutoSverTime";
            RS.value = "19:25:34"; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LevenRegion";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);
          
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AbonementBlock";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AbonementBlockAlert";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AbonementBlockAlertDays";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //chkRZoneActive
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RZoneActive";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //chkRZoneDisableExit
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RZoneDisableExit";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //chkEnableAltimetrChecking
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "EnableAltimetrChecking";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //chkEnableAltimetrChecking
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "EnableAltimetrRazForExit";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AltimetrControlTS0";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AltimetrControlTS1";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AltimetrControlTS2";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AltimetrControlTS3";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //RPSButtonEnabled
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RPSButtonEnabled";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RPSButtonCS";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RPSButtonByte4";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "InvertHatch";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //PenaltyCardBarcode
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PenaltyCardBarcodeCheck";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PrinterDensity";
            RS.value = "200";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "FreeBarrierReverse";
            RS.value = "100";
            RS.type = 2;
            DefSet.Add(RS);

            //BarcodeAltimeter
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BarcodeAltimeter";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //UZ
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UZOperator";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UZLoginX";
            RS.value = "1024";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UZLoginY";
            RS.value = "768";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UZFormX";
            RS.value = "1024";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UZFormY";
            RS.value = "768";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "IgnoreBarierTransactions";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AirportMode";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //LoopD
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LoopD";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //IR2
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "IR2";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "IgnoreFreeTransactions";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CompanyControl";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "EnableRfidReset";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UseDbonly";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BlockByLoopB";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ZetonLoopABasket";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "FireAlarm";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "FireAlarmDelay";
            RS.value = "100";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebIskraEP";
            RS.value = "https://195.190.114.238:8281/kktcloud/api/kktcloud?shop=1"; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebIskraAdr";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebIskraPlace";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebIskraFIO";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebIskraINN";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebIskraTaxation";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebIskraTax";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //TicketScannerType
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketScannerType";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //TicketScannerType
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TicketPrinterType";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebIskra";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebIskraLogin";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "WebIskraPassword";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PPTimeout";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            //txtPPThres
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PPThres";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PPDelta";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            //chkAbonementBlockAllow
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AbonementBlockAllow";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //chkRemoteRackPost
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackPost";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackRaz";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //txtRemoteRackPost1
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackPost1";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackPost2";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackPost3";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackPost4";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackPost5";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackRaz1";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackRaz2";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackRaz3";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackRaz4";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RemoteRackRaz5";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TimeAlarm";
            RS.value = "100";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "AbonementBlockAllow";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UpDoorInverse";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "ExtendedLog";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PairLoops";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UZValue";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "OperatorRFIDHID";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "OperatorRFIDHIDDelay";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RPSButtonDelay";
            RS.value = "20";
            RS.type = 2;
            DefSet.Add(RS);

            //TicketScannerType
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "KKMType";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            //BankSumMax
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BankSumMax";
            RS.value = "1000";
            RS.type = 2;
            DefSet.Add(RS);

            //chkRegTicketAnalysis
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "RegTicketAnalysis";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UnitellerTID";
            RS.value = "00000000"; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UnitellerIP";
            RS.value = "127.0.0.1"; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UnitellerPort";
            RS.value = "10400"; //string
            RS.type = 1;
            DefSet.Add(RS);

            //chkHomelessRack
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "HomelessRack";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BlockedGroupExist";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "BlockedGroupList";
            RS.value = ""; //string
            RS.type = 1;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "SinglePlate";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "LevenSingle";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TarifIdEntrance";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "TarifPlanIdEntrance";
            RS.value = "1";
            RS.type = 2;
            DefSet.Add(RS);

            //PaySummEntrance
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PaySummEntrance";
            RS.value = "100";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "PayEntranceCard";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //chkUpdateHost
            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "UpdateHost";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "IpListIndex";
            RS.value = "0";
            RS.type = 2;
            DefSet.Add(RS);

            RS = new RackSettings();
            gd = Guid.NewGuid();
            RS.id = gd.ToString();
            RS.device_id = DeviceId;
            RS.name = "CardidReverseDec";
            RS.value = "False";
            RS.type = 0;
            DefSet.Add(RS);

            //udPaySummEntrance

            //updown
            //TarifIdEntrance
            //TarifPlanIdEntrance


            //public static bool QRVerificationExit = false;

        }
        public static void InsertDefaultRackSettings()
        {
            //run script if table isn't exists
            if (!IsTableExists())
            {
                //Create Table
                string script = File.ReadAllText(Application.StartupPath + "\\newracksettings.sql");

                // split script on GO command
                IEnumerable<string> queries = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                try
                {
                    using (SqlConnection conn = new SqlConnection(conlocal))
                    {

                        conn.Open();
                        foreach (string query in queries)
                        {
                            if (query.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(query, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка выполнения скрипта создания таблиц " + conlocal);
                }

                Thread.Sleep(1000);
                //Надо заполнить табличку
                
                Thread.Sleep(1000);
            }
            InsertTypesTable();
            List<RackSettings> AlreadyList = new List<RackSettings>();

            try
            {

                string sqlQuery = "SELECT * FROM DeviceRackSettings";
                SqlConnection connection = new SqlConnection(conlocal);
                connection.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlQuery, connection);
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds, "DeviceRackSettings");

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    GlobalClass.RackSettings rackrow = new GlobalClass.RackSettings();
                    rackrow.id = Convert.ToString(dr["ID"]);
                    rackrow.device_id = Convert.ToString(dr["DeviceID"]);
                    rackrow.name = Convert.ToString(dr["Name"]);
                    rackrow.value = Convert.ToString(dr["Value"]);
                    rackrow.type = Convert.ToInt32(dr["ValueType"]);
                    AlreadyList.Add(rackrow);
                }
                connection.Close();
            }
            catch (Exception exx)
            {
                Logging.ToLog("Ошибка при базовом подключении " + conlocal);
            }


            bool founded = false;
            //Сверяем списки с дефолтными:
            for (int i = 0; i < DefSet.Count; i++)
            {
                founded = false;
                for (int j = 0; j < AlreadyList.Count; j++)
                {
                    if (DefSet[i].name == AlreadyList[j].name && !String.IsNullOrEmpty(AlreadyList[j].id))
                    {
                        founded = true;
                    }
                }
                if (!founded)
                {
                    //Добавляем запись
                    InsertRackSettingsRecord(DefSet[i]);
                }
            }
        }
        public static void InsertRackSettingsRecord(RackSettings data)
        {
            string fields = "(ID, DeviceID, Name, Value, ValueType)";
            DateTime dt = new DateTime(2000, 1, 1);
            string values = "";
            values += "('" + data.id + "', '" + data.device_id + "', '" + data.name + "', '" + data.value + "', '" + data.type + "')";
            string sqlQuery = "INSERT INTO DeviceRackSettings" + " " + fields + " VALUES " + values;

            try
            {
                using (SqlConnection connection = new SqlConnection(conlocal))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlQuery, connection);
                    command.Parameters.AddWithValue("Sync", dt);
                    int number = command.ExecuteNonQuery();
                }
            }
            catch (Exception exx)
            {
                Logging.ToLog("Не смог записать настройку " + data.name + " " + conlocal + Environment.NewLine + sqlQuery);
            }
        }
       
        public static void SaveChanges(string whatSave)
        {
            if (RunBdOk != false)
            {
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ee)
                {
                    if (whatSave == null) whatSave = "Исключение при записи в БД :";
                    Logging.ToLog(whatSave + ee.ToString());
                }
            }
        }

        public static bool DisposeNewEntities()
        {
            #region // обновить БД
            try
            {
                db.Dispose(); // Удаляет контекст EF
                db = new Entities();
                return true;
            }

            catch (Exception e)
            {
                Logging.ToLog("Исключение при создании соединения с БД . " + e.ToString());
                return false;
            }
            #endregion
        }

        public static void ClearDevice()
        {
            using (SqlConnection connectdev = new SqlConnection(conlocal))
            {
                SqlBaseImport.Model.Utils.Work = false;
                while (SqlBaseImport.Model.Utils.Running)
                {
                    Thread.Sleep(100);
                }
                try
                {
                    connectdev.Open();
                    SqlBaseImport.Model.BaseSync.ClearDevice(connectdev);
                    //перезапуск синхронизатора
                    SqlBaseImport.Model.Utils.Sync(conserver, conlocal);
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Error in ReSync: " + " Exception: " + ex.Message);
                }
                SqlBaseImport.Model.Utils.Work = true;
            }
        }

        //Потом будет нужна
        public static bool IsPlatePhotoExists()
        {
            Int32 newProdID = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[PlatePhoto]')";
            using (SqlConnection conn = new SqlConnection(conlocal))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                try
                {
                    conn.Open();
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return true;
                }
            }
        }


    }
}
