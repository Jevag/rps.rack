﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static stoyka01.GlobalClass;

namespace stoyka01
{
    public class LanguageClass
    {
        public static void FillDefLang()
        {
            LangDefSet = new List<LangSettings>();

            //external (CSV)
            //оставить место - потом вытащим еще
            LangSettings L;
            L = new LangSettings();
            L.id = 10001;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = " Освободите|проезд|встречному|автомобилю";
            L.EN = "Please|yield|to oncoming|traffic";
            L.ZH = "让迎面而来的汽车通过";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 1;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10002;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Обслуживание|начато";
            L.EN = "Service|started";
            L.ZH = "开始服务";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 8;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10003;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Пожалуйста,|проезжайте";
            L.EN = "Please|pass";
            L.ZH = "请通过";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 2;
            L.EconomPicture = 2;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10004;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Проезд|закрыт";
            L.EN = "Entry|closed";
            L.ZH = "关闭通道";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10005;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Для|проезда|необходимо|подъехать|к стойке";
            L.EN = "Approach|the stand|to pass";
            L.ZH = "为了通行需要到柜台";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10006;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Терминал|временно|не|обслуживает";
            L.EN = "The|terminal|temporarily|out of|service";
            L.ZH = "终端暂时不提供服务";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10007;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Нажмите|кнопку";
            L.EN = "Press|button";
            L.ZH = "单击按钮";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);


            L = new LangSettings();
            L.id = 10008;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Нажмите|кнопку или|приложите|карту";
            L.EN = "Press|button|or apply|your card";
            L.ZH = "单击按钮或插卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10009;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Идет|распоз-навание|номерного|знака";
            L.EN = "Number|plate|detection|is in|processing";
            L.ZH = "正在识别车牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 8;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10010;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Приложите|карту";
            L.EN = "Apply|your|card";
            L.ZH = "插卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 6;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10011;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Выдача|карт|невозможна";
            L.EN = "Cards|cannot|be issued";
            L.ZH = "不发行卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10012;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Выдача|жетонов|невозможна";
            L.EN = "Tokens|cannot|be issued";
            L.ZH = "不发行号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10013;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Кончились|карты";
            L.EN = "Out|of cards";
            L.ZH = "卡用完了";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10014;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Кончились|жетоны";
            L.EN = "Out|of tokens";
            L.ZH = "号牌用完了";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10015;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Нет мест|на парковке";
            L.EN = "No open|parking|spots";
            L.ZH = "没有停车位";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10016;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте|или|приложите|карту";
            L.EN = "Insert|or apply|your card";
            L.ZH = "插卡或刷卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10017;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте|жетон или|приложите|карту";
            L.EN = "Insert your|token|or apply|your card";
            L.ZH = "插入号牌或刷卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10018;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте|парковочную|карту";
            L.EN = "Insert your|parking|card";
            L.ZH = "插入停车卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 7;
            LangDefSet.Add(L);            

            L = new LangSettings();
            L.id = 10019;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте|жетон|в жетоно-приемник";
            L.EN = "Insert your|token into|the token|receiver";
            L.ZH = "将号牌插入号牌接收器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10020;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте|карту|в карто-приемник";
            L.EN = "Insert your|card into|the card|receiver";
            L.ZH = "将卡插入卡片接收器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 7;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10021;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте|парковочный|билет";
            L.EN = "Insert your|parking|ticket";
            L.ZH = "插入停车票";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 7;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10022;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Пожалуйста,|подождите";
            L.EN = "Please wait";
            L.ZH = "请稍等";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 8;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10023;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочную|карту";
            L.EN = "Take your|parking|card";
            L.ZH = "请拿走停车卡和支付的钱"; //косяк!!!
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 9;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10024;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочный|жетон";
            L.EN = "Take your|parking|token";
            L.ZH = "请拿出停车号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10025;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочный|билет";
            L.EN = "Take your|parking|ticket";
            L.ZH = "请拿出停车票";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 9;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10026;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Приглашение|активи-ровано";
            L.EN = "Invitation|activated";
            L.ZH = "邀请激活";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10027;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Необходимо|доплатить";
            L.EN = "You need|to pay|extra";
            L.ZH = "需要额外支付钱";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10028;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Проезд|запрещен|по гос.|номеру";
            L.EN = "No passage|with this|state|number";
            L.ZH = "国家牌号禁止通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10029;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "При|необходи-мости|обратитесь|к оператору";
            L.EN = "Call the|operator";
            L.ZH = "联系运营商";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10030;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Дождитесь|распоз-навания|номера";
            L.EN = "Wait for|the license|plate|identi-fication";
            L.ZH = "等待识别号码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 8;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10031;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Проезд|запрещен";
            L.EN = "No entry";
            L.ZH = "禁止通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10032;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Гос. номер|не|совпадает";
            L.EN = "State|number does|not match";
            L.ZH = "国家牌号不符合";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10033;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Выезд по|штрафной|карте|невозможен";
            L.EN = "Exit is|impossible|with the|penalty|card";
            L.ZH = "不能用罚卡离开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10034;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Выезд по|штрафному|жетону|невозможен";
            L.EN = "Exit is|impossible|with the|penalty|token";
            L.ZH = "不能用罚款号牌离开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10035;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Карта|заблоки-рована";
            L.EN = "Card is|blocked";
            L.ZH = "卡已锁定";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10036;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Жетон|заблокирован";
            L.EN = "Token is|blocked";
            L.ZH = "号牌已锁定";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10037;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Нет карты|в базе|данных";
            L.EN = "No card|in the|database";
            L.ZH = "数据库中没有此卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10038;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Нет жетона|в базе|данных";
            L.EN = "No token|in the|database";
            L.ZH = "数据库中没有此号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10039;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Проезд|запрещен|вашей|группе|клиентов";
            L.EN = "No entry|to your|clients|group";
            L.ZH = "您的客户群禁止通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10040;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "СЕРВИС";
            L.EN = "SERVICE";
            L.ZH = "服务";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10041;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ДИАГНОСТИКА";
            L.EN = "DIAGNOSTICS";
            L.ZH = "诊断";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10042;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "НАСТРОЙКИ";
            L.EN = "SETTINGS";
            L.ZH = "选项";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10043;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ДОП НАСТРОЙКИ";
            L.EN = "ADDITIONAL SETTINGS";
            L.ZH = "高级设置";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10044;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ОТЛАДКА";
            L.EN = "FINE-TUNING";
            L.ZH = "调试";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10046;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Статус";
            L.EN = "Status";
            L.ZH = "状态";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10047;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Статус внешних устройств:";
            L.EN = "External device status:";
            L.ZH = "外部设备状态：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10048;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Статус внутренних устройств:";
            L.EN = "Internal device status:";
            L.ZH = "内部设备状态：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10049;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "loopA свободна";
            L.EN = "Loop A free";
            L.ZH = "loopA空闲";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10050;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "loopA занята";
            L.EN = "Loop A occupied";
            L.ZH = "loopA占用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10051;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ИК свободен";
            L.EN = "MC Free";
            L.ZH = "感应测井空闲";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10052;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ИК занят";
            L.EN = "MC Occupied";
            L.ZH = "感应测井占用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10053;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "loopB свободна";
            L.EN = "Loop B free";
            L.ZH = "loopB空闲";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10054;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "loopB занята";
            L.EN = "Loop B occupied";
            L.ZH = "loopB占用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10055;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "НАЖМИТЕ";
            L.EN = "PRESS";
            L.ZH = "点击";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10056;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "кнопка НАЖМИТЕ не нажата";
            L.EN = "PRESS button is not pressed";
            L.ZH = "未点击按钮";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10057;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "НАЖАТА";
            L.EN = "PRESSED";
            L.ZH = "点击";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10058;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "кнопка НАЖМИТЕ нажата";
            L.EN = "PRESS button is pressed";
            L.ZH = "按下按钮";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10059;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "barrier закрывается";
            L.EN = "barrier is closing";
            L.ZH = "障碍关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10060;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "barrier открывается";
            L.EN = "barrier is opening";
            L.ZH = "障碍打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10061;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "barrier закрыт";
            L.EN = "barrier is closed";
            L.ZH = "障碍关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10062;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "barrier открыт";
            L.EN = "barrier is open";
            L.ZH = "障碍打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10063;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "светофор зеленый";
            L.EN = "green light";
            L.ZH = "绿灯";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10064;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "светофор красный";
            L.EN = "red light";
            L.ZH = "红灯";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10065;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "верхняя дверь открыта";
            L.EN = "upper door open";
            L.ZH = "顶门打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10066;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "нижняя дверь открыта";
            L.EN = "lower door open";
            L.ZH = "底门打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10067;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "верхняя дверь закрыта";
            L.EN = "upper door closed";
            L.ZH = "顶门关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10068;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "нижняя дверь закрыта";
            L.EN = "lower door closed";
            L.ZH = "底门关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10069;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "кнопка ВЫЗОВ не нажата";
            L.EN = "CALL button is not pressed";
            L.ZH = "未按下呼叫按钮";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10070;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "кнопка ВЫЗОВ нажата";
            L.EN = "CALL button is pressed";
            L.ZH = "按下呼叫按钮";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10072;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Оборудование";
            L.EN = "Equipment";
            L.ZH = "设备";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10073;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Дисплей";
            L.EN = "Screen";
            L.ZH = "显示器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10074;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Штрихкод";
            L.EN = "Bar code";
            L.ZH = "二维码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10075;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Диспенсер";
            L.EN = "Dispenser";
            L.ZH = "出租汽车停车场";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10076;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ридер внутренний";
            L.EN = "Internal reader";
            L.ZH = "内部读出器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10077;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ридер внешний";
            L.EN = "External reader";
            L.ZH = "外部读出器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10078;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Slave контроллер";
            L.EN = "Slave controller";
            L.ZH = "Slave控制器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10080;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Тип стойки";
            L.EN = "Type of stand";
            L.ZH = "柜台类型";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10081;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Въезд";
            L.EN = "Entry";
            L.ZH = "不允许进入";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10082;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выезд";
            L.EN = "Exit";
            L.ZH = "离开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10083;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Переезд";
            L.EN = "Crossing";
            L.ZH = "道口";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10085;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Режим стойки";
            L.EN = "Stand mode";
            L.ZH = "柜台模式";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10086;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Блокировать проезд";
            L.EN = "Block entry";
            L.ZH = "封锁通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10087;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Свободный проезд";
            L.EN = "Free entry";
            L.ZH = "自由通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10088;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Штатный режим";
            L.EN = "General mode";
            L.ZH = "常规模式";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10090;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Порты Slave дискретные";
            L.EN = "Discrete Slave ports";
            L.ZH = "从属离散端口";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10091;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "loop, шлагбаум";
            L.EN = "loop, road block";
            L.ZH = "循环，拦路杆";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10092;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вызов, двери";
            L.EN = "Call, doors";
            L.ZH = "呼叫，门";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10093;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Высотомеры, реверс (Петли2, шлаг2)";
            L.EN = "Altimeters, reverse (Loops2, barrier2)";
            L.ZH = "高度计，反向";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10094;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Зона после";
            L.EN = "Zone after";
            L.ZH = "之后的区域";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10095;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Зона до";
            L.EN = "Zone before";
            L.ZH = "之前的区域";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10096;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "COM порт";
            L.EN = "COM port";
            L.ZH = "COM端口";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10098;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Группа разового";
            L.EN = "Single group";
            L.ZH = "单次小组";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10099;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ID ТП разовый";
            L.EN = "Single TP ID";
            L.ZH = "单次 TP ID";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10100;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ID ТС разовый";
            L.EN = "Single TS ID";
            L.ZH = "单次 TS ID";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10101;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Номер сектора";
            L.EN = "Sector No.";
            L.ZH = "部门编号";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10102;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Длительность импульса  управления шлагбаумом  (х100мсек.)";
            L.EN = "Road block impulse control length (x 100 msec)";
            L.ZH = "控制脉冲屏障的持续时间（x100毫秒";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10103;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Время не ИК  (х100мсек)";
            L.EN = "Time not MC (x 100 msec)";
            L.ZH = "时间不是感应测井（x100毫秒";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10104;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Время не loopA  (х100мсек)";
            L.EN = "Time not Loop A (x 100 msec)";
            L.ZH = " 时间不是loopA  (х100毫秒)";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10105;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Время карты в губах (сек.)";
            L.EN = "Time card in lips (sec)";
            L.ZH = "卡片读出的时间（秒";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10107;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Настройка лога";
            L.EN = "Log setup";
            L.ZH = "日志设置";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10108;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "только ошибки";
            L.EN = "only errors";
            L.ZH = "只有错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10109;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "все изменения";
            L.EN = "all changes";
            L.ZH = "所有更改";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10111;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext?
            L.MaxLength = 0; //default
            L.RU = "Распознавание гос. номера";
            L.EN = "State number detection";
            L.ZH = "识别国家号码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10112;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext?
            L.MaxLength = 0; //default
            L.RU = "Не используется";
            L.EN = "Not used";
            L.ZH = "不使用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10113;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext?
            L.MaxLength = 0; //default
            L.RU = "Всегда";
            L.EN = "Always";
            L.ZH = "总是";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10114;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext?
            L.MaxLength = 0; //default
            L.RU = "Въезд-Выезд";
            L.EN = "Entry-Exit";
            L.ZH = "进入－离开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10115;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext?
            L.MaxLength = 0; //default
            L.RU = "Пропуск по номеру";
            L.EN = "Entry by number";
            L.ZH = "通行号码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10116;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext?
            L.MaxLength = 0; //default
            L.RU = "Для информации";
            L.EN = "For information";
            L.ZH = "信息";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10118;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет карт - красный/зеленый";
            L.EN = "No cards - red/green";
            L.ZH = "没有卡片 - 红色/绿色";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10119;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет карт - красный";
            L.EN = "No cards - red";
            L.ZH = "没有卡片 - 红色";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10120;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет карт - зеленый";
            L.EN = "No cards - green";
            L.ZH = "没有卡片 - 绿色";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10122;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Инверсия";
            L.EN = "Inversion";
            L.ZH = "转变";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10123;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите";
            L.EN = "Press";
            L.ZH = "点击";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10124;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ИК";
            L.EN = "MC";
            L.ZH = "感应测井";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10125;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Петля Б";
            L.EN = "Loop B";
            L.ZH = "循环B";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10126;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Петля А";
            L.EN = "Loop A";
            L.ZH = "循环A";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10129;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Итог по клику";
            L.EN = "Total upon click";
            L.ZH = "单击总计";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10130;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Отладка";
            L.EN = "Fine-tuning";
            L.ZH = "调试";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10131;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Имитация";
            L.EN = "Imitation";
            L.ZH = "模仿";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10132;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Работает";
            L.EN = "Available";
            L.ZH = "工作";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10133;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Не работает";
            L.EN = "Not available";
            L.ZH = "不工作";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10135;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ожидание";
            L.EN = "waiting";
            L.ZH = "期望";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10136;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "статус не определен";
            L.EN = "status not determined";
            L.ZH = "位置不确定";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10137;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "не отвечает";
            L.EN = "does not reply";
            L.ZH = "无应答";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10138;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "карта под ридером";
            L.EN = "card under reader";
            L.ZH = "读卡器下的卡片";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10139;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "карта застряла";
            L.EN = "card stuck";
            L.ZH = "卡卡住了";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10140;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "нет карт";
            L.EN = "no cards";
            L.ZH = "没有卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10141;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "мало карт";
            L.EN = "few cards";
            L.ZH = "卡太少";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10142;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "есть карты";
            L.EN = "cards available";
            L.ZH = "有卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10143;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "нет карты";
            L.EN = "no card";
            L.ZH = "无卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10144;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "есть карта";
            L.EN = "card available";
            L.ZH = "有卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10145;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "карта в губах";
            L.EN = "card in lips";
            L.ZH = "唇卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10146;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "нет карты в тракте";
            L.EN = "no card in the tract";
            L.ZH = "系统中没有此卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10147;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "карта прочитана";
            L.EN = "card read";
            L.ZH = "卡已读";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10148;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ошибка чтения";
            L.EN = "reading error";
            L.ZH = "读卡错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10149;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "есть ID";
            L.EN = "ID available";
            L.ZH = "有 ID";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10150;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "нет ID";
            L.EN = "ID not available";
            L.ZH = "无 ID";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10151;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "чужая карта";
            L.EN = "someone else’s card";
            L.ZH = "其它卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10152;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "карта записана";
            L.EN = "card recorded";
            L.ZH = "记录卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10153;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ошибка записи";
            L.EN = "recording error";
            L.ZH = "记录错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10155;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Тип клиента";
            L.EN = "Type of client";
            L.ZH = "客户类型";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10156;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Разовый";
            L.EN = "One time";
            L.ZH = "一次性";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10157;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Постоянный";
            L.EN = "Regular";
            L.ZH = "经常性";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10158;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вездеход";
            L.EN = "Cross-country vehicle";
            L.ZH = "自由出入通行证";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10160;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Время оплаты";
            L.EN = "Payment time";
            L.ZH = "支付时间";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10161;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Пропуск авто";
            L.EN = "Vehicle entry";
            L.ZH = "自动通过";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10162;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Сумма";
            L.EN = "Amount";
            L.ZH = "钱款";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10163;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Время въезда";
            L.EN = "Entry time";
            L.ZH = "进入时间";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10164;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ID ТП";
            L.EN = "ID TP";
            L.ZH = "TP ID";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10165;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ID TC";
            L.EN = "ID TC";
            L.ZH = "交通ID";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10166;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Зона";
            L.EN = "Zone";
            L.ZH = "地区";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10167;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Группа";
            L.EN = "Group";
            L.ZH = "团队";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10168;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ID карты";
            L.EN = "Card ID";
            L.ZH = "ID卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10169;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Итог";
            L.EN = "Total";
            L.ZH = "共计";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10170;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Этап";
            L.EN = "Phase";
            L.ZH = "步骤";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10173;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Принято от ридера";
            L.EN = "Received from reader";
            L.ZH = "从读出器接收";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10174;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Запись";
            L.EN = "Recording";
            L.ZH = "记录";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10175;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Чтение";
            L.EN = "Reading";
            L.ZH = "阅读";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10176;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ключ В";
            L.EN = "Key B";
            L.ZH = "钥匙 В";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10177;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ключ А";
            L.EN = "Key A";
            L.ZH = "钥匙А";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10178;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ID Card";
            L.EN = "ID Card";
            L.ZH = "ID卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10179;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "GetCard";
            L.EN = "GetCard";
            L.ZH = "GetCard";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10180;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "GetId";
            L.EN = "GetId";
            L.ZH = "GetId";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10182;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Карту в отказник";
            L.EN = "Card rejected";
            L.ZH = "卡不能用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10183;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выбросить карту";
            L.EN = "Throw out the card";
            L.ZH = "扔掉卡片";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10184;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выдать в губы";
            L.EN = "Issue in the lips";
            L.ZH = "人泄露";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10185;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выдать под ридер";
            L.EN = "Issue under reader";
            L.ZH = "读出器泄露";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10186;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Статус диспенсера";
            L.EN = "Dispenser status";
            L.ZH = "分配器状态";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10188;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кнопка ВЫЗОВ";
            L.EN = "CALL button";
            L.ZH = "呼叫键";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10189;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "VersionSlave";
            L.EN = "VersionSlave";
            L.ZH = "VersionSlave";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10190;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Светофор красный";
            L.EN = "Red light";
            L.ZH = "红灯";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10191;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Светофор зеленый";
            L.EN = "Green light";
            L.ZH = "绿灯";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10192;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыть шлагбаум";
            L.EN = "Close road block";
            L.ZH = "拦路杆关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10193;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Открыть шлагбаум";
            L.EN = "Open road block";
            L.ZH = "拦路杆打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10195;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Чистка базы";
            L.EN = "Database cleaned";
            L.ZH = "清理数据";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10196;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Группы из файла";
            L.EN = "Groups from file";
            L.ZH = "文件群";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10197;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Делать окно поверх";
            L.EN = "Make window over";
            L.ZH = "在上面做一个窗口";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10198;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Делать окно не поверх";
            L.EN = "Make window under";
            L.ZH = "不在顶端做窗口";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10199;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "сохранить";
            L.EN = "save";
            L.ZH = "保存";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10200;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "URL сервера:";
            L.EN = "Server URL:";
            L.ZH = "URL 服务器：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10201;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ключ";
            L.EN = "Key";
            L.ZH = "钥匙";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10202;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Всё для ...";
            L.EN = "Everything for...";
            L.ZH = "都是为了……";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10204;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Имитация, если";
            L.EN = "Imitation, if";
            L.ZH = "模仿，如果";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10205;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "устройство отключено";
            L.EN = "device switched off";
            L.ZH = "设备关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10208;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет связи с внешним ридером. Проверьте подключение, питание и работоспособность устройства.";
            L.EN = "No connection to the external reader. Check connection, power, and operational performance capability of the device.";
            L.ZH = "与外部读出器无关检查设备的连接，电源和工作能力。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10209;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ошибка чтения/записи во внешнем ридере";
            L.EN = "Reading/Recording error in the external reader";
            L.ZH = "外部读出器读/记录错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10210;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет связи с внутренним ридером. Проверьте подключение, питание и работоспособность устройства.";
            L.EN = "No connection to the internal reader. Check connection, power, and operational performance capability of the device.";
            L.ZH = "与内部读出器没有无关检查设备的连接，电源和工作能力。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10211;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ошибка чтения/записи во внутреннем ридере";
            L.EN = "Reading/Recording error in the internal reader";
            L.ZH = "内部读出器读/记录错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10212;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Карты в диспенсере карт подходят к концу. Загрузите в тубус новые карты.";
            L.EN = "Card dispenser is nearly out of cards, Load new cards into the tube.";
            L.ZH = "卡片分配器中的卡片用完了。将新卡安装到镜筒中。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10213;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "В диспенсере карт закончились карты. Загрузите в тубус новые карты.";
            L.EN = "Card dispenser is out of cards. Load new cards into the tube.";
            L.ZH = "卡片分配器中没有卡片。将新卡安装到镜筒中。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10214;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "В диспенсере карт застряла карта. Требуется присутствие оператора!";
            L.EN = "Card is stuck in the card dispenser. Operator presence is required!";
            L.ZH = "卡片分配器中的卡片卡住了。需要操作员到场！";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10215;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет связи с диспенсером карт. Устройство не работает. Проверьте устройство.";
            L.EN = "No connection to card dispenser. The device is out of service. Check the device.";
            L.ZH = "没有与卡片分配器连接。设备不工作检查设备。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10216;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет связи со слейв контроллером. Проверьте подключение, питание и работоспособность платы.";
            L.EN = "No connection with the slave controller. Check connection, power, and operational performance capability of the circuit.";
            L.ZH = "没有与从控制器连接。检查电路板的连接，电源和工作能力。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10217;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ошибка Дискрета";
            L.EN = "Discrete Error";
            L.ZH = "样件错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10218;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет питания 220В. Работа от UPS.";
            L.EN = "No 220 V power. UPS powered";
            L.ZH = "没有220V的电源UPS运行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10219;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Верхняя дверь открыта";
            L.EN = "Upper door open";
            L.ZH = "顶门打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10220;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нижний люк открыт";
            L.EN = "Lower manhole open";
            L.ZH = "底部舱口开放";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10221;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проблемы с датчиками (петли, ИК)";
            L.EN = "Problems with sensors (loops, MC)";
            L.ZH = "传感器问题（循环，感应测井）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10222;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проблемы со шлагбаумом";
            L.EN = "Problems with road block";
            L.ZH = "拦路杆的问题";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10225;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Есть связь с внешним ридером";
            L.EN = "Connection to the external reader available";
            L.ZH = "与内部读出器有关";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10226;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Есть связь с ридером";
            L.EN = "Connection to the reader available";
            L.ZH = "与读出器有关";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10227;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "чтение/запись";
            L.EN = "reading/recording";
            L.ZH = "读/记录";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10228;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "есть карты";
            L.EN = "cards available";
            L.ZH = "有卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10229;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Есть связь с диспенсером";
            L.EN = "Dispenser connected";
            L.ZH = "与分配器有关";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10230;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Есть связь cо Slave";
            L.EN = "Slave connected";
            L.ZH = "与Slave有关";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10231;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "верхняя дверь закрыта";
            L.EN = "upper door closed";
            L.ZH = "顶门关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10232;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "нижняя дверь закрыта";
            L.EN = "lower door closed";
            L.ZH = "底门关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10235;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "транзакция";
            L.EN = "transaction";
            L.ZH = "处理";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10236;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Штатный проезд";
            L.EN = "Standard passage";
            L.ZH = "员工通道";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10237;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд с отбоем стрелы";
            L.EN = "Passage with crane arm signal";
            L.ZH = "撤回通道";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10238;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "проезд  в режиме свободный";
            L.EN = "free passage";
            L.ZH = "自由通道";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10239;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "отказ от проезда";
            L.EN = "passage canceled";
            L.ZH = "拒绝通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10240;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "кража карты";
            L.EN = "card stolen";
            L.ZH = "卡被盗";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10241;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Попытка проезда - не та зона";
            L.EN = "Passage attempted - wrong zone";
            L.ZH = "尝试通行   - 不是该区域";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10242;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Попытка проезда - стоп-лист (заблокирована)";
            L.EN = "Passage attempted - stop list (blocked)";
            L.ZH = "尝试通行   - 停止列表（已锁定）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10243;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Попытка проезда - не та группа";
            L.EN = "Passage attempted - wrong group";
            L.ZH = "尝试通行   - 不是该组";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10244;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Попытка неоплаченного проезда";
            L.EN = "Unpaid passage attempted";
            L.ZH = "尝试无偿通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10245;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Попытка проезда на переполненную парковку";
            L.EN = "Passage onto full parking lot attempted";
            L.ZH = "尝试开车到拥挤的停车场";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10246;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Попытка проезда - не совпадение номера на выезде";
            L.EN = "Passage attempted - wrong number at exit";
            L.ZH = "尝试通行 - 出口处没有相应的号码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10247;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Попытка проезда - ТС в чёрном списке";
            L.EN = "Passage attempted - vehicle in black list";
            L.ZH = "尝试通行 -  车辆在黑名单上";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10248;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд паровозом";
            L.EN = "Tailgating passage";
            L.ZH = "蒸汽机车通道";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10250;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет лицензии";
            L.EN = "No license";
            L.ZH = "没有许可证";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10251;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Есть лицензия";
            L.EN = "License available";
            L.ZH = "有许可证";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10252;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Петля А долго занята";
            L.EN = "Loop A occupied for a long period of time";
            L.ZH = "循环A长期占用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10253;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Петля В долго занята";
            L.EN = "Loop B occupied for a long period of time";
            L.ZH = "循环B长期占用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10254;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ИК долго занят";
            L.EN = "MC occupied for a long time";
            L.ZH = "感应测井长期占用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10255;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Петля А не занята";
            L.EN = "Loop A is free";
            L.ZH = "循环A不占用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10256;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Петля В не занята";
            L.EN = "Loop B is free";
            L.ZH = "循环B不占用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10257;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ИК не занят";
            L.EN = "MC is free";
            L.ZH = "感应测井不占用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10260;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ПРОГРАММА ПРОВЕРКИ";
            L.EN = "CHECK PROGRAM";
            L.ZH = "检查程序";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10261;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ридер внешний";
            L.EN = "External reader";
            L.ZH = "外部读出器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10262;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Наличие связи";
            L.EN = "Connection is present";
            L.ZH = "有联系";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10263;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Приложите карту";
            L.EN = "Apply your card";
            L.ZH = "插卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10264;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Чтение карты/Запись карты";
            L.EN = "Card reading/Card recording";
            L.ZH = "读卡/记录卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10266;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Внутренний ридер";
            L.EN = "Internal reader";
            L.ZH = "内部读出器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10268;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Диспенсер карт";
            L.EN = "Card dispenser";
            L.ZH = "卡片机";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10269;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Убрать все карты";
            L.EN = "Remove all cards";
            L.ZH = "删除所有卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10270;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка датчика отсутствия карт";
            L.EN = "Check “No Cards” Sensor";
            L.ZH = "检查丢失卡片的传感器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10271;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Положите 5 карт";
            L.EN = "Place 5 cards";
            L.ZH = "放5张卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10272;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка датчика мало карт";
            L.EN = "Checking “Few Cards” Sensor";
            L.ZH = "检查卡片少的传感器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10273;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Положите 30 карт";
            L.EN = "Place 30 cards";
            L.ZH = "放30张卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10274;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка датчика мало карт";
            L.EN = "Checking “Few Cards” Sensor";
            L.ZH = "检查卡片少的传感器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10275;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выдача карты";
            L.EN = "Issue of card";
            L.ZH = "发行卡片";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10276;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите карту и вставьте обратно.";
            L.EN = "Take your card and insert it back";
            L.ZH = "拿出卡片并将其插回。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10277;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Прием карты под ридер.";
            L.EN = "Card inserted under the reader";
            L.ZH = "读卡器接收卡片。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10278;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Отправка карты в Отказник.";
            L.EN = "Card sent to Refuse";
            L.ZH = "将卡发送给不许出境者。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10280;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Slave контроллер";
            L.EN = "Slave controller";
            L.ZH = "Slave控制器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10282;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кнопка вызова оператора.";
            L.EN = "Operator call button.";
            L.ZH = "呼叫操作员的按钮。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10283;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите кнопку вызова оператора";
            L.EN = "Press operator call button";
            L.ZH = "点击呼叫操作员的按钮";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10284;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кнопка нажата";
            L.EN = "Button pressed";
            L.ZH = "按下按钮";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10286;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кнопка выдачи карт";
            L.EN = "Card issuing button";
            L.ZH = "发行卡片的按钮";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10287;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите кнопку выдачи карты";
            L.EN = "Press card issuing button";
            L.ZH = "点击发行卡片的按钮";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10288;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Подсветка работает?";
            L.EN = "Does lighting work?";
            L.ZH = "照明灯工作？";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10289;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите кнопку вызова ГГС до истечения таймера, если подсветка кнопки выдачи карты работает корректно.";
            L.EN = "Press specialist call button before time runs out, if the card issue button is lit.";
            L.ZH = "如果卡片发行按钮的照明灯正常工作，在计时器到期之前按CGS按钮。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10291;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка датчиков дверей";
            L.EN = "Checking door sensors";
            L.ZH = "检查门的传感器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10292;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закройте все двери.";
            L.EN = "Close all doors.";
            L.ZH = "关上所有门。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10293;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Откройте правую дверь.";
            L.EN = "Open right door";
            L.ZH = "打开右门。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10294;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закройте правую дверь.";
            L.EN = "Close right door.";
            L.ZH = "关上右门。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10295;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Откройте левую дверь.";
            L.EN = "Open left door.";
            L.ZH = "打开左门。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10296;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закройте левую дверь";
            L.EN = "Close left door.";
            L.ZH = "关上左门。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10297;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Откройте люк.";
            L.EN = "Open manhole.";
            L.ZH = "打开舱门。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10298;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закройте люк.";
            L.EN = "Close manhole.";
            L.ZH = "关闭舱门。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10300;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Светофор";
            L.EN = "Traffic light";
            L.ZH = "交通灯";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10301;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Светофор горит зеленым";
            L.EN = "Green light is on";
            L.ZH = "交通灯是绿色的";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10302;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите кнопку вызова ГГС до истечения таймера, если все диоды светофора горят зеленым.";
            L.EN = "Press specialist call button before time runs out, if all light diodes are lit green.";
            L.ZH = "如果所有的交通灯二极管都是绿色的，在计时器到期之前按CGS呼叫按钮。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10303;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Светофор горит красным";
            L.EN = "Red light is on";
            L.ZH = "交通灯是红色的";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10304;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите кнопку вызова ГГС до истечения таймера, если все диоды светофора горят красным.";
            L.EN = "Press specialist call button before time runs out, if all light diodes are lit red.";
            L.ZH = "如果所有的交通灯二极管都是红色的，在计时器到期之前按CGS呼叫按钮。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10306;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка сигналов дискретных входов Slave контроллера";
            L.EN = "Check of discrete input points of Slave controller";
            L.ZH = "检查Slave控制站数字量输入的信号";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10307;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Подключите пульт имитации проезда";
            L.EN = "Turn in passage imitation switchboard";
            L.ZH = "连接通行模拟控制台";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10308;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите LOOP A до истечения  таймера";
            L.EN = "Press LOOP A before timer runs out";
            L.ZH = "在计时器到期之前按LOOP A.";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10309;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка LOOP A";
            L.EN = "Checking LOOP A";
            L.ZH = "检查LOOP A.";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10310;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите ИК до истечения  таймера";
            L.EN = "Press IC before timer runs out";
            L.ZH = "在计时器到期之前按感应测井";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10311;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка ИК";
            L.EN = "Checking IC";
            L.ZH = "检查感应测井";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10312;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите LOOP B до истечения  таймера";
            L.EN = "Press LOOP B before timer runs out";
            L.ZH = "在计时器到期之前按 LOOP B";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10313;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка LOOP B";
            L.EN = "Checking LOOP B";
            L.ZH = "检查 LOOP B";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10315;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка сигнала на открытие шлагбаума";
            L.EN = "Checking road block opening signal";
            L.ZH = "在打开拦路杆时检查信号";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10316;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите кнопку вызова ГГС до истечения таймера, если светодиод открытия шлагбаума на имитаторе загорелся зеленым.";
            L.EN = "Press specialist call button before time runs out, if the road block opening diode on the imitator is lit green.";
            L.ZH = "如果在模拟器上打开拦路杆时LED变为绿色，则在计时器到期之前按下HGS的呼叫按钮。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10317;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка сигнала на закрытие шлагбаума";
            L.EN = "Checking road block opening signal";
            L.ZH = "在关闭拦路杆时检查信号";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10318;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите кнопку вызова ГГС до истечения таймера, если светодиод закрытия шлагбаума  на имитаторе загорелся красным.";
            L.EN = "Press specialist call button before time runs out, if the road block closing diode on the imitator is lit red.";
            L.ZH = "如果在模拟器上关闭拦路杆时LED变为红色，则在计时器到期之前按下HGS的呼叫按钮。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10320;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка устройств ГГС";
            L.EN = "Checking specialist system";
            L.ZH = "检查GGS设备";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10321;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка динамика";
            L.EN = "Checking loudspeakers";
            L.ZH = "检查扬声器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10322;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите кнопку вызова ГГС до истечения таймера, если аудио воспроизводится корректно.";
            L.EN = "Press specialist call button before time runs out, if the audio plays back correctly.";
            L.ZH = "如果正确播放音频，请在计时器到期前按GGS呼叫按钮。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10323;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Настройка громкости усилителя";
            L.EN = "Adjusting amplifier volume";
            L.ZH = "调整放大器音量";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10324;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверка ГГС завершена.";
            L.EN = "Specialist system checking is complete.";
            L.ZH = "GGS检查完成。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10327;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Пуск";
            L.EN = "Start-up";
            L.ZH = "启动";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10328;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "В начало";
            L.EN = "Go back to start";
            L.ZH = "开始";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            /*
            L = new LangSettings();
            L.id = 10331;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "или";
            L.EN = "or";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10332;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "используйте";
            L.EN = "use";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10333;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Возьмите";
            L.EN = "Take your";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10334;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "и вставьте карту";
            L.EN = "card and insert it";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10335;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Штрих-код";
            L.EN = "Barcode";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10336;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Этот штрих-код";
            L.EN = "This barcode";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10337;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Закончился срок";
            L.EN = "Term is over";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10339;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Въезд невозможен";
            L.EN = "Entry is impossible";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10340;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Превышен лимит";
            L.EN = "You have reached your limit of";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10341;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "мест для вашей";
            L.EN = "places for your";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10342;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "компании";
            L.EN = "company";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);
            */

            L = new LangSettings();
            L.id = 10343;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Нет карты|в базе";
            L.EN = "No card|in the|database";
            L.ZH = "Нет карты в базе";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);
            /*
            L = new LangSettings();
            L.id = 10344;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Обслуживание начато";
            L.EN = "Servicing started";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10346;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "ИСПОЛЬЗУЙТЕ";
            L.EN = "USE";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10347;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "банковскую карту";
            L.EN = "bank card";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);
            */

            L = new LangSettings();
            L.id = 10350;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Не исполь-зуется";
            L.EN = "Not used";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10351;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Определение|тарифа";
            L.EN = "Determi-nation|of the|rate";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10352;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Пропуск|транспо-ртного|средства";
            L.EN = "Letting|through|the vehicle";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            /*
            L = new LangSettings();
            L.id = 10354;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "обратитесь к оператору";
            L.EN = "Call the operator";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10355;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "подъехать к стойке";
            L.EN = "Approach the stand";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10356;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "карты";
            L.EN = "cards";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10357;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "карта";
            L.EN = "card";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10358;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "штрих-код";
            L.EN = "barcode";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10359;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "действия";
            L.EN = "action";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10360;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "штрих-кода";
            L.EN = "barcode";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10361;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "уже используется";
            L.EN = "is being used";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10362;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "не действителен";
            L.EN = "invalid";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10363;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Дождитесь";
            L.EN = "Wait until";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10364;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "распознавания номера";
            L.EN = "the number plate is detected";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10365;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Распознавание";
            L.EN = "Number plate";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10366;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "номерного знака";
            L.EN = "detection";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10367;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Номер автомобиля распознан";
            L.EN = "Vehicle number detected";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10368;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Распознавание";
            L.EN = "Detection";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10369;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Проезд по RFID невозможен";
            L.EN = "RFID passage is impossible";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10370;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "воспользуйтесь картой";
            L.EN = "use card";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10371;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Проезд по RFID заблокирован";
            L.EN = "RFID passage is blocked";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10372;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Не та зона по RFID";
            L.EN = "Wrong RFID zone";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10373;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Не та группа по RFID";
            L.EN = "Wrong RFID group";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10374;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Задолженность по RFID";
            L.EN = "RFID overdue payments";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10375;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Приглашение отсутствует";
            L.EN = "No invitation";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10376;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "обратитесь к оператору";
            L.EN = "Call the operator";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10377;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Выдача карты невозможна";
            L.EN = "Card cannot be issued";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10378;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Нет мест на парковке.";
            L.EN = "No open parking spot.";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10379;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Выдача приглашения невозможна";
            L.EN = "Invitation cannot be issued";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10380;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "приглашение не найдено";
            L.EN = "invitation not found";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10381;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Приглашение активировано";
            L.EN = "Invitation activated";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10382;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "или обратитесь к оператору.";
            L.EN = "or call the operator";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10383;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Кончились карты.";
            L.EN = "Out of cards.";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10384;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "парковочный жетон";
            L.EN = "parking token";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10385;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "парковочный билет";
            L.EN = "parking ticket";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10386;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте жетон";
            L.EN = "Insert the token";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10387;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте билет";
            L.EN = "Insert the ticket";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10388;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте жетон или";
            L.EN = "Insert the token or";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10389;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Выдача жетона невозможна";
            L.EN = "Token cannot be issued";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10390;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Кончились жетоны.";
            L.EN = "Out of token.";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10391;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Жетон заблокирован";
            L.EN = "Token is blocked";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10392;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Нет жетона в базе.";
            L.EN = "No token in the database.";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10393;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Не системный";
            L.EN = "An irregular";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10394;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "жетон";
            L.EN = "token";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10395;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "и вставьте жетон";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10396;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Заберите чек";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10397;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "для выезда";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10398;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Дождитесь";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10399;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "печати чека";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10400;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Проезд невозможен";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10401;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "подробности в приложении";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);
            */

            L = new LangSettings();
            L.id = 10402;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Ниппель";
            L.EN = "Connector|plug";
            L.ZH = "内接头";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);
            /*
            L = new LangSettings();
            L.id = 10403;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Несоответствие тега";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10404;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Дождитесь распознавания или обратитесь к оператору";
            L.EN = "Please wait for recognize or call the operator";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10405;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Сервер недоступен";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10406;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Невозможно списать средства";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10407;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "с общего счёта компании";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10408;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Пожалуйста, оплатите";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10409;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Номер на штрафной карте";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10410;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "не совпадает";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10411;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "с номером ТС";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10412;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "подъехать";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10413;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "к стойке";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10414;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "ТЕРМИНАЛ";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10415;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "ВРЕМЕННО";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10416;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте жетон";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10417;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "в отверстие";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10418;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "выше";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10419;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте жетон выше";
            L.EN = "PUT THE TAG UPPER";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10420;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Приложите";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10421;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "карту";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10422;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "парковочную";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10423;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "парковочный";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10424;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "жетон";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10425;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "билет";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10426;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Проезд";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10427;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "запрещён";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10428;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "уже";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10429;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "используется";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10430;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Заберите";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10431;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "и вставьте его";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10432;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "в жетоноприемник";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10433;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "для проезда";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10434;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Неверный";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10435;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Вставьте жетон,";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10436;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "который был";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10437;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "оплачен";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10438;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Въезд";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10439;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "невозможен";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);
            */
            L = new LangSettings();
            L.id = 10440;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Слейв 2.0";
            L.EN = "Slave 2.0";
            L.ZH = "Slave 2.0";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);
           
            L = new LangSettings();
            L.id = 10441;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Слейв 3.0";
            L.EN = "Slave 3.0";
            L.ZH = "Slave 3.0";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10442;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "RPS Табло";
            L.EN = "RPS Tablo";
            L.ZH = "RPS显示屏";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10443;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ИНФОТАБ 3/24-3";
            L.EN = "Infotab 3/24-3";
            L.ZH = "停车信号盘3/24-3";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10444;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "UDP китайское";
            L.EN = "Chinese UDP";
            L.ZH = "UDP中文";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10445;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "UDP китайское 5 знаков";
            L.EN = "Chinese UDP 5 digits";
            L.ZH = "UDP中文5个字符";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10446;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Mifare карты";
            L.EN = "Mifare 1K Cards";
            L.ZH = "Mifare卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10447;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Mifare жетоны";
            L.EN = "Mifare 1K Tokens";
            L.ZH = "Mifare号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10448;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Билетики";
            L.EN = "Tickets";
            L.ZH = "票";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10449;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Без НДС";
            L.EN = "VAT Free";
            L.ZH = "不含增值税";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10450;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "10%";
            L.EN = "VAT 10%";
            L.ZH = "10%";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10451;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "18%";
            L.EN = "VAT 18%";
            L.ZH = "18%";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10452;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "20%";
            L.EN = "VAT 20%";
            L.ZH = "20%";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10453;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Закончился срок действия штрих-кода";
            L.EN = "Closed: Barcode expired";
            L.ZH = "关闭：条形码已过期";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10454;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Штрих-код не действителен";
            L.EN = "Closed: Barcode invalid";
            L.ZH = "关闭：条形码无效";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10455;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Проезд запрещён по гос. номеру";
            L.EN = "Closed: No entry with this state number";
            L.ZH = "关闭：国家牌号禁止通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10456;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Кража карты";
            L.EN = "Closed: Card stolen";
            L.ZH = "关闭：卡被盗";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10457;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Проезд запрещён. Нет карты в базе.";
            L.EN = "Closed: No entry. No card in the database.";
            L.ZH = "关闭：禁止通行数据库中没有此卡。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10458;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Проезд запрещён. Нет карты в базе.";
            L.EN = "Closed: No entry. No card in the database.";
            L.ZH = "关闭：禁止通行数据库中没有此卡。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10459;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Проезд запрещён. Нет мест для компании.";
            L.EN = "Closed: No entry. No open places for the Company";
            L.ZH = "关闭：禁止通行没有公司的位置。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10460;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Проезд запрещён. Нет мест для компании.";
            L.EN = "Closed: No entry. No open places for the Company";
            L.ZH = "关闭：禁止通行没有公司的位置。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10461;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Проезд запрещён. Не совпал гос.номер.";
            L.EN = "Closed: No entry Wrong state number.";
            L.ZH = "关闭：禁止通行您的客户群不符合国家号码。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10462;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Проезд запрещён группе клиентов.";
            L.EN = "Closed: No entry to client group";
            L.ZH = "关闭：客户群禁止通行。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10463;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Проезд запрещён Несоответствие зоны.";
            L.EN = "Closed: No entry. Wrong zone";
            L.ZH = "关闭：区域不匹配禁止通行。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10464;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Проезд с отбоем стрелы";
            L.EN = "Closed: Passage with crane arm signal";
            L.ZH = "关闭：撤回通道";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10465;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Отказ от проезда";
            L.EN = "Closed: Passage canceled ";
            L.ZH = "关闭：拒绝通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10466;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Попытка неоплаченного проезда";
            L.EN = "Closed: Unpaid passage attempted";
            L.ZH = "关闭：尝试无偿通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10467;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Разовая карта на внешнем считывателе";
            L.EN = "Closed: Single entry card on the external reader";
            L.ZH = "关闭：外接阅读器上的一次性卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10468;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: ОШИБКА ЧТЕНИЯ КАРТЫ";
            L.EN = "Closed: READING CARD ERROR";
            L.ZH = "关闭：读卡错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10469;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: ОШИБКА КЛЮЧА КАРТЫ";
            L.EN = "Closed: CARD KEY ERROR";
            L.ZH = "关闭：卡的钥匙错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10470;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: ОШИБКА ЗАПИСИ КАРТЫ";
            L.EN = "Closed: CARD RECORDING ERROR";
            L.ZH = "关闭：记录卡错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10471;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Кончились жетоны в хоппере";
            L.EN = "Closed: Out of tokens in the hopper";
            L.ZH = "关闭：储卡器中的号牌用完了";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10472;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыли: Мало жетонов в хоппере";
            L.EN = "Closed: Few tokens in hopper";
            L.ZH = "关闭：储卡器中的少数号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10473;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закончился срок действия штрих-кода";
            L.EN = "Barcode expired";
            L.ZH = "条形码已过期";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10474;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Штрих-код не действителен";
            L.EN = "Barcode invalid";
            L.ZH = "条形码无效";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10475;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд запрещён по гос. номеру";
            L.EN = "No entry with this state number";
            L.ZH = "国家牌号禁止通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10476;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кража карты";
            L.EN = "Card stolen";
            L.ZH = "卡被盗";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10477;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд запрещён. Нет карты в базе.";
            L.EN = "No entry. No card in the database.";
            L.ZH = "禁止通行数据库中没有此卡。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10478;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд запрещён. Нет карты в базе.";
            L.EN = "No entry. No card in the database.";
            L.ZH = "禁止通行数据库中没有此卡。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10479;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд запрещён. Нет мест для компании.";
            L.EN = "No entry. No open places for the Company.";
            L.ZH = "禁止通行没有公司的位置。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10480;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд запрещён. Нет мест для компании.";
            L.EN = "No entry. No open places for the Company.";
            L.ZH = "禁止通行没有公司的位置。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10481;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд запрещён. Не совпал гос.номер.";
            L.EN = "No entry. Wrong state number.";
            L.ZH = "禁止通行您的客户群不符合国家号码。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10482;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд запрещён группе клиентов.";
            L.EN = "No entry to client group.";
            L.ZH = "客户群禁止通行。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10483;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд запрещён Несоответствие зоны.";
            L.EN = "No entry. Wrong zone.";
            L.ZH = "区域不匹配禁止通行。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10484;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд с отбоем стрелы";
            L.EN = "Passage with crane arm signal";
            L.ZH = "撤回通道";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10485;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Отказ от проезда";
            L.EN = "Passage canceled";
            L.ZH = "拒绝通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10486;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Попытка неоплаченного проезда";
            L.EN = "Unpaid passage attempted";
            L.ZH = "尝试无偿通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10487;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Разовая карта на внешнем считывателе";
            L.EN = "Single entry card on the external reader";
            L.ZH = "外接阅读器上的一次性卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10488;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ОШИБКА ЧТЕНИЯ КАРТЫ";
            L.EN = "READING CARD ERROR";
            L.ZH = "读卡错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10489;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ОШИБКА КЛЮЧА КАРТЫ";
            L.EN = "CARD KEY ERROR";
            L.ZH = "卡的钥匙错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10490;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ОШИБКА ЗАПИСИ КАРТЫ";
            L.EN = "CARD RECORDING ERROR";
            L.ZH = "记录卡错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10491;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кончились жетоны в хоппере";
            L.EN = "Out of tokens in the hopper";
            L.ZH = "储卡器中的号牌用完了";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10492;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Мало жетонов в хоппере";
            L.EN = "Few tokens in hopper";
            L.ZH = "储卡器中的少数号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10493;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "В ККМ закончилась бумага. Замените чековую ленту.";
            L.EN = "Cash register out of paper Replace the receipt roll.";
            L.ZH = "收银机缺纸更换收银纸。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10494;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Открыта крышка принтера. ККМ не работает.";
            L.EN = "Printer lid open. Cash register is out of order.";
            L.ZH = "打开打印机盖设备不工作。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);


            L = new LangSettings();
            L.id = 10495;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Транспортировка бумаги вручную в принтере. ККМ не работает.";
            L.EN = "Manual paper transportation in the printer. Cash register is out of order.";
            L.ZH = "在打印机上手动处理纸张设备不工作。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10496;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Механическая ошибка ножа принтера. ККМ не работает.";
            L.EN = "Mechanical error of printer cutter. Cash register is out of order.";
            L.ZH = "打印机刀的机械错误设备不工作。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10497;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Критическая ошибка принтера. ККМ не работает.";
            L.EN = "Critical printer mistake. Cash register is out of order.";
            L.ZH = "打印机的严重错误设备不工作。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10498;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Головка принтера слишком горячая. ККМ не работает.";
            L.EN = "Printer head too hot. Cash register is out of order.";
            L.ZH = "打印头太热了设备不工作。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10499;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Механическая ошибка принтера. ККМ не работает.";
            L.EN = "Mechanical printer error. Cash register is out of order.";
            L.ZH = "打印机的严重错误设备不工作。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10500;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Печать остановлена из-за окончания бумаги. ККМ не работает.";
            L.EN = "Printing discontinued, out of paper. Cash register is out of order.";
            L.ZH = "纸用完了停止打印设备不工作。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10501;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет связи с ККМ. Проверьте устройство.";
            L.EN = "No connection to cash register. Check the device.";
            L.ZH = "与收银机无关检查设备。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10502;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ошибка установки режима KKM.";
            L.EN = "Cash register mode error.";
            L.ZH = "收银机模式设置错误。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10503;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ работает.";
            L.EN = "Cash register is operational.";
            L.ZH = "收银机工作。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10504;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Несовпадение номера Закрыто";
            L.EN = "Wrong number. Closed";
            L.ZH = "关闭不匹配号码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10505;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Есть жетоны";
            L.EN = "Tokens are available";
            L.ZH = "有号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10506;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Распознавание Ок";
            L.EN = "Detection OK";
            L.ZH = "已识别";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10507;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Смена";
            L.EN = "Shift";
            L.ZH = "更改";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10508;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "От:";
            L.EN = "From:";
            L.ZH = "自：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10509;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "До:";
            L.EN = "To:";
            L.ZH = "到：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10510;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Смена закрыта";
            L.EN = "Shift closed";
            L.ZH = "更改关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10511;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Смена открыта";
            L.EN = "Shift open";
            L.ZH = "更改开放";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10512;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Открыть смену";
            L.EN = "Open shift";
            L.ZH = "更改开放";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10513;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыть смену";
            L.EN = "Close shift";
            L.ZH = "关闭更改";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10514;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Смена";
            L.EN = "Shift";
            L.ZH = "更改";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10515;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "открывается";
            L.EN = "is opening";
            L.ZH = "开放";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10516;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "закрывается";
            L.EN = "is closing";
            L.ZH = "关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10517;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Код доступа:";
            L.EN = "Access code:";
            L.ZH = "访问代码：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10518;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Автосмена";
            L.EN = "Automatic shift";
            L.ZH = "自动更改";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10519;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Z отчет";
            L.EN = "Z report";
            L.ZH = "Z-报告";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10520;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "X отчет";
            L.EN = "X report";
            L.ZH = "X-报告";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10521;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Пароль ККМ:";
            L.EN = "Cash Register Password:";
            L.ZH = "收银机密码：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10522;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Пароль режим ККМ:";
            L.EN = "Cash Register Mode Password:";
            L.ZH = "收银机的密码模式：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10523;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Версия ФН:";
            L.EN = "FN Version:";
            L.ZH = "模糊函数：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10524;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Что делать с чеком?";
            L.EN = "What to do with the receipt?";
            L.ZH = "发票怎么办？";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10525;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Втянуть";
            L.EN = "Collect";
            L.ZH = "拖入";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10526;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выбросить";
            L.EN = "Throw out";
            L.ZH = "扔掉";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10527;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Не выпускать без чека";
            L.EN = "Do not release without receipt";
            L.ZH = "没有支票不能发行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10528;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выбросить";
            L.EN = "Throw out";
            L.ZH = "扔掉";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10529;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Втянуть";
            L.EN = "Collect";
            L.ZH = "拖入";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10530;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ничего";
            L.EN = "Nothing";
            L.ZH = "没关系";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10531;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "НДС:";
            L.EN = "VAT:";
            L.ZH = "增值税：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10532;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Строка статуса:";
            L.EN = "Status line:";
            L.ZH = "状态栏：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10533;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ не подключен. Порт не существует.";
            L.EN = "Cash register not connected. Port does not exist.";
            L.ZH = "收银机没有连接端口不存在";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10534;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ не подключается";
            L.EN = "Cash register not connected";
            L.ZH = "收银机没有连接";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10535;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ работает";
            L.EN = "Cash register is operational";
            L.ZH = "收银机工作";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10536;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ не работает";
            L.EN = "Cash register is out of order";
            L.ZH = "收银机不工作";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10537;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ не подключен. Объект не запущен";
            L.EN = "Cash register not connected. Object not launched";
            L.ZH = "收银机没有连接该对象未运行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10538;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ не подключен. Ошибка";
            L.EN = "Cash register not connected. Error";
            L.ZH = "收银机没有连接错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10539;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ не подключен. Снят флажок.";
            L.EN = "Cash register not connected. Flag’s off.";
            L.ZH = "收银机没有连接 取消标签";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10540;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Пайпасс";
            L.EN = "Paypass";
            L.ZH = "PayPass";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10541;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Открыть";
            L.EN = "Open";
            L.ZH = "开放";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10542;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Оплата";
            L.EN = "Payment";
            L.ZH = "支付";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10543;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыть";
            L.EN = "Close";
            L.ZH = "关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10544;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Отмена";
            L.EN = "Cancel";
            L.ZH = "取消";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10545;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Загрузка параметров";
            L.EN = "Loading settings";
            L.ZH = "加载参数";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10546;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Загрузка ПО";
            L.EN = "Software loaded";
            L.ZH = "软件下载";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10547;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Инфо терминала";
            L.EN = "Terminal info";
            L.ZH = "终端信息";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10548;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Загрузка рабочего ключа";
            L.EN = "Work key loaded";
            L.ZH = "下载工作密钥";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10549;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Эхо тест";
            L.EN = "Echo test";
            L.ZH = "回声测试";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10550;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Сверка итогов";
            L.EN = "Checking results";
            L.ZH = "验证总数";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10551;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Состояние";
            L.EN = "State";
            L.ZH = "状态";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10552;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ИД карты";
            L.EN = "Card ID";
            L.ZH = "执行文件卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10553;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Чтение";
            L.EN = "Reading";
            L.ZH = "阅读";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10554;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Запись";
            L.EN = "Recording";
            L.ZH = "记录";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10555;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Втянуть карту";
            L.EN = "Collect the card";
            L.ZH = "拖入卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10556;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Опять под ридер";
            L.EN = "Again under reader";
            L.ZH = "再次放在读出器处";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10557;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Версия";
            L.EN = "Version";
            L.ZH = "版本";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);


            L = new LangSettings();
            L.id = 10558;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Старт";
            L.EN = "Start";
            L.ZH = "开始";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10559;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Стоп";
            L.EN = "Stop";
            L.ZH = "停止";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10560;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Табло #1 (табло РПС)";
            L.EN = "Indicator 1 (RPS)";
            L.ZH = "1号信号盘（信号继电转换开关信号盘）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10561;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Табло #2 (ИНФОТАБ 3/24-3)";
            L.EN = "Indicator 2 (INFOTAB 3/24-3)";
            L.ZH = "2号信号盘（停车信号盘3/24-3）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10562;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Табло #3 (UDP китайское)";
            L.EN = "Indicator 3 (UDP Chinese)";
            L.ZH = "3号信号盘（UDP中文）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10563;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Значение";
            L.EN = "Value";
            L.ZH = "数值";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10564;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Задать";
            L.EN = "Set";
            L.ZH = "问";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10565;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Статус коллектора";
            L.EN = "Collector status";
            L.ZH = "收集器状态";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10566;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выдать в губы";
            L.EN = "Issue in the lips";
            L.ZH = "人泄露";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10567;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Карту в отказник";
            L.EN = "Card rejected";
            L.ZH = "卡不能用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10568;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Захват спереди";
            L.EN = "Front grab";
            L.ZH = "前握把";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10569;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Захват сзади";
            L.EN = "Back grab";
            L.ZH = "后握把";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10570;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Версия";
            L.EN = "Version";
            L.ZH = "版本";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10571;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выкл. таймаут";
            L.EN = "Timeout off";
            L.ZH = "关闭超时";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10572;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Коллектор";
            L.EN = "Collector";
            L.ZH = "收集器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10573;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Хоппер";
            L.EN = "Hopper";
            L.ZH = "储卡器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10574;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Открыть";
            L.EN = "Open";
            L.ZH = "开放";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10575;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыть";
            L.EN = "Close";
            L.ZH = "关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10576;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Прокрутить";
            L.EN = "Rotate";
            L.ZH = "滚动";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10577;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Сброс";
            L.EN = "Reset";
            L.ZH = "重置";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10578;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выдача";
            L.EN = "Issue";
            L.ZH = "发行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10579;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Жетонопроводы";
            L.EN = "Token ducts";
            L.ZH = "号牌线";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10580;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Жетонопровод въезд";
            L.EN = "Token duct at entrance";
            L.ZH = "号牌线入口";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10581;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Жетонопровод выезд";
            L.EN = "Token duct at exit";
            L.ZH = "号牌线出口";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10582;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Соленоид 1 (проезд)";
            L.EN = "Solenoid 1 (passage)";
            L.ZH = "螺线管1（通道）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10583;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Соленоид 2 (возврат)";
            L.EN = "Solenoid 2 (return)";
            L.ZH = "螺线管2（返回）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10584;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Соленоид 3 (отказник)";
            L.EN = "Solenoid 3 (refuse)";
            L.ZH = "螺线管3（不许出境者）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10585;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Запасной дискрет";
            L.EN = "Additional discrete";
            L.ZH = "备用离散";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10586;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Из хоппера";
            L.EN = "from hopper";
            L.ZH = "来自储卡器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10587;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Инициализация";
            L.EN = "Initialization";
            L.ZH = "初始化";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10588;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Не работает Slave";
            L.EN = "Slave down";
            L.ZH = "Slave不工作";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10589;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Не работает Хоппер";
            L.EN = "Hopper down";
            L.ZH = "储卡器不工作";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10590;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Работает";
            L.EN = "Available";
            L.ZH = "工作";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10591;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "не отвечает";
            L.EN = "does not reply";
            L.ZH = "无应答";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10592;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "нет жетона";
            L.EN = "no token";
            L.ZH = "没有号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10593;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "жетон под ридером";
            L.EN = "token under reader";
            L.ZH = "读卡器下的卡片";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10594;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "жетон в губах";
            L.EN = "token in lips";
            L.ZH = "唇牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10595;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "два жетона";
            L.EN = "2 tokens";
            L.ZH = "两张号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10596;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "жетон на возврате";
            L.EN = "token at return";
            L.ZH = "返回号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10597;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "несколько жетонов";
            L.EN = "several tokens";
            L.ZH = "几张号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10598;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик №1 (губы) разомкнут";
            L.EN = "Sensor No. 1 (lips) disconnected";
            L.ZH = "1号传感器（嘴唇）打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10599;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик №1 (ридер) разомкнут";
            L.EN = "Sensor No. 1 (reader) disconnected";
            L.ZH = "1号传感器（读出器）打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10600;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик №1 (губы) замкнут";
            L.EN = "Sensor No. 1 (lips) connected";
            L.ZH = "1号传感器（嘴唇）关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10601;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик №1 (ридер) замкнут";
            L.EN = "Sensor No. 1 (reader) connected";
            L.ZH = "1号传感器（读出器）关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10602;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик №2 (ридер) разомкнут";
            L.EN = "Sensor No. 2 (reader) disconnected";
            L.ZH = "2号传感器（读出器）打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10603;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик №2 (губы) разомкнут";
            L.EN = "Sensor No. 2 (lips) disconnected";
            L.ZH = "2号传感器（嘴唇）打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10604;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик №2 (ридер) замкнут";
            L.EN = "Sensor No. 2 (reader) connected";
            L.ZH = "2号传感器（读出器）关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10605;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик №2 (губы) замкнут";
            L.EN = "Sensor No. 2 (lips) connected";
            L.ZH = "2号传感器（嘴唇）关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10606;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик №3 (возврат) разомкнут";
            L.EN = "Sensor No. 3 (return) disconnected";
            L.ZH = "3号传感器（返回）打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10607;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Хоппер мало жетонов";
            L.EN = "Hopper has few tokens";
            L.ZH = "少数号牌的储卡器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10608;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик №3 (возврат) замкнут";
            L.EN = "Sensor No. 3 (return) connected";
            L.ZH = "3号传感器（返回）关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10609;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Хоппер много жетонов";
            L.EN = "Hopper many tokens";
            L.ZH = "大量号牌的储卡器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10610;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Есть жетоны";
            L.EN = "Tokens are available";
            L.ZH = "有号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10611;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Жетон выдается";
            L.EN = "Token is issued";
            L.ZH = "颁布号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10612;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик свободен";
            L.EN = "Sensor is free";
            L.ZH = "传感器免费";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10613;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Основной язык";
            L.EN = "Main language";
            L.ZH = "基本语言";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10614;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Второй язык";
            L.EN = "Second language";
            L.ZH = "第二语言";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10615;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Носитель:";
            L.EN = "Carrier:";
            L.ZH = "媒介：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10616;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ID ТС parkpass:";
            L.EN = "ID ТS parkpass:";
            L.ZH = "车辆ID parkpass：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10617;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ID ТП parkpass:";
            L.EN = "ID ТP parkpass:";
            L.ZH = "ID TP Parkpass：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);


            L = new LangSettings();
            L.id = 10618;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Группа parkpass:";
            L.EN = "Parkpass Group:";
            L.ZH = "Parkpass群：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10619;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Основные";
            L.EN = "Main";
            L.ZH = "主要";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10620;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Логика проезда";
            L.EN = "Passage logic";
            L.ZH = "通行逻辑";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10621;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Разовые карты по кнопке";
            L.EN = "Single entry cards by button";
            L.ZH = "按钮上的一次性卡片";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10622;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Штрихкод по кнопке";
            L.EN = "Bar code by button";
            L.ZH = "条形码按钮";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);


            L = new LangSettings();
            L.id = 10623;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Контроль мест разовых";
            L.EN = "Controlling single entry stations";
            L.ZH = "控制一次性的地方";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10624;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Контроль мест компаний";
            L.EN = "Controlling company stations";
            L.ZH = "控制公司位置";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10625;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Контроль проезда по петле B";
            L.EN = "Loop B passage control";
            L.ZH = "通过循环B上控制通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10626;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Свободный проезд красный светофор";
            L.EN = "Free passage, red light";
            L.ZH = "红灯自由通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10627;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Проверять зону распознавание";
            L.EN = "Check detection zone";
            L.ZH = "检查识别区域";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10628;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Не проверять зону RFID";
            L.EN = "Do not check RFID zone";
            L.ZH = "不检查RFID区域";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10629;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Не проверять зону HID";
            L.EN = "Do not check HID zone";
            L.ZH = "不检查HID区域";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10630;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Время до тревог сек-петля А";
            L.EN = "Time to alarm, sec, Loop A";
            L.ZH = "Sec-loop A报警前的时间";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10631;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Петля Б";
            L.EN = "Loop B";
            L.ZH = "循环B";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10632;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ИК";
            L.EN = "MC";
            L.ZH = "感应测井";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10633;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Дополнит. бесплатное время парковки, мин:";
            L.EN = "Additional free parking time, min:";
            L.ZH = "额外免费停车时间，分钟：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10634;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Реверс стрелы при проезде паровозом";
            L.EN = "Crane arm reverse when tailgating";
            L.ZH = "机车通行时反向箭头";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10635;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Блокировки";
            L.EN = "Blocks";
            L.ZH = "闭塞";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10636;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вкл. реверс";
            L.EN = "Turn on reverse";
            L.ZH = "打开反向";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10637;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Режим реверса (сообщение)";
            L.EN = "Reverse mode (message)";
            L.ZH = "反向模式（消息）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10638;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Въезд/выезд";
            L.EN = "Entry/Exit";
            L.ZH = "进入－离开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10639;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Между уровнями";
            L.EN = "Between levels";
            L.ZH = "级别之间";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10640;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Двухуровневая";
            L.EN = "Two-level";
            L.ZH = "二级";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10641;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //defaultc
            L.RU = "Разнесенный";
            L.EN = "Different levels";
            L.ZH = "间隔";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10642;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "IP стойки для разнесенного реверса:";
            L.EN = "IP stands for different reverse levels";
            L.ZH = "间隔反向IP支架：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10643;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кол-во пропускаемых автомобилей для разнесенного реверса:";
            L.EN = "Number of vehicles passing through different reverse levels";
            L.ZH = "间隔反向通过的汽车数量：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10644;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Высотомер";
            L.EN = "Altimeter";
            L.ZH = "高度表";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10645;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Высотомер1";
            L.EN = "Altimeter 1";
            L.ZH = "高度表1";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10646;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Высотомер2";
            L.EN = "Altimeter 2";
            L.ZH = "高度表2";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10647;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Высотомер3";
            L.EN = "Altimeter 3";
            L.ZH = "高度表3";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10648;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ТС";
            L.EN = "TS";
            L.ZH = "技术状态";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10649;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ТП";
            L.EN = "TP";
            L.ZH = "TP";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10650;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Устройства";
            L.EN = "Connecting";
            L.ZH = "设备";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10651;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Оборудование";
            L.EN = "Equipment";
            L.ZH = "设备";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10652;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Тип";
            L.EN = "Type";
            L.ZH = "类型";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10653;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Инверсия";
            L.EN = "Inversion";
            L.ZH = "转变";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10654;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Инверт вызов";
            L.EN = "Inverse call";
            L.ZH = "呼叫倒置";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10655;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Хоппер";
            L.EN = "Hopper";
            L.ZH = "储卡器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10656;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "RFID";
            L.EN = "RFID";
            L.ZH = "RFID";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10657;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Коллектор";
            L.EN = "Collector";
            L.ZH = "收集器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10658;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Банковский модуль";
            L.EN = "Banking module";
            L.ZH = "银行模块";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10659;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Паркпасс";
            L.EN = "Parkpass";
            L.ZH = "Parkpass";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10660;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ";
            L.EN = "Cash register";
            L.ZH = "收银机";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10661;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ №";
            L.EN = "Cash register No.";
            L.ZH = "收银机 №";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10662;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Дискретные порты Slave:";
            L.EN = "Discrete Slave ports";
            L.ZH = "Slave离散端口：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10663;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Жетонопроводы (2-ур рев):";
            L.EN = "Token ducts (two-level reverse)";
            L.ZH = "号牌线（2级反向）：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10664;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "RFID TCP";
            L.EN = "RFID TCP";
            L.ZH = "RFID TCP";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10665;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Транспондер на стойке";
            L.EN = "Transponder at stand";
            L.ZH = "柜台上的转发器";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10666;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Номер антенны:";
            L.EN = "Antenna number:";
            L.ZH = "天线号码：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10667;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "IP/порт считывателя:";
            L.EN = "IP/reader port";
            L.ZH = "读卡器IP /端口：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10668;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "IP хоста (2222 порт):";
            L.EN = "Host IP (Port 2222)";
            L.ZH = "主机IP（端口2222）：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10669;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Табло #3 (UDP китайское)";
            L.EN = "Indicator 3 (UDP Chinese)";
            L.ZH = "3号信号盘（UDP中文）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10670;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Цвет";
            L.EN = "Color";
            L.ZH = "颜色";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10671;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Красный до:";
            L.EN = "Red until:";
            L.ZH = "红色到：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10672;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Желтый до:";
            L.EN = "Yellow until:";
            L.ZH = "黄色到：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10673;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Зеленый до:";
            L.EN = "Green until:";
            L.ZH = "绿色到：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10674;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Табло #2 (ИНФОТАБ 3/24-3)";
            L.EN = "Indicator 2 (INFOTAB 3/24-3)";
            L.ZH = "2号信号盘（停车信号盘3/24-3）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10675;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Адрес";
            L.EN = "Address";
            L.ZH = "地址";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10676;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Разряды";
            L.EN = "Types";
            L.ZH = "放电";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10677;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Прочее";
            L.EN = "Other";
            L.ZH = "其它";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10678;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Алармы автомобиля в проезде";
            L.EN = "Automobile alarms during passage";
            L.ZH = "警报车通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10679;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Время закрытия, мин:";
            L.EN = "Closure time, min:";
            L.ZH = "关闭时间，分钟：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10680;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Использовать информацию о карте из базы";
            L.EN = "Use card information from the database";
            L.ZH = "使用数据库中的卡片信息";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10681;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Использовать финансовые сессии компаний";
            L.EN = "Use company’s financial sessions";
            L.ZH = "使用公司的财务会议";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10682;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Не пропускать при задолженности на переезде";
            L.EN = "Do not let through in case of debt";
            L.ZH = "在道口欠债时不能通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10683;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Использовать двухфакторную идентификацию";
            L.EN = "Use two-factorial identification";
            L.ZH = "使用双因素识别";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10684;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Обязательное распознавание для штрафных карт";
            L.EN = "Obligatorily use detection for fine cards";
            L.ZH = "罚款卡的强制性识别";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10685;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Пропускать по штрафной карте при отсутствии синхронизации";
            L.EN = "Let through with fine card with no synchronization";
            L.ZH = "在没有同步的情况下用罚款卡通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10686;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Время закрытия алармов (петля А), мин:";
            L.EN = "Time alarms close (Loop A), min:";
            L.ZH = "报警关闭时间（循环A），分钟：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10687;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Ключ карты, с которого нужно инициализировать Mifare носитель:";
            L.EN = "Card key to initiate Mifare carrier:";
            L.ZH = "需要初始化Mifare媒体的卡密钥：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10688;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Голосовая связь";
            L.EN = "Voice communication";
            L.ZH = "语音通讯";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10689;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Уведомление удаленного сервера о занятии петли A";
            L.EN = "Informing remote server about taking Loop A";
            L.ZH = "循环A上的远程服务器通知";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10690;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Включить уведомление";
            L.EN = "Turn on notifications";
            L.ZH = "启用通知";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10691;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Слать кадры на этот URL";
            L.EN = "Send frames to this URL";
            L.ZH = "将卡送到URL上";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10692;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "URL запроса:";
            L.EN = "URL of the query";
            L.ZH = "请求URL：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10693;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Таймаут запроса, мс:";
            L.EN = "Query timeout, msec:";
            L.ZH = "请求超时，ms：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);


            L = new LangSettings();
            L.id = 10694;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Функции распознавания";
            L.EN = "Detection function";
            L.ZH = "识别功能";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10695;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вкл./выкл. распознавание";
            L.EN = "Turn on/off detection";
            L.ZH = "打开/关闭识别";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10696;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вкл./выкл. оператор";
            L.EN = "Turn on/off operator";
            L.ZH = "打开/关闭操作";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10697;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Опер. LoopA, с:";
            L.EN = "Oper. Loop A, with:";
            L.ZH = "操作LoopA，用：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10698;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Номер не распознан";
            L.EN = "Number not recognized";
            L.ZH = "号码未被识别";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10699;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Пропустить";
            L.EN = "Skip";
            L.ZH = "放行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10700;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Запретить";
            L.EN = "Forbid";
            L.ZH = "禁止";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10701;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "#3 Проезд постоянного клиента";
            L.EN = "#3 Permanent customer passage.";
            L.ZH = "3号常客通道";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10702;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вкл. проезд постоянного клиента";
            L.EN = "Including permanent customer passage.";
            L.ZH = "打开常客通道";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10703;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вкл. Левенштейн";
            L.EN = "Including Levenstein";
            L.ZH = "打开Levenshtein";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10704;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "#1 Чёрный список";
            L.EN = "# 1 Black List";
            L.ZH = "1号黑名单";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10705;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вкл. черный список";
            L.EN = "Including black list";
            L.ZH = "打开黑名单";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10706;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вкл. контроль ПК";
            L.EN = "Including PC control";
            L.ZH = "打开电脑控制";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10707;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "#2 Въезд-выезд";
            L.EN = "# 2 Entry-Exit";
            L.ZH = "2号进入-离开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10708;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вкл. въезд/выезд";
            L.EN = "Including entry / exit";
            L.ZH = "打开进入-离开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10709;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "#4 Выдача приглашений по номеру";
            L.EN = "#4 Issue of invitations by number";
            L.ZH = "4号按号码发出邀请";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10710;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вкл. выдачу приглашений";
            L.EN = "Including issue of invitations";
            L.ZH = "打开发出邀请";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10711;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Для двухуровневой стойки";
            L.EN = "For two-level stand";
            L.ZH = "双层柜台";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10712;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Отключено";
            L.EN = "Disconnected";
            L.ZH = "关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10713;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Получать номер (слушать)";
            L.EN = "Get number (listen)";
            L.ZH = "收到一个号码（听）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10714;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Отправлять номер";
            L.EN = "Send number";
            L.ZH = "发送号码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10715;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "на IP:";
            L.EN = "to IP:";
            L.ZH = "至 IP:";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10716;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Сохранение фото с камеры в БД";
            L.EN = "Save camera photo in DB";
            L.ZH = "将照片从相机保存到数据库";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10717;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Сохранять всегда";
            L.EN = "Always save";
            L.ZH = "永久保存";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10718;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Если номер не распознан";
            L.EN = "If number not recognized";
            L.ZH = "如果号码未识别";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10719;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Распознавание";
            L.EN = "Detection";
            L.ZH = "识别";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10720;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Основные настройки Viinex";
            L.EN = "Main Viinex settings";
            L.ZH = "Viinex基本设置";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10721;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Путь к конфигурационному файлу Viinex:";
            L.EN = "Path to Viinex configuration file:";
            L.ZH = "Viinex配置文件的路径：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10722;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Лиц. ключ Viinex:";
            L.EN = "Personal Viinex key";
            L.ZH = "Viinex面部钥匙：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10723;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Настройка ROI:";
            L.EN = "ROI Setup:";
            L.ZH = "投资回报率设置：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10724;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Слева:";
            L.EN = "Left:";
            L.ZH = "左边：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10725;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Сверху:";
            L.EN = "Top:";
            L.ZH = "上面：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10726;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Справа:";
            L.EN = "Right:";
            L.ZH = "右边：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10727;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Снизу:";
            L.EN = "Bottom:";
            L.ZH = "下面：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10728;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Таймаут, сек:";
            L.EN = "Timeout, sec:";
            L.ZH = "超时，秒：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10729;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Снапшот";
            L.EN = "Snapshot";
            L.ZH = "快照";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10730;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Применить настройки Viinex (требуются права администратора)";
            L.EN = "Apply Viinex settings (administrator rights required)";
            L.ZH = "应用Viinex设置（需要管理员权限）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10731;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Камера 1";
            L.EN = "Camera 1";
            L.ZH = "相机1";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10732;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Камера 2";
            L.EN = "Camera 2";
            L.ZH = "相机2";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10733;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Использовать вторую камеру";
            L.EN = "Use second camera";
            L.ZH = "使用第二台相机";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10734;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кам 1";
            L.EN = "Cam 1";
            L.ZH = "相机1";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10735;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кам 2";
            L.EN = "Cam 2";
            L.ZH = "相机2";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10736;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Настройки камеры #1";
            L.EN = "Camera No. 1 settings";
            L.ZH = "1号相机设置";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10737;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Настройки камеры #2";
            L.EN = "Camera No. 2 settings";
            L.ZH = "2号相机设置";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10738;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "URL Get запроса:";
            L.EN = "URL Get query:";
            L.ZH = "请求获取URL：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10739;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "URL камеры:";
            L.EN = "Camera URL:";
            L.ZH = "URL相机：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10740;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Тип камеры";
            L.EN = "Camera type";
            L.ZH = "相机类型";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10741;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Запрет событий (для ONVIF камер)";
            L.EN = "Events forbidden (for ONVIF cameras)";
            L.ZH = "禁止活动（适用于ONVIF相机）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10742;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Логин:";
            L.EN = "Login:";
            L.ZH = "登陆：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10743;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Пароль:";
            L.EN = "Password:";
            L.ZH = "密码：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10744;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Транслитерация:";
            L.EN = "Transliteration:";
            L.ZH = "音译：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10745;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Лат";
            L.EN = "Latin";
            L.ZH = "LAT";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10746;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кир";
            L.EN = "Cyrillic";
            L.ZH = "CYR";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10747;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Видео";
            L.EN = "Video";
            L.ZH = "视频";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10748;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "События";
            L.EN = "Events";
            L.ZH = "事件";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10749;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Транспорт";
            L.EN = "Transport";
            L.ZH = "交通";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10750;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Skip,кадров:";
            L.EN = "Skip, frames:";
            L.ZH = "Skip frames:";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10751;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Конфиденс:";
            L.EN = "Confidence:";
            L.ZH = "信心：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10752;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Препроцессинг:";
            L.EN = "Preprocessing:";
            L.ZH = "预处理：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10753;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Постпроцессинг:";
            L.EN = "Postprocessing:";
            L.ZH = "后处理：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10754;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Точки области распознавания (X;Y)";
            L.EN = "Recognition area points (X;Y)";
            L.ZH = "识别区域点（X; Y）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10755;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Минимальный размер номера (Ширина/Высота)";
            L.EN = "Minimum number size (width/height)";
            L.ZH = "最小号数（宽/高）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10756;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Максимальный размер номера (Ширина/Высота)";
            L.EN = "Maximum number size (width/height)";
            L.ZH = "最大号数（宽/高）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10757;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Идентификатор настраиваемого канала (0-15):";
            L.EN = "Adjusted channel identifier (0-15):";
            L.ZH = "自定义渠道ID（0-15）：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10758;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Строка подключения RTSP:";
            L.EN = "RTSP connection line:";
            L.ZH = "RTSP连接字符串：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10759;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Таймбюджет, мс:";
            L.EN = "Time budget, msec";
            L.ZH = "时间预算，ms：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10760;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Конфиденс, %:";
            L.EN = "Confidence, %:";
            L.ZH = "信心，％：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10761;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Препроцессинг, мс:";
            L.EN = "Preprocessing, msec:";
            L.ZH = "预处理，ms：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10762;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Постпроцессинг, мс:";
            L.EN = "Postprocessing, msec:";
            L.ZH = "后处理，ms：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10763;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "IP сервера Малленом:";
            L.EN = "IP Mallenom Server";
            L.ZH = "Mullen服务器IP：";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10764;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "IP клиента (стойки):";
            L.EN = "IP of the client (stand):";
            L.ZH = "客户IP （柜台）";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10765;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Отправить настройки Mallenom";
            L.EN = "Send Mallenom settings";
            L.ZH = "发送Mallenom设置";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10766;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Имитация, если устройство отключено";
            L.EN = "Imitation when the device is switched off";
            L.ZH = "如果设备被禁用，模仿";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10767;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Карты в корзину";
            L.EN = "Cards in basket";
            L.ZH = "篮子中的卡片";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10768;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Не удалось скопировать настройки в HKLM. Запустите ПО под администратором.";
            L.EN = "Could not copy settings to HKLM. Launch software as admin.";
            L.ZH = "无法复制HKLM的设置。在管理员下的操作下运行该软件。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10769;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Не удалось обновить настройки из конфига. Запустите ПО под администратором.";
            L.EN = "Could not update settings from config file Launch software as admin.";
            L.ZH = "无法更新配置的设置在管理员下的操作下运行该软件。";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10770;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Неправильный формат настроек!";
            L.EN = "Wrong settings format!";
            L.ZH = "格式设置错误！";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10771;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет доступа на перезапись! Запустите стойку от имени администратора!";
            L.EN = "No access to re-recording! Launch stand as admin!";
            L.ZH = "无法覆盖！以管理员的身份运行柜台！";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10772;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Настройки успешно перезаписаны!";
            L.EN = "Settings successfully re-recorded!";
            L.ZH = "设置已成功覆盖！";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10773;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Настройки успешно перезаписаны, но не удалось перезапустить службу!";
            L.EN = "Settings successfully re-recorded, but service could not be launched!";
            L.ZH = "设置已成功覆盖，但无法重新启动服务！";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10774;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ не работает";
            L.EN = "Cash register is out of order";
            L.ZH = "收银机不工作";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10775;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ отключен!";
            L.EN = "Cash register is switched off!";
            L.ZH = "收银机关闭！";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10776;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ККМ отключена в настройках";
            L.EN = "Cash register is switched off in settings";
            L.ZH = "收银机在设置中关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10777;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Повторный|проезд|запрещен";
            L.EN = "Single|entry|only";
            L.ZH = "禁止重复通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10778;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Тег не|соответ-ствует|вашей|карте";
            L.EN = "Tag|does not|correspond|to your|card";
            L.ZH = "代码与您的卡不相符";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10779;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Ошибка|чтения|карты";
            L.EN = "Card|reading|error";
            L.ZH = "读卡错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10780;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Приложите|карту|еще раз";
            L.EN = "Apply|your card|again";
            L.ZH = "请重刷";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10781;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Ошибка|записи|на карту";
            L.EN = "Card|recording|error";
            L.ZH = "记录卡时出错";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10782;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Не|парковочная|карта";
            L.EN = "Not a|parking|card";
            L.ZH = "不是停车卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10783;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Не|парковочный|жетон";
            L.EN = "Not a|parking|token";
            L.ZH = "不是停车号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10784;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Невозможно|списать|средства|со счета|компании";
            L.EN = "Money|cannot be|written off|the|Company|account";
            L.ZH = "无法从公司帐户中扣款";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10785;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите|кнопку,|приложите|карту или|используйте|штрихкод";
            L.EN = "Press|button,|apply|your card|or use|barcode";
            L.ZH = "单击按钮，刷卡或使用条形码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10786;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Нажмите|кнопку или|используйте|штрихкод";
            L.EN = "Press|button|or use|barcode";
            L.ZH = "单击按钮或使用条形码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10787;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Этот|штрихкод|уже|исполь-зовался";
            L.EN = "This|barcode|has been|already|used";
            L.ZH = "此条形码已使用过";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10788;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Закончился|срок|действия|штрихкода";
            L.EN = "Barcode|expired";
            L.ZH = "条形码已过期";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10789;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Номер|на штрафной|карте не|совпадает|с номером|автомобиля";
            L.EN = "Penalty|card number|does not|match|vehicle|number";
            L.ZH = "罚款卡上的号码与车车牌号不符";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10790;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Штрихкод|недействителен";
            L.EN = "Barcode|is invalid";
            L.ZH = "条形码无效";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10791;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочную|карту или|используйте|штрихкод|для|получения|скидки";
            L.EN = "Take your|parking|card|or use|barcode";
            L.ZH = "拿停车卡或使用条形码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10792;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочный|жетон или|используйте|штрихкод|для|получения|скидки";
            L.EN = "Take your|parking|token|or use|barcode";
            L.ZH = "拿停车号牌或使用条形码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10793;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочную|карту или|используйте|банковскую|карту";
            L.EN = "Take your|parking|card|or use|bank card";
            L.ZH = "拿停车卡或使用银行卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10794;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочный|жетон или|используйте|банковскую|карту";
            L.EN = "Take your|parking|token|or use|bank card";
            L.ZH = "拿停车号牌或使用银行卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10795;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочную|карту,|активируйте|штрихкод|для|получения|скидки или|используйте|банковскую|карту";
            L.EN = "Take your|parking|card,|activate|barcode|or use|bank card";
            L.ZH = "拿停车卡，激活条形码或使用银行卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10796;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочный|жетон,|активируйте|штрихкод|для|получения|скидки или|используйте|банковскую|карту";
            L.EN = "Take your|parking|token,|activate|barcode|or use|bank card";
            L.ZH = "拿停车号牌，激活条形码或使用银行卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10797;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите|и вставьте|карту";
            L.EN = "Take|and insert|card";
            L.ZH = "拿出并插入卡片";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10798;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите|и вставьте|жетон";
            L.EN = "Take|and insert|token";
            L.ZH = "拿出并插入号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10799;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Въезд|невозможен";
            L.EN = "Entry is|impossible";
            L.ZH = "不能入境";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10800;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Превышен|лимит|мест для|вашей|компании";
            L.EN = "Places|limit|for your|Company|has been|reached";
            L.ZH = "超出贵公司的限额";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10801;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд|по RFID|невозможен";
            L.EN = "RFID|passage|is|impossible";
            L.ZH = "不能用RFID通行";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10802;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Восполь-зуйтесь|парковочной|картой";
            L.EN = "Use|parking|card";
            L.ZH = "请使用停车卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10803;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Восполь-зуйтесь|парковочным|жетоном";
            L.EN = "Use|parking|token";
            L.ZH = "请使用停车号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10804;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд|по RFID|заблоки-рован";
            L.EN = "RFID|passage|is blocked";
            L.ZH = "RFID通道被封锁";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10805;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Неверная|зона при|проезде|по RFID";
            L.EN = "Wrong|area when|passing|via RFID";
            L.ZH = "RFID通行是无效区域";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10806;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Неверная|группа при|проезде|по RFID";
            L.EN = "Wrong|group when|passing|via RFID";
            L.ZH = "RFID通行是无效群组";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10807;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Задолжен-ность|при проезде|по RFID";
            L.EN = "Debt|when|passing|via RFID";
            L.ZH = "RFID通行债务";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10808;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Приглашение|отсутствует";
            L.EN = "No|invitation";
            L.ZH = "没有邀请";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10809;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Нет мест|на парковке";
            L.EN = "No open|parking|spots";
            L.ZH = "没有停车位";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10810;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Выдача|приглашения|невозможна";
            L.EN = "Invitation|cannot|be issued";
            L.ZH = "不能发布邀请";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10811;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Приглашение|не найдено";
            L.EN = "Invitation|not found";
            L.ZH = "邀请没找到";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10812;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Заберите|чек|для выезда";
            L.EN = "Take|receipt|for exit";
            L.ZH = "检查出发";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10813;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Дождитесь|печати|чека";
            L.EN = "Wait|until|receipt|is printed";
            L.ZH = "等待打印收款票";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);
           
            L = new LangSettings();
            L.id = 10814;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Вставьте|жетон|в отверстие|выше";
            L.EN = "Insert|token|into|opening|above";
            L.ZH = "将号牌插入上方的孔中";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 51;
            LangDefSet.Add(L);           

            L = new LangSettings();
            L.id = 10815;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Неверный|жетон";
            L.EN = "Wrong|token";
            L.ZH = "号牌错误";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10816;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Вставьте|жетон,|который|был|оплачен";
            L.EN = "Insert|token,|which|was paid|for";
            L.ZH = "插入已付款的号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10817;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Заберите|парковочный|жетон|и вставьте|его|в жетоно-приемник|для проезда";
            L.EN = "Take|the token|and put it|in token|receiver";
            L.ZH = "????";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10818;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет доп функций";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10819;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Инициализация";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10820;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Деинициализация";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10821;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Писать тревоги";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10822;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Не писать тревоги";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10823;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Писать хронометраж";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10824;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Не писать хронометраж";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10825;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Разовый";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10826;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Постоянный";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10827;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вездеход";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10828;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Открыть шлагбаум";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10829;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыть шлагбаум";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10830;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выход реверса";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10831;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вход реверса";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10832;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Светофор зеленый";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10833;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Светофор красный";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10834;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Подсветка кнопки выдачи карт";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10835;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Соленоид проезд";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10836;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Соленоид возврат";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10837;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Соленоид отказник";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10838;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Импульс выдачи жетона";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10839;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выход двухуровневого реверса";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10840;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кнопка выдачи карт";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10841;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Кнопка ГГС";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10842;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик дверей стойки";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10843;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Датчик люка стойки";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10844;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Жетон в губах";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10845;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Жетон под ридером";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10846;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Жетон на возврате";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10847;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Мало жетонов в хоппере";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10848;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вход двухуровневого реверса";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10849;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Вход";
            L.EN = "In";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10850;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выход";
            L.EN = "Out";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10851;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Слейв 3.0";
            L.EN = "Slave 3.0";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10852;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Порты А и Б";
            L.EN = "Ports A,B";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10853;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Порт А";
            L.EN = "Port A";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10854;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Порт Б";
            L.EN = "Port B";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10855;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Дополнительный порт H";
            L.EN = "Additional port H";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10856;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Силовые разъемы, Виганд, RS-485";
            L.EN = "Power supply, Wiegand, RS-485";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10857;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Дискретные входы";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10858;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Дискретные выходы";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10859;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Светофор выключен";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10860;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Подсветка губ диспенсера";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10861;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Истек|срок|действия|карты";
            L.EN = "Your card|is expired";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10862;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Cрок|действия|абонемента|заканчи-вается|через";
            L.EN = "Your card|expires in";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10863;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "дней";
            L.EN = "days";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10864;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Cрок|действия|абонемента|заканчи-вается";
            L.EN = "Your card|expires";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10865;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "сегодня";
            L.EN = "today";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10866;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд|разовых|клиентов|по ParkPass|запрещен";
            L.EN = "Parkpass|are|disabled|for your|tariff";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10867;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Скидка|успешно|активи-рована";
            L.EN = "Discount|activated";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10868;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Скидка|успешно|активи-рована|Вставьте|парковочную|карту";
            L.EN = "Discount|activated|Please,|put|the card";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10869;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Скидка|успешно|активи-рована|Повторно|вставьте|парковочный|жетон";
            L.EN = "Discount|activated|Please,|take|the tag|and put|again";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10870;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "На данную|сумму|скидка|не распро-страняется";
            L.EN = "Invalid|discount|amount";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10871;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Данная|компания|не предо-ставляет|скидок";
            L.EN = "Your|company|does not|provide|discount";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10872;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Данная|скидка|уже активи-рована";
            L.EN = "Discount|already|activated";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10873;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Данная|компания|не предо-ставляет|скидок";
            L.EN = "Invalid|discount";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10874;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Для|получения|скидки|отскани-руйте|QR код|на чеке";
            L.EN = "Scan your|QR code|for|discount";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10875;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Операция|откло-нена|банком|Пожалуйста,|заберите|парковочную|карту";
            L.EN = "Bank|connection|error|Please,|try again";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10876;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Выдача|билетов|невозможна";
            L.EN = "Tickets|cannot|be issued";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10877;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Вытащите|билет|с преды-дущего|проезда|и вставьте|свой";
            L.EN = "Please,|remove|the wrong|ticket";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10878;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Отскани-руйте|парковочный|билет";
            L.EN = "Please,|scan|the ticket";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10879;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Отскани-руйте|парковочный|билет|или|приложите|карту";
            L.EN = "Scan|the ticket|or apply|the parking|card";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10880;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Для проезда|заберите|неисполь|зованный|предыдущим|клиентом|билет";
            L.EN = "Please,|take the used|ticket|for entry";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10881;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Штрихкод|не|определен|системой";
            L.EN = "Sorry,|Invalid|Barcode";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10882;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Парковка|оплачена";
            L.EN = "Parking|is paid";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10883;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Вставьте|парковочную|карту|для выезда";
            L.EN = "Put|the parking|card|for exit";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10884;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Вставьте|парковочный|жетон|для выезда";
            L.EN = "Put|the parking|tag|for exit";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10885;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Пожалуйста,|вставьте|парковочную|карту,|по которой|произошла|оплата";
            L.EN = "This card|is wrong";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10886;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Недействи-тельные|данные";
            L.EN = "Wrong data";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10887;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Внимание!";
            L.EN = "Warning!";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10888;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Нет номера|в базе|при проезде|по распоз-наванию";
            L.EN = "Plate|number|not found|in database";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10889;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Неверный|тип клиента|при проезде|по распоз-наванию";
            L.EN = "Wrong|client type|for your|plate|number";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10890;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Нет мест|для|компании|при проезде|по распоз-наванию";
            L.EN = "No places|for your|plate|number";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10891;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд|по распоз-наванию|невозможен";
            L.EN = "Impossible|for your|plate|number";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10892;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Проезд|по распоз-наванию|заблоки-рован";
            L.EN = "Your plate|number|is blocked";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10893;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Неверная|зона|при проезде|по распоз-наванию";
            L.EN = "Wrong zone|for your|plate|number";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10894;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Неверная|группа|при проезде|по распоз-наванию";
            L.EN = "Wrong group|for your|plate|number";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10895;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Задолжен-ность|при проезде|по распоз-наванию";
            L.EN = "Debt|for your|plate|number";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10896;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Нет|жетонов|в|хоппере";
            L.EN = "No|tags|in|hopper";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10897;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Операция|откло-нена|банком|Пожалуйста,|заберите|парковочный|жетон";
            L.EN = "Bank|connection|error|Please,|try again";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10898;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Операция|откло-нена|банком|Пожалуйста,|заберите|парковочный|билет";
            L.EN = "Bank|connection|error|Please,|try again";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10899;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ИК2";
            L.EN = "MC2";
            L.ZH = "感应测井2";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10900;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Петля Д";
            L.EN = "Loop D2";
            L.ZH = "循环D2";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10901;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Открыть шлагбаум 2";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10902;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Закрыть шлагбаум 2";
            L.EN = "";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10903;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ИК2 свободен";
            L.EN = "MC2 Free";
            L.ZH = "感应测井空闲";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10904;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "ИК2 занят";
            L.EN = "MC2 Occupied";
            L.ZH = "感应测井占用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10905;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "loopD свободна";
            L.EN = "Loop D free";
            L.ZH = "loopA空闲";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10906;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "loopD занята";
            L.EN = "Loop D occupied";
            L.ZH = "loopA占用";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10907;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "barrier2 закрывается";
            L.EN = "barrier2 is closing";
            L.ZH = "障碍关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10908;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "barrier2 открывается";
            L.EN = "barrier2 is opening";
            L.ZH = "障碍打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10909;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "barrier2 закрыт";
            L.EN = "barrier2 is closed";
            L.ZH = "障碍关闭";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10910;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "barrier2 открыт";
            L.EN = "barrier2 is open";
            L.ZH = "障碍打开";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            LangDefSet.Add(L);

            
            L = new LangSettings();
            L.id = 10911;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Введите логин и пароль";
            L.EN = "Login, please";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "iltimos tizimga kiring";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10912;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Телефон:";
            L.EN = "Your phone:";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "Telefon:";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10913;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Пароль:";
            L.EN = "Password:";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "Parol:";
            LangDefSet.Add(L);


            L = new LangSettings();
            L.id = 10914;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Войти";
            L.EN = "Apply";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "kirmoq";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10915;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Нет скидок для данной компании";
            L.EN = "No discounts for your company";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "kompaniyada chegirmalar yo q";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10916;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Доступ в личный кабинет заблокирован";
            L.EN = "Your account is blocked";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "Shaxsiy hisobingizga kirish bloklangan";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10917;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Неверный логин или пароль";
            L.EN = "Invalid login or password";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "Tizimda foydalanuvchi topilmadi";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10918;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Карта №";
            L.EN = "Card ID:";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "ID karta:";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10919;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Время въезда:";
            L.EN = "Entry Time:";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "kirish vaqti:";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10920;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Тарифный план:";
            L.EN = "Tariff:";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "tarif rejasi:";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10921;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Тарифная сетка:";
            L.EN = "Tariff Shedule:";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "tariflar shkalasi:";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10922;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Задолженность:";
            L.EN = "Tariff Shedule:";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "qarzdorlik:";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10923;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Выберите скидку:";
            L.EN = "Choose discount:";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "chegirmani tanlang:";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10924;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Применить";
            L.EN = "Apply";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "topshirmoq";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10925;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Скидка успешно применена";
            L.EN = "Discount success";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "Chegirma muvaffaqiyatli qo llanildi";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10926;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 2; //int
            L.MaxLength = 0; //default
            L.RU = "Рабочее место оператора";
            L.EN = "Operator workstation";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "operator ish stantsiyasi";
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10927;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "QR код|заблоки-рован";
            L.EN = "QR is|blocked";
            L.ZH = "卡已锁定";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10928;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Нет QR|в базе|данных";
            L.EN = "No QR|in the|database";
            L.ZH = "数据库中没有此卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10929;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Или|используйте|штрихкод|для|получения|скидки";
            L.EN = "Or use|barcode";
            L.ZH = "拿停车卡或使用条形码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10930;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Пожарная|тревога";
            L.EN = "Fire|Alarm";
            L.ZH = "火警";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10931;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Внимание! Срок действия абонемента закончился! Для продолжения использования необходимо оплатить его продление.";
            L.EN = "Abonement expired. Please, buy abonement.";
            L.ZH = "火警";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10932;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Истек срок действия абонемента при проезде по RFID.";
            L.EN = "Abonement expired. Please, buy abonement.";
            L.ZH = "火警";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10933;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Истек срок действия абонемента при проезде по распознаванию.";
            L.EN = "Abonement expired. Please, buy abonement.";
            L.ZH = "火警";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.UZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10934;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Приглашение|будет|доступно с";
            L.EN = "Barcode|will be|available|from";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10935;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Скидка|будет|доступна с";
            L.EN = "Discount|will be|available|from";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 3;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10936;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Пожалуйста|подождите|Регистрация|чека";
            L.EN = "Please|wait|QR|registration";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 8;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10937;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Пожалуйста|проезжайте|Ваш QR чек";
            L.EN = "Please|drive|Your QR";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 100;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10938;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Пожалуйста|дождитесь|проезда|предыдущего|автомобиля";
            L.EN = "Please|wait|previous|car";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 8;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10939;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Приглашение|недоступно|для данного|автомобиля";
            L.EN = "Altimeter|invalid|for this|car";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10940;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Пожалуйста|подождите|Идет|обмен|с мобильным|устройством";
            L.EN = "Please|wait|mobile|device|confirmation";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 8;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10941;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Нет доступа|в зону|по вашему|приглашению";
            L.EN = "Access|denied|for your|invitation";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10942;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Тарифная|сетка не|соответ-ствует|занятому|высотомеру";
            L.EN = "Access|denied|for your|altimeter";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 4;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10943;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочный|билет или|используйте|штрихкод|для|получения|скидки";
            L.EN = "Take your|parking|token|or use|barcode";
            L.ZH = "拿停车号牌或使用条形码";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10944;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочный|билет,|активируйте|штрихкод|для|получения|скидки или|используйте|банковскую|карту";
            L.EN = "Take your|parking|token,|activate|barcode|or use|bank card";
            L.ZH = "拿停车号牌，激活条形码或使用银行卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10945;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочный|билет или|используйте|банковскую|карту";
            L.EN = "Take your|parking|token|or use|bank card";
            L.ZH = "拿停车号牌或使用银行卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10946;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Для|посещения|оплатите";
            L.EN = "Please pay|for|entrance";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10947;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "или|приложите|карту";
            L.EN = "Or take|your card";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10948;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочную|карту|и чек";
            L.EN = "Take your|parking|card|and invoice";
            L.ZH = "请拿走停车卡和支付的钱";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 9;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10949;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочный|жетон|и чек";
            L.EN = "Take your|parking|token|and invoice";
            L.ZH = "请拿出停车号牌";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10950;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Возьмите|парковочный|билет|и чек";
            L.EN = "Take your|parking|ticket|and invoice";
            L.ZH = "请拿出停车票";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 9;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10951;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Ваш QR чек.|Возьмите|парковочную|карту";
            L.EN = "Your QR|Take your|parking|card";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 100;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10952;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Ваш QR чек.|Возьмите|парковочный|жетон";
            L.EN = "Your QR|Take your|parking|tag";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 100;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10953;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Ваш QR чек.|Возьмите|парковочный|билет";
            L.EN = "Your QR|Take your|parking|ticket";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 100;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10954;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Вставьте|парковочный|билет";
            L.EN = "Please,|put|the ticket";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10955;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Вставьте|парковочный|билет|или|приложите|карту";
            L.EN = "Put|the ticket|or apply|the parking|card";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10956;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Приложите|карту|или|отскани-руйте|штрихкод";
            L.EN = "Scan|the barcode|or apply|the parking|card";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10957;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Вставьте|парковочный|билет|или|приложите|карту или|отскани-руйте|штрихкод";
            L.EN = "Scan|the barcode|or apply|the parking|card";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10958;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Отскани-руйте|парковочный|билет|или|приложите|карту или|отскани-руйте|штрихкод";
            L.EN = "Scan|the barcode|or apply|the parking|card";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            //hor.ShowMe("Тарифная сетка не соответ-ствует занятому высотомеру", "", false, true, 4);

            //hor.ShowMe("Нет доступа в зону по вашему приглашению", "", false, true, 4);


            //hor.ShowMe("Пожалуйста, подождите. Идет обмен с мобильным устройством", "", false, true, 8);

            //Приглашение недоступно для данного транспортного средства" 4

            //hor.ShowMe("Пожалуйста, дождитесь проезда предыдущего автомобиля", "", false, true, 8);

            /*
            L = new LangSettings();
            L.id = 10932;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Операция|откло-нена|банком|Пожалуйста,|заберите|парковочный|жетон";
            L.EN = "Bank|connection|error|Please,|try again";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);

            L = new LangSettings();
            L.id = 10933;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //int
            L.MaxLength = 0; //default
            L.RU = "Операция|откло-нена|банком|Пожалуйста,|заберите|парковочный|билет";
            L.EN = "Bank|connection|error|Please,|try again";
            L.ZH = "";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 0;
            L.EconomPicture = 0;
            LangDefSet.Add(L);
            */

            /*
              L = new LangSettings();
            L.id = 10037;
            L.DeviceType = 1; //1-rack
            L.InterfaceType = 1; //ext
            L.MaxLength = 0; //default
            L.RU = "Нет карты|в базе|данных";
            L.EN = "No card|in the|database";
            L.ZH = "数据库中没有此卡";
            L.ES = "";
            L.PT = "";
            L.HI = "";
            L.AR = "";
            L.IT = "";
            L.DE = "";
            L.FR = "";
            L.IN = "";
            L.KZ = "";
            L.EconomColor = 1;
            L.EconomPicture = 3;
            LangDefSet.Add(L);
             */

            //Chegirma muvaffaqiyatli qo'llanildi
            //topshirmoq
            //chegirmani tanlang:
            //qarzdorlik:
            //tariflar shkalasi:
            //tarif rejasi:
            //kirish vaqti:

            //китай то работает?
            LangSet = new List<LangSettings>();
            foreach (LangSettings lset in LangDefSet)
            {
                LangSet.Add(lset);
            }

        }

        public static bool IsTableExists()
        {
            Int32 newProdID = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[DeviceTranslation]')";
            using (SqlConnection conn = new SqlConnection(conlocal))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                try
                {
                    conn.Open();
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog(ex.Message);
                    return true;
                }
            }
        }

        public static void InsertDefaultLangSettings()
        {
            //run script if table isn't exists
            if (!IsTableExists())
            {
                //Create Table
                string script = File.ReadAllText(Application.StartupPath + "\\newlanguages.sql"); //написать sql функцию

                // split script on GO command
                IEnumerable<string> queries = Regex.Split(script, @"^\s*GO\s*$",
                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);
                try
                {
                    using (SqlConnection conn = new SqlConnection(conlocal))
                    {

                        conn.Open();
                        foreach (string query in queries)
                        {
                            if (query.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(query, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка выполнения скрипта создания таблиц " + conlocal);
                }

                Thread.Sleep(1000);
                //Надо заполнить табличку

                Thread.Sleep(1000);
            }
            //InsertTypesTable();
            List<LangSettings> AlreadyList = new List<LangSettings>();

            //try
            //{

                string sqlQuery = "SELECT * FROM DeviceTranslation";
                SqlConnection connection = new SqlConnection(conlocal);
                connection.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlQuery, connection);
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds, "DeviceTranslation");

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    LangSettings rackrow = new LangSettings();
                    rackrow.id = Convert.ToInt32(dr["ID"]);
                    rackrow.DeviceType = Convert.ToInt32(dr["DeviceType"]);
                    rackrow.InterfaceType = Convert.ToInt32(dr["InterfaceType"]);
                    rackrow.MaxLength = Convert.ToInt32(dr["MaxLength"]);
                    rackrow.RU = Convert.ToString(dr["RU"]);
                    rackrow.EN = Convert.ToString(dr["EN"]);
                    rackrow.ZH = Convert.ToString(dr["ZH"]);
                    rackrow.ES = Convert.ToString(dr["ES"]);
                    rackrow.PT = Convert.ToString(dr["PT"]);
                    rackrow.HI = Convert.ToString(dr["HI"]);
                    rackrow.AR = Convert.ToString(dr["AR"]);
                    rackrow.IT = Convert.ToString(dr["IT"]);
                    rackrow.DE = Convert.ToString(dr["DE"]);
                    rackrow.FR = Convert.ToString(dr["FR"]);
                    rackrow.IN = Convert.ToString(dr["IN"]);
                    rackrow.KZ = Convert.ToString(dr["KZ"]);
                    rackrow.UZ = Convert.ToString(dr["UZ"]);
                    AlreadyList.Add(rackrow);
                }
                connection.Close();
            //}
            //catch (Exception exx)
            //{
            //    Logging.ToLog("mouse");
            //    Logging.ToLog("Ошибка при базовом подключении " + conlocal);
            //}


            bool founded = false;
            //Сверяем списки с дефолтными:
            for (int i = 0; i < LangDefSet.Count; i++)
            {
                founded = false;
                for (int j = 0; j < AlreadyList.Count; j++)
                {
                    if (LangDefSet[i].id == AlreadyList[j].id)
                    {
                        founded = true;
                    }
                }
                if (!founded)
                {
                    //Добавляем запись
                    InsertLanguageRecord(LangDefSet[i]);
                }
            }
        }

        public static void InsertLanguageRecord(LangSettings data)
        {
            string fields = "(ID, DeviceType, InterfaceType, MaxLength, RU, EN, ZH, ES, PT, HI, AR, IT, DE, FR, \"IN\", KZ, UZ, EconomColor, EconomPicture)";
            DateTime dt = new DateTime(2000, 1, 1);
            string values = "";
            values += "('" + data.id + "', '" + data.DeviceType + "', '" + data.InterfaceType + "', '" + data.MaxLength + "', N'" + data.RU + "', N'" + data.EN + "', N'" + data.ZH + "', N'" + data.ES + "', N'" + data.PT + "', N'" + data.HI + "', N'" + data.AR + "', N'" + data.IT + "', N'" + data.DE + "', N'" + data.FR + "', N'" + data.IN + "', N'" + data.KZ + "', N'" + data.UZ + "', '" + data.EconomColor + "', '" + data.EconomPicture + "')";
            string sqlQuery = "INSERT INTO DeviceTranslation" + " " + fields + " VALUES " + values;

            //Debug.Print(values);

            try
            {
                using (SqlConnection connection = new SqlConnection(conlocal))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlQuery, connection);
                    command.Parameters.AddWithValue("Sync", dt);
                    int number = command.ExecuteNonQuery();
                }
            }
            catch (Exception exx)
            {
                Logging.ToLog("Не смог записать перевод " + data.id.ToString() + " "  + Environment.NewLine + sqlQuery);
            }
        }


        //загрузка языков из БД..
        public static void LoadCurrentLang()
        {
            //
                try
                {
                    string sqlQuery = "SELECT * FROM DeviceTranslation";
                    SqlConnection connection = new SqlConnection(conlocal);
                    connection.Open();

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlQuery, connection);
                    DataSet ds = new DataSet();
                    dataAdapter.Fill(ds, "DeviceTranslation");

                int RecCount = ds.Tables[0].Rows.Count;
                if (RecCount > 0)
                {
                    LangSet = new List<LangSettings>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        LangSettings rackrow = new LangSettings();
                        rackrow.id = Convert.ToInt32(dr["ID"]);
                        rackrow.DeviceType = Convert.ToInt32(dr["DeviceType"]);
                        rackrow.InterfaceType = Convert.ToInt32(dr["InterfaceType"]);
                        rackrow.MaxLength = Convert.ToInt32(dr["MaxLength"]);

                        rackrow.RU = Convert.ToString(dr["RU"]);
                        
                        rackrow.Econom_RU = rackrow.RU;
                        rackrow.RU = rackrow.RU.Replace('|', ' ');
                        rackrow.RU = rackrow.RU.Replace("-", "");
                        
                        rackrow.EN = Convert.ToString(dr["EN"]);
                        rackrow.Econom_EN = rackrow.EN;
                        rackrow.EN = rackrow.EN.Replace('|', ' ');
                        rackrow.EN = rackrow.EN.Replace("-", "");

                        rackrow.ZH = Convert.ToString(dr["ZH"]);
                        rackrow.ES = Convert.ToString(dr["ES"]);
                        rackrow.PT = Convert.ToString(dr["PT"]);
                        rackrow.HI = Convert.ToString(dr["HI"]);
                        rackrow.AR = Convert.ToString(dr["AR"]);
                        rackrow.IT = Convert.ToString(dr["IT"]);
                        rackrow.DE = Convert.ToString(dr["DE"]);
                        rackrow.FR = Convert.ToString(dr["FR"]);
                        rackrow.IN = Convert.ToString(dr["IN"]);
                        rackrow.KZ = Convert.ToString(dr["KZ"]);
                        rackrow.UZ = Convert.ToString(dr["UZ"]);
                        rackrow.UZ = rackrow.UZ.Replace('|', ' ');
                        rackrow.UZ = rackrow.UZ.Replace("-", "");

                        LangSet.Add(rackrow);

                        //наполняем лист для эконома именно тут!!!

                    }
                }
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Logging.ToLog("Ошибка чтения настроек");
                }
        }

        //применение настройки в зависимости от языка (касается языкового интерфейса)
        //второй язык также можно выставить такой же процедурой (пока оставим дефолтно англ)
        public static void UpdateLanguage1(int langIndex)
        {
            CurrentLangDictionary = new Dictionary<int, string>();

            foreach (LangSettings lang in LangSet)
            {
                switch (langIndex)
                {
                    case 0:
                        CurrentLangDictionary.Add(lang.id, lang.RU);
                        break;
                    case 1:
                        CurrentLangDictionary.Add(lang.id, lang.EN);
                        break;
                    case 2:
                        CurrentLangDictionary.Add(lang.id, lang.ZH);
                        break;
                    case 3:
                        CurrentLangDictionary.Add(lang.id, lang.ES);
                        break;
                    case 4:
                        CurrentLangDictionary.Add(lang.id, lang.PT);
                        break;
                    case 5:
                        CurrentLangDictionary.Add(lang.id, lang.HI);
                        break;
                    case 6:
                        CurrentLangDictionary.Add(lang.id, lang.AR);
                        break;
                    case 7:
                        CurrentLangDictionary.Add(lang.id, lang.IT);
                        break;
                    case 8:
                        CurrentLangDictionary.Add(lang.id, lang.DE);
                        break;
                    case 9:
                        CurrentLangDictionary.Add(lang.id, lang.FR);
                        break;
                    case 10:
                        CurrentLangDictionary.Add(lang.id, lang.IN);
                        break;
                    case 11:
                        CurrentLangDictionary.Add(lang.id, lang.KZ);
                        break;
                    case 12:
                        CurrentLangDictionary.Add(lang.id, lang.UZ);
                        break;
                    default:
                        break;
                }                
            }
        }

        public static void UpdateLanguage2(int langIndex2)
        {

            SecondLangDictionary = new Dictionary<int, string>();

            foreach (LangSettings lang in LangSet)
            {
                switch (langIndex2)
                {
                    case 0:
                        SecondLangDictionary.Add(lang.id, lang.RU);
                        break;
                    case 1:
                        SecondLangDictionary.Add(lang.id, lang.EN);
                        break;
                    case 2:
                        SecondLangDictionary.Add(lang.id, lang.ZH);
                        break;
                    case 3:
                        SecondLangDictionary.Add(lang.id, lang.ES);
                        break;
                    case 4:
                        SecondLangDictionary.Add(lang.id, lang.PT);
                        break;
                    case 5:
                        SecondLangDictionary.Add(lang.id, lang.HI);
                        break;
                    case 6:
                        SecondLangDictionary.Add(lang.id, lang.AR);
                        break;
                    case 7:
                        SecondLangDictionary.Add(lang.id, lang.IT);
                        break;
                    case 8:
                        SecondLangDictionary.Add(lang.id, lang.DE);
                        break;
                    case 9:
                        SecondLangDictionary.Add(lang.id, lang.FR);
                        break;
                    case 10:
                        SecondLangDictionary.Add(lang.id, lang.IN);
                        break;
                    case 11:
                        SecondLangDictionary.Add(lang.id, lang.KZ);
                        break;
                    case 12:
                        SecondLangDictionary.Add(lang.id, lang.UZ);
                        break;
                    default:
                        break;
                }
            }
        }

        public static void UpdateEmptyLanguage()
        {
            CurrentLangDictionary = new Dictionary<int, string>();

            SecondLangDictionary = new Dictionary<int, string>();

            for (int i = 10000; i < 12000; i++)
            {
                //LangSettings emptylang = new LangSettings();
                //emptylang.id = 10000;
                CurrentLangDictionary.Add(i, "");
                SecondLangDictionary.Add(i, "");

            }
        }
    }
}
