﻿using Microsoft.Win32;
using NAudio.Wave;
using RPS_Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static stoyka01.GlobalClass;

namespace stoyka01
{
    public class ServiceClass
    {
        public static byte[] ReverseMyfare(byte[] arg)
        {
            //Если 4 байтная карта
            if (arg[1] == 0)
            {
                return new byte[] { 0, 0, 0, 0, arg[7], arg[6], arg[5], arg[4] };
            }
            else //7-байтная
            {
                return new byte[] { 0,arg[7],arg[6],arg[5],arg[4], arg[3], arg[2], arg[1]};
            }
        }

        public static bool IsNumeric(string s)
        {
            double output;
            return double.TryParse(s, out output);
        }

        public static string GuidTo4(Guid g)
        {
            string guidstr = g.ToString();
            string ret = "";

            for (int i = 0; i < 4; i++)
            {
                ret += guidstr.Substring(i, 1);
            }
            ret = ret.Replace("a", "10");
            ret = ret.Replace("b", "11");
            ret = ret.Replace("c", "12");
            ret = ret.Replace("d", "13");
            ret = ret.Replace("e", "14");
            ret = ret.Replace("f", "15");

            if (ret.Length > 4)
            {
                ret = ret.Substring(0, 4);
            }

            return ret;
        }

        public static string GetTimeStamp2015()
        {
            //Время - 151027200 - кол - во секунд с 00:00:00 01.01.2015
            DateTime dtNow = DateTime.Now;
            DateTime dt2015 = new DateTime(2015, 1, 1);

            double ts = (dtNow - dt2015).TotalSeconds;
            long tsint = Convert.ToInt64(ts);
            return tsint.ToString();
        }

        public static DateTime RestoreTimeStamp2015(string arg)
        {
            long tsint = Convert.ToInt64(arg);
            DateTime dt2015 = new DateTime(2015, 1, 1);
            TimeSpan ts = new TimeSpan(tsint*TimeSpan.TicksPerSecond);

            DateTime ret = dt2015 + ts;
            return ret;
        }

        public static string ByteAToString(byte[] bA)
        {
            string S = "";
            foreach (byte b in bA)
                S = S + b.ToString("X2"); ;
            return S;
        }

        public static byte[] StringToByteA(string S)
        {
            int a = S.Length / 2;
            byte[] bA = new byte[a];
            for (int i = 0; i < a * 2; i += 2)
            {
                bool result = byte.TryParse(S.Substring(i, 2), NumberStyles.HexNumber, null as IFormatProvider, out bA[i / 2]);
                if (!result)
                    bA[i / 2] = 0xff;
            }
            return bA;
        }

        public static byte[] EncryptStringToBytes_Aes(string plainText)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            byte[] encrypted;
            // Create an Aes object
            // with the specified key and IV.
            // Create a decrytor to perform the stream transform.
            ICryptoTransform encryptor = myAes.CreateEncryptor(myAes.Key, myAes.IV);

            // Create the streams used for encryption.
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        //Write all data to the stream.
                        swEncrypt.Write(plainText);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }

        public static string DecryptStringFromBytes_Aes(byte[] cipherText)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create a decrytor to perform the stream transform.
            ICryptoTransform decryptor = myAes.CreateDecryptor(myAes.Key, myAes.IV);

            // Create the streams used for decryption.
            using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        // Read the decrypted bytes from the decrypting  stream
                        // and place them in a string.
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            return plaintext;
        }

        public static void NetshStart()
        {
            string dn = Environment.UserDomainName;
            string un = Environment.UserName;

            string cmd = "netsh http add urlacl url=http://+:2222/ user=" + dn + "\\" + un;
            ProcessStartInfo psiOpt = new ProcessStartInfo(@"cmd.exe", @"/C " + cmd);
            psiOpt.WindowStyle = ProcessWindowStyle.Hidden;
            psiOpt.RedirectStandardOutput = true;
            psiOpt.UseShellExecute = false;
            psiOpt.CreateNoWindow = true;
            Process procCommand = Process.Start(psiOpt);
            StreamReader srIncoming = procCommand.StandardOutput;
            procCommand.WaitForExit();
            string result = srIncoming.ReadToEnd();

            string cmd1 = "netsh http add urlacl url=http://+:4444/ user=" + dn + "\\" + un;
            ProcessStartInfo psiOpt1 = new ProcessStartInfo(@"cmd.exe", @"/C " + cmd1);
            psiOpt1.WindowStyle = ProcessWindowStyle.Hidden;
            psiOpt1.RedirectStandardOutput = true;
            psiOpt1.UseShellExecute = false;
            psiOpt1.CreateNoWindow = true;
            Process procCommand1 = Process.Start(psiOpt1);
            StreamReader srIncoming1 = procCommand1.StandardOutput;
            procCommand1.WaitForExit();
            string result1 = srIncoming1.ReadToEnd();

            string cmd3 = "netsh http add urlacl url=http://+:8880/ user=" + dn + "\\" + un;
            ProcessStartInfo psiOpt3 = new ProcessStartInfo(@"cmd.exe", @"/C " + cmd3);
            psiOpt3.WindowStyle = ProcessWindowStyle.Hidden;
            psiOpt3.RedirectStandardOutput = true;
            psiOpt3.UseShellExecute = false;
            psiOpt3.CreateNoWindow = true;
            Process procCommand3 = Process.Start(psiOpt3);
            StreamReader srIncoming3 = procCommand3.StandardOutput;
            procCommand3.WaitForExit();
            string result3 = srIncoming3.ReadToEnd();

            string cmd2 = "aspnet_regiis -pa \"NetFrameworkConfigurationKey\" \"" + dn + "\\" + un + "\"";
            ProcessStartInfo psiOpt2 = new ProcessStartInfo(@"cmd.exe", @"/C " + cmd2);
            psiOpt2.WindowStyle = ProcessWindowStyle.Hidden;
            psiOpt2.RedirectStandardOutput = true;
            psiOpt2.UseShellExecute = false;
            psiOpt2.CreateNoWindow = true;
            Process procCommand2 = Process.Start(psiOpt2);
            StreamReader srIncoming2 = procCommand2.StandardOutput;
            procCommand2.WaitForExit();
            string result2 = srIncoming2.ReadToEnd();
        }

        // Unprotect the connectionStrings section.
        public static void ProtectConfiguration(string section = "connectionStrings")
        {

            // Get the application configuration file.
            Configuration config =
                    ConfigurationManager.OpenExeConfiguration(
                    ConfigurationUserLevel.None);

            // Get the section to protect.
            ConfigurationSection Section =
                config.GetSection(section);

            if (Section != null)
            {
                if (!Section.SectionInformation.IsProtected)
                {
                    if (!Section.ElementInformation.IsLocked)
                    {
                        Section.SectionInformation.ProtectSection(null);

                        Section.SectionInformation.ForceSave = true;
                        config.Save(ConfigurationSaveMode.Full, true);
                    }
                    else
                    {

                    }
                }
                else
                {

                }
            }
            else
            {
            }

        }

        public static void UnProtectConfiguration(string section = "connectionStrings")
        {
            // Get the application configuration file.
            Configuration config =
                    ConfigurationManager.OpenExeConfiguration(
                    ConfigurationUserLevel.None);

            // Get the section to unprotect.
            ConfigurationSection Section =
                config.GetSection(section);

            if (Section != null)
            {
                if (Section.SectionInformation.IsProtected)
                {
                    if (!Section.ElementInformation.IsLocked)
                    {
                        // Unprotect the section.
                        Section.SectionInformation.UnprotectSection();


                        Section.SectionInformation.ForceSave = true;
                        config.Save(ConfigurationSaveMode.Full, true);
                    }
                    else
                    {
                    }
                }
                else
                {
                }
            }
            else
            {
            }
        }

        public static bool MoveRegistryKey()
        {
            //RegistryKey NewRegistryKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\RPS");

            //RegistryKey NewRegistryKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, @"Software\RPS");

            RegistryKey NewRegistryKey = Registry.LocalMachine.OpenSubKey("Software\\RPS");

            try
            {
                if (NewRegistryKey == null)
                {
                    NewRegistryKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\RPS");
                    if (NewRegistryKey.SubKeyCount < 3)
                    {
                        //Debug.Print(NewRegistryKey.SubKeyCount.ToString());
                        //Копируем ключи с HKCU
                        RegistryKey oldRegistryKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RPS");

                        NewRegistryKey.SetValue("conlocalReg", oldRegistryKey.GetValue("conlocalReg"));
                        NewRegistryKey.SetValue("conserverReg", oldRegistryKey.GetValue("conserverReg"));
                        NewRegistryKey.SetValue("EntitiesReg", oldRegistryKey.GetValue("EntitiesReg"));
                        Thread.Sleep(200);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
            
        }

        public static bool CheckRegistry()
        {

            RegistryKey currRegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\RPS");

            string conlocalReg;
            Rps.Crypto.CryptoDecrypto cryp = new Rps.Crypto.CryptoDecrypto();

            // Расшифровываем конфигурацию (если она зашифрована, если нет - не меняет)
            UnProtectConfiguration();

            //Берем строки соединения для синхронизатора
            string section = "connectionStrings";
            Configuration config =
            ConfigurationManager.OpenExeConfiguration(
            ConfigurationUserLevel.None);

            // Get the section to unprotect.
            ConfigurationSection Section = config.GetSection(section);

            // строка 
            conlocal = ConfigurationManager.ConnectionStrings["DeviceConnect"].ConnectionString;
            //Debug.Print(conlocal);

            if (conlocal == conlocal0 || conlocal == null)
            {
                if (currRegistryKey.GetValue("conlocalReg") != null)
                {
                    conlocalReg = currRegistryKey.GetValue("conlocalReg").ToString();
                    conlocal = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(conlocalReg));
                    if (conlocal == null)
                    {
                        SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conlocalReg);
                        conlocal = conlocalReg;
                        string Password = cb.Password;
                        string Password1 = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(Password));
                        if (Password1 == null)
                        {
                            Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                            cb.Password = Password;
                            currRegistryKey.SetValue("conlocalReg", cb.ToString());
                        }
                        else
                        {
                            cb.Password = Password1;
                            conlocal = cb.ToString();
                        }
                    }
                    else
                    {
                        SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conlocal);
                        string Password = cb.Password;
                        Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                        cb.Password = Password;
                        currRegistryKey.SetValue("conlocalReg", cb.ToString());
                    }
                }
            }
            else
            {
                SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conlocal);
                string Password = cb.Password;
                Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                cb.Password = Password;
                try
                {
                    currRegistryKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\RPS");
                    currRegistryKey.SetValue("conlocalReg", cb.ToString());
                }
                catch (Exception ex) {
                    return false;
                }

            }

            string conserverReg;
            // строка                     
            conserver = ConfigurationManager.ConnectionStrings["ServerConnect"].ConnectionString;
            if (conserver == conserver0 || conserver == null)
            {
                if (currRegistryKey.GetValue("conserverReg") != null)
                {
                    conserverReg = currRegistryKey.GetValue("conserverReg").ToString();
                    conserver = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(conserverReg));
                    if (conserver == null)
                    {
                        SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conserverReg);
                        conserver = conserverReg;
                        string Password = cb.Password;
                        string Password1 = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(Password));
                        if (Password1 == null)
                        {
                            Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                            cb.Password = Password;
                            try
                            {
                                currRegistryKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\RPS");
                                currRegistryKey.SetValue("conserverReg", cb.ToString());
                            }
                            catch (Exception ex)
                            {
                                return false;
                            }

                        }
                        else
                        {
                            cb.Password = Password1;
                            conserver = cb.ToString();
                        }
                    }
                    else
                    {
                        SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conserver);
                        string Password = cb.Password;
                        Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                        cb.Password = Password;

                        try
                        {
                            currRegistryKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\RPS");
                            currRegistryKey.SetValue("conserverReg", cb.ToString());
                        }
                        catch (Exception ex)
                        {
                            return false;
                        }
                    }
                }
            }
            else
            {
                SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conserver);
                string Password = cb.Password;
                Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                cb.Password = Password;

                try
                {
                    currRegistryKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\RPS");
                    currRegistryKey.SetValue("conserverReg", cb.ToString());
                }
                catch (Exception ex)
                {
                    return false;
                }
            }


            string EntitiesReg;
            // строка                     
            EntitiesCS = ConfigurationManager.ConnectionStrings["Entities"].ConnectionString;
            if (EntitiesCS == Entities0 | EntitiesCS == null)
            {
                if (currRegistryKey.GetValue("EntitiesReg") != null)
                {
                    EntitiesReg = currRegistryKey.GetValue("EntitiesReg").ToString();
                    EntitiesCS = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(EntitiesReg));
                    if (EntitiesCS == null)
                    {
                        EntitiesCS = EntitiesReg;
                        Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(EntitiesCS));
                        try
                        {
                            currRegistryKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\RPS");
                            currRegistryKey.SetValue("EntitiesReg", Password.ToString());
                        }
                        catch (Exception ex)
                        {
                            return false;
                        }
                    }
                    else
                    {

                    }
                }
            }
            else
            {
                Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(EntitiesCS));

                try
                {
                    //currRegistryKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\RPS"); //
                    currRegistryKey.SetValue("EntitiesReg", Password.ToString());
                }
                catch (Exception ex)
                {
                    return false;
                }

            }

            currRegistryKey.Close();

            // сохраняем новый пустой конфиг
            var conlocalA = config.ConnectionStrings.ConnectionStrings["DeviceConnect"];
            conlocalA.ConnectionString = conlocal0;
            var conserverA = config.ConnectionStrings.ConnectionStrings["ServerConnect"];
            conserverA.ConnectionString = conserver0;
            var EntitiesA = config.ConnectionStrings.ConnectionStrings["Entities"];
            EntitiesA.ConnectionString = Entities0;

            ConfigurationManager.RefreshSection("connectionStrings");
            Section.SectionInformation.ForceSave = true;
            config.Save(ConfigurationSaveMode.Full, true);

            //conlocal = @"Data Source = RPSRACK2\SQLEXPRESS; Initial Catalog = rpsMDBG; User ID = sa; Password = parol-RPS1";
            //conserver = @"Data Source = 192.168.1.1; Initial Catalog = rpsMDBG; User ID = sa; Password = parol-RPS1";
            //EntitiesCS = EntitiesCS.Replace("localhost", @"RPSRACK2\SQLEXPRESS");

            EntitiesA.ConnectionString = EntitiesCS;
            Entities.EntitiesCS = EntitiesA.ToString();

            //закооментить

            return true;
        }

        //теперь используем нулевой айпишник!
        public static void GetIP()
        {
            IpList = new List<string>();

            string strHostName = Dns.GetHostName();
            IPHostEntry iphostentry = Dns.GetHostEntry(strHostName);
            foreach (IPAddress ipaddress in iphostentry.AddressList)
            {
                if (ipaddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    //IP = ipaddress.ToString();
                    //Logging.ToLog("Ip=" + IP);
                    IpList.Add(ipaddress.ToString());
                }
            }
            IP = IpList[0];
        }

        public static bool ExecuteSQLScript(string connectionString, int CommandTimeOut, string scriptFileName)
        {
            if (!File.Exists(scriptFileName))
            {
                Logging.ToLog("Файл " + scriptFileName + " не найден.\r\n Продолжение невозможно.\r\n");
                return false;
            }
            // загружаем скрипт
            string script = File.ReadAllText(scriptFileName, Encoding.GetEncoding(1251));


            // делим скрипт на части
            string[] separators = { "GO\r\n" };
            string[] idents = script.Split(separators, StringSplitOptions.None);
            int n_idents = idents.Length;

            // выполняем части
            string str_sql = null;
            using (SqlConnection sqlConn = new SqlConnection(connectionString))
            {
                //SqlConnection sqlConn = new SqlConnection(connectionString);
                sqlConn.Open();
                SqlCommand Command = sqlConn.CreateCommand();
                SqlTransaction transaction;
                // Start a local transaction.
                transaction = sqlConn.BeginTransaction("LoadSQLTransaction");
                Command.Connection = sqlConn;
                Command.Transaction = transaction;
                Command.CommandTimeout = CommandTimeOut;
                try
                {
                    for (int i = 0; i < n_idents - 1; i++)
                    {
                        str_sql = idents[i];
                        if (!string.IsNullOrEmpty(str_sql))
                        {
                            Command.CommandText = str_sql;
                            Command.ExecuteNonQuery();
                        }
                    }
                    // Attempt to commit the transaction.
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Logging.ToLog(string.Format(
                        "Ошибка выполнения скрипта {0}.\r\nНе выполнен фрагмент:\r\n***{1}***\r\nИсключение: {2}\r\n",
                        scriptFileName, str_sql, ex.Message));

                    // Попытка откатить транзакцию
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        // Если возникла ошибка при откате транзакции
                        Logging.ToLog(string.Format("Rollback Exception Type: {0}\r\n", ex2.GetType()));
                        Logging.ToLog(string.Format("Message: {0}\r\n", ex2.Message));
                    }
                    return false;
                }
            }
            return true;
        }

        public static bool IsSqlServiceLaunched()
        {
            try
            {
                ServiceController ServiceCtrl = new ServiceController("MSSQL$SQLEXPRESS");
                Thread.Sleep(300);
                if (ServiceCtrl.Status.Equals(ServiceControllerStatus.Stopped) | ServiceCtrl.Status.Equals(ServiceControllerStatus.StopPending))
                {
                    try
                    {
                        ServiceCtrl.Start();
                        Thread.Sleep(5000);
                        Logging.ToLog(" ServiceCtrl.Start. OK");
                        Thread.Sleep(500);
                        return true;
                    }
                    catch (Exception u)
                    {
                        RunBdOk = false;
                        Logging.ToLog("Exception ServiceCtrl.Start. " + u);
                        Thread.Sleep(500);
                        return false;
                    }
                }
                else
                {
                    Logging.ToLog("MSSQL$SQLEXPRESS.Started");
                    return true;
                }
            }
            catch
            {
                RunBdOk = false;
                Logging.ToLog(" not MSSQL$SQLEXPRESS ");
                return false;
            }
        }

        /*
        public static string LanguageText(int msg, string LanguageId) // текст из языковой таблицы
        {
            string msgtext = "";
            if (Language[msg] != null)
            {
                try
                {
                    language = Language[msg].Split('\t');

                    if (LanguageId == "RU") msgtext = language[1];   //ru
                    if (LanguageId == "EN") msgtext = language[2];   //en
                    if (LanguageId == "CN") msgtext = language[3];   //CN
                    if (LanguageId == "ESP") msgtext = language[4];   //ESP
                    if (LanguageId == "KZ") msgtext = language[5];   //KZ

                }
                catch
                {
                    try
                    {
                        language = Language[msg].Split(';');

                        if (LanguageId == "RU") msgtext = language[1];   //ru
                        if (LanguageId == "EN") msgtext = language[2];   //en
                        if (LanguageId == "CN") msgtext = language[3];   //CN
                        if (LanguageId == "ESP") msgtext = language[4];   //ESP
                        if (LanguageId == "KZ") msgtext = language[5];   //KZ

                    }
                    catch
                    {

                    }
                }
            }

            return msgtext;
        }
        */

            /*
        public static string LanguageTextN(int msg, string LanguageId)    // текст из языковой таблицы (для language3 чтоб было хоть что-то); если пусто, то RU
        {
            string msgtext = "";
            if (Language[msg] != null)
            {
                try
                {
                    language = Language[msg].Split('\t');

                    if (LanguageId == "RU") msgtext = language[1];   //ru
                    if (LanguageId == "EN") msgtext = language[2];   //en
                    if (LanguageId == "CN") msgtext = language[3];   //CN
                    if (LanguageId == "ESP") msgtext = language[4];   //ESP
                    if (LanguageId == "KZ") msgtext = language[5];   //KZ

                    if (msgtext == "" & language3 != "RU") msgtext = language[2];   //en
                    if (msgtext == "") msgtext = language[1];   //ru
                }
                catch
                {
                    try
                    {
                        language = Language[msg].Split(';');

                        if (LanguageId == "RU") msgtext = language[1];   //ru
                        if (LanguageId == "EN") msgtext = language[2];   //en
                        if (LanguageId == "CN") msgtext = language[3];   //CN
                        if (LanguageId == "ESP") msgtext = language[4];   //ESP
                        if (LanguageId == "KZ") msgtext = language[5];   //KZ

                        if (msgtext == "" & language3 != "RU") msgtext = language[2];   //en
                        if (msgtext == "") msgtext = language[1];   //ru
                    }
                    catch
                    {
                    }
                }
            }
            return msgtext;
        }
        */

        public static void WaveOnDataAvailable(object sender, WaveInEventArgs e)
        {
            for (int index = 0; index < e.BytesRecorded; index += 2)
            {
                short sample = (short)((e.Buffer[index + 1] << 8) | e.Buffer[index + 0]);

                float amplitude = sample / 32768f;
                level = Math.Abs(amplitude); // от 0 до 1

            }
        }

        //получаем тег
        public static string ConvertNewHid(long hidnumb)
        {
            //if (type=="28")
            //{
                //2477555712
                //Debug.Print(Convert.ToString(hidnumb, 2));
                string x = Convert.ToString(hidnumb, 2);
                string x2 = x.Substring(1, 16);
                int o = Convert.ToInt32(x2, 2);
                //Debug.Print(o.ToString());
                return o.ToString();
            //}
            //else
            //{
            //    return null;
            //}
        }

    }
}
