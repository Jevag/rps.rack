﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stoyka01
{
    public class JoinAudio
    {
        public static List<string> Before100(int Number)
        {
            List<string> outList = new List<string>();

            string strNumber = Number.ToString();

            if (Number < 21 || Number == 30 || Number == 40 || Number == 50 || Number == 60 || Number == 70 || Number == 80 || Number == 90)
            {
                outList.Add(Application.StartupPath + "\\sound\\items\\" + strNumber + ".wav");
            }
            else
            {
                string FirstSym = strNumber.Substring(0, 1);

                switch (FirstSym)
                {
                    case "2":
                        outList.Add(Application.StartupPath + "\\sound\\items\\20.wav");
                        break;
                    case "3":
                        outList.Add(Application.StartupPath + "\\sound\\items\\30.wav");
                        break;
                    case "4":
                        outList.Add(Application.StartupPath + "\\sound\\items\\40.wav");
                        break;
                    case "5":
                        outList.Add(Application.StartupPath + "\\sound\\items\\50.wav");
                        break;
                    case "6":
                        outList.Add(Application.StartupPath + "\\sound\\items\\60.wav");
                        break;
                    case "7":
                        outList.Add(Application.StartupPath + "\\sound\\items\\70.wav");
                        break;
                    case "8":
                        outList.Add(Application.StartupPath + "\\sound\\items\\80.wav");
                        break;
                    case "9":
                        outList.Add(Application.StartupPath + "\\sound\\items\\90.wav");
                        break;
                    default:
                        break;
                }
                string SecondSym = strNumber.Substring(1, 1);
                outList.Add(Application.StartupPath + "\\sound\\items\\" + SecondSym + ".wav");
            }

            return outList;

        }

        public static List<string> Before1000(int Number)
        {
            List<string> outList = new List<string>();

            string strNumber = Number.ToString();

            string FirstSym = strNumber.Substring(0, 1);

            switch (FirstSym)
            {
                case "1":
                    outList.Add(Application.StartupPath + "\\sound\\items\\100.wav");
                    break;
                case "2":
                    outList.Add(Application.StartupPath + "\\sound\\items\\200.wav");
                    break;
                case "3":
                    outList.Add(Application.StartupPath + "\\sound\\items\\300.wav");
                    break;
                case "4":
                    outList.Add(Application.StartupPath + "\\sound\\items\\400.wav");
                    break;
                case "5":
                    outList.Add(Application.StartupPath + "\\sound\\items\\500.wav");
                    break;
                case "6":
                    outList.Add(Application.StartupPath + "\\sound\\items\\600.wav");
                    break;
                case "7":
                    outList.Add(Application.StartupPath + "\\sound\\items\\700.wav");
                    break;
                case "8":
                    outList.Add(Application.StartupPath + "\\sound\\items\\800.wav");
                    break;
                case "9":
                    outList.Add(Application.StartupPath + "\\sound\\items\\900.wav");
                    break;
                default:
                    break;
            }
            string SecondSym = strNumber.Substring(1, 2);
            if (SecondSym.Substring(0, 1) == "0")
            {
                SecondSym = SecondSym.Substring(1, 1);
            }

            int value100 = Convert.ToInt32(SecondSym);
            List<string> List100 = Before100(value100);

            foreach (string itm in List100)
            {
                outList.Add(itm);
            }

            return outList;

        }

        public static List<string> Before10000(int Number)
        {
            List<string> outList = new List<string>();

            string strNumber = Number.ToString();

            string FirstSym = strNumber.Substring(0, 1);

            switch (FirstSym)
            {
                case "1":
                    outList.Add(Application.StartupPath + "\\sound\\items\\1000.wav");
                    break;
                case "2":
                    outList.Add(Application.StartupPath + "\\sound\\items\\2000.wav");
                    break;
                case "3":
                    outList.Add(Application.StartupPath + "\\sound\\items\\3000.wav");
                    break;
                case "4":
                    outList.Add(Application.StartupPath + "\\sound\\items\\4000.wav");
                    break;
                case "5":
                    outList.Add(Application.StartupPath + "\\sound\\items\\5000.wav");
                    break;
                case "6":
                    outList.Add(Application.StartupPath + "\\sound\\items\\6000.wav");
                    break;
                case "7":
                    outList.Add(Application.StartupPath + "\\sound\\items\\7000.wav");
                    break;
                case "8":
                    outList.Add(Application.StartupPath + "\\sound\\items\\8000.wav");
                    break;
                case "9":
                    outList.Add(Application.StartupPath + "\\sound\\items\\9000.wav");
                    break;
                default:
                    break;
            }
            string SecondSym = strNumber.Substring(1, 3);
            if (SecondSym.Substring(0, 1) == "0")
            {
                SecondSym = SecondSym.Substring(1, 2);
            }

            if (SecondSym.Substring(0, 1) == "0")
            {
                SecondSym = SecondSym.Substring(1, 1);
            }
            List<string> List1000;
            int value1000 = Convert.ToInt32(SecondSym);
            if (value1000 > 99)
            {
                List1000 = Before1000(value1000);
            }
            else
            {
                List1000 = Before100(value1000);
            }

            foreach (string itm in List1000)
            {
                outList.Add(itm);
            }

            return outList;

        }

        public static List<string> CombineSounds(int Number)
        {
            if (Number > 9999)
            {
                return null;
            }
            else
            {
                List<string> outList = new List<string>();
                //IEnumerable<string> retValue = new List<string>();

                //Индусский код (немного)
                string strNumber = Number.ToString();

                if (strNumber.Length <= 2)
                {
                    outList = Before100(Number);
                }
                else if (strNumber.Length == 3)
                {
                    outList = Before1000(Number);
                }
                else if (strNumber.Length == 4)
                {
                    outList = Before10000(Number);
                }

                //Рубли last digit
                string lastdigit = strNumber.Substring(strNumber.Length - 1, 1);

                if (lastdigit == "1")
                {
                    outList.Add(Application.StartupPath + "\\sound\\items\\rubl.wav");
                }
                else if (lastdigit == "2" || lastdigit == "3" || lastdigit == "4")
                {
                    outList.Add(Application.StartupPath + "\\sound\\items\\rublya.wav");
                }
                else
                {
                    outList.Add(Application.StartupPath + "\\sound\\items\\rublei.wav");
                }

                return outList;
            }

        }

        public static bool CreateSoundFile(string mph,int num) 
        {
            //if (num > 9999)
            //{
            //    return false;
            //}
            //else
            //{
                try
                {
                    List<string> outList = new List<string>();

                    string DestPath = Application.StartupPath + "\\sound\\dolg.wav";
                    if (File.Exists(DestPath))
                        File.Delete(DestPath);
            if (num < 10000)
            {
                //если нестандарт...
                if (num <= 2000 && num % 50 == 0) //whole pay...
                {
                    outList.Add(Application.StartupPath + "\\sound\\whole_pay\\" + num + ".wav");
                }
                else
                {
                    string FirstWord = Application.StartupPath + "\\sound\\zad.wav"; ;

                    outList.Add(FirstWord);

                    List<string> cypher = CombineSounds(num);

                    foreach (string word in cypher)
                    {
                        outList.Add(word);
                    }
                }
            }

            string LastWord = mph;
                    /*
                    if (!IsBM)
                    {
                        LastWord = Application.StartupPath + "\\sound\\need_money.wav";
                    }
                    else
                    {
                        LastWord = Application.StartupPath + "\\sound\\paypass.wav";
                    }
                    */
                    outList.Add(LastWord);

                //var t = Task.Run(() => Concatenate(DestPath, outList));
                //t.Wait();

                Concatenate(DestPath, outList);

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            //}
        }

        public static void Concatenate2(string outputFile, IEnumerable<string> sourceFiles)
        {


        }


        public static void Concatenate(string outputFile, IEnumerable<string> sourceFiles)
        {
            //byte[] buffer = new byte[1024];
            WaveFileWriter waveFileWriter = null;
            byte[] buffer = new byte[1024];
            int x = 0;

            try
            {
                foreach (string sourceFile in sourceFiles)
                {
                    x += 1;

                    using (WaveFileReader reader = new WaveFileReader(sourceFile))
                    {
                        if (waveFileWriter == null)
                        {
                            // first time in create new W2riter
                            waveFileWriter = new WaveFileWriter(outputFile, reader.WaveFormat);
                        }
                        else
                        {
                            if (!reader.WaveFormat.Equals(waveFileWriter.WaveFormat))
                            {
                                //throw new InvalidOperationException("Can't concatenate WAV Files that don't share the same format");
                                Logging.ToLog("Bad File " + x.ToString());
                            }
                        }
                        
                        int read;
                        while ((read = reader.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            waveFileWriter.Write(buffer, 0, read);
                            Application.DoEvents();
                        }
                    }
                }
            }
            finally
            {
                if (waveFileWriter != null)
                {
                    waveFileWriter.Dispose();
                }
            }

        }
    }
}
