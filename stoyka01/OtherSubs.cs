﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using IronBarCode;
using System.Windows.Forms;
using Net.Codecrete.QrCodeGenerator;

namespace stoyka01
{
    //перенести всякое сюда
    public class OtherSubs
    {
        public static void SendSP(int tc)
        {
            List<string> pList = new List<string>();
            pList.Add(GlobalClass.RemoteRackPost1);
            pList.Add(GlobalClass.RemoteRackPost2);
            pList.Add(GlobalClass.RemoteRackPost3);
            pList.Add(GlobalClass.RemoteRackPost4);
            pList.Add(GlobalClass.RemoteRackPost5);

            List<string> rList = new List<string>();
            rList.Add(GlobalClass.RemoteRackRaz1);
            rList.Add(GlobalClass.RemoteRackRaz2);
            rList.Add(GlobalClass.RemoteRackRaz3);
            rList.Add(GlobalClass.RemoteRackRaz4);
            rList.Add(GlobalClass.RemoteRackRaz5);

            //SendMultiSinglePassage
            if (GlobalClass.RemoteRackPost && tc!=0)
            {
                Recognition.OtherHTTPRequests.SendMultiSinglePassage(pList);
            }

            if (GlobalClass.RemoteRackRaz && tc == 0)
            {
                Recognition.OtherHTTPRequests.SendMultiSinglePassage(rList);
            }
        }

        //Для ТКВП
        public static Guid? GetTSGuid(int ts)
        {
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                try
                {
                    DataTable Disc = new DataTable();
                    Disc = UtilsBase.FillTable("select * from TariffScheduleModel where IdFC = '" + ts.ToString() + "'", connect, null, null); // where ISNULL(IsDeleted,0)!=1                                       

                    if (Disc != null && Disc.Rows.Count != 0)
                    {
                        foreach (DataRow dad in Disc.Rows)
                        {
                            return dad.GetGuid("Id");
                        }
                    }
                    else
                    {
                        //Logging.ToLog("Не задано имя TS");
                        return null;
                    }

                }
                catch (Exception e)
                {
                    Logging.ToLog("Exception Tariffshedule " + e);
                    return null;
                }
            }
            return null;
        }

        /*
         SupportRPS, [29.12.2021 14:44]
Защитный интервал счётчика - эначение числового типа, задаётся в поле ConditionECProtectInterval в таблице TariffPlanTariffSheduleModel (тип единицы изменрения времени задан в поле ConditionECProtectIntervalTimeTypeId в той же таблице). Это время ограничивает минимальную продолжительность парковочной сессии, при которой идёт увеличение счётчика ТКВП на выезде. То есть, если автомобиль простоял меньше заданного времени(Now - ParkingEnterTime), то ТКВП не увеличиваем.

SupportRPS, [29.12.2021 14:45]
Вот так надо сделать.

Егор, [29.12.2021 14:45]
не увеличиваем на выезде по инкременту?

SupportRPS, [29.12.2021 14:46]
Да. Если Now - ParkingEnterTime <= ConditionECProtectInterval то не увеличиваем

SupportRPS, [29.12.2021 14:47]
Тут только с единицами измерения времени можно запутаться, ConditionECProtectInterval может иметь разные измерения(в минутах, часах и тд).
             
             */

        public static bool IsIncTKVP()
        {
            DateTime p_enter = GlobalClass.datetime0 + TimeSpan.FromSeconds(GlobalClass._DateCard.ParkingEnterTime);
            DateTime dt_now = DateTime.Now;



            Logging.ToLog(p_enter.ToString());
            int ts_id = GlobalClass._DateCard.TSidFC;
            //Получаем гуид ТС
            Guid? ts = OtherSubs.GetTSGuid(ts_id);
            Logging.ToLog(ts.ToString());
            if (ts == null)
            {
                Logging.ToLog("ТС не найден для данного Id");
                return false;
            }

            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                try
                {
                    DataTable Disc = new DataTable();
                    Disc = UtilsBase.FillTable("select * from TariffPlanTariffScheduleModel where TariffScheduleId = '" + ts.ToString() + "' AND ISNULL(_IsDeleted,0)!=1 AND TypeId ='2'", connect, null, null); // where ISNULL(IsDeleted,0)!=1                                       

                    if (Disc != null && Disc.Rows.Count != 0)
                    {
                        foreach (DataRow dad in Disc.Rows)
                        {
                            int? interval = dad.GetInt("ConditionECProtectInterval");
                            int? interval_type = dad.GetInt("ConditionECProtectIntervalTimeTypId");
                            if (interval == null || interval_type == null)
                            {
                                Logging.ToLog("Защитный интервал для данной ТС Null. Увеличиваем счетчик");
                                return true;
                            }
                            else if (interval_type == 0)
                            {
                                Logging.ToLog("Защитный интервал для данной ТС 0. Увеличиваем счетчик");
                                return true;
                            }
                            else
                            {
                                double diff = 0;
                                switch (interval_type)
                                {
                                    //case 0:
                                    //    diff = (dt_now - p_enter).TotalSeconds;
                                    //    break;
                                    case 1:
                                        diff = (dt_now - p_enter).TotalMinutes;
                                        break;
                                    case 2:
                                        diff = (dt_now - p_enter).TotalHours;
                                        break;
                                    case 3:
                                        diff = (dt_now - p_enter).TotalDays;
                                        break;
                                    case 4:
                                        diff = ((dt_now - p_enter).TotalDays) / 7;
                                        break;
                                    case 5:
                                        diff = ((dt_now - p_enter).TotalDays) / 30;
                                        break;
                                    default: break;
                                }

                                //сравнение защитнега!
                                if (diff <= interval)
                                {
                                    Logging.ToLog("Попали в защитный интервал. Не увеличиваем счетчик");
                                    return false;
                                }
                                else
                                {
                                    Logging.ToLog("Не попали в защитный интервал. Увеличиваем счетчик");
                                    return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        Logging.ToLog("Не найдена ТС в таблице TariffPlanTariffScheduleModel");
                        return false;
                    }

                }
                catch (Exception e)
                {
                    Logging.ToLog("Exception TariffPlanTariffScheduleModel " + e);
                    return false;
                }
            }

            return false;
        }


        public static void AddToQrCheck(DateTime dt, int summ, int number, string qr, string bn)
        {
            List<object> QR = new List<object>();

            QR.Add("Id");
            QR.Add(Guid.NewGuid());

            QR.Add("DeviceId");
            QR.Add(GlobalClass.DeviceIdG);

            QR.Add("PaidTime");
            QR.Add(dt);

            QR.Add("BillSum");
            QR.Add(summ);

            QR.Add("BillNumber");
            QR.Add(bn);

            QR.Add("BillQR");
            QR.Add(qr);

            QR.Add("_IsDeleted");
            QR.Add(false);

            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                UtilsBase.InsertCommand("CheckQR", connect, null, QR.ToArray());
            }
        }

        public static void CreateEconomQR(string qr)
        {
            //QRCodeWriter.CreateQrCode(qr, 42, QRCodeWriter.QrErrorCorrectionLevel.Low).SaveAsPng(Application.StartupPath + "\\mini\\100.png");
            var Qr = QrCode.EncodeText(qr, QrCode.Ecc.Medium);
            Qr.SaveAsPng(Application.StartupPath + "\\mini\\100.png", 1, 3);
        }
    }
}
