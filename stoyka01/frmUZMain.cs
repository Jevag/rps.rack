﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stoyka01
{
    public partial class frmUZMain : Form
    {
        int oldEtap;
        GlobalClass.UZData uzData;

        public frmUZMain()
        {
            InitializeComponent();
        }

        private void tmrDataUpdate_Tick(object sender, EventArgs e)
        {
            if (GlobalClass.etap == 0 || GlobalClass.etap == 1 || GlobalClass.etap ==100 || GlobalClass.etap==200 || GlobalClass.etap==5)
            {
                DataClear();
                cmdApply.Enabled = false;
            }
            else
            {
                if (GlobalClass.UZCardInserted) //fix
                {
                    if (GlobalClass.Itog < 0)
                    {
                        DataUpdate();
                        cmdApply.Enabled = true;
                    }
                    else
                    {
                        DataUpdateShort();
                    }
                }
                ScrUpdate();
            }

            //Обновляем БД
            if (Tashkent.NeedUpdate)
            {
                Tashkent.NeedUpdate = false;
                Tashkent.SelectDiscounts(GlobalClass.UZ_Id);
            }
        }

        void DataClear()
        {
            uzData.Msg = "";
            uzData.UZCardId = "0";
            uzData.UZEntryTime = GlobalClass.datetime0.ToString("F");
            //uzData.UZSumOnCard = "0";
            uzData.UZZadolg = "0";
            uzData.UZTP = "0";
            uzData.UZTS = "0";
            uzData.MsgPicture = "0";

            //чистим!
            lblMsg.Text = GlobalClass.UZShowData.Msg;
            lblUZCardID.Text = uzData.UZCardId;
            lblUZEntryTime.Text = uzData.UZEntryTime;
            //lblUZSumOnCard.Text = uzData.UZSumOnCard;
            if (GlobalClass.UZValue == 0)
            {
                lblUZZadolg.Text = uzData.UZZadolg + " руб.";
            }
            else
            {
                lblUZZadolg.Text = uzData.UZZadolg + " UZS";
            }
            
            lblUZTP.Text = ""; //Tashkent.GetTPname(Convert.ToInt32(uzData.UZTP));
            lblUZTS.Text = ""; //Tashkent.GetTSname(Convert.ToInt32(uzData.UZTS));
            if (uzData.MsgPicture != "0")
            {
                picMsgPicture.Visible = true;
                picMsgPicture.Image = Image.FromFile(Application.StartupPath + "\\" + uzData.MsgPicture + ".png");
            }
            else
            {
                picMsgPicture.Visible = false;
            }
            cboUZDiscount.Items.Clear();
            lblDiscountDescription.Text = "";
            
        }

        //при переходе со 2 на 3 или со 2 на 10
        void DataUpdate()
        {            
            if (!uzData.Equals(GlobalClass.UZShowData))
            {
                //Logging.ToLog(GlobalClass.UZShowData.Msg);
                uzData = GlobalClass.UZShowData;
                lblMsg.Text = uzData.Msg;
                lblUZCardID.Text = uzData.UZCardId;
                lblUZEntryTime.Text = uzData.UZEntryTime;
                //lblUZSumOnCard.Text = uzData.UZSumOnCard;

                if (GlobalClass.UZValue == 0)
                {
                    lblUZZadolg.Text = uzData.UZZadolg + " руб.";
                }
                else
                {
                    lblUZZadolg.Text = uzData.UZZadolg + " UZS";
                }


                lblUZTP.Text = Tashkent.GetTPname(Convert.ToInt32(uzData.UZTP)); //uzData.UZTP;
                lblUZTS.Text = Tashkent.GetTSname(Convert.ToInt32(uzData.UZTS));
                if (uzData.MsgPicture == "0" || uzData.MsgPicture =="" || uzData.MsgPicture ==null)
                {
                    picMsgPicture.Visible = false;                    
                }
                else
                {
                    picMsgPicture.Visible = true;
                    picMsgPicture.Image = Image.FromFile(Application.StartupPath + "\\" + uzData.MsgPicture + ".png");
                }
                AddDiscounts();
            } 
        }

        void DataUpdateShort()
        {
            if (!uzData.Equals(GlobalClass.UZShowData))
            {
                //Logging.ToLog(GlobalClass.UZShowData.Msg);
                uzData = GlobalClass.UZShowData;
                lblMsg.Text = uzData.Msg;
                lblUZCardID.Text = uzData.UZCardId;
                lblUZEntryTime.Text = uzData.UZEntryTime;
                //lblUZSumOnCard.Text = uzData.UZSumOnCard;
                //lblUZZadolg.Text = uzData.UZZadolg + " UZS";
                lblUZTP.Text = Tashkent.GetTPname(Convert.ToInt32(uzData.UZTP)); //uzData.UZTP;
                lblUZTS.Text = Tashkent.GetTSname(Convert.ToInt32(uzData.UZTS));
                if (uzData.MsgPicture != "0")
                {
                    picMsgPicture.Visible = true;
                    picMsgPicture.Image = Image.FromFile(Application.StartupPath + "\\" + uzData.MsgPicture + ".png");
                }
                else
                {
                    picMsgPicture.Visible = false;
                }
                //AddDiscounts();
            }
        }

        void ScrUpdate()
        {
            if (GlobalClass.UZShowData.MsgPicture != "0" && GlobalClass.UZShowData.MsgPicture !=null)
            {
                picMsgPicture.Visible = true;
                picMsgPicture.Image = Image.FromFile(Application.StartupPath + "\\" + GlobalClass.UZShowData.MsgPicture + ".png");
            }
            else
            {
                picMsgPicture.Visible = false;
            }
            lblMsg.Text = GlobalClass.UZShowData.Msg;
        }

        //Тоже не выводить скидки, пока не сравнили тарифы...
        void AddDiscounts()
        {
            cboUZDiscount.Items.Clear();
            foreach (Tashkent.Discounts disc in Tashkent.StartDiscounts)
            {
                if (disc.CurrentIdTP == null || disc.CurrentIdTP.ToString() == uzData.UZTP)
                {
                    cboUZDiscount.Items.Add(disc.Name);
                }
            }
            if (cboUZDiscount.Items.Count > 0)
            {
                cboUZDiscount.SelectedIndex = 0;
            }
        }

        private void frmUZMain_Load(object sender, EventArgs e)
        {
            //AddStartDiscounts();
            uzData = new GlobalClass.UZData();
            cmdApply.Enabled = false;

            lblCardId.Text= GlobalClass.CurrentLangDictionary[10918];
            lblDT.Text= GlobalClass.CurrentLangDictionary[10919];
            lblTP.Text = GlobalClass.CurrentLangDictionary[10920];
            lblTS.Text = GlobalClass.CurrentLangDictionary[10921];
            lblDolg.Text = GlobalClass.CurrentLangDictionary[10922];
            lblDisc.Text = GlobalClass.CurrentLangDictionary[10923];
            cmdApply.Text= GlobalClass.CurrentLangDictionary[10924];
            this.Text= GlobalClass.CurrentLangDictionary[10926];
        }

        private void cboUZDiscount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboUZDiscount.Items.Count > 0)
            {
                foreach (Tashkent.Discounts disc in Tashkent.StartDiscounts)
                {
                    if (disc.Name == cboUZDiscount.Text)
                    {
                        lblDiscountDescription.Text = disc.Description; //+ " (" + disc.Discount + ")UZS";
                        Tashkent.CurrentDiscount = disc;
                        //Сюда Amount
                        if (disc.AnySum == true)
                        {
                            lblManualAmount.Visible = true;
                            txtManualAmount.Visible = true;
                        }
                        else
                        {
                            txtManualAmount.Text = "";
                            lblManualAmount.Visible = false;
                            txtManualAmount.Visible = false;
                        }
                    }
                }
            }
        }

        private void cmdApply_Click(object sender, EventArgs e)
        {
            cmdApply.Enabled = false;
            if (txtManualAmount.Visible)
            {
                if ((txtManualAmount.Text == "" || txtManualAmount.Text == null))
                {
                    Logging.ToLog("Не введена сумма скидки");
                    MessageBox.Show("Введите сумму скидки");
                    Tashkent.ManualAmount = 0;
                    return;
                }
                else
                {
                    Tashkent.ManualAmount = Convert.ToInt32(txtManualAmount.Text);
                }
            }
            Tashkent.UpdateDiscount();

            if (GlobalClass.UZLockPaymentScreen) GlobalClass.UZLockPaymentScreen = false;
            //MessageBox.Show("chegirma muvaffaqiyatli qo'llanildi");
            Logging.ToLog("Скидка успешно применена");
            txtManualAmount.Text = "";
            //frmUZMsg msg = new frmUZMsg(GlobalClass.CurrentLangDictionary[10925], this.Location.X + this.Size.Width / 2, this.Location.Y + this.Size.Height / 2);
            //msg.Show();
            
        }

        private void frmUZMain_Resize(object sender, EventArgs e)
        {
            //GlobalClass.CurrentLangDictionary[10923];
            try
            {
                cboUZDiscount.Width = this.Width - 445;
            }
            catch (Exception ex) { }
        }

        private void frmUZMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalClass.UZOperatorShow = 1;
            frmUZLogin lgn = new frmUZLogin();
            lgn.Left = GlobalClass.UZLoginX;
            lgn.Top = GlobalClass.UZLoginY;
            lgn.Show();
        }

        private void txtManualAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
    }
}
