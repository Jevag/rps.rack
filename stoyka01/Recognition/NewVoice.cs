﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stoyka01.Recognition
{
    

    public class NewVoice
    {
        public SoundPlayer calling;
        public SoundPlayer callto;
        public SoundPlayer endcall;
        public SoundPlayer noanswer;
        public SoundPlayer bad;

        public bool IsPushed = false;

        public bool RequestSended = false;
        public bool RequestSended2 = false;

        public int NewCallTimer = 0;

        public int DrebezgTimer = 0;

        public int PreCallTimer = 0;


        public NewVoice()
        {
            calling = new SoundPlayer(Application.StartupPath + "\\sounds\\calling.wav");
            callto = new SoundPlayer(Application.StartupPath + "\\sounds\\callto.wav");
            endcall = new SoundPlayer(Application.StartupPath + "\\sounds\\endcall.wav");
            noanswer = new SoundPlayer(Application.StartupPath + "\\sounds\\noanswer.wav");
            bad = new SoundPlayer(Application.StartupPath + "\\sounds\\bad.wav");
        }

        public async Task<bool> PushFirstTime(string srvUrl, string rackid)
        {
            try
            {
                //LastAnswer = "empty_answer";
                Logging.ToLog("Запрос к серверу");
                Logging.ToLog(srvUrl + "/action/DeviceCall?T=DeviceRackModel&A=I&DeviceId=" + rackid);
                string data = "";
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(srvUrl + "/action/DeviceCall?T=DeviceRackModel&A=I&DeviceId=" + rackid);
                HttpContent responseContent = response.Content;
                using (StreamReader reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
                {
                    data = await reader.ReadToEndAsync();
                    Debug.Print(data);
                    if (data == "action")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //https://office.r-p-s.ru/ws/StopCall?DeviceId=93A965BB-4A0C-4C5E-9842-57ECE913AD3D

        public async Task<bool> PushSecondTime(string srvUrl, string rackid)
        {
            try
            {
                Logging.ToLog("Запрос к серверу 2");
                Logging.ToLog(srvUrl + "/action/StopCall?DeviceId=" + rackid);
                //LastAnswer = "empty_answer";
                string data = "";
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(srvUrl + "/action/StopCall?DeviceId=" + rackid);
                HttpContent responseContent = response.Content;
                using (StreamReader reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
                {
                    data = await reader.ReadToEndAsync();
                    Debug.Print(data);
                    return true;

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
