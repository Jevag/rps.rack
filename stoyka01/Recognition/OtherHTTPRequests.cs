﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace stoyka01.Recognition
{
    public class OtherHTTPRequests
    {
        public static bool PingResult = true; //было true

        //https://office.r-p-s.ru/api2/recognition/DeviceRecognition/new?guid=111afa14-af16-497d-a38c-cbb0109af4dc&type=tagrecogntionstart&timestamp=16122021T113338&tagid=12345&platenumber=x123xx777
        ///api2/recognition/deviceRecognition/new?guid=11111&type=11111&timestamp=1111
        public static async Task<bool> OperatorUploadImageRFID(string type, string strDate, string RGuid, string TagId, string CurrUrl, byte[] image, string plate)
        {
            try
            {
                Logging.ToLog("Попытка отправки запроса  с картинкой" + CurrUrl + "/api2/recognition/DeviceRecognition/new?guid=" + RGuid + "&type=" + type + "&timestamp=" + strDate + "&tagid=" + TagId + "&platenumber=" + plate);

                
                using (var client = new HttpClient())
                {
                    
                    
                    using (var content =
                        new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                    {
                        
                        content.Add(new StreamContent(new MemoryStream(image)), "strdate", "upload.jpg"); //Дату + название

                        /*
                        var dataContent = new FormUrlEncodedContent(new[]
                        {
                        new KeyValuePair<string, string>("TagId", TagId),
                        });
                        content.Add(dataContent);
                        */

                        //Logging.ToLog();

                        using (
                           var message =
                               await client.PostAsync(CurrUrl + "/api2/recognition/DeviceRecognition/new?guid=" + RGuid + "&type=" + type + "&timestamp=" + strDate + "&tagid=" + TagId + "&platenumber=" + plate, content))
                        {
                            
                            var input = await message.Content.ReadAsStringAsync();
                            //Logging.ToLog("zpk");
                            return true;
                        }
                    }
                    
                }
                


            }
            catch (Exception ex)
            {
                Logging.ToLog("Попытка отправки снапшота по RFID/HID " + ex.Message);
                return false;
                
            }
        }

        public static async Task<bool> OperatorUploadNotifyRFID(string type, string RGuid, string CurrUrl, string strDate)
        {
            try
            {

                //Logging.ToLog("Попытка отправки запроса  с картинкой" + CurrUrl + "/api2/recognition/DeviceRecognition/new?guid=" + RGuid + "&type=" + type + "&timestamp=" + strDate);

                Logging.ToLog("Уведомление: " + CurrUrl + "/api2/recognition/DeviceRecognition/new?guid=" + RGuid + "&type=" + type + "&timestamp=" + strDate);

                using (var client = new HttpClient())
                {
                    await client.GetAsync(CurrUrl + "/api2/recognition/DeviceRecognition/new?guid=" + RGuid + "&type=" + type + "&timestamp=" + strDate);
                }
                return true;

            }
            catch (Exception ex)
            {
                Logging.ToLog("Попытка отправки уведомления по RFID/HID " + ex.Message);
                return false;

            }
        }


        /*
        public static void SendRFIDHIDNotification(string srv, string tag, string rack_id)
        {
            //Logging.ToLog("http://" + GlobalClass.RemoteReverseIP + ":2222/RemoteReversePlus");
            //https://office.r-p-s.ru/api2/loopA/TagNumber=1111&DeviceID=1234
            try
            {
                HttpClient client = new HttpClient();
                Guid id = Guid.NewGuid();
                client.GetAsync("http://" + srv + "/api2/loopA/TagNumber=" + tag.ToString() + "&DeviceID=" + rack_id);
                Logging.ToLog("send_notify");

            }
            catch (Exception ex)
            {
                Logging.ToLog(ex.Message);
            }
        }
        */

        //Отправка сообщений между уровнями стойки для HTTP реверса!
        public static void SendRemoteReversePlus()
        {
            //Logging.ToLog("http://" + GlobalClass.RemoteReverseIP + ":2222/RemoteReversePlus");

            try
            {
                HttpClient client = new HttpClient();
                Guid id = Guid.NewGuid();
                client.GetAsync("http://" + GlobalClass.RemoteReverseIP + ":2222/RemoteReversePlus/g=" + id.ToString());

            }
            catch (Exception ex)
            {
                Logging.ToLog(ex.Message);
            }
        }

        public static void SendRemoteReverseConfirmPlus()
        {
            try
            {
                HttpClient client = new HttpClient();
                Guid id = Guid.NewGuid();
                client.GetAsync("http://" + GlobalClass.RemoteReverseIP + ":2222/RemoteReverseConfirmPlus/g=" + id.ToString());

            }
            catch (Exception ex)
            {
                Logging.ToLog(ex.Message);
            }
        }

        public static void SendRemoteReverseMinus()
        {
            //Logging.ToLog("http://" + GlobalClass.RemoteReverseIP + ":2222/RemoteReverseMinus/g=" + new Guid().ToString());

            try
            {
                HttpClient client = new HttpClient();
                Guid id = Guid.NewGuid();
                client.GetAsync("http://" + GlobalClass.RemoteReverseIP + ":2222/RemoteReverseMinus/g=" + id.ToString());

            }
            catch (Exception ex)
            {
                Logging.ToLog(ex.Message);
            }
        }

        public static void SendRemoteReverseConfirmMinus()
        {
            try
            {
                HttpClient client = new HttpClient();
                Guid id = Guid.NewGuid();
                client.GetAsync("http://" + GlobalClass.RemoteReverseIP + ":2222/RemoteReverseConfirmMinus/g=" + id.ToString());

            }
            catch (Exception ex)
            {
                Logging.ToLog(ex.Message);
            }
        }

        public static void PingRemote(string host)
        {
            if (host.IndexOf("https://") > -1)
            {
                host = host.Replace("https://", "");
            }

            //Logging.ToLog(host);

            Ping ping= new Ping();
            ping.PingCompleted += Ping_PingCompleted;

            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);

            // Wait 12 seconds for a reply.  
            int timeout = 2000;

            // Set options for transmission:  
            // The data can go through 64 gateways or routers  
            // before it is destroyed, and the data packet  
            // cannot be fragmented.  
            PingOptions options = new PingOptions(64, true);

            //Console.WriteLine("Time to live: {0}", options.Ttl);
            //Console.WriteLine("Don't fragment: {0}", options.DontFragment);

            // Send the ping asynchronously.  
            // Use the waiter as the user token.  
            // When the callback completes, it can wake up this thread.  
            ping.SendAsync(host, timeout, buffer, options);

        }

        private static void Ping_PingCompleted(object sender, PingCompletedEventArgs e)
        {

            //PingResult = false;
            //return;

            //throw new NotImplementedException();
            if (e.Cancelled)
            {
                //Console.WriteLine("Ping canceled.");

                // Let the main thread resume.
                // UserToken is the AutoResetEvent object that the main thread
                // is waiting for.  
                //((AutoResetEvent)e.UserState).Set();
                PingResult = false;
            }

            // If an error occurred, display the exception to the user.  
            if (e.Error != null)
            {
                //Console.WriteLine("Ping failed:");
                //Console.WriteLine(e.Error.ToString());

                // Let the main thread resume.
                //((AutoResetEvent)e.UserState).Set();
                PingResult = false;
            }

            PingReply reply = e.Reply;
            if (reply != null)
            {
                PingResult = true;
            }
            else
            {
                PingResult = false;
            }

            //DisplayReply(reply);
        }
        
        //https://expo.r-p-s.ru/api2/Alarm/Fire?DeviceId=DF6904ED-8691-4DFC-AC17-15F06270B5E8&Status=on
        public static void SendFireAlarm(string uri, string deviceid)
        {
            try
            {
                HttpClient client = new HttpClient();
                string link = uri + "/api2/Alarm/Fire?DeviceId=" + deviceid  + "&Status=on";
                Logging.ToLog("Send Fire Alarm: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        //проверка на валидный IP
        public static void SendMultiSinglePassage(List<string> ip_adrs)
        {
            try
            {
                HttpClient client = new HttpClient();
                foreach (string ip_adr in ip_adrs)
                {
                    IPAddress address;
                    bool x = IPAddress.TryParse(ip_adr, out address);
                    if (x)
                    {
                        string link = "http://" + ip_adr + ":2222/SinglePassage";
                        Logging.ToLog("Send SinglePassage to: " + ip_adr);
                        client.GetAsync(link);
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.ToLog("Error SendMultiSinglePassage: " + ex.Message);
            }
        }


    }
}
