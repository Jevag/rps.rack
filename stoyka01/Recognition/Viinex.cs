﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace stoyka01
{
    public class Viinex
    {       
        private bool enabled = true;

        public byte[] OperatorImage { get; set; }

        //Запущена ли служба
        public bool IsViinexRunning
        {
            get
            {
                ServiceController ViinexService = new ServiceController("Viinex");

                if (ViinexService.Status != ServiceControllerStatus.Running)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        List<ViinexResponse> plate;

        List<ViinexTSResponse> ts;

        public double accuracy;
        public string strplate;
        public DateTime timestamp;

        public int accuracy2;
        public string strplate2;
        public DateTime timestamp2;

        public bool IsError;
        public string LastErrorMsg;
        public string LastAnswer;

        public Viinex()
        {
            plate = new List<ViinexResponse>();
            ts = new List<ViinexTSResponse>();
        }

        public static void ViinexUpdateSettings()
        {
            string wdata = "";
            try
            {
                //Создаем
                ViinexSettings vset = new ViinexSettings();
                vset.license = GlobalClass.ViinexLicenseKey;
                vset.objects = new List<Object>();

                Object v1 = new Object();
                if (GlobalClass.CameraType == "Onvif")
                {
                    v1.type = "onvif";
                }
                else
                {
                    v1.type = "rtsp";
                }

                v1.name = "cam1";
                v1.url = GlobalClass.CameraURI;                
                v1.auth = new List<string>();
                v1.auth.Add(GlobalClass.CameraLogin);
                v1.auth.Add(GlobalClass.CameraPassword);

                if (v1.type == "onvif")
                {
                    v1.acquire = new List<string>();

                    if (GlobalClass.IsAcquireVideo)
                    {
                        v1.acquire.Add("video");
                    }

                    if (GlobalClass.IsAcquireEvents)
                    {
                        v1.acquire.Add("events");
                    }
                }
                v1.transport = new List<string>();
                if (GlobalClass.TransportTCP)
                {
                    v1.transport.Add("tcp");
                }
                else if (GlobalClass.TransportUDP)
                {
                    v1.transport.Add("udp");
                }

                vset.objects.Add(v1);

                if (GlobalClass.Camera2Enabled)
                {
                    Object v1_2 = new Object();
                    if (GlobalClass.CameraType2 == "Onvif")
                    {
                        v1_2.type = "onvif";
                    }
                    else
                    {
                        v1_2.type = "rtsp";
                    }

                    v1_2.name = "cam2";

                    v1_2.url = GlobalClass.CameraURI2;
                    //v1_2.port = Convert.ToInt32(GlobalClass.CameraPort2);
                    v1_2.auth = new List<string>();
                    v1_2.auth.Add(GlobalClass.CameraLogin2);
                    v1_2.auth.Add(GlobalClass.CameraPassword2);

                    if (v1_2.type == "onvif")
                    {
                        v1_2.acquire = new List<string>();

                        if (GlobalClass.IsAcquireVideo2)
                        {
                            v1_2.acquire.Add("video");
                        }

                        if (GlobalClass.IsAcquireEvents2)
                        {
                            v1_2.acquire.Add("events");
                        }
                    }
                    if (GlobalClass.TransportTCP2)
                    {
                        v1_2.transport.Add("tcp");
                    }
                    else if (GlobalClass.TransportUDP2)
                    {
                        v1_2.transport.Add("udp");
                    }
                    vset.objects.Add(v1_2);
                }

                Object v2 = new Object();
                v2.type = "webserver";
                v2.name = "web0";
                v2.staticpath = "share/web";
                v2.port = 8880;
                vset.objects.Add(v2);

                Object v3 = new Object();
                v3.type = "alpr";
                v3.name = "alpr0";
                v3.mode = "video";
                v3.engine = "vnxoem";
                v3.datapath = "share/vnxlpr";
                v3.templates = new List<string>();
                v3.templates.Add("lpRUSSIA_ONE_ROW");
                v3.workers = 4;
                if (GlobalClass.ViinexTransliter == 0)
                {
                    v3.transliterate = "latin";
                }
                else
                {
                    v3.transliterate = "cyrillic";
                }
                vset.objects.Add(v3);

                Object v4 = new Object();
                v4.type = "alprvideo";
                v4.name = "alprcam1";

                if (GlobalClass.CameraSkip != "non_idr")
                {
                    double skp = Convert.ToDouble(GlobalClass.CameraSkip);

                    v4.skip = skp;
                }
                else
                {
                    v4.skip = "non_idr";
                }

                string uiSep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

                double pre;
                double pst;

                if (uiSep == ",")
                {
                    pre = Convert.ToDouble(GlobalClass.CameraPreProcess.Replace(".", ","));
                    pst = Convert.ToDouble(GlobalClass.CameraPostProcess.Replace(".", ","));

                    v4.preprocess = pre / 1000; //Convert.ToDouble(GlobalClass.CameraPreProcess.Replace(".",","));
                    v4.postprocess = pst / 1000; //Convert.ToDouble(GlobalClass.CameraPostProcess.Replace(".",","));
                }
                else
                {
                    pre = Convert.ToDouble(GlobalClass.CameraPreProcess);
                    pst = Convert.ToDouble(GlobalClass.CameraPostProcess);

                    v4.preprocess = pre / 1000;
                    v4.postprocess = pst / 1000;
                }

                v4.roi = new List<double>();
                v4.roi.Add((double)GlobalClass.RoiLeft);
                v4.roi.Add((double)GlobalClass.RoiTop);
                v4.roi.Add((double)GlobalClass.RoiRight);
                v4.roi.Add((double)GlobalClass.RoiBottom);


                v4.time_budget = GlobalClass.TimeBudget;

                vset.objects.Add(v4);

               

                vset.links = new List<List<List<string>>>();
                List<List<string>> Link1 = new List<List<string>>();
                List<string> Link1_1;
                if (GlobalClass.Camera2Enabled)
                {
                    Link1_1 = new List<string>(new string[] { "cam1", "cam2" });
                }
                else
                {
                    Link1_1 = new List<string>(new string[] { "cam1"});
                }

                Link1.Add(Link1_1);
                List<string> Link1_2 = new List<string>(new string[] { "web0" });
                Link1.Add(Link1_2);
                vset.links.Add(Link1);

                List<List<string>> Link2 = new List<List<string>>();
                List<string> Link2_1;
                if (GlobalClass.Camera2Enabled)
                {
                    Link2_1 = new List<string>(new string[] { "cam1", "cam2" });
                }
                else
                {
                    Link2_1 = new List<string>(new string[] { "cam1"});
                }

                Link2.Add(Link2_1);


                List<string> Link2_2 = new List<string>(new string[] { "alprcam1" });
                Link2.Add(Link2_2);
                vset.links.Add(Link2);


                List<List<string>> Link4 = new List<List<string>>();
                List<string> Link4_1 = new List<string>(new string[] { "web0", "alpr0" });
                Link4.Add(Link4_1);
                List<string> Link4_2 = new List<string>(new string[] { "alprcam1" });
                Link4.Add(Link4_2);
                vset.links.Add(Link4);

                wdata = JsonConvert.SerializeObject(vset,
                                Formatting.None,
                                new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore
                                });

            }
            catch (Exception ex)
            {
                MessageBox.Show(GlobalClass.CurrentLangDictionary[10770] + Environment.NewLine + ex.Message);
                return;
            }

            //Пишем
            File.WriteAllText(Application.StartupPath + "\\vnxtmp.json", wdata);
            //StopViinex();
            Thread.Sleep(1000);
            try
            {
                File.Copy(Application.StartupPath + "\\vnxtmp.json", GlobalClass.ViinexSettingsPath,true);
            }
            catch (Exception ex)
            {

                MessageBox.Show(GlobalClass.CurrentLangDictionary[10771] + Environment.NewLine + ex.Message);
                //StartViinexRED();
                return;
            }
            //Thread.Sleep(5000);
            //StartViinexRED();
            if (RestartViinexNew(15000))
            {
                MessageBox.Show(GlobalClass.CurrentLangDictionary[10772]);
            }
            else
            {
                MessageBox.Show(GlobalClass.CurrentLangDictionary[10773]);
            }
        }

        public void StartViinexRED()
        {
            try
            {
                using (ServiceController ViinexService = new ServiceController("Viinex"))
                {

                        ViinexService.Start();
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void StartViinex()
        {
            try
            {
                using (ServiceController ViinexService = new ServiceController("Viinex"))
                {
                    //if (ViinexService.Status != ServiceControllerStatus.Running)
                    //{
                        ViinexService.Start();
                    //}
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void StopViinex()
        {
            try
            {
                using (ServiceController ViinexService = new ServiceController("Viinex"))
                {
                    //if (ViinexService.Status == ServiceControllerStatus.Running)
                    //{
                        ViinexService.Stop();
                    //}
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static bool RestartViinexNew(int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController("Viinex");
            try
            {
                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                // count the rest of the timeout
                int millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Init()
        {
            //StartViinex();
        }

        public async Task<bool> RecognizePlate()
        {
            try
            {
                LastAnswer = "empty_answer";
                string data = "";
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(GlobalClass.GetURI + "?unique=" + new Guid().ToString());
                HttpContent responseContent = response.Content;
                using (StreamReader reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
                {
                    data = await reader.ReadToEndAsync();
                    //Debug.Print(data);
                    LastAnswer = data;
                    IsError = false;
                    LastErrorMsg = "";

                    if (data != "[]") //???
                    {
                        if (data.IndexOf("timestamp") == 3) //TimeStamp//Неправильно! Нужно смотреть как-то по сериализации...
                        {
                            //[{"timestamp":"2017-12-05T14:11:23.417Z"}]
                            //Получаем Timestamp
                            ts= JsonConvert.DeserializeObject<List<ViinexTSResponse>>(data);
                            timestamp = ts[0].timestamp;
                            return false;
                        }
                        //[{"confidence":97,"timestamp":"2017-12-05T13:59:59.617Z","plate_rect":{"right":0.40208334,"top":0.5046296,"bottom":0.60925925,"left":0.1796875},"plate_text":"К777МА77"}]
                        else //Нормальный ответ с номером
                        {
                            plate = JsonConvert.DeserializeObject<List<ViinexResponse>>(data);
                            if (plate.Count == 1) //Умолчание
                            {
                                strplate = plate[0].plate_text;
                                accuracy = plate[0].confidence;
                                timestamp = plate[0].timestamp;
                            }
                            else //Считаем первый номер с кадра
                            {
                                double platelen = 0;

                                for (int i = 0; i < plate.Count; i++)
                                {
                                 double maxplatelen = Math.Abs(plate[i].plate_rect.left - plate[i].plate_rect.right);
                                    if (maxplatelen > platelen)
                                    {
                                        platelen = maxplatelen;
                                        strplate = plate[i].plate_text;
                                        accuracy = plate[i].confidence;
                                        timestamp = plate[i].timestamp;
                                    }
                                }
                            }

                            return true;
                        }
                    }
                    else
                    {
                        timestamp = new DateTime(2001, 1, 1, 3, 33, 33);
                        return false; //ответ есть, но проскочила пустышка. Ошибки нет
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                IsError = true;
                LastErrorMsg = ex.Message + " " + ex.InnerException;
                return false;
            }
        }


        //Дополнения
        /*
        public void SendStartRecognition()
        {
            try
            {
                HttpClient client = new HttpClient();

                client.GetAsync("http://" + GlobalClass.TwoLevelSendIP + ":2222/StartRecognition?q=1");

            }
            catch (Exception ex)
            {
            }
        }
        */

        //Отправка сообщений между уровнями стойки для винекса
        public void SendPlateNumber(string PlateNumber)
        {
            try
            {
                HttpClient client = new HttpClient();

                client.GetAsync("http://" + GlobalClass.TwoLevelSendIP + ":2222/GetPlateNumber?LPR=" + PlateNumber);

            }
            catch (Exception ex)
            {
            }
        }

        public async Task<bool> OperatorDownloadImage(bool Listen)
        {
            string snap_path = "";

            if (!Listen)
            {
                if (GlobalClass.Snap1)
                {
                    snap_path = "http://localhost:8880/v1/svc/cam1/snapshot?unique=";
                }
                else
                {
                    snap_path = "http://localhost:8880/v1/svc/cam2/snapshot?unique=";
                }
            }
            else
            {
                if (GlobalClass.Snap1)
                {
                    snap_path = "http://" + GlobalClass.TwoLevelReceiveIP + ":8880/v1/svc/cam1/snapshot?unique=";
                }
                else
                {
                    snap_path = "http://" + GlobalClass.TwoLevelReceiveIP + ":8880/v1/svc/cam2/snapshot?unique=";
                }
            }

            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, snap_path + new Guid().ToString()))
                {
                    using (var response = await client.SendAsync(request))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            byte[] image = await response.Content.ReadAsByteArrayAsync();
                            if (image.Length > 100)
                            {
                                OperatorImage = image;
                                //File.WriteAllBytes(Application.StartupPath + "\\sn.jpg", image); //пока пусть так
                                return true;
                            }
                            else
                            {
                                return false; //no data answer
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

        public async Task<bool> OperatorUploadImage(string RGuid, string strdate, string CurrUrl, byte[] image)
        {
            using (var client = new HttpClient())
            {
                using (var content =
                    new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                {
                    content.Add(new StreamContent(new MemoryStream(image)), "strdate", "upload.jpg"); //Дату + название

                    using (
                       var message =
                           await client.PostAsync(CurrUrl + "/api2/recognition/DeviceRecognition/new?guid=" + RGuid + "&type=picture&timestamp=" + strdate, content))
                    {
                        var input = await message.Content.ReadAsStringAsync();

                        return true;
                    }
                }
            }
        }

        public async Task<bool> PingCamera1()
        {
            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, "http://localhost:8880/v1/svc/cam1?unique=" + new Guid().ToString()))

                {
                    using (var response = await client.SendAsync(request))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            //{"bitrate":2279019,"last_frame":"2018-06-04T14:58:55.0502443Z","resolution":[1280,720]}
                            string answ = await response.Content.ReadAsStringAsync();
                            ViinexCameraPing vnxping = JsonConvert.DeserializeObject<ViinexCameraPing>(answ);
                            DateTime dtime = vnxping.last_frame;
                            if (dtime == null)
                            {
                                return false;
                            }
                            DateTime ndate = DateTime.Now;

                            if (dtime.AddMinutes(185) < ndate)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

        public async Task<bool> PingCamera2()
        {
            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, "http://localhost:8880/v1/svc/cam2?unique=" + new Guid().ToString()))

                {
                    using (var response = await client.SendAsync(request))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            //{"bitrate":2279019,"last_frame":"2018-06-04T14:58:55.0502443Z","resolution":[1280,720]}
                            string answ = await response.Content.ReadAsStringAsync();
                            ViinexCameraPing vnxping = JsonConvert.DeserializeObject<ViinexCameraPing>(answ);
                            DateTime dtime = vnxping.last_frame;
                            if (dtime == null)
                            {
                                return false;
                            }
                            DateTime ndate = DateTime.Now;

                            if (dtime.AddMinutes(185) < ndate)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

    }
}
