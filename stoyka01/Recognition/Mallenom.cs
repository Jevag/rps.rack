﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace stoyka01.Recognition
{

    //Старый класс 2018 года...
    public class Mallenom
    {
        MlnSettings mset;
        /*
        public void MallenomUpdateSettings() //Заполняем JSON-ку
        {
            mset = new MlnSettings();
            mset.clientIp = GlobalClass.MlnClientIP;
            mset.confidence = GlobalClass.MlnConfidence;
            mset.postProcessing = GlobalClass.MlnPostprocess;
            mset.preProcessing = GlobalClass.MlnPreprocess;
            mset.rtsp = GlobalClass.MlnRTSP;
            mset.timeBudget = GlobalClass.TimeBudget;
            mset.videoChannelId = GlobalClass.MlnVideoChannelId;

            //Стуктуры
            mset.realPlateSize1 = new RealPlateSize1();
            mset.realPlateSize1.width = GlobalClass.MlnMinSizeW;
            mset.realPlateSize1.height = GlobalClass.MlnMinSizeH;

            mset.realPlateSize2 = new RealPlateSize2();
            mset.realPlateSize2.width = GlobalClass.MlnMaxSizeW;
            mset.realPlateSize2.height = GlobalClass.MlnMaxSizeW;

            mset.points = new List<mPoint>();
            mPoint p1 = new mPoint();
            p1.x = GlobalClass.MlnPointX1;
            p1.y = GlobalClass.MlnPointY1;

            mPoint p2 = new mPoint();
            p2.x = GlobalClass.MlnPointX2;
            p2.y = GlobalClass.MlnPointY2;

            mPoint p3 = new mPoint();
            p3.x = GlobalClass.MlnPointX3;
            p3.y = GlobalClass.MlnPointY3;

            mPoint p4 = new mPoint();
            p4.x = GlobalClass.MlnPointX4;
            p4.y = GlobalClass.MlnPointY4;

            mPoint p5 = new mPoint();
            p5.x = GlobalClass.MlnPointX5;
            p5.y = GlobalClass.MlnPointY5;

            mPoint p6 = new mPoint();
            p6.x = GlobalClass.MlnPointX6;
            p6.y = GlobalClass.MlnPointY6;

            mPoint p7 = new mPoint();
            p7.x = GlobalClass.MlnPointX7;
            p7.y = GlobalClass.MlnPointY7;

            mPoint p8 = new mPoint();
            p8.x = GlobalClass.MlnPointX8;
            p8.y = GlobalClass.MlnPointY8;

            mset.points.Add(p1);
            mset.points.Add(p2);
            mset.points.Add(p3);
            mset.points.Add(p4);
            mset.points.Add(p5);
            mset.points.Add(p6);
            mset.points.Add(p7);
            mset.points.Add(p8);
        }
        */
        //
        // GET от РПС к Маленом на распознавание. (Ответ - 200 ОК).
        //http://server:5050/Recognize?run=true&timestamp=yyyyMMddThhmmsszzz

            /*
        public void SendRecognizeRequest()
        {
            try
            {
                HttpClient client = new HttpClient();
                DateTime cDate = DateTime.Now;
                string strDate = cDate.ToString("ddMMyyyyTHHmmsszzz", CultureInfo.InvariantCulture);

                client.GetAsync("http://" + GlobalClass.MlnServerIP + ":5050/Recognize?run=true&timestamp=" + strDate);
            }
            catch (Exception ex)
            {
            }
        }
        */

        //http://server:5050/Settings

            /*
        public void SendSettings()
        {
            MallenomUpdateSettings();
            string jsonstring = JsonConvert.SerializeObject(mset);

            HttpClient client = new HttpClient();
            var content = new StringContent(jsonstring, Encoding.UTF8, "application/json");
            var result = client.PostAsync("http://" + GlobalClass.MlnServerIP + "5050/Settings", content).Result;
        }
        */

            /*
        public void MallenomPing()
        {
            //http://localhost:5050/Status
            try
            {
                HttpClient client = new HttpClient();
                DateTime cDate = DateTime.Now;
                string strDate = cDate.ToString("ddMMyyyyTHHmmsszzz", CultureInfo.InvariantCulture);

                client.GetAsync("http://" + GlobalClass.MlnServerIP + ":5050/Status");
            }
            catch (Exception ex)
            {
            }
        }
        */

        //http://localhost:5050/SnapShot=yyyyMMddThhmmsszzz

            /*
        public void GetSnapshot() //Распарсить ответ в картинку!!!
        {
            try
            {
                HttpClient client = new HttpClient();
                DateTime cDate = DateTime.Now;
                string strDate = cDate.ToString("ddMMyyyyTHHmmsszzz", CultureInfo.InvariantCulture);

                client.GetAsync("http://" + GlobalClass.MlnServerIP + ":5050/SnapShot=" + strDate);
            }
            catch (Exception ex)
            {
            }
        }
        */
    }
}
