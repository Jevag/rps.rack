﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stoyka01
{
    //Enums by Lesnichenko
    public class Enums
    {
        /*
        public enum PlateNumberUseOptions
        {
            Disabled = 0, // отключено

            BlacklistAllow = 1, // черный список, если не распознан - пропускать
            BlacklistNotAllow = 2, // черный список, если не распознан - не пропускать
            BlacklistNeedOperator = 4,
            InOutAllow = 8,
            InOutNotAllow = 16,
            InOutNeedOperator = 32,
            RegularClient = 64,
            BarcodeInUse = 128,
        }
        */

            
        public enum PlateControlStatus : int
        {
            Passthru = 0, //-совпало или не проверяется,

            NotRAllow = 1, //Не распознан-пропуск

            NotRDeny = 2, //Не распознан-запрет

            InOutNotMatch = 3, //не совпал по ВВ

            BlackListed = 4, //запрет чс

            RegularMatch = 5, // пропуск по 

            RegularNotMatch = 6, //Нету ПК

            BarcodeExist = 7, //есть штрихкод

            BarcodeNotExist = 8, //нет штрихкода

            BarcodeExpired = 9, //устарел

           
        }
       
        /*
        public enum PlateType
        {
            InPermanent = 1,
            OutPermanentAndOnetimeClient = 2,
            InOnetimeClient = 3,
            PermanentClientAllow = 4
        }
        */
    }
}
