﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stoyka01.Recognition
{
    public class Am_Graphics
    {
        public static PointF[] basepoints = new PointF[15];
        public static PointF[] points = new PointF[15];

        public static PointF MinPlateSizePoint = new PointF();
        public static PointF MaxPlateSizePoint = new PointF();

        public struct PRect
        {
            public float X1;
            public float X2;
            public float Y1;
            public float Y2;
        }

        public struct iRect
        {
            public int X1;
            public int X2;
            public int Y1;
            public int Y2;
        }

        public static Rectangle MinPlateSize = new Rectangle();
        public static Rectangle MaxPlateSize = new Rectangle();


        public static PRect[] rects = new PRect[8];

        public static PRect MinPlateCorner = new PRect();
        public static PRect MaxPlateCorner = new PRect();

        public static void Transform2Rect2(int dotR)
        {
            MinPlateCorner.X1 = MinPlateSize.Right - dotR;
            MinPlateCorner.Y1 = MinPlateSize.Bottom - dotR;

            MinPlateCorner.X2 = MinPlateSize.Right + dotR;
            MinPlateCorner.Y2 = MinPlateSize.Bottom + dotR;

            MaxPlateCorner.X1 = MaxPlateSize.Right - dotR;
            MaxPlateCorner.Y1 = MaxPlateSize.Bottom - dotR;

            MaxPlateCorner.X2 = MaxPlateSize.Right + dotR;
            MaxPlateCorner.Y2 = MaxPlateSize.Bottom + dotR;
        }

        public static int DetectPlateIndex(int X, int Y)
        {
            //int ret = -1;
            if (X > MinPlateCorner.X1 && X < MinPlateCorner.X2 && Y > MinPlateCorner.Y1 && Y < MinPlateCorner.Y2)
            {
                return 11;
            }
            else
            {

            }

            return -1;
        }

        public static void Transform2Rect(int dotR)
        {
            for (int i = 0; i < 8; i++)
            {
                rects[i].X1 = points[i].X - dotR;
                rects[i].Y1 = points[i].Y - dotR;

                rects[i].X2 = points[i].X + dotR;
                rects[i].Y2 = points[i].Y + dotR;
            }
        }

        public static int DetectIndex(int X, int Y)
        {
            //int ret = -1;
            for (int i = 0; i < 8; i++)
            {
                if (X > rects[i].X1 && X < rects[i].X2 && Y > rects[i].Y1 && Y < rects[i].Y2)
                {
                    return i;
                }
            }

            if (X > MinPlateCorner.X1 && X < MinPlateCorner.X2 && Y > MinPlateCorner.Y1 && Y < MinPlateCorner.Y2)
            {
                return 11;
            }
            else if (X > MaxPlateCorner.X1 && X < MaxPlateCorner.X2 && Y > MaxPlateCorner.Y1 && Y < MaxPlateCorner.Y2)
            {
                return 12;
            }

            return -1;
        }

        public static void Init()
        {
            if (GlobalClass.MlnCamera == 1)
            {
                basepoints[0].X = Convert.ToSingle(GlobalClass.MlnPointX1_1);
                basepoints[1].X = Convert.ToSingle(GlobalClass.MlnPointX2_1);
                basepoints[2].X = Convert.ToSingle(GlobalClass.MlnPointX3_1);
                basepoints[3].X = Convert.ToSingle(GlobalClass.MlnPointX4_1);
                basepoints[4].X = Convert.ToSingle(GlobalClass.MlnPointX5_1);
                basepoints[5].X = Convert.ToSingle(GlobalClass.MlnPointX6_1);
                basepoints[6].X = Convert.ToSingle(GlobalClass.MlnPointX7_1);
                basepoints[7].X = Convert.ToSingle(GlobalClass.MlnPointX8_1);

                basepoints[0].Y = Convert.ToSingle(GlobalClass.MlnPointY1_1);
                basepoints[1].Y = Convert.ToSingle(GlobalClass.MlnPointY2_1);
                basepoints[2].Y = Convert.ToSingle(GlobalClass.MlnPointY3_1);
                basepoints[3].Y = Convert.ToSingle(GlobalClass.MlnPointY4_1);
                basepoints[4].Y = Convert.ToSingle(GlobalClass.MlnPointY5_1);
                basepoints[5].Y = Convert.ToSingle(GlobalClass.MlnPointY6_1);
                basepoints[6].Y = Convert.ToSingle(GlobalClass.MlnPointY7_1);
                basepoints[7].Y = Convert.ToSingle(GlobalClass.MlnPointY8_1);

                MinPlateSize.Size = new Size(Convert.ToInt32(GlobalClass.MlnMinSizeW_1 * 540) , Convert.ToInt32(GlobalClass.MlnMinSizeH_1 * 400) );
                MaxPlateSize.Size = new Size(Convert.ToInt32(GlobalClass.MlnMaxSizeW_1 * 540) , Convert.ToInt32(GlobalClass.MlnMaxSizeH_1 * 400) );
                MinPlateSize.Location = new Point(350, 80);
                MaxPlateSize.Location = new Point(270, 180);
            }
            else if (GlobalClass.MlnCamera == 2)
            {
                basepoints[0].X = Convert.ToSingle(GlobalClass.MlnPointX1_2);
                basepoints[1].X = Convert.ToSingle(GlobalClass.MlnPointX2_2);
                basepoints[2].X = Convert.ToSingle(GlobalClass.MlnPointX3_2);
                basepoints[3].X = Convert.ToSingle(GlobalClass.MlnPointX4_2);
                basepoints[4].X = Convert.ToSingle(GlobalClass.MlnPointX5_2);
                basepoints[5].X = Convert.ToSingle(GlobalClass.MlnPointX6_2);
                basepoints[6].X = Convert.ToSingle(GlobalClass.MlnPointX7_2);
                basepoints[7].X = Convert.ToSingle(GlobalClass.MlnPointX8_2);

                basepoints[0].Y = Convert.ToSingle(GlobalClass.MlnPointY1_2);
                basepoints[1].Y = Convert.ToSingle(GlobalClass.MlnPointY2_2);
                basepoints[2].Y = Convert.ToSingle(GlobalClass.MlnPointY3_2);
                basepoints[3].Y = Convert.ToSingle(GlobalClass.MlnPointY4_2);
                basepoints[4].Y = Convert.ToSingle(GlobalClass.MlnPointY5_2);
                basepoints[5].Y = Convert.ToSingle(GlobalClass.MlnPointY6_2);
                basepoints[6].Y = Convert.ToSingle(GlobalClass.MlnPointY7_2);
                basepoints[7].Y = Convert.ToSingle(GlobalClass.MlnPointY8_2);

                //MinPlateSize.Size = new SizeF(Convert.ToSingle(GlobalClass.MlnMinSizeW_1) * 540, Convert.ToSingle(GlobalClass.MlnMinSizeH_1) * 400);
                //MaxPlateSize.Size = new SizeF(Convert.ToSingle(GlobalClass.MlnMaxSizeW_1) * 540, Convert.ToSingle(GlobalClass.MlnMaxSizeH_1) * 400);

                //MinPlateSize.Location = new PointF(200, 200);
                //MaxPlateSize.Location = new PointF(400, 400);
            }

            points[0].X = basepoints[0].X * 540;
            points[1].X = basepoints[1].X * 540;
            points[2].X = basepoints[2].X * 540;
            points[3].X = basepoints[3].X * 540;
            points[4].X = basepoints[4].X * 540;
            points[5].X = basepoints[5].X * 540;
            points[6].X = basepoints[6].X * 540;
            points[7].X = basepoints[7].X * 540;

            points[0].Y = basepoints[0].Y * 400;
            points[1].Y = basepoints[1].Y * 400;
            points[2].Y = basepoints[2].Y * 400;
            points[3].Y = basepoints[3].Y * 400;
            points[4].Y = basepoints[4].Y * 400;
            points[5].Y = basepoints[5].Y * 400;
            points[6].Y = basepoints[6].Y * 400;
            points[7].Y = basepoints[7].Y * 400;

            Transform2Rect(7);
            Transform2Rect2(5);
        }

    }
}
