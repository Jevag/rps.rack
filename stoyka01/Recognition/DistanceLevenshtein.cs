﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stoyka01
{
    public class Levenshtein
    {
        public struct RegionValue
        {
            public string LeftPlate;
            public string Region;
        }

        public static int LevenshteinDistance(string string1, string string2)
        {
            if (string1 == null) throw new ArgumentNullException("string1");
            if (string2 == null) throw new ArgumentNullException("string2");
            int diff;
            int[,] m = new int[string1.Length + 1, string2.Length + 1];

            for (int i = 0; i <= string1.Length; i++) { m[i, 0] = i; }
            for (int j = 0; j <= string2.Length; j++) { m[0, j] = j; }

            for (int i = 1; i <= string1.Length; i++)
            {
                for (int j = 1; j <= string2.Length; j++)
                {
                    diff = (string1[i - 1] == string2[j - 1]) ? 0 : 1;

                    m[i, j] = Math.Min(Math.Min(m[i - 1, j] + 1,
                                             m[i, j - 1] + 1),
                                             m[i - 1, j - 1] + diff);
                }
            }
            return m[string1.Length, string2.Length];
        }

        public static RegionValue LevenRegion(string plate)
        {
            RegionValue retval = new RegionValue();
            retval.LeftPlate = "";
            retval.Region = "";
            int plate_len = plate.Length;
            int back_index = 0;
            int fwd_index = plate_len - back_index - 1;
            try
            {
                while (char.IsNumber(plate, fwd_index))
                {
                    fwd_index -= 1;
                    back_index += 1;
                    if (fwd_index == 0) return retval;
                }

                if (back_index == 2 || back_index == 3)
                {
                    retval.Region = plate.Substring(fwd_index + 1, back_index);
                    retval.LeftPlate = plate.Substring(0, fwd_index + 1);
                }
            }
            catch (Exception ex)
            {
                return retval;
            }

            return retval;
        }
    }
}
