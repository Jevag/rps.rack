﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace stoyka01.Recognition
{
    public class AM_Xml
    {
        // using System.Xml.Serialization;
        // XmlSerializer serializer = new XmlSerializer(typeof(Kernel));
        // using (StringReader reader = new StringReader(xml))
        // {
        //    var test = (Kernel)serializer.Deserialize(reader);
        // }

        [XmlRoot(ElementName = "IsEnabled")]
        public class IsEnabled
        {
            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "Priority")]
        public class Priority
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "Stencil")]
        public class Stencil
        {

            [XmlAttribute(AttributeName = "Id")]
            public string Id { get; set; }

            [XmlAttribute(AttributeName = "Weight")]
            public int Weight { get; set; }
        }

        [XmlRoot(ElementName = "Stencils")]
        public class Stencils
        {

            [XmlElement(ElementName = "Stencil")]
            public List<Stencil> Stencil { get; set; }
        }

        [XmlRoot(ElementName = "Model")]
        public class Model
        {

            [XmlElement(ElementName = "IsEnabled")]
            public IsEnabled IsEnabled { get; set; }

            [XmlElement(ElementName = "Priority")]
            public Priority Priority { get; set; }

            [XmlElement(ElementName = "Stencils")]
            public Stencils Stencils { get; set; }

            [XmlAttribute(AttributeName = "Id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "Models")]
        public class Models
        {

            [XmlElement(ElementName = "Model")]
            public Model Model { get; set; }
        }

        [XmlRoot(ElementName = "IgnoreMovementDirection")]
        public class IgnoreMovementDirection
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "DecisionHistoryLength")]
        public class DecisionHistoryLength
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "KnownPlateListEnabled")]
        public class KnownPlateListEnabled
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "MinSimilarity")]
        public class MinSimilarity
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "MaxShift")]
        public class MaxShift
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "Decisions")]
        public class Decisions
        {

            [XmlElement(ElementName = "IgnoreMovementDirection")]
            public IgnoreMovementDirection IgnoreMovementDirection { get; set; }

            [XmlElement(ElementName = "DecisionHistoryLength")]
            public DecisionHistoryLength DecisionHistoryLength { get; set; }

            [XmlElement(ElementName = "KnownPlateListEnabled")]
            public KnownPlateListEnabled KnownPlateListEnabled { get; set; }

            [XmlElement(ElementName = "MinSimilarity")]
            public MinSimilarity MinSimilarity { get; set; }

            [XmlElement(ElementName = "MaxShift")]
            public MaxShift MaxShift { get; set; }
        }

        [XmlRoot(ElementName = "ChannelName")]
        public class ChannelName
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "Undefined")]
        public class Undefined
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "Down")]
        public class Down
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "Up")]
        public class Up
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "VehicleDirections")]
        public class VehicleDirections
        {

            [XmlElement(ElementName = "Undefined")]
            public Undefined Undefined { get; set; }

            [XmlElement(ElementName = "Down")]
            public Down Down { get; set; }

            [XmlElement(ElementName = "Up")]
            public Up Up { get; set; }
        }

        [XmlRoot(ElementName = "SurveillanceOnly")]
        public class SurveillanceOnly
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "ColorMode")]
        public class ColorMode
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "VehicleDirectionNames")]
        public class VehicleDirectionNames
        {

            [XmlElement(ElementName = "Undefined")]
            public Undefined Undefined { get; set; }

            [XmlElement(ElementName = "Down")]
            public Down Down { get; set; }

            [XmlElement(ElementName = "Up")]
            public Up Up { get; set; }
        }

        [XmlRoot(ElementName = "VideoSourceType")]
        public class VideoSourceType
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "StreamUrl")]
        public class StreamUrl
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "OpenTimeout")]
        public class OpenTimeout
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "AutoRepeat")]
        public class AutoRepeat
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "PreferStreamTimestamps")]
        public class PreferStreamTimestamps
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "Transport")]
        public class Transport
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "PreferTCPProtocol")]
        public class PreferTCPProtocol
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "MinimumUDPPort")]
        public class MinimumUDPPort
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "MaximumUDPPort")]
        public class MaximumUDPPort
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "SocketTimeout")]
        public class SocketTimeout
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "RTSP")]
        public class RTSP
        {

            [XmlElement(ElementName = "Transport")]
            public Transport Transport { get; set; }

            [XmlElement(ElementName = "PreferTCPProtocol")]
            public PreferTCPProtocol PreferTCPProtocol { get; set; }

            [XmlElement(ElementName = "MinimumUDPPort")]
            public MinimumUDPPort MinimumUDPPort { get; set; }

            [XmlElement(ElementName = "MaximumUDPPort")]
            public MaximumUDPPort MaximumUDPPort { get; set; }

            [XmlElement(ElementName = "SocketTimeout")]
            public SocketTimeout SocketTimeout { get; set; }
        }

        [XmlRoot(ElementName = "VideoSource")]
        public class VideoSource
        {

            [XmlElement(ElementName = "StreamUrl")]
            public StreamUrl StreamUrl { get; set; }

            [XmlElement(ElementName = "OpenTimeout")]
            public OpenTimeout OpenTimeout { get; set; }

            [XmlElement(ElementName = "AutoRepeat")]
            public AutoRepeat AutoRepeat { get; set; }

            [XmlElement(ElementName = "PreferStreamTimestamps")]
            public PreferStreamTimestamps PreferStreamTimestamps { get; set; }

            [XmlElement(ElementName = "RTSP")]
            public RTSP RTSP { get; set; }
        }

        [XmlRoot(ElementName = "ProcessingDelay")]
        public class ProcessingDelay
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "K1")]
        public class K1
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "K2")]
        public class K2
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "CorrectDistortion")]
        public class CorrectDistortion
        {

            [XmlElement(ElementName = "K1")]
            public K1 K1 { get; set; }

            [XmlElement(ElementName = "K2")]
            public K2 K2 { get; set; }

            [XmlAttribute(AttributeName = "IsEnabled")]
            public bool IsEnabled { get; set; }
        }

        [XmlRoot(ElementName = "Vp")]
        public class Vp
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "VerticalPerspective")]
        public class VerticalPerspective
        {

            [XmlElement(ElementName = "Vp")]
            public Vp Vp { get; set; }

            [XmlAttribute(AttributeName = "IsEnabled")]
            public bool IsEnabled { get; set; }
        }

        [XmlRoot(ElementName = "Hp")]
        public class Hp
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "HorizontalPerspective")]
        public class HorizontalPerspective
        {

            [XmlElement(ElementName = "Hp")]
            public Hp Hp { get; set; }

            [XmlAttribute(AttributeName = "IsEnabled")]
            public bool IsEnabled { get; set; }
        }

        [XmlRoot(ElementName = "Angle")]
        public class Angle
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "Rotate")]
        public class Rotate
        {

            [XmlElement(ElementName = "Angle")]
            public Angle Angle { get; set; }

            [XmlAttribute(AttributeName = "IsEnabled")]
            public bool IsEnabled { get; set; }
        }

        [XmlRoot(ElementName = "ScaleX")]
        public class ScaleX
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "ScaleY")]
        public class ScaleY
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "Scale")]
        public class Scale
        {

            [XmlElement(ElementName = "ScaleX")]
            public ScaleX ScaleX { get; set; }

            [XmlElement(ElementName = "ScaleY")]
            public ScaleY ScaleY { get; set; }

            [XmlAttribute(AttributeName = "IsEnabled")]
            public bool IsEnabled { get; set; }
        }

        [XmlRoot(ElementName = "ShearX")]
        public class ShearX
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "ShearY")]
        public class ShearY
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "Shear")]
        public class Shear
        {

            [XmlElement(ElementName = "ShearX")]
            public ShearX ShearX { get; set; }

            [XmlElement(ElementName = "ShearY")]
            public ShearY ShearY { get; set; }

            [XmlAttribute(AttributeName = "IsEnabled")]
            public bool IsEnabled { get; set; }
        }

        [XmlRoot(ElementName = "Affine")]
        public class Affine
        {

            [XmlElement(ElementName = "Rotate")]
            public Rotate Rotate { get; set; }

            [XmlElement(ElementName = "Scale")]
            public Scale Scale { get; set; }

            [XmlElement(ElementName = "Shear")]
            public Shear Shear { get; set; }
        }

        [XmlRoot(ElementName = "Origin")]
        public class Origin
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "X1")]
        public class X1
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "Y1")]
        public class Y1
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "X2")]
        public class X2
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "Y2")]
        public class Y2
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "Bounds")]
        public class Bounds
        {

            [XmlElement(ElementName = "Origin")]
            public Origin Origin { get; set; }

            [XmlElement(ElementName = "X1")]
            public X1 X1 { get; set; }

            [XmlElement(ElementName = "Y1")]
            public Y1 Y1 { get; set; }

            [XmlElement(ElementName = "X2")]
            public X2 X2 { get; set; }

            [XmlElement(ElementName = "Y2")]
            public Y2 Y2 { get; set; }

            [XmlAttribute(AttributeName = "IsEnabled")]
            public bool IsEnabled { get; set; }
        }

        [XmlRoot(ElementName = "Spatial")]
        public class Spatial
        {

            [XmlElement(ElementName = "CorrectDistortion")]
            public CorrectDistortion CorrectDistortion { get; set; }

            [XmlElement(ElementName = "VerticalPerspective")]
            public VerticalPerspective VerticalPerspective { get; set; }

            [XmlElement(ElementName = "HorizontalPerspective")]
            public HorizontalPerspective HorizontalPerspective { get; set; }

            [XmlElement(ElementName = "Affine")]
            public Affine Affine { get; set; }

            [XmlElement(ElementName = "Bounds")]
            public Bounds Bounds { get; set; }
        }

        [XmlRoot(ElementName = "ImageTransforms")]
        public class ImageTransforms
        {

            [XmlElement(ElementName = "Spatial")]
            public Spatial Spatial { get; set; }
        }

        [XmlRoot(ElementName = "MotionDetectMethod")]
        public class MotionDetectMethod
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "DecisionKeepTimeout")]
        public class DecisionKeepTimeout
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "FrameQueueSize")]
        public class FrameQueueSize
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "IsCopyImagesInMotionsFrames")]
        public class IsCopyImagesInMotionsFrames
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "BlockByPrevDecisionMode")]
        public class BlockByPrevDecisionMode
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "BlockUp")]
        public class BlockUp
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "BlockDown")]
        public class BlockDown
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "BlockUndefined")]
        public class BlockUndefined
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "FrozenBlockEnabled")]
        public class FrozenBlockEnabled
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "FrozenCounter")]
        public class FrozenCounter
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "FrozenPlateZoneIntersection")]
        public class FrozenPlateZoneIntersection
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "IsVehicleDetectionEnabled")]
        public class IsVehicleDetectionEnabled
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "IsVehicleTypeClassificationEnabled")]
        public class IsVehicleTypeClassificationEnabled
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "Localization")]
        public class Localization
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "ImageResizing")]
        public class ImageResizing
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "ProcessingStrategy")]
        public class ProcessingStrategy
        {

            [XmlElement(ElementName = "Localization")]
            public Localization Localization { get; set; }

            [XmlElement(ElementName = "ImageResizing")]
            public ImageResizing ImageResizing { get; set; }
        }

        [XmlRoot(ElementName = "MaxAllowedUnrecognizedChars")]
        public class MaxAllowedUnrecognizedChars
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "UsePlateSizeSpecifiedLimitsInRecognitionStage")]
        public class UsePlateSizeSpecifiedLimitsInRecognitionStage
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "Composite")]
        public class Composite
        {

            [XmlElement(ElementName = "IsVehicleDetectionEnabled")]
            public IsVehicleDetectionEnabled IsVehicleDetectionEnabled { get; set; }

            [XmlElement(ElementName = "IsVehicleTypeClassificationEnabled")]
            public IsVehicleTypeClassificationEnabled IsVehicleTypeClassificationEnabled { get; set; }

            [XmlElement(ElementName = "ProcessingStrategy")]
            public ProcessingStrategy ProcessingStrategy { get; set; }

            [XmlElement(ElementName = "MaxAllowedUnrecognizedChars")]
            public MaxAllowedUnrecognizedChars MaxAllowedUnrecognizedChars { get; set; }

            [XmlElement(ElementName = "UsePlateSizeSpecifiedLimitsInRecognitionStage")]
            public UsePlateSizeSpecifiedLimitsInRecognitionStage UsePlateSizeSpecifiedLimitsInRecognitionStage { get; set; }
        }

        [XmlRoot(ElementName = "MatrixesCount")]
        public class MatrixesCount
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "IsProcessAllMatrixes")]
        public class IsProcessAllMatrixes
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "Point")]
        public class Point
        {

            [XmlAttribute(AttributeName = "X")]
            public double X { get; set; }

            [XmlAttribute(AttributeName = "Y")]
            public double Y { get; set; }
        }

        [XmlRoot(ElementName = "Roi")]
        public class Roi
        {

            [XmlElement(ElementName = "Point")]
            public List<Point> Point { get; set; }
        }

        [XmlRoot(ElementName = "RealPlateSize1")]
        public class RealPlateSize1
        {

            [XmlAttribute(AttributeName = "Mode")]
            public string Mode { get; set; }

            [XmlAttribute(AttributeName = "X")]
            public int X { get; set; }

            [XmlAttribute(AttributeName = "Y")]
            public int Y { get; set; }

            [XmlAttribute(AttributeName = "Width")]
            public double Width { get; set; }

            [XmlAttribute(AttributeName = "Height")]
            public double Height { get; set; }
        }

        [XmlRoot(ElementName = "RealPlateSize2")]
        public class RealPlateSize2
        {

            [XmlAttribute(AttributeName = "Mode")]
            public string Mode { get; set; }

            [XmlAttribute(AttributeName = "X")]
            public int X { get; set; }

            [XmlAttribute(AttributeName = "Y")]
            public int Y { get; set; }

            [XmlAttribute(AttributeName = "Width")]
            public double Width { get; set; }

            [XmlAttribute(AttributeName = "Height")]
            public double Height { get; set; }
        }

        [XmlRoot(ElementName = "Convenient")]
        public class Convenient
        {

            [XmlElement(ElementName = "MatrixesCount")]
            public MatrixesCount MatrixesCount { get; set; }

            [XmlElement(ElementName = "IsProcessAllMatrixes")]
            public IsProcessAllMatrixes IsProcessAllMatrixes { get; set; }

            [XmlElement(ElementName = "Roi")]
            public Roi Roi { get; set; }

            [XmlElement(ElementName = "RealPlateSize1")]
            public RealPlateSize1 RealPlateSize1 { get; set; }

            [XmlElement(ElementName = "RealPlateSize2")]
            public RealPlateSize2 RealPlateSize2 { get; set; }
        }

        [XmlRoot(ElementName = "KM")]
        public class KM
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "AMotion")]
        public class AMotion
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "InertiaCoefficient")]
        public class InertiaCoefficient
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "InertiaFramesCount")]
        public class InertiaFramesCount
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "AM")]
        public class AM
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "SpecialPointThresholdAddition")]
        public class SpecialPointThresholdAddition
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "CoefMinThreshold")]
        public class CoefMinThreshold
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "CoefMaxThreshold")]
        public class CoefMaxThreshold
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "MotionDetection")]
        public class MotionDetection
        {

            [XmlElement(ElementName = "KM")]
            public KM KM { get; set; }

            [XmlElement(ElementName = "AMotion")]
            public AMotion AMotion { get; set; }

            [XmlElement(ElementName = "InertiaCoefficient")]
            public InertiaCoefficient InertiaCoefficient { get; set; }

            [XmlElement(ElementName = "InertiaFramesCount")]
            public InertiaFramesCount InertiaFramesCount { get; set; }

            [XmlElement(ElementName = "AM")]
            public AM AM { get; set; }

            [XmlElement(ElementName = "SpecialPointThresholdAddition")]
            public SpecialPointThresholdAddition SpecialPointThresholdAddition { get; set; }

            [XmlElement(ElementName = "CoefMinThreshold")]
            public CoefMinThreshold CoefMinThreshold { get; set; }

            [XmlElement(ElementName = "CoefMaxThreshold")]
            public CoefMaxThreshold CoefMaxThreshold { get; set; }
        }

        [XmlRoot(ElementName = "Threshold")]
        public class Threshold
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "DetectedFrameCount")]
        public class DetectedFrameCount
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "NonDetectedFrameCount")]
        public class NonDetectedFrameCount
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "VehicleDetection")]
        public class VehicleDetection
        {

            [XmlElement(ElementName = "Threshold")]
            public Threshold Threshold { get; set; }

            [XmlElement(ElementName = "DetectedFrameCount")]
            public DetectedFrameCount DetectedFrameCount { get; set; }

            [XmlElement(ElementName = "NonDetectedFrameCount")]
            public NonDetectedFrameCount NonDetectedFrameCount { get; set; }
        }

        [XmlRoot(ElementName = "TopIgnoreLines")]
        public class TopIgnoreLines
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "BottomIgnoreLines")]
        public class BottomIgnoreLines
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "PlateMinWidthTop")]
        public class PlateMinWidthTop
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "PlateMinHeightTop")]
        public class PlateMinHeightTop
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "PlateMaxWidthTop")]
        public class PlateMaxWidthTop
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "PlateMaxHeightTop")]
        public class PlateMaxHeightTop
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "PlateMinWidthBottom")]
        public class PlateMinWidthBottom
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "PlateMinHeightBottom")]
        public class PlateMinHeightBottom
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "PlateMaxWidthBottom")]
        public class PlateMaxWidthBottom
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "PlateMaxHeightBottom")]
        public class PlateMaxHeightBottom
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "PlatesNeeded")]
        public class PlatesNeeded
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "LowerLimitSquareHistogram")]
        public class LowerLimitSquareHistogram
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "UpperLimitSquareHistogram")]
        public class UpperLimitSquareHistogram
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "MinimumQuantityThreshold")]
        public class MinimumQuantityThreshold
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "ColsPerPlate")]
        public class ColsPerPlate
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "ElimWeakCandsNeighborCount")]
        public class ElimWeakCandsNeighborCount
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "SquareIterations")]
        public class SquareIterations
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "MinSquareThreshold")]
        public class MinSquareThreshold
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "ExpandColumnWidth")]
        public class ExpandColumnWidth
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "PlateLocalization")]
        public class PlateLocalization
        {

            [XmlElement(ElementName = "TopIgnoreLines")]
            public TopIgnoreLines TopIgnoreLines { get; set; }

            [XmlElement(ElementName = "BottomIgnoreLines")]
            public BottomIgnoreLines BottomIgnoreLines { get; set; }

            [XmlElement(ElementName = "PlateMinWidthTop")]
            public PlateMinWidthTop PlateMinWidthTop { get; set; }

            [XmlElement(ElementName = "PlateMinHeightTop")]
            public PlateMinHeightTop PlateMinHeightTop { get; set; }

            [XmlElement(ElementName = "PlateMaxWidthTop")]
            public PlateMaxWidthTop PlateMaxWidthTop { get; set; }

            [XmlElement(ElementName = "PlateMaxHeightTop")]
            public PlateMaxHeightTop PlateMaxHeightTop { get; set; }

            [XmlElement(ElementName = "PlateMinWidthBottom")]
            public PlateMinWidthBottom PlateMinWidthBottom { get; set; }

            [XmlElement(ElementName = "PlateMinHeightBottom")]
            public PlateMinHeightBottom PlateMinHeightBottom { get; set; }

            [XmlElement(ElementName = "PlateMaxWidthBottom")]
            public PlateMaxWidthBottom PlateMaxWidthBottom { get; set; }

            [XmlElement(ElementName = "PlateMaxHeightBottom")]
            public PlateMaxHeightBottom PlateMaxHeightBottom { get; set; }

            [XmlElement(ElementName = "PlatesNeeded")]
            public PlatesNeeded PlatesNeeded { get; set; }

            [XmlElement(ElementName = "LowerLimitSquareHistogram")]
            public LowerLimitSquareHistogram LowerLimitSquareHistogram { get; set; }

            [XmlElement(ElementName = "UpperLimitSquareHistogram")]
            public UpperLimitSquareHistogram UpperLimitSquareHistogram { get; set; }

            [XmlElement(ElementName = "MinimumQuantityThreshold")]
            public MinimumQuantityThreshold MinimumQuantityThreshold { get; set; }

            [XmlElement(ElementName = "ColsPerPlate")]
            public ColsPerPlate ColsPerPlate { get; set; }

            [XmlElement(ElementName = "ElimWeakCandsNeighborCount")]
            public ElimWeakCandsNeighborCount ElimWeakCandsNeighborCount { get; set; }

            [XmlElement(ElementName = "SquareIterations")]
            public SquareIterations SquareIterations { get; set; }

            [XmlElement(ElementName = "MinSquareThreshold")]
            public MinSquareThreshold MinSquareThreshold { get; set; }

            [XmlElement(ElementName = "ExpandColumnWidth")]
            public ExpandColumnWidth ExpandColumnWidth { get; set; }
        }

        [XmlRoot(ElementName = "LocalizationBrightStep")]
        public class LocalizationBrightStep
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "FirstGoodModelOut")]
        public class FirstGoodModelOut
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "ZoneSearchMethod")]
        public class ZoneSearchMethod
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "MinPlateWidth")]
        public class MinPlateWidth
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "MaxPlateWidth")]
        public class MaxPlateWidth
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "MinPlateHeight")]
        public class MinPlateHeight
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "MaxPlateHeight")]
        public class MaxPlateHeight
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "SymbolMinWidth")]
        public class SymbolMinWidth
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "SymbolMaxWidth")]
        public class SymbolMaxWidth
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "SymbolMinHeight")]
        public class SymbolMinHeight
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "SymbolMaxHeight")]
        public class SymbolMaxHeight
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "SymbolMinSquare")]
        public class SymbolMinSquare
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "SymbolMaxSquare")]
        public class SymbolMaxSquare
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "MinAllowRecogSymbolCount")]
        public class MinAllowRecogSymbolCount
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "MaxAllowUnrecogSymbolCount")]
        public class MaxAllowUnrecogSymbolCount
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "SymbolRecogMinProbability")]
        public class SymbolRecogMinProbability
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "SegmKDX")]
        public class SegmKDX
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "SegmKDY")]
        public class SegmKDY
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "ReplaceStencilSymbPosition")]
        public class ReplaceStencilSymbPosition
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "ThSymbolsIntersection")]
        public class ThSymbolsIntersection
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "RecognitionBrightStep")]
        public class RecognitionBrightStep
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "RecognTryRecoveryPlateNeuro")]
        public class RecognTryRecoveryPlateNeuro
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "PlateRecognition")]
        public class PlateRecognition
        {

            [XmlElement(ElementName = "LocalizationBrightStep")]
            public LocalizationBrightStep LocalizationBrightStep { get; set; }

            [XmlElement(ElementName = "FirstGoodModelOut")]
            public FirstGoodModelOut FirstGoodModelOut { get; set; }

            [XmlElement(ElementName = "ZoneSearchMethod")]
            public ZoneSearchMethod ZoneSearchMethod { get; set; }

            [XmlElement(ElementName = "MinPlateWidth")]
            public MinPlateWidth MinPlateWidth { get; set; }

            [XmlElement(ElementName = "MaxPlateWidth")]
            public MaxPlateWidth MaxPlateWidth { get; set; }

            [XmlElement(ElementName = "MinPlateHeight")]
            public MinPlateHeight MinPlateHeight { get; set; }

            [XmlElement(ElementName = "MaxPlateHeight")]
            public MaxPlateHeight MaxPlateHeight { get; set; }

            [XmlElement(ElementName = "SymbolMinWidth")]
            public SymbolMinWidth SymbolMinWidth { get; set; }

            [XmlElement(ElementName = "SymbolMaxWidth")]
            public SymbolMaxWidth SymbolMaxWidth { get; set; }

            [XmlElement(ElementName = "SymbolMinHeight")]
            public SymbolMinHeight SymbolMinHeight { get; set; }

            [XmlElement(ElementName = "SymbolMaxHeight")]
            public SymbolMaxHeight SymbolMaxHeight { get; set; }

            [XmlElement(ElementName = "SymbolMinSquare")]
            public SymbolMinSquare SymbolMinSquare { get; set; }

            [XmlElement(ElementName = "SymbolMaxSquare")]
            public SymbolMaxSquare SymbolMaxSquare { get; set; }

            [XmlElement(ElementName = "MinAllowRecogSymbolCount")]
            public MinAllowRecogSymbolCount MinAllowRecogSymbolCount { get; set; }

            [XmlElement(ElementName = "MaxAllowUnrecogSymbolCount")]
            public MaxAllowUnrecogSymbolCount MaxAllowUnrecogSymbolCount { get; set; }

            [XmlElement(ElementName = "SymbolRecogMinProbability")]
            public SymbolRecogMinProbability SymbolRecogMinProbability { get; set; }

            [XmlElement(ElementName = "SegmKDX")]
            public SegmKDX SegmKDX { get; set; }

            [XmlElement(ElementName = "SegmKDY")]
            public SegmKDY SegmKDY { get; set; }

            [XmlElement(ElementName = "ReplaceStencilSymbPosition")]
            public ReplaceStencilSymbPosition ReplaceStencilSymbPosition { get; set; }

            [XmlElement(ElementName = "ThSymbolsIntersection")]
            public ThSymbolsIntersection ThSymbolsIntersection { get; set; }

            [XmlElement(ElementName = "RecognitionBrightStep")]
            public RecognitionBrightStep RecognitionBrightStep { get; set; }

            [XmlElement(ElementName = "RecognTryRecoveryPlateNeuro")]
            public RecognTryRecoveryPlateNeuro RecognTryRecoveryPlateNeuro { get; set; }
        }

        [XmlRoot(ElementName = "DecisionTimeout")]
        public class DecisionTimeout
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "MaxProbDifference")]
        public class MaxProbDifference
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "MinDecisionThreshold")]
        public class MinDecisionThreshold
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "MinDecisionFrames")]
        public class MinDecisionFrames
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "MinUnrecogDecisionFrames")]
        public class MinUnrecogDecisionFrames
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "MaxDecisionFrames")]
        public class MaxDecisionFrames
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "SelectBestPlateForStencilStep")]
        public class SelectBestPlateForStencilStep
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "MotionTimeout")]
        public class MotionTimeout
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "MakeUnrecogDecisionByMotion")]
        public class MakeUnrecogDecisionByMotion
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "FrameNumberFromDetectMotion")]
        public class FrameNumberFromDetectMotion
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "IsImproveBestResult")]
        public class IsImproveBestResult
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "IsImproveUnrecogSymbols")]
        public class IsImproveUnrecogSymbols
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "IntersectionThreshold")]
        public class IntersectionThreshold
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "ValueSymbolCoef")]
        public class ValueSymbolCoef
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "IsCoeffConfidenceEnabled")]
        public class IsCoeffConfidenceEnabled
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "ReductionConfidenceCoefficient")]
        public class ReductionConfidenceCoefficient
        {

            [XmlAttribute(AttributeName = "Value")]
            public double Value { get; set; }
        }

        [XmlRoot(ElementName = "DecisionMaking")]
        public class DecisionMaking
        {

            [XmlElement(ElementName = "DecisionTimeout")]
            public DecisionTimeout DecisionTimeout { get; set; }

            [XmlElement(ElementName = "MaxProbDifference")]
            public MaxProbDifference MaxProbDifference { get; set; }

            [XmlElement(ElementName = "MinDecisionThreshold")]
            public MinDecisionThreshold MinDecisionThreshold { get; set; }

            [XmlElement(ElementName = "MinDecisionFrames")]
            public MinDecisionFrames MinDecisionFrames { get; set; }

            [XmlElement(ElementName = "MinUnrecogDecisionFrames")]
            public MinUnrecogDecisionFrames MinUnrecogDecisionFrames { get; set; }

            [XmlElement(ElementName = "MaxDecisionFrames")]
            public MaxDecisionFrames MaxDecisionFrames { get; set; }

            [XmlElement(ElementName = "MaxAllowedUnrecognizedChars")]
            public MaxAllowedUnrecognizedChars MaxAllowedUnrecognizedChars { get; set; }

            [XmlElement(ElementName = "SelectBestPlateForStencilStep")]
            public SelectBestPlateForStencilStep SelectBestPlateForStencilStep { get; set; }

            [XmlElement(ElementName = "MotionTimeout")]
            public MotionTimeout MotionTimeout { get; set; }

            [XmlElement(ElementName = "MakeUnrecogDecisionByMotion")]
            public MakeUnrecogDecisionByMotion MakeUnrecogDecisionByMotion { get; set; }

            [XmlElement(ElementName = "FrameNumberFromDetectMotion")]
            public FrameNumberFromDetectMotion FrameNumberFromDetectMotion { get; set; }

            [XmlElement(ElementName = "IsImproveBestResult")]
            public IsImproveBestResult IsImproveBestResult { get; set; }

            [XmlElement(ElementName = "IsImproveUnrecogSymbols")]
            public IsImproveUnrecogSymbols IsImproveUnrecogSymbols { get; set; }

            [XmlElement(ElementName = "IntersectionThreshold")]
            public IntersectionThreshold IntersectionThreshold { get; set; }

            [XmlElement(ElementName = "ValueSymbolCoef")]
            public ValueSymbolCoef ValueSymbolCoef { get; set; }

            [XmlElement(ElementName = "IsCoeffConfidenceEnabled")]
            public IsCoeffConfidenceEnabled IsCoeffConfidenceEnabled { get; set; }

            [XmlElement(ElementName = "ReductionConfidenceCoefficient")]
            public ReductionConfidenceCoefficient ReductionConfidenceCoefficient { get; set; }
        }

        [XmlRoot(ElementName = "FramesForEntry")]
        public class FramesForEntry
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "FramesWithPlateForEntry")]
        public class FramesWithPlateForEntry
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "FramesForExit")]
        public class FramesForExit
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "FramesWithoutPlateForExit")]
        public class FramesWithoutPlateForExit
        {

            [XmlAttribute(AttributeName = "Value")]
            public int Value { get; set; }
        }

        [XmlRoot(ElementName = "ProcessUnit")]
        public class ProcessUnit
        {

            [XmlElement(ElementName = "MotionDetectMethod")]
            public MotionDetectMethod MotionDetectMethod { get; set; }

            [XmlElement(ElementName = "DecisionKeepTimeout")]
            public DecisionKeepTimeout DecisionKeepTimeout { get; set; }

            [XmlElement(ElementName = "FrameQueueSize")]
            public FrameQueueSize FrameQueueSize { get; set; }

            [XmlElement(ElementName = "IsCopyImagesInMotionsFrames")]
            public IsCopyImagesInMotionsFrames IsCopyImagesInMotionsFrames { get; set; }

            [XmlElement(ElementName = "BlockByPrevDecisionMode")]
            public BlockByPrevDecisionMode BlockByPrevDecisionMode { get; set; }

            [XmlElement(ElementName = "BlockUp")]
            public BlockUp BlockUp { get; set; }

            [XmlElement(ElementName = "BlockDown")]
            public BlockDown BlockDown { get; set; }

            [XmlElement(ElementName = "BlockUndefined")]
            public BlockUndefined BlockUndefined { get; set; }

            [XmlElement(ElementName = "FrozenBlockEnabled")]
            public FrozenBlockEnabled FrozenBlockEnabled { get; set; }

            [XmlElement(ElementName = "FrozenCounter")]
            public FrozenCounter FrozenCounter { get; set; }

            [XmlElement(ElementName = "FrozenPlateZoneIntersection")]
            public FrozenPlateZoneIntersection FrozenPlateZoneIntersection { get; set; }

            [XmlElement(ElementName = "Composite")]
            public Composite Composite { get; set; }

            [XmlElement(ElementName = "Convenient")]
            public Convenient Convenient { get; set; }

            [XmlElement(ElementName = "MotionDetection")]
            public MotionDetection MotionDetection { get; set; }

            [XmlElement(ElementName = "VehicleDetection")]
            public VehicleDetection VehicleDetection { get; set; }

            [XmlElement(ElementName = "PlateLocalization")]
            public PlateLocalization PlateLocalization { get; set; }

            [XmlElement(ElementName = "PlateRecognition")]
            public PlateRecognition PlateRecognition { get; set; }

            [XmlElement(ElementName = "DecisionMaking")]
            public DecisionMaking DecisionMaking { get; set; }

            [XmlElement(ElementName = "VehicleTypeClassification")]
            public object VehicleTypeClassification { get; set; }

            [XmlElement(ElementName = "FramesForEntry")]
            public FramesForEntry FramesForEntry { get; set; }

            [XmlElement(ElementName = "FramesWithPlateForEntry")]
            public FramesWithPlateForEntry FramesWithPlateForEntry { get; set; }

            [XmlElement(ElementName = "FramesForExit")]
            public FramesForExit FramesForExit { get; set; }

            [XmlElement(ElementName = "FramesWithoutPlateForExit")]
            public FramesWithoutPlateForExit FramesWithoutPlateForExit { get; set; }
        }

        [XmlRoot(ElementName = "CyclicReconnection")]
        public class CyclicReconnection
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "CyclicReconnectionTimeout")]
        public class CyclicReconnectionTimeout
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "Reconnection")]
        public class Reconnection
        {

            [XmlAttribute(AttributeName = "Value")]
            public bool Value { get; set; }
        }

        [XmlRoot(ElementName = "UpdateMatrixTimeout")]
        public class UpdateMatrixTimeout
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "ReconnectionTimeout")]
        public class ReconnectionTimeout
        {

            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "ReconnectionFeature")]
        public class ReconnectionFeature
        {

            [XmlElement(ElementName = "CyclicReconnection")]
            public CyclicReconnection CyclicReconnection { get; set; }

            [XmlElement(ElementName = "CyclicReconnectionTimeout")]
            public CyclicReconnectionTimeout CyclicReconnectionTimeout { get; set; }

            [XmlElement(ElementName = "Reconnection")]
            public Reconnection Reconnection { get; set; }

            [XmlElement(ElementName = "UpdateMatrixTimeout")]
            public UpdateMatrixTimeout UpdateMatrixTimeout { get; set; }

            [XmlElement(ElementName = "ReconnectionTimeout")]
            public ReconnectionTimeout ReconnectionTimeout { get; set; }
        }

        [XmlRoot(ElementName = "Features")]
        public class Features
        {
            [XmlElement(ElementName = "ReconnectionFeature")]
            public ReconnectionFeature ReconnectionFeature { get; set; }
        }

        [XmlRoot(ElementName = "VideoChannel")]
        public class VideoChannel
        {

            [XmlElement(ElementName = "ChannelName")]
            public ChannelName ChannelName { get; set; }

            [XmlElement(ElementName = "VehicleDirections")]
            public VehicleDirections VehicleDirections { get; set; }

            [XmlElement(ElementName = "SurveillanceOnly")]
            public SurveillanceOnly SurveillanceOnly { get; set; }

            [XmlElement(ElementName = "ColorMode")]
            public ColorMode ColorMode { get; set; }

            [XmlElement(ElementName = "VehicleDirectionNames")]
            public VehicleDirectionNames VehicleDirectionNames { get; set; }

            [XmlElement(ElementName = "VideoSourceType")]
            public VideoSourceType VideoSourceType { get; set; }

            [XmlElement(ElementName = "VideoSource")]
            public VideoSource VideoSource { get; set; }

            [XmlElement(ElementName = "ProcessingDelay")]
            public ProcessingDelay ProcessingDelay { get; set; }

            [XmlElement(ElementName = "ImageTransforms")]
            public ImageTransforms ImageTransforms { get; set; }

            [XmlElement(ElementName = "ProcessUnit")]
            public ProcessUnit ProcessUnit { get; set; }

            [XmlElement(ElementName = "Features")]
            public Features Features { get; set; }

            [XmlElement(ElementName = "LinkedChannels")]
            public object LinkedChannels { get; set; }

            [XmlAttribute(AttributeName = "Id")]
            public int Id { get; set; }
        }

        [XmlRoot(ElementName = "VideoChannels")]
        public class VideoChannels
        {

            [XmlElement(ElementName = "VideoChannel")]
            public List<VideoChannel> VideoChannel { get; set; }
        }

        [XmlRoot(ElementName = "Kernel")]
        public class Kernel
        {

            [XmlElement(ElementName = "Models")]
            public Models Models { get; set; }

            [XmlElement(ElementName = "Decisions")]
            public Decisions Decisions { get; set; }

            [XmlElement(ElementName = "VideoSourceProviders")]
            public object VideoSourceProviders { get; set; }

            [XmlElement(ElementName = "VideoChannels")]
            public VideoChannels VideoChannels { get; set; }
        }


    }
}
