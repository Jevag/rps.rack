﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace stoyka01.Recognition
{
    public class Am_Core
    {
        public static AM_Xml2.Kernel Set_Kernel;

        public static int SourceCount=1;

        //Здесь будут пабликами сменяемые настройки...
        //public static bool isRecognized = false;
        public static bool BadMln = false;


        public static string SetPath = @"c:\ProgramData\Mallenom\Recar2.Kernel.Service\Configuration\recar2.kernel.cfg";
        public static string ServName = "recar2.kernel.service";
        public static string ServName2 = "AutoMarshal.SDK.Service";

        public static string LocalPath;

        public static HttpListener httpListener;
        public static int MalPort = 9595;
        public static int ReceiverPort = 49999;
        public static string MalHost = "http://127.0.0.1";
        public static string ReceiverHost = "http://127.0.0.1";
        public static string CurrentSubscription;
        //public static List<string> SubsList;
        public static string CurrentToken; //токен обработки!

        //public static byte[] SnapShotLoopA;
        //public static byte[] SnapShotResult;

        public static bool isRecognized = false;
        public static string BruttoResult = "";
        public static string NettoResult = "";

        public static System.Windows.Forms.Timer tmrRet;

        public static byte[] SnapShotLoopA;
        public static byte[] SnapShotResult;

        public static byte[] FinalImage;

        public static byte[] OperatorImage; //{ get; set; }

        public static string plate = "";

        public static bool RecStarted = false;

        public static bool PlateDefined = false;



        #region StartStopService

        public static bool IsMlnServiceLaunched()
        {
            try
            {
                ServiceController ServiceCtrl = new ServiceController(ServName2);
                Thread.Sleep(300);
                if (ServiceCtrl.Status.Equals(ServiceControllerStatus.Stopped) | ServiceCtrl.Status.Equals(ServiceControllerStatus.StopPending))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch
            {
                //RunBdOk = false;
                Logging.ToLog("Не могу проверить запущенность службы Mallenom");
                return false;
            }
        }

        public static bool RestartAM2(int timeoutMilliseconds)
        {

            try
            {
                ServiceController service = new ServiceController(ServName2);

                Logging.ToLog("Остановка службы Mallenom");

                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            }
            catch (Exception exx2)
            {
                Logging.ToLog("Служба уже остановлена");
            }


            try
            {
                //Logging.ToLog("Before Update");
                UpdateXML();
                Thread.Sleep(500);
                //Logging.ToLog("After Update");
                //Logging.ToLog(LocalPath + "---" + SetPath);
                File.Copy(LocalPath, SetPath,true);
                

                // count the rest of the timeout
                //int millisec2 = Environment.TickCount;
                //timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                //service.Start();
                //service.WaitForStatus(ServiceControllerStatus.Running, timeout);

                //Logging.ToLog("Старт службы Mallenom");

                return true;
            }
            catch (Exception exx)
            {
                Logging.ToLog(exx.Message);
                //Logging.ToLog("Ошибка перезапуска службы Mallenom");
                return false;
            }
        }


        public static bool RestartAM(int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(ServName);
            try
            {
                Logging.ToLog("Выключение службы Mallenom");

                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                UpdateXML();
                Thread.Sleep(200);

                // count the rest of the timeout
                int millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);

                Logging.ToLog("Старт службы Mallenom");

                return true;
            }
            catch
            {
                Logging.ToLog("Ошибка перезапуска службы Mallenom");
                return false;
            }
        }

        public static void StartAM()
        {
            try
            {
                using (ServiceController service = new ServiceController(ServName))
                {
                    service.Start();
                }
            }
            catch (Exception ex)
            {
                Logging.ToLog("Ошибка старта службы Малленом " + ex.Message);
            }
        }

        public void StopAM()
        {
            try
            {
                using (ServiceController service = new ServiceController(ServName))
                {
                    service.Stop();
                }
            }
            catch (Exception ex)
            {
                Logging.ToLog("Ошибка остановки службы Малленом " + ex.Message);
            }
        }
        #endregion

        #region Converters
        //Таких букв только 12 — А, В, Е, К, М, Н, О, Р, С, Т, У, Х

        public static string LatToCyr(string plate)
        {
            string retval = "";
            for (int i = 0; i < plate.Length; i++)
            {
                string myletter = plate.Substring(i, 1);
                switch (myletter)
                {
                    case "A":
                        retval += "А";
                        break;
                    case "B":
                        retval += "В";
                        break;
                    case "E":
                        retval += "Е";
                        break;
                    case "K":
                        retval += "К";
                        break;
                    case "M":
                        retval += "М";
                        break;
                    case "H":
                        retval += "Н";
                        break;
                    case "O":
                        retval += "О";
                        break;
                    case "P":
                        retval += "Р";
                        break;
                    case "C":
                        retval += "С";
                        break;
                    case "T":
                        retval += "Т";
                        break;
                    case "Y":
                        retval += "У";
                        break;
                    case "X":
                        retval += "Х";
                        break;
                    default:
                        retval += myletter;
                        break;
                }
            }
            return retval;
        }


        public static double DoubleFromString(string arg)
        {
            if (arg != null)
            {
                Debug.Print(arg);
                char a = Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                string separ = Convert.ToString(a);

                //убираем запятые тысячных разрядов...
                arg = arg.Replace("\"", "");
                Debug.Print(arg);
                if (separ == ",")
                {
                    return Convert.ToDouble(arg.Replace(".", ","));
                }
                else
                {
                    return Convert.ToDouble(arg);
                }
            }
            else return 0;
        }

        public static double ConvertFromXMLValue(string arg)
        {
            //<ProcessingDelay Value="PT2S" />
            string upd_arg = arg.Replace("PT", "");
            upd_arg = upd_arg.Replace("S", "");
            return DoubleFromString(upd_arg);
        }

        public static string ConvertToXMLValue(double arg)
        {
            string upd_arg = arg.ToString().Replace(',', '.');
            return "PT" + upd_arg + "S";
        }

        #endregion

        public static void UpdateFromPath()
        {
            LocalPath = Application.StartupPath + "\\kernel.cfg";

            string XmlText = "";
            using (StreamReader reader = File.OpenText(LocalPath))
            {
                XmlText = reader.ReadToEnd();
            }

            XmlSerializer serializer = new XmlSerializer(typeof(AM_Xml2.Kernel));
            using (StringReader reader = new StringReader(XmlText))
            {
                //Debug.Print(reader.ToString());
                //try
                //{
                    Set_Kernel = (AM_Xml2.Kernel)serializer.Deserialize(reader);
                //}
                //catch (Exception ex) {}
                    //Logging.ToLog(Set_Kernel.Models.Model.Stencils.Stencil[3].Id);
                //SourceCount = Set_Kernel.VideoChannels.VideoChannel.Count;
                //Читаем настройки с первой!!!
                //Debug.Print(SourceCount.ToString());
                AM_Xml2.VideoChannel ch1 = Set_Kernel.VideoChannels.VideoChannel[0];
                GlobalClass.MlnConString_1 = ch1.VideoSource.StreamUrl.Value;
                //GlobalClass.MlnPreprocess_1 = ConvertFromXMLValue(ch1.ProcessingDelay.Value);
                //GlobalClass.MlnAngle_1 =(double) ch1.ImageTransforms.Spatial.Affine.Rotate.Angle.Value;
                GlobalClass.MlnMinSizeW_1 = ch1.ProcessUnit.Convenient.RealPlateSize1.Width;
                GlobalClass.MlnMinSizeH_1 = ch1.ProcessUnit.Convenient.RealPlateSize1.Height;
                GlobalClass.MlnMaxSizeW_1 = ch1.ProcessUnit.Convenient.RealPlateSize2.Width;
                GlobalClass.MlnMaxSizeH_1 = ch1.ProcessUnit.Convenient.RealPlateSize2.Height;
                //ROI
                GlobalClass.MlnPointX1_1 = ch1.ProcessUnit.Convenient.Roi.Point[0].X;
                GlobalClass.MlnPointX2_1 = ch1.ProcessUnit.Convenient.Roi.Point[1].X;
                GlobalClass.MlnPointX3_1 = ch1.ProcessUnit.Convenient.Roi.Point[2].X;
                GlobalClass.MlnPointX4_1 = ch1.ProcessUnit.Convenient.Roi.Point[3].X;
                GlobalClass.MlnPointX5_1 = ch1.ProcessUnit.Convenient.Roi.Point[4].X;
                GlobalClass.MlnPointX6_1 = ch1.ProcessUnit.Convenient.Roi.Point[5].X;
                GlobalClass.MlnPointX7_1 = ch1.ProcessUnit.Convenient.Roi.Point[6].X;
                GlobalClass.MlnPointX8_1 = ch1.ProcessUnit.Convenient.Roi.Point[7].X;

                GlobalClass.MlnPointY1_1 = ch1.ProcessUnit.Convenient.Roi.Point[0].Y;
                GlobalClass.MlnPointY2_1 = ch1.ProcessUnit.Convenient.Roi.Point[1].Y;
                GlobalClass.MlnPointY3_1 = ch1.ProcessUnit.Convenient.Roi.Point[2].Y;
                GlobalClass.MlnPointY4_1 = ch1.ProcessUnit.Convenient.Roi.Point[3].Y;
                GlobalClass.MlnPointY5_1 = ch1.ProcessUnit.Convenient.Roi.Point[4].Y;
                GlobalClass.MlnPointY6_1 = ch1.ProcessUnit.Convenient.Roi.Point[5].Y;
                GlobalClass.MlnPointY7_1 = ch1.ProcessUnit.Convenient.Roi.Point[6].Y;
                GlobalClass.MlnPointY8_1 = ch1.ProcessUnit.Convenient.Roi.Point[7].Y;

                GlobalClass.MlnDistorsion = (int) ch1.ImageTransforms.Spatial.CorrectDistortion.K1.Value;

                GlobalClass.MlnVertPers =(int) ch1.ImageTransforms.Spatial.VerticalPerspective.Vp.Value;
                GlobalClass.MlnHorPers = (int)ch1.ImageTransforms.Spatial.HorizontalPerspective.Hp.Value;

                //Добавим угол
                /*
                 <ImageTransforms>
<Spatial>
<CorrectDistortion IsEnabled="false">
            <K1 Value="-28.40" /><!--значения в % от -100% до 100%.-->
            <K2 Value="0" />
          </CorrectDistortion>
                 */


                SourceCount = 1;

                //2 камерка
                /*
                if (SourceCount > 1)
                {
                    AM_Xml.VideoChannel ch2 = Set_Kernel.VideoChannels.VideoChannel[1];
                    GlobalClass.MlnConString_2 = ch2.VideoSource.StreamUrl.Value;
                    GlobalClass.MlnPreprocess_2 = ConvertFromXMLValue(ch2.ProcessingDelay.Value);
                    GlobalClass.MlnAngle_2 = (double)ch2.ImageTransforms.Spatial.Affine.Rotate.Angle.Value;
                    GlobalClass.MlnMinSizeW_2 = Convert.ToString(ch2.ProcessUnit.Convenient.RealPlateSize1.Width);
                    GlobalClass.MlnMinSizeH_2 = Convert.ToString(ch2.ProcessUnit.Convenient.RealPlateSize1.Height);
                    GlobalClass.MlnMaxSizeW_2 = Convert.ToString(ch2.ProcessUnit.Convenient.RealPlateSize2.Width);
                    GlobalClass.MlnMaxSizeH_2 = Convert.ToString(ch2.ProcessUnit.Convenient.RealPlateSize2.Height);
                    //ROI
                    GlobalClass.MlnPointX1_2 = ch2.ProcessUnit.Convenient.Roi.Point[0].X;
                    GlobalClass.MlnPointX2_2 = ch2.ProcessUnit.Convenient.Roi.Point[1].X;
                    GlobalClass.MlnPointX3_2 = ch2.ProcessUnit.Convenient.Roi.Point[2].X;
                    GlobalClass.MlnPointX4_2 = ch2.ProcessUnit.Convenient.Roi.Point[3].X;
                    GlobalClass.MlnPointX5_2 = ch2.ProcessUnit.Convenient.Roi.Point[4].X;
                    GlobalClass.MlnPointX6_2 = ch2.ProcessUnit.Convenient.Roi.Point[5].X;
                    GlobalClass.MlnPointX7_2 = ch2.ProcessUnit.Convenient.Roi.Point[6].X;
                    GlobalClass.MlnPointX8_2 = ch2.ProcessUnit.Convenient.Roi.Point[7].X;

                    GlobalClass.MlnPointY1_2 = ch2.ProcessUnit.Convenient.Roi.Point[0].Y;
                    GlobalClass.MlnPointY2_2 = ch2.ProcessUnit.Convenient.Roi.Point[1].Y;
                    GlobalClass.MlnPointY3_2 = ch2.ProcessUnit.Convenient.Roi.Point[2].Y;
                    GlobalClass.MlnPointY4_2 = ch2.ProcessUnit.Convenient.Roi.Point[3].Y;
                    GlobalClass.MlnPointY5_2 = ch2.ProcessUnit.Convenient.Roi.Point[4].Y;
                    GlobalClass.MlnPointY6_2 = ch2.ProcessUnit.Convenient.Roi.Point[5].Y;
                    GlobalClass.MlnPointY7_2 = ch2.ProcessUnit.Convenient.Roi.Point[6].Y;
                    GlobalClass.MlnPointY8_2 = ch2.ProcessUnit.Convenient.Roi.Point[7].Y;
                }
                */
                //Debug.Print("moveIT");
            }
        }

        public static void UpdateXML()
        {
            LocalPath = Application.StartupPath + "\\kernel.cfg";
            //Читаем настройки!
            string XmlText = "";
            using (StreamReader reader = File.OpenText(LocalPath))
            {
                XmlText = reader.ReadToEnd();
            }

            XmlSerializer serializer = new XmlSerializer(typeof(AM_Xml2.Kernel));
            using (StringReader reader = new StringReader(XmlText))
            {
                //Debug.Print(reader.ToString());

                Set_Kernel = (AM_Xml2.Kernel)serializer.Deserialize(reader);
            }
                //Logging.ToLog(Set_Kernel.Models.Model.Stencils.Stencil[3].Id);
                SourceCount = Set_Kernel.VideoChannels.VideoChannel.Count;
                //Читаем настройки с первой!!!
                //Debug.Print(SourceCount.ToString());
                AM_Xml2.VideoChannel ch1 = Set_Kernel.VideoChannels.VideoChannel[0];
                //Обратка!

                ch1.VideoSource.StreamUrl.Value= GlobalClass.MlnConString_1;
                //ch1.ProcessingDelay.Value= ConvertToXMLValue(GlobalClass.MlnPreprocess_1);
            /*
            if (GlobalClass.MlnAngle_1 > 0)
            {
                ch1.ImageTransforms.Spatial.Affine.Rotate.IsEnabled = true;
            }
            else
            {
                ch1.ImageTransforms.Spatial.Affine.Rotate.IsEnabled = false;
            }
            
            ch1.ImageTransforms.Spatial.Affine.Rotate.Angle.Value=(int) GlobalClass.MlnAngle_1;
            ch1.ProcessUnit.Convenient.RealPlateSize1.Width = Convert.ToDouble(GlobalClass.MlnMinSizeW_1);
            ch1.ProcessUnit.Convenient.RealPlateSize1.Height = Convert.ToDouble(GlobalClass.MlnMinSizeH_1);
            ch1.ProcessUnit.Convenient.RealPlateSize2.Width = Convert.ToDouble(GlobalClass.MlnMaxSizeW_1);
            ch1.ProcessUnit.Convenient.RealPlateSize2.Height= Convert.ToDouble(GlobalClass.MlnMaxSizeH_1);
            */

            //ROI
            ch1.ProcessUnit.Convenient.Roi.Point[0].X = GlobalClass.MlnPointX1_1;
            ch1.ProcessUnit.Convenient.Roi.Point[1].X = GlobalClass.MlnPointX2_1;
            ch1.ProcessUnit.Convenient.Roi.Point[2].X = GlobalClass.MlnPointX3_1;
            ch1.ProcessUnit.Convenient.Roi.Point[3].X = GlobalClass.MlnPointX4_1;
            ch1.ProcessUnit.Convenient.Roi.Point[4].X = GlobalClass.MlnPointX5_1;
            ch1.ProcessUnit.Convenient.Roi.Point[5].X = GlobalClass.MlnPointX6_1;
            ch1.ProcessUnit.Convenient.Roi.Point[6].X = GlobalClass.MlnPointX7_1;
            ch1.ProcessUnit.Convenient.Roi.Point[7].X = GlobalClass.MlnPointX8_1;

            ch1.ProcessUnit.Convenient.Roi.Point[0].Y = GlobalClass.MlnPointY1_1;
            ch1.ProcessUnit.Convenient.Roi.Point[1].Y = GlobalClass.MlnPointY2_1;
            ch1.ProcessUnit.Convenient.Roi.Point[2].Y = GlobalClass.MlnPointY3_1;
            ch1.ProcessUnit.Convenient.Roi.Point[3].Y = GlobalClass.MlnPointY4_1;
            ch1.ProcessUnit.Convenient.Roi.Point[4].Y = GlobalClass.MlnPointY5_1;
            ch1.ProcessUnit.Convenient.Roi.Point[5].Y = GlobalClass.MlnPointY6_1;
            ch1.ProcessUnit.Convenient.Roi.Point[6].Y = GlobalClass.MlnPointY7_1;
            ch1.ProcessUnit.Convenient.Roi.Point[7].Y = GlobalClass.MlnPointY8_1;
            //ch1.ProcessUnit.MotionDetectMethod.Value = "ExternalPlugin";

            /*
            if (GlobalClass.MlnHorPers == 100)
            {
                ch1.ImageTransforms.Spatial.HorizontalPerspective.IsEnabled = false;
            }
            else
            {
                ch1.ImageTransforms.Spatial.HorizontalPerspective.IsEnabled = true;
            }

            if (GlobalClass.MlnVertPers == 100)
            {
                ch1.ImageTransforms.Spatial.VerticalPerspective.IsEnabled = false;
            }
            else
            {
                ch1.ImageTransforms.Spatial.VerticalPerspective.IsEnabled = true;
            }
            */
            //ch1.ImageTransforms.Spatial.Affine.Rotate.Angle.Value = (int)GlobalClass.MlnAngle_1;
            ch1.ProcessUnit.Convenient.RealPlateSize1.Width = Convert.ToDouble(GlobalClass.MlnMinSizeW_1);
            ch1.ProcessUnit.Convenient.RealPlateSize1.Height = Convert.ToDouble(GlobalClass.MlnMinSizeH_1);
            ch1.ProcessUnit.Convenient.RealPlateSize2.Width = Convert.ToDouble(GlobalClass.MlnMaxSizeW_1);
            ch1.ProcessUnit.Convenient.RealPlateSize2.Height = Convert.ToDouble(GlobalClass.MlnMaxSizeH_1);


            ch1.ImageTransforms.Spatial.HorizontalPerspective.Hp.Value = GlobalClass.MlnHorPers;
            ch1.ImageTransforms.Spatial.VerticalPerspective.Vp.Value = GlobalClass.MlnVertPers;

            ch1.ImageTransforms.Spatial.CorrectDistortion.K1.Value = GlobalClass.MlnDistorsion;

            Set_Kernel.VideoChannels.VideoChannel[0] = ch1;

            /*
                             GlobalClass.MlnMinSizeW_1 = ch1.ProcessUnit.Convenient.RealPlateSize1.Width;
                GlobalClass.MlnMinSizeH_1 = ch1.ProcessUnit.Convenient.RealPlateSize1.Height;
                GlobalClass.MlnMaxSizeW_1 = ch1.ProcessUnit.Convenient.RealPlateSize2.Width;
                GlobalClass.MlnMaxSizeH_1 = ch1.ProcessUnit.Convenient.RealPlateSize2.Height;
             */

            //Logging.ToLog("upd1");

            SourceCount = 1;

            //2 камерка
            /*
            if (SourceCount > 1)
                {
                AM_Xml2.VideoChannel ch2 = Set_Kernel.VideoChannels.VideoChannel[1];
                //Обратка!

                ch2.VideoSource.StreamUrl.Value = GlobalClass.MlnConString_2;
                ch2.ProcessingDelay.Value = ConvertToXMLValue(GlobalClass.MlnPreprocess_2);
                if (GlobalClass.MlnAngle_2 > 0)
                {
                    ch2.ImageTransforms.Spatial.Affine.Rotate.IsEnabled = true;
                }
                else
                {
                    ch2.ImageTransforms.Spatial.Affine.Rotate.IsEnabled = false;
                }
                ch2.ImageTransforms.Spatial.Affine.Rotate.Angle.Value = (int)GlobalClass.MlnAngle_2;
                ch2.ProcessUnit.Convenient.RealPlateSize1.Width = Convert.ToDouble(GlobalClass.MlnMinSizeW_2);
                ch2.ProcessUnit.Convenient.RealPlateSize1.Height = Convert.ToDouble(GlobalClass.MlnMinSizeH_2);
                ch2.ProcessUnit.Convenient.RealPlateSize2.Width = Convert.ToDouble(GlobalClass.MlnMaxSizeW_2);
                ch2.ProcessUnit.Convenient.RealPlateSize2.Height = Convert.ToDouble(GlobalClass.MlnMaxSizeH_2);

                //ROI
                ch2.ProcessUnit.Convenient.Roi.Point[0].X = GlobalClass.MlnPointX1_2;
                ch2.ProcessUnit.Convenient.Roi.Point[1].X = GlobalClass.MlnPointX2_2;
                ch2.ProcessUnit.Convenient.Roi.Point[2].X = GlobalClass.MlnPointX3_2;
                ch2.ProcessUnit.Convenient.Roi.Point[3].X = GlobalClass.MlnPointX4_2;
                ch2.ProcessUnit.Convenient.Roi.Point[4].X = GlobalClass.MlnPointX5_2;
                ch2.ProcessUnit.Convenient.Roi.Point[5].X = GlobalClass.MlnPointX6_2;
                ch2.ProcessUnit.Convenient.Roi.Point[6].X = GlobalClass.MlnPointX7_2;
                ch2.ProcessUnit.Convenient.Roi.Point[7].X = GlobalClass.MlnPointX8_2;

                ch2.ProcessUnit.Convenient.Roi.Point[0].Y = GlobalClass.MlnPointY1_2;
                ch2.ProcessUnit.Convenient.Roi.Point[1].Y = GlobalClass.MlnPointY2_2;
                ch2.ProcessUnit.Convenient.Roi.Point[2].Y = GlobalClass.MlnPointY3_2;
                ch2.ProcessUnit.Convenient.Roi.Point[3].Y = GlobalClass.MlnPointY4_2;
                ch2.ProcessUnit.Convenient.Roi.Point[4].Y = GlobalClass.MlnPointY5_2;
                ch2.ProcessUnit.Convenient.Roi.Point[5].Y = GlobalClass.MlnPointY6_2;
                ch2.ProcessUnit.Convenient.Roi.Point[6].Y = GlobalClass.MlnPointY7_2;
                ch2.ProcessUnit.Convenient.Roi.Point[7].Y = GlobalClass.MlnPointY8_2;
                ch2.ProcessUnit.MotionDetectMethod.Value = "ExternalPlugin";

                Set_Kernel.VideoChannels.VideoChannel[1] = ch2;
            }
            */
            //Stream fs = new FileStream("c:\\Rack\\Mallenom\\SetTest.xml", FileMode.Create);

            using (var writer = new StreamWriter(LocalPath))
            using (var xmlWriter = XmlWriter.Create(writer, new XmlWriterSettings { Indent = true }))
            {
                serializer.Serialize(xmlWriter, Set_Kernel);
            }

            Logging.ToLog("FullUPD");

            /*
            using (var xmlWriter = XmlWriter.Create(writer, new XmlWriterSettings { Indent = false }))
            {
                serializer.Serialize(xmlWriter, myObject);
            }
            */

            //XmlWriter writer = new XmlTextWriter(fs, Encoding.UTF8);
            //XmlWriterSettings set = new XmlWriterSettings { Indent = false };
            //writer.Settings = set;
            // Serialize using the XmlTextWriter.
            //serializer.Serialize(writer, Set_Kernel);
            //writer.Close();
        }

        //не будем использовать.
        public void LoadDef()
        {
            string XmlText = "";
            string def_xml = Application.StartupPath + "\\am_def.xml";
            using (StreamReader reader = File.OpenText(def_xml))
            {
                XmlText = reader.ReadToEnd();
            }

            XmlSerializer serializer = new XmlSerializer(typeof(AM_Xml.Kernel));
            using (StringReader reader = new StringReader(XmlText))
            {
                //Debug.Print(reader.ToString());

                Set_Kernel = (AM_Xml2.Kernel)serializer.Deserialize(reader);
                //Logging.ToLog(Set_Kernel.Models.Model.Stencils.Stencil[3].Id);
            }
        }

        #region MainFunctions
        public static string ParseNetto(string arg)
        {
            try
            {
                int pos1 = arg.IndexOf("{", 0);
                int pos2 = arg.IndexOf("---", pos1);
                //Debug.Print(pos1.ToString() + " " + pos2.ToString());
                string ret = arg.Substring(pos1, pos2 - pos1);
                return ret;
            }
            catch (Exception ex)
            {
                Logging.ToLog("Некорректный результат: (аргумент)" + arg + " " + ex.Message);
                return "";
            }

        }

        public static void SetCallBack()
        {
            try
            {
                var req = WebRequest.CreateHttp("http://127.0.0.1:9595/api/v1/subscriptions");
                req.Method = "POST";
                req.Accept = "*/*";
                req.ContentType = "application/json";

                var filter = new CallBackFilter();
                filter.CallBack.Address = "http://127.0.0.1:49999";
                filter.Type = "recognition";

                var serObj = JsonConvert.SerializeObject(filter);
                var body = Encoding.UTF8.GetBytes(serObj);
                req.ContentLength = body.Length;

                using (var sw = req.GetRequestStream())
                {
                    sw.Write(body, 0, body.Length);
                }
            }
            catch (Exception ex)
            {
                Logging.ToLog("Невозможно соединиться со службой Малленом. Указанные порты закрыты. Требуется перезапустить службу.");
                //MessageBox.Show("Невозможно соединиться со службой Малленом. Указанные порты закрыты. Требуется перезапустить службу.");
            }
        }

        public static string CreateNewSubscription()
        {
            try
            {
                CurrentSubscription = "";
                var req = WebRequest.CreateHttp("http://localhost:9595//api/v1/subscriptions/new");
                req.ContentType = "application/json";
                req.Method = "POST";
                req.Accept = "*/*";

                var jsonBody = new SubscriptionJson();
                jsonBody.CallBack.Address = "http://127.0.0.1:49999/";
                jsonBody.CallBack.Method = "POST";
                jsonBody.Type = "recognition";

                var filter = new FilterProperty
                {
                    Channels = new int[] { 0 },
                    DecisionComposition = new string[] { "Plate", "Channel" },
                    ImagesComposition = new string[] { },
                };

                jsonBody.Filter = JsonConvert.SerializeObject(filter);

                string serObj = JsonConvert.SerializeObject(jsonBody);
                Debug.Print(serObj);

                var body = Encoding.UTF8.GetBytes(serObj);
                req.ContentLength = body.Length;

                using (var sw = req.GetRequestStream())
                {
                    sw.Write(body, 0, body.Length);
                }

                using (var resp = (HttpWebResponse)req.GetResponse())
                using (var respStream = resp.GetResponseStream())


                using (var sr = new StreamReader(respStream))
                {
                    Debug.Print(resp.StatusCode.ToString());
                    //Debug.Print(sr.ReadToEnd());
                    string ResJson = sr.ReadToEnd();
                    Subscription sub = JsonConvert.DeserializeObject<Subscription>(ResJson);
                    CurrentSubscription = sub.id;
                    Debug.Print(CurrentSubscription);
                }
                return CurrentSubscription;
            }
            catch (Exception ex)
            {
                Logging.ToLog("Невозможно соединиться со службой Малленом. Указанные порты закрыты. Требуется перезапустить службу.");
                //MessageBox.Show("Невозможно соединиться со службой Малленом. Указанные порты закрыты. Требуется перезапустить службу.");
                return "";
            }
        }

        //Получить все подписки...
        public static async Task<List<string>> ListAllSubscriptions()
        {
            HttpClient httpClient = new HttpClient();
            string endpoint = "http://localhost:9595//api/v1/subscriptions";
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //StringContent content = new StringContent(jsonstring, Encoding.UTF8, "application/json");
            HttpResponseMessage Response = await httpClient.GetAsync(endpoint);
            string ResultJson = await Response.Content.ReadAsStringAsync();
            Debug.WriteLine(ResultJson);
            List<string> SubsList = new List<string>();
            List<Subscription> subs = JsonConvert.DeserializeObject<List<Subscription>>(ResultJson);
            foreach (Subscription sub in subs)
            {
                Debug.Print(sub.id);
                SubsList.Add(sub.id);
            }
            //тут вывести в листочек!
            return SubsList;
        }

        //Удалить одну подписку!
        public static async Task DeleteSubByID(string sub)
        {
            HttpClient httpClient = new HttpClient();

            string endpoint = "http://localhost:9595//api/v1/subscriptions/subscription" + "?id=" + sub;
            //string query = "id=" + sub;
            //Debug.Print(query);
            HttpResponseMessage Response = null;

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri(endpoint),
                Content = new StringContent("", Encoding.UTF8, "application/json")
            };
            var ResponseTask = httpClient.SendAsync(request);
            Response = await ResponseTask;
            string ResultJson = await Response.Content.ReadAsStringAsync();
            Debug.WriteLine(ResultJson); //Сделать обработку ошибок всех!!!
        }

        //Удалить все подписки
        public static async Task DeleteAllSubs()
        {
            try
            {
                List<string> SubsList = await ListAllSubscriptions();
                foreach (string sub in SubsList)
                {
                    await DeleteSubByID(sub);
                }
            }
            catch (Exception ex) {
                Logging.ToLog(ex.Message);
            }
        }

        //Старт распознавания
        //HTTP GET /api/v1/kernel/processing/start? chId =[ChId]
        public static async Task StartRec()
        {
            //TODO: сделать грамотную обработку Exception
            try
            {
                HttpClient httpClient = new HttpClient();
                string endpoint = "http://localhost:9595//api/v1/kernel/processing/start?chId=0";
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //StringContent content = new StringContent(jsonstring, Encoding.UTF8, "application/json");
                HttpResponseMessage Response = await httpClient.GetAsync(endpoint);
                string ResultJson = await Response.Content.ReadAsStringAsync();
                //Debug.WriteLine(ResultJson);
            }
            catch (Exception ex)
            {
                BadMln = true;
                Logging.ToLog(ex.Message);
            }
        }

        //Стоп распознавания
        ///api/v1/kernel/processing/stop?token=[Token]&delay=[Ms]&force=[Force]
        public static async Task StopRec(string token)
        {
            HttpClient httpClient = new HttpClient();
            string endpoint = "http://localhost:9595//api/v1/kernel/processing/stop?token=" + token;
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //StringContent content = new StringContent(jsonstring, Encoding.UTF8, "application/json");
            HttpResponseMessage Response = await httpClient.GetAsync(endpoint);
            string ResultJson = await Response.Content.ReadAsStringAsync();
            //Debug.WriteLine(ResultJson);
        }

        //Список токенов распознавания
        ////api/v1/kernel/processing/tokens
        public static async Task<List<string>> ListRecTokens()
        {
            //TODO: видимо невозможно получить список токенов, либо неправильная конвертация
            HttpClient httpClient = new HttpClient();
            string endpoint = "http://localhost:9595//api/v1/kernel/processing/tokens";
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //StringContent content = new StringContent(jsonstring, Encoding.UTF8, "application/json");
            HttpResponseMessage Response = await httpClient.GetAsync(endpoint);
            string ResultJson = await Response.Content.ReadAsStringAsync();
            //Debug.Print(ResultJson);
            return JsonConvert.DeserializeObject<List<string>>(ResultJson);
        }

        public static async Task StopAllTokens()
        {
            try
            {
                List<string> tokens = await ListRecTokens();
                foreach (string tkn in tokens)
                {
                    await StopRec(tkn);
                }
            }
            catch (Exception ex) { }
            //TODO: подумать, почему тут ошибка...Малленом, токены
        }

        //Берем снапшот с потока!
        ///api/v1/kernel/videochannels/image?id=[Id]&format=[Format]&timestamp=[Timestamp]
        public static async Task<byte[]> GetSnaphot()
        {
            try
            {
                HttpClient httpClient = new HttpClient();
                string endpoint = "http://localhost:9595//api/v1/kernel/videochannels/image?id=0";
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //StringContent content = new StringContent(jsonstring, Encoding.UTF8, "application/json");
                HttpResponseMessage Response = await httpClient.GetAsync(endpoint);
                byte[] image = await Response.Content.ReadAsByteArrayAsync();
                //Debug.Print(image.Length.ToString());
                return image;
            }
            catch (Exception ex)
            {
                BadMln = true;
                Logging.ToLog(ex.Message);
                //byte[] emptyimage= Array.Empty<byte>();
                return Array.Empty<byte>();
            }

            //string ResultData= await Response.Content.ReadAsStringAsync();
            //Debug.Print(ResultData);
            //return JsonConvert.DeserializeObject<List<string>>(ResultJson);
        }

        public static Bitmap ByteToImage(byte[] snap)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = snap;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }


        public static async Task ListenService()
        {
            try
            {
                httpListener = new HttpListener();
                httpListener.Prefixes.Add("http://127.0.0.1:49999/");
                httpListener.Start();
                HttpListenerContext x = await httpListener.GetContextAsync();
                var encoding = Encoding.UTF8;
                using (var responce = x.Response)
                using (var inputStream = x.Request.InputStream)
                using (var reader = new StreamReader(inputStream, encoding))
                {
                    var obj = reader.ReadToEnd();
                    BruttoResult = obj;
                    Debug.Print(obj);
                    //tmrRet.Enabled = true;
                    isRecognized = true;

                    //Debug.Print(obj); //его и парсим!
                    responce.Close();   //обязательно закрываем! Метод отправляет ответ!
                }
            }
            catch (Exception ex) { }
        }

        public static void StopListening()
        {
            try
            {
                httpListener.Stop();
            }
            catch (Exception ex) { }
        }

        public static async Task<bool> OperatorUploadImage(string RGuid, string strdate, string CurrUrl)
        {
            byte[] image;

            if (SnapShotResult != null) image = SnapShotResult;
            else image = SnapShotLoopA;

            using (var client = new HttpClient())
            {
                using (var content =
                new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                {
                    content.Add(new StreamContent(new MemoryStream(image)), "strdate", "upload.jpg"); //Дату + название

                    using (
                       var message =
                           await client.PostAsync(CurrUrl + "/api2/recognition/DeviceRecognition/new?guid=" + RGuid + "&type=picture&timestamp=" + strdate, content))
                    {
                        var input = await message.Content.ReadAsStringAsync();

                        return true;
                    }
                }
            }
        }

        #endregion

        #region Summary
        public static async Task<bool> StartMln()
        {
            //Logging.ToLog("start mln!");
            if (IsMlnServiceLaunched())
            {
                try
                {
                    PlateDefined = false;
                    RecStarted = true; //Не даем постоянно запускать
                    isRecognized = false;
                    SnapShotLoopA = await GetSnaphot(); //Здесь ошибка
                    FinalImage = SnapShotLoopA;
                    SnapShotResult = null;
                    tmrRet.Enabled = true; //???
                    BruttoResult = "";
                    NettoResult = "";
                    plate = "";
                    SetCallBack();
                    CreateNewSubscription();
                    await StartRec();
                    await ListenService();
                    return true;
                }
                catch (Exception ex)
                {
                    BadMln = true;
                    Logging.ToLog(ex.Message);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static async Task<bool> StopMln()
        {
            //Logging.ToLog("I'm stop mln!");
            if (IsMlnServiceLaunched())
            {
                RecStarted = false;
                try
                {
                    StopListening();
                }
                catch (Exception ex) {
                    return false;
                } //если вдруг закрыли

                await StopAllTokens();
                await DeleteAllSubs();
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void TimerInit()
        {
            tmrRet = new System.Windows.Forms.Timer();
            tmrRet.Interval = 100; //250...
            tmrRet.Tick += TmrRet_Tick;
            tmrRet.Enabled = false;
        }

        private static async void TmrRet_Tick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            //Распознали!
            if (isRecognized)
            {
                //лупим снапшот!
                //isRecognized = false;
                tmrRet.Enabled = false;
                await StopMln();
                SnapShotResult = await GetSnaphot(); //Снапшот с распознанным номером!
                FinalImage = SnapShotResult;
                NettoResult = ParseNetto(BruttoResult);
                Debug.Print(NettoResult);
                if (NettoResult != "")
                {
                    Result result = JsonConvert.DeserializeObject<Result>(NettoResult);
                    plate = result.plate.number; //Послать его в стойку!
                }
                else
                {
                    plate = "unknown";
                }

                PlateDefined = true;
            }
        }

        public static void SendPlateNumber(string PlateNumber)
        {
            try
            {
                HttpClient client = new HttpClient();

                client.GetAsync("http://" + GlobalClass.TwoLevelSendIP + ":2222/GetPlateNumber?LPR=" + PlateNumber);

            }
            catch (Exception ex)
            {
            }
        }

        //отправляем на мастер
        public static void SendTransactGuid(string guid)
        {
            try
            {
                HttpClient client = new HttpClient();

                client.GetAsync("http://" + GlobalClass.TwoLevelReceiveIP + ":2222/GetMlnSnapShot?guid=" + guid);
            }
            catch (Exception ex)
            {
            }
        }

        /*
        public static async Task<bool> OperatorDownloadImage(bool Listen)
        {
            string snap_path = "";

            //оператор
            if (!Listen)
            {
            }
            else
            {
                snap_path = "http://" + GlobalClass.TwoLevelReceiveIP + ":2222/GetMlnSnapshot?unique=";
            }

            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, snap_path + new Guid().ToString()))
                {
                    using (var response = await client.SendAsync(request))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            byte[] image = await response.Content.ReadAsByteArrayAsync();
                            if (image.Length > 100)
                            {
                                OperatorImage = image;
                                //File.WriteAllBytes(Application.StartupPath + "\\sn.jpg", image); //пока пусть так
                                return true;
                            }
                            else
                            {
                                return false; //no data answer
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }
        */

        #endregion
    }

    #region JsonClasses
    public class Channel
    {
        public object groupId { get; set; }
        public int id { get; set; }
        public string name { get; set; }
    }

    public class Plate
    {
        public string number { get; set; }
        public double confidence { get; set; }
        public object stencil { get; set; }
        public object bounds { get; set; }
        public object symbols { get; set; }
    }

    public class Result
    {
        public object tokens { get; set; }
        public int id { get; set; }
        public Channel channel { get; set; }
        public object timestamps { get; set; }
        public Plate plate { get; set; }
        public object movement { get; set; }
        public object additionalInfo { get; set; }
    }

    public class CallBackProperty
    {
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "method")]
        public string Method { get; set; }
    }

    public class CallBackFilter
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "callback")]
        public CallBackProperty CallBack { get; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "isPermanent")]
        public bool IsPermanent { get; set; }

        public CallBackFilter()
        {
            CallBack = new CallBackProperty();
        }
    }

    public class FilterProperty
    {
        [JsonProperty(PropertyName = "channels")]
        public int[] Channels { get; set; }

        [JsonProperty(PropertyName = "decisionСomposition")]
        public string[] DecisionComposition { get; set; }

        [JsonProperty(PropertyName = "imagesСomposition")]
        public string[] ImagesComposition { get; set; }

        [JsonProperty(PropertyName = "isBlocked")]
        public bool IsBlocked { get; set; }
    }

    public class SubscriptionJson
    {
        [JsonProperty(PropertyName = "callback")]
        public CallBackProperty CallBack { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "filter")]
        public string Filter { get; set; }

        public SubscriptionJson()
        {
            CallBack = new CallBackProperty();
        }
    }

    public class Callback
    {
        public string address { get; set; }
        public string method { get; set; }
    }

    public class Subscription
    {
        public string id { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public Callback callback { get; set; }
        public string type { get; set; }
        public bool isPermanent { get; set; }
        public int notificationsQueueCount { get; set; }
        public int notificationLifeTime { get; set; }
        public int notificationResendTimeout { get; set; }
        public string filter { get; set; }
    }
    #endregion
}
