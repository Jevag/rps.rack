﻿using Rps.ShareBase;
//using RPS.TariffCalc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using static CalculatorTS.ToTariffPlan;
using static stoyka01.GlobalClass;
using RPS.TariffCalc;
using CalculatorTS;

namespace stoyka01
{
    public class Tashkent
    {
        static int CalcRes = 0;

        public static Guid discount_guid;

        public static bool NeedUpdate;

        public static int ManualAmount=0;

        public struct Discounts
        {
            public Guid ID;
            public Guid CompanyID;
            public int? Discount;
            public string Name;
            public string Description;
            public int? CurrentIdTP;
            public int? IdTP;
            public int? IdTS;
            public int? groupIdFC;
            public Guid? IdTPForCount;
            public Guid? IdTSForCount;
            public Guid? TPForRent;
            public Guid? TSForRent;
            public bool? ResetLRT;
            public bool? AnySum;
        }

        public static List<Discounts> StartDiscounts;

        public static Discounts CurrentDiscount;

        public static string GetTPname(int tp)
        {
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                try
                {
                    DataTable Disc = new DataTable();
                    Disc = UtilsBase.FillTable("select * from Tariffs where IdFC = '" + tp.ToString() + "'", connect, null, null); // where ISNULL(IsDeleted,0)!=1                                       

                    if (Disc != null && Disc.Rows.Count != 0)
                    {
                        foreach (DataRow dad in Disc.Rows)
                        {
                            return dad.GetString("Name");                            
                        }
                    }
                    else
                    {
                        //Logging.ToLog("Не задано имя тарифа");
                        return "";
                    }

                }
                catch (Exception e)
                {
                    Logging.ToLog("Exception Tariffs " + e);
                    return "";
                }
            }
            return "";
        }

        public static string GetTSname(int ts)
        {
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                try
                {
                    DataTable Disc = new DataTable();
                    Disc = UtilsBase.FillTable("select * from TariffScheduleModel where IdFC = '" + ts.ToString() + "'", connect, null, null); // where ISNULL(IsDeleted,0)!=1                                       

                    if (Disc != null && Disc.Rows.Count != 0)
                    {
                        foreach (DataRow dad in Disc.Rows)
                        {
                            return dad.GetString("Name");
                        }
                    }
                    else
                    {
                        //Logging.ToLog("Не задано имя TS");
                        return "";
                    }

                }
                catch (Exception e)
                {
                    Logging.ToLog("Exception Tariffshedule " + e);
                    return "";
                }
            }
            return "";
        }

        //сделать обратные процедуры!!! (получение Id по гуиду)
        public static int GetTPIDFC(Guid? tp)
        {
            if (tp == null) return 1;

            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                try
                {
                    DataTable Disc = new DataTable();
                    Disc = UtilsBase.FillTable("select * from Tariffs where Id = '" + tp.ToString() + "'", connect, null, null); // where ISNULL(IsDeleted,0)!=1                                       

                    if (Disc != null && Disc.Rows.Count != 0)
                    {
                        foreach (DataRow dad in Disc.Rows)
                        {
                            return (int) dad.GetInt("IdFC");
                        }
                    }
                    else
                    {
                        //Logging.ToLog("Не задано имя тарифа");
                        return 1;
                    }

                }
                catch (Exception e)
                {
                    Logging.ToLog("Exception Tariffs " + e);
                    return 1;
                }
            }
            return 1;
        }

        public static int GetTSIDFC(Guid? ts)
        {
            if (ts == null) return 1;

            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                try
                {
                    DataTable Disc = new DataTable();
                    Disc = UtilsBase.FillTable("select * from TariffScheduleModel where Id = '" + ts.ToString() + "'", connect, null, null); // where ISNULL(IsDeleted,0)!=1                                       

                    if (Disc != null && Disc.Rows.Count != 0)
                    {
                        foreach (DataRow dad in Disc.Rows)
                        {
                            return (int)dad.GetInt("IdFC");
                        }
                    }
                    else
                    {
                        //Logging.ToLog("Не задано имя тарифа");
                        return 1;
                    }

                }
                catch (Exception e)
                {
                    Logging.ToLog("Exception Tariffs " + e);
                    return 1;
                }
            }
            return 1;
        }

        public static bool SelectDiscounts(Guid CompanyID)
        {
            StartDiscounts = new List<Discounts>();
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                try
                {
                    DataTable Disc = new DataTable();
                    Disc = UtilsBase.FillTable("select * from BarcodeModel where CompanyId = '" + CompanyID + "'", connect, null, null); // where ISNULL(IsDeleted,0)!=1                                       

                    if (Disc != null && Disc.Rows.Count != 0)
                    {
                        foreach (DataRow dad in Disc.Rows)
                        {
                            //Logging.ToLog("bububka");

                            Guid? ID = dad.GetGuid("Id");
                            //Guid? CompanyID = dad.GetGuid("CompanyId");
                            int? Discount = dad.GetInt("DiscountAmount");
                            string Description = dad.GetString("Description");
                            string Name = dad.GetString("Name");
                            int? CurrentIdTP = dad.GetInt("CurrentIdTP");
                            int? IdTP = dad.GetInt("IdTP");
                            int? IdTS = dad.GetInt("IdTS");
                            int? groupIdFC = dad.GetInt("groupIdFC");
                            Guid? IdTPForCount = dad.GetGuid("IdTPForCount");
                            Guid? IdTSForCount = dad.GetGuid("IdTSForCount");
                            Guid? TPForRent = dad.GetGuid("TPForRent");
                            Guid? TSForRent = dad.GetGuid("TSForRent");
                            bool? isDel = dad.GetBool("_IsDeleted");
                            int? DiscType = dad.GetInt("DiscountType");
                            bool? ResetLRT = dad.GetBool("ResetLRT");
                            bool? AnySum = dad.GetBool("AnySum");
                            //Logging.ToLog(isDel.ToString());
                            //проверить здесь IsDeleted!!!
                            if (ID != null  && Name != null && isDel !=true && DiscType==0)  //&& groupIdFC !=null) 
                            {
                                //Logging.ToLog("koteg");
                                Discounts disc = new Discounts();
                                disc.ID = (Guid)ID;
                                disc.CompanyID = CompanyID;
                                disc.Discount = Discount;
                                disc.Description = Description;
                                disc.Name = Name;
                                disc.CurrentIdTP = CurrentIdTP;
                                disc.IdTP = IdTP;
                                disc.IdTS = IdTS;
                                disc.groupIdFC = groupIdFC; //Берем группу с карты, если она Null!
                                disc.IdTPForCount = IdTPForCount;
                                disc.IdTSForCount = IdTSForCount;
                                disc.TPForRent = TPForRent;
                                disc.TSForRent = TPForRent;
                                disc.ResetLRT = ResetLRT;
                                disc.AnySum = AnySum;
                                StartDiscounts.Add(disc);
                            }
                        }
                    }
                    else
                    {
                        Logging.ToLog("Нет скидок для данной компании");
                        return false;
                    }
                    if (StartDiscounts.Count == 0)
                    {
                        Logging.ToLog("Нет скидок для данной компании (некорректно заполнены поля скидки и описания)");
                        return false; //0 скидок
                    }
                    else
                    {
                        return true;
                    }

                }
                catch (Exception e)
                {
                    Logging.ToLog("Exception SelectDiscounts " + e);
                    return false;
                }
            }
            //return false;

        }

        //public static string CurrentLogin;
        public static bool CheckLK(Guid guid)
        {
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                try
                {
                    DataTable LK = new DataTable();
                    LK = UtilsBase.FillTable("select * from CompanyModel where Id = '" + guid + "'", connect, null, null); // where ISNULL(IsDeleted,0)!=1                                       

                    if (LK != null && LK.Rows.Count != 0)
                    {
                        foreach (DataRow dad in LK.Rows)
                        {
                            bool? BlockLK = dad.GetBool("BlockLK");

                            if (BlockLK == null || BlockLK == false)
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    Logging.ToLog("Exception BlockLK " + e);
                    return false;
                }
            }
            return false;
        }

        public static string RetSHA1(string txt)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(txt));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("x2"));
                }

                return(sb.ToString());
            }
        }

        public static bool Authorize(string lgn, string pwd)
        {
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                try
                {
                    DataTable AuthUser = new DataTable();
                    AuthUser = UtilsBase.FillTable("select * from AuthUser where LoginName = '" + lgn + "'", connect, null, null); // where ISNULL(IsDeleted,0)!=1                                       

                    if (AuthUser != null && AuthUser.Rows.Count != 0)
                    {
                        foreach (DataRow dad in AuthUser.Rows)
                        {
                            string CryptoPass = dad.GetString("Password");
                            //Rps.Crypto.CryptoDecrypto cryp = new Rps.Crypto.CryptoDecrypto();
                            //string pass = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(CryptoPass));
                            string pass = RetSHA1(pwd); //шифрованный
                            Logging.ToLog("tsht_key " + pass);
                            if (CryptoPass == pass)
                            {
                                GlobalClass.UZ_Id = (Guid)dad.GetGuid("Id");
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    Logging.ToLog("Exception AuthUser " + e);
                    return false;
                }
            }
            return false;
        }

        public static int CalcTashkent()
        {
            /*
             если ResetLRT = 1 (True) - записываем расчет задолженности в ячейку Ammount генерируемой скидки в таблицы BarcodeInUse. Задолженность рассчитываем следующим образом: начало сессии - время въезда (LastRecounTime), 
             конец сессии - текущее время, 
             тарифный план/тарифная сетка idTPForCount/idTSForCount выбранной модели таблицы BarCodeModel.              
             */
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                var card = new RPS.TariffCalc.Api.TariffCalcParams();

                //DateCard TashDateCard = new DateCard();
                card.ClientGroupidFC = _DateCard.ClientGroupidFC;
                card.ClientTypidFC = _DateCard.ClientTypidFC;
                card.DateSaveCard = _DateCard.DateSaveCard;
                card.LastPaymentTime = _DateCard.LastPaymentTime;
                card.LastRecountTime = _DateCard.LastRecountTime;
                card.Nulltime1 = _DateCard.Nulltime1;
                card.Nulltime2 = _DateCard.Nulltime2;
                card.Nulltime3 = _DateCard.Nulltime3;
                card.ParkingEnterTime = _DateCard.ParkingEnterTime;
                card.SumOnCard = _DateCard.SumOnCard;
                int tpz = GetTPIDFC(CurrentDiscount.IdTPForCount);
                Logging.ToLog("FC_IDTP " + tpz.ToString());
                card.TPidFC = (byte)GetTPIDFC(CurrentDiscount.IdTPForCount);
                int tsz = GetTSIDFC(CurrentDiscount.IdTSForCount);
                Logging.ToLog("FC_IDTS " + tsz.ToString());
                card.TSidFC = (byte)GetTSIDFC(CurrentDiscount.IdTSForCount);
                card.TVP = _DateCard.TVP;
                card.ZoneidFC = _DateCard.ZoneidFC;

                var api = new RPS.TariffCalc.Api(card, connect);
                api.SetZone(0);

                int tp = card.TPidFC;
                DateTime pEnterTime = datetime0 + TimeSpan.FromSeconds(_DateCard.LastRecountTime);
                DateTime pExitTime = DateTime.Now;

                string SumAfter = api.GetSumOnCard(tp, pEnterTime, pExitTime);
                Debug.Print(SumAfter);
                if (SumAfter != "" && SumAfter != null)
                {
                    int retval = Convert.ToInt32(SumAfter);
                    //retval = Math.Abs(retval - _DateCard.SumOnCard);
                    retval = -1 * retval;
                    Logging.ToLog("Подсчет калькулятора " + retval.ToString());
                    retval = retval + _DateCard.SumOnCard;
                    Logging.ToLog("Чистая задолженность с учетом SumOnCard " + retval.ToString());
                    return retval;
                }
                else
                {
                    return 0;
                }
            }
            
            //api.SetZone((int)udZoneAfter.Value);

            //string SumAfter = api.GetSumOnCard(tp, pEnterTime, pExitTime);

        }

        public static void UpdateDiscount() //Проставляем в БД
        {
            /*
             Заполняем следующие колонки: IdBCM (ID выбранной модели)+, CompanyId+, IdTP+, IdTS+, DiscountAmount+, groupIdFC+, 
             UsageTime+, CreatedAt (now)+, IdTPForCount+, IdTSForCount+, CardId (прочитанная карта), Used (true), ActivatedAt (now), 
             ParkingEnterTime (=ParkingEnterTime карты), TPForRent, TSForRent, IsDeleted (0). Остальные поля NULL. 

             */

            //Пишем в BarcodeInUse!

            //if (CurrentDiscount.ResetLRT==false || CurrentDiscount.ResetLRT==null)

            //int a = GetTPIDFC(CurrentDiscount.IdTPForCount);

            //int b = GetTSIDFC(CurrentDiscount.IdTSForCount);
            //int calcres = 0;
            if (CurrentDiscount.ResetLRT==true)
                CalcRes= CalcTashkent();


            //Logging.ToLog(CurrentDiscount.ResetLRT.ToString());

            List<object> BC_list = new List<object>();
            BC_list.Add("Id");
            discount_guid = Guid.NewGuid(); //0
            BC_list.Add(discount_guid);

            BC_list.Add("IdBCM"); //1
            BC_list.Add(CurrentDiscount.ID);

            BC_list.Add("CompanyId"); //2
            BC_list.Add(CurrentDiscount.CompanyID);

            if (CurrentDiscount.IdTP != null)
            {
                BC_list.Add("IdTP"); //3
                BC_list.Add(CurrentDiscount.IdTP);
            }
            /*
            else
            {
                BC_list.Add("IdTP"); //3
                BC_list.Add(GlobalClass._DateCard.TPidFC);
            }
            */
            if (CurrentDiscount.IdTS != null)
            {
                BC_list.Add("IdTS"); //4
                BC_list.Add(CurrentDiscount.IdTS);
            }
            /*
            else
            {
                BC_list.Add("IdTS"); //4
                BC_list.Add(GlobalClass._DateCard.TSidFC);
            }
            */
            BC_list.Add("CreatedAt"); //5
            BC_list.Add(DateTime.Now);

            //Добавляем

            if (CurrentDiscount.AnySum == true)
            {
                BC_list.Add("DiscountAmount"); //6
                BC_list.Add(ManualAmount);
            }
            else

            {
                if (CurrentDiscount.Discount != null)
                {
                    BC_list.Add("DiscountAmount"); //6
                    BC_list.Add(CurrentDiscount.Discount);
                }
            }



            if (CurrentDiscount.groupIdFC != null)
            {
                BC_list.Add("GroupIdFC");
                BC_list.Add(CurrentDiscount.groupIdFC); //7
            }
            else
            {
                BC_list.Add("GroupIdFC");
                BC_list.Add(GlobalClass._DateCard.ClientGroupidFC);
            }

            BC_list.Add("UsageTime"); //8
            BC_list.Add(DateTime.Now);

            if (CurrentDiscount.IdTPForCount != null)
            {
                BC_list.Add("IdTPForCount");
                BC_list.Add(CurrentDiscount.IdTPForCount); //9
            }
            if (CurrentDiscount.IdTSForCount != null)
            {
                BC_list.Add("IdTSForCount");
                BC_list.Add(CurrentDiscount.IdTSForCount); //10
            }

            BC_list.Add("CardId");
            BC_list.Add(GlobalClass.UZShowData.UZCardId); //11

            if (CurrentDiscount.ResetLRT == true)
            {
                BC_list.Add("Ammount");
                BC_list.Add(CalcRes); //11
            }

            BC_list.Add("Used");
            BC_list.Add(true); //12

            BC_list.Add("ActivatedAt");
            BC_list.Add(DateTime.Now); //13

            BC_list.Add("ParkingEnterTime");
            BC_list.Add(Convert.ToDateTime(GlobalClass.UZShowData.UZEntryTime)); //14
            //BC_list.Add(DateTime.Now);

            if (CurrentDiscount.TPForRent != null)
            {
                BC_list.Add("TPForRent");
                BC_list.Add(CurrentDiscount.TPForRent); //9
            }

            if (CurrentDiscount.TSForRent != null)
            {
                BC_list.Add("TSForRent");
                BC_list.Add(CurrentDiscount.TSForRent); //10
            }

            BC_list.Add("IsDeleted");
            BC_list.Add(false);

            //BC_list.Add("ExternalId");
            //BC_list.Add(null);

            string log_test = string.Join(",", BC_list.ToArray());
            Debug.Print(log_test);
            //Debug.Print("ll");
            //пишем в Cards!!!
            /*
             TPIdFC (Cards) = IdTP (BarCodeInUse)
             TSIdFC (Cards) = IdTS (BarCodeInUse)
             DateSaveCard (Cards)= now
             DiscountAmount (BarCodeInUse) если не равен NULL прибавляет к значению SumOnCard (Cards)
             */


            List<object> Cards_list = new List<object>();

            
            if (CurrentDiscount.IdTP != null)
            {
                Cards_list.Add("TPidFC");
                Cards_list.Add(CurrentDiscount.IdTP);
                GlobalClass._DateCard.TPidFC = (byte)CurrentDiscount.IdTP;
            }
            /*
            else
            {
                Cards_list.Add(GlobalClass._DateCard.TPidFC);
            }
            */
            
            if (CurrentDiscount.IdTS != null)
            {
                Cards_list.Add("TSidFC");
                Cards_list.Add(CurrentDiscount.IdTS);
                GlobalClass._DateCard.TSidFC = (byte)CurrentDiscount.IdTS;
            }
            /*
            else
            {
                Cards_list.Add(GlobalClass._DateCard.TSidFC);
            }
            */
            if (CurrentDiscount.AnySum == true)
            {
                Cards_list.Add("SumOnCard");
                //_DateCard.SumOnCard += (int)CheckDiscount.DiscountAmount;
                Cards_list.Add(GlobalClass.UZShowData.UZSumOnCard + ManualAmount);
                GlobalClass._DateCard.SumOnCard = (int)GlobalClass.UZShowData.UZSumOnCard + (int)ManualAmount;
            }
            else
            {

                if (CurrentDiscount.Discount != null)
                {
                    Cards_list.Add("SumOnCard");
                    //_DateCard.SumOnCard += (int)CheckDiscount.DiscountAmount;
                    Cards_list.Add(GlobalClass.UZShowData.UZSumOnCard + CurrentDiscount.Discount);
                    GlobalClass._DateCard.SumOnCard = (int)GlobalClass.UZShowData.UZSumOnCard + (int)CurrentDiscount.Discount;
                }
            }
            if (CurrentDiscount.ResetLRT == true)
            {
                Cards_list.Add("LastRecountTime");
                Cards_list.Add(DateTime.Now);
            }

            Cards_list.Add("DateSaveCard");
            Cards_list.Add(DateTime.Now);

            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                UtilsBase.InsertCommand("BarCodeInUse", connect, null, BC_list.ToArray());
                UtilsBase.UpdateCommand("Cards", Cards_list.ToArray(), "CardId ='" + GlobalClass.UZShowData.UZCardId + "'", null, connect);
            }

            //обновим переменную _datecard?

            Thread.Sleep(500);
            TashkentTransact();
            TashkentCardTransact();
            GlobalClass.UZDiscountApproved = true;
            

            //transact("check_discount");
        }

        public static void TashkentTransact()
        {
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                DataTable BarCodeInUse =
                            //UtilsBase.FillTable("select * from BarCodeInUse where IdBCM = '" + CurrentDiscount.ID.ToString() + "'", connect,
                            UtilsBase.FillTable("select * from BarCodeInUse where Id = '" + discount_guid + "'", connect,
                            null,
                            null);
                if (BarCodeInUse != null && BarCodeInUse.Rows.Count != 0)
                {
                    foreach (DataRow dad in BarCodeInUse.Rows)
                    {

                        if (dad.GetBool("IsDeleted") != true)
                        {
                            List<object> lst = new List<object>();
                            lst.Add("Id");
                            lst.Add(Guid.NewGuid());
                            
                            if (dad.GetGuid("Id") != null)
                            {
                                lst.Add("BarCodeInUseId");
                                lst.Add(dad.GetGuid("Id"));
                            }
                            
                            //lst.Add("BarCodeInUseId");
                            //lst.Add(discount_guid);

                            if (dad.GetGuid("OperId") != null)
                            {
                                lst.Add("OperId");
                                lst.Add(dad.GetGuid("OperId"));
                            }
                            if (dad.GetGuid("CompanyId") != null)
                            {
                                lst.Add("CompanyId");
                                lst.Add(dad.GetGuid("CompanyId"));
                                //CompanyIdGu = (Guid)dad.GetGuid("CompanyId");
                            }
                            if (dad.GetInt("IdTP") != null)
                            {
                                lst.Add("IdTP");
                                lst.Add(dad.GetInt("IdTP"));
                            }
                            if (dad.GetInt("IdTS") != null)
                            {
                                lst.Add("IdTS");
                                lst.Add(dad.GetInt("IdTS"));
                            }

                            if (CurrentDiscount.AnySum == true)
                            {
                                lst.Add("DiscountAmount");
                                lst.Add(ManualAmount);
                            }
                            else
                            {
                                if (dad.GetInt("DiscountAmount") != null)
                                {
                                    lst.Add("DiscountAmount");
                                    lst.Add(dad.GetInt("DiscountAmount"));
                                }
                            }

                            if (dad.GetInt("GroupIdFC") != null)
                            {
                                lst.Add("GroupIdFC");
                                lst.Add(dad.GetInt("GroupIdFC"));
                            }
                            else
                            {
                                //
                            }

                            if (dad.GetDateTime("UsageTime") != null)
                            {
                                lst.Add("UsageTime");
                                lst.Add(dad.GetDateTime("UsageTime"));
                            }

                            lst.Add("ParkingTime");
                            lst.Add(0);
                            if (dad.GetDateTime("CreatedAt") != null)
                            {
                                lst.Add("CreatedAt");
                                lst.Add(dad.GetDateTime("CreatedAt"));
                            }
                            if (dad.GetDateTime("ParkingEnterTime") != null)
                            {
                                lst.Add("ParkingEnterTime");
                                lst.Add(dad.GetDateTime("ParkingEnterTime"));
                            }
                            else
                            {
                                lst.Add("ParkingEnterTime");
                                lst.Add(Convert.ToDateTime(GlobalClass.UZShowData.UZEntryTime));
                            }
                            if (dad.GetGuid("IdTPForCount") != null)
                            {
                                lst.Add("IdTPForCount");
                                lst.Add(dad.GetGuid("IdTPForCount"));
                            }

                            if (dad.GetGuid("IdTSForCount") != null)
                            {
                                lst.Add("IdTSForCount");
                                lst.Add(dad.GetGuid("IdTSForCount"));
                            }
                            
                            lst.Add("CardId");
                            lst.Add(GlobalClass.UZShowData.UZCardId);


                                if (CurrentDiscount.ResetLRT == true)
                                {
                                    lst.Add("Ammount");
                                    lst.Add(CalcRes); //11
                                }

                            lst.Add("Used");
                            lst.Add(true);
                            lst.Add("ActivatedAt");
                            lst.Add(DateTime.Now);

                            lst.Add("IdBCM");
                            lst.Add(CurrentDiscount.ID.ToString());

                            lst.Add("ExternalId");
                            lst.Add(dad.GetString("ExternalId"));

                            try
                            {
                                UtilsBase.InsertCommand("BCTransaction", connect, null, lst.ToArray());
                            }
                            catch (Exception e)
                            {
                                //Logging.ToLog("Exception Tashkent BCTransaction " + e);
                            }
                            break;
                        }
                    }
                }
            }       
        }

        public static void TashkentCardTransact()
        {
            #region CardsTransaction
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                    List<object> lst = new List<object>();
                    lst.Add("Id");
                    lst.Add(Guid.NewGuid());
                    lst.Add("Blocked");
                    lst.Add(0);
                    lst.Add("_IsDeleted");
                    lst.Add(false);
                    lst.Add("CardId");
                    lst.Add(GlobalClass.UZShowData.UZCardId);                    
                    lst.Add("ParkingEnterTime");
                    lst.Add(GlobalClass.datetime0 + TimeSpan.FromSeconds(GlobalClass._DateCard.ParkingEnterTime));
                if (CurrentDiscount.ResetLRT == true)
                {
                    lst.Add("LastRecountTime");
                    lst.Add(DateTime.Now);
                }
                else
                {
                    lst.Add("LastRecountTime");
                    lst.Add(GlobalClass.datetime0 + TimeSpan.FromSeconds(GlobalClass._DateCard.LastRecountTime));
                }

                    lst.Add("LastPaymentTime");
                    lst.Add(GlobalClass.datetime0 + TimeSpan.FromSeconds(GlobalClass._DateCard.LastPaymentTime));
                    lst.Add("TSidFC");
                    lst.Add(GlobalClass._DateCard.TSidFC);
                    lst.Add("TPidFC");
                    lst.Add(GlobalClass._DateCard.TPidFC);
                    lst.Add("ZoneidFC");
                    lst.Add(GlobalClass._DateCard.ZoneidFC);                  
                    lst.Add("ClientGroupidFC");
                    lst.Add(GlobalClass._DateCard.ClientGroupidFC);
                    lst.Add("SumOnCard");
                    lst.Add(GlobalClass._DateCard.SumOnCard);
                    lst.Add("Nulltime1");
                    lst.Add(GlobalClass.datetime0 + TimeSpan.FromSeconds(GlobalClass._DateCard.Nulltime1));
                    lst.Add("Nulltime2");
                    lst.Add(GlobalClass.datetime0 + TimeSpan.FromSeconds(GlobalClass._DateCard.Nulltime2));
                    lst.Add("Nulltime3");
                    lst.Add(GlobalClass.datetime0 + TimeSpan.FromSeconds(GlobalClass._DateCard.Nulltime3));
                    lst.Add("TVP");
                    lst.Add(GlobalClass.datetime0 + TimeSpan.FromSeconds(GlobalClass._DateCard.TVP));
                    lst.Add("TKVP");
                    lst.Add(GlobalClass._DateCard.TKVP);
                    lst.Add("ClientTypidFC");
                    lst.Add(GlobalClass._DateCard.ClientTypidFC);
                    lst.Add("DateSaveCard");
                    lst.Add(DateTime.Now.AddSeconds(3));
                    lst.Add("LastPlate");
                    lst.Add("");                
                    try
                    {
                        UtilsBase.InsertCommand("CardsTransaction", connect, null, lst.ToArray());
                    }
                    catch (Exception e)
                    {
                         Logging.ToLog("Exception CardsTransaction " + e);
                    }
                }
            #endregion

        }

    }
}
