﻿using ExternalDisplay;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Класс, содержащий JSON объекты для разных взаимодействий
namespace stoyka01
{
    public class JsonClasses
    {
        

    }

    #region ForViinex

    public class PlateRect
    {
        public double right { get; set; }
        public double top { get; set; }
        public double bottom { get; set; }
        public double left { get; set; }
    }

    public class ViinexResponse
    {
        public double confidence { get; set; }
        public DateTime timestamp { get; set; }
        public PlateRect plate_rect { get; set; }
        public string plate_text { get; set; }
    }

    public class ViinexTSResponse
    {
        public DateTime timestamp { get; set; }
    }

    public class Object
    {
        public string type { get; set; }
        public string name { get; set; }
        public string host { get; set; }
        public string url { get; set; }
        public List<string> auth { get; set; }
        public List<string> acquire { get; set; }
        public string staticpath { get; set; }
        public int? port { get; set; }
        public string mode { get; set; }
        public string engine { get; set; }
        public string datapath { get; set; }
        public List<string> templates { get; set; }
        public int? workers { get; set; }
        public dynamic skip { get; set; }
        public double? preprocess { get; set; }
        public double? postprocess { get; set; }
        public string transliterate { get; set; }
        public string snapshots_dump_path { get; set; }
        public int? time_budget { get; set; }
        public List<string> transport { get; set; }
        public List<double> roi { get; set; }
    }
    public class ViinexSettings
    {
        public List<Object> objects { get; set; }
        public List<List<List<string>>> links { get; set; }
        public string license { get; set; }
    }

    public class ViinexCameraPing
    {
        public int bitrate { get; set; }
        public DateTime last_frame { get; set; }
        public List<int> resolution { get; set; }
    }

    #endregion

    #region forMallenom
    public class mPoint
    {
        public double x { get; set; }
        public double y { get; set; }
    }

    public class RealPlateSize1
    {
        public double width { get; set; }
        public double height { get; set; }
    }

    public class RealPlateSize2
    {
        public double width { get; set; }
        public double height { get; set; }
    }

    public class MlnSettings
    {
        public List<mPoint> points { get; set; }
        public RealPlateSize1 realPlateSize1 { get; set; }
        public RealPlateSize2 realPlateSize2 { get; set; }
        public int videoChannelId { get; set; }
        public string rtsp { get; set; }
        public string clientIp { get; set; }
        public int confidence { get; set; }
        public int timeBudget { get; set; }
        public int preProcessing { get; set; }
        public int postProcessing { get; set; }
    }
    #endregion

    #region ForAtol
    public class AtolAuth
    {
        public string login { get; set; }
        public string pass { get; set; }
    }

    /*
     Ответ на запрос
Пример ответа: 1 2 3 4 5 { "error":null, "token":"fj45u923j59ju42395iu9423i59243u0", "timestamp":"30.11.2017 17:58:53" }
Пример ответа с ошибкой: 1 2 3 4 5 6 7 8 9 { "status":"fail", "error":{ "error_id":"4475d6d8d-844d-4d05-aa8b-e3dbdf3defd5", "code":12, "text":"Неверный логин или пароль" }, "timestamp":"30.11.2017 17:58:53" }
     */
    public class AuthError
    {
        public string error_id { get; set; }
        public int code { get; set; }
        public string text { get; set; }
    }

    public class AtolAuthResponse
    {
        public string status { get; set; }
        public string timestamp { get; set; }
        public string token { get; set; }
        public AuthError error { get; set; }
    }

    //Отправка чека
    public class Client
    {
        public string email { get; set; }
        public string phone { get; set; }
    }

    public class Company
    {
        public string email { get; set; }
        public string sno { get; set; }
        public string inn { get; set; }
        public string payment_address { get; set; }
    }

    public class Vat
    {
        public string type { get; set; }
    }

    public class Item
    {
        public string name { get; set; }
        public double price { get; set; }
        public double quantity { get; set; }
        public double sum { get; set; }
        public string measurement_unit { get; set; }
        public string payment_method { get; set; }
        public string payment_object { get; set; }
        public string nomenclature_code { get; set; }
        public Vat vat { get; set; }
    }

    public class Payment
    {
        public int type { get; set; }
        public double sum { get; set; }
    }

    public class Vat2
    {
        public string type { get; set; }
        public double sum { get; set; }
    }

    public class Receipt
    {
        public Client client { get; set; }
        public Company company { get; set; }
        public List<Item> items { get; set; }
        public List<Payment> payments { get; set; }
        public List<Vat2> vats { get; set; }
        public double total { get; set; }
    }

    public class Service
    {
        public string callback_url { get; set; }
    }

    public class AtolReceipt
    {
        public string external_id { get; set; }
        public Receipt receipt { get; set; }
        public Service service { get; set; }
        public string timestamp { get; set; }
    }

    #endregion

    #region forEconom


    #endregion

    #region For_Iskra_KKM
    public class RequestToken
    {
        public string login { get; set; }
        public string password { get; set; }
    }

    public class RequestTokenRoot
    {
        public string request_type { get; set; }
        public RequestToken request { get; set; }
    }

    public class ResponseError
    {
        public int result { get; set; }
        public int error_code { get; set; }
        public string comment { get; set; }
    }

    public class ResponseToken
    {
        public string token { get; set; }
    }

    public class ResponseTokenRoot
    {
        public ResponseError response_error { get; set; }
        public ResponseToken response { get; set; }
    }

    public class Position
    {
        public string name { get; set; }
        public double price { get; set; }
        public double quantity { get; set; }
        public double amount { get; set; }
        public object measure_unit { get; set; }
        public int tax { get; set; }
        public int calc_method_sign { get; set; }
        public int calc_subject_sign { get; set; }
        public object ktn { get; set; }
        public object add_props { get; set; }
        public object excise { get; set; }
        public object country_code { get; set; }
        public object declaration_number { get; set; }
        public object agent_data { get; set; }
    }

    public class IskraPayment
    {
        public int payment_type { get; set; }
        public double amount { get; set; }
    }

    public class ChequeContent
    {
        public string calc_address { get; set; }
        public string calc_place { get; set; }
        public string cashier_name { get; set; }

        [JsonProperty("cashier_inn ")]
        public string CashierInn { get; set; }
        public int calc_sign { get; set; }
        public int taxation { get; set; }
        public object agent_data { get; set; }
        public object provider_data { get; set; }
        public List<Position> positions { get; set; }
        public double total { get; set; }
        public object add_user_props { get; set; }
        public object customer { get; set; }
        public object customer_inn { get; set; }
        public object client_info { get; set; }
        public List<IskraPayment> payments { get; set; }
        public bool correction { get; set; }
        public int correction_type { get; set; }
        public object correction_basis { get; set; }
    }

    public class ChequeRequest
    {
        public string external_id { get; set; }
        public string terminal_id { get; set; }
        public int terminal_type { get; set; }
        public string timestamp { get; set; }
        public ChequeContent cheque_content { get; set; }
    }

    public class ChequeRoot
    {
        public string request_type { get; set; }
        public ChequeRequest request { get; set; }
    }

    //good answer
    public class ResponseChequeError
    {
        public int result { get; set; }
        public int error_code { get; set; }
        public object comment { get; set; }
    }

    //uuid+timestamp
    public class ResponseCheque
    {
        public string uuid { get; set; }
        public string timestamp { get; set; }
    }

    public class ResponseChequeRoot
    {
        public ResponseChequeError response_error { get; set; }
        public object response { get; set; }
    }

    public class ChequeStatusRequest
    {
        public string uuid { get; set; }
    }

    public class ChequeStatusRequestRoot
    {
        public string request_type { get; set; }
        public ChequeStatusRequest request { get; set; }
    }

    public class ChequeStatusResponseError
    {
        public int result { get; set; }
        public int error_code { get; set; }
        public string comment { get; set; }
    }

    public class ChequeInfo
    {
        public string external_id { get; set; }
        public string timestamp { get; set; }
        public string kkt_serial { get; set; }
        public string fn_serial { get; set; }
        public int fd_number { get; set; }
        public string fiscal_sign { get; set; }
        public string qr_value { get; set; }
    }

    public class ChequeError
    {
        public int error_code { get; set; }
        public string comment { get; set; }
    }

    public class ChequeStatusResponse
    {
        public int status { get; set; }
        public ChequeInfo cheque_info { get; set; }
        public ChequeError cheque_error { get; set; }
    }

    public class ChequeStatus
    {
        public ChequeStatusResponseError response_error { get; set; }
        public ChequeStatusResponse response { get; set; }
    }

    #endregion
}
