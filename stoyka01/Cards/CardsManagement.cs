﻿using Rps.ShareBase;
using RPS_Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static stoyka01.GlobalClass;

namespace stoyka01
{
    public class CardsManagement
    {
        public static void CardKey()          // дешифратор ключа карт
        {
            try
            {
                byte[] encrypted;
                string decrypted = "";

                //Ищем в таблице DeviceRackSettings...
                foreach (RackSettings rackrow in RSet) //Лист настроек...
                {
                    if (rackrow.name == "CardKey")
                    {
                        if (rackrow.value != null)
                        {
                            encrypted = ServiceClass.StringToByteA(rackrow.value);
                            decrypted = ServiceClass.DecryptStringFromBytes_Aes(encrypted);
                        }
                        else
                        {
                            decrypted = "FFFFFFFFFFFF";
                        }
                    }
                }
                NumberStyles style;
                style = NumberStyles.AllowHexSpecifier;

                int a = decrypted.Length / 2;
                for (int i = 0; i < a * 2; i += 2)
                {
                    try
                    {
                        keyA[i / 2] = byte.Parse(decrypted.Substring(i, 2), style);  //два символа из строки = число "41" , переводит в hex 0x41
                    }
                    catch
                    {
                        keyA[i / 2] = 0xff;
                    }
                }
            }
            catch (Exception e)
            {
                Logging.ToLog("Exception дешифратор ключа карт: " + e.Message);
            }
        }

        public static bool GetCardFromDB()
        {
            using (SqlConnection connect = new SqlConnection(conlocal))
            {
                connect.Open();
                DataTable Cards = UtilsBase.FillTable("select * from Cards where CardId = '" + CardIdCard.ToString() + "'", connect, null, null);
                if (Cards != null && Cards.Rows.Count != 0)
                {
                    foreach (DataRow Cardi in Cards.Rows)
                    {
                        if (Cardi.GetBool("_IsDeleted") != true)
                        {
                            _DateCard.ParkingEnterTime = (int)((DateTime)Cardi.GetDateTime("ParkingEnterTime") - datetime0).TotalSeconds;
                            _DateCard.DateSaveCard = (int)((DateTime)Cardi.GetDateTime("DateSaveCard") - datetime0).TotalSeconds;
                            _DateCard.Nulltime1 = (int)((DateTime)Cardi.GetDateTime("Nulltime1") - datetime0).TotalSeconds;
                            _DateCard.Nulltime2 = (int)((DateTime)Cardi.GetDateTime("Nulltime2") - datetime0).TotalSeconds;
                            _DateCard.Nulltime3 = (int)((DateTime)Cardi.GetDateTime("Nulltime3") - datetime0).TotalSeconds;
                            _DateCard.LastPaymentTime = (int)((DateTime)Cardi.GetDateTime("LastPaymentTime") - datetime0).TotalSeconds;
                            _DateCard.LastRecountTime = (int)((DateTime)Cardi.GetDateTime("LastRecountTime") - datetime0).TotalSeconds;
                            _DateCard.TVP = (int)((DateTime)Cardi.GetDateTime("TVP") - datetime0).TotalSeconds;


                            _DateCard.ClientGroupidFC = (byte)Cardi.GetInt("ClientGroupidFC");
                            _DateCard.ClientTypidFC = (byte)Cardi.GetInt("ClientTypidFC");
                            _DateCard.SumOnCard = (int)Cardi.GetInt("SumOnCard");
                            _DateCard.TKVP = (byte)Cardi.GetInt("TKVP");
                            _DateCard.TPidFC = (byte)Cardi.GetInt("TPidFC");
                            _DateCard.TSidFC = (byte)Cardi.GetInt("TSidFC");
                            _DateCard.ZoneidFC = (byte)Cardi.GetInt("ZoneidFC");
                             //BlockTime = Cardi.GetDateTime("BlockTime");
                        }
                    }
                }

            }
            return true;
        }

        

        public static bool GetCardFromDBTicket()
        {
            //try
            //{

                using (SqlConnection connect = new SqlConnection(conlocal))
                {
                    connect.Open();
                    DataTable Cards = UtilsBase.FillTable("select * from Cards where CardId = '" + CardIdCardS + "'", connect, null, null);
                    if (Cards != null && Cards.Rows.Count != 0)
                    {
                        //Logging.ToLog("123");

                        foreach (DataRow Cardi in Cards.Rows)
                        {
                            if (Cardi.GetBool("_IsDeleted") != true)
                            {
                                //Logging.ToLog("12345");

                                _DateCard.ParkingEnterTime = (int)((DateTime)Cardi.GetDateTime("ParkingEnterTime") - datetime0).TotalSeconds;
                                _DateCard.DateSaveCard = (int)((DateTime)Cardi.GetDateTime("DateSaveCard") - datetime0).TotalSeconds;


                            if (Cardi.GetDateTime("Nulltime1") == null) { _DateCard.Nulltime1 = 0; } else
                                { _DateCard.Nulltime1 = (int)((DateTime)Cardi.GetDateTime("Nulltime1") - datetime0).TotalSeconds;}

                            if (Cardi.GetDateTime("Nulltime2") == null) { _DateCard.Nulltime2 = 0; }
                            else
                                { _DateCard.Nulltime2 = (int)((DateTime)Cardi.GetDateTime("Nulltime2") - datetime0).TotalSeconds; }

                            if (Cardi.GetDateTime("Nulltime3") == null) { _DateCard.Nulltime3 = 0; }
                            else
                                { _DateCard.Nulltime3 = (int)((DateTime)Cardi.GetDateTime("Nulltime3") - datetime0).TotalSeconds; }

                            if (Cardi.GetDateTime("LastPaymentTime") == null) { _DateCard.LastPaymentTime = 0; }
                            else
                            { _DateCard.LastPaymentTime = (int)((DateTime)Cardi.GetDateTime("LastPaymentTime") - datetime0).TotalSeconds; }

                            if (Cardi.GetDateTime("LastRecountTime") == null) { _DateCard.LastRecountTime = 0; }
                            else
                            { _DateCard.LastRecountTime = (int)((DateTime)Cardi.GetDateTime("LastRecountTime") - datetime0).TotalSeconds; }

                            if (Cardi.GetDateTime("TVP") == null) { _DateCard.TVP = 0; }
                            else
                            { _DateCard.TVP = (int)((DateTime)Cardi.GetDateTime("TVP") - datetime0).TotalSeconds; }

                            //_DateCard.LastPaymentTime = (int)((DateTime)Cardi.GetDateTime("LastPaymentTime") - datetime0).TotalSeconds;
                            //_DateCard.LastRecountTime = (int)((DateTime)Cardi.GetDateTime("LastRecountTime") - datetime0).TotalSeconds;
                            //_DateCard.TVP = (int)((DateTime)Cardi.GetDateTime("TVP") - datetime0).TotalSeconds;
                            if (Cardi.GetInt("TKVP") == null) { _DateCard.TKVP = 0; }
                            else
                            { _DateCard.TKVP = (byte)Cardi.GetInt("TKVP"); }


                                _DateCard.ClientGroupidFC = (byte)Cardi.GetInt("ClientGroupidFC");
                                _DateCard.ClientTypidFC = (byte)Cardi.GetInt("ClientTypidFC");
                                _DateCard.SumOnCard = (int)Cardi.GetInt("SumOnCard");
                                //_DateCard.TKVP = (byte)Cardi.GetInt("TKVP");
                                _DateCard.TPidFC = (byte)Cardi.GetInt("TPidFC");
                                _DateCard.TSidFC = (byte)Cardi.GetInt("TSidFC");
                                _DateCard.ZoneidFC = (byte)Cardi.GetInt("ZoneidFC");
                                BlockTime = Cardi.GetDateTime("BlockTime");
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
            //}
            //catch (Exception ex) {
            //    Logging.ToLog(ex.Message);
            //    return false;
            //}
            return false;
        }

        public static void rBlockToData()             // считанное с карты в Данные
        {
            try
            {
                using (SqlConnection connect = new SqlConnection(conlocal))
                {
                    connect.Open();
                    DataTable BlockedCards = new DataTable();
                    BlockedCards = UtilsBase.FillTable("select * from Cards where CardId = '" + CardIdCardS + "'", connect, null, null); // where ISNULL(IsDeleted,0)!=1
                    if (BlockedCards != null && BlockedCards.Rows.Count != 0)
                    {
                        DataRow dad = BlockedCards.Rows[0];
                        BlockTime = dad.GetDateTime("BlockTime");
                    }
                }
            }
            catch (Exception ex) { }
                DateTime CardLPTime;
            DateTime DBLPTime;

            _DateCard.ParkingEnterTime = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.ParkingEnterTime += rBlock[i] << (i * 8);
            }
            _DateCard.LastRecountTime = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.LastRecountTime += rBlock[i + 4] << (i * 8);
            }
            _DateCard.TSidFC = rBlock[8];
            _DateCard.TPidFC = rBlock[9];
            TPBefore = _DateCard.TPidFC;
            _DateCard.ZoneidFC = rBlock[10];
            ZonaDoCard = _DateCard.ZoneidFC;
            _DateCard.ClientGroupidFC = rBlock[11];
            _DateCard.SumOnCard = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.SumOnCard += rBlock[i + 12] << (i * 8);
            }
            _DateCard.LastPaymentTime = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.LastPaymentTime += rBlock[i + 16] << (i * 8);
            }
            _DateCard.Nulltime1 = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.Nulltime1 += rBlock[i + 20] << (i * 8);
            }
            _DateCard.Nulltime2 = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.Nulltime2 += rBlock[i + 24] << (i * 8);
            }
            _DateCard.Nulltime3 = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.Nulltime3 += rBlock[i + 28] << (i * 8);
            }
            _DateCard.TVP = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.TVP += rBlock[i + 32] << (i * 8);
            }
            _DateCard.TKVP = rBlock[36];
            _DateCard.ClientTypidFC = rBlock[37];

            _DateCard.DateSaveCard = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.DateSaveCard += rBlock[i + 44] << (i * 8);
            }

            DateSaveCard = datetime0 + TimeSpan.FromSeconds(_DateCard.DateSaveCard);
            CardLPTime= datetime0 + TimeSpan.FromSeconds(_DateCard.LastPaymentTime);

            GlobalClass.PenaltyLastRecountSync = false;

            Logging.logCard("прочитано с физической карты");      // запись данных с карты

            #region// проверка карта/база, что новее
            //Доступ к бд через db
            try
            {
                db.Dispose(); // Удаляет контекст EF
                db = new Entities();
                
                var Cardi = db.Cards.FirstOrDefault(x => x.CardId == CardIdCard);
                //BlockTime = Cardi.BlockTime;
                //здесь проверять ласт паймент синхронизацию...
                if (!UseDbonly)
                {
                    if (Cardi != null && CardsOnline == 1)
                    {
                        try
                        {
                            DateTime timeEntry = (DateTime)Cardi.ParkingEnterTime;

                            DateTime CardiParkingEnterTime = (DateTime)Cardi.ParkingEnterTime;
                            DateTime CardiDateSaveCard = (DateTime)Cardi.DateSaveCard;
                            DateTime CardiNulltime1 = (DateTime)Cardi.Nulltime1;
                            DateTime CardiNulltime2 = (DateTime)Cardi.Nulltime2;
                            DateTime CardiNulltime3 = (DateTime)Cardi.Nulltime3;

                            DateTime CardiLastPaymentTime = (DateTime)Cardi.LastPaymentTime;
                            DateTime CardiLastRecountTime = (DateTime)Cardi.LastRecountTime;
                            


                            if (CardiLastPaymentTime == CardLPTime)
                            {
                                GlobalClass.PenaltyLastRecountSync = true;
                            }

                            string What = "Данные из базы:" + Environment.NewLine +
                                "CardId  " + CardIdCard.ToString() + Environment.NewLine +
                                "ParkingEnterTime  " + CardiParkingEnterTime.ToString("F") + Environment.NewLine +
                                "DateSaveCard  " + CardiDateSaveCard.ToString("F") + Environment.NewLine +
                                "Nulltime1  " + CardiNulltime1.ToString("F") + Environment.NewLine +
                                "Nulltime2  " + CardiNulltime2.ToString("F") + Environment.NewLine +
                                "Nulltime3  " + CardiNulltime3.ToString("F") + Environment.NewLine +
                                "LastPaymentTime  " + CardiLastPaymentTime.ToString("F") + Environment.NewLine +
                                "LastRecountTime  " + CardiLastRecountTime.ToString("F") + Environment.NewLine +
                                "TVP  " + Cardi.TVP.ToString() + Environment.NewLine +
                                "ClientGroupidFC  " + Cardi.ClientGroupidFC.ToString() + Environment.NewLine +
                                "ClientTypidFC  " + Cardi.ClientTypidFC.ToString() + Environment.NewLine +
                                "SumOnCard  " + Cardi.SumOnCard.ToString() + Environment.NewLine +
                                "TKVP  " + Cardi.TKVP.ToString() + Environment.NewLine +
                                "TPidFC  " + Cardi.TPidFC.ToString() + Environment.NewLine +
                                "TSidFC  " + Cardi.TSidFC.ToString() + Environment.NewLine +
                                "ZoneidFC  " + Cardi.ZoneidFC.ToString() + Environment.NewLine;
                            Logging.ToLog(What);
                        }
                        catch (Exception ee)
                        {
                            Console.WriteLine("Exception запись в лог событий: " + ee.Message);
                        }



                        if (Cardi.DateSaveCard != null)
                        {
                            if (Cardi.DateSaveCard > DateSaveCard)
                            {
                                if (Cardi.ParkingEnterTime != null)
                                    _DateCard.ParkingEnterTime = (int)((DateTime)Cardi.ParkingEnterTime - datetime0).TotalSeconds;
                                else _DateCard.ParkingEnterTime = 0;
                                if (Cardi.DateSaveCard != null)
                                    _DateCard.DateSaveCard = (int)((DateTime)Cardi.DateSaveCard - datetime0).TotalSeconds;
                                else _DateCard.DateSaveCard = 0;
                                if (Cardi.Nulltime1 != null)
                                    _DateCard.Nulltime1 = (int)((DateTime)Cardi.Nulltime1 - datetime0).TotalSeconds;
                                else _DateCard.Nulltime1 = 0;
                                if (Cardi.Nulltime2 != null)
                                    _DateCard.Nulltime2 = (int)((DateTime)Cardi.Nulltime2 - datetime0).TotalSeconds;
                                else _DateCard.Nulltime2 = 0;
                                if (Cardi.Nulltime3 != null)
                                    _DateCard.Nulltime3 = (int)((DateTime)Cardi.Nulltime3 - datetime0).TotalSeconds;
                                else _DateCard.Nulltime3 = 0;
                                if (Cardi.LastPaymentTime != null)
                                    _DateCard.LastPaymentTime = (int)((DateTime)Cardi.LastPaymentTime - datetime0).TotalSeconds;
                                else _DateCard.LastPaymentTime = 0;
                                if (Cardi.LastRecountTime != null)
                                    _DateCard.LastRecountTime = (int)((DateTime)Cardi.LastRecountTime - datetime0).TotalSeconds;
                                else _DateCard.LastRecountTime = 0;
                                if (Cardi.TVP != null)
                                    _DateCard.TVP = (int)((DateTime)Cardi.TVP - datetime0).TotalSeconds;
                                else _DateCard.TVP = 0;

                                if (Cardi.ClientGroupidFC != null)
                                    _DateCard.ClientGroupidFC = (byte)Cardi.ClientGroupidFC;
                                else _DateCard.ClientGroupidFC = 0;
                                if (Cardi.ClientTypidFC != null)
                                    _DateCard.ClientTypidFC = (byte)Cardi.ClientTypidFC;
                                else _DateCard.ClientTypidFC = 0;
                                if (Cardi.SumOnCard != null)
                                    _DateCard.SumOnCard = (int)Cardi.SumOnCard;
                                else _DateCard.SumOnCard = 0;
                                if (Cardi.TKVP != null)
                                    _DateCard.TKVP = (byte)Cardi.TKVP;
                                else _DateCard.TKVP = 0;
                                if (Cardi.TPidFC != null)
                                    _DateCard.TPidFC = (byte)Cardi.TPidFC;
                                else _DateCard.TPidFC = 0;
                                if (Cardi.TSidFC != null)
                                    _DateCard.TSidFC = (byte)Cardi.TSidFC;
                                else _DateCard.TSidFC = 0;
                                if (Cardi.ZoneidFC != null)
                                    _DateCard.ZoneidFC = (byte)Cardi.ZoneidFC;
                                else _DateCard.ZoneidFC = 0;
                                Logging.logCard("используем данные из базы");
                            }
                            else
                            {
                                Logging.logCard("используем данные с физ карты. DataSaveCard в базе меньше или равно физ карте");
                            }

                            //Logging.logCard("после проверки в базе");      // запись данных с карты
                        }
                        else
                        {
                            Logging.logCard("используем данные с физ карты. DataSaveCard в базе = NULL");
                        }
                    }
                    else
                    {
                        Logging.logCard("используем данные с физ карты. нет карты в БД или CardsOnline выкл.");
                    }
                }
                else
                {
                    if (Cardi.ParkingEnterTime != null)
                        _DateCard.ParkingEnterTime = (int)((DateTime)Cardi.ParkingEnterTime - datetime0).TotalSeconds;
                    else _DateCard.ParkingEnterTime = 0;
                    if (Cardi.DateSaveCard != null)
                        _DateCard.DateSaveCard = (int)((DateTime)Cardi.DateSaveCard - datetime0).TotalSeconds;
                    else _DateCard.DateSaveCard = 0;
                    if (Cardi.Nulltime1 != null)
                        _DateCard.Nulltime1 = (int)((DateTime)Cardi.Nulltime1 - datetime0).TotalSeconds;
                    else _DateCard.Nulltime1 = 0;
                    if (Cardi.Nulltime2 != null)
                        _DateCard.Nulltime2 = (int)((DateTime)Cardi.Nulltime2 - datetime0).TotalSeconds;
                    else _DateCard.Nulltime2 = 0;
                    if (Cardi.Nulltime3 != null)
                        _DateCard.Nulltime3 = (int)((DateTime)Cardi.Nulltime3 - datetime0).TotalSeconds;
                    else _DateCard.Nulltime3 = 0;
                    if (Cardi.LastPaymentTime != null)
                        _DateCard.LastPaymentTime = (int)((DateTime)Cardi.LastPaymentTime - datetime0).TotalSeconds;
                    else _DateCard.LastPaymentTime = 0;
                    if (Cardi.LastRecountTime != null)
                        _DateCard.LastRecountTime = (int)((DateTime)Cardi.LastRecountTime - datetime0).TotalSeconds;
                    else _DateCard.LastRecountTime = 0;
                    if (Cardi.TVP != null)
                        _DateCard.TVP = (int)((DateTime)Cardi.TVP - datetime0).TotalSeconds;
                    else _DateCard.TVP = 0;

                    if (Cardi.ClientGroupidFC != null)
                        _DateCard.ClientGroupidFC = (byte)Cardi.ClientGroupidFC;
                    else _DateCard.ClientGroupidFC = 0;
                    if (Cardi.ClientTypidFC != null)
                        _DateCard.ClientTypidFC = (byte)Cardi.ClientTypidFC;
                    else _DateCard.ClientTypidFC = 0;
                    if (Cardi.SumOnCard != null)
                        _DateCard.SumOnCard = (int)Cardi.SumOnCard;
                    else _DateCard.SumOnCard = 0;
                    if (Cardi.TKVP != null)
                        _DateCard.TKVP = (byte)Cardi.TKVP;
                    else _DateCard.TKVP = 0;
                    if (Cardi.TPidFC != null)
                        _DateCard.TPidFC = (byte)Cardi.TPidFC;
                    else _DateCard.TPidFC = 0;
                    if (Cardi.TSidFC != null)
                        _DateCard.TSidFC = (byte)Cardi.TSidFC;
                    else _DateCard.TSidFC = 0;
                    if (Cardi.ZoneidFC != null)
                        _DateCard.ZoneidFC = (byte)Cardi.ZoneidFC;
                    else _DateCard.ZoneidFC = 0;
                    Logging.logCard("принудительно используем данные из базы. защита от мошенников.");
                }
            }
            catch (Exception e)
            {
                Logging.ToLog("Exception CardsOnline. " + e.ToString());
            }
            #endregion
            Save_DateCard = _DateCard;          // сохраним на всякий случай
            _DateCard2 = _DateCard;          // сохраним на всякий случай с карты или базы
        }

        public static void rBlockToDataPP()             // считанное с карты в Данные
        {
            DateTime CardLPTime;
            DateTime DBLPTime;

            _DateCard.ParkingEnterTime = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.ParkingEnterTime += rBlockPP[i] << (i * 8);
            }
            _DateCard.LastRecountTime = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.LastRecountTime += rBlockPP[i + 4] << (i * 8);
            }
            _DateCard.TSidFC = rBlockPP[8];
            _DateCard.TPidFC = rBlockPP[9];
            TPBefore = _DateCard.TPidFC;
            _DateCard.ZoneidFC = rBlockPP[10];
            ZonaDoCard = _DateCard.ZoneidFC;
            _DateCard.ClientGroupidFC = rBlockPP[11];
            _DateCard.SumOnCard = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.SumOnCard += rBlockPP[i + 12] << (i * 8);
            }
            _DateCard.LastPaymentTime = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.LastPaymentTime += rBlockPP[i + 16] << (i * 8);
            }
            _DateCard.Nulltime1 = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.Nulltime1 += rBlockPP[i + 20] << (i * 8);
            }
            _DateCard.Nulltime2 = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.Nulltime2 += rBlockPP[i + 24] << (i * 8);
            }
            _DateCard.Nulltime3 = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.Nulltime3 += rBlockPP[i + 28] << (i * 8);
            }
            _DateCard.TVP = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.TVP += rBlockPP[i + 32] << (i * 8);
            }
            _DateCard.TKVP = rBlockPP[36];
            _DateCard.ClientTypidFC = rBlockPP[37];

            _DateCard.DateSaveCard = 0;
            for (int i = 0; i < 4; i++)
            {
                _DateCard.DateSaveCard += rBlockPP[i + 44] << (i * 8);
            }

            DateSaveCard = datetime0 + TimeSpan.FromSeconds(_DateCard.DateSaveCard);
            CardLPTime = datetime0 + TimeSpan.FromSeconds(_DateCard.LastPaymentTime);

            GlobalClass.PenaltyLastRecountSync = false;

            Logging.logCard("прочитано Паркпасс");      // запись данных с карты

            #region// проверка карта/база, что новее
            //Доступ к бд через db
            try
            {
                db.Dispose(); // Удаляет контекст EF
                db = new Entities();

                var Cardi = db.Cards.FirstOrDefault(x => x.CardId == CardIdCard);

                //здесь проверять ласт паймент синхронизацию...

                if (Cardi != null && CardsOnline == 1)
                {
                    try
                    {
                        DateTime timeEntry = (DateTime)Cardi.ParkingEnterTime;

                        DateTime CardiParkingEnterTime = (DateTime)Cardi.ParkingEnterTime;
                        DateTime CardiDateSaveCard = (DateTime)Cardi.DateSaveCard;
                        DateTime CardiNulltime1 = (DateTime)Cardi.Nulltime1;
                        DateTime CardiNulltime2 = (DateTime)Cardi.Nulltime2;
                        DateTime CardiNulltime3 = (DateTime)Cardi.Nulltime3;

                        DateTime CardiLastPaymentTime = (DateTime)Cardi.LastPaymentTime;
                        DateTime CardiLastRecountTime = (DateTime)Cardi.LastRecountTime;

                        if (CardiLastPaymentTime == CardLPTime)
                        {
                            GlobalClass.PenaltyLastRecountSync = true;
                        }

                        string What = "Данные из базы:" + Environment.NewLine +
                            "CardId  " + CardIdCard.ToString() + Environment.NewLine +
                            "ParkingEnterTime  " + CardiParkingEnterTime.ToString("F") + Environment.NewLine +
                            "DateSaveCard  " + CardiDateSaveCard.ToString("F") + Environment.NewLine +
                            "Nulltime1  " + CardiNulltime1.ToString("F") + Environment.NewLine +
                            "Nulltime2  " + CardiNulltime2.ToString("F") + Environment.NewLine +
                            "Nulltime3  " + CardiNulltime3.ToString("F") + Environment.NewLine +
                            "LastPaymentTime  " + CardiLastPaymentTime.ToString("F") + Environment.NewLine +
                            "LastRecountTime  " + CardiLastRecountTime.ToString("F") + Environment.NewLine +
                            "TVP  " + Cardi.TVP.ToString() + Environment.NewLine +
                            "ClientGroupidFC  " + Cardi.ClientGroupidFC.ToString() + Environment.NewLine +
                            "ClientTypidFC  " + Cardi.ClientTypidFC.ToString() + Environment.NewLine +
                            "SumOnCard  " + Cardi.SumOnCard.ToString() + Environment.NewLine +
                            "TKVP  " + Cardi.TKVP.ToString() + Environment.NewLine +
                            "TPidFC  " + Cardi.TPidFC.ToString() + Environment.NewLine +
                            "TSidFC  " + Cardi.TSidFC.ToString() + Environment.NewLine +
                            "ZoneidFC  " + Cardi.ZoneidFC.ToString() + Environment.NewLine;
                        Logging.ToLog(What);
                    }
                    catch (Exception ee)
                    {
                        Console.WriteLine("Exception запись в лог событий: " + ee.Message);
                    }

                    if (Cardi.DateSaveCard != null)
                    {
                        if (Cardi.DateSaveCard > DateSaveCard)
                        {
                            if (Cardi.ParkingEnterTime != null)
                                _DateCard.ParkingEnterTime = (int)((DateTime)Cardi.ParkingEnterTime - datetime0).TotalSeconds;
                            else _DateCard.ParkingEnterTime = 0;
                            if (Cardi.DateSaveCard != null)
                                _DateCard.DateSaveCard = (int)((DateTime)Cardi.DateSaveCard - datetime0).TotalSeconds;
                            else _DateCard.DateSaveCard = 0;
                            if (Cardi.Nulltime1 != null)
                                _DateCard.Nulltime1 = (int)((DateTime)Cardi.Nulltime1 - datetime0).TotalSeconds;
                            else _DateCard.Nulltime1 = 0;
                            if (Cardi.Nulltime2 != null)
                                _DateCard.Nulltime2 = (int)((DateTime)Cardi.Nulltime2 - datetime0).TotalSeconds;
                            else _DateCard.Nulltime2 = 0;
                            if (Cardi.Nulltime3 != null)
                                _DateCard.Nulltime3 = (int)((DateTime)Cardi.Nulltime3 - datetime0).TotalSeconds;
                            else _DateCard.Nulltime3 = 0;
                            if (Cardi.LastPaymentTime != null)
                                _DateCard.LastPaymentTime = (int)((DateTime)Cardi.LastPaymentTime - datetime0).TotalSeconds;
                            else _DateCard.LastPaymentTime = 0;
                            if (Cardi.LastRecountTime != null)
                                _DateCard.LastRecountTime = (int)((DateTime)Cardi.LastRecountTime - datetime0).TotalSeconds;
                            else _DateCard.LastRecountTime = 0;
                            if (Cardi.TVP != null)
                                _DateCard.TVP = (int)((DateTime)Cardi.TVP - datetime0).TotalSeconds;
                            else _DateCard.TVP = 0;

                            if (Cardi.ClientGroupidFC != null)
                                _DateCard.ClientGroupidFC = (byte)Cardi.ClientGroupidFC;
                            else _DateCard.ClientGroupidFC = 0;
                            if (Cardi.ClientTypidFC != null)
                                _DateCard.ClientTypidFC = (byte)Cardi.ClientTypidFC;
                            else _DateCard.ClientTypidFC = 0;
                            if (Cardi.SumOnCard != null)
                                _DateCard.SumOnCard = (int)Cardi.SumOnCard;
                            else _DateCard.SumOnCard = 0;
                            if (Cardi.TKVP != null)
                                _DateCard.TKVP = (byte)Cardi.TKVP;
                            else _DateCard.TKVP = 0;
                            if (Cardi.TPidFC != null)
                                _DateCard.TPidFC = (byte)Cardi.TPidFC;
                            else _DateCard.TPidFC = 0;
                            if (Cardi.TSidFC != null)
                                _DateCard.TSidFC = (byte)Cardi.TSidFC;
                            else _DateCard.TSidFC = 0;
                            if (Cardi.ZoneidFC != null)
                                _DateCard.ZoneidFC = (byte)Cardi.ZoneidFC;
                            else _DateCard.ZoneidFC = 0;
                            Logging.logCard("используем данные из базы");
                        }
                        else
                        {
                            Logging.logCard("используем данные с физ карты. DataSaveCard в базе меньше");
                        }

                        //Logging.logCard("после проверки в базе");      // запись данных с карты
                    }
                    else
                    {
                        Logging.logCard("используем данные с физ карты. DataSaveCard в базе = NULL");
                    }
                }
                else
                {
                    Logging.logCard("используем данные с физ карты. нет карты в БД или CardsOnline выкл.");
                }

            }
            catch (Exception e)
            {
                Logging.ToLog("Exception CardsOnline. " + e.ToString());
            }
            #endregion
            Save_DateCard = _DateCard;          // сохраним на всякий случай
            _DateCard2 = _DateCard;          // сохраним на всякий случай с карты или базы
        }

        public static void DataToWBlock()         // данные карты для записи на карту
        {

            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.ParkingEnterTime & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlock[i] = (byte)a;
            }
            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.LastRecountTime & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlock[i + 4] = (byte)a;
            }
            wBlock[8] = _DateCard.TSidFC;
            wBlock[9] = _DateCard.TPidFC;
            wBlock[10] = _DateCard.ZoneidFC;
            wBlock[11] = _DateCard.ClientGroupidFC;

            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.SumOnCard & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlock[i + 12] = (byte)a;
            }
            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.LastPaymentTime & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlock[i + 16] = (byte)a;
            }
            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.Nulltime1 & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlock[i + 20] = (byte)a;
            }

            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.Nulltime2 & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlock[i + 24] = (byte)a;
            }
            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.Nulltime3 & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlock[i + 28] = (byte)a;
            }
            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.TVP & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlock[i + 32] = (byte)a;
            }
            wBlock[36] = _DateCard.TKVP;
            wBlock[37] = _DateCard.ClientTypidFC;

            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.DateSaveCard & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlock[i + 44] = (byte)a;
            }
        }

        public static void DataToWBlockPP()         // данные карты для записи на карту
        {
            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.ParkingEnterTime & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlockPP[i] = (byte)a;
            }
            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.LastRecountTime & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlockPP[i + 4] = (byte)a;
            }
            wBlockPP[8] = _DateCard.TSidFC;
            wBlockPP[9] = _DateCard.TPidFC;
            wBlockPP[10] = _DateCard.ZoneidFC;
            wBlockPP[11] = _DateCard.ClientGroupidFC;

            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.SumOnCard & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlockPP[i + 12] = (byte)a;
            }
            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.LastPaymentTime & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlockPP[i + 16] = (byte)a;
            }
            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.Nulltime1 & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlockPP[i + 20] = (byte)a;
            }

            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.Nulltime2 & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlockPP[i + 24] = (byte)a;
            }
            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.Nulltime3 & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlockPP[i + 28] = (byte)a;
            }
            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.TVP & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlockPP[i + 32] = (byte)a;
            }
            wBlockPP[36] = _DateCard.TKVP;
            wBlockPP[37] = _DateCard.ClientTypidFC;

            for (int i = 0; i < 4; i++)
            {
                int a = (_DateCard.DateSaveCard & (255 << (i * 8))) >> (i * 8);  // выбор байта, начиная с младшего
                wBlockPP[i + 44] = (byte)a;
            }
        }

        public static void obnulim()  // обнулятор исходных данных
        {
            Itog = 0;
            _DateCard.ZoneidFC = 0;
            _DateCard.SumOnCard = 0;
            _DateCard.ParkingEnterTime = 0;
            _DateCard.LastRecountTime = 0;
            CardIdCard = 0;
            CardIdCardS = "0";
            _DateCard.ClientGroupidFC = 0;
            _DateCard.TSidFC = 0;
            _DateCard.TPidFC = 0;
            _DateCard.LastPaymentTime = 0;
            _DateCard.ClientTypidFC = 0;
        }

        
    }
}
