﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stoyka01
{
    public partial class frmUZMsg : Form
    {
        
       const short SWP_NOMOVE = 0X2;
       const short SWP_NOSIZE = 1;
       const short SWP_NOZORDER = 0X4;
       const int SWP_SHOWWINDOW = 0x0040;
       
        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

        public frmUZMsg(string txt, int x, int y)
        {
            InitializeComponent();

            this.Text = txt;
            lblText.Text = txt;
            x -= this.Width / 2;
            y -= this.Height / 2;
            SetWindowPos(this.Handle, 0, x, y, 0, 0, SWP_NOSIZE);
            Refresh();
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
