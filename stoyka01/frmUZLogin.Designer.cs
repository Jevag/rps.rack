﻿namespace stoyka01
{
    partial class frmUZLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblPass = new System.Windows.Forms.Label();
            this.cmdEntry = new System.Windows.Forms.Button();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRegion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPhonePrefix = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtPassword.Location = new System.Drawing.Point(150, 95);
            this.txtPassword.MaxLength = 20;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(322, 38);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPassword_KeyDown);
            this.txtPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPassword_KeyPress);
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPhone.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblPhone.Location = new System.Drawing.Point(14, 38);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(87, 24);
            this.lblPhone.TabIndex = 2;
            this.lblPhone.Text = "Telefon:";
            // 
            // lblPass
            // 
            this.lblPass.AutoSize = true;
            this.lblPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPass.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblPass.Location = new System.Drawing.Point(12, 98);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(91, 31);
            this.lblPass.TabIndex = 3;
            this.lblPass.Text = "Parol:";
            // 
            // cmdEntry
            // 
            this.cmdEntry.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cmdEntry.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmdEntry.Location = new System.Drawing.Point(328, 152);
            this.cmdEntry.Name = "cmdEntry";
            this.cmdEntry.Size = new System.Drawing.Size(142, 41);
            this.cmdEntry.TabIndex = 4;
            this.cmdEntry.Text = "kirmoq";
            this.cmdEntry.UseVisualStyleBackColor = false;
            this.cmdEntry.Click += new System.EventHandler(this.cmdEntry_Click);
            // 
            // txtPhone
            // 
            this.txtPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtPhone.Location = new System.Drawing.Point(328, 34);
            this.txtPhone.MaxLength = 7;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(144, 38);
            this.txtPhone.TabIndex = 2;
            this.txtPhone.TextChanged += new System.EventHandler(this.txtPhone_TextChanged);
            this.txtPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(109, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 42);
            this.label3.TabIndex = 6;
            this.label3.Text = "+";
            // 
            // txtRegion
            // 
            this.txtRegion.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtRegion.Location = new System.Drawing.Point(150, 34);
            this.txtRegion.MaxLength = 3;
            this.txtRegion.Name = "txtRegion";
            this.txtRegion.Size = new System.Drawing.Size(57, 38);
            this.txtRegion.TabIndex = 0;
            this.txtRegion.TextChanged += new System.EventHandler(this.txtRegion_TextChanged);
            this.txtRegion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRegion_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(207, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 42);
            this.label4.TabIndex = 7;
            this.label4.Text = "(";
            // 
            // txtPhonePrefix
            // 
            this.txtPhonePrefix.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtPhonePrefix.Location = new System.Drawing.Point(237, 34);
            this.txtPhonePrefix.MaxLength = 3;
            this.txtPhonePrefix.Name = "txtPhonePrefix";
            this.txtPhonePrefix.Size = new System.Drawing.Size(57, 38);
            this.txtPhonePrefix.TabIndex = 1;
            this.txtPhonePrefix.TextChanged += new System.EventHandler(this.txtPhonePrefix_TextChanged);
            this.txtPhonePrefix.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhonePrefix_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(297, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 42);
            this.label5.TabIndex = 9;
            this.label5.Text = ")";
            // 
            // frmUZLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GrayText;
            this.ClientSize = new System.Drawing.Size(484, 216);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtPhonePrefix);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtRegion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.cmdEntry);
            this.Controls.Add(this.lblPass);
            this.Controls.Add(this.lblPhone);
            this.Controls.Add(this.txtPassword);
            this.MaximizeBox = false;
            this.Name = "frmUZLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "iltimos tizimga kiring";
            this.Activated += new System.EventHandler(this.frmUZLogin_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmUZLogin_FormClosed);
            this.Load += new System.EventHandler(this.frmUZLogin_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmUZLogin_KeyDown);
            this.Leave += new System.EventHandler(this.frmUZLogin_Leave);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Button cmdEntry;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRegion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPhonePrefix;
        private System.Windows.Forms.Label label5;
    }
}