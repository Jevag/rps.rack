﻿using Microsoft.Win32;
using RPSLinphone;
//using sipdotnet;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stoyka01
{
    public class linphone
    {
        public RPSLinphone.RPSLinphone rpsl;

        //public RPSLinphone.Phone phone { get; set; }
        public bool old_calling=false;

        public SoundPlayer calling;
        public SoundPlayer callto;
        public SoundPlayer endcall;
        public SoundPlayer noanswer;
        public SoundPlayer bad;

        public bool IsPushed = false;

        public bool RequestSended = false;
        public bool RequestSended2 = false;

        public int NewCallTimer = 0;

        public int DrebezgTimer = 0;

        public int PreCallTimer = 0;

        static bool keepRunning = true;
        static SqlConnection conn = null;

        public System.Timers.Timer tmrCheck;//таймер чекнуть соединение

        public System.Timers.Timer tmrRestart;

        public static string GetStringConnectFromRegistry()
        {
            string constr = string.Empty;

            try
            {
                RegistryKey currRegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\RPS");

                if (currRegistryKey != null && currRegistryKey.GetValue("conlocalReg") != null)
                {

                    string conlocalReg = currRegistryKey.GetValue("conlocalReg").ToString();
                    if (!string.IsNullOrEmpty(conlocalReg) && !string.IsNullOrWhiteSpace(conlocalReg))
                    {
                        Rps.Crypto.CryptoDecrypto cryp = new Rps.Crypto.CryptoDecrypto();


                        if (Regex.Match(conlocalReg, "Data Source", RegexOptions.IgnoreCase).Success)
                        {
                            var builder = new SqlConnectionStringBuilder(conlocalReg);

                            builder.Password = cryp.Decrypto(builder.Password);

                            constr = builder.ToString();
                        }
                        else
                        {
                            constr = cryp.Decrypto(conlocalReg);
                        }
                    }
                }
                else
                {
                    //LogData.Log.Instance.SaveToFile(LogData.TypeModule.ConnectDevice, $"Ошибка нет доступа к реестру CURRENT_USER\\SOFTWARE\\RPS");
                }
            }
            catch (Exception ex)
            {
                //LogData.Log.Instance.SaveToFile(LogData.TypeModule.ConnectDevice, $"Ошибка {ex.Message} {ex.StackTrace}");
            }

            return constr;
        }

        public linphone()
        {
          
            calling = new SoundPlayer(Application.StartupPath + "\\sounds\\calling.wav");
            callto = new SoundPlayer(Application.StartupPath + "\\sounds\\callto.wav");
            endcall = new SoundPlayer(Application.StartupPath + "\\sounds\\endcall.wav");
            noanswer = new SoundPlayer(Application.StartupPath + "\\sounds\\noanswer.wav");
            bad = new SoundPlayer(Application.StartupPath + "\\sounds\\bad.wav");

            tmrCheck = new System.Timers.Timer();
            tmrCheck.Interval = 1000; //5200...
            tmrCheck.Elapsed += TmrCheck_Elapsed;
            tmrCheck.Enabled = true;
            tmrCheck.AutoReset = true;

            tmrRestart = new System.Timers.Timer();
            tmrRestart.Interval = 60000; //5200...
            tmrRestart.Elapsed += TmrRestart_Elapsed;
            tmrRestart.Enabled = false;
            tmrRestart.AutoReset = true;

            //создаем подключение!!!
            string loccs = GetStringConnectFromRegistry();
            //if (loccs.Length > 10)
            //{
                string conn_string = loccs;
            //}

            try
            {
                conn = new SqlConnection(conn_string);
                conn.Open();

                rpsl = new RPSLinphone.RPSLinphone(conn);

                VoipParams vp = rpsl.voipParams;
                
                Logging.ToLog($"{vp.client_ip} -> {vp.server_ip}:{vp.server_port} by {vp.server_pwd}");
                Logging.ToLog(rpsl.GetLinphoneInfo());
                /*
                while (Program.keepRunning)
                {
                    Console.WriteLine($"isCallActive = {rpsl.isCallActive}; isConnectPhone = {rpsl.phone.CurrentConnectState}");
                    Thread.Sleep(5000);
                }
                */
            }
            catch (Exception e)
            {
                Logging.ToLog(e.ToString());
                if (conn != null)
                    conn.Close();
            }
        }

        private void TmrRestart_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            Logging.ToLog("Try to Connect...");
            rpsl.phone.Connect();
            
        }

        private void TmrCheck_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            //выводим статус коннекта и статус звонка
            try
            {
                //Logging.ToLog("isCallActive: " + rpsl.isCallActive.ToString() + "isConnectPhone: " + rpsl.phone.CurrentConnectState.ToString());
                if (rpsl.phone.CurrentConnectState == sipdotnet.Phone.ConnectState.Disconnected)
                {
                    tmrRestart.Enabled = true;
                }
                else if (rpsl.phone.CurrentConnectState == sipdotnet.Phone.ConnectState.Disconnected)
                {
                    tmrRestart.Enabled = false;
                }
            }
            catch (Exception ex) {
                Logging.ToLog(ex.Message);

            }

            if (NewCallTimer > 0 && IsPushed) NewCallTimer--;
            if (DrebezgTimer > 0) DrebezgTimer--;

            if (IsPushed && NewCallTimer == 0 && !rpsl.isCallActive)
            {
                IsPushed = false;
                //Проигрываем отсутствует
                noanswer.Play();
            }
        }


        public async Task<bool> PushFirstTime(string srvUrl, string rackid)
        {
            try
            {
                //LastAnswer = "empty_answer";
                Logging.ToLog("Запрос к серверу");
                Logging.ToLog(srvUrl + "/action/DeviceCall?T=DeviceRackModel&A=I&DeviceId=" + rackid);
                string data = "";
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(srvUrl + "/action/DeviceCall?T=DeviceRackModel&A=I&DeviceId=" + rackid);
                HttpContent responseContent = response.Content;
                using (StreamReader reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
                {
                    data = await reader.ReadToEndAsync();
                    Debug.Print(data);
                    if (data == "action")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> PushSecondTime(string srvUrl, string rackid)
        {
            try
            {
                Logging.ToLog("Запрос к серверу 2");
                Logging.ToLog(srvUrl + "/action/StopCall?DeviceId=" + rackid);
                //LastAnswer = "empty_answer";
                string data = "";
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(srvUrl + "/action/StopCall?DeviceId=" + rackid);
                HttpContent responseContent = response.Content;
                using (StreamReader reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
                {
                    data = await reader.ReadToEndAsync();
                    Debug.Print(data);
                    return true;

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //переделка!!!
        /*
        public bool PushFirstTime(string srvUrl, string rackid)
        {
            try
            {
                //LastAnswer = "empty_answer";
                //rpsl.phone.Connect();
                Logging.ToLog("Запрос к серверу sip_client 1");
                return true;
                
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> PushSecondTime(string srvUrl, string rackid)
        {
            try
            {
                Logging.ToLog("Запрос к серверу sip_client2");
                return true;
                
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        */
    }
}
