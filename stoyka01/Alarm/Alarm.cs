﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static stoyka01.GlobalClass;

namespace stoyka01
{
    public class Alarm
    {
        public static void alarmW(int id, int? alarmColorId, string message) // запись новой тревоги
        {
            Guid alarmWG = Guid.NewGuid();
            try
            {
                using (SqlConnection connect = new SqlConnection(conlocal))
                {
                    connect.Open();
                    try
                    {
                        DataTable AlarmModel = UtilsBase.FillTable("select * from AlarmModel where TypeId =" + id.ToString() + " and [End] = NULL", connect, null, null);
                        if (AlarmModel == null || AlarmModel.Rows.Count == 0)
                        {

                            DataTable MessageTypeModel = UtilsBase.FillTable("select * from MessageTypeModel where Id =" + id.ToString(), connect, null, null);
                            if (MessageTypeModel != null && MessageTypeModel.Rows.Count > 0)
                            {
                                foreach (DataRow mt in MessageTypeModel.Rows)
                                {
                                    alarmColorId = mt.GetInt("Color").Value;
                                }
                            }
                            List<object> lstt = new List<object>();
                            lstt.Add("Id");                         //0
                            lstt.Add(alarmWG);
                            lstt.Add("DeviceId");                   //1
                            lstt.Add(DeviceIdG);
                            lstt.Add("TypeId");                       //2
                            lstt.Add(id);
                            lstt.Add("AlarmColorId");                         //0
                            lstt.Add(alarmColorId);
                            lstt.Add("[Begin]");                   //1
                            lstt.Add(DateTime.Now);
                            lstt.Add("Value");                   //1
                            lstt.Add(message);
                            lstt.Add("ValueEnd");                       //2
                            lstt.Add(" ");
                            lstt.Add("_IsDeleted");                   //1
                            lstt.Add(false);

                            try
                            {
                                UtilsBase.InsertCommand("AlarmModel", connect, null, lstt.ToArray());
                            }
                            catch (Exception e)
                            {
                                Logging.ToLog("Exception alarmW " + e);
                            }

                            if (logTyp == 2)
                            {
                                if (log != null)
                                {
                                    if (alarmColorId > 0)
                                        log.LogEvent((int)alarmColorId, alarmWG, DateTime.Now);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Logging.ToLog("Exception alarmW " + e);
                    }
                }
            }
            catch { }
        }

        public static void alarmNotW(int id, string message)  // запись конца тревоги
        {
            if (SettingsInit.DisposeNewEntities())
            {
                // найти все не закрытые тревоги "диспенсер не отвечает" и закрыть
                var query =
                    from ord in db.AlarmModel
                    where (ord.TypeId == id & ord.End == null)
                    select ord;

                if (query.Count() != 0)            // Execute the query, and change the column values 
                                                   // you want to change.
                    foreach (var ord in query)
                    {
                        ord.End = DateTime.Now;
                        ord.ValueEnd = message;
                        // Insert any additional changes to column values.
                    }
                SettingsInit.SaveChanges("Исключение при записи тревоги ");
            }
        }

        public static void AlarmCancel()
        {
            alarmNotW(101, GlobalClass.CurrentLangDictionary[10225]); // "Есть связь с внешним ридером"); // запись конца тревог
            alarmNotW(102, GlobalClass.CurrentLangDictionary[10227]); // "чтение/запись"); // запись новой тревоги
            alarmNotW(103, GlobalClass.CurrentLangDictionary[10226]); // "Есть связь с ридером"); // запись новой тревоги
            alarmNotW(104, GlobalClass.CurrentLangDictionary[10227]); // "чтение/запись"); // запись новой тревоги
            alarmNotW(105, " "); // запись новой тревоги "не мало карт"
            alarmNotW(106, GlobalClass.CurrentLangDictionary[10228]); // "есть карты"); // запись новой тревоги
            alarmNotW(107, " "); // запись новой тревоги
            alarmNotW(108, GlobalClass.CurrentLangDictionary[10229]); // "Есть связь с диспенсером"); // запись конца тревоги
            alarmNotW(109, GlobalClass.CurrentLangDictionary[10230]); // "Есть связь cо Slave"); // запись новой тревоги
            alarmNotW(110, "Diskret ok"); // запись новой тревоги
            alarmNotW(112, GlobalClass.CurrentLangDictionary[10231]); // "верхняя дверь закрыта"); // запись новой тревоги
            alarmNotW(113, GlobalClass.CurrentLangDictionary[10232]); // "нижняя дверь закрыта"); // запись новой тревоги
            alarmNotW(116, GlobalClass.CurrentLangDictionary[10255]); // "Петля А не занята"); // запись новой тревоги
            alarmNotW(117, GlobalClass.CurrentLangDictionary[10256]); // "Петля В не занята"); // запись новой тревоги
            alarmNotW(118, GlobalClass.CurrentLangDictionary[10257]); // "ИК не занят"); // запись новой тревоги
            alarmNotW(120, GlobalClass.CurrentLangDictionary[10251]); // "Есть лицензия" запись новой тревоги

            //newAlarms
            alarmNotW(121, CurrentLangDictionary[10453]);
            alarmNotW(122, CurrentLangDictionary[10454]);
            alarmNotW(123, CurrentLangDictionary[10455]);
            alarmNotW(124, CurrentLangDictionary[10456]);
            alarmNotW(125, CurrentLangDictionary[10457]);
            alarmNotW(126, CurrentLangDictionary[10458]);
            alarmNotW(127, CurrentLangDictionary[10459]);
            alarmNotW(128, CurrentLangDictionary[10460]);
            alarmNotW(129, CurrentLangDictionary[10461]);
            alarmNotW(130, CurrentLangDictionary[10462]);
            alarmNotW(131, CurrentLangDictionary[10463]);
            alarmNotW(132, CurrentLangDictionary[10464]);
            alarmNotW(133, CurrentLangDictionary[10465]);
            alarmNotW(134, CurrentLangDictionary[10466]);
            alarmNotW(135, CurrentLangDictionary[10467]);
            alarmNotW(136, CurrentLangDictionary[10468]);
            alarmNotW(137, CurrentLangDictionary[10469]);
            alarmNotW(138, CurrentLangDictionary[10470]);
            alarmNotW(191, CurrentLangDictionary[10471]);
            alarmNotW(192, CurrentLangDictionary[10472]);
            Logging.ToLogD("Отмена тревог OK");
        }

        public static void Alarm121()
        {
            if (OneAlarmEnabled)
            {
                alarmW(121, 3, CurrentLangDictionary[10473]);
                timerAlarm121 = OneAlarmTime * 60;
            }
        }

        public static void Alarm122()
        {
            if (OneAlarmEnabled)
            {
                alarmW(122, 3, CurrentLangDictionary[10474]);
                timerAlarm122 = OneAlarmTime * 60;
            }
        }

        public static void Alarm123()
        {
            if (OneAlarmEnabled)
            {
                alarmW(123, 3, CurrentLangDictionary[10475]);
                timerAlarm123 = OneAlarmTime * 60;
            }
        }

        public static void Alarm124()
        {
            if (OneAlarmEnabled)
            {
                alarmW(124, 3, CurrentLangDictionary[10476]);
                timerAlarm124 = OneAlarmTime * 60;
            }
        }

        public static void Alarm125()
        {
            if (OneAlarmEnabled)
            {
                alarmW(125, 3, CurrentLangDictionary[10477]);
                timerAlarm125 = OneAlarmTime * 60;
            }
        }

        //нет карты в базе
        public static void Alarm126()
        {
            if (OneAlarmEnabled)
            {
                alarmW(126, 3, CurrentLangDictionary[10478]);
                timerAlarm126 = OneAlarmTime * 60;
            }
        }

        public static void Alarm126Z()
        {
            if (OneAlarmEnabled)
            {
                alarmW(126, 3, CurrentLangDictionary[10478]);
                timerAlarm126 = OneAlarmTime * 60;
            }
        }

        //нет мест компании
        public static void Alarm127()
        {
            if (OneAlarmEnabled)
            {
                alarmW(127, 3, CurrentLangDictionary[10479]);
                timerAlarm127 = OneAlarmTime * 60;
            }
        }

        public static void Alarm127Z()
        {
            if (OneAlarmEnabled)
            {
                alarmW(127, 3, CurrentLangDictionary[10479]);
                timerAlarm127 = OneAlarmTime * 60;
            }
        }

        public static void Alarm128()
        {
            if (OneAlarmEnabled)
            {
                alarmW(128, 3, CurrentLangDictionary[10480]);
                timerAlarm128 = OneAlarmTime * 60;
            }
        }

        public static void Alarm129()
        {
            if (OneAlarmEnabled)
            {
                alarmW(129, 3, CurrentLangDictionary[10481]);
                timerAlarm129 = OneAlarmTime * 60;
            }
        }

        public static void Alarm130()
        {
            if (OneAlarmEnabled)
            {
                alarmW(130, 3, CurrentLangDictionary[10482]);
                timerAlarm130 = OneAlarmTime * 60;
            }
        }

        public static void Alarm131()
        {
            if (OneAlarmEnabled)
            {
                alarmW(131, 3, CurrentLangDictionary[10483]);
                timerAlarm131 = OneAlarmTime * 60;
            }
        }

        public static void Alarm132()
        {
            if (OneAlarmEnabled)
            {
                alarmW(132, 3, CurrentLangDictionary[10484]);
                timerAlarm132 = OneAlarmTime * 60;
            }
        }

        public static void Alarm133()
        {
            if (OneAlarmEnabled)
            {
                alarmW(133, 3, CurrentLangDictionary[10485]);
                timerAlarm133 = OneAlarmTime * 60;
            }
        }

        public static void Alarm134()
        {
            if (OneAlarmEnabled)
            {
                alarmW(134, 3, CurrentLangDictionary[10486]);
                timerAlarm134 = OneAlarmTime * 60;
            }
        }

        public static void Alarm135()
        {
            if (OneAlarmEnabled)
            {
                alarmW(135, 3, CurrentLangDictionary[10487]);
                timerAlarm135 = OneAlarmTime * 60;
            }
        }

        public static void Alarm136()
        {
            if (OneAlarmEnabled)
            {
                alarmW(136, 3, CurrentLangDictionary[10488]);
                timerAlarm136 = 30;
            }
        }

        public static void Alarm137()
        {
            if (OneAlarmEnabled)
            {
                alarmW(137, 3, CurrentLangDictionary[10489]);
                timerAlarm137 = OneAlarmTime * 60;
            }
        }

        public static void Alarm138()
        {
            if (OneAlarmEnabled)
            {
                alarmW(138, 3, CurrentLangDictionary[10490]);
                timerAlarm138 = OneAlarmTime * 60;
            }
        }

        //Новые алармы привести ко старым в итоге

        #region HopperAlarms
            
        public static void Alarm191()
        {
            if (OneAlarmEnabled)
            {
                alarmW(191, 3, CurrentLangDictionary[10491]);
                timerAlarm191 = OneAlarmTime * 60;
            }
        }

        public static void Alarm192()
        {
            if (OneAlarmEnabled)
            {
                alarmW(192, 3, CurrentLangDictionary[10492]);
                timerAlarm192 = OneAlarmTime * 60;
            }
        }
        
        #endregion

        #region KKMAlarms

        public static void Alarm281()
        {
            if (!Alarm281Enabled)
            {
                alarmW(281, 2, CurrentLangDictionary[10493]);
                Alarm281Enabled = true;
            }
        }

        public static void Alarm286()
        {
            if (!Alarm286Enabled)
            {
                alarmW(286, 2, CurrentLangDictionary[10494]);
                Alarm286Enabled = true;
            }
        }

        public static void Alarm287()
        {
            if (!Alarm287Enabled)
            {
                alarmW(287, 2, CurrentLangDictionary[10495]);
                Alarm287Enabled = true;
            }
        }

        public static void Alarm288()
        {
            if (!Alarm288Enabled)
            {
                alarmW(288, 2, CurrentLangDictionary[10496]);
                Alarm288Enabled = true;
            }
        }

        public static void Alarm289()
        {
            if (!Alarm289Enabled)
            {
                alarmW(289, 2, CurrentLangDictionary[10497]);
                Alarm289Enabled = true;
            }
        }

        public static void Alarm290()
        {
            if (!Alarm290Enabled)
            {
                alarmW(290, 2, CurrentLangDictionary[10498]);
                Alarm290Enabled = true;
            }
        }

        public static void Alarm291()
        {
            if (!Alarm291Enabled)
            {
                alarmW(291, 2, CurrentLangDictionary[10499]);
                Alarm291Enabled = true;
            }
        }

        public static void Alarm292()
        {
            if (!Alarm292Enabled)
            {
                alarmW(292, 2, CurrentLangDictionary[10500]);
                Alarm292Enabled = true;
            }
        }

        public static void Alarm282()
        {
            if (!Alarm282Enabled)
            {
                alarmW(282, 2, CurrentLangDictionary[10501]);
                Alarm282Enabled = true;
            }
        }

        public static void Alarm283()
        {
            if (!Alarm283Enabled)
            {
                alarmW(283, 2, CurrentLangDictionary[10502]);
                Alarm283Enabled = true;
            }
        }

        public static void Alarm284()
        {
            if (!Alarm284Enabled)
            {
                alarmW(284, 2, "Ошибка закрытия смены");
                Alarm284Enabled = true;
            }
        }

        public static void Alarm285()
        {
            if (!Alarm285Enabled)
            {
                Logging.ToLog("Прочие ошибки ККМ");
                alarmW(285, 2, "Прочие ошибки ККМ");
                Alarm285Enabled = true;
            }
        }

        public static void Alarm293()
        {
            if (!Alarm293Enabled)
            {
                Logging.ToLog("Суперкнопка не отвечает");
                alarmW(293, 2, "Суперкнопка не отвечает");
                Alarm293Enabled = true;
            }
        }

        public static void Alarm293Cancel()
        {
            if (Alarm293Enabled)
            {
                alarmNotW(293, "Суперкнопка работает");
                Alarm293Enabled = false;
            }
        }

        public static void CancelKKMAlarms()
        {
            if (Alarm281Enabled)
            {
                alarmNotW(281, CurrentLangDictionary[10503]);
                Alarm281Enabled = false;
            }
            if (Alarm282Enabled)
            {
                alarmNotW(282, CurrentLangDictionary[10503]);
                Alarm282Enabled = false;
            }
            if (Alarm283Enabled)
            {
                alarmNotW(283, CurrentLangDictionary[10503]);
                Alarm283Enabled = false;
            }

            if (Alarm284Enabled)
            {
                alarmNotW(284, CurrentLangDictionary[10503]);
                Alarm284Enabled = false;
            }

            if (Alarm285Enabled)
            {
                alarmNotW(285, CurrentLangDictionary[10503]);
                Alarm285Enabled = false;
            }

            if (Alarm286Enabled)
            {
                alarmNotW(286, CurrentLangDictionary[10503]);
                Alarm286Enabled = false;
            }
            if (Alarm287Enabled)
            {
                alarmNotW(287, CurrentLangDictionary[10503]);
                Alarm287Enabled = false;
            }
            if (Alarm288Enabled)
            {
                alarmNotW(288, CurrentLangDictionary[10503]);
                Alarm288Enabled = false;
            }
            if (Alarm289Enabled)
            {
                alarmNotW(289, CurrentLangDictionary[10503]);
                Alarm289Enabled = false;
            }
            if (Alarm290Enabled)
            {
                alarmNotW(290, CurrentLangDictionary[10503]);
                Alarm290Enabled = false;
            }
            if (Alarm291Enabled)
            {
                alarmNotW(291, CurrentLangDictionary[10503]);
                Alarm291Enabled = false;
            }
            if (Alarm292Enabled)
            {
                alarmNotW(291, CurrentLangDictionary[10503]);
                Alarm291Enabled = false;
            }
        }

        #endregion

        #region TicketAlarms
        public static void Alarm170()
        {
            if (!Alarm170Enabled)
            {
                alarmW(170, 2, "Мало билетов в принтере билетов");
                Alarm170Enabled = true;
            }
        }

        public static void Alarm170Cancel()
        {
            if (Alarm170Enabled)
            {
                alarmNotW(170, "Есть билеты в принтере билетов");
                Alarm170Enabled = false;
            }
        }


        #endregion

        #region OtherNewAlarms
        /*
        public static void Alarm171()
        {
            if (!Alarm171Enabled)
            {
                alarmW(171, 2, "Цепь питания разомкнута");
                Alarm171Enabled = true;
            }
        }

        public static void Alarm171Cancel()
        {
            if (Alarm171Enabled)
            {
                alarmNotW(171, "Цепь питания в порядке");
                Alarm171Enabled = false;
            }
        }
        */

        
        public static void Alarm172()
        {
            if (!Alarm172Enabled)
            {
                alarmW(172, 2, "Пожарная тревога");
                Alarm172Enabled = true;
            }
        }

        public static void Alarm172Cancel()
        {
            if (Alarm172Enabled)
            {
                alarmNotW(172, "Отмена пожарной тревоги");
                Alarm172Enabled = false;
            }
        }
        
        #endregion


        public static void alarmR()       // запись тревог в БД
        {
            if (statReadOutN != _statReadOut && statReadOutN == "не отвечает")
            {
                timerAlarm101 = timeAlarm;
            }
            if (statReadOutN != "не отвечает")
            {
                timerAlarm101 = 0;
            }
            if (timerAlarm101 == 1)
            {
                timerAlarm101 = 0;
                alarmW(101, 1, CurrentLangDictionary[10208]); //"Нет связи"); // запись новой тревоги
            }

            if (statReadOutN != _statReadOut && (statReadOutN == "ошибка чтения" || statReadOutN == "ошибка записи"))
            {
                timerAlarm102 = timeAlarm;
            }
            if (statReadOutN != "ошибка чтения" && statReadOutN != "ошибка записи")
            {
                timerAlarm102 = 0;
            }
            if (timerAlarm102 == 1)
            {
                timerAlarm102 = 0;
                //alarmW(102, 1, CurrentLangDictionary[10209]); //"Ошибка чтения/записи"); // запись новой тревоги ()
            }

            if (statReadInN != _statReadIn && statReadInN == "не отвечает")
            {
                timerAlarm103 = timeAlarm;
            }
            if (statReadInN != "не отвечает")
            {
                timerAlarm103 = 0;
            }
            if (timerAlarm103 == 1)
            {
                timerAlarm103 = 0;
                alarmW(103, 1, CurrentLangDictionary[10210]); //"Нет связи"); // запись новой тревоги
            }

            if (statReadInN != _statReadIn && (statReadInN == "ошибка чтения" | statReadInN == "ошибка записи"))
            {
                timerAlarm104 = timeAlarm;
            }
            if (statReadInN != "ошибка чтения" && statReadInN != "ошибка записи")
            {
                timerAlarm104 = 0;
            }
            if (timerAlarm104 == 1)
            {
                timerAlarm104 = 0;
                alarmW(104, 1, CurrentLangDictionary[10211]); //"Ошибка чтения/записи"); // запись новой тревоги
            }

            if (statusCard != _statusCard && statusCard == "мало карт" && RackType == "Въезд")
            {
                timerAlarm105 = timeAlarm;
            }
            if (statusCard != "мало карт" || RackType != "Въезд")
            {
                timerAlarm105 = 0;
            }

            if (timerAlarm105 == 1)
            {
                timerAlarm105 = 0;
                alarmW(105, 3, CurrentLangDictionary[10212]); //"мало карт"); // запись новой тревоги
            }

            if (statusCard != _statusCard && statusCard == "нет карт" && RackType == "Въезд")
            {
                timerAlarm106 = timeAlarm;
            }
            if (statusCard != "нет карт" || RackType != "Въезд")
            {
                timerAlarm106 = 0;
            }
            if (timerAlarm106 == 1)
            {
                timerAlarm106 = 0;
                alarmW(106, 1, CurrentLangDictionary[10213]); //"нет карт"); // запись новой тревоги
            }

            if (statDispensN != _statDispens && statDispensN == "карта застряла")
            {
                timerAlarm107 = timeAlarm;
            }
            if (statDispensN != "карта застряла")
            {
                timerAlarm107 = 0;
            }
            if (timerAlarm107 == 1)
            {
                timerAlarm107 = 0;
                alarmW(107, 1, CurrentLangDictionary[10214]); //"карта застряла"); // запись новой тревоги
            }


            if (statDispensN != _statDispens && statDispensN == "не отвечает")
            {
                timerAlarm108 = timeAlarm;
            }
            if (statDispensN != "не отвечает")
            {
                timerAlarm108 = 0;
            }
            if (timerAlarm108 == 1)
            {
                timerAlarm108 = 0;
                alarmW(108, 1, CurrentLangDictionary[10215]); //"Нет связи c диспенсером"); // запись новой тревоги
            }

            if (statSlave != _statSlave & statSlave == "не отвечает")
            {
                timerAlarm109 = timeAlarm;
            }
            if (statSlave != "не отвечает")
            {
                timerAlarm109 = 0;
            }
            if (timerAlarm109 == 1)
            {
                timerAlarm109 = 0;
                alarmW(109, 1, CurrentLangDictionary[10216]); //"Нет связи cо Slave"); // запись новой тревоги
            }

            if (statSlave != _statSlave & statSlave == "Diskret er")
            {
                timerAlarm110 = timeAlarm;
            }
            if (statSlave != "Diskret er")
            {
                timerAlarm110 = 0;
            }
            if (timerAlarm110 == 1)
            {
                timerAlarm110 = 0;
                alarmW(110, 1, CurrentLangDictionary[10217]); //"Ошибка Дискрета"); // запись новой тревоги
            }

            if (timerAlarm154 == 1)
            {
                timerAlarm154 = 0;
                alarmNotW(154, CurrentLangDictionary[10504]);
            }

            //--
            if (statDoorUpN != _statDoorUp & statDoorUpN == "верхняя дверь открыта")
            {
                timerAlarm112 = timeAlarm;
            }
            if (statDoorUpN != "верхняя дверь открыта")
            {
                timerAlarm112 = 0;
            }
            if (timerAlarm112 == 1)
            {
                timerAlarm112 = 0;
                alarmW(112, 3, CurrentLangDictionary[10219]); //"верхняя дверь открыта"); // запись новой тревоги
            }
            //--
            if (statDoorDnN != _statDoorDn & statDoorDnN == "нижняя дверь открыта")
            {
                timerAlarm113 = timeAlarm;
            }
            if (statDoorDnN != "нижняя дверь открыта")
            {
                timerAlarm113 = 0;
            }
            if (timerAlarm113 == 1)
            {
                timerAlarm113 = 0;
                alarmW(113, 3, CurrentLangDictionary[10220]); //"нижняя дверь открыта"); // запись новой тревоги
            }
            //--

            if (_timer_LoopA == 1 & timer_LoopA == 0)
            {
                alarmW(116, 2, CurrentLangDictionary[10252]); //"Петля А долго занята"); // запись новой тревоги
                _timer_LoopA = 0;
            }

            if (_timer_LoopB == 1 & timer_LoopB == 0)
            {
                alarmW(117, 2, CurrentLangDictionary[10253]); //"Петля В долго занята"); // запись новой тревоги
                _timer_LoopB = 0;
            }

            if (_timer_IR == 1 & timer_IR == 0)
            {
                alarmW(118, 2, CurrentLangDictionary[10254]); //"ИК долго занят"); // запись новой тревоги
                _timer_IR = 0;
            }


            if (LicenseWork != _LicenseWork & LicenseWork == false)
            {
                alarmW(120, 1, CurrentLangDictionary[10250]); //"Not LicenseWork"); // запись новой тревоги
                _LicenseWork = LicenseWork;
            }
            // ОТМЕНА ТРЕВОГ

            NotalarmR();       // отмена тревог в БД

            if (statDispensN != "ожидание" && statDispensN != "неопределен")
                _statDispens = statDispensN;

            _statReadIn = statReadInN;
            _statReadOut = statReadOutN;
            _statSlave = statSlave;
            _statusCard = statusCard;
            _statDoorDn = statDoorDnN;
            _statDoorUp = statDoorUpN;
        }

        public static void NotalarmR()       // отмена тревог в БД
        {
            
            if (!Alarm166Genered)
            {
                alarmNotW(166, CurrentLangDictionary[10505]);
            }

            if (!Alarm152Genered)
            {
                alarmNotW(152, CurrentLangDictionary[10506]);
            }

            if (statReadOutN != _statReadOut && _statReadOut == "не отвечает")
            {
                alarmNotW(101, CurrentLangDictionary[10225]); // "Есть связь с внешним ридером"); // запись конца тревог
            }

            if (statReadOutN != _statReadOut && (_statReadOut == "ошибка чтения" || _statReadOut == "ошибка записи"))
            {
                alarmNotW(102, CurrentLangDictionary[10227]); // "чтение/запись"); // запись новой тревоги
            }

            if (statReadInN != _statReadIn && _statReadIn == "не отвечает")
            {
                alarmNotW(103, CurrentLangDictionary[10226]); // "Есть связь с ридером"); // запись новой тревоги
            }

            if (statReadInN != _statReadIn && (_statReadIn == "ошибка чтения" || _statReadIn == "ошибка записи"))
            {
                alarmNotW(104, CurrentLangDictionary[10227]); // "чтение/запись"); // запись новой тревоги
            }

            if (statusCard != _statusCard && _statusCard == "мало карт")
            {
                alarmNotW(105, " "); // запись новой тревоги
            }

            if (statusCard != _statusCard && _statusCard == "нет карт")
            {
                alarmNotW(106, CurrentLangDictionary[10228]); // "есть карты"); // запись новой тревоги
            }

            if (statDispensN != _statDispens && _statDispens == "карта застряла")
            {
                alarmNotW(107, " "); // запись новой тревоги
            }

            if (statDispensN != _statDispens && statDispensN != "не отвечает" && statDispensN != "ожидание" && statDispensN != "неопределен")
            {
                alarmNotW(108, CurrentLangDictionary[10229]); // "Есть связь с диспенсером"); // запись конца тревоги
            }


            if (statSlave != _statSlave && (statSlave == "Diskret ok" || statSlave == "Diskret er" || statSlave == "SignatureSlave ok"))
            {
                alarmNotW(109, CurrentLangDictionary[10230]); // "Есть связь cо Slave"); // запись новой тревоги
            }

            if (statSlave != _statSlave && statSlave == "Diskret ok")
            {
                alarmNotW(110, "Diskret ok"); // запись новой тревоги
            }

            if (statDoorUpN != _statDoorUp && statDoorUpN == "верхняя дверь закрыта")
            {
                alarmNotW(112, CurrentLangDictionary[10231]); // "верхняя дверь закрыта"); // запись новой тревоги
            }

            if (statDoorDnN != _statDoorDn && statDoorDnN == "нижняя дверь закрыта")
            {
                alarmNotW(113, CurrentLangDictionary[10232]); // "нижняя дверь закрыта"); // запись новой тревоги
            }


            if (_timer_LoopA == 0 && timer_LoopA != 0)
            {
                alarmNotW(116, CurrentLangDictionary[10255]); // "Петля А не занята"); // запись новой тревоги
                _timer_LoopA = 1;
            }

            if (_timer_LoopB == 0 && timer_LoopB != 0)
            {
                alarmNotW(117, CurrentLangDictionary[10256]); // "Петля В не занята"); // запись новой тревоги
                _timer_LoopB = 1;
            }

            if (_timer_IR == 0 && timer_IR != 0)
            {
                alarmNotW(118, CurrentLangDictionary[10257]); // "ИК не занят"); // запись новой тревоги
                _timer_IR = 1;
            }

            //if (LicenseWork == true)
            if (LicenseWork != _LicenseWork & LicenseWork == true)
            {
                alarmNotW(120, CurrentLangDictionary[10251]); // "Есть лицензия" запись новой тревоги
            }
        }
    }
}
