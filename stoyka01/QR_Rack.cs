﻿using Newtonsoft.Json;
using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace stoyka01
{
    //QR json ETC
    public class QR_Rack
    {
        public static string CurrentExitTimeStamp = "";

        /*
         Ожидание подтверждения статуса проезда от стойки. Стойка присылает на сервер GET запрос https://expo.r-p-s.ru/api2/qr/confirm?cardId=1942943351102011010&Status=true/false
         */
        #region Entrance
        public static void SendEntranceConfirmation(string uri, string cardid, bool stat, string plnum, string dv_id)
        {
            /*
             Для Егора. В этом запросе кроме ID билета передаём еще:
1. Время въезда
2. Номер автомобиля
3. Четырёхзначный DeviceID устройства
https://office.r-p-s.ru/api2/qr/enterConfirm?EticketId=1942943351102011010&EnterTime=20200508185945&PlateNumber=X349HO198&DeviceID=1234&Status=true/false
//https://office.r-p-s.ru/api2/loopA/TagNumber=1111&DeviceID=1234
             */

            try
            {
                HttpClient client = new HttpClient();
                //string link = uri + "/api2/qr/enterConfirm?CardId=" + cardid + "&Status=" + stat.ToString();
                //DateTime dt = DateTime.Now;
                string utime = DateTime.Now.ToString("yyyyMMddHHmmss");
                //https://office.r-p-s.ru/api2/qr/enterConfirm?EticketId=1942943351102011010&EnterTime=20200508185945&PlateNumber=X349HO198&DeviceID=1234&Status=true/false
                string link = uri + "/api2/qr/enterConfirm?EticketId=" + cardid + "&EnterTime=" + utime + "&PlateNumber=" + plnum + "&DeviceID=" + dv_id + "&Status=" + stat.ToString();

                Logging.ToLog("ToServer Entrance Confirmation: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static string CreateQREntrance(string URI, string Ticket)
        {
            /*
            if (URI.IndexOf("http://") > -1)
            {
                URI = URI.Replace("http://", "");
            }
            */
            //http://office.r-p-s.ru/qr/?type=enter&code=1942943351102011010


            //новый Новый формат office.r-p-s.ru/qr/?t=1&eticket=1942943351102011010

            return URI + "t=1&eticket=" + Ticket;
        }

        /*
         //dt используем из билета или NOW?
        public static string ReturnCorrectValue(DateTime dt, string plate)
        {
            EntranceData edata = new EntranceData();
            
        }
        */
        public static bool TicketComparison(string rack, string serv)
        {
            if (rack == serv) return true;
            else return false;
        }
        
        public static string ReturnCorrectValue(string plate)
        {
            EntranceData edata = new EntranceData();
            string dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            edata.enterTime = dt;
            if (plate == "") plate = null;
            edata.enterCarNumber = plate;
            Entrance e = new Entrance();
            e.errorMessage = null;
            e.data = edata;
            return JsonConvert.SerializeObject(e);
        }
        
        //как?
        public static string ReturnWrongValue()
        {
            Entrance e = new Entrance();
            //EntranceData edata = new EntranceData();
            e.errorMessage = "Ticket ID Invalid";
            //e.data = edata;
            return JsonConvert.SerializeObject(e);
        }
        #endregion

        #region exit
        public static string CreateQRExit(string URI)
        {
            /*
            if (URI.IndexOf("http://") > -1)
            {
                URI = URI.Replace("http://", "");
            }
            */
            /*
             Новый формат: office.r-p-s.ru/qr/?t=2&d=0987&qr=123456789
             */

            string ts2015 = ServiceClass.GetTimeStamp2015();

            //Logging.ToLog(ts2015);

            string XORKey1 = "111111111";


            string CryptedTS = ""; 

            
            for (int i = 0; i < 9; i++)
            {
                byte a = Convert.ToByte(ts2015.Substring(i, 1));
                byte x = Convert.ToByte(XORKey1.Substring(i, 1));

                //Debug.Print((a ^ x).ToString());
                CryptedTS += (a ^ x).ToString();
            }
            //Logging.ToLog(CryptedTS);
            string RackNum = GlobalClass.ProductionNumber.ToString().PadLeft(4, '0');

            //Logging.ToLog(RackNum);

            string XORKey2 = "1111";

            string CryptedRackNum = "";

            for (int i = 0; i < 4; i++)
            {
                byte a = Convert.ToByte(RackNum.Substring(i, 1));
                byte x = Convert.ToByte(XORKey2.Substring(i, 1));

                //Debug.Print((a ^ x).ToString());
                CryptedRackNum += (a ^ x).ToString();
            }
            CurrentExitTimeStamp = CryptedTS;

            Logging.ToLog("cts: " + CurrentExitTimeStamp);

            /*
 Новый формат: office.r-p-s.ru/qr/?t=2&d=0987&qr=123456789
 */

            //http://office.r-p-s.ru/qr/?t=2&id=0987&qrnumber=123456789
            string mm = URI + "t=2&d=" + CryptedRackNum + "&qrnumber=" + CryptedTS;

            Logging.ToLog("QR выезд - генерируем " + mm);

            return mm; //+ Ticket;
        }

        public static bool TS2015Comparison(string serv)
        {
            if (CurrentExitTimeStamp == serv) return true;
            else return false;
        }

        /*
         https://office.r-p-s.ru/api2/qr/exitConfirm?EticketId=1942943351102011010&ExitTime=20200508185945&PlateNumber=X349HO198&DeviceID=1234&Status=tr
         */

        /*
                         HttpClient client = new HttpClient();
            //string link = uri + "/api2/qr/enterConfirm?CardId=" + cardid + "&Status=" + stat.ToString();
            //DateTime dt = DateTime.Now;
            string utime = DateTime.Now.ToString("yyyyMMddHHmmss");
            //https://office.r-p-s.ru/api2/qr/enterConfirm?EticketId=1942943351102011010&EnterTime=20200508185945&PlateNumber=X349HO198&DeviceID=1234&Status=true/false
            string link = uri + "/api2/qr/enterConfirm?EticketId=" + cardid + "&EnterTime=" + utime + "&PlateNumber=" + plnum + "&DeviceID=" + dv_id + "&Status=" + stat.ToString();

            Logging.ToLog("ToServer Entrance Confirmation: " + link);
            client.GetAsync(link);
         */


        public static void SendExitConfirmation(string uri, string cardid, bool stat, string plate, string devid)
        {
            try
            {
                //DateTime dt = DateTime.Now;
                string utime = DateTime.Now.ToString("yyyyMMddHHmmss");

                HttpClient client = new HttpClient();
                string link = uri + "/api2/qr/exitConfirm?EticketId=" + cardid + "&ExitTime=" + utime + "&PlateNumber=" + plate + "&DeviceID=" +devid + "&Status=" + stat.ToString();
                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static void SendRegularExitConfirmation(string uri, string cardid, bool stat, string plate, string devid)
        {
            try
            {
                //DateTime dt = DateTime.Now;
                string utime = DateTime.Now.ToString("yyyyMMddHHmmss");

                HttpClient client = new HttpClient();
                string link = uri + "/api2/qr/exitConfirm?RegularCardID=" + cardid + "&ExitTime=" + utime + "&PlateNumber=" + plate + "&DeviceID=" + devid + "&Status=" + stat.ToString();
                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }


        //статус в зависимости от проверок
        public static void SendExitStatus(string uri, string cardid, string stat, double amount)
        {
            /*
             https://office.r-p-s.ru/api2/qr/exitStatus?EticketId=1942943351102011010&Status=exit_allowed (Пожалуйста проезжайте)
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                if (amount <= 0)
                {
                   link = uri + "/api2/qr/exitStatus?EticketId=" + cardid + "&Status=" + stat;
                }
                else
                {
                    link = uri + "/api2/qr/exitStatus?EticketId=" + cardid + "&Status=" + stat + "&amount=" + amount.ToString();
                }
                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        //постоянник выезд
        public static void SendRegularExitStatus(string uri, string cardid, string stat, double amount, string lp)
        {
            /*
             Карта не найдена в базе https://office.r-p-s.ru/api2/qr/exitStatus?RegularCustomerCardID=1942943351102011010&RegularCardStatus=invalid_card
             */
            /*
             Карта заблокирована
https://office.r-p-s.ru/api2/qr/exitStatus?PlateNumber=X123XX77&RegularCustomerCardID=1942943351102011010&RegualrCardStatus=is_blocked 

             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                if (lp == "")
                {
                    if (amount <= 0)
                    {
                        link = uri + "/api2/qr/exitStatus?RegularCustomerCardID=" + cardid + "&RegularCardStatus=" + stat;
                    }
                    else
                    {
                        link = uri + "/api2/qr/exitStatus?RegularCustomerCardID=" + cardid + "&RegularCardStatus=" + stat + "&amount=" + amount.ToString();
                    }
                }
                else
                {
                    if (amount <= 0)
                    {
                        link = uri + "/api2/qr/exitStatus?PlateNumber=" + lp + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=" + stat;
                    }
                    else
                    {
                        link = uri + "/api2/qr/exitStatus?PlateNumber=" + lp + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=" + stat + "&amount=" + amount.ToString();
                    }
                }

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }


        public static string ReturnCorrectExit()
        {
            Exit edata = new Exit();
            return JsonConvert.SerializeObject(edata);
        }

        public static string ReturnWrongExit(string msg)
        {
            Exit edata = new Exit();
            edata.errorMessage = msg;
            return JsonConvert.SerializeObject(edata);
        }

        public static bool CardInDBExists(string card_id)
        {
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                DataTable dat =
                    UtilsBase.FillTable("select * from Cards where CardId ='" + card_id + "'", connect, null, null);
                if (dat != null && dat.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion

        #region regular_entrance
        //постоянная карта - въезд

        //на всю эту штуку - 11 запросов добавляется бул параметр с номером машины
        public static async void SendRegularNoCard(string uri, string cardid, string ticket_id, string lp)
        {
            /*
             https://office.r-p-s.ru/api2/qr/enterStatus?RegularCustomerCardID=1942943351102011010&RegularCardStatus=invalid_card&EticketIdStatus=enter_allowed
             
             new
             https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&RegularCustomerCardID=1942943351102011010&RegularCardStatus=invalid_card&EticketIdStatus=enter_allowed
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=invalid_card&EticketIdStatus=enter_allowed";
                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }
                Logging.ToLog("ToServer: " + link);
                await client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static async void SendRegularAltimeter(string uri, string cardid, string ticket_id, string lp)
        {
            /*             
             new
             https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&RegularCustomerCardID=1942943351102011010&RegularCardStatus=invalid_altimeter&EticketIdStatus=enter_allowed
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=invalid_altimeter&EticketIdStatus=enter_allowed";

                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }

                Logging.ToLog("ToServer: " + link);
                await client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static async void SendRegularInvTag(string uri, string cardid, string ticket_id, string lp)
        {
            /*             
             new
             https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&RegularCustomerCardID=1942943351102011010&RegularCardStatus=invalid_tag&EticketIdStatus=enter_allowed
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=invalid_tag&EticketIdStatus=enter_allowed";

                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }

                Logging.ToLog("ToServer: " + link);
                await client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static void SendRegularNoEntry(string uri, string cardid, string ticket_id, string lp)
        {
            /*
             https://office.r-p-s.ru/api2/qr/enterStatus?RegularCustomerCardID=1942943351102011010&RegualrCardStatus=no_entry&EticketIdStatus=enter_allowed 
            
            
            https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&RegularCustomerCardID=1942943351102011010&RegularCardStatus=invalid_tag&EticketIdStatus=enter_allowed
            (Пожалуйста проезжайте)
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=no_entry&EticketIdStatus=enter_allowed";
                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static void SendRegularBlocked(string uri, string cardid,string ticket_id, string lp)
        {
            /*
             https://office.r-p-s.ru/api2/qr/enterStatus?RegularCustomerCardID=1942943351102011010&RegualrCardStatus=сard_is_blocked&EticketIdStatus=enter_allowed 

                         new
             https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&RegularCustomerCardID=1942943351102011010&RegularCardStatus=сard_is_blocked&EticketIdStatus=enter_allowed
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id +  "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=сard_is_blocked&EticketIdStatus=enter_allowed";
                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static void SendRegularCompany(string uri, string cardid, string ticket_id, string lp)
        {
            /*
             https://office.r-p-s.ru/api2/qr/enterStatus?RegularCustomerCardID=1942943351102011010&RegualrCardStatus=сard_is_blocked&EticketIdStatus=enter_allowed 

                         new
             https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&RegularCustomerCardID=1942943351102011010&RegularCardStatus=invalid_companyplace&EticketIdStatus=enter_allowed
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=invalid_companyplace&EticketIdStatus=enter_allowed";

                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static void SendRegularZone(string uri, string cardid, string ticket_id, string lp)
        {
            /*
             https://office.r-p-s.ru/api2/qr/enterStatus?RegularCustomerCardID=1942943351102011010&RegualrCardStatus=invalid_zone&EticketIdStatus=enter_allowed 
            (Пожалуйста проезжайте)
             https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&RegularCustomerCardID=1942943351102011010&RegularCardStatus=invalid_zone&EticketIdStatus=enter_allowed
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=invalid_zone&EticketIdStatus=enter_allowed";

                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static void SendRegularGroup(string uri, string cardid, string ticket_id, string lp)
        {
            /*
             https://office.r-p-s.ru/api2/qr/enterStatus?RegularCustomerCardID=1942943351102011010&RegualrCardStatus=invalid_group&EticketIdStatus=enter_allowed
            (Пожалуйста проезжайте)
             https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&RegularCustomerCardID=1942943351102011010&RegularCardStatus=invalid_companyplace&EticketIdStatus=enter_allowed
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=invalid_group&EticketIdStatus=enter_allowed";

                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static void SendRegularInvTPTS(string uri, string cardid, string ticket_id, string lp)
        {
            /*
             https://office.r-p-s.ru/api2/qr/enterStatus?RegularCustomerCardID=1942943351102011010&RegualrCardStatus=invalid_group&EticketIdStatus=enter_allowed
            (Пожалуйста проезжайте)
             https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&RegularCustomerCardID=1942943351102011010&RegularCardStatus=invalid_tpts&EticketIdStatus=enter_allowed
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=invalid_tpts&EticketIdStatus=enter_allowed";

                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static void SendRegularInvAbon(string uri, string cardid, string ticket_id, string lp)
        {
            /*             
            (Пожалуйста проезжайте)
             https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&RegularCustomerCardID=1942943351102011010&RegularCardStatus=invalid_abonement&EticketIdStatus=enter_allowed
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=invalid_abonement&EticketIdStatus=enter_allowed";

                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static void SendRegularOK(string uri, string cardid, string ticket_id, string lp)
        {
            /*
             https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&RegularCustomerCardID=1942943351102011010&RegularCardStatus=enter_allowed

            (Пожалуйста проезжайте)
             */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&RegularCustomerCardID=" + cardid + "&RegularCardStatus=enter_allowed";

                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }
        

        //подтверждение
        //https://office.r-p-s.ru/api2/qr/enterConfirm?EticketId=1942943351102011010&EnterTime=20200508185945&RegularCardID=1942943351102011010&DeviceID=1234&Status=true/false

        /*
         После успешной транзакции въезда ПО стойки отправляет на сервер запрос с подтверждением въезда: 
https://office.r-p-s.ru/api2/qr/enterConfirm?EticketId=1942943351102011010&EnterTime=20200508185945&RegularCustomerCardID=1942943351102011010&DeviceID=1234&Status=true/false

         */
        public static void SendRegularEntranceConfirmation(string uri, string cardid, bool stat, string eticket_id, string dv_id, string lp)
        {
            /*
             Для Егора. В этом запросе кроме ID билета передаём еще:
1. Время въезда
2. Номер автомобиля
3. Четырёхзначный DeviceID устройства
             */
            try
            {
                HttpClient client = new HttpClient();
                //string link = uri + "/api2/qr/enterConfirm?CardId=" + cardid + "&Status=" + stat.ToString();
                //DateTime dt = DateTime.Now;
                string utime = DateTime.Now.ToString("yyyyMMddHHmmss");
                //https://office.r-p-s.ru/api2/qr/enterConfirm?EticketId=1942943351102011010&EnterTime=20200508185945&RegularCustomerCardID=1942943351102011010&DeviceID=1234&Status=true/false
                string link = uri + "/api2/qr/enterConfirm?EticketId=" + eticket_id + "&EnterTime=" + utime + "&RegularCustomerCardID=" + cardid + "&DeviceID=" + dv_id + "&Status=" + stat.ToString();

                if (lp != "")
                {
                    link += "&PlateNumber=" + lp;

                }

                Logging.ToLog("ToServer Regular Entrance Confirmation: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        //ошибки номера       
        public static void SendLPNotFound(string uri, string ticket_id, string lp)
        {
            /*
        Номер ТС не найден в Базе
https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&PlateNumber=X123XX77&PlateNumberStatus=invalid_plate_number&EticketIdStatus=enter_allowed. 
        */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&PlateNumber=" + lp + "&PlateNumberStatus=invalid_plate_number&EticketIdStatus=enter_allowed";

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }

        }

        public static void SendLPNotFoundExit(string uri, string lp)
        {
            /*
        Номер ТС не найден в Базе
https://office.r-p-s.ru/api2/qr/exitStatus?PlateNumber=X123XX77&PlateNumberStatus=invalid_plate_number
        */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/exitStatus?" + "PlateNumber=" + lp + "&PlateNumberStatus=invalid_plate_number";

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }

        }

        public static void SendLPBlackList(string uri, string ticket_id, string lp)
        {
            /*
 Номер в чёрном списке
https://office.r-p-s.ru/api2/qr/enterStatus?EticketId=1942943351102011010&PlateNumber=X123XX77&PlateNumberStatus=black_list&EticketIdStatus=enter_allowed. 

 */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/enterStatus?EticketId=" + ticket_id + "&PlateNumber=" + lp + "&PlateNumberStatus=black_list&EticketIdStatus=enter_allowed";

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }
        }

        public static void SendLPBlackListExit(string uri, string lp)
        {
            /*
        Номер ТС не найден в Базе
https://office.r-p-s.ru/api2/qr/exitStatus?PlateNumber=X123XX77&PlateNumberStatus=black_list. 

        */

            try
            {
                HttpClient client = new HttpClient();
                string link = "";
                link = uri + "/api2/qr/exitStatus?" + "PlateNumber=" + lp + "&PlateNumberStatus=black_list";

                Logging.ToLog("ToServer: " + link);
                client.GetAsync(link);
            }
            catch (Exception ex)
            {
            }

        }

        //Ищем карту по номеру -> "0" не нашли, "1" - в блеклисте, "другое" - номер карты
        //не очень работает поиск в БД через виртуал в том потоке...
        public static string FindQRPlateCard(string tag_id)
        {
            using (SqlConnection connect = new SqlConnection(GlobalClass.conlocal))
            {
                connect.Open();
                DataTable dat =
                    UtilsBase.FillTable(
                        "select * from VirtualCard where ISNULL(IsDeleted,0)!=1 AND TagId ='" +
                        tag_id + "'", connect, null, null);
                if (dat != null && dat.Rows.Count > 0)
                {
                    DataRow dr = dat.Rows[0];
                    string mifare_id = dr["MiFareCardId"].ToString(); //С пробелом
                    //CardIdCard = Convert.ToInt64(mifare_id);
                    //Далее отправляемся на проверки!!!
                    long mfr_lng = Convert.ToInt64(mifare_id);
                    var card = GlobalClass.db.Cards.SingleOrDefault(
                        x => x.CardId == mfr_lng & x.C_IsDeleted != true);

                    DataTable bdat =
                    UtilsBase.FillTable(
                        "select * from BlackList where ISNULL(IsDeleted,0)!=1 AND PlateNumber ='" +
                        tag_id + "'", connect, null, null);
                    if (bdat != null && bdat.Rows.Count > 0)
                    {
                        return "1";
                    }
                    return mifare_id; //возвращаем ид постоянника
                        /*
                        if (card != null)
                        {
                            ToVirtualCard(card);
                        }
                        CardIdCardS = CardIdCard.ToString();
                        */                    //sasai1                   

                    }
                else
                {
                    return "0";
                }
            }

        }

        #endregion


        #region jsonClasses
        public class EntranceData
        {
            //public string enterTransactionId { get; set; }
            public string enterTime { get; set; }
            public string enterCarNumber { get; set; }
        }

        public class Entrance
        {
            public object errorMessage { get; set; }
            public EntranceData data { get; set; }
        }

        public class Exit
        {
            public string errorMessage { get; set; }
            public object data { get; set; }
        }
        #endregion
    }
}
