using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Microsoft.SqlServer.Dac;
using Microsoft.SqlServer.Dac.Compare;
using NLog;


namespace SqlBaseImport.Model
{


    public class SchamaCompare
    {
        //private static Logger log = LogManager.GetCurrentClassLogger();
        
        /*static void Main(string[] args)
        {
            DateTime start = DateTime.Now;
            var localConnect = @"Data Source=localhost;Initial Catalog=rpsMDBG;User ID=sa;Password=parol-RPS1";
            var remoteConnect = @"Data Source=localhost;Initial Catalog=rpsMDBG_bb;User ID=sa;Password=parol-RPS1";


            var source = new SqlConnectionStringBuilder();

            source.DataSource = "localhost";
            source.InitialCatalog = "rpsMDBG";
            source.UserID = "sa";
            source.Password = "parol-RPS1";
            source.IntegratedSecurity = true;

            var target = new SqlConnectionStringBuilder();

            target.DataSource = "localhost";
            target.InitialCatalog = "NeboDB";
            target.UserID = "sa";
            target.Password = "parol-RPS1";
            target.IntegratedSecurity = true;


            //string local = Path.Combine(Directory.GetCurrentDirectory(), "local.dacpac");

            //Extract(localConnect, local);

            //string remote = Path.Combine(Directory.GetCurrentDirectory(), "remote.dacpac");

            //Extract(remoteConnect, remote);


            var result =  Compare(source.ConnectionString, target.ConnectionString);

            //var result =  CompareDac(local,remote);

            
            


            File.WriteAllText(Path.Combine(Directory.GetCurrentDirectory(), "update.sql"), result.GenerateScript("rpsMDBG").Script);

            

            
            Console.ReadKey();

        }*/
        public static SchemaComparisonResult CompareDac(string from, string to)
        {
            //var sourceDacpac = new SchemaCompareDacpacEndpoint(from);
            var sourceDatabase = new SchemaCompareDacpacEndpoint(from);
            //var csb = new SqlConnectionStringBuilder();
            //csb.DataSource = "SERVER";
            //csb.InitialCatalog = "DATABASE";
            //csb.IntegratedSecurity = true;
            var targetDatabase = new SchemaCompareDacpacEndpoint(to);
            var comparison = new SchemaComparison(sourceDatabase, targetDatabase);
            // Persist comparison file to disk in Schema Compare (.scmp) format
            // Load comparison from Schema Compare (.scmp) file
            //comparison = new SchemaComparison(@"C:\project\mycomparison.scmp");

            DacDeployOptions deo = new DacDeployOptions();
            deo.BlockOnPossibleDataLoss = false;
            deo.BackupDatabaseBeforeChanges = true;
            comparison.Options = deo;
            SchemaComparisonResult comparisonResult = comparison.Compare();
            return comparisonResult;
            //// Find the change to table1 and exclude it.
            //foreach (SchemaDifference difference in comparisonResult.Differences)
            //{
            //    if (difference.TargetObject.Name != null &&
            //        difference.TargetObject.Name.HasName &&
            //        difference.TargetObject.Name.Parts[1] == "table1")
            //    {
            //        comparisonResult.Exclude(difference);
            //        break;
            //    }
            //}

            //// Publish the changes to the target database
            //SchemaComparePublishResult publishResult = comparisonResult.PublishChangesToTarget();

            //log.Trace(publishResult.Success ? "Publish succeeded." : "Publish failed.");
        }

        public static void Compare(string from, string to)
        {

            Log.Instance.SaveInfo(TypeModule.CompareSchema,"FROM:"+from);
            Log.Instance.SaveInfo(TypeModule.CompareSchema,"to:" + to);
            DateTime start = DateTime.Now;
            //var source = new SqlConnectionStringBuilder(from);
            var targetString = new SqlConnectionStringBuilder(to);
            //var sourceDatabase = new SchemaCompareDatabaseEndpoint(from);
            var targetDatabase = new SchemaCompareDatabaseEndpoint(targetString.ConnectionString);
            //log.Debug("FROM2:" + source.ConnectionString);
            //log.Debug("to2:" + target.ConnectionString);
            string source = Path.Combine(Directory.GetCurrentDirectory(), "source.dacpac");
            Extract(from, source);
            Log.Instance.SaveInfo(TypeModule.CompareSchema,"Success extract from : "+from);
            //string target = Path.Combine(Directory.GetCurrentDirectory(), "target.dacpac");
            //Extract(to, target);
            var sourceDatabase = new SchemaCompareDacpacEndpoint(source);
            //var targetDatabase = new SchemaCompareDacpacEndpoint(to);
            var comparison = new SchemaComparison(sourceDatabase, targetDatabase);
            // Persist comparison file to disk in Schema Compare (.scmp) format
            // Load comparison from Schema Compare (.scmp) file
            //comparison = new SchemaComparison(@"C:\project\mycomparison.scmp");
            comparison.Options.BackupDatabaseBeforeChanges = true;
            comparison.Options.BlockOnPossibleDataLoss = false;
            
            SchemaComparisonResult result = comparison.Compare();
            Log.Instance.SaveInfo(TypeModule.CompareSchema,"Compare finished");
            foreach (var error in result.GetErrors())
            {
                Log.Instance.SaveError(TypeModule.CompareSchema,"Compare error: " + error.Message);
            }
            if (result.Differences.Any())
            {
                var res = result.PublishChangesToTarget();
                if (!res.Errors.Any())
                {
                    Log.Instance.SaveInfo(TypeModule.CompareSchema,"Compare with no errors");
                }
                else
                {
                    foreach (var error in res.Errors)
                    {
                        Log.Instance.SaveError(TypeModule.CompareSchema,"Publish errror: " + error.Message);
                    }
                }
            }
            Log.Instance.SaveInfo(TypeModule.CompareSchema,"Update time: " + (DateTime.Now - start).TotalSeconds + "s");
            //// Find the change to table1 and exclude it.
            //foreach (SchemaDifference difference in comparisonResult.Differences)
            //{
            //    if (difference.TargetObject.Name != null &&
            //        difference.TargetObject.Name.HasName &&
            //        difference.TargetObject.Name.Parts[1] == "table1")
            //    {
            //        comparisonResult.Exclude(difference);
            //        break;
            //    }
            //}

            //// Publish the changes to the target database
            //SchemaComparePublishResult publishResult = comparisonResult.PublishChangesToTarget();

            //log.Trace(publishResult.Success ? "Publish succeeded." : "Publish failed.");
        }

        public static void Extract(string ConnectionString, string dacPacPath)
        {
            DacServices ds = new DacServices(ConnectionString);
            DacExtractOptions deo = new DacExtractOptions();
            deo.ExtractAllTableData = false;
            deo.IgnoreExtendedProperties = false;
            var cs = new SqlConnectionStringBuilder(ConnectionString);
            ds.Extract(dacPacPath, cs.InitialCatalog, "RPS", new Version(1, 0), null, null, deo);
            //// Now we take the schema and apply it to our database
            //DacDeployOptions ddo = new DacDeployOptions();
            //ddo.CreateNewDatabase = false;
            //ddo.BlockOnPossibleDataLoss = false;
            //ddo.GenerateSmartDefaults = true;
            //ddo.VerifyDeployment = false;
            //ds.Deploy(DacPackage.Load(dacPacPath), newDbName, true, ddo);
        }
    }
}
