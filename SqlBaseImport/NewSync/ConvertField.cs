﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CodeXBase
{
    public static class ConvertField
    {
        public static DateTime? GetDateTime(this DataRow row, string field)
        {
            DateTime? result = null;
            try
            {
                result = Convert.ToDateTime(row[field]);
            }
            catch { }
            return result;
        }
        public static DateTime? GetDateTimeFromRow(this DataRow row, string field, string format = "yyyy-MM-dd")
        {
            DateTime? result = null;
            object rowdata = row[field];
            if (rowdata != null && rowdata != System.DBNull.Value)
            {
                string rowdatastr = System.Convert.ToString(rowdata);
                if (!string.IsNullOrEmpty(rowdatastr) && !string.IsNullOrWhiteSpace(rowdatastr))
                {
                    try
                    {
                        string[] strs = rowdatastr.Split(' ');
                        //string conv = "2009-09-24 00:00:00.000";
                        result = DateTime.ParseExact(strs[0], format,
                                        CultureInfo.InvariantCulture);
                    }
                    catch { }
                }
            }
            return result;
        }
        public static int? GetInt(this DataRow row, string field)
        {
            int? result = null;
            try
            {
                result = Convert.ToInt32(row[field]);
            }
            catch { }
            return result;
        }
        public static string GetString(this DataRow row, string field)
        {
            string result = string.Empty;
            try
            {
                result = Convert.ToString(row[field]);
            }
            catch { }
            return result;
        }
        public static bool? GetBool(this DataRow row, string field)
        {
            bool? result = null;
            try
            {
                result = Convert.ToBoolean(row[field]);
            }
            catch { }
            return result;
        }
        public static long? GetLong(this DataRow row, string field)
        {
            long? result = null;
            try
            {
                result = Convert.ToInt64(row[field]);
            }
            catch { }
            return result;
        }
        public static Guid? GetGuid(this DataRow row, string field)
        {
            Guid? result = null;
            try
            {
                result = Guid.Parse(row[field].ToString());
            }
            catch { }
            return result;
        }
        public static double? GetDouble(this DataRow row, string field)
        {
            double? result = null;
            object rowcurcode = row[field];
            if (rowcurcode != null && rowcurcode != System.DBNull.Value)
            {
                try
                {
                    var ci = CultureInfo.InvariantCulture.Clone() as CultureInfo;
                    ci.NumberFormat.NumberDecimalSeparator = ".";
                    result = System.Convert.ToDouble(rowcurcode);
                }
                catch (Exception ex)
                { }
            }
            return result;
        }

        public static DataRow GetRow(this object data)
        {
            DataRow result = null;

            DataRow privrow = data as DataRow;
            if (privrow != null)
            { result = privrow; }
            DataRowView privrowview = data as DataRowView;
            if (privrowview != null)
            { result = privrowview.Row; }

            return result;
        }

    }
}
