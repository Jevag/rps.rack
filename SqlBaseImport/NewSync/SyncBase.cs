﻿using CodeXBase.Schema;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase
{
    public class SyncBase
    {
        public static string GetNameField(string namefield)
        {
            string result = namefield;
            if(namefield[0]!='[')
            {
                result = "[" + namefield;
            }
            if(result[result.Length-1]!=']')
            {
                result = result + "]";
            }
            return result;
        }

        public static DataTable ReadChangeData(TableBase tableBase, IBaseService service)
        {
            DateTime zeroDate = new DateTime(2000, 1, 1);
            DateTime from = tableBase.LastUpdate;
            DateTime to = tableBase.LastChange;
            if(from<zeroDate)
            {
                from = zeroDate;
            }
            if(to<zeroDate)
            {
                to = zeroDate;
            }
            Log.Instance.SaveInfo($"Читаем какие данные были изменаны по таблице {tableBase.Name} " +
                $"за период с {from.ToString("dd.MM.yyyy HH:mm:ss")} по {to.ToString("dd.MM.yyyy HH:mm:ss")}");
            DataTable result = null;

            if (tableBase.SyncColumn != null)
            {
                string qr = $"select * from {GetNameField(tableBase.Name)} where {tableBase.SyncColumn}>@from and {tableBase.SyncColumn}<=@to";
                SortedList<string, object> prs = new SortedList<string, object>() { { "@from", from }, { "@to", to } };
                result = service.LoadData(qr, prs);
            }
            if(result!=null && result.Rows!=null && result.Rows.Count>0)
            {
                Log.Instance.SaveInfo($"По таблице {tableBase.Name} найдено {result.Rows.Count} строк которые нужно синхронизировать");
            }
            return result;
        }

        public static bool HasRow(DataRow row,string nametable, IBaseService service)
        {
            var tableBase = ContextBase.CurrentBaseDevice.FindTable(nametable);
            return HasRow(row, tableBase, service);
        }

        public static bool HasRow(DataRow row, TableBase tableBase,IBaseService service)
        {
            Log.Instance.SaveInfo("Ищем есть ли строка");
            bool result = false;
            if(tableBase!=null && tableBase.ColumnPrimary!=null)
            {
                if(row!=null && row.Table.Columns.Contains(tableBase.ColumnPrimary.Name))
                {
                    object valuekey = row[tableBase.ColumnPrimary.Name];
                    if (valuekey != null && valuekey != System.DBNull.Value)
                    {
                        Log.Instance.SaveInfo($"Ищем строку в таблице  {tableBase.Name} где {tableBase.ColumnPrimary.Name} равно {valuekey.ToString()}");
                        string qr = $"select top 1 {GetNameField(tableBase.ColumnPrimary.Name)} from {GetNameField(tableBase.Name)} where {GetNameField(tableBase.ColumnPrimary.Name)}=@key";
                        SortedList<string, object> prs = new SortedList<string, object>() { { "@key", valuekey } };
                        var resitem=service.ExecuteScalar(qr, prs);
                        result = resitem.Item1;
                        if(result)
                        {
                            Log.Instance.SaveInfo($"В таблице {tableBase.Name} есть строка где {tableBase.ColumnPrimary.Name} равно {valuekey.ToString()}");
                        }
                        else
                        {
                            Log.Instance.SaveInfo($"В таблице {tableBase.Name} нет строки где {tableBase.ColumnPrimary.Name} равно {valuekey.ToString()}");
                        }
                    }
                    else
                    {
                        Log.Instance.SaveError($"Значение первичного ключа {tableBase.ColumnPrimary.Name} в таблице {tableBase.Name} равно null");
                    }
                }
                else
                {
                    Log.Instance.SaveError($"Не найдено значение первичного ключа {tableBase.ColumnPrimary.Name} в таблице {tableBase.Name}");
                }
            }
            else
            {
                Log.Instance.SaveError($"Не найдено поле первичный ключ в таблице {tableBase.Name}");
            }
            return result;
        }

        public static SortedList<string, object> GetParametrs(DataRow row, string nametable)
        {
            var tableBase = ContextBase.CurrentBaseDevice.FindTable(nametable);
            return GetParametrs(row, tableBase);
        }

        public static SortedList<string,object> GetParametrs(DataRow row,TableBase tableBase)
        {
            Log.Instance.SaveInfo($"Формируем параметры по таблице {tableBase.Name}");
            SortedList<string, object> prs = null;
            var infoCompare = ContextBase.CompareTables(tableBase.Name);
            if (infoCompare != null && infoCompare.HasColumns != null && infoCompare.HasColumns.Count > 0)
            {
                prs = new SortedList<string, object>();
                foreach (var namefield in infoCompare.HasColumns)
                {
                    if (row != null && row.Table.Columns.Contains(namefield))
                    {
                        
                        object dat = row[namefield];
                        if (!prs.ContainsKey(namefield))
                        {
                            Log.Instance.SaveInfo($"Добавили параметр {namefield} значение {dat} по таблице {tableBase.Name}");
                            prs.Add(namefield, dat);
                        }
                    }
                }
            }
            if (prs != null)
            {
                Log.Instance.SaveInfo($"Сформировано {prs.Count} параметров по таблице {tableBase.Name}");
            }
            else
            {
                Log.Instance.SaveInfo($"Нет параметров по таблице {tableBase.Name}");

            }
            return prs;
        }

        public static List<DataLinkItem> HasLinkRow(DataRow row,TableBase table,IBaseService service)
        {
            Log.Instance.SaveInfo($"Проверяем связанные данные по таблице {table.Name}");
            List<DataLinkItem> links = new List<DataLinkItem>();
            var columns = from t in table.Columns where t.Link != null select t;
            if (columns.Any())
            {
                Log.Instance.SaveInfo($"Найдено {columns.Count()} ссылочных столбцов");

                foreach (ColumnBase col in columns)
                {
                    Log.Instance.SaveInfo($"Формируем информацию по ссылке по столбцу {col.Name} по таблице {table.Name}");
                    string tableLink = col.Link.Table.Name;
                    string linkColumn = col.Link.Name;
                    object data = row[col.Name];

                    if (data != null && data != System.DBNull.Value)
                    {
                        string qr = $"select top 1 {GetNameField(linkColumn)} from {GetNameField(tableLink)} where {GetNameField(linkColumn)}=@key";
                        var resfind = service.ExecuteScalar(qr, new SortedList<string, object>() { { "@key", data } });
                        if (!resfind.Item1)
                        {
                            links.Add(new DataLinkItem() { NameTable = tableLink, Field = linkColumn, Data = data });
                            Log.Instance.SaveInfo($"Добавлена информация по ссылке столбец {col.Name} по таблице {table.Name} значение ссылки Таблица: {tableLink} Столбец: {linkColumn} Значение: {data.ToString()}");
                        }
                    }
                }
            }
            return links;
        }

        public static void InsertRow(DataRow fromRow,TableBase table,IBaseService fromService,IBaseService toService,List<DataRow> syncrows)
        {
            string meskey = string.Empty;
            if(table.ColumnPrimary!=null)
            {
                meskey = $" по полю {table.ColumnPrimary.Name}";
            }
            if(fromRow!=null && fromRow.Table!=null && fromRow.Table.Columns.Contains(table.ColumnPrimary.Name))
            {
                meskey = meskey + $" значение {fromRow[table.ColumnPrimary.Name]}";
            }
            Log.Instance.SaveInfo($"Добавляем строку в таблицу {table.Name}");
            bool findLinks = false;
            var prs = GetParametrs(fromRow, table);
            try
            {
                toService.InsertData(table.Name, prs,table.IsIdentity);
                Log.Instance.SaveInfo($"Строка в таблицу {table.Name} {meskey} добавлена успешно");
                syncrows.Add(fromRow);
            }
            catch(Exception ex)
            {
                findLinks = true;
                Log.Instance.SaveError($"Возникла ошибка при добавлении в  таблицу {table.Name} {meskey} {ex.Message} {ex.StackTrace}");
            }
            if(findLinks)
            {
                var links = HasLinkRow(fromRow, table, toService);
                if(links!=null && links.Count>0)
                {
                    InsertLinks(links, fromService, toService);
                    try
                    {
                        toService.InsertData(table.Name, prs, table.IsIdentity);
                        Log.Instance.SaveInfo($"Строка в таблицу {table.Name} {meskey} добавлена успешно");
                        syncrows.Add(fromRow);
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.SaveError($"Возникла ошибка при добавлении в  таблицу {table.Name} {meskey} {ex.Message} {ex.StackTrace}");
                    }
                }
            }
        }

        public static void InsertLinks(List<DataLinkItem> links, IBaseService fromService, IBaseService toService)
        {
            Log.Instance.SaveInfo("Добавляем необходимые ссылки для");
            SortedList<string, object> insertParametrs = null;
            foreach (var item in links)
            {
                Log.Instance.SaveInfo($"Начинаем добавлять ссылку Таблица {item.NameTable} Поле {item.Field} Значение {item.Data}");
                bool needInsertLinks = false;
                TableBase tableBase = ContextBase.CurrentBaseDevice.FindTable(item.NameTable);
                string seltFromRow = $"select top 1 * from {item.NameTable} where {GetNameField(item.Field)}=@key";
                DataTable tbl = null;
                try
                {
                    tbl=fromService.LoadData(seltFromRow, new SortedList<string, object>() { { "@key", item.Data } });
                    Log.Instance.SaveInfo($"Успешно загружены данные по ссылке Таблица {item.NameTable} Поле {item.Field} Значение {item.Data}");
                }
                catch(Exception ex)
                {
                    Log.Instance.SaveInfo($"Возникла ошибка при загрузке данных по ссылке Таблица {item.NameTable} Поле {item.Field} Значение {item.Data} {ex.Message} {ex.StackTrace}");

                }
                if (tbl != null && tbl.Rows != null && tbl.Rows.Count > 0)
                {
                    insertParametrs = GetParametrs(tbl.Rows[0], tableBase.Name);
                }

                try
                {
                    toService.InsertData(tableBase.Name, insertParametrs, tableBase.IsIdentity);
                    needInsertLinks = false;
                    Log.Instance.SaveInfo($"Успешно добавлены данные по ссылке Таблица {item.NameTable} Поле {item.Field} Значение {item.Data}");

                }
                catch (Exception ex)
                {
                    needInsertLinks = true;
                    Log.Instance.SaveError($"Возникла ошибка при добавлении данных по ссылке Таблица {item.NameTable} Поле {item.Field} Значение {item.Data} {ex.Message} {ex.StackTrace}");

                }

                if (needInsertLinks)
                {
                    var childLinks = HasLinkRow(tbl.Rows[0], tableBase, toService);
                    InsertLinks(childLinks, fromService, toService);

                    try
                    {
                        toService.InsertData(tableBase.Name, insertParametrs, tableBase.IsIdentity);
                        Log.Instance.SaveInfo($"Успешно добавлены данные по ссылке Таблица {item.NameTable} Поле {item.Field} Значение {item.Data}");
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.SaveError($"Возникла ошибка при добавлении данных по ссылке Таблица {item.NameTable} Поле {item.Field} Значение {item.Data} {ex.Message} {ex.StackTrace}");
                    }
                }
                if (tbl != null) tbl.Dispose();
            }
        }

        public static void UpdateRow(DataRow fromRow,TableBase table, IBaseService fromService, IBaseService toService,List<DataRow> syncrows)
        {
            string meskey = string.Empty;
            if (table.ColumnPrimary != null)
            {
                meskey = $" по полю {table.ColumnPrimary.Name}";
            }
            if (fromRow != null && fromRow.Table != null && fromRow.Table.Columns.Contains(table.ColumnPrimary.Name))
            {
                meskey = meskey + $" значение {fromRow[table.ColumnPrimary.Name]}";
            }
            Log.Instance.SaveInfo($"Обновляем строку в таблице {table.Name} {meskey}");
            bool findLinks = false;
            var prs = GetParametrs(fromRow, table);
            try
            {
                if (table.ColumnPrimary != null)
                {
                    object key = fromRow[table.ColumnPrimary.Name];
                    if(prs.ContainsKey(table.ColumnPrimary.Name))
                    {
                        prs.Remove(table.ColumnPrimary.Name);
                    }
                    toService.UpdateData(table.Name, prs, $"{table.ColumnPrimary.Name}=@key", new SortedList<string, object>() { { "@key", key } });
                    Log.Instance.SaveInfo($"Успешно обновлено по таблице {table.Name} {meskey}");
                    syncrows.Add(fromRow);
                }
                else
                {
                    Log.Instance.SaveError($"Не найдено поле первичного ключа в таблице {table.Name}");
                }
            }
            catch (Exception ex)
            {
                findLinks = true;
                Log.Instance.SaveError($"Возникла ошибка при обновлении в таблице {table.Name} {meskey} {ex.Message} {ex.StackTrace}");
            }
            if (findLinks)
            {
                var links = HasLinkRow(fromRow, table, toService);
                InsertLinks(links, fromService, toService);
                try
                {
                    object key = fromRow[table.ColumnPrimary.Name];
                    toService.UpdateData(table.Name, prs, $"{table.ColumnPrimary.Name}=@key", new SortedList<string, object>() { { "@key", key } });
                    Log.Instance.SaveInfo($"Успешно обновлено по таблице {table.Name} {meskey}");
                    syncrows.Add(fromRow);
                }
                catch(Exception ex)
                {
                    Log.Instance.SaveError($"Возникла ошибка при обнолвении в таблице {table.Name} {meskey} {ex.Message} {ex.StackTrace}");
                }
            }
        }

        public static void SyncTable(TableBase table,IBaseService serviceFrom,IBaseService serviceTo)
        {
            Log.Instance.SaveInfo($"Начианаем синхронизацию по таблице {table.Name}");
            DataTable dataTable = ReadChangeData(table, serviceFrom);
            List<DataRow> syncrows = new List<DataRow>();
            if(dataTable!=null && dataTable.Rows!=null && dataTable.Rows.Count>0)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    string meskey = string.Empty;
                    if (table.ColumnPrimary != null)
                    {
                        meskey = $" по полю {table.ColumnPrimary.Name}";
                    }
                    if (row != null && row.Table != null && row.Table.Columns.Contains(table.ColumnPrimary.Name))
                    {
                        meskey = meskey + $" значение {row[table.ColumnPrimary.Name]}";
                    }

                    try
                    {
                        if (HasRow(row, table, serviceTo))
                        {
                            UpdateRow(row, table, serviceFrom, serviceTo,syncrows);
                        }
                        else
                        {
                            InsertRow(row, table, serviceFrom, serviceTo,syncrows);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.SaveError($"Возникла ошибка при синхронизации таблицы {table.Name} {meskey} {ex.Message} {ex.StackTrace}");
                    }
                }
                if (table.ColumnPrimary != null)
                {
                    if (table.IsRemove)
                    {
                        RemoveRows(table, serviceFrom, serviceTo, syncrows);
                        /*
                        if (syncrows.Count <= 900)
                        {
                            RemoveRows(table, serviceFrom,serviceTo, syncrows);
                        }
                        else
                        {
                            List<DataRow> rowsdel = new List<DataRow>();
                            int curindex = 0;
                            while(curindex<syncrows.Count)
                            {
                                for(int i=0;i<900;i++)
                                {
                                    if(curindex<syncrows.Count)
                                    {
                                        rowsdel.Add(syncrows[curindex]);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    curindex++;
                                }
                                if(rowsdel.Count>0)
                                {
                                    RemoveRows(table, serviceFrom, serviceTo, rowsdel);
                                }
                                rowsdel.Clear();
                            }
                        }
                        */
                    }
                }
                SetLastUpdate(table);
            }
            else
            {
                SetLastUpdate(table);
            }
            if (dataTable != null) dataTable.Dispose();
        }

        private static void RemoveRows(TableBase table, IBaseService serviceFrom, IBaseService serviceTo, List<DataRow> syncrows)
        {
            Log.Instance.SaveInfo($"Начинаем удалять строки из таблицы {table.Name} их количество {syncrows.Count}");
            /*
            SortedList<string, object> whprs = new SortedList<string, object>();
            int indexpar = 0;

            StringBuilder sins = new StringBuilder();
            foreach (var item in syncrows)
            {
                object key = item[table.ColumnPrimary.Name];
                if (key != null && key != System.DBNull.Value)
                {
                    string nameparam = $"@wh_pr{indexpar}";
                    whprs.Add(nameparam, key);
                    if (indexpar == 0) sins.Append(nameparam);
                    else { sins.Append(","); sins.Append(nameparam); }
                    indexpar++;
                }
            }
            if (indexpar > 0)
            {
                string where = $" {table.ColumnPrimary.Name} in ({sins.ToString()})";
                if (!string.IsNullOrEmpty(table.UslovieRemove) && !string.IsNullOrWhiteSpace(table.UslovieRemove))
                {
                    where = where + " and " + table.UslovieRemove.Trim();
                    Log.Instance.SaveInfo($"Дополнительное условие удаления {table.UslovieRemove.Trim()}");
                }
                
                int countremove=serviceFrom.DeleteData(table.Name, where, whprs);
                Log.Instance.SaveInfo($"Из таблицы {table.Name} удалено {countremove} строк");
               
            }
            */

            string selquery = $"select top 1 {table.ColumnPrimary.Name} from {table.Name} where {table.ColumnPrimary.Name}=@wh_id ";
            string wheredel = $" {table.ColumnPrimary.Name}=@wh_id";
            if (!string.IsNullOrEmpty(table.UslovieRemove) && !string.IsNullOrWhiteSpace(table.UslovieRemove))
            {
                selquery = selquery + " and " + table.UslovieRemove.Trim();
                wheredel = wheredel + " and " + table.UslovieRemove.Trim();
                Log.Instance.SaveInfo($"Дополнительное условие удаления {table.UslovieRemove.Trim()}");
            }

            foreach (DataRow row in syncrows)
            {
                try
                {
                    object key = row[table.ColumnPrimary.Name];
                    if (key != null && key != System.DBNull.Value)
                    {
                        object findObj = serviceTo.ExecuteScalar(selquery, new SortedList<string, object>() { { "@wh_id", key } });
                        if (findObj != null && findObj != System.DBNull.Value)
                        {
                            int countdel=serviceFrom.DeleteData(table.Name, wheredel, new SortedList<string, object>() { { "@wh_id", key } });
                            if (countdel > 0)
                            {
                                Log.Instance.SaveInfo(TypeModule.Sync, $"Удалена строка из таблицы {table.Name} где {table.ColumnPrimary.Name}={key.ToString()}");
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(table.UslovieRemove) && !string.IsNullOrWhiteSpace(table.UslovieRemove))
                                {
                                    Log.Instance.SaveInfo(TypeModule.Sync, $"Не найдена строка для удаления из таблицы {table.Name} где {table.ColumnPrimary.Name}={key.ToString()}" +
                                        $" и {table.UslovieRemove.Trim()}");
                                }
                                else
                                {
                                    Log.Instance.SaveInfo(TypeModule.Sync, $"Не найдена строка для удаления из таблицы {table.Name} где {table.ColumnPrimary.Name}={key.ToString()}");
                                }
                            }
                        }
                        else
                        {
                            Log.Instance.SaveError(TypeModule.Sync, $"SQL выдал что успешно прошла синхронизация по таблице {table.Name} где {table.ColumnPrimary.Name}={key.ToString()} " +
                                $" а в итоговй таблице такой строка не найдена");
                        }
                    }
                }
                catch(Exception ex)
                {
                    Log.Instance.SaveError(TypeModule.Sync, $"Ошибка {ex.Message} {ex.StackTrace}");
                }
            }
        }

        public static void RunSyncBase()
        {
            Log.Instance.SaveInfo("Начинаем синхронизацию таблиц c сервера");
            var fromServer = from t in ContextBase.CurrentBaseDevice.Tables where t.TypeSync == TypeSync.FromServer select t;
            if (fromServer != null && fromServer.Any() && fromServer.Count() > 0)
            {
                Log.Instance.SaveInfo($"Найдено {fromServer.Count()} таблиц для синхронизации с сервера");
                foreach (var item in fromServer)
                {
                    FactoryBaseService.Service.ExecuteDevice((IBaseService device) =>
                    {
                        FactoryBaseService.Service.ExecuteServer((IBaseService server) =>
                        {
                            SyncTable(item, server, device);
                        });
                    });
                }
            }
            Log.Instance.SaveInfo("Начинаем синхронизацию таблиц c устройства");
            var fromDevice = from t in ContextBase.CurrentBaseDevice.Tables where t.TypeSync == TypeSync.FromDevice select t;
            if (fromDevice != null && fromDevice.Any() && fromDevice.Count() > 0)
            {
                Log.Instance.SaveInfo($"Найдено {fromDevice.Count()} таблиц для синхронизации с устройства");
                foreach (var item in fromDevice)
                {
                    FactoryBaseService.Service.ExecuteDevice((IBaseService device) =>
                    {
                        FactoryBaseService.Service.ExecuteServer((IBaseService server) =>
                        {
                            SyncTable(item, device, server);
                        });
                    });
                }
            }
        }

        private static void SetLastUpdate(TableBase table)
        {
            
            Log.Instance.SaveInfo($"Устанавливаем дату последней синхронизации Таблица: {table.Name} Новая дата синхронизации: {table.LastChange.ToString("dd.MM.yyyy HH:mm:ss")}");
            FactoryBaseService.Service.ExecuteDevice((IBaseService serv) =>
            {
                try
                {
                    serv.UpdateData("SyncTables", new SortedList<string, object>() { { "LastUpdate", table.LastChange } }, $"TableName='{table.Name}'", null);
                    table.LastUpdate = table.LastChange;

                    Log.Instance.SaveInfo($"Успешно установлена дата последней синхронизации Таблица: {table.Name} Новая дата синхронизации: {table.LastChange.ToString("dd.MM.yyyy HH:mm:ss")}");
                }
                catch(Exception ex)
                {
                    Log.Instance.SaveInfo($"Ошибка при установлении даты последней синхронизации Таблица: {table.Name} Новая дата синхронизации: {table.LastChange.ToString("dd.MM.yyyy HH:mm:ss")} {ex.Message} {ex.StackTrace}");

                }

            });
        }
    }

    public class DataLinkItem
    {
        public string NameTable { get; set; }
        public string Field { get; set; }
        public object Data { get; set; }
    }
}
