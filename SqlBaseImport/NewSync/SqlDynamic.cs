﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase
{
    public class SqlDymanic
    {
        private List<string> fields = new List<string>();
        private SortedList<string, string> joins = new SortedList<string, string>();
        private List<string> orderjoin = new List<string>();
        private List<string> uslovies = new List<string>();
        private SortedList<string, object> parametrs = new SortedList<string, object>();
        private List<string> orders = new List<string>();
        private List<string> groupbys = new List<string>();

        public string NameTable { get; set; }
        private string prefix = "m";
        public string Preix
        {
            get { return prefix; }
        }

        private int countTop = 100;
        public int CountTop
        {
            get
            {
                return countTop;
            }
            set
            {
                countTop = value;
            }
        }

        public SqlDymanic SetMainPrefix(string prefix)
        {
            this.prefix = prefix;
            return this;
        }
        public SqlDymanic AddField(string field) { fields.Add(field); return this; }
        public SqlDymanic AddFieldMainPrefix(string field)
        {
            if (!string.IsNullOrEmpty(Preix) && !string.IsNullOrWhiteSpace(Preix))
            {
                fields.Add(Preix + "." + field);
            }
            else
            {
                fields.Add(field);
            }
            return this;
        }
        public SqlDymanic AddFields(params string[] afields)
        {
            if (afields != null && afields.Length > 0)
            {
                foreach (var item in afields)
                {
                    fields.Add(item);
                }
            }

            return this;
        }
        public SqlDymanic AddJoin(string key, string joinusloiv)
        {
            if (!joins.ContainsKey(key)) { joins.Add(key, joinusloiv); orderjoin.Add(joinusloiv); }
            return this;
        }

        int indexjoin = 0;
        public string GetNameNextJoin()
        {
            indexjoin++;
            return $"jn{indexjoin}";
        }

        public SqlDymanic AddParametr(string key, object data)
        {
            if (!parametrs.ContainsKey(key)) { parametrs.Add(key, data); }
            return this;
        }

        public SqlDymanic AddUslovie(string uslovie)
        {
            uslovies.Add(uslovie);
            return this;
        }
        public SqlDymanic AddOrders(string order)
        {
            orders.Add(order);
            return this;
        }

        public SqlDymanic AddGroupBy(string field)
        {
            groupbys.Add(field);
            return this;
        }

        int index = 0;
        public string GetNextNameParam()
        {
            index++;
            return $"@param{index}";
        }

        public string Generate()
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(NameTable) && !string.IsNullOrWhiteSpace(NameTable))
            {
                StringBuilder build = new StringBuilder();
                build.Append(" select ");
                if (CountTop > 0)
                {
                    build.Append(" top "); build.Append(CountTop); build.Append(" ");
                }
                bool isfirst = true;
                foreach (var item in fields)
                {
                    if (isfirst) { build.Append(item); isfirst = false; }
                    else { build.Append(","); build.Append(item); }
                }
                build.Append(" from "); build.Append(NameTable);
                if (!string.IsNullOrEmpty(Preix) && !string.IsNullOrWhiteSpace(Preix))
                {
                    build.Append(" "); build.Append(Preix); build.Append(" ");
                }
                isfirst = true;
                foreach (var join in orderjoin)
                {
                    if (isfirst) { build.Append(join); isfirst = false; build.Append(" "); }
                    else { build.Append(join); isfirst = false; build.Append(" "); }
                }

                if (uslovies.Count > 0)
                {
                    build.Append(" where ");
                    isfirst = true;
                    foreach (var uslov in uslovies)
                    {
                        if (isfirst) { build.Append(uslov); isfirst = false; }
                        else { build.Append(" and "); build.Append(uslov); isfirst = false; }
                    }
                }

                if (groupbys.Count > 0)
                {
                    build.Append(" group by ");
                    isfirst = true;
                    foreach (var item in groupbys)
                    {
                        if (isfirst) { build.Append(item); isfirst = false; }
                        else { build.Append(" , "); build.Append(item); isfirst = false; }
                    }
                }
                if (orders.Count > 0)
                {
                    build.Append(" order by ");
                    isfirst = true;
                    foreach (var item in orders)
                    {
                        if (isfirst) { build.Append(item); isfirst = false; }
                        else { build.Append(","); build.Append(item); }
                    }
                }
                result = build.ToString();
            }
            return result;
        }

        public DataTable FillTable(IBaseService serv)
        {
            DataTable table = null;
            string zap = Generate();
            if (!string.IsNullOrEmpty(zap) && !string.IsNullOrWhiteSpace(zap))
            {
                table=serv.LoadData(zap, parametrs);
            }
            return table;
        }

        public void LoadList<T>(IBaseService serv,ObservableCollection<T> collect)
        {
            string zap = Generate();
            if (!string.IsNullOrEmpty(zap) && !string.IsNullOrWhiteSpace(zap))
            {
                serv.LoadListObject<T>(zap,parametrs,collect);
            }
        }


    }
}
