﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase
{
    public class MultiKey : IComparable, IComparable<MultiKey>
    {
        public int Order { get; set; }
        private List<IComparable> keys = new List<IComparable>();

        public int CompareTo(MultiKey other)
        {
            int result = -1;
            if (this.keys.Count == other.keys.Count)
            {
                for (int i = 0; i < this.keys.Count; i++)
                {
                    object obj = Convert.ChangeType(other.keys[i], this.keys[i].GetType());
                    result = this.keys[i].CompareTo(obj);
                    if (result != 0)
                    {
                        break;
                    }
                }
            }
            return result;
        }

        public int CompareTo(object obj)
        {
            int result = -1;
            MultiKey testobj = obj as MultiKey;
            if (testobj != null)
            {
                result = CompareTo(testobj);
            }
            return result;
        }

        public void AddKey(IComparable compare)
        {
            keys.Add(compare);
        }

        public MultiKey(params IComparable[] compares)
        {
            if (compares != null && compares.Length > 0)
            {
                foreach (var item in compares)
                {
                    AddKey(item);
                }
            }
        }
    }
}
