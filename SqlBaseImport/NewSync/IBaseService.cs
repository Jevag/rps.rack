﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace CodeXBase
{
    public interface IBaseService
    {
        int DeleteData(string NameTable, string where, SortedList<string, object> paramwhere);
        void ExecuteNonQuery(string query, SortedList<string, object> parametrs);
        Tuple<bool, T> ExecuteScalar<T>(string query, SortedList<string, object> parametrs);
        Tuple<bool, object> ExecuteScalar(string query, SortedList<string, object> parametrs);
        object InsertData(string NameTable, SortedList<string, object> data, bool setIdentity = false);
        DataTable LoadData(string query, SortedList<string, object> parametrs);
        void LoadData(string query, SortedList<string, object> parametrs, Action<DataRow> actionrow);
        ObservableCollection<T> LoadListObject<T>(string query, SortedList<string, object> parametrs);
        void LoadListObject<T>(string query, SortedList<string, object> parametrs, ObservableCollection<T> collect);
        T LoadObject<T>(string query, SortedList<string, object> parametrs);
        void SetDataCollect(string NameTable, string KeyField, bool isSetKey, IEnumerable setdata, string[] setfieldsInsert, string[] setfielsdUpdate, string[] equalfields, string where, SortedList<string, object> paramwhere, Action<object, object> setkeyaction = null, GetDataHandler actgetdata = null);
        void UpdateData(string NameTable, SortedList<string, object> data, string where, SortedList<string, object> paramwhere);
        Tuple<TypeUpsert,object> UpsertData(string NameTable, string KeyField, SortedList<string, object> data, string where, SortedList<string, object> paramwhere);
    }

    public enum TypeUpsert
    {
        Insert=0,
        Update=1
    }
}