﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CodeXBase
{
    public class InfoDynamic : DynamicObject, INotifyPropertyChanged
    {
        Dictionary<string, object> dict = new Dictionary<string, object>();


        private SortedList<string, List<IBehvoirInfoDynamic>> behavoirs = new SortedList<string, List<IBehvoirInfoDynamic>>();


        public void AddBehavoir(string nameprop, IBehvoirInfoDynamic behavoir)
        {
            if (behavoirs.ContainsKey(nameprop)) { behavoirs[nameprop].Add(behavoir); }
            else { behavoirs.Add(nameprop, new List<IBehvoirInfoDynamic>() { behavoir }); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyCahnged(string nameproperty)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(nameproperty));
            }
        }
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return dict.TryGetValue(binder.Name, out result);
        }
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            dict[binder.Name] = value;
            RaisePropertyCahnged(binder.Name);
            return true;
        }

        public void SetData(string nameprop, object value)
        {
            dict[nameprop] = value;
            RaisePropertyCahnged(nameprop);
        }

        public object GetData(string nameprop)
        {
            object result;
            dict.TryGetValue(nameprop, out result);
            return result;
        }

        public string GetString(string nameprop)
        {
            string result = string.Empty;
            object res;
            if (dict.TryGetValue(nameprop, out res))
            {
                if (res != null)
                {
                    result = res.ToString();
                }
            }
            return result;
        }

        public int? GetInt(string nameprop)
        {
            int? result = null;
            object res;
            if (dict.TryGetValue(nameprop, out res))
            {
                if (res != null)
                {
                    try
                    {
                        result = System.Convert.ToInt32(res);
                    }
                    catch { }
                }
            }
            return result;
        }

        public long? GetLong(string nameprop)
        {
            long? result = null;
            object res;
            if (dict.TryGetValue(nameprop, out res))
            {
                if (res != null)
                {
                    try
                    {
                        result = System.Convert.ToInt64(res);
                    }
                    catch { }
                }
            }
            return result;
        }

        public double? GetDouble(string nameprop)
        {
            double? result = null;
            object res;
            if (dict.TryGetValue(nameprop, out res))
            {
                if (res != null)
                {
                    try
                    {
                        result = System.Convert.ToDouble(res);
                    }
                    catch { }
                }
            }
            return result;
        }

        public bool? GetBool(string nameprop)
        {
            bool? result = null;
            object res;
            if (dict.TryGetValue(nameprop, out res))
            {
                if (res != null)
                {
                    try
                    {
                        result = System.Convert.ToBoolean(res);
                    }
                    catch { }
                }
            }
            return result;
        }

        public DateTime? GetDateTime(string nameprop)
        {
            DateTime? result = null;
            object res;
            if (dict.TryGetValue(nameprop, out res))
            {
                if (res != null)
                {
                    try
                    {
                        result = System.Convert.ToDateTime(res);
                    }
                    catch { }
                }
            }
            return result;
        }

        public IComparable GetComparable(string nameprop)
        {
            IComparable result = null;
            object res;
            if (dict.TryGetValue(nameprop, out res))
            {
                result = res as IComparable;
            }
            return result;
        }

        public Dictionary<string, object> GetProperties()
        {
            return dict;
        }

        public void SetDataIfHasProperty(string nameprop, object value)
        {
            if (dict.ContainsKey(nameprop))
            {
                SetData(nameprop, value);
            }
        }

        public bool HasProperty(string nameprop)
        {
            return dict.ContainsKey(nameprop);
        }

        public void RemoveProperty(string nameprop)
        {
            if (dict.ContainsKey(nameprop))
            {
                dict.Remove(nameprop);
            }
        }

    }

    public interface IBehvoirInfoDynamic
    {
        void Execute(InfoDynamic info);
    }
}
