﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase
{
    public class SqlBaseService : IBaseService
    {
        SqlConnection connect;
        SqlTransaction transact;

        public string GetNameField(string namefield)
        {
            string result = namefield;
            if (namefield[0] != '[')
            {
                result = "[" + namefield;
            }
            if (result[result.Length - 1] != ']')
            {
                result = result + "]";
            }
            return result;
        }

        public SqlBaseService(SqlConnection connect)
        {
            this.connect = connect;
        }
        public SqlBaseService(SqlConnection connect,SqlTransaction transact)
        {
            this.connect = connect;
            this.transact = transact;
        }

        private  SqlCommand CreateCommand(string query, SortedList<string, object> parametrs)
        {
            SqlCommand comm = new SqlCommand();
            comm.CommandText = query;
            if(parametrs!=null && parametrs.Count>0)
            {
                foreach(var item in parametrs)
                {
                    SqlParameter pr = new SqlParameter(item.Key, (dynamic)item.Value);
                    comm.Parameters.Add(pr);
                }
            }
            return comm;
        }
        /// <summary>
        /// Выполняет запрос на выборку данных и заполняет таблицу
        /// </summary>
        /// <param name="query">Запрос на выборку</param>
        /// <param name="dats">Параметры запроса</param>
        /// <returns>Результирующая таблица</returns>
        public  DataTable LoadData(string query,SortedList<string,object> parametrs)
        {
            DataTable table = new DataTable();
            using (SqlDataAdapter adap = new SqlDataAdapter())
            {
                using (SqlCommand comsel = CreateCommand(query,parametrs))
                {
                   
                    comsel.Connection = connect;
                    comsel.Transaction = transact;
                    adap.SelectCommand = comsel;
                    adap.Fill(table);
                }
            }
            return table;
        }

        public void LoadData(string query, SortedList<string, object> parametrs,Action<DataRow> actionrow)
        {
            using (SqlDataAdapter adap = new SqlDataAdapter())
            {
                using (SqlCommand comsel = CreateCommand(query, parametrs))
                {
                    using (DataTable table = new DataTable())
                    {
                        comsel.Connection = connect;
                        comsel.Transaction = transact;
                        adap.SelectCommand = comsel;
                        adap.Fill(table);
                        if(actionrow!=null && table!=null && table.Rows!=null && table.Rows.Count>0)
                        {
                            foreach(DataRow row in table.Rows)
                            {
                                actionrow(row);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Выполняет запрос на выборку данных берет одну строку
        /// создает объект заданного типа заполняет его и возвращает его
        /// </summary>
        /// <param name="query">Запрос на выборку</param>
        /// <param name="dats">Параметры запроса</param>
        /// <returns>Конечный объект</returns>
        public T LoadObject<T>(string query, SortedList<string, object> parametrs)
        {
            T result = default(T);
            using (SqlDataAdapter adap = new SqlDataAdapter())
            {
                using (SqlCommand comsel = CreateCommand(query, parametrs))
                {
                    using (DataTable table = new DataTable())
                    {
                        comsel.Connection = connect;
                        comsel.Transaction = transact;
                        adap.SelectCommand = comsel;
                        adap.Fill(table);
                        if (table.Rows != null && table.Rows.Count > 0)
                        {
                            result = Activator.CreateInstance<T>();
                            UtilsReflectionSql.FillDataFromRow(result, table.Rows[0]);
                        }
                        if (table != null) table.Dispose();
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Выполняет запрос на выборку данных
        /// проходит по всем строкам для каждой из них создает 
        /// объект заданного типа и заполняет его
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="query">Запрос</param>
        /// <param name="connect">Коннект к БД</param>
        /// <param name="transact">Транзакция к БД</param>
        /// <param name="dats">параметры запроса</param>
        /// <returns>Коллекция объектов</returns>
        public  ObservableCollection<T> LoadListObject<T>(string query, SortedList<string, object> parametrs)
        {
            ObservableCollection<T> collect = new ObservableCollection<T>();
            using (SqlDataAdapter adap = new SqlDataAdapter())
            {
                using (SqlCommand comsel = CreateCommand(query, parametrs))
                {
                    using (DataTable table = new DataTable())
                    {
                        comsel.Connection = connect;
                        comsel.Transaction = transact;
                        adap.SelectCommand = comsel;
                        adap.Fill(table);
                        if (table.Rows != null && table.Rows.Count > 0)
                        {
                            foreach (DataRow row in table.Rows)
                            {
                                T info = Activator.CreateInstance<T>();
                                UtilsReflectionSql.FillDataFromRow(info, row);
                                collect.Add(info);
                            }
                        }
                        if (table != null) table.Dispose();
                    }
                }
            }
            return collect;
        }

        /// <summary>
        /// Выполняет запрос на выборку данных
        /// проходит по всем строкам для каждой из них создает 
        /// объект заданного типа и заполняет его
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="query">Запрос</param>
        /// <param name="connect">Коннект к БД</param>
        /// <param name="transact">Транзакция к БД</param>
        /// <param name="dats">параметры запроса</param>
        /// <returns>Коллекция объектов</returns>
        public void LoadListObject<T>(string query, SortedList<string, object> parametrs,ObservableCollection<T> collect)
        {
            using (SqlDataAdapter adap = new SqlDataAdapter())
            {
                using (SqlCommand comsel = CreateCommand(query, parametrs))
                {
                    using (DataTable table = new DataTable())
                    {
                        comsel.Connection = connect;
                        comsel.Transaction = transact;
                        adap.SelectCommand = comsel;
                        adap.Fill(table);
                        if (table.Rows != null && table.Rows.Count > 0)
                        {
                            foreach (DataRow row in table.Rows)
                            {
                                T info = Activator.CreateInstance<T>();
                                UtilsReflectionSql.FillDataFromRow(info, row);
                                collect.Add(info);
                            }
                        }
                        if (table != null) table.Dispose();
                    }
                }
            }
        }

        public object InsertData(string NameTable,SortedList<string,object> data,bool setIdentity=false)
        {
            object result = null;
            using (SqlCommand command = new SqlCommand())
            {
                StringBuilder strins = new StringBuilder();
                StringBuilder strvalues = new StringBuilder();
                int indexparam = 0;
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        if (item.Value != null && item.Value != System.DBNull.Value)
                        {
                            if (indexparam == 0)
                            {
                                strins.Append(GetNameField(item.Key));
                                string namepr = "@pr" + indexparam.ToString();
                                strvalues.Append(namepr);
                                SqlParameter pr = new SqlParameter(namepr, (dynamic)item.Value);
                                command.Parameters.Add(pr);
                                indexparam++;
                            }
                            else
                            {
                                strins.Append(",");
                                strins.Append(GetNameField(item.Key));
                                string namepr = "@pr" + indexparam.ToString();
                                strvalues.Append(",");
                                strvalues.Append(namepr);
                                SqlParameter pr = new SqlParameter(namepr, (dynamic)item.Value);
                                command.Parameters.Add(pr);
                                indexparam++;
                            }
                        }
                        else
                        {
                            if (indexparam == 0)
                            {
                                strins.Append(GetNameField(item.Key));
                                strvalues.Append("null");
                                indexparam++;
                            }
                            else
                            {
                                strins.Append(",");
                                strins.Append(GetNameField(item.Key));
                                strvalues.Append(",");
                                strvalues.Append("null");
                                indexparam++;
                            }
                        }
                    }

                    string strcommand = "insert into " + GetNameField(NameTable) + "(" + strins.ToString() + ") values (" + strvalues.ToString() + ") select SCOPE_IDENTITY()";
                    command.CommandText = strcommand;
                    if (setIdentity)
                    {
                        string fullcommand = $" set identity_insert {GetNameField(NameTable)} on {strcommand} set identity_insert {GetNameField(NameTable)} off";
                        command.CommandText = fullcommand;
                    }
                    command.Connection = connect;
                    command.Transaction = transact;
                    object res = command.ExecuteScalar();
                    if (res != null && res != System.DBNull.Value)
                    {
                        result = res;
                    }
                }
            }
            return result;
        }

        public void UpdateData(string NameTable,SortedList<string,object> data,string where,SortedList<string,object> paramwhere)
        {
            using (SqlCommand command = new SqlCommand())
            {
                StringBuilder strbuild = new StringBuilder();

                int indexparam = 0;
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        if (item.Value != null && item.Value != System.DBNull.Value)
                        {
                            if (indexparam == 0)
                            {
                                strbuild.Append(GetNameField(item.Key));
                                strbuild.Append("=");
                                string namepr = "@prset" + indexparam.ToString();
                                strbuild.Append(namepr);
                                SqlParameter pr = new SqlParameter(namepr, (dynamic)item.Value);
                                command.Parameters.Add(pr);
                                indexparam++;
                            }
                            else
                            {
                                strbuild.Append(",");
                                strbuild.Append(GetNameField(item.Key));
                                strbuild.Append("=");
                                string namepr = "@prset" + indexparam.ToString();
                                strbuild.Append(namepr);
                                SqlParameter pr = new SqlParameter(namepr, (dynamic)item.Value);
                                command.Parameters.Add(pr);
                                indexparam++;
                            }
                        }
                        else
                        {
                            if (indexparam == 0)
                            {
                                strbuild.Append(GetNameField(item.Key));
                                strbuild.Append("=null");
                                indexparam++;
                            }
                            else
                            {
                                strbuild.Append(",");
                                strbuild.Append(GetNameField(item.Key));
                                strbuild.Append("=null");
                                indexparam++;
                            }
                        }
                    }


                    if (!string.IsNullOrEmpty(where) && !string.IsNullOrWhiteSpace(where))
                    {
                        strbuild.Append(" where ");
                        strbuild.Append(where);
                        if (paramwhere != null && paramwhere.Count > 0)
                        {
                            foreach (var item in paramwhere)
                            {
                                SqlParameter pr = new SqlParameter(item.Key, (dynamic)item.Value);
                                command.Parameters.Add(pr);
                            }
                        }
                    }

                    command.CommandText = "update " + GetNameField(NameTable) + " set " + strbuild.ToString();

                    command.Connection = connect;
                    command.Transaction = transact;
                    command.ExecuteNonQuery();
                }
            }
        }

        public Tuple<TypeUpsert,object> UpsertData(string NameTable,string KeyField, SortedList<string, object> data, string where, SortedList<string, object> paramwhere)
        {
            TypeUpsert type = TypeUpsert.Insert;
            object result = null;

            string sel = "select top 1 " + GetNameField(KeyField) + " from " + GetNameField(NameTable);
            if (!string.IsNullOrEmpty(where) && !string.IsNullOrWhiteSpace(where))
            {
                sel = sel + " where " + where;
            }

            object res = null;
            using (var comm = CreateCommand(sel, paramwhere))
            {
                comm.Connection = connect;
                comm.Transaction = transact;
                res = comm.ExecuteScalar();
            }

            if (res != null && res != System.DBNull.Value)
            {
                type = TypeUpsert.Insert;
                result = res;
                UpdateData(NameTable, data, KeyField + "=@prwh1", new SortedList<string, object>() { { "@prwh1", res } });
            }
            else
            {
                type = TypeUpsert.Update;
                result = InsertData(NameTable,data);
            }

            return new Tuple<TypeUpsert, object>(type, result);
        }

        public int DeleteData(string NameTable, string where, SortedList<string, object> paramwhere)
        {
            int countremove = 0;
            string commstr = "delete from " + GetNameField(NameTable);
            if (!string.IsNullOrEmpty(where) && !string.IsNullOrWhiteSpace(where))
            {
                commstr = commstr + " where " + where;
            }
            using (var scommand = CreateCommand(commstr, paramwhere))
            {
                scommand.Connection = connect;
                scommand.Transaction = transact;
                countremove=scommand.ExecuteNonQuery();
            }
            return countremove;
        }

        public Tuple<bool,T> ExecuteScalar<T>(string query,SortedList<string,object> parametrs)
        {
            bool has = false;
            T result = default(T);
            if (!string.IsNullOrEmpty(query) && !string.IsNullOrWhiteSpace(query))
            {
                using (var scommand = CreateCommand(query, parametrs))
                {
                    scommand.Connection = connect;
                    scommand.Transaction = transact;
                    object res=scommand.ExecuteScalar();
                    if(res!=null && res!=System.DBNull.Value)
                    {
                        try
                        {
                            result = (T)res;
                            has = true;
                        }
                        catch { }
                    }
                }
            }
            return new Tuple<bool, T>(has, result);
        }

        public Tuple<bool, object> ExecuteScalar(string query, SortedList<string, object> parametrs)
        {
            bool has = false;
            object result =null;
            if (!string.IsNullOrEmpty(query) && !string.IsNullOrWhiteSpace(query))
            {
                using (var scommand = CreateCommand(query, parametrs))
                {
                    scommand.Connection = connect;
                    scommand.Transaction = transact;
                    object res = scommand.ExecuteScalar();
                    if (res != null && res != System.DBNull.Value)
                    {
                        try
                        {
                            result = res;
                            has = true;
                        }
                        catch { }
                    }
                }
            }
            return new Tuple<bool, object>(has, result);
        }

        public void ExecuteNonQuery(string query, SortedList<string, object> parametrs)
        {
            if (!string.IsNullOrEmpty(query) && !string.IsNullOrWhiteSpace(query))
            {
                using (var scommand = CreateCommand(query, parametrs))
                {
                    scommand.Connection = connect;
                    scommand.Transaction = transact;
                    scommand.ExecuteNonQuery();
                }
            }
        }

        public void SetDataCollect(string NameTable, string KeyField, bool isSetKey, IEnumerable setdata
                      , string[] setfieldsInsert, string[] setfielsdUpdate
                      , string[] equalfields, string where,SortedList<string,object> paramwhere, Action<object, object> setkeyaction = null
                      , GetDataHandler actgetdata = null)
        {
            string error = string.Empty;
            //Список из БД по заданному условию
            #region Берем текущие данные из БД
            SortedList<MultiKey, DataRow> basesort = new SortedList<MultiKey, DataRow>();
            StringBuilder strbuild = new StringBuilder();

            #region Формируем запрос к БД на получение данных
            strbuild.Append("select " + GetNameField(KeyField));
            bool isfirst = false;


            if (equalfields != null && equalfields.Length > 0)
            {
                foreach (string eqfield in equalfields)
                {
                    if (isfirst)
                    {
                        strbuild.Append(GetNameField(eqfield)); isfirst = false;
                    }
                    else
                    {
                        strbuild.Append(","); strbuild.Append(GetNameField(eqfield));
                    }
                }
            }

            strbuild.Append(" from "); strbuild.Append(GetNameField(NameTable));

            List<SqlParameter> lparams = new List<SqlParameter>();
            isfirst = true;

            if(!string.IsNullOrEmpty(where) && !string.IsNullOrWhiteSpace(where))
            {
                strbuild.Append(" where " + where);
            }

            if (paramwhere != null && paramwhere.Count > 0)
            {
                foreach (var item in paramwhere)
                {
                    if (item.Value != null)
                    {
                        lparams.Add(new SqlParameter(item.Key, item.Value));
                    }
                }
            }
            #endregion Формируем запрос к БД на получение данных

            #region Берем данные из БД и сортируем полученные данные
            DataTable table = new DataTable();
            using (SqlDataAdapter adap = new SqlDataAdapter())
            {
                using (SqlCommand comm = new SqlCommand(strbuild.ToString(), connect, transact))
                {
                    foreach (SqlParameter sparam in lparams)
                    {
                        comm.Parameters.Add(sparam);
                    }
                    comm.Connection = connect;
                    comm.Transaction = transact;
                    adap.SelectCommand = comm;
                    adap.Fill(table);
                }
            }


            foreach (DataRow row in table.Rows)
            {
                bool isadd = true;
                MultiKey key = new MultiKey();
                try
                {
                    foreach (var fkey in equalfields)
                    {
                        IComparable comp = row[fkey] as IComparable;
                        if (comp != null)
                        {
                            key.AddKey(comp);
                        }
                        else
                        {
                            isadd = false;
                            break;
                        }
                    }
                }
                catch
                {
                    isadd = false;
                    break;
                }
                if (isadd)
                {
                    if (!basesort.ContainsKey(key))
                    {
                        basesort.Add(key, row);
                    }
                }
            }
            #endregion Берем данные из БД и сортируем полученные данные


            #endregion Берем текущие данные из БД
            //Текущие данные которые нужно установить сортируем
            int order = 0;
            #region Сортируем данные которые нужно установить
            SortedList<MultiKey, object> setdatasort = new SortedList<MultiKey, object>();
            if (equalfields != null && equalfields.Length > 0)
            {
                foreach (object data in setdata)
                {
                    bool isadd = true;
                    MultiKey key = new MultiKey();
                    try
                    {
                        foreach (string fkey in equalfields)
                        {
                            IComparable comp = null;
                            if (actgetdata != null)
                            {
                                comp = actgetdata(data, fkey) as IComparable;
                            }
                            else
                            {
                                comp = data.GetValue(fkey) as IComparable;
                            }
                            if (comp != null)
                            {
                                key.AddKey(comp);
                            }
                            else
                            {
                                isadd = false;
                                break;
                            }
                        }
                    }
                    catch
                    {
                        isadd = false;
                        break;
                    }
                    if (isadd)
                    {
                        if (!setdatasort.ContainsKey(key))
                        {
                            key.Order = order; order++;
                            setdatasort.Add(key, data);
                        }
                    }
                }
            }
            #endregion Сортируем данные которые нужно установить


            #region Обновляем БД
            //Проходим по списку который нужно установить 
            //если находим соответсвеи в тех которые нужно устновить то делаем Update при Update уставнавливаем все поля кроме ключа
            //иначе делаем Insert
            var orders = from t in setdatasort orderby t.Key.Order select t;
            foreach (var item in orders)
            {
                SortedList<string, object> lst = new SortedList<string, object>();

                if (basesort.ContainsKey(item.Key))
                {
                    foreach (var fl in setfielsdUpdate)
                    {
                        if (!fl.Equals(KeyField))
                        {
                            object row = item.Value;
                            object dt = null;
                            if (actgetdata != null)
                            {
                                dt = actgetdata(row, fl);
                            }
                            else
                            {
                                dt = row.GetValue(fl);
                            }
                            if (dt != null)
                            {
                                lst.Add(fl, dt);
                            }
                        }
                    }
                    //Update
                    object key = basesort[item.Key][KeyField];
                    string wh = GetNameField(KeyField) + "=@prkey1";
                    UpdateData(NameTable, lst, wh, new SortedList<string, object>() { { "@prkey1",key } });
                }
                else
                {
                    foreach (var fl in setfieldsInsert)
                    {
                        if (!fl.Equals(KeyField))
                        {
                            object row = item.Value;
                            object dt = null;
                            if (actgetdata != null)
                            {
                                dt = actgetdata(row, fl);
                            }
                            else
                            {
                                dt = row.GetValue(fl);
                            }
                            if (dt != null)
                            {
                                lst.Add(fl, dt);
                            }
                        }
                        else
                        {
                            if (isSetKey)
                            {
                                object row = item.Value;
                                object dt = null;
                                if (actgetdata != null)
                                {
                                    dt = actgetdata(row, fl);
                                }
                                else
                                {
                                    dt = row.GetValue(fl);
                                }
                                if (dt != null)
                                {
                                    lst.Add(fl, dt);
                                }
                            }
                        }
                    }
                    //Insert
                    var res = InsertData(NameTable, lst);
                    if (setkeyaction != null)
                    {
                        setkeyaction(item.Value, res);
                    }
                    else
                    {
                        if (res != null && res != System.DBNull.Value && item.Value.HasPropertyObject(KeyField))
                        {
                            try
                            {
                                var setkey = System.Convert.ChangeType(res, item.Value.GetTypeProperty(KeyField));
                                item.Value.SetValue(KeyField, setkey);
                            }
                            catch (Exception ex)
                            { }
                        }
                    }
                }
            }
            //Проходим по списку из БД и пытаемся найти ему соответсвеи в устанавливаемых
            //если не находим то удаляем
            foreach (var itwm in basesort)
            {
                if (!setdatasort.ContainsKey(itwm.Key))
                {
                    object key = itwm.Value[KeyField];
                    string wh = KeyField + "=@prkey1";
                    DeleteData(NameTable, wh, new SortedList<string, object>() { { "@prkey1", key } } );
                }
            }
            #endregion Обновляем БД
        }
    }


}
