﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase
{
    public class FactoryBaseService
    {
        public static string ConfigConnectNameDevice { get; set; }
        private static string connectStringDevice = string.Empty;
        public static string ConnectStringDevice
        {
            get { return connectStringDevice; }
            set
            {
                connectStringDevice = value;
            }
        }

        public static string ConfigConnectNameServer { get; set; }
        private static string connectStringServer = string.Empty;
        public static string ConnectStringServer
        {
            get { return connectStringServer; }
            set
            {
                connectStringServer = value;
            }
        }

        public static Action<Exception> ActionException { get; set; }

        private TypeBase typeBase = TypeBase.Sql;
        public TypeBase TypeBase
        {
            get { return typeBase; }
            set { typeBase = value; }
        }

        private static FactoryBaseService service = null;
        public static FactoryBaseService Service
        {
            get
            {
                if(service==null)
                {
                    service = new FactoryBaseService();
                }
                return service;
            }
        }

        public void ExecuteDevice(Action<IBaseService> action)
        {
            switch(TypeBase)
            {
                case TypeBase.Sql:ExecuteSqlDevice(action); break;
            }
        }

        public void ExecuteTransactDevice(Action<IBaseService> action)
        {
            switch (TypeBase)
            {
                case TypeBase.Sql: ExecuteSqlTransactDevice(action); break;
            }
        }

        public void ExecuteServer(Action<IBaseService> action)
        {
            switch (TypeBase)
            {
                case TypeBase.Sql: ExecuteSqlServer(action); break;
            }
        }

        public void ExecuteTransactServer(Action<IBaseService> action)
        {
            switch (TypeBase)
            {
                case TypeBase.Sql: ExecuteSqlTransactServer(action); break;
            }
        }

        private string GetConnectionStringDevice()
        {
            string cstr = string.Empty;
            if (!string.IsNullOrEmpty(ConfigConnectNameDevice) && !string.IsNullOrWhiteSpace(ConfigConnectNameDevice))
            {
                cstr = ConfigurationManager.ConnectionStrings[ConfigConnectNameDevice].ConnectionString;
            }
            else
            {
                if (!string.IsNullOrEmpty(ConnectStringDevice) && !string.IsNullOrWhiteSpace(ConnectStringDevice))
                {
                    cstr = ConnectStringDevice;
                }
            }
            return cstr;
        }

        private string GetConnectionStringServer()
        {
            string cstr = string.Empty;
            if (!string.IsNullOrEmpty(ConfigConnectNameServer) && !string.IsNullOrWhiteSpace(ConfigConnectNameServer))
            {
                cstr = ConfigurationManager.ConnectionStrings[ConfigConnectNameServer].ConnectionString;
            }
            else
            {
                if (!string.IsNullOrEmpty(ConnectStringServer) && !string.IsNullOrWhiteSpace(ConnectStringServer))
                {
                    cstr = ConnectStringServer;
                }
            }
            return cstr;
        }

        #region SqlServiceDevice
        private void ExecuteSqlDevice(Action<IBaseService> action)
        {
            using (System.Data.SqlClient.SqlConnection connect = new System.Data.SqlClient.SqlConnection(GetConnectionStringDevice()))
            {
                try
                {
                    connect.Open();
                    var serv = new SqlBaseService(connect);
                    action(serv);
                }
                catch (Exception ex)
                {
                    /*
                    if (ActionException != null)
                    {
                        ActionException(ex);
                    }
                    else
                    {
                        throw ex;
                    }
                    */
                    Log.Instance.SaveError($"Ошибка {ex.Message} {ex.StackTrace}");
                }
                finally
                {
                    if (connect != null && connect.State == ConnectionState.Open)
                    {
                        connect.Close();
                    }
                }

            }
        }
        private void ExecuteSqlTransactDevice(Action<IBaseService> action)
        {
            using (System.Data.SqlClient.SqlConnection connect = new System.Data.SqlClient.SqlConnection(GetConnectionStringDevice()))
            {
                connect.Open();
                using (var tran = connect.BeginTransaction())
                {
                    try
                    {
                        var serv = new SqlBaseService(connect,tran);
                        action(serv);
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            tran.Rollback();
                        }
                        catch
                        {
                        }
                        Log.Instance.SaveError($"Ошибка {ex.Message} {ex.StackTrace}");

                        //if (ActionException != null)
                        //{
                        //    ActionException(ex);
                        //}
                        //else
                        //{
                        //    throw ex;
                        //}
                    }
                    finally
                    {
                        if (connect != null && connect.State == ConnectionState.Open)
                        {
                            connect.Close();
                        }
                    }
                }
            }
        }
        #endregion

        #region SqlServiceServer
        private void ExecuteSqlServer(Action<IBaseService> action)
        {
            using (System.Data.SqlClient.SqlConnection connect = new System.Data.SqlClient.SqlConnection(GetConnectionStringServer()))
            {
                try
                {
                    connect.Open();
                    var serv = new SqlBaseService(connect);
                    action(serv);
                }
                catch (Exception ex)
                {
                    Log.Instance.SaveError($"Ошибка {ex.Message} {ex.StackTrace}");

                    //if (ActionException != null)
                    //{
                    //    ActionException(ex);
                    //}
                    //else
                    //{
                    //    throw ex;
                    //}
                }
                finally
                {
                    if (connect != null && connect.State == ConnectionState.Open)
                    {
                        connect.Close();
                    }
                }

            }
        }
        private void ExecuteSqlTransactServer(Action<IBaseService> action)
        {
            using (System.Data.SqlClient.SqlConnection connect = new System.Data.SqlClient.SqlConnection(GetConnectionStringServer()))
            {
                connect.Open();
                using (var tran = connect.BeginTransaction())
                {
                    try
                    {
                        var serv = new SqlBaseService(connect, tran);
                        action(serv);
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            tran.Rollback();
                        }
                        catch
                        {
                        }
                        Log.Instance.SaveError($"Ошибка {ex.Message} {ex.StackTrace}");

                        //if (ActionException != null)
                        //{
                        //    ActionException(ex);
                        //}
                        //else
                        //{
                        //    throw ex;
                        //}
                    }
                    finally
                    {
                        if (connect != null && connect.State == ConnectionState.Open)
                        {
                            connect.Close();
                        }
                    }
                }
            }
        }
        #endregion

    }

    public enum TypeBase
    {
        Sql=0,
        MySql=1
    }
}
