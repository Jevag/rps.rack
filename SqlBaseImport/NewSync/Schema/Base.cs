﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase.Schema
{
    public class Base
    {
        /// <summary>
        /// Коллекция таблиц
        /// </summary>
        private List<TableBase> tables = null;
        /// <summary>
        /// Коллекция таблиц
        /// </summary>
        public List<TableBase> Tables
        {
            get
            {
                if (tables == null)
                {
                    tables = new List<TableBase>();
                }
                return tables;
            }
        }

        public TableBase GetTable(string nametable)
        {
            TableBase tbl = null;
            var find = (from t in Tables where t.Name == nametable select t).FirstOrDefault();
            if (find != null)
            {
                tbl = find;
            }
            else
            {
                tbl = new TableBase();tbl.TypeSync = TypeSync.None;tbl.IsRemove = false; tbl.Name = nametable; Tables.Add(tbl);
            }
            return tbl;
        }

        public TableBase FindTable(string nametable)
        {
            TableBase tbl = null;
            var find = (from t in Tables where t.Name == nametable select t).FirstOrDefault();
            if (find != null)
            {
                tbl = find;
            }
            return tbl;
        }
    }
}
