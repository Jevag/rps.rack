﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase.Schema
{
    public class SchemaBase : Notifier
    {
        public static string GetStringSchema()
        {
            StringBuilder b = new StringBuilder();
            b.Append($" SELECT  T.TABLE_SCHEMA, T.TABLE_NAME, C.COLUMN_NAME, C.DATA_TYPE,");
            b.Append($" convert(bit,COLUMNPROPERTY (OBJECT_ID(T.TABLE_SCHEMA + '.' + T.TABLE_NAME),C.COLUMN_NAME, 'IsIdentity')) as IsIdentity,");
            b.Append($" convert(bit, COLUMNPROPERTY (OBJECT_ID(T.TABLE_SCHEMA + '.' + T.TABLE_NAME),C.COLUMN_NAME, 'AllowsNull')) as AllowsNull,");
            b.Append($" COLUMNPROPERTY (OBJECT_ID(T.TABLE_SCHEMA + '.' + T.TABLE_NAME),C.COLUMN_NAME, 'Precision') as [Length],");
            b.Append($" COLUMNPROPERTY (OBJECT_ID(T.TABLE_SCHEMA + '.' + T.TABLE_NAME),C.COLUMN_NAME, 'IsComputed') as [IsComputed],");
            b.Append($" (SELECT TOP 1 def.definition  FROM sys.default_constraints def  WHERE def.object_id =COLUMNPROPERTY (OBJECT_ID(T.TABLE_SCHEMA + '.' + T.TABLE_NAME),C.COLUMN_NAME, 'Default')) AS 'Default',");
            b.Append($" (select top 1 col.[definition] FROM sys.Computed_columns AS col INNER JOIN sys.objects AS obj ON col.object_id = obj.object_id	where t.TABLE_NAME=obj.name and C.COLUMN_NAME=col.name) as [Formula],");
            b.Append($" tcons.CONSTRAINT_NAME as constname,");
            b.Append($" t1.CONSTRAINT_TYPE as consttype,");
            b.Append($" trefconst.UNIQUE_CONSTRAINT_NAME as uniccontrain,");
            b.Append($" tprim.COLUMN_NAME as PKCOL,");
            b.Append($" tprim.TABLE_NAME as PKTable,");
            b.Append($" T.TABLE_TYPE as TableType");
            b.Append($" FROM INFORMATION_SCHEMA.TABLES AS T (NOLOCK)");
            b.Append($" INNER JOIN INFORMATION_SCHEMA.COLUMNS AS C (NOLOCK)   ON C.TABLE_SCHEMA = T.TABLE_SCHEMA AND C.TABLE_NAME = T.TABLE_NAME");
            b.Append($" left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE as tcons on C.COLUMN_NAME=tcons.COLUMN_NAME and C.TABLE_NAME=tcons.TABLE_NAME");
            b.Append($" left join INFORMATION_SCHEMA.TABLE_CONSTRAINTS as t1 on t1.CONSTRAINT_NAME=tcons.CONSTRAINT_NAME");
            b.Append($" left join INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS as trefconst on tcons.CONSTRAINT_NAME=trefconst.CONSTRAINT_NAME");
            b.Append($" left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE as tprim on trefconst.UNIQUE_CONSTRAINT_NAME=tprim.CONSTRAINT_NAME");
            b.Append($" WHERE");
            b.Append($" T.TABLE_TYPE = 'BASE TABLE'");
            b.Append($" ORDER BY T.TABLE_SCHEMA, T.TABLE_NAME, C.COLUMN_NAME");
            return b.ToString();
        }

        private string _TABLE_SCHEMA;
        public string TABLE_SCHEMA
        {
            get { return _TABLE_SCHEMA; }
            set
            {
                if (_TABLE_SCHEMA != value)
                {
                    _TABLE_SCHEMA = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _TABLE_NAME;
        public string TABLE_NAME
        {
            get { return _TABLE_NAME; }
            set
            {
                if (_TABLE_NAME != value)
                {
                    _TABLE_NAME = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _COLUMN_NAME;
        public string COLUMN_NAME
        {
            get { return _COLUMN_NAME; }
            set
            {
                if (_COLUMN_NAME != value)
                {
                    _COLUMN_NAME = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _DATA_TYPE;
        public string DATA_TYPE
        {
            get { return _DATA_TYPE; }
            set
            {
                if (_DATA_TYPE != value)
                {
                    _DATA_TYPE = value;
                    OnPropertyChanged();
                }
            }
        }
        private bool? _IsIdentity;
        public bool? IsIdentity
        {
            get { return _IsIdentity; }
            set
            {
                if (_IsIdentity != value)
                {
                    _IsIdentity = value;
                    OnPropertyChanged();
                }
            }
        }
        private bool? _AllowsNull;
        public bool? AllowsNull
        {
            get { return _AllowsNull; }
            set
            {
                if (_AllowsNull != value)
                {
                    _AllowsNull = value;
                    OnPropertyChanged();
                }
            }
        }
        private int? _Length;
        public int? Length
        {
            get { return _Length; }
            set
            {
                if (_Length != value)
                {
                    _Length = value;
                    OnPropertyChanged();
                }
            }
        }
        private int? _IsComputed;
        public int? IsComputed
        {
            get { return _IsComputed; }
            set
            {
                if (_IsComputed != value)
                {
                    _IsComputed = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _Default;
        public string Default
        {
            get { return _Default; }
            set
            {
                if (_Default != value)
                {
                    _Default = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _Formula;
        public string Formula
        {
            get { return _Formula; }
            set
            {
                if (_Formula != value)
                {
                    _Formula = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _constname;
        public string constname
        {
            get { return _constname; }
            set
            {
                if (_constname != value)
                {
                    _constname = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _consttype;
        public string consttype
        {
            get { return _consttype; }
            set
            {
                if (_consttype != value)
                {
                    _consttype = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _uniccontrain;
        public string uniccontrain
        {
            get { return _uniccontrain; }
            set
            {
                if (_uniccontrain != value)
                {
                    _uniccontrain = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _PKCOL;
        public string PKCOL
        {
            get { return _PKCOL; }
            set
            {
                if (_PKCOL != value)
                {
                    _PKCOL = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _PKTable;
        public string PKTable
        {
            get { return _PKTable; }
            set
            {
                if (_PKTable != value)
                {
                    _PKTable = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _TableType;
        public string TableType
        {
            get { return _TableType; }
            set
            {
                if (_TableType != value)
                {
                    _TableType = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
