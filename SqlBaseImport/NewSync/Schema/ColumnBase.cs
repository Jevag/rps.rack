﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase.Schema
{
    public class ColumnBase
    {
        public TableBase Table { get; set; }
        public string Name { get; set; }
        public string FullSqlType { get; set; }
        public int Length { get; set; }
        public bool AllowNull { get; set; }
        public string ConstName { get; set; }
        public ColumnBase Link { get; set; }
        public bool IsIdentity { get; set; }
        public bool IsPrimaryKey { get; set; }

        public bool DataEqual(ColumnBase col)
        {
            bool result = false;
            if (GetNormaliseString(Name) == GetNormaliseString(col.Name)
                && GetNormaliseString(FullSqlType) == GetNormaliseString(col.FullSqlType) && AllowNull == col.AllowNull
                && EqualLink(Link, col.Link))
            {
                result = true;
            }
            return result;
        }

        public string GetNormaliseString(string data)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(data) && !string.IsNullOrWhiteSpace(data))
            { result = data.Trim().ToLower(); }
            return result;
        }

        public bool EqualLink(ColumnBase column1, ColumnBase column2)
        {
            bool result = false;
            if (column1 != null && column2 != null)
            {
                if (GetNormaliseString(column1.Name) == GetNormaliseString(column2.Name)
                    && GetNormaliseString(column1.Table.Name) == GetNormaliseString(column2.Table.Name))
                {
                    result = true;
                }
            }
            if (column1 == null && column2 == null)
            {
                result = true;
            }
            return result;
        }

        public override string ToString()
        {
            string res = this.Name;
            return res;
        }
    }
}
