﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase.Schema
{
    public class ContextBase
    {
        private static Base currentBaseDevice;
        public static Base CurrentBaseDevice
        {
            get
            {
                if (currentBaseDevice == null)
                {
                    currentBaseDevice = new Base();
                    Log.Instance.SaveInfo("Чтение структуры БД устройства");
                    FactoryBaseService.Service.ExecuteDevice((IBaseService serv) => 
                    {
                        ReadCurrentSchema(currentBaseDevice,serv, ExceptTables, null);
                        Log.Instance.SaveInfo("Чтение структуры БД устройства прошло успешно");
                        Log.Instance.SaveInfo(TypeModule.CalculateOrder, "Начинаем рассчитывать порядок обновления таблиц устройства");
                        CalculateOrderBase(currentBaseDevice);
                        Log.Instance.SaveInfo(TypeModule.CalculateOrder, "Порядок обновления таблиц устройства рассчитан");
                    });
                }
                return currentBaseDevice;
            }
        }

        private static Base currentBaseServer;
        public static Base CurrentBaseServer
        {
            get
            {
                if (currentBaseServer == null)
                {
                    currentBaseServer = new Base();
                    Log.Instance.SaveInfo("Чтение структуры БД сервера");
                    FactoryBaseService.Service.ExecuteServer((IBaseService serv) =>
                    {
                        ReadCurrentSchema(currentBaseServer, serv, ExceptTables, null);
                        Log.Instance.SaveInfo("Чтение структуры БД сервера прошло успешно");

                        Log.Instance.SaveInfo(TypeModule.CalculateOrder, "Начинаем рассчитывать порядок обновления таблиц сервера");
                        CalculateOrderBase(currentBaseServer);
                        Log.Instance.SaveInfo(TypeModule.CalculateOrder, "Порядок обновления таблиц сервера рассчитан");

                    });
                }
                return currentBaseServer;
            }
        }

        public static string[] ExceptTables = { };
        public static string[] ExceptEditColumns = { };

        public static void ReadCurrentSchema(Base current,IBaseService baseService, string[] excepttable, string[] exceptcolumns)
        {
            SortedList<string, string> sortexcepttable = new SortedList<string, string>();
            if (excepttable != null)
            {
                foreach (var item in excepttable)
                {
                    if (!sortexcepttable.ContainsKey(item))
                    {
                        sortexcepttable.Add(item, item);
                    }
                }
            }

            SortedList<string, string> sortexceptcolumn = new SortedList<string, string>();
            if (exceptcolumns != null)
            {
                foreach (var item in exceptcolumns)
                {
                    if (!sortexceptcolumn.ContainsKey(item))
                    {
                        sortexceptcolumn.Add(item, item);
                    }
                }
            }

            ObservableCollection<SchemaBase> sourcecollect = new ObservableCollection<SchemaBase>();
                string qr = SchemaBase.GetStringSchema();
            baseService.LoadListObject<SchemaBase>(qr, null, sourcecollect);


            foreach (SchemaBase row in sourcecollect)
            {
                string nametable = row.TABLE_NAME;
                if (!sortexcepttable.ContainsKey(nametable))
                {
                    var table = current.GetTable(nametable);
                    string namecolumn = row.COLUMN_NAME;
                    if (!sortexceptcolumn.ContainsKey(namecolumn))
                    {
                        var column = table.GetColumn(namecolumn);
                        column.Length = row.Length.GetValueOrDefault();
                        column.AllowNull = row.AllowsNull.GetValueOrDefault(false);
                        column.ConstName = row.constname;
                        column.Name = namecolumn;
                        //row.
                        column.IsIdentity = row.IsIdentity.GetValueOrDefault();
                        //PRIMARY KEY=row.consttype;
                        if (row.consttype == "PRIMARY KEY")
                        {
                            column.IsPrimaryKey = true;
                        }
                        else
                        {
                            column.IsPrimaryKey = false;
                        }
                        if (!string.IsNullOrEmpty(row.PKTable) && !string.IsNullOrWhiteSpace(row.PKTable)
                            && !string.IsNullOrEmpty(row.PKCOL) && !string.IsNullOrWhiteSpace(row.PKCOL))
                        {
                            var tblink = current.GetTable(row.PKTable);
                            if (tblink != null)
                            {
                                column.Link = tblink.GetColumn(row.PKCOL);
                            }
                        }
                        column.FullSqlType = row.DATA_TYPE;
                        if (row.DATA_TYPE.Contains("char"))
                        {
                            if (column.Length > 0) { column.FullSqlType = $"{row.DATA_TYPE}({column.Length})"; }
                            else { column.FullSqlType = $"{row.DATA_TYPE}(MAX)"; }
                        }
                        if (row.DATA_TYPE.Contains("varbin"))
                        {
                            column.FullSqlType = $"{row.DATA_TYPE}(MAX)";
                        }
                    }
                }
            }
        }

        public static void CalculateOrderBase(Base db)
        {
            
            List<TableBase> ordered = db.Tables.FindAll(x => x.LinkedTables.Count == 0).OrderBy(x => x.Name).ToList();
            List<TableBase> noorder = db.Tables.Except(ordered).ToList().OrderBy(x => x.LinkedTables.Count).ToList();
            while (noorder.Count != 0)
            {
                foreach (var linkTable in noorder)
                {
                    if (linkTable.CanUpdate(ordered.Select(x => x.Name).ToList()))
                    {
                        ordered.Add(linkTable);
                    }
                }
                noorder = db.Tables.Except(ordered).ToList().OrderBy(x => x.LinkedTables.Count).ToList();
            }
            var i = 0;
            foreach (var lt in ordered)
            {
                i++;
                lt.Order = i;
            }
            
        }

        private static int currentLevel = 0;
        public static int CurrentLevel
        {
            get
            {
                Log.Instance.SaveInfo(TypeModule.Sync, $"Читаем текущий уровень БД");
                if (currentLevel<=0)
                {
                    string qr = "select top 1 CurrentLevel from ConfigSync";
                    FactoryBaseService.Service.ExecuteDevice((IBaseService serv) => 
                    {
                        var reslevel = serv.ExecuteScalar<int>(qr, null);
                        if(reslevel.Item1)
                        {
                            currentLevel = reslevel.Item2;
                        }
                    });
                    return currentLevel;
                }
                Log.Instance.SaveInfo(TypeModule.Sync, $"Текущий уровень БД равен {currentLevel}");
                return currentLevel;
            }
            set
            {
                currentLevel = value;
                Log.Instance.SaveInfo(TypeModule.Sync, $"Установлен текущий уровень синхронизации он равен {currentLevel}");
            }
        }

        public static void FillInfoSyncFromDevice()
        {
            foreach(var item in CurrentBaseDevice.Tables)
            {
                item.TypeSync = TypeSync.None;
                item.LastChange = DateTime.MinValue;
                item.LastUpdate = DateTime.MinValue;
            }
            string fieldSyncFrom = "SyncFrom";
            if (CurrentLevel == 2)
            {
                Log.Instance.SaveInfo(TypeModule.Sync, $"Текущий уровень БД равен 2 поле для синхронизации SyncFrom2");
                fieldSyncFrom = "SyncFrom2";
            }
            else
            {
                Log.Instance.SaveInfo(TypeModule.Sync, $"Текущий уровень БД не равен 2 поле для синхронизации SyncFrom");

            }

            string qr = $"select TableName,[DateTime],LastUpdate,SyncField,isnull({fieldSyncFrom},0) as SyncFrom,isnull(IsRemove,0) as IsRemove,UslovieRemove from SyncTables";
            ObservableCollection<SyncTable> devices = new ObservableCollection<SyncTable>();
            ObservableCollection<SyncTable> servers = new ObservableCollection<SyncTable>();

            FactoryBaseService.Service.ExecuteDevice((IBaseService serv) => 
            {
                serv.LoadListObject<SyncTable>(qr, null, devices);
            });
            FactoryBaseService.Service.ExecuteServer((IBaseService serv) =>
            {
                serv.LoadListObject<SyncTable>(qr, null, servers);
            });

            foreach(var tbl in CurrentBaseDevice.Tables)
            {
                tbl.TypeSync = TypeSync.None;
                tbl.IsRemove = false;tbl.UslovieRemove = string.Empty;
            }

            DateTime zeroDate = new DateTime(2000, 1, 1);
            foreach (var item in devices)
            {
                TableBase table = CurrentBaseDevice.FindTable(item.TableName);
                if(table!=null)
                {
                    table.LastUpdate = item.LastUpdate.GetValueOrDefault();
                    if(table.LastUpdate<zeroDate)
                    {
                        table.LastUpdate = zeroDate;
                    }
                    table.IsRemove = item.IsRemove;
                    table.UslovieRemove = item.UslovieRemove;
                    table.SyncColumn=table.FindColumn(item.SyncField);
                    if(item.SyncFrom==2)
                    {
                        var findserv = (from t in servers where t.TableName == item.TableName select t).FirstOrDefault();
                        if(findserv!=null)
                        {
                            table.LastChange = findserv.DateTime.GetValueOrDefault();
                            if(table.LastChange<zeroDate)
                            {
                                table.LastChange = zeroDate;
                            }
                            table.TypeSync = TypeSync.FromServer;
                        }
                    }
                    if(item.SyncFrom==1)
                    {
                        table.LastChange = item.DateTime.GetValueOrDefault();
                        if (table.LastChange < zeroDate)
                        {
                            table.LastChange = zeroDate;
                        }
                        table.TypeSync = TypeSync.FromDevice;
                    }
                }
            }
        }

        private static SortedList<string, InfoCompareTables> cashCompare = null;
        public static SortedList<string,InfoCompareTables> CashCompare
        {
            get
            {
                if(cashCompare==null)
                {
                    cashCompare = new SortedList<string, InfoCompareTables>();
                }
                return cashCompare;
            }
        }
        public static InfoCompareTables CompareTables(string NameTable)
        {
            InfoCompareTables info = null;
            if(CashCompare.ContainsKey(NameTable))
            {
                info = CashCompare[NameTable];
            }
            else
            {
                info = new InfoCompareTables();
                TableBase tblserv = CurrentBaseServer.FindTable(NameTable);
                TableBase tbldev = CurrentBaseDevice.FindTable(NameTable);
                if(tblserv!=null && tbldev!=null)
                {
                    foreach(var item in tbldev.Columns)
                    {
                        var findinserv = tblserv.FindColumn(item.Name);
                        if(findinserv!=null)
                        {
                            info.HasColumns.Add(item.Name);
                        }
                    }
                }
            }
            return info;
        }

    }

    public class SyncTable : Notifier
    {
        private string _TableName;
        public string TableName
        {
            get { return _TableName; }
            set
            {
                if (_TableName != value)
                {
                    _TableName = value;
                    OnPropertyChanged();
                }
            }
        }
        private DateTime? _DateTime;
        public DateTime? DateTime
        {
            get { return _DateTime; }
            set
            {
                if (_DateTime != value)
                {
                    _DateTime = value;
                    OnPropertyChanged();
                }
            }
        }
        private DateTime? _LastUpdate;
        public DateTime? LastUpdate
        {
            get { return _LastUpdate; }
            set
            {
                if (_LastUpdate != value)
                {
                    _LastUpdate = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _SyncField;
        public string SyncField
        {
            get { return _SyncField; }
            set
            {
                if (_SyncField != value)
                {
                    _SyncField = value;
                    OnPropertyChanged();
                }
            }
        }
        private int _SyncFrom;
        public int SyncFrom
        {
            get { return _SyncFrom; }
            set
            {
                if (_SyncFrom != value)
                {
                    _SyncFrom = value;
                    OnPropertyChanged();
                }
            }
        }
        private bool _IsRemove;
        public bool IsRemove
        {
            get { return _IsRemove; }
            set
            {
                if (_IsRemove != value)
                {
                    _IsRemove = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _UslovieRemove;
        public string UslovieRemove
        {
            get { return _UslovieRemove; }
            set
            {
                if (_UslovieRemove != value)
                {
                    _UslovieRemove = value;
                    OnPropertyChanged();
                }
            }
        }
    }

    public class InfoCompareTables
    {
        public string NameTable { get; set; }
        private List<string> hasColumns = null;
        public List<string> HasColumns
        {
            get
            {
                if(hasColumns==null)
                {
                    hasColumns = new List<string>();
                }
                return hasColumns;
            }
        }

    }
}
