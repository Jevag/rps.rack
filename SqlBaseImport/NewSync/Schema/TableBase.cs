﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase.Schema
{
    public class TableBase
    {
        public string Name { get; set; }

        public int Order { get; set; }

        /// <summary>
        /// Коллекция столбцов
        /// </summary>
        private ObservableCollection<ColumnBase> columns = null;
        /// <summary>
        /// Коллекция столбцов
        /// </summary>
        public ObservableCollection<ColumnBase> Columns
        {
            get
            {
                if (columns == null)
                {
                    columns = new ObservableCollection<ColumnBase>();
                }
                return columns;
            }
        }

        public ColumnBase GetColumn(string namecolumn)
        {
            ColumnBase col = null;
            var find = (from t in Columns where t.Name == namecolumn select t).FirstOrDefault();
            if (find != null)
            {
                col = find;
            }
            else
            {
                col = new ColumnBase(); col.Name = namecolumn; col.Table = this; Columns.Add(col);
            }
            return col;
        }

        public ColumnBase FindColumn(string namecolumn)
        {
            ColumnBase col = null;
            var find = (from t in Columns where t.Name == namecolumn select t).FirstOrDefault();
            if (find != null)
            {
                col = find;
            }
            return col;
        }

        public bool HasColumn(string name)
        {
            bool result = false;
            var find = (from t in Columns where t.Name == name select t).FirstOrDefault();
            if (find != null) { result = true; }
            return result;
        }

        public override string ToString()
        {
            string res = this.Name;
            return res;
        }

        public bool CanUpdate(List<string> tbls)
        {
            if (LinkedTables.Except(tbls).Any())
            {
                return false;
            }

            return true;
        }

        private List<string> linkedTables = null;
        public List<string> LinkedTables
        {
            get
            {
                if(linkedTables==null)
                {
                    linkedTables = new List<string>();
                    var finds = from t in Columns where t.Link != null && t.Link.Table != null select t;
                    foreach(var item in finds)
                    {
                        if (!linkedTables.Contains(item.Link.Table.Name))
                        {
                            linkedTables.Add(item.Link.Table.Name);
                        }
                    }
                }
                return linkedTables;
            }
        }

        private ColumnBase columnPrimary;
        public ColumnBase ColumnPrimary
        {
            get
            {
                if(columnPrimary==null)
                {
                    var find = (from t in Columns where t.IsPrimaryKey select t).FirstOrDefault();
                    if(find!=null)
                    {
                        columnPrimary = find;
                    }
                }
                return columnPrimary;
            }
        }

        public bool IsIdentity
        {
            get
            {
                return (from t in Columns where t.IsIdentity select t).Any();
            }
        }

        public DateTime LastChange { get; set; }
        public DateTime LastUpdate { get; set; }
        public ColumnBase SyncColumn { get; set; }
        private TypeSync typeSync = TypeSync.None;
        public TypeSync TypeSync
        {
            get { return typeSync; }
            set
            {
                typeSync = value;
            }
        }

        private bool isRemove = false;
        public bool IsRemove
        {
            get { return isRemove; }
            set { isRemove = value; }
        }
        public string UslovieRemove { get; set; }
    }

    public enum TypeSync
    {
        None=0,
        FromServer=2,
        FromDevice=1
    }
}
