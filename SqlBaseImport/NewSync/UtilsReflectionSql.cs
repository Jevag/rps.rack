﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase
{
    public static class UtilsReflectionSql
    {
        public static void FillDataFromRow(this object data, System.Data.DataRow row)
        {
            var props = data.GetType().GetProperties();
            foreach (System.Data.DataColumn col in row.Table.Columns)
            {
                string prop = col.ColumnName;
                var pinfo = data.GetType().GetProperty(col.ColumnName);
                if (pinfo != null)
                {
                    object objdata = row[prop];
                    if (objdata != null && objdata != System.DBNull.Value)
                    {
                        if (pinfo.GetSetMethod() != null)
                        {
                            pinfo.SetValue(data, row[prop]);
                        }
                    }
                }
            }
        }

        public static void FillRowFromData(this object data, System.Data.DataRow row)
        {
            var props = data.GetType().GetProperties();
            SortedList<string, PropertyInfo> sortprops = new SortedList<string, PropertyInfo>();
            foreach (var pr in props)
            {
                if (!sortprops.ContainsKey(pr.Name))
                {
                    sortprops.Add(pr.Name, pr);
                }
            }

            foreach (System.Data.DataColumn col in row.Table.Columns)
            {
                string prop = col.ColumnName;
                if (sortprops.ContainsKey(prop))
                {
                    var pinfo = sortprops[prop];
                    if (pinfo != null)
                    {
                        object objset = pinfo.GetValue(data);
                        if (objset != null && objset != System.DBNull.Value)
                        {
                            row[col.ColumnName] = objset;
                        }
                        else
                        {
                            //row[col.ColumnName] = objset;
                        }
                    }
                }
            }
            sortprops.Clear();
        }

        public static void FillRowFromOtherRow(this DataRow torow, DataRow fromrow)
        {
            SortedList<string, DataColumn> columnstorow = new SortedList<string, DataColumn>();
            foreach (DataColumn column in torow.Table.Columns)
            {
                if (!columnstorow.ContainsKey(column.ColumnName))
                {
                    columnstorow.Add(column.ColumnName, column);
                }
            }
            foreach (DataColumn col in fromrow.Table.Columns)
            {
                if (columnstorow.ContainsKey(col.ColumnName))
                {
                    torow[columnstorow[col.ColumnName]] = fromrow[col];
                }
            }
        }

        /// <summary>
        /// Функция возвращающая значение в заданном свойстве
        /// </summary>
        /// <param name="NameField">Название свойства если оно вложеннное то передвать через точку</param>
        /// <param name="from">Объект у которого брать</param>
        /// <returns>Значение свойства</returns>
        public static object GetValue(this object from, string NameField)
        {
            object result = null;
            try
            {
                if (NameField != null && !NameField.Trim().Equals(string.Empty))
                {
                    Type currenttype = from.GetType();
                    object currentobject = from;
                    string[] paths = NameField.Split('.');
                    for (int i = 0; i < paths.Length; i++)
                    {
                        string currentprop = paths[i];
                        PropertyInfo propinfo = currenttype.GetProperty(currentprop, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                        currentobject = currenttype.InvokeMember(currentprop,
                             BindingFlags.Public | BindingFlags.NonPublic |
                             BindingFlags.Instance | BindingFlags.GetField | BindingFlags.GetProperty, null, currentobject, null);
                        if (currentobject != null)
                        {
                            currenttype = currentobject.GetType();
                        }
                        else
                        {
                            break;
                        }
                    }
                    result = currentobject;
                }
            }
            catch { }
            return result;
        }

        /// <summary>
        /// Функция которая устанавливает значение у заданного свойства
        /// </summary>
        /// <param name="NameField">Название свойства если оно вложеннное то передвать через точку</param>
        /// <param name="DataObject">Данные которые необходимо установить</param>
        /// <param name="ContainObject">Объект у которого необходимо установть эти данные</param>
        public static void SetValue(this object ContainObject, string NameField, object DataObject)
        {
            Type currenttype = ContainObject.GetType();
            object currentobject = ContainObject;
            string[] paths = NameField.Split('.');
            string endNameProp = NameField;
            //foreach (string currentprop in paths)
            for (int i = 0; i < paths.Length; i++)
            {
                string currentprop = paths[i];
                if (i == paths.Length - 1)
                {
                    currenttype.InvokeMember(currentprop,
                                             BindingFlags.Public | BindingFlags.NonPublic |
                                             BindingFlags.Instance | BindingFlags.SetField |
                                              BindingFlags.SetProperty, null,
                                              currentobject, new object[] { DataObject });
                }
                else
                {
                    //PropertyInfo propinfo = currenttype.GetProperty(currentprop, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    currentobject = currenttype.InvokeMember(currentprop,
                         BindingFlags.Public | BindingFlags.NonPublic |
                         BindingFlags.Instance | BindingFlags.GetField | BindingFlags.GetProperty, null, currentobject, null);
                    currenttype = currentobject.GetType();
                }
            }
        }

        /// <summary>
        /// Проверяет наличе свойства
        /// у заданного типа
        /// </summary>
        /// <param name="TpObject">Тип у которого проверяем</param>
        /// <param name="NameProperty">Название свойства</param>
        /// <returns>true-есть свойства false-нет свойства</returns>
        public static bool HasProperty(this Type TpObject, string NameProperty)
        {
            bool find = false;
            Type currenttype = TpObject;
            string[] paths = NameProperty.Split('.');
            string endNameProp = NameProperty;
            for (int i = 0; i < paths.Length; i++)
            {
                string currentprop = paths[i];
                PropertyInfo propinfo = currenttype.GetProperty(currentprop, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                currenttype = currenttype.BaseType;
                if (i == paths.Length - 1)
                {
                    if (propinfo != null)
                    {
                        find = true;
                    }
                }
            }
            return find;
        }

        public static Type GetTypeProperty(this object data, string NameProperty)
        {
            Type result = null;
            Type currenttype = data.GetType(); ;
            string[] paths = NameProperty.Split('.');
            string endNameProp = NameProperty;
            for (int i = 0; i < paths.Length; i++)
            {
                string currentprop = paths[i];
                PropertyInfo propinfo = currenttype.GetProperty(currentprop, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                currenttype = currenttype.BaseType;
                if (i == paths.Length - 1)
                {
                    if (propinfo != null)
                    {
                        result = propinfo.PropertyType;
                    }
                }
            }
            return result;
        }

        public static bool HasPropertyObject(this object data, string NameProperty)
        {
            bool result = false;
            if (data != null)
            {
                result = HasProperty(data.GetType(), NameProperty);
            }

            return result;
        }

        /// <summary>
        /// Выполнаяет статический метод
        /// </summary>
        /// <param name="NameMethod">Название метода</param>
        /// <param name="TypeRun">Тип у которого вызываеться</param>
        /// <param name="Parametrs">Параметры которые необходимо передать</param>
        /// <returns>Результат выполнения метода</returns>
        public static object InvokeStaticMethod(this Type TypeRun, string NameMethod, object[] Parametrs)
        {
            object result = null;
            MethodInfo minfo = TypeRun.GetMethod(NameMethod, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
            if (minfo != null)
            {
                result = minfo.Invoke(null, Parametrs);
            }
            return result;
        }

        public static object GetStaticValue(this Type TypeRun, string NameProperty)
        {
            object result = null;
            var minfo = TypeRun.GetMember(NameProperty, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
            if (minfo != null)
            {
                result = TypeRun.InvokeMember(NameProperty,
                     BindingFlags.Public | BindingFlags.NonPublic |
                     BindingFlags.Static | BindingFlags.GetField | BindingFlags.GetProperty, null, null, null);
            }
            return result;
        }

        /// <summary>
        /// Запуск на выполнение статического метода
        /// </summary>
        /// <param name="TypeRun">Тип у которого запускаем</param>
        /// <param name="NameMethod">Название метода который запускаем</param>
        /// <param name="Parametrs">Парметры для запсука</param>
        /// <param name="Signatures">Описание сигнатуры метода</param>
        /// <returns>Результат выполнения метода</returns>
        public static object InvokeStaticMethod(this Type TypeRun, string NameMethod, object[] Parametrs, Type[] Signatures)
        {
            object result = null;
            MethodInfo minfo = TypeRun.GetMethod(NameMethod,
                              BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreCase,
                              null, CallingConventions.Any, Signatures, null);
            if (minfo != null)
            {
                result = minfo.Invoke(null, Parametrs);
            }
            return result;
        }

        /// <summary>
        /// Выполнаяет не статический метод
        /// у заданного объекта
        /// </summary>
        /// <param name="NameMethod">Название метода</param>
        /// <param name="ObjectRun">Объект у которого вызываеться</param>
        /// <param name="Parametrs">Параметры которые необходимо передать</param>
        /// <returns>Результат выполнения метода</returns>
        public static object InvokeMethod(this object ObjectRun, string NameMethod, object[] Parametrs)
        {
            object result = null;
            if (ObjectRun != null)
            {
                Type TypeRun = ObjectRun.GetType();
                MethodInfo minfo = TypeRun.GetMethod(NameMethod, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
                if (minfo != null)
                {
                    result = minfo.Invoke(ObjectRun, Parametrs);
                }
            }
            return result;
        }

        /// <summary>
        /// Установа свойств из одного объекта в другой
        /// ключ название свойств
        /// </summary>
        /// <param name="ToObject"></param>
        /// <param name="FromObject"></param>
        public static void FillPropertyObjcet(this object ToObject, object FromObject)
        {
            if (FromObject != null && ToObject != null)
            {
                PropertyInfo[] properties = ToObject.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                if (properties != null && properties.Length > 0)
                {
                    foreach (PropertyInfo property in properties)
                    {
                        if (property.GetSetMethod() != null && property.GetGetMethod() != null)
                        {
                            if (FromObject.GetType().HasProperty(property.Name))
                            {
                                object getvalue = FromObject.GetValue(property.Name);
                                //if (getvalue != null)
                                //{
                                ToObject.SetValue(property.Name, getvalue);
                                //}
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Возвращает список свойст у заданного типа
        /// которые не реализуют заданный интерфейс
        /// </summary>
        /// <param name="typefrom">Тип у которого беруться свойства</param>
        /// <param name="nameInterface">Название интерфейса с которыми проверяеться</param>
        /// <returns>Список свойст для данного типа не реализующий заданный интерфейс</returns>
        public static List<InfoPropertySql> GetPropertiesNotHasInterface(Type typefrom, string nameInterface)
        {
            List<InfoPropertySql> lstProp = new List<InfoPropertySql>();
            PropertyInfo[] propinfos = typefrom.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (PropertyInfo pinfo in propinfos)
            {
                Type tpobj = pinfo.PropertyType.GetInterface(nameInterface, true);
                if (tpobj != null)
                {
                }
                else
                {
                    InfoPropertySql info = new InfoPropertySql();
                    info.propInfo = pinfo;


                    info.ShowName = pinfo.Name;
                    object[] attribs = pinfo.GetCustomAttributes(typeof(DisplayNameAttribute), true);
                    if (attribs != null && attribs.Length > 0)
                    {
                        DisplayNameAttribute disp = attribs[0] as DisplayNameAttribute;
                        if (disp != null)
                        {
                            info.ShowName = disp.DisplayName;
                        }
                    }
                    if (info.ShowName.Trim().Equals(string.Empty))
                    {
                        info.ShowName = pinfo.Name;
                    }
                    lstProp.Add(info);
                }
            }
            return lstProp;
        }

        public static SortedList<string, PropertyInfo> GetProperties(Type typefrom)
        {
            SortedList<string, PropertyInfo> lstProp = new SortedList<string, PropertyInfo>();
            PropertyInfo[] propinfos = typefrom.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (PropertyInfo pinfo in propinfos)
            {
                lstProp.Add(pinfo.Name, pinfo);
            }
            return lstProp;
        }

        public static InfoFieldSql[] GetConstStrings(Type tp)
        {
            List<InfoFieldSql> lst = new List<InfoFieldSql>();

            var items = from t in tp.GetFields()
                        where t.IsLiteral == true
                        select t;
            foreach (FieldInfo finf in items)
            {
                InfoFieldSql inf = new InfoFieldSql();
                inf.NameField = finf.Name;
                inf.ShowName = finf.Name;
                object[] attribs = finf.GetCustomAttributes(typeof(DisplayNameAttribute), true);
                if (attribs != null && attribs.Length > 0)
                {
                    DisplayNameAttribute disp = attribs[0] as DisplayNameAttribute;
                    if (disp != null)
                    {
                        inf.ShowName = disp.DisplayName;
                    }
                }
                object objvalue = finf.GetRawConstantValue();
                if (objvalue != null)
                {
                    inf.ContantValue = objvalue.ToString();
                }
                lst.Add(inf);

            }
            return lst.ToArray();
        }

        public static void GetConstStrings(Type tp, List<InfoFieldSql> lst)
        {

            var items = from t in tp.GetFields()
                        where t.IsLiteral == true
                        select t;
            foreach (FieldInfo finf in items)
            {
                InfoFieldSql inf = new InfoFieldSql();
                inf.NameField = finf.Name;
                inf.ShowName = finf.Name;
                lst.Add(inf);
                object[] attribs = finf.GetCustomAttributes(typeof(DisplayNameAttribute), true);
                if (attribs != null && attribs.Length > 0)
                {
                    DisplayNameAttribute disp = attribs[0] as DisplayNameAttribute;
                    if (disp != null)
                    {
                        inf.ShowName = disp.DisplayName;
                    }
                }
                object objvalue = finf.GetRawConstantValue();
                if (objvalue != null)
                {
                    inf.ContantValue = objvalue.ToString();
                }
            }
        }

        /// <summary>
        /// Получение имени свойства по по лямбда-выражению
        /// </summary>
        /// <typeparam name="TItem">тип</typeparam>
        /// <typeparam name="TResult">тип свойства</typeparam>
        /// <param name="expression">лямбда-выражение</param>
        /// <returns>имя свойства</returns>
        public static string GetPropertyName<TItem, TResult>(Expression<Func<TItem, TResult>> expression)
        {
            if (expression.Body.NodeType == ExpressionType.Convert)
                return (((MemberExpression)(((UnaryExpression)(expression.Body)).Operand)).Member).Name;
            if (expression.Body.NodeType == ExpressionType.MemberAccess)
                return ((MemberExpression)expression.Body).Member.Name;
            throw new NotSupportedException("This overload accepts only member access lambda expressions");
        }

        private static long GetKey<T>(Expression<Func<T, long>> expression)
        {
            var exp = expression;
            if (exp != null)
            {
                //exp.Compile().Invoke();
            }
            return 10;
        }


        /// <summary>
        /// Получение имени свойства по по лямбда-выражению
        /// </summary>
        /// <typeparam name="TItem">тип</typeparam>
        /// <param name="expression">лямбда-выражение</param>
        /// <returns>имя свойства</returns>
        public static string GetPropertyName<TItem>(Expression<Func<TItem, object>> expression)
        {
            return GetPropertyName<TItem, object>(expression);
        }

        public static void FillInfoDynamicFromDataRow(InfoDynamic to, DataRow from)
        {
            if (from != null && to != null)
            {
                foreach (DataColumn col in from.Table.Columns)
                {
                    to.SetData(col.ColumnName, from[col]);
                }
            }
        }

        public static void FillRowFromInfoDynamic(DataRow to, InfoDynamic from)
        {
            if (from != null && to != null)
            {
                foreach (DataColumn col in to.Table.Columns)
                {
                    if (from.HasProperty(col.ColumnName))
                    {
                        object dat = from.GetData(col.ColumnName);
                        if (dat != null)
                        {
                            to[col] = dat;
                        }
                        else
                        {
                            to[col] = System.DBNull.Value;
                        }
                    }
                }
            }
        }

    }

    public class InfoFieldSql
    {
        private bool _isSelect = false;
        public bool IsSelect
        {
            get { return _isSelect; }
            set { _isSelect = value; }
        }
        public string NameField { get; set; }
        public string ShowName { get; set; }

        public string ContantValue { get; set; }

        public override string ToString()
        {
            string result = ShowName;
            return ShowName;
        }

        public override bool Equals(object obj)
        {
            InfoFieldSql tetsobj = obj as InfoFieldSql;
            bool result = false;
            if (tetsobj != null)
            {
                result = this.NameField.Equals(tetsobj.NameField);
            }
            return result;
        }

    }

    public class InfoPropertySql
    {
        public PropertyInfo propInfo;
        public string ShowName;
        public object[] FindAttributes;

        public override string ToString()
        {
            string result = ShowName;
            if (result.Trim().Equals(string.Empty))
            {
                result = propInfo.Name;
            }
            return ShowName;
        }

        public override bool Equals(object obj)
        {
            InfoPropertySql tetsobj = obj as InfoPropertySql;
            bool result = false;
            if (tetsobj != null)
            {
                result = tetsobj.propInfo.Equals(this.propInfo);
            }
            return result;
        }
        public override int GetHashCode()
        {
            int result = -1;
            if (propInfo != null)
            {
                result = this.propInfo.GetHashCode();
            }
            return result;
        }
    }
}
