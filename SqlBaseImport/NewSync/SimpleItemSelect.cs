﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeXBase
{
    public class SimpleItemSelect:Notifier
    {
        private int id;
        public int Id
        {
            get { return id; }
            set
            {
                if(id!=value)
                {
                    id = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool isSelect;
        public bool IsSelect
        {
            get { return isSelect; }
            set
            {
                if(isSelect!=value)
                {
                    isSelect = value;
                    OnPropertyChanged();
                }
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if(name!=value)
                {
                    name = value;
                    OnPropertyChanged();
                }
            }
        }

        private object tag;
        public object Tag
        {
            get { return tag; }
            set
            {
                if(tag!=value)
                {
                    tag = value;
                    OnPropertyChanged();
                }
            }
        }
        
    }
}
