﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlBaseImport.Model
{
    public class Log
    {
        private static Log instance = null;
        public static Log Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Log();
                }
                return instance;
            }
        }

        private int LevelLog = 1;
        private DateTime GetLevelLog = DateTime.Now.AddMinutes(-30);
        private string KeyLevel = "LevelLog";

        /// <summary>
        /// 0- только ошибки
        /// 1-ошибки и информация
        /// </summary>
        /// <param name="level"></param>
        public void SetLevelLog(int level)
        {
            this.LevelLog = level;
        }

        private int ReadLevelLog()
        {
            if (DateTime.Now.Subtract(GetLevelLog).Minutes > 20)
            {
                try
                {
                    GetLevelLog = DateTime.Now;
                    string[] keys = ConfigurationManager.AppSettings.AllKeys;
                    if (keys != null && keys.Length > 0 && keys.Contains("LevelLog"))
                    {
                        string level = ConfigurationManager.AppSettings["LevelLog"];
                        this.LevelLog = Convert.ToInt32(level);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return LevelLog;
        }

        public ILog SaveLog { get; set; }
        public void SaveError(TypeModule module, string message)
        {
            if (SaveLog != null)
            {
                SaveLog.SaveError(module, message);
            }
            else
            {
                string file = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"LogSync_{DateTime.Today.ToString("dd_MM_yyyy")}.txt");
                System.IO.File.AppendAllLines(file, new string[] { $"{DateTime.Now.ToString()}|{module}|Error|{message}" });
            }
        }
        public void SaveInfo(TypeModule module, string message)
        {
            if (ReadLevelLog() > 0)
            {
                if (SaveLog != null)
                {
                    SaveLog.SaveInfo(module, message);
                }
                else
                {
                    string file = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"LogSync_{DateTime.Today.ToString("dd_MM_yyyy")}.txt");
                    System.IO.File.AppendAllLines(file, new string[] { $"{DateTime.Now.ToString()}|{module}|Info|{message}" });
                }
            }
        }
    }

    public enum TypeModule
    {
        Curis = 0,
        ConnectDevice = 1,
        SendWeb = 2,
        Share = 3,
        Sync=4,
        CalculateOrder=5,
        CompareSchema=6
    }

    public interface ILog
    {
        void SaveError(TypeModule module, string message);
        void SaveInfo(TypeModule module, string message);
    }
}
