﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using CodeXBase.Schema;
using Dapper;
using NLog;


namespace SqlBaseImport.Model
{
    public class Utils
    {
        //private static Logger log = LogManager.GetCurrentClassLogger();
        
        public static void ConfigurateModel(BaseSync bs)
        {
            
            #region Correctorder
            var tbl_TariffScheduleModel = bs.RegisterTable("TariffScheduleModel", TypeSync.FromServer, 0);
            tbl_TariffScheduleModel.AddField("Id");
            tbl_TariffScheduleModel.AddField("Name");
            //tbl_TariffScheduleModel.AddField("_IsSync2");
            //tbl_TariffScheduleModel.AddField("_IsSync3");
            //tbl_TariffScheduleModel.AddField("_Modified");
            tbl_TariffScheduleModel.AddField("_IsDeleted");
            tbl_TariffScheduleModel.AddField("IdFC");
            tbl_TariffScheduleModel.SetKey("Id");
            tbl_TariffScheduleModel.FieldUpdate = "Sync";

            var tbl_ZoneModel = bs.RegisterTable("ZoneModel", TypeSync.FromServer, 0);
            tbl_ZoneModel.AddField("Id");
            tbl_ZoneModel.AddField("Name");
            tbl_ZoneModel.AddField("Capacity");
            tbl_ZoneModel.AddField("Reserved");
            //tbl_ZoneModel.AddField("_IsSync2");
            //tbl_ZoneModel.AddField("_IsSync3");
            //tbl_ZoneModel.AddField("_Modified");
            tbl_ZoneModel.AddField("_IsDeleted");
            tbl_ZoneModel.AddField("IdFC");
            tbl_ZoneModel.AddField("FreeSpace");
            tbl_ZoneModel.AddField("OccupId");
            tbl_ZoneModel.SetKey("Id");
            tbl_ZoneModel.FieldUpdate = "Sync";

            var tbl_ZoneGroup = bs.RegisterTable("ZoneGroup", TypeSync.FromServer, 0);
            tbl_ZoneGroup.SetKey("Id");
            tbl_ZoneGroup.FieldUpdate = "Sync";


            var tbl_Group = bs.RegisterTable("Group", TypeSync.FromServer, 0);
            tbl_Group.AddField("Id");
            tbl_Group.AddField("idFC");
            tbl_Group.AddField("Name");
            tbl_Group.SetKey("Id");
            tbl_Group.FieldUpdate = "Sync";

            var tbl_CompanyCards = bs.RegisterTable("CompanyCards", TypeSync.FromServer, 0);
            tbl_CompanyCards.AddField("Id");
            tbl_CompanyCards.AddField("CardId");
            tbl_CompanyCards.AddField("CompanyId");
            tbl_CompanyCards.SetKey("Id");
            tbl_CompanyCards.FieldUpdate = "Sync";

            var tbl_TariffModel = bs.RegisterTable("TariffModel", TypeSync.FromServer, 0);
            tbl_TariffModel.AddField("Id");
            tbl_TariffModel.AddField("Name");
            tbl_TariffModel.AddField("TimeTypeId");
            //tbl_TariffModel.AddField("_IsSync2");
            //tbl_TariffModel.AddField("_IsSync3");
            //tbl_TariffModel.AddField("_Modified");
            tbl_TariffModel.AddField("_IsDeleted");
            tbl_TariffModel.AddField("Color");
            tbl_TariffModel.SetKey("Id");
            tbl_TariffModel.FieldUpdate = "Sync";

            var tbl_Tariffs = bs.RegisterTable("Tariffs", TypeSync.FromServer, 0);
            tbl_Tariffs.AddField("Id");
            tbl_Tariffs.AddField("Name");
            tbl_Tariffs.AddField("ChangingTypeId");
            tbl_Tariffs.AddField("TypeId");
            tbl_Tariffs.AddField("Initial");
            tbl_Tariffs.AddField("InitialTimeTypeId");
            tbl_Tariffs.AddField("InitialAmount");
            tbl_Tariffs.AddField("ProtectedInterval");
            tbl_Tariffs.AddField("ProtectedIntervalTimeTypeId");
            tbl_Tariffs.AddField("FreeTime");
            tbl_Tariffs.AddField("FreeTimeTypeId");
            //tbl_Tariffs.AddField("_IsSync2");
            //tbl_Tariffs.AddField("_IsSync3");
            //tbl_Tariffs.AddField("_Modified");
            tbl_Tariffs.AddField("_IsDeleted");
            tbl_Tariffs.AddField("IdFC");
            tbl_Tariffs.SetKey("Id");
            tbl_Tariffs.FieldUpdate = "Sync";


            var tbl_TariffMaxAmountIntervalModel = bs.RegisterTable("TariffMaxAmountIntervalModel", TypeSync.FromServer, 10);
            tbl_TariffMaxAmountIntervalModel.AddField("Id");
            tbl_TariffMaxAmountIntervalModel.AddField("Amount");
            //tbl_TariffMaxAmountIntervalModel.AddField("_IsSync2");
            //tbl_TariffMaxAmountIntervalModel.AddField("_IsSync3");
            //tbl_TariffMaxAmountIntervalModel.AddField("_Modified");
            tbl_TariffMaxAmountIntervalModel.AddField("_IsDeleted");
            tbl_TariffMaxAmountIntervalModel.AddField("TariffId");
            tbl_TariffMaxAmountIntervalModel.AddField("Start");
            tbl_TariffMaxAmountIntervalModel.AddField("StartTimeTypeId");
            tbl_TariffMaxAmountIntervalModel.AddField("Duration");
            tbl_TariffMaxAmountIntervalModel.AddField("DurationTimeTypeId");
            tbl_TariffMaxAmountIntervalModel.AddField("Repeat");
            tbl_TariffMaxAmountIntervalModel.SetKey("Id");
            tbl_TariffMaxAmountIntervalModel.FieldUpdate = "Sync";


            //var tbl_ClientGroup = bs.RegisterTable("ClientGroup", TypeSync.FromServer, 10);
            //tbl_ClientGroup.AddField("Id");
            //tbl_ClientGroup.AddField("ZoneID");
            //tbl_ClientGroup.SetKey("Id");
            //tbl_ClientGroup.FieldUpdate = "Sync";

            var tbl_TariffIntervalModel = bs.RegisterTable("TariffIntervalModel", TypeSync.FromServer, 10);
            tbl_TariffIntervalModel.AddField("Id");
            tbl_TariffIntervalModel.AddField("TariffId");
            tbl_TariffIntervalModel.AddField("Start");
            tbl_TariffIntervalModel.AddField("Duration");
            tbl_TariffIntervalModel.AddField("Amount");
            //tbl_TariffIntervalModel.AddField("_IsSync2");
            //tbl_TariffIntervalModel.AddField("_IsSync3");
            //tbl_TariffIntervalModel.AddField("_Modified");
            tbl_TariffIntervalModel.AddField("_IsDeleted");
            tbl_TariffIntervalModel.SetKey("Id");
            tbl_TariffIntervalModel.FieldUpdate = "Sync";

            var tbl_Cards = bs.RegisterTable("Cards", TypeSync.FromServer, 10);
            tbl_Cards.AddField("Id");
            tbl_Cards.AddField("Blocked");
            //tbl_Cards.AddField("_IsSync2");
            //tbl_Cards.AddField("_IsSync3");
            //tbl_Cards.AddField("_Modified");
            tbl_Cards.AddField("_IsDeleted");
            tbl_Cards.AddField("CardId");
            tbl_Cards.AddField("ParkingEnterTime");
            tbl_Cards.AddField("LastRecountTime");
            tbl_Cards.AddField("LastPaymentTime");
            tbl_Cards.AddField("TSidFC");
            tbl_Cards.AddField("TPidFC");
            tbl_Cards.AddField("ZoneidFC");
            tbl_Cards.AddField("ClientGroupidFC");
            tbl_Cards.AddField("SumOnCard");
            tbl_Cards.AddField("Nulltime1");
            tbl_Cards.AddField("Nulltime2");
            tbl_Cards.AddField("Nulltime3");
            tbl_Cards.AddField("TVP");
            tbl_Cards.AddField("TKVP");
            tbl_Cards.AddField("ClientTypidFC");
            tbl_Cards.AddField("DateSaveCard");
            tbl_Cards.AddField("LastPlate");
            tbl_Cards.SetKey("Id");
            tbl_Cards.FieldUpdate = "Sync";

            var tbl_BarCodeInUse = bs.RegisterTable("BarCodeInUse", TypeSync.FromServer, 10);
            tbl_BarCodeInUse.AddField("Id");
            tbl_BarCodeInUse.AddField("IdBCM");
            tbl_BarCodeInUse.AddField("OperId");
            tbl_BarCodeInUse.AddField("CompanyId");
            tbl_BarCodeInUse.AddField("IdTP");
            tbl_BarCodeInUse.AddField("IdTS");
            tbl_BarCodeInUse.AddField("DiscountAmount");
            tbl_BarCodeInUse.AddField("GroupIdFC");
            tbl_BarCodeInUse.AddField("UsageTime");
            tbl_BarCodeInUse.AddField("GuestPlateNumber");
            tbl_BarCodeInUse.AddField("GuestCarModel");
            tbl_BarCodeInUse.AddField("GuestName");
            tbl_BarCodeInUse.AddField("GuestEmail");
            tbl_BarCodeInUse.AddField("ParkingTime");
            tbl_BarCodeInUse.AddField("EnterId");
            tbl_BarCodeInUse.AddField("ExitId");
            tbl_BarCodeInUse.AddField("IdTPForCount");
            tbl_BarCodeInUse.AddField("IdTSForCount");
            tbl_BarCodeInUse.AddField("CardId");
            tbl_BarCodeInUse.AddField("Ammount");
            tbl_BarCodeInUse.SetKey("Id");
            tbl_BarCodeInUse.FieldUpdate = "Sync";

            var tbl_ClientModel = bs.RegisterTable("ClientModel", TypeSync.FromServer, 20);
            tbl_ClientModel.AddField("Id");
            tbl_ClientModel.AddField("ContactName");
            tbl_ClientModel.AddField("Phone");
            tbl_ClientModel.AddField("Email");
            tbl_ClientModel.AddField("CardId");
            tbl_ClientModel.SetKey("Id");
            tbl_ClientModel.FieldUpdate = "Sync";

            var tbl_ClientCompany = bs.RegisterTable("ClientCompany", TypeSync.FromServer, 30);
            tbl_ClientCompany.AddField("Id");
            tbl_ClientCompany.AddField("CompanyId");
            tbl_ClientCompany.AddField("ClientId");
            tbl_ClientCompany.SetKey("Id");
            tbl_ClientCompany.FieldUpdate = "Sync";



            var tbl_ClientCars = bs.RegisterTable("ClientCars", TypeSync.FromServer, 30);
            tbl_ClientCars.AddField("Id");
            tbl_ClientCars.AddField("ClientId");
            tbl_ClientCars.AddField("CompanyId");
            tbl_ClientCars.AddField("CarModel");
            tbl_ClientCars.AddField("CarColor");
            tbl_ClientCars.AddField("CarPlate");
            tbl_ClientCars.SetKey("Id");
            tbl_ClientCars.FieldUpdate = "Sync";


            var tbl_TariffTariffPlanModel = bs.RegisterTable("TariffTariffPlanModel", TypeSync.FromServer, 20);
            tbl_TariffTariffPlanModel.AddField("Id");
            tbl_TariffTariffPlanModel.AddField("TariffId");
            tbl_TariffTariffPlanModel.AddField("TariffPlanId");
            //tbl_TariffTariffPlanModel.AddField("_IsSync2");
            //tbl_TariffTariffPlanModel.AddField("_IsSync3");
            //tbl_TariffTariffPlanModel.AddField("_Modified");
            tbl_TariffTariffPlanModel.AddField("_IsDeleted");
            tbl_TariffTariffPlanModel.SetKey("Id");
            tbl_TariffTariffPlanModel.FieldUpdate = "Sync";

            var tbl_TariffPlanPeriodModel = bs.RegisterTable("TariffPlanPeriodModel", TypeSync.FromServer, 30);
            tbl_TariffPlanPeriodModel.AddField("TariffPlanPeriodId");
            tbl_TariffPlanPeriodModel.AddField("Start");
            tbl_TariffPlanPeriodModel.AddField("End");
            //tbl_TariffPlanPeriodModel.AddField("_IsSync2");
            //tbl_TariffPlanPeriodModel.AddField("_IsSync3");
            //tbl_TariffPlanPeriodModel.AddField("_Modified");
            tbl_TariffPlanPeriodModel.AddField("_IsDeleted");
            tbl_TariffPlanPeriodModel.AddField("TariffTariffPlanId");
            tbl_TariffPlanPeriodModel.SetKey("TariffPlanPeriodId");
            tbl_TariffPlanPeriodModel.FieldUpdate = "Sync";

            var tbl_TariffPlanTariffScheduleModel = bs.RegisterTable("TariffPlanTariffScheduleModel", TypeSync.FromServer, 20);
            tbl_TariffPlanTariffScheduleModel.AddField("Id");
            tbl_TariffPlanTariffScheduleModel.AddField("TariffScheduleId");
            tbl_TariffPlanTariffScheduleModel.AddField("TariffPlanId");
            tbl_TariffPlanTariffScheduleModel.AddField("TariffPlanNextId");
            tbl_TariffPlanTariffScheduleModel.AddField("TypeId");
            //tbl_TariffPlanTariffScheduleModel.AddField("_IsSync2");
            //tbl_TariffPlanTariffScheduleModel.AddField("_IsSync3");
            //tbl_TariffPlanTariffScheduleModel.AddField("_Modified");
            tbl_TariffPlanTariffScheduleModel.AddField("_IsDeleted");
            tbl_TariffPlanTariffScheduleModel.AddField("ConditionZoneAfter");
            tbl_TariffPlanTariffScheduleModel.AddField("ConditionParkingTime");
            tbl_TariffPlanTariffScheduleModel.AddField("ConditionParkingTimeTypeId");
            tbl_TariffPlanTariffScheduleModel.AddField("ConditionEC");
            tbl_TariffPlanTariffScheduleModel.AddField("ConditionECProtectInterval");
            tbl_TariffPlanTariffScheduleModel.AddField("ConditionECProtectIntervalTimeTypId");
            tbl_TariffPlanTariffScheduleModel.AddField("ConditionAbonementPrice");
            tbl_TariffPlanTariffScheduleModel.AddField("ConditionBackTime");
            tbl_TariffPlanTariffScheduleModel.AddField("ConditionBackTimeTypeId");
            tbl_TariffPlanTariffScheduleModel.SetKey("Id");
            tbl_TariffPlanTariffScheduleModel.FieldUpdate = "Sync";

            var tbl_BlackList = bs.RegisterTable("BlackList", TypeSync.FromServer, 0);
            tbl_BlackList.AddField("Id");
            tbl_BlackList.AddField("Description");
            tbl_BlackList.AddField("Plate");
            //tbl_BlackList.AddField("_IsSync2");
            //tbl_BlackList.AddField("_IsSync3");
            //tbl_BlackList.AddField("_Modified");
            tbl_BlackList.AddField("_IsDeleted");
            tbl_BlackList.SetKey("Id");
            tbl_BlackList.FieldUpdate = "Sync";


            #endregion
            ///////////////////////////////////
            #region correct device
            var tbl_AlarmModel = bs.RegisterTable("AlarmModel", TypeSync.FromDevice, 0);
            tbl_AlarmModel.AddField("Id");
            tbl_AlarmModel.AddField("DeviceId");
            tbl_AlarmModel.AddField("Begin");
            tbl_AlarmModel.AddField("End");
            tbl_AlarmModel.AddField("Value");
            tbl_AlarmModel.AddField("ValueEnd");
            //tbl_AlarmModel.AddField("_IsSync2");
            //tbl_AlarmModel.AddField("_IsSync3");
            //tbl_AlarmModel.AddField("_Modified");
            tbl_AlarmModel.AddField("_IsDeleted");
            tbl_AlarmModel.AddField("AlarmColorId");
            tbl_AlarmModel.AddField("TypeId");
            tbl_AlarmModel.SetKey("Id");
            tbl_AlarmModel.FieldUpdate = "Sync";

            var tbl_Transactions = bs.RegisterTable("Transactions", TypeSync.FromDevice, 0);
            tbl_Transactions.AddField("Id");
            tbl_Transactions.AddField("DeviceId");
            tbl_Transactions.AddField("Time");
            tbl_Transactions.AddField("TimeEntry");
            tbl_Transactions.AddField("TimeExit");
            tbl_Transactions.AddField("ZoneAfterId");
            tbl_Transactions.AddField("ZoneBeforeId");
            //tbl_Transactions.AddField("_IsSync2");
            //tbl_Transactions.AddField("_IsSync3");
            tbl_Transactions.AddField("_IsDeleted");
            tbl_Transactions.AddField("DeviceTypeID");
            tbl_Transactions.AddField("Cardnumber");
            tbl_Transactions.AddField("ClientID");
            tbl_Transactions.AddField("TarifPlanId");
            tbl_Transactions.AddField("TariffScheduleId");
            tbl_Transactions.AddField("Nulltime1");
            tbl_Transactions.AddField("Nulltime2");
            tbl_Transactions.AddField("Nulltime3");
            tbl_Transactions.AddField("TVP");
            tbl_Transactions.AddField("TKVP");
            tbl_Transactions.AddField("SumOnCard");
            tbl_Transactions.AddField("Balance");
            tbl_Transactions.AddField("PaId");
            tbl_Transactions.AddField("ClientGroupId");
            tbl_Transactions.AddField("ClientTypidFC");
            tbl_Transactions.AddField("ZoneId");
            tbl_Transactions.AddField("LastRecountTime");
            tbl_Transactions.AddField("TimeOplat");
            tbl_Transactions.AddField("PassageTransactionType");
            tbl_Transactions.AddField("PlateNumberEntrance");
            tbl_Transactions.AddField("PlateNumberExit");
            tbl_Transactions.AddField("PaymentTransactionTypeId");
            tbl_Transactions.AddField("ChangeIssued");
            tbl_Transactions.AddField("CachDispenerUp");
            tbl_Transactions.AddField("CachDispenerDown");
            tbl_Transactions.AddField("Hopper1");
            tbl_Transactions.AddField("Hopper2");
            tbl_Transactions.AddField("BanknotesAccepted");
            tbl_Transactions.AddField("CoinsAccepted");
            tbl_Transactions.AddField("BankCardAccepted");
            tbl_Transactions.SetKey("Id");
            tbl_Transactions.FieldUpdate = "Sync";

            var tbl_DeviceRackModel = bs.RegisterTable("DeviceRackModel", TypeSync.FromDevice, 0);
            tbl_DeviceRackModel.AddField("DeviceId");
            tbl_DeviceRackModel.AddField("ZoneBeforeId");
            tbl_DeviceRackModel.AddField("ZoneAfterId");
            tbl_DeviceRackModel.AddField("DispenserTypeId");
            tbl_DeviceRackModel.AddField("BarcodeTypeId");
            tbl_DeviceRackModel.AddField("BankModuleTypeId");
            tbl_DeviceRackModel.AddField("MaxBusyTimeAntennaA");
            tbl_DeviceRackModel.AddField("MaxBusyTimeAntennaB");
            tbl_DeviceRackModel.AddField("MaxBusyTimeIR");
            //tbl_DeviceRackModel.AddField("_IsSync2");
            //tbl_DeviceRackModel.AddField("_IsSync3");
            //tbl_DeviceRackModel.AddField("_Modified");
            tbl_DeviceRackModel.AddField("_IsDeleted");
            tbl_DeviceRackModel.AddField("IdStoyky");
            tbl_DeviceRackModel.AddField("SlaveExist");
            tbl_DeviceRackModel.AddField("ReaderOutExists");
            tbl_DeviceRackModel.AddField("OutReaderTypeId");
            tbl_DeviceRackModel.AddField("ReaderInExists");
            tbl_DeviceRackModel.AddField("InReaderTypeId");
            tbl_DeviceRackModel.AddField("DispensExists");
            tbl_DeviceRackModel.AddField("FeederExists");
            tbl_DeviceRackModel.AddField("FeederTypeId");
            tbl_DeviceRackModel.AddField("BarcodeExist");
            tbl_DeviceRackModel.AddField("BankModuleExist");
            tbl_DeviceRackModel.AddField("DisplayExists");
            tbl_DeviceRackModel.AddField("DisplayTypeId");
            tbl_DeviceRackModel.AddField("ComDisplay");
            tbl_DeviceRackModel.AddField("ComSlave");
            tbl_DeviceRackModel.AddField("ComReadOut");
            tbl_DeviceRackModel.AddField("ComReadIn");
            tbl_DeviceRackModel.AddField("ComDispens");
            tbl_DeviceRackModel.AddField("ComFeeder");
            tbl_DeviceRackModel.AddField("ComBarcode");
            tbl_DeviceRackModel.AddField("PortDiscret1");
            tbl_DeviceRackModel.AddField("PortDiscret2");
            tbl_DeviceRackModel.AddField("PortDiscret3");
            tbl_DeviceRackModel.AddField("PlateNumberUse");
            tbl_DeviceRackModel.AddField("TimeCardV");
            tbl_DeviceRackModel.AddField("TimeBarrier");
            tbl_DeviceRackModel.AddField("NumerSector");
            tbl_DeviceRackModel.AddField("TarifIdScheduleId");
            tbl_DeviceRackModel.AddField("TarifPlanId");
            tbl_DeviceRackModel.AddField("ClientGroupidToC");
            tbl_DeviceRackModel.AddField("TimeNotLoopA");
            tbl_DeviceRackModel.AddField("TimeNotIr");
            tbl_DeviceRackModel.AddField("LoopA");
            tbl_DeviceRackModel.AddField("LoopB");
            tbl_DeviceRackModel.AddField("IR");
            tbl_DeviceRackModel.AddField("PUSH");
            tbl_DeviceRackModel.AddField("Debug");
            tbl_DeviceRackModel.AddField("SlaveDebug");
            tbl_DeviceRackModel.AddField("ReaderOutDebug");
            tbl_DeviceRackModel.AddField("ReaderInDebug");
            tbl_DeviceRackModel.AddField("DispensDebug");
            tbl_DeviceRackModel.AddField("FeederDebug");
            tbl_DeviceRackModel.AddField("checkBoxItog");
            tbl_DeviceRackModel.AddField("logNastroy");
            tbl_DeviceRackModel.AddField("CardKey");
            tbl_DeviceRackModel.AddField("WebCameraIP");
            tbl_DeviceRackModel.AddField("WebCameraLogin");
            tbl_DeviceRackModel.AddField("WebCameraPassword");
            tbl_DeviceRackModel.AddField("ServerURL");
            tbl_DeviceRackModel.AddField("TransitTypeId");
            tbl_DeviceRackModel.AddField("RackWorkingModeId");
            tbl_DeviceRackModel.SetKey("DeviceId");
            tbl_DeviceRackModel.FieldUpdate = "Sync";

            var tbl_RackCustomerGroup = bs.RegisterTable("RackCustomerGroup", TypeSync.FromDevice, 0);
            tbl_RackCustomerGroup.AddField("Id");
            tbl_RackCustomerGroup.AddField("DeviceId");
            tbl_RackCustomerGroup.AddField("ClientGroupId");
            tbl_RackCustomerGroup.SetKey("Id");
            tbl_RackCustomerGroup.FieldUpdate = "Sync";

            var tbl_RackAltimetrModel = bs.RegisterTable("RackAltimetrModel", TypeSync.FromDevice, 0);
            tbl_RackAltimetrModel.AddField("Id");
            tbl_RackAltimetrModel.AddField("DeviceId");
            tbl_RackAltimetrModel.AddField("Input");
            tbl_RackAltimetrModel.AddField("RackAltimetrModeId");
            tbl_RackAltimetrModel.AddField("TariffId");
            tbl_RackAltimetrModel.AddField("Mode_Id");
            tbl_RackAltimetrModel.SetKey("Id");
            tbl_RackAltimetrModel.FieldUpdate = "Sync";

            var tbl_DeviceCMModel = bs.RegisterTable("DeviceCMModel", TypeSync.FromDevice, 0);
            tbl_DeviceCMModel.AddField("DeviceId");
            tbl_DeviceCMModel.AddField("ZoneId");
            tbl_DeviceCMModel.AddField("CashDispenserTypeId");
            tbl_DeviceCMModel.AddField("MaxCountUp");
            tbl_DeviceCMModel.AddField("MaxCountDown");
            tbl_DeviceCMModel.AddField("CountUp");
            tbl_DeviceCMModel.AddField("CountDown");
            tbl_DeviceCMModel.AddField("CashDispenserComPortId");
            tbl_DeviceCMModel.AddField("CashAcceptorLimit");
            tbl_DeviceCMModel.AddField("CashAcceptorAlarm");
            tbl_DeviceCMModel.AddField("CashAcceptorCurrent");
            tbl_DeviceCMModel.AddField("CashAcceptorComPortId");
            tbl_DeviceCMModel.AddField("HopperTypeId");
            tbl_DeviceCMModel.AddField("MaxCountRight");
            tbl_DeviceCMModel.AddField("MaxCountLeft");
            tbl_DeviceCMModel.AddField("CountRight");
            tbl_DeviceCMModel.AddField("CountLeft");
            tbl_DeviceCMModel.AddField("Hopper1ComPortId");
            tbl_DeviceCMModel.AddField("Hopper2ComPortId");
            tbl_DeviceCMModel.AddField("CoinAcceptorLimit");
            tbl_DeviceCMModel.AddField("CoinAcceptorAlarm");
            tbl_DeviceCMModel.AddField("CoinAcceptorCurrent");
            tbl_DeviceCMModel.AddField("BankModuleTypeId");
            tbl_DeviceCMModel.AddField("Reader");
            tbl_DeviceCMModel.AddField("PayPass");
            tbl_DeviceCMModel.AddField("BarcodeTypeId");
            tbl_DeviceCMModel.AddField("BarcodeComPortId");
            tbl_DeviceCMModel.AddField("VAT");
            tbl_DeviceCMModel.AddField("MaximalChange");
            tbl_DeviceCMModel.AddField("PrinterTypeId");
            tbl_DeviceCMModel.AddField("PrinterComPortId");
            tbl_DeviceCMModel.AddField("CardReaderComPortId");
            tbl_DeviceCMModel.AddField("CardTimeOutToBasket");
            tbl_DeviceCMModel.AddField("PenalCardTimeOut");
            tbl_DeviceCMModel.AddField("CancelRefund");
            tbl_DeviceCMModel.AddField("ShiftOpened");
            //tbl_DeviceCMModel.AddField("_IsSync2");
            //tbl_DeviceCMModel.AddField("_IsSync3");
            //tbl_DeviceCMModel.AddField("_Modified");
            tbl_DeviceCMModel.AddField("_IsDeleted");
            tbl_DeviceCMModel.AddField("CardKey");
            tbl_DeviceCMModel.AddField("CashierNumber");
            tbl_DeviceCMModel.AddField("CashDispenserExist");
            tbl_DeviceCMModel.AddField("NominalUpId");
            tbl_DeviceCMModel.AddField("NominalDownId");
            tbl_DeviceCMModel.AddField("RejectCount");
            tbl_DeviceCMModel.AddField("CashAcceptorExist");
            tbl_DeviceCMModel.AddField("HopperExist");
            tbl_DeviceCMModel.AddField("NominalRightId");
            tbl_DeviceCMModel.AddField("NominalLeftId");
            tbl_DeviceCMModel.AddField("CoinAcceptorExist");
            tbl_DeviceCMModel.AddField("CoinAcceptorTypeId");
            tbl_DeviceCMModel.AddField("BankModuleExist");
            tbl_DeviceCMModel.AddField("CardDispenserExists");
            tbl_DeviceCMModel.AddField("CardDispenserTypeId");
            tbl_DeviceCMModel.AddField("CardDispenserComPortId");
            tbl_DeviceCMModel.AddField("BarcodeExist");
            tbl_DeviceCMModel.AddField("KKMExists");
            tbl_DeviceCMModel.AddField("KKMTypeId");
            tbl_DeviceCMModel.AddField("ShiftAutoClose");
            tbl_DeviceCMModel.AddField("ShiftNewShiftTime");
            tbl_DeviceCMModel.AddField("CommandPassword");
            tbl_DeviceCMModel.AddField("RegistartionPass");
            tbl_DeviceCMModel.AddField("WithOutCleaningPass");
            tbl_DeviceCMModel.AddField("WithCleaningPass");
            tbl_DeviceCMModel.AddField("KKMComPortId");
            tbl_DeviceCMModel.AddField("PrinterExists");
            tbl_DeviceCMModel.AddField("CardReaderExist");
            tbl_DeviceCMModel.AddField("CardReaderTypeId");
            tbl_DeviceCMModel.AddField("SlaveExist");
            tbl_DeviceCMModel.AddField("SlaveTypeId");
            tbl_DeviceCMModel.AddField("SlaveComPortId");
            tbl_DeviceCMModel.AddField("PenaltyCardTPId");
            tbl_DeviceCMModel.AddField("PenaltyCardTSId");
            tbl_DeviceCMModel.AddField("SyncNominals");
            tbl_DeviceCMModel.AddField("ServerExchangeProtocolId");
            tbl_DeviceCMModel.AddField("ServerURL");
            tbl_DeviceCMModel.AddField("CurrentTransactionNumber");
            tbl_DeviceCMModel.AddField("LanguageTimeOut");
            tbl_DeviceCMModel.AddField("DefaultLanguageID");
            tbl_DeviceCMModel.AddField("WebCameraIP");
            tbl_DeviceCMModel.AddField("WebCameraLogin");
            tbl_DeviceCMModel.AddField("WebCameraPassword");
            tbl_DeviceCMModel.AddField("LastShiftOpenTime");
            tbl_DeviceCMModel.AddField("CashAcceptorTypeId");
            tbl_DeviceCMModel.AddField("CoinAcceptorComPortId");
            tbl_DeviceCMModel.SetKey("DeviceId");
            tbl_DeviceCMModel.FieldUpdate = "Sync";

            var tbl_CashAcceptorAllowModel = bs.RegisterTable("CashAcceptorAllowModel", TypeSync.FromDevice, 0);
            tbl_CashAcceptorAllowModel.AddField("DeviceId");
            tbl_CashAcceptorAllowModel.AddField("BanknoteId");
            tbl_CashAcceptorAllowModel.AddField("Allow");
            tbl_CashAcceptorAllowModel.AddField("Id");
            tbl_CashAcceptorAllowModel.SetKey("Id");
            tbl_CashAcceptorAllowModel.FieldUpdate = "Sync";

            var tbl_CoinAcceptorAllowModel = bs.RegisterTable("CoinAcceptorAllowModel", TypeSync.FromDevice, 0);
            tbl_CoinAcceptorAllowModel.AddField("DeviceId");
            tbl_CoinAcceptorAllowModel.AddField("CoinId");
            tbl_CoinAcceptorAllowModel.AddField("Allow");
            tbl_CoinAcceptorAllowModel.AddField("Id");
            tbl_CoinAcceptorAllowModel.SetKey("Id");
            tbl_CoinAcceptorAllowModel.FieldUpdate = "Sync";

            var tbl_CardsTransaction = bs.RegisterTable("CardsTransaction", TypeSync.FromDevice, 30);
            tbl_CardsTransaction.AddField("Id");
            tbl_CardsTransaction.AddField("LinkCardId");
            tbl_CardsTransaction.AddField("Blocked");
            //tbl_CardsTransaction.AddField("_IsSync2");
            //tbl_CardsTransaction.AddField("_IsSync3");
            //tbl_CardsTransaction.AddField("_Modified");
            tbl_CardsTransaction.AddField("_IsDeleted");
            tbl_CardsTransaction.AddField("CardId");
            tbl_CardsTransaction.AddField("ParkingEnterTime");
            tbl_CardsTransaction.AddField("LastRecountTime");
            tbl_CardsTransaction.AddField("LastPaymentTime");
            tbl_CardsTransaction.AddField("TSidFC");
            tbl_CardsTransaction.AddField("TPidFC");
            tbl_CardsTransaction.AddField("ZoneidFC");
            tbl_CardsTransaction.AddField("ClientGroupidFC");
            tbl_CardsTransaction.AddField("SumOnCard");
            tbl_CardsTransaction.AddField("Nulltime1");
            tbl_CardsTransaction.AddField("Nulltime2");
            tbl_CardsTransaction.AddField("Nulltime3");
            tbl_CardsTransaction.AddField("TVP");
            tbl_CardsTransaction.AddField("TKVP");
            tbl_CardsTransaction.AddField("ClientTypidFC");
            tbl_CardsTransaction.AddField("DateSaveCard");
            tbl_CardsTransaction.AddField("LastPlate");
            tbl_CardsTransaction.SetKey("Id");
            tbl_CardsTransaction.FieldUpdate = "Sync";

            var tbl_BCTransaction = bs.RegisterTable("BCTransaction", TypeSync.FromServer, 10);
            tbl_BCTransaction.AddField("Id");
            tbl_BCTransaction.AddField("BarCodeInUseId");
            tbl_BCTransaction.AddField("IdBCM");
            tbl_BCTransaction.AddField("OperId");
            tbl_BCTransaction.AddField("CompanyId");
            tbl_BCTransaction.AddField("IdTP");
            tbl_BCTransaction.AddField("IdTS");
            tbl_BCTransaction.AddField("DiscountAmount");
            tbl_BCTransaction.AddField("GroupIdFC");
            tbl_BCTransaction.AddField("UsageTime");
            tbl_BCTransaction.AddField("GuestPlateNumber");
            tbl_BCTransaction.AddField("EnterId");
            tbl_BCTransaction.AddField("ExitId");
            tbl_BCTransaction.AddField("IdTPForCount");
            tbl_BCTransaction.AddField("IdTSForCount");
            tbl_BCTransaction.AddField("CardId");
            tbl_BCTransaction.AddField("Ammount");
            tbl_BCTransaction.SetKey("Id");
            tbl_BCTransaction.FieldUpdate = "Sync";

            #endregion
        }

        private static bool flag = true;
        private static bool flag1 = false;
        public static bool Work
        {
            get
            {
                return flag;
            }
            set
            {
                flag = value;
            }
        }
        public static bool Running
        {
            get
            {
                return flag1;
            }
        }
     
        public static DbSchema ServerSchema;

        public static DbSchema DeviceSchema;

        public static void Sync(string connectserver, string connectdevice)
        {
            try
            {
                Log.Instance.SaveInfo(TypeModule.Sync,"Строка подключения к устройству "+connectdevice);

                Log.Instance.SaveInfo(TypeModule.Sync, "Строка подключения к серверу " + connectserver);

                using (IDbConnection connectserv = new SqlConnection(connectserver))
                {
                    using (IDbConnection connectdev = new SqlConnection(connectdevice))
                    {
                        ConfigSync.SetConnection(connectdev);
                        Log.Instance.SaveInfo(TypeModule.Sync, "Читаем информацию для синхронизации");
                        BaseSync bs = BaseSync.Create(connectserv, connectdev);
                        if (bs != null)
                        {
                            bs.SyncAllField(connectserv, connectdev);
                        }
                        connectdev.Close();
                    }
                    connectserv.Close();
                }
            }
            catch (Exception ex)
            {
                Log.Instance.SaveError(TypeModule.Sync, $"Ошибка синхронизации {ex.Message} {ex.StackTrace}");   
            }
        }
        
        public static Task Run(string connectserver, string connectdevice, int mswait)
        {
            /*
            Task ts = new Task(() =>
            {
                #region Расчет порядка обновления
                try
                {
                    using (IDbConnection connection = new SqlConnection(connectdevice))
                    {
                        Log.Instance.SaveInfo(TypeModule.CompareSchema, "Начинаем читать структуру БД и обновляем порядок обновления");
                        var start = DateTime.Now;
                        DeviceSchema = new DbSchema();
                        DeviceSchema.FromSql(connection);
                        DeviceSchema.DbUpdate();
                        Log.Instance.SaveInfo(TypeModule.CompareSchema, "Успешно причтали структуру БД и обновили порядок обновления");

                    }
                }
                catch (Exception ex)
                {
                    Log.Instance.SaveError(TypeModule.CompareSchema, $"Ошибка чтения схемы БД и порядка обновления {ex.Message} {ex.StackTrace}");
                }
                #endregion

                #region Обновление схемы БД
                try
                {
                    using (IDbConnection connection = new SqlConnection(connectdevice))
                    {
                        ConfigSync.SetConnection(connection);
                        var allow = connection.QuerySingleOrDefault<int>("SELECT Value FROM [Setting] where Name='AllowCompareScheme'");
                        if (allow == 1)
                        {
                            Log.Instance.SaveInfo(TypeModule.CompareSchema,"Start schema compare");
                            if (ConfigSync.Level == 1)
                            {
                                SchamaCompare.Compare(connectserver, connectdevice);    
                            }
                            Log.Instance.SaveInfo(TypeModule.CompareSchema, "Stop schema compare");
                        }
                        else
                        {
                            Log.Instance.SaveInfo(TypeModule.CompareSchema, "Not allow schema compare, add AllowCompareScheme=1 to Setting ");
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.Instance.SaveError(TypeModule.CompareSchema, $"Ошибка  {e.Message} {e.StackTrace}");
                }
                #endregion Обновление схемы БД

                #region Синхронизация данных
                while (flag)
                {
                    try
                    {
                        flag1 = true;
                        //Thread.Sleep(100);
                        Log.Instance.SaveInfo(TypeModule.Sync, "Начинаем синхронизацию");
                        Sync(connectserver, connectdevice);
                        flag1 = false;

                    }
                    catch (Exception ex)
                    {
                        Log.Instance.SaveInfo(TypeModule.Sync, $"Ошибка синхронизации {ex.Message} {ex.StackTrace}");
                    }
                    Thread.Sleep(mswait);
                }
                #endregion Синхронизация данных
            });
            ts.Start();
            return ts;
            */

            Task ts = Task.Factory.StartNew(() => 
            {
                CodeXBase.FactoryBaseService.ConnectStringDevice = connectdevice;//"Data Source=DESKTOP-9M4IQ5B;User Id=sa;Password=1;Initial Catalog=TestSynDevice";
                CodeXBase.FactoryBaseService.ConnectStringServer = connectserver;// "Data Source=DESKTOP-9M4IQ5B;User Id=sa;Password=1;Initial Catalog=TestSyncServer";
                Log.Instance.SaveInfo(TypeModule.Sync, $"Строка подключения к устройству {connectdevice}");
                Log.Instance.SaveInfo(TypeModule.Sync, $"Строка подключения к серверу {connectserver}");


                var tables = CodeXBase.Schema.ContextBase.CurrentBaseServer.Tables;
                var tbldev = CodeXBase.Schema.ContextBase.CurrentBaseDevice.Tables;
                bool flag = true;
                while (flag)
                {
                    try
                    {
                        ContextBase.FillInfoSyncFromDevice();
                        CodeXBase.SyncBase.RunSyncBase();
                    }
                    catch(Exception ex)
                    {
                        CodeXBase.Log.Instance.SaveError($"Ошибка синхронизации {ex.Message} {ex.StackTrace}");
                    }
                    Thread.Sleep(mswait);
                }
                
            });
            return ts;
        }

        public static void RunNotTask(string connectserver, string connectdevice, int mswait)
        {
            CodeXBase.FactoryBaseService.ConnectStringDevice = connectdevice;//"Data Source=DESKTOP-9M4IQ5B;User Id=sa;Password=1;Initial Catalog=TestSynDevice";
            CodeXBase.FactoryBaseService.ConnectStringServer = connectserver;// "Data Source=DESKTOP-9M4IQ5B;User Id=sa;Password=1;Initial Catalog=TestSyncServer";

            var tables = CodeXBase.Schema.ContextBase.CurrentBaseServer.Tables;
            var tbldev = CodeXBase.Schema.ContextBase.CurrentBaseDevice.Tables;
            bool flag = true;
            while (flag)
            {
                try
                {
                    ContextBase.FillInfoSyncFromDevice();
                    CodeXBase.SyncBase.RunSyncBase();
                }
                catch (Exception ex)
                {
                    CodeXBase.Log.Instance.SaveError($"Ошибка синхронизации {ex.Message} {ex.StackTrace}");
                }
                Thread.Sleep(mswait);
            }
        }
    }
}
