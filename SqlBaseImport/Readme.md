# Синхронизатор

Для получения логов необходимо внести настройки логирования

## Настройки логирования

```
<configuration>
...
<configSections>
    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog" />
</configSections>
...
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.nlog-project.org/schemas/NLog.xsd NLog.xsd"
      autoReload="true"
      throwExceptions="false"
      internalLogLevel="Off" internalLogFile="c:\temp\nlog-internal.log">
    <variable name="myvar" value="myvalue"/>
    <targets>
        <target xsi:type="File" name="debugFile" fileName="${basedir}/logs/SyncLog.txt"
                layout="${date:format=dd.MM.yyyy HH\:mm\:ss.fff} ${uppercase:${level}} ${logger} 
              ${callsite:filename=true}
              ${message} 
              ${exception:format=toString}"
                archiveNumbering="Date" enableArchiveFileCompression="false" maxArchiveFiles="24" archiveEvery="Hour"
                archiveFileName="${basedir}/logs/SyncLog_{#####}.txt" archiveDateFormat="dd.MM.yyy_HH" createDirs="true"
                keepFileOpen="false"/>

        <target xsi:type="File" name="traceFile" fileName="${basedir}/logs/SyncLogFull.txt"
                layout="${date:format=dd.MM.yyyy HH\:mm\:ss.fff} ${uppercase:${level}} ${logger} 
              ${callsite:filename=true} 
              ${message} 
              ${exception:format=toString}"
                archiveNumbering="Date" enableArchiveFileCompression="false" maxArchiveFiles="24" archiveEvery="Hour"
                archiveFileName="${basedir}/logs/SyncLogFull_{#####}.txt" archiveDateFormat="dd.MM.yyy_HH"
                createDirs="true" keepFileOpen="false"/>
        <target xsi:type="File" name="infoFile" fileName="${basedir}/logs/SyncLogInfo.txt"
                layout="${date:format=dd.MM.yyyy HH\:mm\:ss.fff} ${logger} ${message} ${exception:format=toString}"
                archiveNumbering="Date" enableArchiveFileCompression="false" maxArchiveFiles="24" archiveEvery="Hour"
                archiveFileName="${basedir}/logs/SyncLogInfo_{#####}.txt" archiveDateFormat="dd.MM.yyy_HH"
                createDirs="true" keepFileOpen="false"/>
    </targets>

    <rules>
       
        <logger name="*" level="Info" writeTo="infoFile"/>

        <logger name="*" level="Debug" writeTo="debugFile"/>

        <logger name="*" level="Trace" writeTo="traceFile"/>


    </rules>
</nlog>

```
  
## Настройки

Таблица - Setting

Включение ошибок при синхронизации 

AllowAlarmOnSync = 1

Включение сравнение и обновление схемы при синхронизации на 1м уровне 

AllowCompareScheme = 1
