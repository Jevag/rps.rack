﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using Dapper;
using NLog;


namespace SqlBaseImport.Model
{
    public class DbColumn
    {
        public string Name { get; set; }
        public string Default { get; set; }
        public string Id { get; set; }
        public string IsPrimaryKey { get; set; }
        public string AllowNull { get; set; }
        public string Length { get; set; }
        public string Type { get; set; }
        public string IsUnique { get; set; }
        public string ColumnReferencesTableId { get; set; }
        public string ColumnReferencesTableColumnId { get; set; }
    }

    public class DbTrigger
    {
        public string Name { get; set; }
        public string TableName { get; set; }
        public string Text { get; set; }
    }

    public class DbTable
    {
         //private static Logger log = LogManager.GetCurrentClassLogger();

        public int Order;
        public string Schema { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }

        public bool CanUpdate(List<string> tbls)
        {
            if (LinkedTables.Except(tbls).Any())
            {
                return false;
            }

            return true;
        }

        public List<string> LinkedTables = new List<string>();

        public List<DbColumn> Columns = new List<DbColumn>();

        public List<DbTrigger> Triggers = new List<DbTrigger>();

        public List<DbCompareErrors> Equals(DbTable table)
        {
            var isEqual = true;

            var errors = new List<DbCompareErrors>();

            try
            {
                if (table.Columns.Select(x => x.Name).Except(Columns.Select(x => x.Name)).Any() || Columns
                        .Select(x => x.Name).Except(table.Columns.Select(x => x.Name)).Any())
                {
                    foreach (var name in table.Columns.Select(x => x.Name).Except(Columns.Select(x => x.Name)))
                    {
                        errors.Add(new DbCompareErrors()
                        {
                            TableName = name,
                            Type = "Column",
                            Remote = name,
                            Local = null
                        });
                    }

                    foreach (var name in Columns
                        .Select(x => x.Name).Except(table.Columns.Select(x => x.Name)))
                    {
                        errors.Add(new DbCompareErrors()
                        {
                            TableName = name,
                            Type = "Column",
                            Remote = null,
                            Local = name
                        });
                    }
                }
                if (table.Triggers.Select(x => x.Name).Except(Triggers.Select(x => x.Name)).Any() || Triggers
                        .Select(x => x.Name).Except(table.Triggers.Select(x => x.Name)).Any())
                {
                    foreach (var name in table.Triggers.Select(x => x.Name).Except(Triggers.Select(x => x.Name)))
                    {
                        errors.Add(new DbCompareErrors()
                        {
                            TriggerName = name,
                            TableName = table.Triggers.Find(x => x.Name == name).TableName,
                            Type = "Trigger",
                            Remote = name,
                            Local = null
                        });
                    }

                    foreach (var name in Triggers.Select(x => x.Name).Except(table.Triggers.Select(x => x.Name)))
                    {
                        errors.Add(new DbCompareErrors()
                        {
                            TriggerName = name,
                            TableName = table.Triggers.Find(x => x.Name == name).TableName,
                            Type = "Trigger",
                            Remote = null,
                            Local = name
                        });
                    }
                }


                var noAttr = new List<string> {"Id", "ColumnReferencesTableId", "ColumnReferencesTableColumnId"};

                foreach (var tc in table.Columns.ToArray())
                {
                    var local = Columns.Find(y => y.Name == tc.Name);

                    if (local == null) continue;

                    foreach (var property in tc.GetType().GetProperties().ToList())
                    {
                        if (!noAttr.Contains(property.Name))
                        {
                            var remVal = (string) property.GetValue(tc);

                            var localVal = (string) property.GetValue(local);

                            if (remVal != localVal)
                            {
                                errors.Add(new DbCompareErrors()
                                {
                                    TableName = table.Name,
                                    ColumnName = tc.Name,
                                    AttributeName = property.Name,
                                    Type = "Attribute",
                                    Remote = remVal,
                                    Local = localVal
                                });
                            }
                        }


                        Log.Instance.SaveInfo(TypeModule.CompareSchema,"Change Property:"+property.Name);
                    }
                    Log.Instance.SaveInfo(TypeModule.CompareSchema, "Change Column:" + tc.Name);
                }

                noAttr = new List<string> {"TableName"};
                foreach (var tr in table.Triggers.ToArray())
                {
                    var local = Triggers.Find(y => y.Name == tr.Name);

                    if (local == null) continue;

                    foreach (var property in typeof(DbTrigger).GetProperties())
                    {
                        if (!noAttr.Contains(property.Name))
                        {
                            var remVal = (string) property.GetValue(tr);
                            var localVal = (string) property.GetValue(local);
                            if (remVal != localVal)
                            {
                                errors.Add(new DbCompareErrors()
                                {
                                    TableName = table.Name,
                                    ColumnName = tr.Name,
                                    AttributeName = property.Name,
                                    Type = "Trigger",
                                    Remote = remVal,
                                    Local = localVal
                                });
                            }
                        }

                        Log.Instance.SaveInfo(TypeModule.CompareSchema, "Change Property 1:" + property.Name);
                    }
                    Log.Instance.SaveInfo(TypeModule.CompareSchema, "Change Column 1:" + tr.Name);
                }
            }
            catch (Exception e)
            {
                return errors;
                Log.Instance.SaveError(TypeModule.CompareSchema, "Error: " + e.Message + e.StackTrace);
            }


            return errors;
        }
    }

    public class DbCompareErrors
    {
        public string ColumnName { get; set; }
        public string TriggerName { get; set; }
        public string TableName { get; set; }
        public string AttributeName { get; set; }

        // Table , Column, Attribute 
        public string Type { get; set; }


        public string Local { get; set; }
        public string Remote { get; set; }
    }

    public class DbSchema
    {
        public List<DbTable> Tables = new List<DbTable>();

        private IDbConnection dbConnection;

        public void FromSql(IDbConnection connect)
        {
            dbConnection = connect;
            var sql =
                @"SELECT (SELECT  s.name AS 'Schema',  t.name AS 'Name',  t.object_id AS 'Id',  
(  SELECT  so.name AS 'Name',  OBJECT_NAME(so.parent_object_id) AS 'TableName',  
OBJECT_DEFINITION(so.object_id) AS 'Text'  FROM sys.objects so  WHERE so.type = 'TR' 
and t.name = OBJECT_NAME(so.parent_object_id) FOR XML PATH ('Trigger'), TYPE  ),  (SELECT  c.name AS 'Name', 
(SELECT TOP 1 def.definition  FROM sys.default_constraints def  WHERE def.object_id = c.default_object_id) AS 'Default',  c.column_id AS 'Id', 
IIF(ic.object_id IS NOT NULL, 1,  0) AS 'IsPrimaryKey',  c.is_nullable AS 'AllowNull',  c.max_length AS 'Length',  
(SELECT TOP 1 t.name  FROM sys.types t  WHERE t.system_type_id = c.system_type_id) AS 'Type', 
OBJECT_NAME(fkc.referenced_object_id) AS 'ColumnReferencesTableId',  
(select cc.name from sys.columns cc where cc.column_id = fkc.referenced_column_id and fkc.parent_object_id = cc.object_id) AS 'ColumnReferencesTableColumnId',
(SELECT count(*)  FROM sys.index_columns AS icols INNER JOIN sys.indexes idx  ON idx.object_id = icols.object_id 
AND idx.index_id = icols.index_id  WHERE c.column_id = icols.column_id AND c.object_id = icols.object_id AND idx.is_unique = 1) AS 'IsUnique'  
FROM sys.columns AS c LEFT OUTER JOIN sys.index_columns AS ic  ON c.object_id = ic.object_id AND c.column_id = ic.column_id AND ic.index_id = 1 
LEFT OUTER JOIN sys.foreign_key_columns AS fkc  ON c.object_id = fkc.parent_object_id AND c.column_id = fkc.parent_column_id  
LEFT OUTER JOIN sys.indexes AS idx ON c.object_id = idx.object_id AND ic.index_id = idx.index_id  WHERE c.object_id = t.object_id 
FOR XML PATH ('Column'), TYPE)  FROM sys.schemas AS s INNER JOIN sys.tables AS t ON s.schema_id = t.schema_id  
FOR XML PATH ('Table'), ROOT ('Tables'), TYPE) AS data";


            var xml = connect.ExecuteScalar<string>(sql);
            Log.Instance.SaveInfo(TypeModule.CalculateOrder, "Читаем схему даннных про установки очередности");
            try
            {
                FromXml(xml);
                Log.Instance.SaveInfo(TypeModule.CalculateOrder, "Cхема даннных для установки очередности прочтена");

            }
            catch (Exception ex)
            {
                Log.Instance.SaveError(TypeModule.CalculateOrder, "Ошибка при чтении схемы даннных для установки очередности прочтена"+ex.Message+ex.StackTrace);

            }
        }

        public void FromXml(string xmltext)
        {
            var xml = new XmlDocument();

            xml.LoadXml(xmltext);

            var nodes = xml.SelectNodes("/Tables/Table");

            if (nodes == null)
            {
                return;
            }

            foreach (XmlNode child in nodes)
            {
                var table = new DbTable();
                table.Name = child.SelectSingleNode("Name")?.InnerText;
                table.Id = child.SelectSingleNode("Id")?.InnerText;

                var TriggerList = child.SelectNodes("Trigger");

                if (TriggerList != null)
                {
                    foreach (XmlNode triggerNode in TriggerList)
                    {
                        var tr = new DbTrigger();
                        var tnodes = triggerNode.ChildNodes;

                        foreach (XmlNode attr in triggerNode.ChildNodes)
                        {
                            tr.GetType().GetProperty(attr.Name)?.SetValue(tr, attr.InnerText);
                        }


                        table.Triggers.Add(tr);
                    }
                }

                var xmlNodeList = child.SelectNodes("Column");

                if (xmlNodeList != null)
                    foreach (XmlNode column in xmlNodeList)
                    {
                        var col = new DbColumn();

                        var cnodes = column.ChildNodes;


                        foreach (XmlNode attr in column.ChildNodes)
                        {
                            col.GetType().GetProperty(attr.Name)?.SetValue(col, attr.InnerText);
                        }

                        if (!string.IsNullOrEmpty(col.ColumnReferencesTableId))
                        {
                            table.LinkedTables.Add(col.ColumnReferencesTableId);
                        }


                        table.Columns.Add(col);
                    }

                Tables.Add(table);
            }


            CalculateOrder();
        }

        public void CalculateOrder()
        {
            /*
            foreach (DataRow row in tbl.Rows)
            {
                LinkTable tableLink = tables.Find(x => x.Name == Convert.ToString(row["TABLE_NAME"]));
                string colname = Convert.ToString(row["COLUMN_NAME"]);
                var find = (from t in tblfk.Rows.OfType<DataRow>() where tableLink.Name.Equals(t["FK_Table"]) && colname.Equals(t["FK_Column"]) select t).FirstOrDefault();
                if (find != null)
                {
                    tableLink.Linked.Add(Convert.ToString(find["PK_Table"]));
                    
                }
                else
                {
                   
                }
             } 
             */
            Log.Instance.SaveInfo(TypeModule.CalculateOrder, "Начинаем рассчитывать порядок обновления таблиц");
            List<DbTable> ordered = Tables.FindAll(x => x.LinkedTables.Count == 0).OrderBy(x => x.Name).ToList();
            List<DbTable> noorder = Tables.Except(ordered).ToList().OrderBy(x => x.LinkedTables.Count).ToList();
            while (noorder.Count != 0)
            {
                foreach (var linkTable in noorder)
                {
                    if (linkTable.CanUpdate(ordered.Select(x => x.Name).ToList()))
                    {
                        ordered.Add(linkTable);
                    }
                }

                noorder = Tables.Except(ordered).ToList().OrderBy(x => x.LinkedTables.Count).ToList();
            }
            var i = 0;
            foreach (var lt in ordered)
            {
                i++;
                lt.Order = i;
            }
            Log.Instance.SaveInfo(TypeModule.CalculateOrder, "Порядок обновления таблиц рассчитан");

        }

        public void DbUpdate()
        {
            Log.Instance.SaveInfo(TypeModule.CalculateOrder, "Обновялем порядок таблиц");
            foreach (var lt in Tables)
            {
                var comupdate = "update SyncTables set OrderSync=" + lt.Order + ",OrderSync2=" + lt.Order +
                                " where TableName='" + lt.Name + "'";

                dbConnection.Execute(comupdate, null, null, 120);
            }
            Log.Instance.SaveInfo(TypeModule.CalculateOrder, "Успешно обновили порядок таблиц");

        }

        public List<DbCompareErrors> Equals(IDbConnection connect)
        {
            var errors = new List<DbCompareErrors>();
            var remote = new DbSchema();
            remote.FromSql(connect);
            var isEqual = true;
            if (remote.Tables.Select(x => x.Name).Except(Tables.Select(x => x.Name)).Any())
            {
                foreach (var name in remote.Tables.Select(x => x.Name).Except(Tables.Select(x => x.Name)))
                {
                    errors.Add(new DbCompareErrors()
                    {
                        TableName = name,
                        Type = "Table",
                        Remote = name,
                        Local = null
                    });
                }
            }

            if (Tables.Select(x => x.Name).Except(remote.Tables.Select(x => x.Name)).Any())
            {
                foreach (var name in Tables.Select(x => x.Name).Except(remote.Tables.Select(x => x.Name)))
                {
                    errors.Add(new DbCompareErrors()
                    {
                        TableName = name,
                        Type = "Table",
                        Remote = null,
                        Local = name
                    });
                }
            }


            foreach (DbTable dbTable in remote.Tables)
            {
                var local = Tables.Find(x => x.Name == dbTable.Name);

                if (local == null) continue;

                var terrors = dbTable.Equals(local);

                errors.AddRange(terrors);
            }

            return errors;
        }
    }

    public partial class AlarmModel
    {
        public Guid Id { get; set; }

        public Guid DeviceId { get; set; }

        public DateTime Begin { get; set; }

        public Nullable<DateTime> End { get; set; }

        public string Value { get; set; }

        public string ValueEnd { get; set; }


        public Nullable<DateTime> Sync { get; set; }

        public Nullable<int> AlarmColorId { get; set; }

        public Nullable<int> TypeId { get; set; }
    }

    public class Table
    {
        //private static Logger log = LogManager.GetCurrentClassLogger();

        public TypeSync Sync { get; set; }
        public string NameTable { get; set; }
        public string FieldUpdate { get; set; }


        public int Order { get; set; }

        private bool isRemove = false;

        public bool IsRemove
        {
            get { return isRemove; }
            set { isRemove = value; }
        }

        public Table()
        {
            DeviceLevel = 1;
        }

        public int DeviceLevel { get; set; }

        public string GetNameTableSync()
        {
            string res = "SyncTables";
            //if (DeviceLevel > 1) { res = "SyncTables3"; }
            return res;
        }

        public string UslovieRemove { get; set; }

        private SortedList<string, Field> fields = new SortedList<string, Field>();

        public Table AddField(string namefield /*,SqlDbType type*/)
        {
            if (!fields.ContainsKey(namefield))
            {
                //fields.Add(namefield, new Field() {IsKey=false,NameField=namefield,TypeField=type });
                fields.Add(namefield, new Field() {IsKey = false, NameField = namefield});
            }
            return this;
        }

        public Table SetKey(string namefield)
        {
            var finds = from t in fields.Values where t.IsKey select t;
            if (finds != null)
            {
                foreach (var item in finds.ToArray())
                {
                    item.IsKey = false;
                }
            }
            if (fields.ContainsKey(namefield))
            {
                fields[namefield].IsKey = true;
            }
            else
            {
                fields.Add(namefield, new Field() {IsKey = true, NameField = namefield});
            }
            return this;
        }

        public object GetKeyValue(DataRow row)
        {
            object result = null;
            var find = (from t in fields.Values where t.IsKey select t).FirstOrDefault();
            if (find != null && row != null)
            {
                if (row.Table.Columns.Contains(find.NameField))
                {
                    object res = row[find.NameField];
                    if (res != null && res != DBNull.Value)
                    {
                        result = res;
                    }
                }
            }
            return result;
        }

        public object GetData(DataRow row, string namefield)
        {
            object result = null;
            if (row != null)
            {
                if (row.Table.Columns.Contains(namefield))
                {
                    object res = row[namefield];
                    if (res != null && res != DBNull.Value)
                    {
                        result = res;
                    }
                }
            }
            return result;
        }


        public void InsertFromRow(IDbConnection connect, DataRow row, SqlTransaction transact = null)
        {
            var start = DateTime.Now;

            StringBuilder strfl = new StringBuilder();
            StringBuilder strdata = new StringBuilder();
            var sqlParams = new DynamicParameters();
            bool isfirst = true;
            foreach (var field in fields.Values)
            {
                object dat = GetData(row, field.NameField);
                if (dat != null)
                {
                    if (isfirst)
                    {
                        strfl.Append("[" + field.NameField + "]");
                        strdata.Append("@" + field.NameField);
                        isfirst = false;
                    }
                    else
                    {
                        strfl.Append(",[" + field.NameField + "]");
                        strdata.Append(",@" + field.NameField);
                    }
                    sqlParams.Add(field.NameField, dat);
                    //par.SqlDbType = field.TypeField;
                }
            }

            
            string strins = "insert into [" + NameTable + "](" + strfl.ToString() + ") values (" + strdata.ToString() +")";

            
            Log.Instance.SaveInfo(TypeModule.Sync, "Начинаем добавляеть строку" + strins);
            var incount = connect.Execute(strins, sqlParams, null, 120);
            Log.Instance.SaveInfo(TypeModule.Sync, "Добавлена строка " + strins);
        }

        public void UpdateFromRow(IDbConnection connect, DataRow row, SqlTransaction transact = null)
        {
            var start = DateTime.Now;

            StringBuilder set = new StringBuilder();
            var sqlParams = new DynamicParameters();
            bool isfirst = true;
            set.Append("update [" + NameTable + "] set ");
            object key = GetKeyValue(row);
            if (key != null)
            {
                var findkey = (from t in fields.Values where t.IsKey select t).FirstOrDefault();

                foreach (var field in fields.Values.Where(x => x.NameField != findkey?.NameField))
                {
                    object dat = GetData(row, field.NameField);
                    if (dat != null)
                    {
                        if (isfirst)
                        {
                            set.Append("[" + field.NameField + "]=@" + field.NameField);
                            isfirst = false;
                        }
                        else
                        {
                            set.Append(",[" + field.NameField + "]=@" + field.NameField);
                        }
                        //SqlParameter par = new SqlParameter("@" + field.NameField, dat);
                        //par.SqlDbType = field.TypeField;
                        sqlParams.Add(field.NameField, dat);
                    }
                }


                if (findkey != null)
                {
                    string strcomm = set.ToString() + " where [" + findkey.NameField + "]=@Key_" + findkey.NameField;
                    SqlParameter par = new SqlParameter("@Key_" + findkey.NameField, key);
                    //par.SqlDbType = findkey.TypeField;
                    //comm.Parameters.Add(par.ParameterName);
                    sqlParams.Add("Key_" + findkey.NameField, key);
                    //comm.CommandText = strcomm;
                    //comm.Connection = (SqlConnection) connect;
                    Log.Instance.SaveInfo(TypeModule.Sync, "Начинаем обновлять строку " + strcomm);
                    var res = connect.Execute(strcomm, sqlParams, null, 120);
                    Log.Instance.SaveInfo(TypeModule.Sync, "Обновили строку " +  strcomm);
                }
                else
                {
                    Log.Instance.SaveError(TypeModule.Sync, $"Обновление строки поле ключ не найдено {NameTable}");
                }

            }
            else
            {
                Log.Instance.SaveError(TypeModule.Sync, $"Обновление строки поле ключ значение поля ключа равно null");
            }
        }


        public DataTable SelectTable(IDbConnection connect, string namefield, DateTime datafrom, DateTime datato,
            SqlTransaction transact = null)
        {
            DataTable table = new DataTable();

            var sqlParams = new DynamicParameters();

            StringBuilder set = new StringBuilder();
            bool isfirst = true;
            foreach (var field in fields.Values)
            {
                if (isfirst)
                {
                    set.Append("[" + field.NameField + "]");
                    isfirst = false;
                }
                else
                {
                    set.Append(",[" + field.NameField + "]");
                }
            }
            isfirst = true;
            StringBuilder strwhere = new StringBuilder();
            if (!string.IsNullOrEmpty(namefield)) //&& fields.ContainsKey(namefield))
            {
                //var fl = fields[namefield];
                if (datafrom != null)
                {
                    strwhere.Append("[" + namefield + "]>@from");
                    isfirst = false;
                    sqlParams.Add("from", datafrom);
                    //par.SqlDbType = fl.TypeField;
                }
                if (datato != null)
                {
                    if (isfirst)
                    {
                        strwhere.Append("[" + namefield + "]<=@to");
                        isfirst = false;
                    }
                    else
                    {
                        strwhere.Append(" and [" + namefield + "]<=@to");
                    }

                    sqlParams.Add("to", datato);
                }
            }
            StringBuilder strres = new StringBuilder();
            strres.Append("select ");
            strres.Append(set.ToString());
            strres.Append(" from [" + NameTable + "]");
            if (!isfirst)
            {
                strres.Append(" where ");
                strres.Append(strwhere.ToString());
            }
            Log.Instance.SaveInfo(TypeModule.Sync, "Читаем данные:" + strres.ToString());
            using (var reader = connect.ExecuteReader(strres.ToString(), sqlParams, null, 120))
            {
                table.Load(reader);
                reader.Close();
            }
            Log.Instance.SaveInfo(TypeModule.Sync, "Данные успешно прочтены :" + strres.ToString() +$" найдено {table.Rows.Count} строк");
            return table;
        }

        public bool HasRow(IDbConnection connect, DataRow row, SqlTransaction transact = null)
        {
            var start = DateTime.Now;

            bool result = true;

            string command = null;

            var findkey = (from t in fields.Values where t.IsKey select t).FirstOrDefault();
            Log.Instance.SaveInfo(TypeModule.Sync, "Проверяем наличие строки");
            if (findkey != null)
            {
                object key = GetData(row, findkey.NameField);
                if (key != null)
                {
                    Log.Instance.SaveInfo(TypeModule.Sync, $"Ключ:{key}");
                    result = false;
                    command = "select TOP 1 1 from [" + NameTable + "] where [" + findkey.NameField + "]=@key";
                    var count = connect.QuerySingleOrDefault<int>(command, new {key = key});
                    if (count > 0)
                    {
                        result = true;
                        Log.Instance.SaveInfo(TypeModule.Sync, $"Строка с  Ключом:{key} найдена");
                    }
                    else
                    {
                        Log.Instance.SaveInfo(TypeModule.Sync, $"Строка с  Ключом:{key} не найдена");
                    }
                }
                else
                {
                    Log.Instance.SaveError(TypeModule.Sync, $"Значение ключа в таблице {NameTable} равно null");
                }
            }
            else
            {
                Log.Instance.SaveError(TypeModule.Sync, $"Не найдено поле ключа {NameTable}");
            }
            return result;
        }

        /*
        public void SyncTable(SqlConnection from, SqlConnection to, DateTime datafrom, DateTime datato, string 
            SqlTransaction transact = null)
        {
            if (TestNeedSinc(from, Convert.ToDateTime(datafrom)))
            {

                if (!string.IsNullOrEmpty(FieldUpdate) && !string.IsNullOrWhiteSpace(FieldUpdate))
                {
                    //Из from берем все строки за указанный промежуток
                    var fromtable = SelectTable(from, FieldUpdate, datafrom, datato, transact);
                    //Проходим по каждой строку таблица и ищем ей соответсвие в соответсвующей таблице to
                    if (fromtable != null && fromtable.Rows.Count > 0)
                    {
                        foreach (DataRow row in fromtable.Rows)
                        {
                            try
                            {
                                if (HasRow(to, row))
                                {
                                    UpdateFromRow(to, row, transact);
                                }
                                else
                                {
                                    InsertFromRow(to, row, transact);
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Trace("Error: " + ex.Message);
                            }
                        }
                        fromtable.Dispose();
                    }
                }
            }
        }
        */

        public DataTable SelectTableAllField(IDbConnection connect, string namefield, DateTime datafrom,
            DateTime datato, SqlTransaction transact = null)
        {
            Log.Instance.SaveInfo(TypeModule.Sync, $"Читаем данные из таблицы {NameTable} за период с {datafrom.ToString("dd.MM.yyyy HH:mm:ss")} по {datato.ToString("dd.MM.yyyy HH:mm:ss")} по полю {namefield}");
            DataTable table = new DataTable();
            DynamicParameters sqlParams = new DynamicParameters();
            bool isfirst = true;
            StringBuilder strwhere = new StringBuilder();
            if (!string.IsNullOrEmpty(namefield)) //&& fields.ContainsKey(namefield))
            {
                //var fl = fields[namefield];
                if (datafrom != null)
                {
                    strwhere.Append("[" + namefield + "]>@from");
                    isfirst = false;
                    sqlParams.Add("from", datafrom);
                    //par.SqlDbType = fl.TypeField;
                    // comm.Parameters.Add(par);
                }
                if (datato != null)
                {
                    if (isfirst)
                    {
                        strwhere.Append("[" + namefield + "]<=@to");
                        isfirst = false;
                    }
                    else
                    {
                        strwhere.Append(" and [" + namefield + "]<=@to");
                    }
                    sqlParams.Add("to", datato);
                    //par.SqlDbType = fl.TypeField;
                    //comm.Parameters.Add(par);
                }
            }
            StringBuilder strres = new StringBuilder();
            strres.Append("select * ");
            strres.Append(" from [" + NameTable + "]");
            if (!isfirst)
            {
                strres.Append(" where ");
                strres.Append(strwhere.ToString() + $" order by {namefield} ASC");
            }
            Log.Instance.SaveInfo(TypeModule.Sync, $"Читаем данные: {strres.ToString()} ");

            using (var reader = connect.ExecuteReader(strres.ToString(), sqlParams, null, 120))
            {
                table.Load(reader);
                reader.Close();
            }
            Log.Instance.SaveInfo(TypeModule.Sync, $"Данные прочтены найдено {table.Rows.Count} строк");

            return table;
        }

        public bool SyncTableAllField(IDbConnection from, IDbConnection to, DateTime datafrom, DateTime dateto,
            SqlTransaction transact = null)
        {
            Log.Instance.SaveInfo(TypeModule.Sync,$"Начинаем синхронизацию по таблице {NameTable} за период с {datafrom.ToString("dd.MM.yyyy HH:mm:ss")} по {dateto.ToString("dd.MM.yyyy HH:mm:ss")}");
            //List<Guid> lstsync = new List<Guid>();
            List<object> lstsync = new List<object>();
            bool result = true;
            // if (TestNeedSinc(from, Convert.ToDateTime(datafrom)))
            //{
            if (!string.IsNullOrEmpty(FieldUpdate) && !string.IsNullOrWhiteSpace(FieldUpdate))
            {
                //Из from берем все строки за указанный промежуток
                var fromtable = SelectTableAllField(from, FieldUpdate, datafrom, dateto, transact);
                //Заполняем список полей
                
                var zapto = "select top 1 * from [" + NameTable + "]";
                DataTable tableto = new DataTable();
                using (var reader = to.ExecuteReader(zapto))
                {
                    tableto.Load(reader);
                    reader.Close();
                }
                Log.Instance.SaveInfo(TypeModule.Sync, $"Структура таблицы прочтена найдено {tableto.Columns.Count} столбцов");

                foreach (var fl in fields.Values.ToArray())
                {
                    if (!fl.IsKey)
                    {
                        fields.Remove(fl.NameField);
                        Log.Instance.SaveInfo(TypeModule.Sync, $"Удалили из спсика полей поле ключа {fl.NameField}");
                    }
                }

                foreach (DataColumn col in fromtable.Columns)
                {
                    string field = col.ColumnName;
                    if (!string.IsNullOrEmpty(field) && !string.IsNullOrWhiteSpace(field) && !field.Equals(FieldUpdate))
                    {
                        if (tableto.Columns.Contains(field))
                        {
                            AddField(field);
                        }
                    }
                }

                Log.Instance.SaveInfo(TypeModule.Sync, $"Проходим по каждой строке таблицы from {NameTable} и ищем ей соответсвие в соответсвующей таблице to");
                //Проходим по каждой строку таблица и ищем ей соответсвие в соответсвующей таблице to
                if (fromtable != null && fromtable.Rows.Count > 0)
                {
                    var findkey = (from t in fields.Values where t.IsKey select t).FirstOrDefault();

                    DateTime? startDateTime = null;
                    var count = 0;
                    foreach (DataRow row in fromtable.Rows)
                    {
                        result = true;
                        try
                        {
                            object data = GetData(row, findkey.NameField);
                            lstsync.Add((dynamic) data);

                            #region Log
                            if (data != null)
                            {
                                Log.Instance.SaveInfo(TypeModule.Sync,
                                    $"Начало Синронизации {NameTable}: значение {data}");
                            }
                            #endregion log

                            if (HasRow(to, row))
                            {
                                UpdateFromRow(to, row, transact);
                                var valueEnd = $"Ошибка синхронизации при Update в {NameTable},{data}";
                                alarmNotW(to, 400, 2, valueEnd, valueEnd);
                            }
                            else
                            {
                                InsertFromRow(to, row, transact);
                                var valueEnd = $"Ошибка синхронизации при Insert в {NameTable},{data}";
                                alarmNotW(to, 401, 2, valueEnd, valueEnd);
                            }
                            Log.Instance.SaveInfo(TypeModule.Sync,
                                $"Успешно прошла синронизация по строке в таблице {NameTable}: значение {data} ");
                        }
                        catch (Exception ex)
                        {
                            result = false;
                            object data = GetData(row, findkey.NameField);
                            var action = HasRow(to, row) ? "Update" : "Insert";
                            var valueEnd = $"Ошибка синхронизации при {action} в {NameTable},{data}";
                            alarmW(to, action == "Update" ? 400 : 401, 2, valueEnd, ex.Message);
                            Log.Instance.SaveError(TypeModule.Sync,$"Ошибка синхронизации при {action} в {NameTable},{data} :" + ex.Message+ex.StackTrace);
                        }

                       


                        if (IsRemove && result)
                        {
                            object data = GetData(row, findkey.NameField);
                            Log.Instance.SaveInfo(TypeModule.Sync, $"Начало удаление строки из {NameTable} значение {data}");
                            try
                            {
                                RemoveRows(from, new List<object>() {(dynamic) data});
                                Log.Instance.SaveInfo(TypeModule.Sync, $"Успешно прошло удаление строки из {NameTable} значение {data}");

                                var valueEnd = $"Ошибка синхронизации при Delete в {NameTable},{data}";
                                alarmNotW(from, 402, 2, valueEnd, valueEnd);
                            }
                            catch (Exception ex)
                            {
                                result = false;
                                var action = "Delete";
                                var valueEnd = $"Ошибка синхронизации при Delete в {NameTable},{data}";
                                alarmW(from, 402, 2, valueEnd, ex.Message);
                                Log.Instance.SaveError(TypeModule.Sync,
                                    $"Ошибка синхронизации при {action} в {NameTable},{data} :" + ex.Message+ex.StackTrace);
                            }
                        }
                        //if (result)
                        //{
                        //    SetLastUpdate(to, (DateTime) GetData(row,FieldUpdate));
                        //}
                        Log.Instance.SaveInfo(TypeModule.Sync, $"Завершена синхронизацию по таблице {NameTable} за период с {datafrom.ToString("dd.MM.yyyy HH:mm:ss")} по {dateto.ToString("dd.MM.yyyy HH:mm:ss")}");
                    }
                    fromtable.Dispose();
                }
            }
            else
            {
                Log.Instance.SaveError(TypeModule.Sync, "Не задано поле синхронизации");
            }
            //}
            return result;
        }

        private void alarmW(IDbConnection connect, int id, int? alarmColorId, string message,string messageEnd) // запись новой тревоги
        {
            try
            {
                if (ConfigSync.Level != 1 || !ConfigSync.AllowAlarmOnSync)
                {
                    return;
                }

                var count = ConfigSync.Connection.QuerySingle<int>(
                    "select  count(*) from AlarmModel where TypeId = " + id.ToString() +
                    " and [End] is NULL");


                if (count == 0)
                {
                    Guid deviceId = ConfigSync.Connection.QuerySingle<Guid>("select TOP 1 Id from DeviceModel");
                    var sql =
                        @"INSERT INTO AlarmModel (Id, DeviceId, _IsDeleted, [Begin], Value, ValueEnd, AlarmColorId, TypeId) VALUES 
                    (newid(), @DEV, 0, GETDATE() , @msg, @msg2, @color, @id);";

                    ConfigSync.Connection.Execute(sql,
                        new
                        {
                            DEV = deviceId,
                            msg = message.ToString(),
                            msg2 = messageEnd.ToString(),
                            color = alarmColorId,
                            id = id
                        }
                        , null, 120);
                }
            }
            catch (Exception e)
            {
                Log.Instance.SaveError(TypeModule.Sync,"Ошибка при установке аларма"+e.Message+e.StackTrace);
            }
        }

        private void alarmNotW(IDbConnection connect, int id, int? alarmColorId, string message,
                string messageEnd) // запись конца тревоги
        {
            if (ConfigSync.Level != 1)
            {
                return;
            }

            var alarmModels =
                ConfigSync.Connection.Query<AlarmModel>(
                    "select * from AlarmModel where TypeId = " + id.ToString() +
                    " and [End] is NULL")?.ToArray();

            try
            {
                if (alarmModels != null && alarmModels.Any())
                {
                    var query = alarmModels.Select(x => x.Id);

                    ConfigSync.Connection.Execute("update AlarmModel set [End] = GETDATE() where Id IN @Ids",
                        new {Ids = query}, null, 120);
                }
            }
            catch (Exception e)
            {
                Log.Instance.SaveError(TypeModule.Sync, "Ошибка при закрытии аларма" + e.Message + e.StackTrace);

            }
        }


        public void RemoveRows(IDbConnection connect, List<object> lst)
        {
            int current = 0;
            int maxcount = 500;
            int index = 0;
            while (current < lst.Count)
            {
                var items = (from t in lst select t).Skip(index * maxcount).Take(maxcount).ToArray();
                if (items != null && items.Length > 0)
                {
                    var find = (from t in fields.Values where t.IsKey select t).FirstOrDefault();
                    var rem = "";
                    if (!string.IsNullOrEmpty(UslovieRemove) && !string.IsNullOrWhiteSpace(UslovieRemove))
                    {
                        rem = " and " + UslovieRemove;
                    }
                    var dcount =
                        connect.Execute(
                            "delete from " + NameTable + " where " + find.NameField + " in @Ids " + rem,
                            new {Ids = lst}, null, 120);
                }
                index++;
                current = maxcount * index;
            }
        }

        public bool TestNeedSinc(IDbConnection connect, DateTime olddate)
        {
            bool result = true;
            string strcomm = "select * from " + GetNameTableSync() + " where TableName=@ntable";
            DynamicParameters sqlParams = new DynamicParameters();

            sqlParams.Add("ntable", NameTable);
            //comm.Parameters.Add(new SqlParameter("@date", olddate));
            DataTable table = new DataTable();
            using (var reader = connect.ExecuteReader(strcomm.ToString(), sqlParams))
            {
                table.Load(reader);
            }

            if (table.Rows != null && table.Rows.Count > 0)
            {
                object objdate = table.Rows[0]["DateTime"];
                if (objdate != null && objdate != DBNull.Value)
                {
                    DateTime lastupdate = Convert.ToDateTime(objdate);
                    if (lastupdate >= olddate)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            else
            {
                result = false;
            }
            table.Dispose();

            return result;
        }


        public void SetLastUpdate(IDbConnection connect, DateTime date)
        {
            string strhas = "select top 1 * from " + GetNameTableSync() + " where TableName=@tblname";
            DynamicParameters sqlParams = new DynamicParameters();
            sqlParams.Add("tblname", NameTable);
            var res = connect.QuerySingle(strhas, sqlParams);
            if (res != null && res != DBNull.Value)
            {
                string upd = "update " + GetNameTableSync() + " set LastUpdate=@date where TableName=@tblname";
                sqlParams = new DynamicParameters();
                sqlParams.Add("date", date);
                sqlParams.Add("tblname", NameTable);
                connect.Execute(upd, sqlParams, null, 120);
            }
            else
            {
                string ins = "insert into " + GetNameTableSync() + " (LastUpdate,TableName) values (@date,@tblname)";

                sqlParams.Add("date", date);
                sqlParams.Add("tblname", NameTable);

                connect.Execute(ins, sqlParams, null, 120);
            }
        }

        public DateTime GetLastUpdate(IDbConnection connect)
        {
            DateTime date = new DateTime(2000, 1, 1);

            var LastUpdate = connect.QuerySingleOrDefault<DateTime?>("select top 1 LastUpdate from " + GetNameTableSync() + " where TableName=@tblname",
                new {tblname = NameTable});


            if (LastUpdate.HasValue)
            {
                try
                {
                    date = LastUpdate.Value;
                }
                catch
                {
                }
            }

            return date;
        }

        public DateTime GetLastChange(IDbConnection connect)
        {
            DateTime date = new DateTime(2000, 1, 1);
            var LastChange = connect.QuerySingleOrDefault<DateTime?>(
                "select top 1 DateTime from " + GetNameTableSync() + " where TableName=@tblname",
                new {tblname = NameTable});
            if (LastChange.HasValue)
            {
                try
                {
                    date = LastChange.Value;
                }
                catch
                {
                }
            }
            return date;
        }

        public void ClearTable(SqlConnection connect)
        {
            string command = "delete from [" + NameTable + "]";
            string comupdate = "update [" + GetNameTableSync() + "] set LastUpdate=null where TableName='" +NameTable + "'";

            if (connect.State == ConnectionState.Closed)
            {
                connect.Open();
            }
            using (SqlCommand comm = new SqlCommand(command, connect))
            {
                comm.ExecuteNonQuery();
            }
            using (SqlCommand commnull = new SqlCommand(comupdate, connect))
            {
                commnull.ExecuteNonQuery();
            }
        }
    }

    public class BaseSync
    {
        //private static Logger log = LogManager.GetCurrentClassLogger();

        public static int DeviceLevel { get; set; }

        public static string GetNameTableSync()
        {
            string res = "SyncTables";
            // if (DeviceLevel > 1) { res = "SyncTables3"; }
            return res;
        }

        private SortedList<string, Table> tables = new SortedList<string, Table>();

        public Table GetTable(string NameTable)
        {
            Table result = null;
            if (tables.ContainsKey(NameTable))
            {
                result = tables[NameTable];
            }
            else
            {
                result = new Table();
                result.NameTable = NameTable;
                tables.Add(NameTable, result);
            }
            return result;
        }

        public Table RegisterTable(string NameTable, TypeSync sync, int order)
        {
            Table result = null;
            if (!tables.ContainsKey(NameTable))
            {
                result = new Table();
                result.Order = order;
                result.Sync = sync;
                result.NameTable = NameTable;
                tables.Add(NameTable, result);
            }
            else
            {
                result = tables[NameTable];
            }
            return result;
        }

        public void SyncAllField(IDbConnection connectserver, IDbConnection connectdevice)
        {
            try
            {
                var fromservs = from t in tables.Values where t.Sync == TypeSync.FromServer orderby t.Order select t;
                foreach (var tbl in fromservs)
                {
                    try
                    {
                        Log.Instance.SaveInfo(TypeModule.Sync,$"Проверяем нужна ли синхронизации по таблице {tbl.NameTable} с сервера" );
                        DateTime datefrom = tbl.GetLastUpdate(connectdevice);
                        DateTime dateto = tbl.GetLastChange(connectserver);
                        //var times = connectserver.QuerySingle("select MIN(Sync) as datefrom, MAX(Sync) as dateto from [" + tbl.NameTable+"]");
                        // var datefrom = times.datefrom;
                        //var dateto = times.dateto;

                        if (dateto > datefrom)
                        {
                            var res = tbl.SyncTableAllField(connectserver, connectdevice, datefrom, dateto);
                            if (res)
                            {
                                tbl.SetLastUpdate(connectdevice, dateto);
                                Log.Instance.SaveInfo(TypeModule.Sync, $"Установлено время последнего обновленияв по таблице {tbl.NameTable} время {dateto.ToString("dd.MM.yyyy HH:mm:ss")}");

                            }
                        }
                        else
                        {
                            Log.Instance.SaveInfo(TypeModule.Sync, $"Синхронизации по таблице {tbl.NameTable} с сервера не нужна с {datefrom.ToString("dd.MM.yyyy HH:mm:ss")} по {dateto.ToString("dd.MM.yyyy HH:mm:ss")}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.SaveError(TypeModule.Sync, $"Ошибка {tbl.NameTable} {ex.Message} {ex.StackTrace}");
                    }
                    Log.Instance.SaveInfo(TypeModule.Sync, $"Завершена синхронизации по таблице {tbl.NameTable} с сервера");
                }
                Log.Instance.SaveInfo(TypeModule.Sync, $"Завершена синхронизации всех таблиц с сервера");

                var fromdevice = from t in tables.Values where t.Sync == TypeSync.FromDevice orderby t.Order select t;
                foreach (var tbl in fromdevice)
                {
                    try
                    {
                        Log.Instance.SaveInfo(TypeModule.Sync, $"Проверяем нужна ли синхронизации по таблице {tbl.NameTable} с устройства");
                        DateTime datefrom = tbl.GetLastUpdate(connectdevice);
                        DateTime dateto = tbl.GetLastChange(connectdevice);
                        if (dateto > datefrom)
                        {
                            var res = tbl.SyncTableAllField(connectdevice, connectserver, datefrom, dateto);
                            if (res)
                            {
                                tbl.SetLastUpdate(connectdevice, dateto);
                                Log.Instance.SaveInfo(TypeModule.Sync, $"Установлено время последнего обновленияв по таблице {tbl.NameTable} время {dateto.ToString("dd.MM.yyyy HH:mm:ss")}");
                            }
                        }
                        else
                        {
                            Log.Instance.SaveInfo(TypeModule.Sync, $"Синхронизации по таблице {tbl.NameTable} с устройства не нужна с {datefrom.ToString("dd.MM.yyyy HH:mm:ss")} по {dateto.ToString("dd.MM.yyyy HH:mm:ss")}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.SaveError(TypeModule.Sync,$"Ошибка {tbl.NameTable} с устройства {ex.Message} {ex.StackTrace}");
                    }
                    Log.Instance.SaveInfo(TypeModule.Sync, $"Завершена синхронизации по таблице {tbl.NameTable} с устройства");
                }
                Log.Instance.SaveInfo(TypeModule.Sync, $"Завершена синхронизации всех таблиц с устройства");
            }
            catch (Exception ex)
            {
                Log.Instance.SaveError(TypeModule.Sync,$"Ошибка: {ex.Message} {ex.StackTrace}");
            }
        }

        public static BaseSync Create(IDbConnection connectserv, IDbConnection connectdevice)
        {
            return CreateSyncTables(connectdevice);
        }

        private static BaseSync CreateSyncTables(IDbConnection connectdevice)
        {
            DeviceLevel = -1;

            if (ConfigSync.Level > 0)
            {
                DeviceLevel = ConfigSync.Level;
            }
            BaseSync bs = new BaseSync();
            if (DeviceLevel > 0)
            {
                //bs.log.Trace("00");          // start control
                //string command = "select * from SyncTables where TableName='TestSync'";
                string command = "select * from " + GetNameTableSync();
                DataTable table = new DataTable();
                using (var reader = connectdevice.ExecuteReader(command))
                {
                    table.Load(reader);

                    if (table != null && table.Rows != null && table.Rows.Count > 0)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            string keyfield = GetStringValue(row["KeyField"]);
                            string syncfield = GetStringValue(row["SyncField"]);
                            int? order = GetIntValue(row["OrderSync"]);
                            if (DeviceLevel >= 2)
                            {
                                order = GetIntValue(row["OrderSync2"]);
                            }
                            int? syncfrom = GetIntValue(row["SyncFrom"]);
                            if (DeviceLevel >= 2)
                            {
                                syncfrom = GetIntValue(row["SyncFrom2"]);
                            }
                            string uslovremove = GetStringValue(row["UslovieRemove"]);
                            bool? isremove = GetBoolValue(row["IsRemove"]);

                            if (!string.IsNullOrEmpty(keyfield) && !string.IsNullOrWhiteSpace(keyfield)
                                && !string.IsNullOrEmpty(syncfield) && !string.IsNullOrWhiteSpace(syncfield)
                                && order != null && order.HasValue && order > 0 && syncfrom != null &&
                                syncfrom.HasValue)
                            {
                                TypeSync ts = TypeSync.FromDevice;
                                if (syncfrom.Value == 2)
                                {
                                    ts = TypeSync.FromServer;
                                }

                                Table tbl = bs.RegisterTable(GetStringValue(row["TableName"]), ts, order.Value);
                                tbl.SetKey(keyfield);
                                tbl.FieldUpdate = syncfield;
                                tbl.DeviceLevel = DeviceLevel;
                                if (DeviceLevel == 1)
                                {
                                    if (isremove != null && isremove.HasValue)
                                    {
                                        tbl.IsRemove = isremove.Value;
                                    }
                                    else
                                    {
                                        tbl.IsRemove = false;
                                    }
                                    if (!string.IsNullOrEmpty(uslovremove) && !string.IsNullOrWhiteSpace(uslovremove))
                                    {
                                        tbl.UslovieRemove = uslovremove;
                                    }
                                    else
                                    {
                                        tbl.UslovieRemove = string.Empty;
                                    }
                                }
                                else
                                {
                                    tbl.IsRemove = false;
                                    tbl.UslovieRemove = string.Empty;
                                }
                            }
                        }
                    }
                    reader.Close();
                }
            }
            return bs;
        }

        private static string GetStringValue(object data)
        {
            string result = string.Empty;
            if (data != null && data != DBNull.Value)
            {
                result = Convert.ToString(data);
            }
            return result;
        }

        private static int? GetIntValue(object data)
        {
            int? result = null;
            if (data != null && data != DBNull.Value)
            {
                try
                {
                    result = Convert.ToInt32(data);
                }
                catch
                {
                }
            }
            return result;
        }

        private static bool? GetBoolValue(object data)
        {
            bool? result = null;
            if (data != null && data != DBNull.Value)
            {
                try
                {
                    result = Convert.ToBoolean(data);
                }
                catch
                {
                }
            }
            return result;
        }

        public static void ClearDevice(SqlConnection connectdevice)
        {
            BaseSync bs = CreateSyncTables(connectdevice);

            var fromservs = from t in bs.tables.Values
                where t.Sync == TypeSync.FromServer
                orderby t.Order descending
                select t;
            foreach (var tbl in fromservs)
            {
                try
                {
                    Log.Instance.SaveInfo(TypeModule.Sync,$"Начало очистки таблицы {tbl.NameTable}");
                    tbl.ClearTable(connectdevice);
                    Log.Instance.SaveInfo(TypeModule.Sync, $"Таблицы {tbl.NameTable} очишена успешно");

                }
                catch (Exception ex)
                {
                    Log.Instance.SaveError(TypeModule.Sync, $"Ошибка при очистки таблицы {tbl.NameTable} {ex.Message} {ex.StackTrace}");
                }
            }
        }
    }

    public class Field
    {
        public string NameField { get; set; }
        private bool isKey = false;

        public bool IsKey
        {
            get { return isKey; }
            set { isKey = value; }
        }

        //public SqlDbType TypeField { get; set; }
    }

    public enum TypeSync
    {
        FromServer = 1,
        FromDevice = 2
    }
}