﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg
{
    public class InfoDownload
    {
        public int Level = 2;
        public int NewVersion = 0;
        public string ftp3 = "ftp://mc.r-p-s.ru/";
        public string userftp3 = "updater";
        public string pwdftp3 = "parol-RPS1";

        public string localfolderdest = string.Empty;
        public string netfoldersource = string.Empty;
        public string basename = string.Empty;

        public string ftp2 = string.Empty;
        public string userftp2 = string.Empty;
        public string pwdftp2 = string.Empty;

        public bool CopyLocalFtp = true;

        public InfoDownload(int Level)
        {
            this.Level = Level;
        }

        /// <summary>
        /// Возвращает название папки где хранится 
        /// новая версия программы
        /// </summary>
        /// <returns></returns>
        public string NewVersionFolder()
        {
            string filename = string.Format(basename, NewVersion);
            string folder = System.IO.Path.GetFileNameWithoutExtension(filename);
            string destfolder = System.IO.Path.Combine(localfolderdest, folder);
            return destfolder;
        }

        /// <summary>
        /// Проверяет есь ли папка с новой версией на сервере 2 уровня
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public bool HasNetSourFolder(int version)
        {
            bool result = false;
            /*
            string filename = string.Format(basename, version);
            string sourfile = System.IO.Path.Combine(netfoldersource, filename);
            if (System.IO.File.Exists(sourfile))
            {
                result = true;
            }
            */
            string filename = string.Format(basename, version);
            FtpClient ftp = new FtpClient(ftp2, userftp2, pwdftp2);
            var files=ftp.ListDirectory();
            var find = (from t in files where t.Trim().ToLower().Equals(filename.Trim().ToLower()) select t).FirstOrDefault();
            if(!string.IsNullOrEmpty(find) && !string.IsNullOrWhiteSpace(find))
            {
                result = true;
            }

            return result;
        }
        /// <summary>
        /// Если ли папка с новой версией локально
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public bool HasLocalFile(int version)
        {
            bool result = false;
            string filename = string.Format(basename, version);
            string destfile = System.IO.Path.Combine(localfolderdest, filename);
            if (System.IO.File.Exists(destfile))
            {
                result = true;
            }
            return result;
        }
        /// <summary>
        /// Есть папка с новой версией локально и она рапскована
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public bool HasLocalFolder(int version)
        {
            bool result = false;
            string filename = string.Format(basename, version);
            string folder = System.IO.Path.GetFileNameWithoutExtension(filename);
            string destfolder = System.IO.Path.Combine(localfolderdest, folder);
            if (System.IO.Directory.Exists(destfolder))
            {
                result = true;
            }
            return result;
        }
        /// <summary>
        /// Локально копируем файл архива  и уго распаковываем
        /// </summary>
        /// <param name="version"></param>
        public void LocalCopyAndUnZip(int version)
        {
            string filename = string.Format(basename, version);
            string destfile = System.IO.Path.Combine(localfolderdest, filename);
            if (CopyLocalFtp)
            {
                string sourcefile = System.IO.Path.Combine(ftp2, filename);
                FtpClient ftpcl = new FtpClient(ftp2, userftp2, pwdftp2);
                ftpcl.DownloadFile(sourcefile, destfile);

            }
            else
            {
                string sourfile = System.IO.Path.Combine(netfoldersource, filename);
                System.IO.File.Copy(sourfile, destfile, true);
            }
            UnZipLocalFile(destfile);
        }

        /// <summary>
        /// Копирует файл с 2 уровня на 1
        /// </summary>
        /// <param name="version"></param>
        public void CopyToLocal(int version)
        {
            string filename = string.Format(basename, version);
            string destfile = System.IO.Path.Combine(localfolderdest, filename);
            if (CopyLocalFtp)
            {
                string sourcefile = System.IO.Path.Combine(ftp2, filename);
                FtpClient ftpcl = new FtpClient(ftp2, userftp2, pwdftp2);
                ftpcl.DownloadFile(filename, destfile);
            }
            else
            {
                string sourfile = System.IO.Path.Combine(netfoldersource, filename);
                System.IO.File.Copy(sourfile, destfile, true);
            }
        }
        
        /// <summary>
        /// Распаковываем архив из заданного файла
        /// </summary>
        /// <param name="file"></param>
        public void UnZipLocalFile(string file)
        {
            using (ZipFile zip = ZipFile.Read(file))
            {
                zip.ExtractAll(localfolderdest, ExtractExistingFileAction.OverwriteSilently);
            }
        }

        /// <summary>
        /// Распаковываем архив для заданной версии
        /// </summary>
        /// <param name="version"></param>
        public void UnZipLocalFile(int version)
        {
            string filename = string.Format(basename, version);
            string destfile = System.IO.Path.Combine(localfolderdest, filename);
            UnZipLocalFile(destfile);
        }
        /// <summary>
        /// Копируем с ftp на локальную машину
        /// </summary>
        /// <param name="version"></param>
        public void FtpDownload(int version)
        {
            string filename = string.Format(basename, version);
            string destfile = System.IO.Path.Combine(localfolderdest, filename);
            FtpClient ftpcl = new FtpClient(ftp3, userftp3, pwdftp3);
            ftpcl.DownloadFile(filename, destfile);
        }

     

        public static InfoDownload CreateStoika(int version, int Level)
        {
            InfoDownload info = new InfoDownload(Level);
            info.NewVersion = version;
            info.basename = "RackV{0}.zip";
            StandartFillInfo(info);
            return info;
        }
        public static InfoDownload CreateCash(int version, int Level)
        {
            InfoDownload info = new InfoDownload(Level);
            info.NewVersion = version;
            //info.basename = "CashierTestV{0}.zip";
            info.basename = "CashierV{0}.zip";
            StandartFillInfo(info);
            return info;
        }
        public static InfoDownload CreateWeb(int version, int Level)
        {
            InfoDownload info = new InfoDownload(Level);
            info.NewVersion = version;
            info.basename = "WebV{0}.zip";
            StandartFillInfo(info);
            info.localfolderdest = @"C:\RPS";
            return info;
        }
        public static InfoDownload CreateServiceTool(int version, int Level)
        {
            InfoDownload info = new InfoDownload(Level);
            info.NewVersion = version;
            info.basename = "ServiceToolV{0}.zip";
            StandartFillInfo(info);
            info.localfolderdest = @"C:\RPS";

            return info;
        }

        public static InfoDownload CreateDb(int Level)
        {
            InfoDownload info = new InfoDownload(Level);
            info.NewVersion = 0;
            info.basename = "UpdateDB.zip";
            StandartFillInfo(info);
            return info;
        }

        public static InfoDownload CreateUpdate(int Level)
        {
            InfoDownload info = new InfoDownload(Level);
            info.NewVersion = 0;
            info.basename = "Updater.zip";
            StandartFillInfo(info);
            return info;
        }

        private static void StandartFillInfo(InfoDownload info)
        {
            if (info.Level == 2)
            {
                info.localfolderdest = @"C:\Update\RPS";
            }
            if (info.Level == 1)
            {
                info.localfolderdest = @"C:\RPS";
            }
            info.netfoldersource = @"Z:\RPS";
            //info.netfoldersource = @"C:\Update\RPS";
            info.ftp3 = "ftp://mc.r-p-s.ru/";
            info.userftp3 = "updater";
            info.pwdftp3 = "parol-RPS1";
        }
    }
}
