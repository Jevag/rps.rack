﻿using Ionic.Zip;

using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UpdateProg
{
    public class UpdateServiceTool2
    {
        #region Fields
        public string PathUpdater = string.Empty;
        private InfoDownload ServiceTool = null;
        private InfoDownload Web = null;
        private InfoDownload Kassa = null;
        private InfoDownload Stoika = null;
        private InfoDownload Db = null;
        private InfoDownload infoupd = null;

        private Guid? idupdateprogramm = null;
        private Guid? idupdatedb = null;
        private SqlConnection connect;
        private int Level = 2;
        private int waitsec = 60;
        private int waitrestartdevice = 300;
        #endregion Fields

        public UpdateServiceTool2(SqlConnection connect)
        {
            this.connect = connect;
            infoupd = InfoDownload.CreateUpdate(2);
            this.PathUpdater = System.IO.Path.Combine(infoupd.NewVersionFolder(), "Updater.exe");

        }

        /// <summary>
        /// Проверяет есть ли в базе строка с IsUpdate=true и больше текущей версии
        /// </summary>
        /// <param name="connect">Подключение в БД</param>
        /// <returns>Строка с информацией по обновлению или null</returns>
        protected DataRow TestNeedUpdate(int status=2)
        {
            string command = "select top 1 * from SystemVersion " +
                            " where Status = " + status.ToString();
            DataRow result = UtilsBase.FillRow(command, connect, null);
            return result;
        }

        /// <summary>
        /// Расчитывает какие именно программы обновляем
        /// </summary>
        /// <param name="toupdate">Строка с информацией до какой версии обновляем</param>
        /// <param name="connect">Строка с </param>
        /// <returns>Информация каке именно программы скачиваем и какие вресии скачиваем</returns>
        protected InfoVersionDownload CalculateUpdate(DataRow toupdate)
        {
            InfoVersionDownload info = null;
            //Берем текущую версию
            #region Берем информацию по текущей версии

            string command = "select top 1 * from SystemVersion " +
                            " where Status = "+StatSystemVersion.Текущая.ToString();
            DataRow currentver = UtilsBase.FillRow(command, connect, null);

            #endregion Берем информацию по текущей версии
            //Расчитываем что нужно обновить
            if (toupdate != null && currentver != null)
            {
                info = new InfoVersionDownload();
                info.SystemVersion = Convert.ToInt32(toupdate["SysVersion"]);
                info.Kassa = TestVersion(currentver, toupdate, "CMVer");
                if (info.Kassa != null && info.Kassa.HasValue)
                {
                    if (this.Kassa == null)
                    {
                        this.Kassa = InfoDownload.CreateCash(info.Kassa.Value, Level);
                    }
                    else
                    {
                        if (this.Kassa.NewVersion != info.Kassa.Value) { this.Kassa = InfoDownload.CreateCash(info.Kassa.Value, Level); }
                    }
                }
                info.Stoika = TestVersion(currentver, toupdate, "RackVer");
                if (info.Stoika != null && info.Stoika.HasValue)
                {
                    if (this.Stoika == null)
                    {
                        this.Stoika = InfoDownload.CreateStoika(info.Stoika.Value, Level);
                    }
                    else
                    {
                        if (this.Stoika.NewVersion != info.Stoika.Value) { this.Stoika = InfoDownload.CreateStoika(info.Stoika.Value, Level); }
                    }
                }
                info.Web = TestVersion(currentver, toupdate, "WebVer");
                if (info.Web != null && info.Web.HasValue)
                {
                    if (this.Web == null)
                    {
                        this.Web = InfoDownload.CreateWeb(info.Web.Value, Level);
                    }
                    else
                    {
                        if (this.Web.NewVersion != info.Web.Value) { this.Web = InfoDownload.CreateWeb(info.Web.Value, Level); }
                    }
                }
                info.ServiceTool = TestVersion(currentver, toupdate, "STVer");
                if (info.ServiceTool != null && info.ServiceTool.HasValue)
                {
                    if (this.ServiceTool == null)
                    {
                        this.ServiceTool = InfoDownload.CreateServiceTool(info.ServiceTool.Value, Level);
                    }
                    else
                    {
                        if (this.ServiceTool.NewVersion != info.ServiceTool.Value) { this.ServiceTool = InfoDownload.CreateServiceTool(info.ServiceTool.Value, Level); }
                    }
                }

                var curdb = Rps.ShareBase.UpdateDbEngine.GetCurrentDbVersion(this.connect);
                if (curdb != null)
                {
                    int curver = curdb.Packet;
                    int newver = ConvertField.GetInt(toupdate, "DbVer").Value;

                    if (newver > curver)
                    {
                        info.Db = newver;
                        if (this.Db == null)
                        {
                            this.Db = InfoDownload.CreateDb(Level);
                        }
                    }
                }


                #region FindKeys
                string zapfind = "select Top 1 Id from UpdateProgramm where TypeProgramm='ServiceTool' and Status<3 and SysVersion=" + info.SystemVersion.ToString();
                var objdevid = UtilsBase.ExecuteScalar(zapfind, connect, null);

                if (objdevid != null && objdevid != System.DBNull.Value)
                {
                    idupdateprogramm = (Guid)objdevid;
                }

                string zapdb = "select top 1 Id from UpdateProgramm where TypeProgramm='Db-ServiceTool' and Status<3 and SysVersion=" + info.SystemVersion.ToString();
                objdevid = UtilsBase.ExecuteScalar(zapdb, connect, null);
                bool iscontinue = true;
                if (objdevid != null && objdevid != System.DBNull.Value)
                {
                    idupdatedb = (Guid)objdevid;
                }
                #endregion FindKeys
            }
            return info;
        }

        /// <summary>
        /// Проверяем на равенство текушую версию и ту версию до которо нужно обновить
        /// </summary>
        /// <param name="curent">Текуая версия</param>
        /// <param name="toupdate">Вресия до которой обновляем</param>
        /// <param name="field">Поле в которой записаные версии</param>
        /// <returns>Значение версии до которой обновляем если надо или null</returns>
        protected int? TestVersion(DataRow curent, DataRow toupdate, string field)
        {
            int? result = null;
            int curver = Convert.ToInt32(curent[field]);
            int tover = Convert.ToInt32(toupdate[field]);
            if (tover>curver)
            {
                result = tover;
            }
            return result;
        }

        /// <summary>
        /// Скачивание необходимых файлов с ftp
        /// </summary>
        /// <param name="info">Информация о версия</param>
        protected bool DownLoadFilesFromFtp(InfoVersionDownload info, SqlConnection connect)
        {
            bool result = true;
            try
            {
                if (info != null)
                {
                    infoupd.FtpDownload(1);
                    infoupd.UnZipLocalFile(1);

                    if (Kassa != null && info.Kassa != null && info.Kassa.HasValue && !Kassa.HasLocalFile(info.Kassa.Value))
                    {
                        Kassa.FtpDownload(info.Kassa.Value);
                    }
                    if (Stoika != null && info.Stoika != null && info.Stoika.HasValue && !Stoika.HasLocalFile(info.Stoika.Value))
                    {
                        Stoika.FtpDownload(info.Stoika.Value);
                    }
                    if (ServiceTool != null && info.ServiceTool != null && info.ServiceTool.HasValue && !ServiceTool.HasLocalFile(info.ServiceTool.Value))
                    {
                        ServiceTool.FtpDownload(info.ServiceTool.Value);
                        ServiceTool.UnZipLocalFile(info.ServiceTool.Value);

                        if(!(idupdateprogramm!=null && idupdateprogramm.HasValue))
                        {
                            this.idupdateprogramm = Guid.NewGuid();
                        }
                        UtilsBase.SetOne("UpdateProgramm", "Id", new object[] {"SysVersion",info.SystemVersion.Value, "VersionProgramm",info.ServiceTool.Value,
                                        "Status",2,"TypeProgramm","ServiceTool","Id",idupdateprogramm},
                                "SysVersion=@ver and TypeProgramm=@tp", new object[] { "@ver", info.SystemVersion.Value, "@tp", "ServiceTool" }, connect);
                    }
                    if (Web != null && info.Web != null && info.Web.HasValue && !Web.HasLocalFile(info.Web.Value))
                    {
                        Web.FtpDownload(info.Web.Value);
                        Web.UnZipLocalFile(info.Web.Value);
                        Guid key = Guid.NewGuid();
                        UtilsBase.SetOne("UpdateProgramm", "Id", new object[] {"SysVersion",info.SystemVersion.Value, "VersionProgramm",info.ServiceTool.Value,
                                        "Status",2,"TypeProgramm","Web","Id",key},
                                "SysVersion=@ver and TypeProgramm=@tp", new object[] { "@ver", info.SystemVersion.Value, "@tp", "Web" }, connect);
                    }
                    if(this.Db!=null && info.Db!=null && info.Db.HasValue)
                    {
                        Db.FtpDownload(info.Db.Value);
                        Db.UnZipLocalFile(info.Db.Value);

                        if(!(idupdatedb!=null && idupdatedb.HasValue))
                        {
                            this.idupdatedb = Guid.NewGuid();
                        }
                       
                        UtilsBase.SetOne("UpdateProgramm", "Id", new object[] {"SysVersion",info.SystemVersion.Value, "VersionProgramm",info.Db.Value,
                                        "Status",2,"TypeProgramm","Db-ServiceTool","Id",idupdatedb},
                                "SysVersion=@ver and TypeProgramm=@tp", new object[] { "@ver", info.SystemVersion.Value, "@tp", "Db-ServiceTool" }, connect);
                    }
                }
            }
            catch(Exception ex)
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Проверяем все ли устройства которые нужны скачали свои новые версии
        /// </summary>
        /// <param name="connect">Соедиение с БД</param>
        /// <returns>true-все необходимые устройства себя скачали false не скачали</returns>
        protected bool DownloadedProgramm(InfoVersionDownload infoversion)
        {
            #region Programm
            bool result = false;
            //Касса Type=2
            //Стойка Type=1
            string include = string.Empty;
            bool isfirst = true;
            bool downloaddevices = true;
            if (infoversion.Kassa != null && infoversion.Kassa.HasValue)
            {
                include = "2"; isfirst = false; downloaddevices = false;
            }
            if (infoversion.Stoika != null && infoversion.Stoika.HasValue)
            {
                downloaddevices = false;
                if (isfirst) { include = "1"; isfirst = false; }
                else { include = include + ",1"; }
            }

            if (!string.IsNullOrEmpty(include) && !string.IsNullOrWhiteSpace(include))
            {
                string zap = "select  count(*) from DeviceModel dm " +
                            " left join UpdateProgramm up on up.DeviceId = dm.Id and up.SysVersion = " + infoversion.SystemVersion.Value.ToString() +" and up.TypeProgramm != 'Db' " +
                            " where dm.Type in(" + include + ") and isnull(up.Status, 0)< 2";
                object cnobj = UtilsBase.ExecuteScalar(zap, connect, null, null);
                if (cnobj != null && cnobj != System.DBNull.Value)
                {
                    int cn = System.Convert.ToInt32(cnobj);
                    if (cn > 0)
                    {
                        downloaddevices = false;
                    }
                    else
                    {
                        downloaddevices = true;
                    }
                }
                else
                {
                    downloaddevices = true;
                }
            }

            bool downloadservicetool = false;
            if (infoversion.ServiceTool != null && infoversion.ServiceTool.HasValue)
            {
                //Проверяем для ServiceTool
                string zap = "select top 1 Id from UpdateProgramm where SysVersion = " + infoversion.SystemVersion.Value.ToString() + " and TypeProgramm = 'ServiceTool' and Status = 2";
                object cnobj = UtilsBase.ExecuteScalar(zap, connect, null, null);
                if (cnobj != null && cnobj != System.DBNull.Value)
                {
                    downloadservicetool = true;
                }
            }
            else
            {
                downloadservicetool = true;
            }

            bool downloadweb = false;
            if (infoversion.Web != null && infoversion.Web.HasValue)
            {
                //Проверяем для Web
                string zap = "select top 1 Id from UpdateProgramm where SysVersion = " + infoversion.SystemVersion.Value.ToString() + " and TypeProgramm = 'Web' and Status = 2";
                object cnobj = UtilsBase.ExecuteScalar(zap, connect, null, null);
                if (cnobj != null && cnobj != System.DBNull.Value)
                {
                    downloadweb = true;
                }
            }
            else
            {
                downloadweb = true;
            }

            if (downloaddevices && downloadservicetool && downloadweb)
            { result = true; }
            else { result = false; }
            #endregion Programm

            #region DownloadDb
            bool downloaddevdb = true;
            bool donwloadservdb = true;
            if (infoversion.Db != null && infoversion.Db.HasValue)
            {
                string zapdb = "select count(*) from DeviceModel dm " +
                              " left join UpdateProgramm up on dm.Id = up.DeviceId and up.SysVersion = " + infoversion.SystemVersion.Value.ToString() + " and up.TypeProgramm = 'Db' " +
                               " where isnull(up.Status, 0)< 2";
                object cnobj = UtilsBase.ExecuteScalar(zapdb, connect, null, null);
                if (cnobj != null && cnobj != System.DBNull.Value)
                {
                    int cn = System.Convert.ToInt32(cnobj);
                    if (cn > 0)
                    {
                        downloaddevdb = false;
                    }
                    else
                    {
                        downloaddevdb = true;
                    }
                }
                else
                {
                    downloaddevdb = true;
                }

                string zapservdb = "select count(*) from UpdateProgramm where SysVersion=" + infoversion.SystemVersion.Value.ToString() + " and TypeProgramm = 'Db-ServiceTool' and isnull(Status, 0)< 2";

                object cnservdb = UtilsBase.ExecuteScalar(zapservdb, connect, null, null);
                if (cnservdb != null && cnservdb != System.DBNull.Value)
                {
                    int cn = System.Convert.ToInt32(cnservdb);
                    if (cn > 0)
                    {
                        donwloadservdb = false;
                    }
                    else
                    {
                        donwloadservdb = true;
                    }
                }
                else
                {
                    donwloadservdb = true;
                }
            }
            #endregion DownloadDb

            if(result && downloaddevdb && donwloadservdb) { result = true; }
            else { result = false; }

            return result;
        }


        /// <summary>
        /// Из текущей БД 2 уроня берем параметры и формируме строку для доступа к 3 уровню
        /// </summary>
        /// <param name="connectdev"></param>
        /// <returns></returns>
        protected string GetConnectLevel3(SqlConnection connectdev)
        {
            string result = string.Empty;
            string host = string.Empty;
            string namedb = string.Empty;
            string pwd = string.Empty;
            string login = string.Empty;
            string sel = "select Name,Value from Setting where Name in('DB3Host','DB3Password','DB3Login','DB3Name')";
            using (var table = UtilsBase.FillTable(sel, connectdev, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    string name = ConvertField.GetString(row, "Name");
                    string val = ConvertField.GetString(row, "Value");
                    if (!string.IsNullOrEmpty(name) && !string.IsNullOrWhiteSpace(name))
                    {
                        if ("db3host".Equals(name.Trim().ToLower()))
                        { host = val; }
                        if ("db3password".Equals(name.Trim().ToLower()))
                        {
                            Rps.Crypto.CryptoDecrypto crypto = new Rps.Crypto.CryptoDecrypto();
                            pwd = crypto.Decrypto(val);
                        }
                        if ("db3login".Equals(name.Trim().ToLower()))
                        { login = val; }
                        if ("db3name".Equals(name.Trim().ToLower()))
                        { namedb = val; }
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(host) && !string.IsNullOrEmpty(host)
                && !string.IsNullOrWhiteSpace(namedb) && !string.IsNullOrEmpty(namedb)
                && !string.IsNullOrWhiteSpace(pwd) && !string.IsNullOrEmpty(pwd)
                && !string.IsNullOrWhiteSpace(login) && !string.IsNullOrEmpty(login))
            {
                result = "Data Source=" + host + "; Initial Catalog=" + namedb + "; User Id=" + login + "; Password=" + pwd;

            }
            return result;
        }

        /// <summary>
        /// Установка версии до которой обновли текущей
        /// </summary>
        /// <param name="torow">DataRor из SystemVersion до которой обновлили</param>
        protected void SetCurrent(DataRow torow)
        {
            object res = torow["SysVersion"];
            using (SqlTransaction transact = connect.BeginTransaction())
            {
                try
                {
                    UtilsBase.UpdateCommand("SystemVersion", new object[] { "Status", StatSystemVersion.Архивная }, "Status="+StatSystemVersion.Текущая.ToString(), null, connect,transact);
                    UtilsBase.UpdateCommand("SystemVersion", new object[] { "Status", StatSystemVersion.Текущая }, "SysVersion=@prwh1", new object[] { "@prwh1", (dynamic)res }, connect, transact);
                    transact.Commit();
                }
                catch(Exception ex)
                {
                    transact.Rollback();
                }
            }
            string conlevel3 = GetConnectLevel3(connect);
            if (!string.IsNullOrEmpty(conlevel3) && !string.IsNullOrWhiteSpace(conlevel3))
            {
                using (var con3 = new SqlConnection(conlevel3))
                {

                    try
                    {
                        con3.Open();
                        using (SqlTransaction transact = con3.BeginTransaction())
                        {
                            try
                            {
                                UtilsBase.UpdateCommand("SystemVersion", new object[] { "Status", StatSystemVersion.Архивная }, "Status=" + StatSystemVersion.Текущая.ToString(), null, con3, transact);
                                UtilsBase.UpdateCommand("SystemVersion", new object[] { "Status", StatSystemVersion.Текущая }, "SysVersion=@prwh1", new object[] { "@prwh1", (dynamic)res }, con3, transact);
                                transact.Commit();
                            }
                            catch (Exception ex)
                            {
                                transact.Rollback();
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        try
                        {
                            con3.Close();
                        }
                        catch { }
                    }
                }
            }
        }

        /// <summary>
        /// Отправка через базу команды устройству на перезапуск
        /// </summary>
        /// <param name="deviceid">Код устройства</param>
        /// <param name="SystemVersion">Вресия до которой обновляем</param>
        protected void SendRestart(Guid deviceid,int SystemVersion)
        {
            UtilsBase.InsertCommand("CommandDevice", connect, null,"Id",Guid.NewGuid(),
                "DeviceId", deviceid, "Command", "RestartUpdate:" + SystemVersion.ToString(), "Status", 0);
        }

        /// <summary>
        /// Проверка обновилось устройство или нет
        /// </summary>
        /// <param name="deviceid">Код устройства</param>
        /// <param name="SystemVersion">Вресия системы до которой обновляем</param>
        /// <returns>true- обновилось false-не обновилось</returns>
        protected bool IsUpdaterDevice(Guid deviceid,int SystemVersion)
        {
            bool result = false;
            bool updatedb = true;
            if(this.Db!=null)
            {
                updatedb = false;
                string zapdb = "select top 1 Status from UpdateProgramm where TypeProgramm='DB'and Status=4 and DeviceId=@devid and SysVersion=" + SystemVersion.ToString();
                var rowdb = UtilsBase.FillRow(zapdb, connect, null, "@devid", deviceid);
                if(rowdb!=null)
                {
                    updatedb = true;
                }
            }

            string zap = "select top 1 Status from UpdateProgramm where Status=4 and DeviceId=@devid and SysVersion=" + SystemVersion.ToString();
            var row=UtilsBase.FillRow(zap, connect, null, "@devid", deviceid);
            if(row!=null && updatedb)
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// Откатываем команду на обновления
        /// </summary>
        /// <param name="row">Строка из SystemVersion до которой пытались обновить</param>
        protected void RollBackUpdater(DataRow row)
        {
            int id = ConvertField.GetInt(row, "SysVersion").Value;
            UtilsBase.UpdateCommand("SystemVersion", new object[] { "Status", StatSystemVersion.ОбновлениесОшибкой }, "SysVersion=" + id.ToString(), null, connect);
            string conlevel3 = GetConnectLevel3(connect);
            if (!string.IsNullOrEmpty(conlevel3) && !string.IsNullOrWhiteSpace(conlevel3))
            {
                using (var con3 = new SqlConnection(conlevel3))
                {
                    try
                    {
                        con3.Open();
                        UtilsBase.UpdateCommand("SystemVersion", new object[] { "Status", StatSystemVersion.ОбновлениесОшибкой }, "SysVersion=" + id.ToString(), null, con3);
                        con3.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        /// <summary>
        /// Запуск утилиты обновления
        /// </summary>
        /// <param name="info">Информация для запуска утилиты</param>
        /// <returns>true- запуск прошел успешно false- не успешно</returns>
        protected bool RunRestartServiceTool2(InfoDownload info)
        {
            bool isclose = true;
            try
            {
                ProcessStartInfo proc = new ProcessStartInfo();
                proc.UseShellExecute = true;
                proc.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
                proc.FileName = PathUpdater;

                string nameexe = "ServiceTool2.exe";

                //string nameexe = System.IO.Path.GetFileName(AppDomain.CurrentDomain.FriendlyName);

                string oldfolder = AppDomain.CurrentDomain.BaseDirectory;

                string newfolder = info.NewVersionFolder();

                if(oldfolder[oldfolder.Length-1]=='\\')
                {
                    oldfolder = oldfolder.Remove(oldfolder.Length - 1, 1);
                }

                if (newfolder[newfolder.Length - 1] == '\\')
                {
                    newfolder = newfolder.Remove(newfolder.Length - 1, 1);
                }
                //deviceupdateid = Guid.Parse("099e71b5-765d-4a13-b8bc-2beabde2e645");


                proc.Arguments =idupdateprogramm.ToString() + " \"" + nameexe + "\" \"" + oldfolder + "\" \"" + newfolder + "\"";
                proc.Verb = "runas";
                var resproc=Process.Start(proc);
                if(resproc==null)
                {
                    isclose = false;
                    UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 3, "Error", "Не удалось запустить" }, "Id=@id", new object[] { "@id", idupdateprogramm }, connect);
                }
            }
            catch (Exception ex)
            {
                isclose = false;
                UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 3, "Error", ex.Message }, "Id=@id", new object[] { "@id", idupdateprogramm }, connect);
            }
            return isclose;
        }
        /// <summary>
        /// Вызов обновления БД
        /// </summary>
        /// <returns>true- обновление прошло успешно false- Не успешно</returns>
        public bool UpdateDb()
        {
            bool isclose = true;
            if (Db != null)
            {
                try
                {
                    ProcessStartInfo proc = new ProcessStartInfo();
                    proc.UseShellExecute = true;
                    //proc.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    string nameexe = System.IO.Path.Combine(this.Db.NewVersionFolder(), "Rps.UpdateDB.exe");
                    proc.FileName = nameexe;
                    //deviceupdateid = Guid.Parse("099e71b5-765d-4a13-b8bc-2beabde2e645");

                    proc.Arguments = idupdatedb.ToString();
                    proc.Verb = "runas";
                    var resproc = Process.Start(proc);
                    if (resproc == null)
                    {
                        isclose = false;
                        UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 3, "Error", "Не удалось запустить" }, "Id=@id", new object[] { "@id", idupdatedb }, connect);
                    }

                    string zap = "select top 1 Status from UpdateProgramm where Id=@id";
                    bool flag = true;
                    while (flag)
                    {
                        try
                        {
                            var row = UtilsBase.FillRow(zap, connect, null, "@id", idupdatedb);
                            if (row != null)
                            {
                                int stat = row.GetInt("Status").Value;
                                if (stat > 2)
                                {
                                    flag = false;
                                    if (stat == 3) { isclose = false; }
                                    if (stat == 4) { isclose = true; }
                                }
                            }
                        }
                        catch { }
                        if (flag)
                        {
                            Thread.Sleep(TimeSpan.FromSeconds(waitsec));
                        }
                    }

                    isclose = true;
                }
                catch (Exception ex)
                {
                    isclose = false;
                    UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 3, "Error", ex.Message }, "Id=@id", new object[] { "@id", idupdatedb }, connect);
                }
            }
            return isclose;
        }

        /// <summary>
        /// Ожидаем пока устройства выкачают файлы со 2 уровня на устройства 1 уровня
        /// и проставят информацию об этом в UpdateProgramm
        /// </summary>
        /// <param name="updater">Информаци о том что обновляем</param>
        private void WaitDownloadFilesToDevices(InfoVersionDownload updater)
        {
            bool flag1 = true;
            while (flag1)
            {
                //Ожидаем когда все на себя скопируют файлы
                if (DownloadedProgramm(updater))
                {
                    flag1 = false;
                }
                if (flag1)
                {
                    Thread.Sleep(new TimeSpan(0, 1, 0));
                }
            }
        }
        /// <summary>
        /// Скачиаем файлы для обновления с 3 уровня на 2 уровень через ftp
        /// </summary>
        /// <param name="updater">Информация о том что обновляем</param>
        private void WaitDownloadFromFTP(InfoVersionDownload updater,DataRow updaterow)
        {
            bool isftpdownlod = true;
            while (isftpdownlod)
            {
                if (DownLoadFilesFromFtp(updater, connect))
                {
                    isftpdownlod = false;
                    int id = updaterow.GetInt("SysVersion").Value;
                    //Устанавливем статус можно скачивать
                    UtilsBase.UpdateCommand("SystemVersion", new object[] { "Status", StatSystemVersion.МожноСкачивать },
                        "SysVersion=" + id.ToString(), null, this.connect);
                    break;
                }
                else
                {
                    Thread.Sleep(new TimeSpan(0, 1, 0));
                }
            }
        }

        /// <summary>
        /// Обновления устройств
        /// </summary>
        /// <param name="row">Строка из SystemVersion до которой нужно обновить</param>
        /// <param name="updater">Информация для обновления</param>
        private void UpdateDevices(DataRow row, InfoVersionDownload updater)
        {
            //2 Проходим по Device и вызываем для перезапуска их
            bool isupdaterdevice = true;
            string zap = "select up.*,dm.Host,dm.Type from UpdateProgramm up " +
                        " left join DeviceModel dm on dm.Id = up.DeviceId " +
                        " where up.SysVersion = " + updater.SystemVersion.ToString() + " and up.Status = 2 and up.DeviceId is not null";
            List<Guid> usedevices = new List<Guid>();
            using (DataTable table = UtilsBase.FillTable(zap, connect, null))
            {
                if (table != null && table.Rows.Count > 0)
                {
                    isupdaterdevice = false;
                    foreach (DataRow devrow in table.Rows)
                    {
                        int type = devrow.GetInt("Type").Value;
                        string host = devrow.GetString("Host");
                        int status = devrow.GetInt("Status").Value;
                        Guid gd = (Guid)devrow["DeviceId"];
                        if (!usedevices.Contains(gd))
                        {
                            usedevices.Add(gd);
                            if (status <= 2)
                            {
                                SendRestart(gd, updater.SystemVersion.Value);
                            }
                            DateTime dtstart = DateTime.Now;
                            bool flagwait = true;
                            while (flagwait)
                            {
                                Thread.Sleep(TimeSpan.FromSeconds(waitsec));
                                isupdaterdevice = IsUpdaterDevice(gd, updater.SystemVersion.Value);
                                if(isupdaterdevice)
                                {
                                    flagwait = false;
                                }
                                else
                                {
                                    DateTime dtend = DateTime.Now;
                                    if(dtend.Subtract(dtstart)>TimeSpan.FromSeconds(waitrestartdevice))
                                    {
                                        flagwait = false;
                                    }
                                }
                            }

                            //Проверяем успешно ли прошло обновление для этого устройства
                            
                            if (!isupdaterdevice)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            //Устанавливаем ошибку у устройств 
            string updatererror = "update UpdateProgramm set Status=3,Error='Нет ответа от устройства' where " +
            "SysVersion=" + updater.SystemVersion.ToString() + " and Status<=2 and DeviceId is not null";
            UtilsBase.ExecuteNonQuery(updatererror, connect, null);

            //Также устанавливаем текущие версии на сервере 2 уровня и 3 уровня
            if (isupdaterdevice)
            {
                SetCurrent(row);//как подлючится к серверу 3 уровня
            }
            else
            {
                RollBackUpdater(row);
            }
        }

        #region Update Web
        /// <summary>
        /// Обновление Web
        /// </summary>
        /// <param name="updater">Информация по обновлению</param>
        private void UpdaterWeb(InfoVersionDownload updater)
        {
            #region Обновляем сайт
            if (updater.Web != null && updater.Web.HasValue)
            {
                bool web = RestartWeb();

                Guid gdweb = Guid.NewGuid();
                if (web)
                {
                    UtilsBase.SetOne("UpdateProgramm", "Id", new object[] {"SysVersion",updater.SystemVersion.Value,
                                    "VersionProgramm",updater.Web.Value,
                                      "Status",4,"TypeProgramm","Web","Id",gdweb},
                          "SysVersion=" + updater.SystemVersion.Value.ToString() + " and TypeProgramm='Web'", null
                          , connect);
                }
                else
                {
                    UtilsBase.SetOne("UpdateProgramm", "Id", new object[] {"SysVersion",updater.SystemVersion.Value,
                                    "VersionProgramm",updater.Web.Value,
                                      "Status",3,"TypeProgramm","Web","Id",gdweb,"Error","Не удалось перезапустить web"},
                          "SysVersion=" + updater.SystemVersion.Value.ToString() + " and TypeProgramm='Web'", null
                          , connect);
                }
            }
            #endregion Обновляем сайт
        }
       /// <summary>
       /// Перезапуск служб сайта для его обновления
       /// </summary>
       /// <returns>true-успешный перезапуск false- не успешно</returns>
        protected bool RestartWeb()
        {
            bool res = true;
            string PathWeb = @"C:\WebRps\WebRps_Run";

            try
            {
                #region Old
                /*
                ProcessStartInfo proc = new ProcessStartInfo();
                proc.UseShellExecute = true;
                proc.WorkingDirectory = Environment.CurrentDirectory;
                proc.FileName = @"C:\Windows\System32\iisreset.exe";
                proc.Arguments = "/stop";
                proc.Verb = "runas";
                Process.Start(proc);

                DirectoryInfo oldweb = new DirectoryInfo(PathWeb);
                string oldfolder = Path.Combine(PathWeb, "Old" + DateTime.Today.ToString("yyyyMMdd"));
                oldweb.MoveTo(oldfolder);

                DirectoryInfo newweb = new DirectoryInfo(this.Web.NewVersionFolder());
                newweb.MoveTo(PathWeb);
                oldweb.Delete(true);

                ProcessStartInfo proc1 = new ProcessStartInfo();
                proc1.UseShellExecute = true;
                proc1.WorkingDirectory = Environment.CurrentDirectory;
                proc1.FileName = @"C:\Windows\System32\iisreset.exe";
                proc1.Arguments = "/start";
                proc1.Verb = "runas";
                Process.Start(proc1);
                */
                #endregion Old
                if (Web!=null)
                {
                    string folder = Web.NewVersionFolder();
                    string filename = "Update.bat";
                    filename = System.IO.Path.Combine(folder, filename);
                    if (System.IO.File.Exists(filename))
                    {
                        ProcessStartInfo proc = new ProcessStartInfo();
                        proc.UseShellExecute = true;
                        proc.WorkingDirectory = folder;
                        proc.FileName = filename;
                        proc.Verb = "runas";
                        Process.Start(proc);
                        if(proc!=null)
                        {
                            res = true;
                        }
                        else
                        {
                            res = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                res = false;
            }

            return res;
        }
        #endregion Update Web

        /// <summary>
        /// Продолженеи обновления после запуска 
        /// </summary>
        public void UpdateAfterRestart()
        {
            try
            {
                var row = TestNeedUpdate(StatSystemVersion.МожноСкачивать);
                if (row != null)
                {
                    var updater = CalculateUpdate(row);
                    #region FindKeys
                    string zapfind = "select Top 1 Id from UpdateProgramm where TypeProgramm='ServiceTool' and Status<3 and SysVersion="+updater.SystemVersion.ToString();
                    var objdevid = UtilsBase.ExecuteScalar(zapfind, connect, null);

                    if (objdevid != null && objdevid != System.DBNull.Value)
                    {
                        UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 4 }, "Id=@key", new object[] { "@key", (dynamic)objdevid }, connect);
                        idupdateprogramm = (Guid)objdevid;
                    }

                    string zapdb = "select top 1 Id from UpdateProgramm where TypeProgramm='Db-ServiceTool' and Status<3 and SysVersion=" + updater.SystemVersion.ToString();
                    objdevid = UtilsBase.ExecuteScalar(zapdb, connect, null);
                    bool iscontinue = true;
                    if (objdevid != null && objdevid != System.DBNull.Value)
                    {
                        idupdatedb = (Guid)objdevid;
                        iscontinue = UpdateDb();
                    }
                    #endregion FindKeys
                    
                    if (iscontinue)
                    {
                        //1 Проверяем нужно ли перезапускать Web
                        UpdaterWeb(updater);
                        //2 Проходим по устройствам и даем команду на обновление
                        UpdateDevices(row, updater);
                    }
                    else
                    {
                        RollBackUpdater(row);
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }

        /// <summary>
        /// Отслеживание и выполнение обновления
        /// </summary>
        public void ProcessUpdate()
        {
            bool flag = true;
            while (flag)
            {
                int sysversion = -1;
                int versionprog = -1;
                try
                {
                    var row = TestNeedUpdate();
                    if (row != null)
                    {
                        #region DownLoadFilesFromFTP
                        //1 Скачиваем файлы
                        sysversion = row.GetInt("SysVersion").Value;
                        versionprog = row.GetInt("STVer").Value;
                        var updater = CalculateUpdate(row);//Определяем что нужно обновить
                                                           //Выкачиваем все файлы
                        WaitDownloadFromFTP(updater,row);
                        #endregion DownLoadFilesFromFTP

                        #region Ждем скачивания архивов на устройства и их распаковку
                        //Ожидаем пока все скачают архивы и распакуют их и выставт сосотояние равное 2
                        WaitDownloadFilesToDevices(updater);
                        #endregion Ждем скачивания архивов на устройства и их распаковку

                        //После того как все все что нужно скопировали проеряем нужно ли себя перезапускать если нужно
                        //то выходим оз обновления и выставляем фд=лаг закрытия программы и устанваливаем чо делать 
                        //при закрытии программы
                        if (updater.ServiceTool != null && updater.ServiceTool.HasValue)
                        {
                            flag = false;
                            UtilsFlags.IsCloseProgramm = true;
                            UtilsFlags.UpdaterClosed = true;
                            UtilsFlags.lstClosed.Add(() =>
                            {
                                this.RunRestartServiceTool2(this.ServiceTool);
                            });
                            break;
                        }
                        else//Себя не нужно перегружать
                        {
                            Guid key = Guid.NewGuid();
                            //0 Проеряем нужно ли обновлять БД
                            #region Обновляем БД
                            bool iscontinue = false;
                            iscontinue = UpdateDb();
                            #endregion Обновляем БД
                            if (iscontinue)
                            {
                                //1 Проверяем нужно ли перезапускать Web
                                UpdaterWeb(updater);
                                UpdateDevices(row, updater);

                            }
                            else
                            {
                                RollBackUpdater(row);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                if (flag)
                {
                    Thread.Sleep(new TimeSpan(0, 1, 0));
                }
            }
        }

        /// <summary>
        /// Функция запуска обновления в отдельном потоке
        /// </summary>
        /// <param name="connect">Соединение к БД 2 уровня</param>
        public static void RunUpdater(string constr)
        {
            SqlConnection connect = new SqlConnection(constr);
            connect.Open();
            UpdateServiceTool2 update = new UpdateServiceTool2(connect);
            update.UpdateAfterRestart();
            Task ts = new Task(() =>
            {
                update.ProcessUpdate();
            });
            ts.Start();
        }

        /*
public static void HttpSendUpdate(string host, int type)
{
    try
    {
        //string baseadress = ConfigurationManager.AppSettings["web"];// "http://format.r-p-s.ru/action/RemoteNotifier";
        string port = string.Empty;
        if (type == 2)
        {
            port = ":3333";
        }
        string zap = "http://" + host + port + "/Update";
        using (var wc = new WebClient())
        {
            wc.UploadData(new Uri(zap), new byte[0]);
        }
    }
    catch (Exception ex)
    {

    }
}
*/
    }

    public class DeviceUpdater
    {
        public Guid DeviceId { get; set; }
        public int SystemVersion { get; set; }

    }

    public class InfoVersionDownload
    {
        public int? SystemVersion = null;
        public int? ServiceTool = null;
        public int? Web = null;
        public int? Kassa = null;
        public int? Stoika = null;
        public int? Db = null;
    }

   
}
