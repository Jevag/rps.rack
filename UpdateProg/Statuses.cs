﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg
{
    public class StatSystemVersion
    {
        public const int ЕстьЗапись = 1;
        public const int Обновляемая = 2;
        public const int МожноСкачивать = 3;
        public const int Текущая = 4;
        public const int ОбновлениесОшибкой = 5;
        public const int Архивная = 6;
    }
}
