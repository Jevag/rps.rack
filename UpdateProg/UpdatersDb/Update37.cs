﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update37 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 38;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update37) && !string.IsNullOrWhiteSpace(UpdateResource.Update37))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update37, connect, null);
            }
        }
    }
}
