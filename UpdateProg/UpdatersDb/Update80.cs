﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update80 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 81;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update80) && !string.IsNullOrWhiteSpace(UpdateResource.Update80))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update80, connect, null);
            }
        }
    }
}
