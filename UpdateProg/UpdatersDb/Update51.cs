﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update51 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 52;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update51) && !string.IsNullOrWhiteSpace(UpdateResource.Update51))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update51, connect, null);
            }
        }
    }
}
