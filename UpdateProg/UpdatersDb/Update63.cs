﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update63 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 64;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update63) && !string.IsNullOrWhiteSpace(UpdateResource.Update63))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update63, connect, null);
            }
        }
    }
}
