﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update48 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 49;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update48) && !string.IsNullOrWhiteSpace(UpdateResource.Update48))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update48, connect, null);
            }
        }
    }
}
