﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update69 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 70;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update69) && !string.IsNullOrWhiteSpace(UpdateResource.Update69))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update69, connect, null);
            }
        }
    }
}
