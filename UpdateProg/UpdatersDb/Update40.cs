﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update40 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 41;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update40) && !string.IsNullOrWhiteSpace(UpdateResource.Update40))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update40, connect, null);
            }
        }
    }
}
