﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update75 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 76;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update75) && !string.IsNullOrWhiteSpace(UpdateResource.Update75))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update75, connect, null);
            }
        }
    }
}
