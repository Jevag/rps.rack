﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update58 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 59;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update58) && !string.IsNullOrWhiteSpace(UpdateResource.Update58))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update58, connect, null);
            }
        }
    }
}
