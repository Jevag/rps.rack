﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update61 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 62;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update61) && !string.IsNullOrWhiteSpace(UpdateResource.Update61))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update61, connect, null);
            }
        }
    }
}
