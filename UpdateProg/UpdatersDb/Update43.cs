﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update43 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 44;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update43) && !string.IsNullOrWhiteSpace(UpdateResource.Update43))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update43, connect, null);
            }
        }
    }
}
