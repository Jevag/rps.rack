﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update45 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 46;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update45) && !string.IsNullOrWhiteSpace(UpdateResource.Update45))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update45, connect, null);
            }
        }
    }
}
