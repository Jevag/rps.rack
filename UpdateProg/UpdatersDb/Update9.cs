﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update9 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 10;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update9) && !string.IsNullOrWhiteSpace(UpdateResource.Update9))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update9, connect, null);
            }
        }
    }
}
