﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update56 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 57;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update56) && !string.IsNullOrWhiteSpace(UpdateResource.Update56))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update56, connect, null);
            }
        }
    }
}
