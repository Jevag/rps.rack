﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update55 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 56;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update55) && !string.IsNullOrWhiteSpace(UpdateResource.Update55))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update55, connect, null);
            }
        }
    }
}
