﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update2 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 3;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update2) && !string.IsNullOrWhiteSpace(UpdateResource.Update2))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update2, connect, null);
            }
        }
    }
}
