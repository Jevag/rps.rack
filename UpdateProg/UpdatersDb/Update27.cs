﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update27 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 28;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update27) && !string.IsNullOrWhiteSpace(UpdateResource.Update27))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update27, connect, null);
            }
        }
    }
}
