﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update11 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 12;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update11) && !string.IsNullOrWhiteSpace(UpdateResource.Update11))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update11, connect, null);
            }
        }
    }
}
