﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update50 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 51;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update50) && !string.IsNullOrWhiteSpace(UpdateResource.Update50))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update50, connect, null);
            }
        }
    }
}
