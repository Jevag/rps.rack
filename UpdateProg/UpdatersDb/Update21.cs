﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update21 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 22;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update21) && !string.IsNullOrWhiteSpace(UpdateResource.Update21))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update21, connect, null);
            }
        }
    }
}
