﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update28 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 29;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update28) && !string.IsNullOrWhiteSpace(UpdateResource.Update28))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update28, connect, null);
            }
        }
    }
}
