﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update18 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 19;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update18) && !string.IsNullOrWhiteSpace(UpdateResource.Update18))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update18, connect, null);
            }
        }
    }
}
