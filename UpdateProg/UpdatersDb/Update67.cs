﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update67 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 68;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update67) && !string.IsNullOrWhiteSpace(UpdateResource.Update67))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update67, connect, null);
            }
        }
    }
}
