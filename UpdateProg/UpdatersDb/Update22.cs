﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update22 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 23;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update22) && !string.IsNullOrWhiteSpace(UpdateResource.Update22))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update22, connect, null);
            }
        }
    }
}
