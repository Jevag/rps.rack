﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update17 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 18;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update17) && !string.IsNullOrWhiteSpace(UpdateResource.Update17))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update17, connect, null);
            }
        }
    }
}
