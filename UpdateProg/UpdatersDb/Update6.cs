﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update6 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 7;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update6) && !string.IsNullOrWhiteSpace(UpdateResource.Update6))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update6, connect, null);
            }
        }
    }
}
