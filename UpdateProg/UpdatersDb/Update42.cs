﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update42 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 43;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update42) && !string.IsNullOrWhiteSpace(UpdateResource.Update42))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update42, connect, null);
            }
        }
    }
}
