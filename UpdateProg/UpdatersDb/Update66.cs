﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update66 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 67;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update66) && !string.IsNullOrWhiteSpace(UpdateResource.Update66))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update66, connect, null);
            }
        }
    }
}
