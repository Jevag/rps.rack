﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update26 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 27;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update26) && !string.IsNullOrWhiteSpace(UpdateResource.Update26))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update26, connect, null);
            }
        }
    }
}
