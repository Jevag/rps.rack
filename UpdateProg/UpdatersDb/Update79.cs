﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update79 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 80;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update79) && !string.IsNullOrWhiteSpace(UpdateResource.Update79))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update79, connect, null);
            }
        }
    }
}
