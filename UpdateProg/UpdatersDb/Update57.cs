﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update57 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 58;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update57) && !string.IsNullOrWhiteSpace(UpdateResource.Update57))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update57, connect, null);
            }
        }
    }
}
