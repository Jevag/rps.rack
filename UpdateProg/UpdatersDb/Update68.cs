﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update68 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 69;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update68) && !string.IsNullOrWhiteSpace(UpdateResource.Update68))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update68, connect, null);
            }
        }
    }
}
