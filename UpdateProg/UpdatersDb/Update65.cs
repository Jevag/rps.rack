﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update65 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 66;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update65) && !string.IsNullOrWhiteSpace(UpdateResource.Update65))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update65, connect, null);
            }
        }
    }
}
