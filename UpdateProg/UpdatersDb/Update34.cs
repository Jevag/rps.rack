﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update34 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 35;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update34) && !string.IsNullOrWhiteSpace(UpdateResource.Update34))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update34, connect, null);
            }
        }
    }
}
