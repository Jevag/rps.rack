﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update35 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 36;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update35) && !string.IsNullOrWhiteSpace(UpdateResource.Update35))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update35, connect, null);
            }
        }
    }
}
