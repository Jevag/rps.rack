﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update62 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 63;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update62) && !string.IsNullOrWhiteSpace(UpdateResource.Update62))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update62, connect, null);
            }
        }
    }
}
