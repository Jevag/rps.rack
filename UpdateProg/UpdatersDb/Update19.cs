﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update19 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 20;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update19) && !string.IsNullOrWhiteSpace(UpdateResource.Update19))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update19, connect, null);
            }
        }
    }
}
