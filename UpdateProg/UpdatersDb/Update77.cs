﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update77 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 78;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update77) && !string.IsNullOrWhiteSpace(UpdateResource.Update77))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update77, connect, null);
            }
        }
    }
}
