﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update31 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 32;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update31) && !string.IsNullOrWhiteSpace(UpdateResource.Update31))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update31, connect, null);
            }
        }
    }
}
