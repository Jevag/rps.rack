﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update16 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 17;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update16) && !string.IsNullOrWhiteSpace(UpdateResource.Update16))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update16, connect, null);
            }
        }
    }
}
