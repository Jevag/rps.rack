﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update71 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 72;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update71) && !string.IsNullOrWhiteSpace(UpdateResource.Update71))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update71, connect, null);
            }
        }
    }
}
