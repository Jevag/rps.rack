﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update10 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 11;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update10) && !string.IsNullOrWhiteSpace(UpdateResource.Update10))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update10, connect, null);
            }
        }
    }
}
