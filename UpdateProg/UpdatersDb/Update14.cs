﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update14 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 15;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update14) && !string.IsNullOrWhiteSpace(UpdateResource.Update14))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update14, connect, null);
            }
        }
    }
}
