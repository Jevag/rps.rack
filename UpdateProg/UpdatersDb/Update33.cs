﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update33 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 34;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update33) && !string.IsNullOrWhiteSpace(UpdateResource.Update33))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update33, connect, null);
            }
        }
    }
}
