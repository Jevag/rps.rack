﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update7 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 8;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update7) && !string.IsNullOrWhiteSpace(UpdateResource.Update7))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update7, connect, null);
            }
        }
    }
}
