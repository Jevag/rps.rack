﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update60 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 61;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update60) && !string.IsNullOrWhiteSpace(UpdateResource.Update60))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update60, connect, null);
            }
        }
    }
}
