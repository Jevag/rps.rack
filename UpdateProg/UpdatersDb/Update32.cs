﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update32 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 33;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update32) && !string.IsNullOrWhiteSpace(UpdateResource.Update32))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update32, connect, null);
            }
        }
    }
}
