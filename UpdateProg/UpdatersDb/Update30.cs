﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update30 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 31;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update30) && !string.IsNullOrWhiteSpace(UpdateResource.Update30))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update30, connect, null);
            }
        }
    }
}
