﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update46 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 47;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update46) && !string.IsNullOrWhiteSpace(UpdateResource.Update46))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update46, connect, null);
            }
        }
    }
}
