﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update41 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 42;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update41) && !string.IsNullOrWhiteSpace(UpdateResource.Update41))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update41, connect, null);
            }
        }
    }
}
