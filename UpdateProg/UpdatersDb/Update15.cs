﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update15 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 16;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update15) && !string.IsNullOrWhiteSpace(UpdateResource.Update15))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update15, connect, null);
            }
        }
    }
}
