﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update20 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 21;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update20) && !string.IsNullOrWhiteSpace(UpdateResource.Update20))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update20, connect, null);
            }
        }
    }
}
