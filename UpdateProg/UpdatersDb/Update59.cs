﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update59 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 60;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update59) && !string.IsNullOrWhiteSpace(UpdateResource.Update59))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update59, connect, null);
            }
        }
    }
}
