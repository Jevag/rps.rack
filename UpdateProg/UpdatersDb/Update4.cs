﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update4 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 5;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update4) && !string.IsNullOrWhiteSpace(UpdateResource.Update4))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update4, connect, null);
            }
        }
    }
}
