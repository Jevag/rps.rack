﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update44 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 45;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update44) && !string.IsNullOrWhiteSpace(UpdateResource.Update44))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update44, connect, null);
            }
        }
    }
}
