﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update1 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 2;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update1) && !string.IsNullOrWhiteSpace(UpdateResource.Update1))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update1, connect, null);
            }
        }
    }
}
