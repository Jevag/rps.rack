﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update3 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 4;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update3) && !string.IsNullOrWhiteSpace(UpdateResource.Update3))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update3, connect, null);
            }
        }
    }
}
