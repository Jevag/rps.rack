﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update53 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 54;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update53) && !string.IsNullOrWhiteSpace(UpdateResource.Update53))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update53, connect, null);
            }
        }
    }
}
