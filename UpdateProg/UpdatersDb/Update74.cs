﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update74 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 75;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update74) && !string.IsNullOrWhiteSpace(UpdateResource.Update74))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update74, connect, null);
            }
        }
    }
}
