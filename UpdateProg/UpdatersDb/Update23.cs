﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update23 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 24;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update23) && !string.IsNullOrWhiteSpace(UpdateResource.Update23))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update23, connect, null);
            }
        }
    }
}
