﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update24 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 25;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update24) && !string.IsNullOrWhiteSpace(UpdateResource.Update24))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update24, connect, null);
            }
        }
    }
}
