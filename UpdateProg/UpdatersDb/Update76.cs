﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update76 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 77;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update76) && !string.IsNullOrWhiteSpace(UpdateResource.Update76))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update76, connect, null);
            }
        }
    }
}
