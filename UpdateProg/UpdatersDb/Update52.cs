﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update52 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 53;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update52) && !string.IsNullOrWhiteSpace(UpdateResource.Update52))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update52, connect, null);
            }
        }
    }
}
