﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update73 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 74;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update73) && !string.IsNullOrWhiteSpace(UpdateResource.Update73))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update73, connect, null);
            }
        }
    }
}
