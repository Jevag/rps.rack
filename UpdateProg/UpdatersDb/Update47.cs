﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update47 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 48;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update47) && !string.IsNullOrWhiteSpace(UpdateResource.Update47))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update47, connect, null);
            }
        }
    }
}
