﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update38 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 39;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update38) && !string.IsNullOrWhiteSpace(UpdateResource.Update38))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update38, connect, null);
            }
        }
    }
}
