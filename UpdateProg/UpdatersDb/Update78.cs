﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update78 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 79;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update78) && !string.IsNullOrWhiteSpace(UpdateResource.Update78))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update78, connect, null);
            }
        }
    }
}
