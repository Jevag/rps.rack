﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update25 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 26;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update25) && !string.IsNullOrWhiteSpace(UpdateResource.Update25))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update25, connect, null);
            }
        }
    }
}
