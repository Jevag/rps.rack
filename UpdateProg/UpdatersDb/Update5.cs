﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update5 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 6;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update5) && !string.IsNullOrWhiteSpace(UpdateResource.Update5))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update5, connect, null);
            }
        }
    }
}
