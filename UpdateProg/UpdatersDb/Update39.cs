﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update39 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 40;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update39) && !string.IsNullOrWhiteSpace(UpdateResource.Update39))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update39, connect, null);
            }
        }
    }
}
