﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update0 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 1;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update0) && !string.IsNullOrWhiteSpace(UpdateResource.Update0))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update0, connect, null);
            }
        }
    }
}
