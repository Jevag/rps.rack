﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update8 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 9;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update8) && !string.IsNullOrWhiteSpace(UpdateResource.Update8))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update8, connect, null);
            }
        }
    }
}
