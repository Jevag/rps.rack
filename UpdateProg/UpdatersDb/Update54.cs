﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update54 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 55;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update54) && !string.IsNullOrWhiteSpace(UpdateResource.Update54))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update54, connect, null);
            }
        }
    }
}
