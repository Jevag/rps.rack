﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update13 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 14;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update13) && !string.IsNullOrWhiteSpace(UpdateResource.Update13))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update13, connect, null);
            }
        }
    }
}
