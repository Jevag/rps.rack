﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update70 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 71;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update70) && !string.IsNullOrWhiteSpace(UpdateResource.Update70))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update70, connect, null);
            }
        }
    }
}
