﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update49 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 50;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update49) && !string.IsNullOrWhiteSpace(UpdateResource.Update49))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update49, connect, null);
            }
        }
    }
}
