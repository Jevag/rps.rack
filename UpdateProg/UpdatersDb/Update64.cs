﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update64 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 65;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update64) && !string.IsNullOrWhiteSpace(UpdateResource.Update64))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update64, connect, null);
            }
        }
    }
}
