﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg.UpdatersDb
{
    public class Update72 : IUpdateDb_old
    {
        public int EndVersion
        {
            get
            {
                return 73;
            }
        }

        public void Update(SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(UpdateResource.Update72) && !string.IsNullOrWhiteSpace(UpdateResource.Update72))
            {
                UtilsBase.ExecuteNonQuery(UpdateResource.Update72, connect, null);
            }
        }
    }
}
