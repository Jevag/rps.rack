﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateProg
{
    public interface IUpdateDb_old
    {
        int EndVersion { get; }
        void Update(SqlConnection connect);
    }

    public class UpdateDbEngine_old
    {
        public string NameTable = "DbVersion";
        public string NameField = "[CurrentDb]";
        private SortedList<int, IUpdateDb_old> sortUpdates = new SortedList<int, IUpdateDb_old>();

        public void Register(IUpdateDb_old updater)
        {
            if (!sortUpdates.ContainsKey(updater.EndVersion))
            {
                sortUpdates.Add(updater.EndVersion, updater);
            }
        }

        public void FillUpdater()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            if (assemblies != null && assemblies.Length > 0)
            {
                foreach (var assem in assemblies)
                {
                    var types = assem.GetTypes();
                    if (types != null && types.Length > 0)
                    {
                        foreach (Type tp in types)
                        {
                            var res = tp.GetInterface(typeof(IUpdateDb_old).Name, true);
                            if (res != null)
                            {
                                try
                                {
                                    IUpdateDb_old updater = Activator.CreateInstance(tp) as IUpdateDb_old;
                                    if (updater != null)
                                    {
                                        this.Register(updater);
                                    }
                                }
                                catch { }
                            }
                        }
                    }
                }
            }
        }

        public int GetCurrentDbVersion(SqlConnection connect)
        {
            int result = 0;
            string qr = UtilsBase.MergeString(Sql.Sel, " Top 1 ", NameField, Sql.from, NameTable);
            object res = UtilsBase.ExecuteScalar(qr, connect, null);
            if (res != null && res != System.DBNull.Value)
            {
                try
                {
                    result = System.Convert.ToInt32(res);
                }
                catch { }
            }
            return result;
        }

        public void SetDbVersion(int version, SqlConnection connect)
        {
            UtilsBase.SetOneRow(NameTable, NameField, connect, null, NameField, version);
        }

        public void UpdateToCurrent(SqlConnection connect)
        {
            try
            {
                string zap = "select top 1 DbVer from SystemVersion where IsUpdate=@up";
                var res = UtilsBase.ExecuteScalar(zap, connect, null, "@up", true);
                if (res != null && res != System.DBNull.Value)
                {
                    int ver = System.Convert.ToInt32(res);
                    UpdateToVersion(connect, ver);
                }
            }
            catch(Exception ex)
            {
                SaveErrorDevice(connect, ex.Message);
            }
        }

        public void SaveErrorDevice(SqlConnection connect,string message)
        {
            string selcount = "select count(*) from DeviceModel";
            var findcount = UtilsBase.ExecuteScalar(selcount, connect,null);
            if (findcount != null && findcount != System.DBNull.Value)
            {
                int cn = 0;
                Int32.TryParse(findcount.ToString(), out cn);
                if (cn == 1)
                {
                    string seldev = "select top 1 Id from DeviceModel";
                    var findobj = UtilsBase.ExecuteScalar(seldev, connect, null);
                    if (findobj != null && findobj != System.DBNull.Value)
                    {
                        Guid gd = (Guid)findobj;
                        UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 3, "Error", message },
                            "Status<=2 and DeviceId=@id", new object[] { "@id", gd }, connect);
                    }
                }
            }
        }

        public void UpdateToVersion(SqlConnection connect, int ToVersion)
        {
            int curversion = GetCurrentDbVersion(connect);
            var updater = from t in sortUpdates.Keys where t > curversion && t <= ToVersion orderby t ascending select t;
            foreach (int vers in updater)
            {
                sortUpdates[vers].Update(connect);
                SetDbVersion(vers, connect);
            }

        }
    }
}
