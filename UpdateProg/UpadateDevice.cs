﻿using Rps.ShareBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UpdateProg
{
    public class UpdateDevice
    {
        private Guid deviceid;
        private int curver = 0;
        private TypeDevice typedev;
        private Action actrestart;
        private SqlConnection connect;
        private int waitsec = 60;
        private string CashierSystemVersion = "CMVer";
        private string RackSystemVersion = "RackVer";
        private InfoDownload infoupdatedevice = null;
        private InfoDownload infoupdateDb = null;
        private int newversion=0;
        private int newsysver = 0;
        private Guid keyupdateprog;
        private Guid idupdatedb;
        private string PathUpdater;
        private string PathUpdateDB;
        InfoDownload infoupd = null;
        string ConnectionString = string.Empty;
        public bool NeedRestart { get; private set; }

        public static UpdateDevice Instance { get; private set; }

        public void FillFtpLevel2(InfoDownload info)
        {
            string seldata = "select * from Setting where Name in('FTP2Host','FTP2Password','FTP2Port','FTP2User')";
            using (var table = UtilsBase.FillTable(seldata, connect, null))
            {
                if (table != null && table.Rows != null && table.Rows.Count > 0)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        string name = row.GetString("Name");
                        if(!string.IsNullOrEmpty(name) && !string.IsNullOrWhiteSpace(name))
                        {
                            string value = row.GetString("Value");
                            if(!string.IsNullOrEmpty(value) && !string.IsNullOrWhiteSpace(value))
                            {
                                if (name.Trim().ToLower().Equals("ftp2host")) { info.ftp2 = "ftp://"+value.Trim()+"/"; }
                                if (name.Trim().ToLower().Equals("ftp2password"))
                                {
                                    var decryp = new Rps.Crypto.CryptoDecrypto();
                                    info.pwdftp2 = decryp.Decrypto(value.Trim());
                                }
                                if (name.Trim().ToLower().Equals("ftp2port")) { string port = value.Trim(); }
                                if (name.Trim().ToLower().Equals("ftp2user"))
                                {
                                    info.userftp2 =value.Trim();
                                }
                            }
                        }
                    }
                }
            }


        }

        public UpdateDevice(Guid Deviceid,int Curversion,TypeDevice Typedevice,SqlConnection Connect)
        {
            this.NeedRestart = false;
            this.deviceid = Deviceid;
            this.curver = Curversion;
            this.typedev = Typedevice;
            this.connect = Connect;
            infoupd = InfoDownload.CreateUpdate(1);
            FillFtpLevel2(infoupd);
            this.PathUpdater = System.IO.Path.Combine(infoupd.NewVersionFolder(), "Updater.exe");
            this.PathUpdateDB = @"C:\RPS\UpdateDB";
        }

        public void SetRestartAction(Action Actrestart)
        {
            this.actrestart = Actrestart;
        }

        public static void StartThreadAction(Guid Deviceid, int Curversion, TypeDevice Typedevice,Action actrestart,string connectstr)
        {

            Task ts = new Task(()=> 
            {
                using (SqlConnection connect = new SqlConnection(connectstr))
                {
                    connect.Open();
                    UpdateDevice upd = new UpdateDevice(Deviceid, Curversion, Typedevice, connect);
                    upd.ConnectionString = connectstr;
                    UpdateDevice.Instance = upd;
                    if (actrestart != null)
                    {
                        upd.SetRestartAction(actrestart);
                    }
                    upd.Start();
                }
            });
            ts.Start();
        }

        public void Start()
        {
            bool flag = true;
            while (flag)
            {
                try
                {
                    if (ValidateNeedUpdate())
                    {
                        if (infoupdatedevice != null)
                        {
                            flag = false;
                            
                        }
                        if (flag)
                        {
                            if (infoupdateDb != null)
                            {
                                //Здесь же обновляем БД
                                WiatDownloadDBProg();
                                UpdateDb();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string aa = ex.Message;
                }
                if (flag)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(waitsec));
                }
            }

            #region ValidateProgramm
            if (infoupdatedevice != null)
            {
                flag = true;
                while (flag)
                {
                    bool find = false;

                    //1 Пооверяем есть ли локально директория если есть то устанваливаем статус 2 и флаг найдена
                    //выходим из ожидания и переходим в ожидание перезпуска

                    if (infoupdatedevice.HasLocalFolder(newversion))
                    {
                        find = true;
                        flag = false;
                        FindKeyUpdateProgramm();
                        UtilsBase.SetOne("UpdateProgramm", "Id", new object[] {"Id",keyupdateprog,"SysVersion",newsysver,
                            "VersionProgramm",newversion,"Status",2,"DeviceId",this.deviceid,"TypeProgramm",typedev.ToString()},
                        "SysVersion=" + newsysver + " and DeviceId=@devid and TypeProgramm='" + typedev.ToString() + "'",
                        new object[] { "@devid", this.deviceid }, connect);
                    }

                    //2 Проверяем есть файл архива локально если есть флаг найден и распаковываем ахив
                    if (!find && infoupdatedevice.HasLocalFile(newversion))
                    {
                        find = true;
                        //Распаковываем архив
                        infoupdatedevice.UnZipLocalFile(newversion);
                        infoupd.UnZipLocalFile(newversion);
                    }

                    //3 Проверяем есть ли на 2 уровне нужный файл если есть то скачиваем и флаг найден в true
                    if (!find && infoupdatedevice.HasNetSourFolder(newversion))
                    {
                        FindKeyUpdateProgramm();
                        UtilsBase.SetOne("UpdateProgramm", "Id", new object[] {"Id",keyupdateprog,"SysVersion",newsysver,
                            "VersionProgramm",newversion,"Status",1,"DeviceId",this.deviceid,"TypeProgramm",typedev.ToString()},
                        "SysVersion=" + newsysver + " and DeviceId=@devid and TypeProgramm='" + typedev.ToString() + "'",
                        new object[] { "@devid", this.deviceid }, connect);

                        find = true;
                        infoupdatedevice.CopyToLocal(newversion);
                        infoupd.CopyToLocal(newversion);
                    }

                    if (!find && flag)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(waitsec));
                    }
                }
            }
            #endregion ValidateProgramm

            WiatDownloadDBProg();
            WaitRestart();

        }

        private void FindKeyUpdateProgramm()
        {
            string findhas = "select top 1 Id from UpdateProgramm where TypeProgramm!='Db' and SysVersion=" + newsysver + " and DeviceId=@devid";
            var row = UtilsBase.FillRow(findhas, connect, null, new object[] { "@devid", this.deviceid });
            if (row != null)
            {
                keyupdateprog = (Guid)row["Id"];
            }
            else
            {
                keyupdateprog = Guid.NewGuid();
            }
        }

        private void WiatDownloadDBProg()
        {
            #region ValidateDb
            if (infoupdateDb != null)
            {
                bool flag = true;
                while (flag)
                {
                    if (infoupdateDb.HasNetSourFolder(newversion))
                    {
                        flag = false;
                        infoupdateDb.CopyToLocal(newversion);
                        infoupdateDb.UnZipLocalFile(newversion);

                        string findhas = "select top 1 Id from UpdateProgramm where TypeProgramm='Db' and SysVersion=" + newsysver + " and DeviceId=@devid";
                        var row = UtilsBase.FillRow(findhas, connect, null, new object[] { "@devid", this.deviceid });
                        if (row != null)
                        {
                            idupdatedb = (Guid)row["Id"];
                        }
                        else
                        {
                            idupdatedb = Guid.NewGuid();
                        }
                        UtilsBase.SetOne("UpdateProgramm", "Id", new object[] {"Id",idupdatedb,"SysVersion",newsysver,
                            "VersionProgramm",newversion,"Status",2,"DeviceId",this.deviceid,"TypeProgramm","Db"},
                        "SysVersion=" + newsysver + " and DeviceId=@devid and TypeProgramm='Db'", new object[] { "@devid", this.deviceid }, connect);
                    }

                    if (flag)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(waitsec));
                    }
                }
            }
            #endregion ValidateDb
        }

        private void WaitRestart()
        {
            bool flag = true;
            while (flag)
            {
                try
                {
                    string zap = "select top 1 * from CommandDevice where DeviceId=@devid and Status=0 and Command like '%RestartUpdate%'";
                    var row = UtilsBase.FillRow(zap, connect, null, "@devid", deviceid);
                    if (row != null)
                    {
                        flag = false;
                    }
                }
                catch (Exception ex)
                {

                }
                if (flag)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(waitsec));
                }
            }
            if (!flag)
            {
                UtilsBase.UpdateCommand("CommandDevice", new object[] { "Status", 1 }, "DeviceId=@devid and Status=0 and Command like '%RestartUpdate%'",
                            new object[] { "@devid", deviceid }, connect);
                if (actrestart != null)
                {
                    actrestart();
                }
                this.NeedRestart = true;
            }
        }

        public bool ValidateNeedUpdate()
        {
            bool result = false;
            DataRow infoverrow = null;
            string command = "select top 1 * from SystemVersion " +
                            " where Status = " + StatSystemVersion.МожноСкачивать.ToString();

            infoverrow = UtilsBase.FillRow(command, connect, null);
           
            if (infoverrow != null)
            {
                #region UpdateProgramm
                //Сверяем версии в SystemVersion с текущей версией
                switch (typedev)
                {
                    case TypeDevice.Cashier:
                        {
                            newversion = ConvertField.GetInt(infoverrow, CashierSystemVersion).Value;
                            break;
                        }
                    case TypeDevice.Rack:
                        {
                            newversion = ConvertField.GetInt(infoverrow, RackSystemVersion).Value;
                            break;
                        }
                }
                if(newversion > curver)
                {
                    newsysver = ConvertField.GetInt(infoverrow, "SysVersion").Value;
                    switch (typedev)
                    {
                        case TypeDevice.Cashier:
                            {
                                infoupdatedevice = InfoDownload.CreateCash(newversion, 1);
                                FillFtpLevel2(infoupdatedevice);
                                result = true;
                                break;
                            }
                        case TypeDevice.Rack:
                            {
                                infoupdatedevice = InfoDownload.CreateStoika(newversion, 1);
                                FillFtpLevel2(infoupdatedevice);
                                result = true;
                                break;
                            }
                    }
                }
                #endregion UpdateProgramm
                #region UpdateDB
                var curdb=Rps.ShareBase.UpdateDbEngine.GetCurrentDbVersion(this.connect);
                if(curdb!=null && curdb.Packet<ConvertField.GetInt(infoverrow,"DbVer").Value)
                {
                    infoupdateDb = InfoDownload.CreateDb(1);
                    FillFtpLevel2(infoupdateDb);
                }

                #endregion UpdateDB
            }

            return result;
        }

        public bool RunRestartProgramm()
        {
            bool isclose = true;
            try
            {
                ProcessStartInfo proc = new ProcessStartInfo();
                proc.UseShellExecute = true;
                proc.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
                proc.FileName = PathUpdater;
                //string nameexe = "RunUpdateCash.exe";

                string nameexe = System.IO.Path.GetFileName(AppDomain.CurrentDomain.FriendlyName);

                string oldfolder = AppDomain.CurrentDomain.BaseDirectory;

                string newfolder = infoupdatedevice.NewVersionFolder();

                if (oldfolder[oldfolder.Length - 1] == '\\')
                {
                    oldfolder = oldfolder.Remove(oldfolder.Length - 1, 1);
                }

                if (newfolder[newfolder.Length - 1] == '\\')
                {
                    newfolder = newfolder.Remove(newfolder.Length - 1, 1);
                }
                //яTODO: Задана Id для тестирования
                //deviceupdateid = Guid.Parse("099e71b5-765d-4a13-b8bc-2beabde2e645");


                proc.Arguments = keyupdateprog.ToString() + " \"" + nameexe + "\" \"" + oldfolder + "\" \"" + newfolder + "\"";
                proc.Verb = "runas";
                var resproc = Process.Start(proc);
                if (resproc == null)
                {
                    isclose = false;
                    using (SqlConnection con = new SqlConnection(ConnectionString))
                    {
                        con.Open();
                        UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 3, "Error", "Не удалось запустить" }, "Id=@id", new object[] { "@id", keyupdateprog }, connect);
                        con.Close();
                    }
                }
                isclose = true;
            }
            catch (Exception ex)
            {
                isclose = false;
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 3, "Error", ex.Message }, "Id=@id", new object[] { "@id", keyupdateprog }, connect);
                    con.Close();
                }
            }
            return isclose;
        }

        public bool UpdateDb()
        {
            bool isclose = true;
            if (infoupdateDb != null)
            {
                try
                {
                    ProcessStartInfo proc = new ProcessStartInfo();
                    proc.UseShellExecute = true;
                    proc.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    string nameexe = "Rps.UpdateDB.exe";
                    proc.FileName = System.IO.Path.Combine(this.PathUpdateDB,nameexe);
                    //тофорTODO: Задана Id для тестирования
                    //deviceupdateid = Guid.Parse("099e71b5-765d-4a13-b8bc-2beabde2e645");

                    proc.Arguments = idupdatedb.ToString();
                    proc.Verb = "runas";
                    var resproc = Process.Start(proc);
                    if (resproc == null)
                    {
                        isclose = false;
                        using (SqlConnection con = new SqlConnection(ConnectionString))
                        {
                            con.Open();
                            UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 3, "Error", "Не удалось запустить" }, "Id=@id", new object[] { "@id", idupdatedb }, connect);
                            con.Close();
                        }
                    }
                    else
                    {

                        string zap = "select top 1 Status from UpdateProgramm where Id=@id";
                        bool flag = true;
                        while (flag)
                        {
                            try
                            {
                                DataRow row = null;
                                using (SqlConnection con = new SqlConnection(ConnectionString))
                                {
                                    con.Open();
                                    row = UtilsBase.FillRow(zap, con, null, "@id", idupdatedb);
                                    con.Close();
                                }
                                if (row != null)
                                {
                                    int stat = row.GetInt("Status").Value;
                                    if (stat > 2)
                                    {
                                        flag = false;
                                        if (stat == 3) { isclose = false; }
                                        if (stat == 4) { isclose = true; }
                                    }
                                }
                            }
                            catch { }
                            if (flag)
                            {
                                Thread.Sleep(TimeSpan.FromSeconds(waitsec));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    isclose = false;
                    using (SqlConnection con = new SqlConnection(ConnectionString))
                    {
                        con.Open();
                        UtilsBase.UpdateCommand("UpdateProgramm", new object[] { "Status", 3, "Error", ex.Message }, "Id=@id", new object[] { "@id", idupdatedb }, connect);
                        con.Close();
                    }
                }
            }
            return isclose;
        }
        
    }

    public enum TypeDevice
    {
        Cashier=0,
        Rack=1
    }
}
