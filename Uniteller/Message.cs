﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class Message
    {
        #region commands
        public static string CMD_SALE = "10";
        public static string CMD_REVERSAL_SALE_BY_RRN = "19";
        public static string CMD_REVERSAL_SALE_BY_ORDER_ID = "1A";
        public static string CMD_GET_REPORT = "21";
        public static string CMD_GET_REPORT_RESPONSE = "22";
        public static string CMD_LOGIN = "30";
        public static string CMD_LOGIN_RESPONSE = "31";
        public static string CMD_PRINT_LINE = "32";
        public static string CMD_BREAK = "33";
        public static string CMD_BREAK_RESPONSE = "34";
        public static string CMD_DEVICE_CONTROL = "35";
        public static string CMD_DEVICE_EVENT = "36";
        public static string CMD_CARD_UUID = "37";
        public static string CMD_CARD_UUID_RESPONSE = "38";
        public static string CMD_INITIAL_RESPONSE = "50";
        public static string CMD_PIN_ENTRY_REQUIRED = "52";
        public static string CMD_ONLINE_REQUIRED = "53";
        public static string CMD_ERROR_RESPONSE = "5X";
        public static string CMD_AUTH_HOST_RESPONSE = "60";
        public static string CMD_GET_STATE = "80";
        public static string CMD_GET_STATE_RESPONSE = "81";
        #endregion

        #region response_codes
        public static string RC_SUCCESS = "00";

        public static string RC_INCORRECT_MESSAGE_FORMAT = "01";

        public static string RC_PREFIX_0B = "0B";
        public static string RC_CARD_READER_HARDWARE_ERROR = "0B00";

        #endregion

        public static string DEVICE_ID_PIN_PAD = "PP";
        public static string DEVICE_ID_CARD_READER = "CR";

        public static string EVENT_ID_PRESS = "PE";
        public static string EVENT_ID_CARD_INSERTED = "CI";
        public static string EVENT_ID_CARD_CAPTURED = "CC";
        public static string EVENT_ID_CARD_OUT = "CO";

        public static string EVENT_CODE_PRESS_TIMEOUT = "T";
        public static string EVENT_CODE_PRESS_NUMERIC = "N";
        public static string EVENT_CODE_PRESS_CLEAR = "B";
        public static string EVENT_CODE_PRESS_CANCEL = "C";
        public static string EVENT_CODE_PRESS_ENTER = "D";

        public class DataType
        {
            public enum DType
            {
                UNKNOWN,
                NUMERIC,
                CHAR
            };

            public DType Type { get; set; } = DType.UNKNOWN;
            public int Length { get; set; } = 0;
            public bool IsFixed { get; set; } = true;

            public DataType(DType type, int len, bool isFixed = true)
            {
                this.Type = type;
                this.Length = len;
                this.IsFixed = isFixed;
            }
        }

        public static DataType DT_AMOUNT = new DataType(DataType.DType.NUMERIC, 12);
        public static DataType DT_RRN = new DataType(DataType.DType.NUMERIC, 12);
        public static DataType DT_REPORT_TYPE = new DataType(DataType.DType.CHAR, 1);

        public static string REPORT_TYPE_WITHOUT_CLOSE = "0";
        public static string REPORT_TYPE_WITH_CLOSE = "1";

        public class HostData
        {
            public string OperationType { get; set; } = string.Empty;
            public uint TransactionSum { get; set; } = 0;
            public string TransactionCurrency { get; set; } = string.Empty;
            public string TransactionDate { get; set; } = string.Empty;
            public string TransactionTime { get; set; } = string.Empty;
            public string MerchantID { get; set; } = string.Empty;
            public string RRN { get; set; } = string.Empty;
            public string ResponseCode { get; set; } = string.Empty;
            public string ConfirmationCode { get; set; } = string.Empty;
            public string CardNumber { get; set; } = string.Empty;
            public string CardLabel { get; set; } = string.Empty;
            public string Message { get; set; } = string.Empty;

            public string ToPrintString()
            {
                string message = string.Empty;

                message += "OPT = " + OperationType + Environment.NewLine;
                message += "SUM = " + TransactionSum + Environment.NewLine;
                message += "CUR = " + TransactionCurrency + Environment.NewLine;
                message += "DT  = " + TransactionDate + Environment.NewLine;
                message += "TM  = " + TransactionTime + Environment.NewLine;
                message += "MID = " + MerchantID + Environment.NewLine;
                message += "RRN = " + RRN + Environment.NewLine;
                message += "RC  = " + ResponseCode + Environment.NewLine;
                message += "CC  = " + ConfirmationCode + Environment.NewLine;
                message += "PAN = " + CardNumber + Environment.NewLine;
                message += "CL  = " + CardLabel + Environment.NewLine;
                message += "MES = " + Message + Environment.NewLine;

                return message;
            }
        }

        public string Command { get; set; }
        public string TerminalID { get; set; }
        public int LengthData { get; set; }
        public List<Tuple<DataType, string>> Data { get; set; } = null;
        public string RawData { get; set; } = string.Empty;
        public string ErrorCode { get; set; } = string.Empty;
        public string ErrorMessage { get; set; } = string.Empty;
        public string LastLineFlag { get; set; } = string.Empty;
        public string TextLine { get; set; } = string.Empty;
        public string LastDevice { get; set; } = string.Empty;
        public string StatusCode { get; set; } = string.Empty;
        public string DeviceID { get; set; } = string.Empty;
        public string EventID { get; set; } = string.Empty;
        public string Params { get; set; } = string.Empty;
        public HostData HostResponse { get; set; } = null;
        public string BreakResult { get; set; } = string.Empty;

        public Message(string command, string terminalID)
        {
            this.Command = command;
            this.TerminalID = terminalID.PadLeft(10, '0');
            this.LengthData = 0;
            this.Data = null;
        }
        public Message(string command, string terminalID, List<Tuple<DataType, string>> data)
        {
            this.Command = command;
            this.TerminalID = terminalID.PadLeft(10, '0');

            int lendata = 0;
            foreach (Tuple<DataType, string> d in data)
            {
                DataType dt = d.Item1;
                lendata += dt.Length;
            }

            this.LengthData = lendata;
            this.Data = data;
        }
        public Message(string command, string terminalID, int lenData, string rawData)
        {
            this.Command = command;
            this.TerminalID = terminalID.PadLeft(10, '0');

            this.LengthData = lenData;
            this.RawData = rawData;
        }

        public static Message Parse(string sMessage)
        {
            string command = sMessage.Substring(0, 2);

            //Console.WriteLine(command);

            string terminalID = sMessage.Substring(2, 10);

            //Console.WriteLine(terminalID);

            string slen = sMessage.Substring(12, 2);
            int len = Convert.ToInt32(slen, 16);

            //Console.WriteLine(len);

            string sdata = string.Empty;
            if (len > 0)
                sdata = sMessage.Substring(14, len);

            Message message = new Message(command, terminalID, len, sdata);

            if (command == Message.CMD_ERROR_RESPONSE)
            {
                message.ErrorCode = sdata.Substring(0, 2);
                message.ErrorMessage = sdata.Substring(2, sdata.Length - 2);
            }
            else if (command == Message.CMD_PRINT_LINE)
            {
                message.LastLineFlag = sdata.Substring(0, 1);
                message.TextLine = sdata.Substring(1, sdata.Length - 1).Replace("\r", String.Empty);
            }
            else if (command == Message.CMD_GET_STATE_RESPONSE)
            {
                message.LastDevice = sdata.Substring(0, 1);
                message.StatusCode = sdata.Substring(1, 2);
            }
            else if (command == Message.CMD_DEVICE_EVENT)
            {
                message.DeviceID = sdata.Substring(0, 2);
                message.EventID = sdata.Substring(2, 2);
                message.Params = sdata.Substring(4, sdata.Length - 4);
            }
            else if (command == Message.CMD_AUTH_HOST_RESPONSE)
            {
                try
                {
                    message.HostResponse = new HostData
                    {
                        OperationType = sdata.Substring(0, 1),
                        TransactionSum = UInt32.Parse(sdata.Substring(1, 12)),
                        TransactionCurrency = sdata.Substring(13, 3),
                        TransactionDate = sdata.Substring(16, 8),
                        TransactionTime = sdata.Substring(24, 6),
                        MerchantID = sdata.Substring(30, 15),
                        RRN = sdata.Substring(45, 12),
                        ResponseCode = sdata.Substring(57, 2)
                    };
                    try
                    {
                        string tmp = sdata.Substring(59, sdata.Length - 59);
                        string[] tmps = tmp.Split((char)0x1B);

                        message.HostResponse.ConfirmationCode = tmps[0];
                        message.HostResponse.CardNumber = tmps[1];
                        message.HostResponse.CardLabel = tmps[2];
                        message.HostResponse.Message = tmps[3];

                        //Console.WriteLine(tmp);
                    }
                    catch { }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }

            }
            else if (command == Message.CMD_BREAK_RESPONSE)
            {
                message.BreakResult = sdata.Substring(0, 1);
            }

            return message;
        }

        public override string ToString()
        {
            string message = string.Empty;

            message += Command;
            message += TerminalID;
            message += LengthData.ToString("X2");

            if (Data != null && Data.Count > 0)
            {
                int n = Data.Count;
                for (int i = 0; i < n; ++i)
                    message += Data[i].Item2.PadLeft(Data[i].Item1.Length, '0');
            }

            return message;
        }

        public string ToRawString()
        {
            string message = string.Empty;

            message += Command;
            message += TerminalID;
            message += LengthData.ToString("X2");
            message += RawData;

            return message;
        }
        public string ToPrintString()
        {
            string message = string.Empty;

            message += "CMD = " + Command + Environment.NewLine;
            message += "TID = " + TerminalID + Environment.NewLine;
            message += "LEN = " + LengthData.ToString("X2") + Environment.NewLine;
            message += "RAW = " + RawData;

            if (ErrorCode != string.Empty)
                message += Environment.NewLine + "ERC = " + ErrorCode;

            if (ErrorMessage != string.Empty)
                message += Environment.NewLine + "ERM = " + ErrorMessage;

            if (HostResponse != null)
                message += Environment.NewLine + HostResponse.ToPrintString();

            return message;
        }


    }
}
