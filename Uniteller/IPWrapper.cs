﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Communications
{
    public class IPWrapper
    {
        private IPAddress ipAddress = null;
        private IPEndPoint remoteEP = null;
        private Socket client = null;
        private int RECIEVE_TIMEOUT = 40000;
        private Queue<Message> receivedMessages = new Queue<Message>();
        public bool isConnected { get; set; } = false;

        public IPWrapper(string ip, int port)
        {
            try
            {
                // Establish the remote endpoint for the socket.  
                ipAddress = IPAddress.Parse(ip);
                remoteEP = new IPEndPoint(ipAddress, port);

                // Create a TCP/IP socket.  
                client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                client.NoDelay = true;
                client.ReceiveTimeout = RECIEVE_TIMEOUT;

                //Console.WriteLine("Is connected? = " + client.Connected);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        ~IPWrapper()
        {
            // Release the socket.  
            if (client != null)
            {
                if (client.Connected)
                    client.Shutdown(SocketShutdown.Both);
                client.Close();
            }
            //Console.WriteLine("~IPWrapper()");
        }
        public bool Open()
        {
            // Connect to the remote endpoint.            
            if (client == null)
            {
                client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                //client.NoDelay = true;
                client.ReceiveTimeout = RECIEVE_TIMEOUT;
            }

            client.Connect(remoteEP);
            isConnected = client.Connected;

            Console.WriteLine("OPEN: Is connected? = " + client.Connected);

            return isConnected;
        }
        public void Close()
        {
            // Clear queue
            receivedMessages.Clear();

            // Release the socket.  
            if (client != null)
            {
                if (client.Connected)
                    client.Shutdown(SocketShutdown.Both);
                client.Close();
            }

            Console.WriteLine("CLOSE: Is connected? = " + client.Connected);

            client = null;
        }
        public bool Send(Message request)
        {
            string message = string.Empty;

            try
            {
                message = request.ToString();

                byte[] cmd = Encoding.ASCII.GetBytes(message);
                int b = client.Send(cmd, 0, cmd.Length, 0);

                Console.WriteLine($"[{DateTime.Now.ToString("HH.mm.ss.ffffff")}] SND [{b.ToString("000")}] [{b.ToString("000")}] --> {message}");

                return (b > 0 ? true : false);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);

                throw e;
            }
        }
        public Message Receive()
        {
            //RCV[077][035] <--   320000026496150TC: 11D5659D95B3688E3200000264960A1OrderId: 36000002649604CRCO
            //string test = "320000026496150TC: 11D5659D95B3688E3200000264960A1OrderId: 36000002649604CRCO";
            //test += test;

            Message response = null;
            string message = string.Empty;

            try
            {
                if (receivedMessages.Count > 0)
                {
                    response = receivedMessages.Dequeue();
                    Console.WriteLine($"[{DateTime.Now.ToString("HH.mm.ss.ffffff")}] RCV [---] [---] <-- {response.ToRawString()}");
                }
                else
                {
                    int n = 0;
                    int lenMes = 0;
                    int offset = 0;
                    byte[] buffer = null;
                    byte[] data = null;

                    // Get data from socket
                    buffer = new byte[4096];
                    n = client.Receive(buffer);
                    //buffer = Encoding.Default.GetBytes(test);
                    //n = buffer.Length;

                    data = new byte[n];
                    Buffer.BlockCopy(buffer, offset, data, 0, n);

                    message = Encoding.Default.GetString(data);
                    response = Message.Parse(message);
                    lenMes = 14 + response.LengthData;
                    Console.WriteLine($"[{DateTime.Now.ToString("HH.mm.ss.ffffff")}] RCV [{n.ToString("000")}] [{lenMes.ToString("000")}] <-- {message}");

                    // When data store only one message 
                    if (n <= lenMes)
                    {
                        PrintQueue();
                        return response;
                    }

                    // When data store few message 

                    // Get first message
                    data = new byte[lenMes];
                    Buffer.BlockCopy(buffer, offset, data, 0, lenMes);
                    message = Encoding.Default.GetString(data);
                    Console.WriteLine(message);
                    response = Message.Parse(message);
                    lenMes = 14 + response.LengthData;

                    // Get other messages
                    while (n > lenMes)
                    {
                        n -= lenMes;
                        offset += lenMes;

                        // Get lenMes for message
                        data = new byte[n];
                        Buffer.BlockCopy(buffer, offset, data, 0, n);
                        message = Encoding.Default.GetString(data);
                        Console.WriteLine(message);
                        Message tmp = Message.Parse(message);
                        lenMes = 14 + tmp.LengthData;

                        // Push message to queue
                        data = new byte[lenMes];
                        Buffer.BlockCopy(buffer, offset, data, 0, lenMes);
                        message = Encoding.Default.GetString(data);
                        Console.WriteLine(message);
                        Message oneMore = Message.Parse(message);
                        lenMes = 14 + oneMore.LengthData;

                        receivedMessages.Enqueue(oneMore);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);

                throw e;
            }
            PrintQueue();
            return response;
        }
        private void PrintQueue()
        {
            int n = receivedMessages.Count;
            for (int i = 0; i < n; ++i)
                Console.WriteLine($"{i + 1}/{n} {receivedMessages.ToArray()[i].ToRawString()}");
        }

    }
}
