﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Communications
{
    public class Uniteller : CommonBankModule
    {
        static string sStatusBeginOperation = "Пожалуйста, воспользуйтесь банковской картой или наличными";
        static string sStatusCardInserted = "Банковская карта обнаружена";
        static string sStatusPINRequired = "Введите ПИН: _ _ _ _";
        static string sStatusPINRequired1D = "Введите ПИН: * _ _ _";
        static string sStatusPINRequired2D = "Введите ПИН: * * _ _";
        static string sStatusPINRequired3D = "Введите ПИН: * * * _";
        static string sStatusPINRequired4D = "Введите ПИН: * * * *";
        static string sStatusAuth = "Связь с банком-эмитентом";
        static string sStatusCardRemoved = "Банковская карта забрана";
        static string sStatusAbortOperation = "Отказ от оплаты картой";
        static string sStatusOK = "Операция выполнена успешно";
        static string sStatusError = "Ошибка выполнения операции";

        static int nPINDigits = 0;

        string sLastRRN = "";
        uint sLastAmount = 0;

        enum BankModuleCommandT
        {
            Unknown,
            Login,
            Purchase,
            Cancell,
            Verification
        }

        private IPWrapper ipwrap = null;
        private string TID = string.Empty;
        private string ip = string.Empty;
        private int port = 0;

        BankModuleCommandT bankModuleCommand = BankModuleCommandT.Unknown;
        private static Thread StatusThread = null;
        private static bool run = false;

        private static DateTime dtStartVerification = DateTime.MinValue;
        private static DateTime dtStartTechnicalCancelation = DateTime.MinValue;

        private static bool isOpened = false;

        private static string VERSION = "EFTPOS 3.0";

        private string sLastAutoVerifactionLog = "";

        private void releaseAuthSession()
        {
            nPINDigits = 0;
            isCancelled = false;
            AuthorizationStarted = false;
            TransactionStarted = false;
            cardInserted = false;
            cardWasInserted = false;
            AuthResult = string.Empty;
            AuthReceipt = string.Empty;
            StopSession();
            toLog("DONE");
        }

        public Uniteller(Log Log, string ip, int port, string TID)
        {
            this.log = Log;
            this.type = BM_TYPE.UNITELLER;
            this.TID = TID;
            this.ip = ip;
            this.port = port;
            this.ipwrap = new IPWrapper(ip, port);

            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);

            toLog("");
            toLog("Session UNITELLER start");
            toLog("");
            toLog($"VERSION DLL = {fvi.FileVersion}");
            toLog("");
            toLog("");

        }
        ~Uniteller()
        {
            StopSession();
            ipwrap = null;

            toLog("");
            toLog("Session UNITELLER finish");
            toLog("");
            toLog("");
            toLog("");
        }

        ResponseCode SetResultCode(bool Ret)
        {
            if (Ret)
                return ResponseCode.Sucsess;

            return ResponseCode.SysytemError;
        }

        ResponseCode SetResultCode(string Ret)
        {
            string rc = Ret.Substring(0, 2);

            if (rc == Message.RC_SUCCESS)
                return ResponseCode.Sucsess;

            int nrc;
            bool isNumeric = int.TryParse(rc, out nrc);

            if (isNumeric)
            {
                if (nrc == 0)
                    return ResponseCode.Sucsess;
                else if (nrc >= 1 && nrc <= 99)
                    return ResponseCode.UserError;
            }
            else
            {
                if (rc == Message.RC_PREFIX_0B)
                    return ResponseCode.SysytemError;
            }

            return ResponseCode.SysytemError;

        }

        public override bool Open()
        {
            toLog("Start");
            toLog("isOpened = " + isOpened);

            resultCodeU = ResponseCode.OtherError;
            resultDescription = "";
            deviceEnabled = true;
            BMStatus = BM_STATUS.WAITING_COMMAND;

            if (isOpened)
            {
                toLog("Already opened...");
                run = true;
                toLog("Stop; return always true");

                return true;
            }

            if (StatusThread != null)
            {
                System.Threading.ThreadState tsct = StatusThread.ThreadState;
                toLog("ThreadState Current Thread = " + tsct.ToString());
            }

            try
            {
                versionDescription = VERSION;

                bool bres = StartSession();
                if (!bres)
                {
                    toLog("Stop; Start session is failed");
                    return false;
                }

                //if (StatusThread == null)
                {
                    StatusThread = new Thread(PoolThread);
                    StatusThread.Name = "BankModeuleStatusThread";
                    StatusThread.IsBackground = true;
                }
                System.Threading.ThreadState ts = StatusThread.ThreadState;
                toLog("ThreadState = " + ts.ToString());
                //if ((ts & (ThreadState.Stopped | ThreadState.Unstarted)) != 0)
                //{
                if (run)
                {
                    toLog("run is true... Why?..");
                    run = false;
                    Thread.Sleep(3000);
                }
                run = true;
                StatusThread.Start();
                ts = StatusThread.ThreadState;
                toLog("ThreadState after Start = " + ts.ToString());
                isOpened = true;
                //}
                //else
                //{
                //   toLog("StatusThread is already started");
                //}
            }
            catch (Exception e)
            {
                toLog(e.Message);
                toLog(e.StackTrace);
            }

            toLog("Stop; return always true");
            return true;
        }
        public override bool Close()
        {
            toLog("Start");
            isOpened = false;
            run = false;
            deviceEnabled = false;
            //StatusThread.Abort();
            releaseAuthSession();
            Thread.Sleep(1500);
            BMStatus = BM_STATUS.UNKNOWN;
            toLog("Stop; return always true");
            return true;
        }


        public override bool StartSession()
        {
            bool res = false;

            try
            {
                res = ipwrap.Open();

                if (!res)
                {
                    resultCodeU = ResponseCode.SysytemError;
                    return false;
                }

                Message request = new Message(Message.CMD_LOGIN, TID);
                toLog(request.ToPrintString());

                res = ipwrap.Send(request);
                toLog("Send res = " + res);

                if (!res)
                {
                    resultCodeU = ResponseCode.SysytemError;
                    return false;
                }

                Message response = ipwrap.Receive();
                toLog(response.ToPrintString());

                if (response.Command == Message.CMD_LOGIN_RESPONSE)
                {
                    resultCodeU = ResponseCode.Sucsess;
                    resultDescription = sStatusOK;
                    return true;
                }
                else if (response.Command == Message.CMD_ERROR_RESPONSE)
                {
                    resultCodeU = SetResultCode(response.ErrorCode);
                    resultDescription = sStatusError;
                    return false;
                }

                resultCodeU = ResponseCode.SysytemError;
                resultDescription = sStatusError;
                return false;

            }
            catch (Exception e)
            {
                toLog(e.Message);
                toLog(e.StackTrace);
                resultCodeU = ResponseCode.SysytemError;
                resultDescription = sStatusError;
                return false;
            }
        }
        public override bool StopSession()
        {
            try
            {
                ipwrap.Close();
            }
            catch { }

            return true;
        }
        public override string GetVersionDescription()
        {
            return VERSION;
        }
        public override bool Purchase()
        {
            toLog("");
            toLog("");
            toLog("");
            toLog("Start");

            releaseAuthSession();

            StartSession();

            bool bRes = false;

            try
            {
                AuthResult = string.Empty;
                AuthReceipt = string.Empty;

                List<Tuple<Message.DataType, string>> data = new List<Tuple<Message.DataType, string>>
                {
                    Tuple.Create(Message.DT_AMOUNT, ((uint)Math.Round((AmountDue * 100), 0)).ToString())
                };
                Message request = new Message(Message.CMD_SALE, TID, data);
                toLog("REQUEST: " + request.ToString());
                bRes = ipwrap.Send(request);
            }
            catch (Exception e)
            {
                bRes = false;

                resultCodeU = ResponseCode.SysytemError;
                resultDescription = sStatusError;
                authRequest = sStatusError;

                AuthorizationStarted = false;
                TransactionStarted = false;
                cardInserted = false;
                cardWasInserted = false;
                BMStatus = BM_STATUS.WAITING_COMMAND;

                toLog(e.Message);
                toLog(e.StackTrace);
            }


            if (bRes)
            {
                TransactionStarted = true;
                AuthorizationStarted = true;
                BMStatus = BM_STATUS.GET_COMMAND;

                bankModuleCommand = BankModuleCommandT.Purchase;
                CommandStatus = CommandStatusT.Run;

                toLog("set BankModuleCommandT.Purchase and CommandStatusT.Run");
            }


            return bRes;
        }
        public override bool Cancel(string RRN, decimal AmountDue)
        {

            toLog("Start");

            releaseAuthSession();

            StartSession();

            toLog("Cancel RRN=" + RRN + " AmountDue=" + AmountDue.ToString());

            lock (locker)
            {
                if (RRN == null || RRN == String.Empty)
                {
                    resultDescription = "Не задан RRN";
                    toLog(resultDescription);
                    return false;
                }

                try
                {
                    List<Tuple<Message.DataType, string>> data = new List<Tuple<Message.DataType, string>>
                    {
                        Tuple.Create(Message.DT_RRN, RRN),
                        Tuple.Create(Message.DT_AMOUNT, ((uint)Math.Round((AmountDue * 100), 0)).ToString()),
                    };
                    Message request = new Message(Message.CMD_REVERSAL_SALE_BY_RRN, TID, data);
                    toLog("REQUEST: " + request.ToString());

                    bool res = ipwrap.Send(request);
                    toLog("Send res = " + res);

                    if (!res)
                    {
                        resultCodeU = ResponseCode.SysytemError;
                        resultDescription = sStatusError;
                        return false;
                    }

                    Message response = null;
                    bool isActive = true;
                    do
                    {
                        response = ipwrap.Receive();
                        toLog(response.ToPrintString());

                        if (response.Command == Message.CMD_ERROR_RESPONSE)
                        {
                            isActive = false;
                        }
                        else if (response.Command == Message.CMD_PRINT_LINE)
                        {
                            AuthReceipt += response.TextLine + "\n";

                            if (response.LastLineFlag == "1")
                                isActive = false;
                        }
                        else if (response.Command == Message.CMD_AUTH_HOST_RESPONSE)
                        {
                            resultCodeU = SetResultCode(response.HostResponse.ResponseCode);
                            resultDescription = response.HostResponse.Message;
                            authRequest = resultDescription;
                            _status = resultDescription;

                            if (response.HostResponse.ResponseCode == Message.RC_SUCCESS)
                            {
                                AuthResult = response.HostResponse.ToPrintString();

                            }
                            else
                            {
                                isActive = false;


                            }

                        }

                    }
                    while (isActive);
                }
                catch (Exception e)
                {
                    toLog(e.Message);
                    toLog(e.StackTrace);

                    resultCodeU = ResponseCode.SysytemError;
                    resultDescription = sStatusError;

                    return false;
                }


                toLog(AuthResult);
                toLog(AuthReceipt, BMLogType.LOG_HEX);
                toLog("Stop");
                return true;
            }
        }

        private bool GetState()
        {
            toLog("Start");
            releaseAuthSession();

            StartSession();

            try
            {
                Message request = new Message(Message.CMD_GET_STATE, TID);
                toLog(request.ToPrintString());

                bool res = ipwrap.Send(request);
                toLog("Send res = " + res);

                if (!res)
                {
                    resultCodeU = ResponseCode.SysytemError;
                    resultDescription = sStatusError;
                    return false;
                }

                Message response = null;

                res = true;

                do
                {
                    response = ipwrap.Receive();
                    toLog(response.ToPrintString());

                    if (response.Command == Message.CMD_GET_STATE_RESPONSE)
                    {
                        if (response.StatusCode != "00")
                            res = false;

                        if (response.LastDevice == "1")
                        {
                            resultCodeU = ResponseCode.Sucsess;
                            resultDescription = sStatusOK;

                            toLog($"Result Terminal Info {res}");
                            return res;
                        }

                    }
                    else if (response.Command == Message.CMD_ERROR_RESPONSE)
                    {
                        resultCodeU = SetResultCode(response.ErrorCode);
                        resultDescription = response.ErrorMessage;

                        toLog("Stop");
                        return false;
                    }
                    else
                    {
                        resultCodeU = ResponseCode.OtherError;
                        resultDescription = response.ErrorMessage;

                        toLog("Stop");
                        return false;

                    }

                } while (true);
            }
            catch (Exception e)
            {
                resultCodeU = ResponseCode.SysytemError;
                resultDescription = sStatusError;

                toLog(e.Message);
                toLog(e.StackTrace);

                toLog("Stop");

                return false;
            }
        }

        public override bool EchoTest()
        {
            toLog("Start");

            return GetState();
        }

        public override bool TerminalInfo()
        {
            toLog("Start");

            return GetState();
        }


        public override bool Verification()
        {
            toLog("Start");

            dtStartVerification = DateTime.Now;
            toLog("Verifiction started at " + dtStartVerification.ToLongDateString());

            toLog("Try delete old logs...");
            deleteOldBMLogs();
            toLog("Finish delete old logs...");

            AuthResult = string.Empty;
            AuthReceipt = string.Empty;

            releaseAuthSession();

            StartSession();

            //lock (locker)
            {
                try
                {
                    List<Tuple<Message.DataType, string>> data = new List<Tuple<Message.DataType, string>>
                    {
                        Tuple.Create(Message.DT_REPORT_TYPE, Message.REPORT_TYPE_WITH_CLOSE)
                        //Tuple.Create(Message.DT_REPORT_TYPE, Message.REPORT_TYPE_WITHOUT_CLOSE)
                    };
                    Message request = new Message(Message.CMD_GET_REPORT, TID, data);
                    toLog("REQUEST: " + request.ToString());

                    bool res = ipwrap.Send(request);
                    toLog("Send res = " + res);

                    if (!res)
                    {
                        resultCodeU = ResponseCode.SysytemError;
                        resultDescription = sStatusError;
                        return false;
                    }

                    Message response = ipwrap.Receive();
                    toLog(response.ToPrintString());

                    if (response.Command == Message.CMD_GET_REPORT_RESPONSE)
                    {
                        resultCodeU = ResponseCode.Sucsess;
                        resultDescription = sStatusOK;

                        AuthResult = response.RawData;
                        AuthReceipt = response.RawData;

                        toLog("Stop");

                        dtStartVerification = DateTime.MinValue;

                        return true;
                    }
                    else if (response.Command == Message.CMD_ERROR_RESPONSE)
                    {
                        resultCodeU = SetResultCode(response.ErrorCode);
                        resultDescription = response.ErrorMessage;

                        AuthResult = string.Empty;
                        AuthReceipt = string.Empty;

                        toLog("Stop");

                        return false;
                    }

                    resultCodeU = ResponseCode.SysytemError;
                    resultDescription = sStatusError;

                    AuthResult = string.Empty;
                    AuthReceipt = string.Empty;

                    toLog("Stop");

                    return false;
                }
                catch (Exception e)
                {
                    toLog(e.Message);
                    toLog(e.StackTrace);


                    resultCodeU = ResponseCode.SysytemError;
                    resultDescription = sStatusError;

                    toLog("Stop");

                    return false;
                }
            }
        }
        private void autoVerifacation()
        {

            if (deviceEnabled)
            {
                bool bResVer = Verification();
                toLog("bResVer=" + bResVer);
                if (bResVer)
                {
                    isVerification = false;
                }
                else
                {
                    deviceEnabled = false;
                }
            }
            string sLog = "isVerification=" + isVerification + " deviceEnabled=" + deviceEnabled;
            if (sLastAutoVerifactionLog != sLog)
            {
                toLog(sLog);
                sLastAutoVerifactionLog = sLog;
            }
        }
        public override string GetOpStatus(bool IsCancel = false)
        {
            bool bRes = false;
            toLog("Start");
            toLog("IsCancel=" + IsCancel.ToString() + " isCancelled=" + isCancelled.ToString());
            toLog("CommandStatus = " + CommandStatus.ToString());

            //lock (locker)
            {
                if (deviceEnabled)
                {
                    if (IsCancel && !isCancelled)
                    {
                        try
                        {
                            Message message = new Message(Message.CMD_BREAK, TID);
                            bRes = ipwrap.Send(message);
                        }
                        catch (Exception e)
                        {
                            bRes = false;
                            toLog(e.Message);
                            toLog(e.StackTrace);
                        }
                        toLog("AbortTransaction nRes: " + bRes);
                        resultCodeU = SetResultCode(bRes);
                    }
                    toLog("Stop; Ret: " + sStatusAbortOperation);
                    isCancel = false;
                    busy = false;
                    return sStatusAbortOperation;
                }
                toLog("Stop; deviceEnabled = " + deviceEnabled);
                isCancel = false;
                busy = false;
                return "";
            }
        }
        protected internal void PoolThread()
        {
            toLog("PoolThread -> START");

            CommandStatusT csLast = CommandStatusT.Null;

            while (run)
            {
                if (csLast != CommandStatus)
                    csLast = CommandStatus;

                switch (CommandStatus)
                {
                    case CommandStatusT.Run:
                        #region CommandStatusT.Run
                        toLog("CommandStatusT.Run");

                        switch (bankModuleCommand)
                        {
                            case BankModuleCommandT.Purchase:
                                toLog("CommandStatusT.Run -> BankModuleCommandT.Purchase");

                                if (!deviceEnabled)
                                {
                                    try
                                    {
                                        toLog("CommandStatusT.Run -> deviceEnabled = " + deviceEnabled + ". Try open");
                                        Open();
                                    }
                                    catch (Exception e)
                                    {
                                        toLog(e.Message);
                                        toLog(e.StackTrace);

                                        resultCodeU = ResponseCode.OtherError;
                                        resultDescription = sStatusError;
                                        CommandStatus = CommandStatusT.Completed;
                                    }
                                }

                                if (deviceEnabled)
                                {
                                    lock (locker)
                                    {
                                        authRequest = sStatusBeginOperation;
                                        _status = sStatusBeginOperation;

                                        try
                                        {
                                            Message response = ipwrap.Receive();
                                            toLog(response.ToPrintString());

                                            if (response.Command == Message.CMD_INITIAL_RESPONSE)
                                            {
                                                toLog("GET INITIAL RESPONSE");
                                                nPINDigits = 0;

                                                authRequest = sStatusBeginOperation;
                                                _status = sStatusBeginOperation;

                                                toLog(authRequest);
                                            }
                                            else if (response.Command == Message.CMD_DEVICE_EVENT)
                                            {
                                                toLog("GET DEVICE EVENT");

                                                if (response.DeviceID == Message.DEVICE_ID_CARD_READER &&
                                                    response.EventID == Message.EVENT_ID_CARD_INSERTED)
                                                {
                                                    cardInserted = true;
                                                    cardWasInserted = true;
                                                    authRequest = sStatusCardInserted;
                                                    _status = sStatusCardInserted;
                                                    CommandStatus = CommandStatusT.Running;
                                                    BMStatus = BM_STATUS.INSERT_CARD;
                                                    toLog("Card Inserted; go to CommandStatusT.Running");
                                                    toLog(authRequest);
                                                }
                                            }
                                            else if (response.Command == Message.CMD_ERROR_RESPONSE)
                                            {
                                                toLog("GET ERROR RESPONSE");

                                                resultCodeU = SetResultCode(response.ErrorCode);
                                                authRequest = response.ErrorMessage;
                                                resultDescription = response.ErrorMessage;

                                                CommandStatus = CommandStatusT.Completed;

                                                toLog("Error Response; go to CommandStatusT.Completed");
                                                toLog(authRequest);
                                            }
                                            else if (response.Command == Message.CMD_BREAK_RESPONSE)
                                            {
                                                toLog($"GET BREAK RESPONSE <{response.BreakResult}>");

                                                if (response.BreakResult == "0")
                                                {
                                                    isCancelled = true;

                                                    resultCodeU = ResponseCode.Sucsess;
                                                    resultDescription = sStatusAbortOperation;

                                                    authRequest = sStatusAbortOperation;
                                                    _status = sStatusAbortOperation;

                                                    CommandStatus = CommandStatusT.Completed;

                                                    toLog("nResponseCodeAbortOperation; go to CommandStatusT.Completed");
                                                }
                                                else
                                                {
                                                    isCancelled = false;
                                                }

                                                toLog(authRequest);

                                            }
                                            else
                                            {
                                                toLog("UNKNOWN MESSAGE");
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            toLog(e.Message);
                                            toLog(e.StackTrace);

                                            resultCodeU = ResponseCode.SysytemError;
                                            authRequest = sStatusError;
                                            resultDescription = sStatusError;

                                            CommandStatus = CommandStatusT.Completed;
                                            toLog("ERROR; go to CommandStatusT.Completed");

                                            toLog(authRequest);
                                        }
                                    }
                                }
                                else
                                    resultCodeU = ResponseCode.OtherError;
                                break;
                            default:
                                break;
                        }
                        break;
                    #endregion CommandStatusT.Run
                    case CommandStatusT.Running:
                        #region CommandStatusT.Running
                        toLog("");
                        toLog("CommandStatusT.Running");
                        //lock (locker)
                        {
                            try
                            {
                                Message response = ipwrap.Receive();
                                toLog(response.ToPrintString());

                                if (response.Command == Message.CMD_PIN_ENTRY_REQUIRED)
                                {
                                    toLog("GET PIN ENTRY REQUIRED");

                                    authRequest = sStatusPINRequired;
                                    _status = sStatusPINRequired;

                                    toLog("CommandStatusT.Running -> sStatusPINRequired: " + sStatusPINRequired);

                                    toLog(authRequest);
                                }
                                else if (response.Command == Message.CMD_DEVICE_EVENT)
                                {
                                    toLog("GET DEVICE EVENT");
                                    toLog($"DEVICE ID: {response.DeviceID} EVENT ID: {response.EventID} PARAMS: {response.Params}");

                                    if (response.DeviceID == Message.DEVICE_ID_PIN_PAD)
                                    {
                                        if (response.Params == Message.EVENT_CODE_PRESS_NUMERIC)
                                        {
                                            nPINDigits++;
                                        }
                                        else if (response.Params == Message.EVENT_CODE_PRESS_CLEAR)
                                        {
                                            if (nPINDigits <= 4)
                                                nPINDigits--;
                                            else
                                                nPINDigits = 3;
                                        }

                                        if (nPINDigits <= 0)
                                        {
                                            authRequest = sStatusPINRequired;
                                            _status = sStatusPINRequired;
                                        }
                                        else if (nPINDigits == 1)
                                        {
                                            authRequest = sStatusPINRequired1D;
                                            _status = sStatusPINRequired1D;
                                        }
                                        else if (nPINDigits == 2)
                                        {
                                            authRequest = sStatusPINRequired2D;
                                            _status = sStatusPINRequired2D;
                                        }
                                        else if (nPINDigits == 3)
                                        {
                                            authRequest = sStatusPINRequired3D;
                                            _status = sStatusPINRequired3D;
                                        }
                                        else if (nPINDigits >= 4)
                                        {
                                            authRequest = sStatusPINRequired4D;
                                            _status = sStatusPINRequired4D;
                                        }

                                    }
                                    else if (response.DeviceID == Message.DEVICE_ID_CARD_READER)
                                    {
                                        if (response.EventID == Message.EVENT_ID_CARD_OUT)
                                        {
                                            authRequest = sStatusCardRemoved;
                                            _status = sStatusCardRemoved;

                                            toLog("HEX RECEIPT");
                                            toLog(AuthReceipt, BMLogType.LOG_HEX);

                                            AuthorizationStarted = false;
                                            TransactionStarted = false;
                                            cardInserted = false;
                                            cardWasInserted = false;

                                            CommandStatus = CommandStatusT.Completed;

                                            toLog("go to CommandStatusT.Completed");
                                        }
                                    }

                                    toLog(authRequest);
                                }
                                else if (response.Command == Message.CMD_ONLINE_REQUIRED)
                                {
                                    toLog("GET ONLINE REQUIRED");
                                    authRequest = sStatusAuth;
                                    _status = sStatusAuth;

                                    toLog(authRequest);

                                }
                                else if (response.Command == Message.CMD_AUTH_HOST_RESPONSE)
                                {
                                    toLog("GET AUTH HOST RESPONSE");

                                    resultCodeU = SetResultCode(response.HostResponse.ResponseCode);
                                    resultDescription = response.HostResponse.Message;
                                    authRequest = resultDescription;
                                    _status = resultDescription;

                                    if (response.HostResponse.ResponseCode == Message.RC_SUCCESS)
                                    {
                                        AuthResult = response.HostResponse.ToPrintString();
                                        //toLog(AuthResult);

                                        sLastRRN = response.HostResponse.RRN;
                                        sLastAmount = response.HostResponse.TransactionSum;
                                    }
                                    else
                                    {
                                        AuthorizationStarted = false;
                                        TransactionStarted = false;
                                        cardInserted = false;
                                        cardWasInserted = false;

                                        CommandStatus = CommandStatusT.Completed;

                                        toLog("Error Host Response; go to CommandStatusT.Completed");
                                    }

                                    toLog(authRequest);
                                }
                                else if (response.Command == Message.CMD_PRINT_LINE)
                                {
                                    toLog($"GET PRINT LINE <{response.LastLineFlag}>");

                                    AuthReceipt += response.TextLine + "\n";

                                    if (response.LastLineFlag == "1")
                                    {
                                        toLog("HEX RECEIPT");
                                        toLog(AuthReceipt, BMLogType.LOG_HEX);

                                        //AuthorizationStarted = false;
                                        //TransactionStarted = false;
                                        //cardInserted = false;
                                        //cardWasInserted = false;

                                        //CommandStatus = CommandStatusT.Completed;

                                        toLog("Last Print Line");

                                        toLog(authRequest);
                                    }
                                }
                                else if (response.Command == Message.CMD_ERROR_RESPONSE)
                                {
                                    resultCodeU = SetResultCode(response.ErrorCode);
                                    authRequest = response.ErrorMessage;
                                    resultDescription = response.ErrorMessage;

                                    AuthorizationStarted = false;
                                    TransactionStarted = false;
                                    cardInserted = false;
                                    cardWasInserted = false;

                                    CommandStatus = CommandStatusT.Completed;

                                    toLog("Error Response; go to CommandStatusT.Completed");

                                    toLog(authRequest);
                                }
                                else if (response.Command == Message.CMD_BREAK_RESPONSE)
                                {
                                    if (response.BreakResult == "0")
                                    {
                                        isCancelled = true;

                                        resultCodeU = ResponseCode.Sucsess;
                                        resultDescription = sStatusAbortOperation;

                                        authRequest = sStatusAbortOperation;
                                        _status = sStatusAbortOperation;

                                        CommandStatus = CommandStatusT.Completed;

                                        toLog("nResponseCodeAbortOperation; go to CommandStatusT.Completed");
                                    }
                                    else
                                    {
                                        isCancelled = false;
                                    }

                                }
                                else
                                {
                                    toLog("UNKNOWN MESSAGE");
                                }
                            }
                            catch (Exception e)
                            {
                                toLog(e.Message);
                                toLog(e.StackTrace);

                                resultCodeU = ResponseCode.SysytemError;
                                authRequest = sStatusError;
                                resultDescription = sStatusError;

                                CommandStatus = CommandStatusT.Completed;
                                toLog("ERROR; go to CommandStatusT.Completed");
                            }
                        }
                        break;
                    #endregion
                    case CommandStatusT.Completed:
                        #region CommandStatusT.Completed
                        if (csLast != CommandStatus)
                        {
                            toLog("CommandStatusT.Completed -> start");
                            csLast = CommandStatus;
                        }

                        AuthorizationStarted = false;
                        TransactionStarted = false;
                        cardInserted = false;
                        cardWasInserted = false;
                        BMStatus = BM_STATUS.WAITING_COMMAND;

                        StopSession();

                        try
                        {
                            if (isVerification)
                            {
                                Thread.Sleep(200);
                                autoVerifacation();
                            }
                        }
                        catch (Exception e)
                        {
                            toLog(e.Message);
                            toLog(e.StackTrace);
                            toLog("CommandStatusT.Completed -> err: " + e.Message);
                        }
                        break;
                    #endregion
                    case CommandStatusT.Undeifned:
                        #region CommandStatusT.Undeifned

                        if (csLast != CommandStatus)
                        {
                            toLog("CommandStatusT.Undeifned -> start");
                            csLast = CommandStatus;
                        }

                        if (AuthorizationStarted)
                            AuthorizationStarted = false;
                        if (resultDescription != "")
                            resultDescription = "";


                        if (isVerification)
                        {
                            Thread.Sleep(200);
                            toLog("CommandStatusT.Undeifned autoVerifacation");
                            autoVerifacation();
                        }

                        break;
                        #endregion CommandStatusT.Undeifned
                }
                Thread.Sleep(200);
            }

            toLog("PoolThread -> FINISH");
        }

    }
}
