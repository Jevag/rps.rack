﻿using System;

namespace rps.Data
{
  /// <summary>
  /// Контекст вычисления
  /// </summary>
  public class EvaluationContext
  {
    private class TemporaryShift : IDisposable
    {
      private EvaluationContext _context;
      private DateTime _timeCurrent;
      private DateTime _timePrevious;

      public TemporaryShift(EvaluationContext c)
      {
        _context = c;
        _timeCurrent = c.TimeCurrent;
        _timePrevious = c.TimePrevious;
      }

      public void Dispose()
      {
        _context.TimeCurrent = _timeCurrent;
        _context.TimePrevious = _timePrevious;
      }
    }

    public readonly DateTime TimeStart;
    public readonly DateTime TimeEnd;
    public readonly object Metadata;

    public DateTime TimeCurrent;
    public DateTime TimePrevious;
    public DateTime TimeRule;
    public decimal Amount;

    public readonly Tariff Tariff;
    public Rule Rule;
    public Interval Interval;
    public TimeSpan Duration;
    public Limitation Limitation;
    public int RepeatIndex;
    public bool SwitchExact;

    private bool _isFirstInterval;
    private Condition _closeCondition;
    private DateTime _timeShift;

    #region [ properties ]

    public bool IsClosed
    {
      get { return _closeCondition.Evaluate(this) && (_timeShift == DateTime.MinValue); }
    }

    public bool IsFirstInterval
    {
      get { return _isFirstInterval; }
    }

    #endregion

    public EvaluationContext(Tariff tariff, DateTime timeStart, DateTime timeEnd, object metaData)
    {
      TimeStart = timeStart.TrimMilliseconds();
      TimeEnd = timeEnd.TrimMilliseconds();
      Metadata = metaData;

      TimeCurrent = TimeStart;
      TimePrevious = TimeCurrent;
      TimeRule = TimeCurrent;
      Amount = 0;

      Tariff = tariff;
      Rule = null;
      Interval = null;
      Duration = TimeSpan.Zero;
      Limitation = null;
      RepeatIndex = 0;
      SwitchExact = false;

      _isFirstInterval = true;
      _closeCondition = new DateReachedCondition(TimeEnd);
      _timeShift = DateTime.MinValue;
    }

    public void Next(TimeSpan interval)
    {
      Next(interval, false);
    }

    public void Next(TimeSpan interval, bool advance)
    {
      TimePrevious = TimeCurrent;
      TimeCurrent += interval;
      //Duration = TimeSpan.Zero;
      if (advance)
        _isFirstInterval = false;
    }

    public IDisposable TryNext(TimeSpan interval)
    {
      TemporaryShift t = new TemporaryShift(this);
      Next(interval, false);

      return t;
    }

    public bool SetShift()
    {
      if (TimePrevious == TimeCurrent)
        return false;

      _timeShift = TimePrevious;

      return true;
    }

    public bool ReleaseShift()
    {
      if (_timeShift == DateTime.MinValue)
        return false;

      TimeCurrent = _timeShift;
      TimePrevious = _timeShift;
      _timeShift = DateTime.MinValue;

      return true;
    }

    public override string ToString()
    {
      return String.Format("{0}, {1:C}{2}", TimeCurrent, Amount, IsClosed ? " (closed)" : String.Empty);
    }
  }
}