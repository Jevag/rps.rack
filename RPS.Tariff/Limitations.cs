﻿using System;
using System.Runtime.Serialization;

namespace rps.Data
{
  /// <summary>
  /// Область действия ограничения
  /// </summary>
  [DataContract]
  public enum LimitationScope : int
  {
    /// <summary>
    /// Глобальное
    /// </summary>
    [EnumMember]
    Global = 1,

    /// <summary>
    /// Правило
    /// </summary>
    [EnumMember]
    Rule = 2,

    /// <summary>
    /// Интервал
    /// </summary>
    [EnumMember]
    Interval = 3
  }

  /// <summary>
  /// Ограничение
  /// </summary>
  [DataContract]
  public class Limitation : ICloneable
  {
    [DataMember]
    public Guid ID;
    [DataMember]
    public ICondition Match;
    [DataMember]
    public int Repeat;
    [DataMember]
    public decimal Amount;
    [DataMember]
    public SwitchMode BeforeMode;
    [DataMember]
    public SwitchMode AfterMode;
    [DataMember]
    public bool Exclusive;
    [DataMember]
    public string Description;
    [DataMember]
    public LimitationScope Scope;
    [DataMember]
    public Guid RuleID;
    [DataMember]
    public Guid IntervalID;

    public Limitation()
      : this(Guid.NewGuid())
    {
      //
    }

    public Limitation(Guid id)
    {
      ID = id;
      Match = Condition.Never;
      Repeat = 1;
      Amount = 0;
      BeforeMode = SwitchMode.Match;
      AfterMode = SwitchMode.Match;
      Exclusive = false;
      Description = String.Empty;
      Scope = LimitationScope.Global;
      RuleID = Guid.Empty;
      IntervalID = Guid.Empty;
    }

    public Limitation(Limitation limitation)
    {
      ID = limitation.ID;
      Match = limitation.Match;
      Repeat = limitation.Repeat;
      Amount = limitation.Amount;
      BeforeMode = limitation.BeforeMode;
      AfterMode = limitation.AfterMode;
      Exclusive = limitation.Exclusive;
      Description = limitation.Description;
      Scope = limitation.Scope;
      RuleID = limitation.RuleID;
      IntervalID = limitation.IntervalID;
    }

    #region [ ICloneable ]

    public object Clone()
    {
      return new Limitation(this);
    }

    #endregion

    public override string ToString()
    {
      string r = String.Format(" x{0}", (Repeat == Interval.Unlimited) ? Double.PositiveInfinity.ToString() : Repeat.ToString());
      string d = String.IsNullOrWhiteSpace(Description) ? Scope.ToString() : Description;

      return String.Format("{0}{1}, {2:C} ({3})", Match, r, Amount, d);
    }
  }
}