﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace rps.Data
{
  /// <summary>
  /// Интерфейс условия
  /// </summary>
  public interface ICondition
  {
    SwitchMode Mode { get; set; }
    EvaluationResult Evaluate(EvaluationContext context);
    DateTime? GetTimeMax(DateTime timeStart);
  }

  /// <summary>
  /// Результат вычисления условия
  /// </summary>
  public class EvaluationResult
  {
    #region [ static ]

    public static implicit operator Boolean(EvaluationResult result)
    {
      return result.Result;
    }

    #endregion

    public readonly bool Result;
    public DateTime? TimeStart;
    public DateTime? TimeMax;
    public DateTime? TimeNext;

    public EvaluationResult(bool result)
    {
      Result = result;
      TimeStart = null;
      TimeMax = null;
      TimeNext = null;
    }

    public override string ToString()
    {
      return String.Format("{0}, {1} - {2} > {3}", Result, TimeStart.GetString(), TimeMax.GetString(), TimeNext.GetString());
    }
  }

  /// <summary>
  /// Абстрактный базовый класс условия
  /// </summary>
  [DataContract]
  [KnownType(typeof(DefaultCondition))]
  [KnownType(typeof(MultipleCondition))]
  [KnownType(typeof(FirstIntervalCondition))]
  [KnownType(typeof(TimeElapsedCondition))]
  [KnownType(typeof(TimeReachedCondition))]
  [KnownType(typeof(TimeBetweenCondition))]
  [KnownType(typeof(DateReachedCondition))]
  [KnownType(typeof(DateBetweenCondition))]
  [KnownType(typeof(DateIntervalCondition))]
  [KnownType(typeof(MonthlyIntervalCondition))]
  [KnownType(typeof(WeeklyIntervalCondition))]
  [KnownType(typeof(WorkdayCondition))]
  [KnownType(typeof(WeekendCondition))]
  [KnownType(typeof(RepeatIntervalCondition))]
  public abstract class Condition : ICondition
  {
    [DataMember]
    private SwitchMode _mode;

    #region [ static ]

    public static ICondition Never = new DefaultCondition(false);
    public static ICondition Always = new DefaultCondition(true);

    #endregion

    protected Condition()
    {
      _mode = SwitchMode.Inherit;
    }

    #region [ ICondition ]

    public SwitchMode Mode
    {
      get { return _mode; }
      set { _mode = value; }
    }

    public abstract EvaluationResult Evaluate(EvaluationContext context);

    public virtual DateTime? GetTimeMax(DateTime timeStart)
    {
      return null;
    }

    #endregion

    public override string ToString()
    {
      return _mode.ToString();
    }
  }

  /// <summary>
  /// Условие по умолчанию (всегда или никогда)
  /// </summary>
  [DataContract]
  public sealed class DefaultCondition : Condition
  {
    [DataMember]
    private bool _result;

    public DefaultCondition(bool result)
    {
      _result = result;
    }

    #region [ Condition ]

    public override EvaluationResult Evaluate(EvaluationContext context)
    {
      return new EvaluationResult(_result);
    }

    #endregion

    public override string ToString()
    {
      return _result.ToString();
    }
  }

  /// <summary>
  /// Множественное условие
  /// </summary>
  [DataContract]
  public sealed class MultipleCondition : Condition
  {
    [DataMember]
    private List<ICondition> _conditions;
    [DataMember]
    private bool _any;

    private EvaluationResult[] _lastResults;
    private DateTime?[] _lastTimeMax;

    #region [ properties ]

    internal bool Any
    {
      set { _any = value; }
    }

    #endregion

    public MultipleCondition()
    {
      _conditions = new List<ICondition>();
      _any = false;
    }

    #region [ Condition ]

    public override EvaluationResult Evaluate(EvaluationContext context)
    {
      _lastResults = _conditions.Select(c => c.Evaluate(context)).ToArray();
      bool b = false;
      if (_any)
        b = _lastResults.Any(r => r.Result);
      else
        b = _lastResults.All(r => r.Result);

      var z = new EvaluationResult(b);
      z.TimeStart = GetNearestValue(_lastResults.Where(r => r.TimeStart.HasValue).Select(r => r.TimeStart.Value.Ticks), _any);
      z.TimeMax = GetNearestValue(_lastResults.Where(r => r.Result && r.TimeMax.HasValue).Select(r => r.TimeMax.Value.Ticks), !_any);
      z.TimeNext = GetNearestValue(_lastResults.Where(r => r.TimeNext.HasValue).Select(r => r.TimeNext.Value.Ticks), _any);

      return z;
    }

    public override DateTime? GetTimeMax(DateTime timeStart)
    {
      _lastTimeMax = _conditions.Select(c => c.GetTimeMax(timeStart)).ToArray();

      return GetNearestValue(_lastTimeMax.Where(t => t.HasValue).Select(t => t.Value.Ticks), !_any);
    }

    #endregion

    internal void Append(ICondition c)
    {
      _conditions.Add(c);
    }

    private DateTime? GetNearestValue(IEnumerable<long> t, bool min)
    {
      return t.Any() ? new DateTime?(new DateTime(min ? t.Min() : t.Max())) : null;
    }

    public override string ToString()
    {
      return _conditions.GetString(_any ? " |" : " &");
    }
  }

  /// <summary>
  /// Первый интервал
  /// </summary>
  [DataContract]
  public class FirstIntervalCondition : Condition
  {
    #region [ Condition ]

    public override EvaluationResult Evaluate(EvaluationContext context)
    {
      return new EvaluationResult(context.IsFirstInterval);
    }

    #endregion
  }

  /// <summary>
  /// Прошло времени от начала
  /// </summary>
  [DataContract]
  public class TimeElapsedCondition : Condition
  {
    [DataMember]
    private TimeSpan _ts;

    public TimeElapsedCondition(TimeSpan ts)
    {
      _ts = ts;
    }

    #region [ Condition ]

    public override EvaluationResult Evaluate(EvaluationContext context)
    {
      DateTime dt = context.TimeStart + _ts;
      var r = new EvaluationResult(context.TimeCurrent >= dt);
      r.TimeStart = context.TimeStart;
      if (r)
        r.TimeMax = dt;

      return r;
    }

    #endregion

    public override string ToString()
    {
      return String.Format("... {0}", _ts);
    }
  }

  /// <summary>
  /// Достигнуто указанное время дня
  /// </summary>
  [DataContract]
  public class TimeReachedCondition : Condition
  {
    [DataMember]
    private TimeSpan _ts;

    public static TimeReachedCondition Hour(int hour)
    {
      return new TimeReachedCondition(TimeSpan.FromHours(hour));
    }

    public TimeReachedCondition(TimeSpan ts)
    {
      _ts = ts;
    }

    #region [ Condition ]

    public override EvaluationResult Evaluate(EvaluationContext context)
    {
      DateTime dt = context.TimeCurrent;
      bool b = (dt.TimeOfDay >= _ts);
      var r = new EvaluationResult(b);
      if (b)
        r.TimeMax = dt.Date + _ts;

      return r;
    }

    #endregion

    public override string ToString()
    {
      return _ts.ToString();
    }
  }

  /// <summary>
  /// Время дня находится в указанном интервале
  /// </summary>
  [DataContract]
  public class TimeBetweenCondition : Condition
  {
    [DataMember]
    private TimeSpan _ts;
    [DataMember]
    private TimeSpan _te;

    #region [ static ]

    public static TimeBetweenCondition Hours(int hs, int he)
    {
      return new TimeBetweenCondition(TimeSpan.FromHours(hs), TimeSpan.FromHours(he));
    }

    #endregion

    public TimeBetweenCondition(TimeSpan ts, TimeSpan te)
    {
      _ts = ts;
      _te = te;
    }

    #region [ Condition ]

    public override EvaluationResult Evaluate(EvaluationContext context)
    {
      bool b = false;
      DateTime? n = null;
      DateTime? m = null;
      TimeSpan t = context.TimeCurrent.TimeOfDay;

      //normal range
      if (_ts < _te)
      {
        b = ((t >= _ts) && (t < _te));
        if (b)
          //in range, set max to end
          m = context.TimeCurrent.Date.Add(_te);
        else
        {
          //out of range, set next to start
          n = context.TimeCurrent.Date.Add(_ts);
          if (t >= _te)
            //end is passed, move to next day
            n += TimeSpan.FromDays(1);
        }
      }
      //reverse range
      else if (_ts > _te)
      {
        if (t >= _ts)
        {
          //in range (end of day), set max to next day end
          m = context.TimeCurrent.Date.AddDays(1).Add(_te);
          b = true;
        }
        else if (t < _te)
        {
          //in range (start of day), set max to end
          m = context.TimeCurrent.Date.Add(_te);
          b = true;
        }
        else
        {
          //out of range (middle of day), set next to end
          n = context.TimeCurrent.Date.Add(_ts);
        }
      }
      //invalid range
      else
        throw new ApplicationException("Invalid range");

      var r = new EvaluationResult(b);
      r.TimeStart = context.TimeCurrent.Date.Add(_ts);
      r.TimeNext = n;
      r.TimeMax = m;

      return r;
    }

    public override DateTime? GetTimeMax(DateTime timeStart)
    {
      TimeSpan d = _te - _ts;
      if (_ts > _te)
        d += TimeSpan.FromDays(1);

      return timeStart + d;
    }

    #endregion

    public override string ToString()
    {
      return String.Format("{0} - {1}", _ts, _te);
    }
  }

  /// <summary>
  /// Достигнута указанная дата
  /// </summary>
  [DataContract]
  public class DateReachedCondition : Condition
  {
    [DataMember]
    private DateTime _dt;

    public DateReachedCondition(DateTime dt)
    {
      _dt = dt;
    }

    #region [ Condition ]

    public override EvaluationResult Evaluate(EvaluationContext context)
    {
      DateTime dt = context.TimeCurrent;
      bool b = (dt >= _dt);
      var r = new EvaluationResult(b);
      //r.TimeStart = context.TimeCurrent;
      if (b)
        r.TimeMax = dt;
      else
        r.TimeNext = dt;

      return r;
    }

    #endregion

    public override string ToString()
    {
      return _dt.ToString();
    }
  }

  /// <summary>
  /// Абстрактный базовый класс для интервала дат
  /// </summary>
  [DataContract]
  public abstract class DateBetweenCondition : Condition
  {
    protected abstract DateTime? GetStartDate(EvaluationContext c);

    protected abstract DateTime? GetEndDate(EvaluationContext c);

    protected abstract DateTime? GetNext(EvaluationContext c, DateTime? dt);

    protected abstract DateTime? GetPrevious(EvaluationContext c, DateTime? dt);

    #region [ Condition ]

    public override EvaluationResult Evaluate(EvaluationContext context)
    {
      DateTime dt = context.TimeCurrent;
      DateTime? dts = GetStartDate(context);
      DateTime? dte = GetEndDate(context);

      bool b = false;
      if (dts.HasValue && dte.HasValue)
      {
        //normal range
        if (dts < dte)
        {
          b = ((dt >= dts) && (dt < dte));
          if (!b)
          {
            //out of range
            if (dt >= dte)
              //advance to next start
              dts = GetNext(context, dts);
          }
        }
        //reverse range
        else if (dts > dte)
        {
          if (dt >= dts)
          {
            //check max
            dte = GetNext(context, dte);
            b = (dte.HasValue && (dt < dte));
            if (!b)
              //advance next
              dts = GetNext(context, dts);
          }
          else if (dt < dte)
          {
            //check start
            dts = GetPrevious(context, dts);
            b = (dts.HasValue && (dt >= dts));
          }
        }
        //invalid range
        else
          throw new ApplicationException("Invalid range");
      }

      var r = new EvaluationResult(b);
      r.TimeStart = dts;
      if (b)
        r.TimeMax = dte;
      else if (dts.HasValue && (dt < dts))
        //next only if before current
        r.TimeNext = dts;

      return r;
    }

    #endregion
  }

  /// <summary>
  /// Дата находится в указанном интервале
  /// </summary>
  [DataContract]
  public class DateIntervalCondition : DateBetweenCondition
  {
    [DataMember]
    private DateTime _dts;
    [DataMember]
    private DateTime _dte;

    public DateIntervalCondition(DateTime dts, DateTime dte)
    {
      _dts = dts;
      _dte = dte;
    }

    #region [ DateBetweenCondition ]

    protected override DateTime? GetStartDate(EvaluationContext c)
    {
      return _dts;
    }

    protected override DateTime? GetEndDate(EvaluationContext c)
    {
      return _dte;
    }

    protected override DateTime? GetNext(EvaluationContext c, DateTime? dt)
    {
      return null;
    }

    protected override DateTime? GetPrevious(EvaluationContext c, DateTime? dt)
    {
      return null;
    }

    #endregion

    public override string ToString()
    {
      return String.Format("{0} - {1}", _dts, _dte);
    }
  }

  /// <summary>
  /// Ежемесячный интервал дат
  /// </summary>
  [DataContract]
  public class MonthlyIntervalCondition : DateBetweenCondition
  {
    [DataMember]
    private int _ds;
    [DataMember]
    private TimeSpan _ts;
    [DataMember]
    private int _de;
    [DataMember]
    private TimeSpan _te;

    #region [ static ]

    public static MonthlyIntervalCondition OneDay(int d)
    {
      return new MonthlyIntervalCondition(d, TimeSpan.Zero, d, TimeSpan.Zero);
    }

    public static MonthlyIntervalCondition Days(int ds, int de)
    {
      return new MonthlyIntervalCondition(ds, TimeSpan.Zero, de, TimeSpan.Zero);
    }

    #endregion

    public MonthlyIntervalCondition(int ds, TimeSpan ts, int de, TimeSpan te)
    {
      _ds = ds;
      _ts = ts;
      _de = de;
      _te = te;
    }

    #region [ DateBetweenCondition ]

    protected override DateTime? GetStartDate(EvaluationContext c)
    {
      DateTime dt = c.TimeCurrent;
      int y = dt.Year;
      int m = dt.Month;
      int d = Math.Min(_ds, DateTime.DaysInMonth(y, m)); //round to month end

      return new DateTime(y, m, d).Add(_ts);
    }

    protected override DateTime? GetEndDate(EvaluationContext c)
    {
      DateTime dt = c.TimeCurrent;
      int y = dt.Year;
      int m = dt.Month;
      int d = _de;
      if (d > DateTime.DaysInMonth(y, m)) //round to month end
      {
        d = 1;
        m++;
        if (m > 12)
        {
          m = 1;
          y++;
        }
      }

      return new DateTime(y, m, d).Add(_te);
    }

    protected override DateTime? GetNext(EvaluationContext c, DateTime? dt)
    {
      if (dt.HasValue)
      {
        int y = dt.Value.Year;
        int m = dt.Value.Month;
        m++;
        if (m > 12)
        {
          m = 1;
          y++;
        }
        int d = Math.Min(dt.Value.Day, DateTime.DaysInMonth(y, m)); //round to month end
        dt = new DateTime(y, m, d).Add(dt.Value.TimeOfDay);
      }

      return dt;
    }

    protected override DateTime? GetPrevious(EvaluationContext c, DateTime? dt)
    {
      if (dt.HasValue)
      {
        int y = dt.Value.Year;
        int m = dt.Value.Month;
        m--;
        if (m < 1)
        {
          m = 12;
          y--;
        }
        int d = Math.Min(dt.Value.Day, DateTime.DaysInMonth(y, m)); //round to month end
        dt = new DateTime(y, m, d).Add(dt.Value.TimeOfDay);
      }

      return dt;
    }

    #endregion

    public override string ToString()
    {
      return String.Format("{0} {1} - {2} {3}", _ds, _ts, _de, _te);
    }
  }

  /// <summary>
  /// Еженедельный интервал дат
  /// </summary>
  [DataContract]
  public class WeeklyIntervalCondition : DateBetweenCondition
  {
    [DataMember]
    private DayOfWeek _ds;
    [DataMember]
    private TimeSpan _ts;
    [DataMember]
    private DayOfWeek _de;
    [DataMember]
    private TimeSpan _te;

    #region [ static ]

    public static WeeklyIntervalCondition OneDay(DayOfWeek d)
    {
      return new WeeklyIntervalCondition(d, TimeSpan.Zero, d, TimeSpan.Zero);
    }

    public static WeeklyIntervalCondition Days(DayOfWeek ds, DayOfWeek de)
    {
      return new WeeklyIntervalCondition(ds, TimeSpan.Zero, de, TimeSpan.Zero);
    }

    #endregion

    public WeeklyIntervalCondition(DayOfWeek ds, TimeSpan ts, DayOfWeek de, TimeSpan te)
    {
      _ds = ds;
      _ts = ts;
      _de = de;
      _te = te;
    }

    #region [ DateBetweenCondition ]

    protected override DateTime? GetStartDate(EvaluationContext c)
    {
      DateTime dt = c.TimeCurrent;
      int i = TariffExtensions.WeekDays[_ds] - TariffExtensions.WeekDays[dt.DayOfWeek];
      if (TariffExtensions.WeekDays[_de] < TariffExtensions.WeekDays[_ds] && TariffExtensions.WeekDays[dt.DayOfWeek] < TariffExtensions.WeekDays[_de])
        i -= 7; //reverse and this week, shift start to previous week

      return dt.Date.AddDays(i).Add(_ts);
    }

    protected override DateTime? GetEndDate(EvaluationContext c)
    {
      //int i = Tariff50Extensions.WeekDays[_de] - 1;
      //DayOfWeek de = Tariff50Extensions.WeekDays.ElementAt((i == 7) ? 0 : i).Key;

      DateTime dt = c.TimeCurrent;
      int i = TariffExtensions.WeekDays[_de] - TariffExtensions.WeekDays[dt.DayOfWeek];
      if (TariffExtensions.WeekDays[_de] < TariffExtensions.WeekDays[_ds] && TariffExtensions.WeekDays[dt.DayOfWeek] >= TariffExtensions.WeekDays[_de])
        i += 7; //reverse and in range, shift end to next week

      return dt.Date.AddDays(i).Add(_te);
    }

    protected override DateTime? GetNext(EvaluationContext c, DateTime? dt)
    {
      if (dt.HasValue)
        dt = dt.Value.AddDays(7);

      return dt;
    }

    protected override DateTime? GetPrevious(EvaluationContext c, DateTime? dt)
    {
      if (dt.HasValue)
        dt = dt.Value.AddDays(-7);

      return dt;
    }

    #endregion

    public override string ToString()
    {
      return String.Format("{0} {1} - {2} {3}", _ds.GetString(), _ts, _de.GetString(), _te);
    }
  }

  /// <summary>
  /// По рабочим дням
  /// </summary>
  [DataContract]
  public class WorkdayCondition : Condition
  {
    #region [ Condition ]

    public override EvaluationResult Evaluate(EvaluationContext context)
    {
      DayOfWeek d = context.TimeCurrent.DayOfWeek;
      var r = new EvaluationResult((d != DayOfWeek.Saturday) && (d != DayOfWeek.Sunday));
      DateTime dt = context.TimeCurrent.Date.Subtract(TimeSpan.FromDays(TariffExtensions.WeekDays[d] - 1));
      r.TimeStart = dt;
      if (r)
        r.TimeMax = dt.AddDays(5);
      else
        r.TimeNext = dt.AddDays(7); //next week

      return r;
    }

    #endregion

    public override string ToString()
    {
      return "Рабочие дни";
    }
  }

  /// <summary>
  /// По выходным дням
  /// </summary>
  [DataContract]
  public class WeekendCondition : Condition
  {
    #region [ Condition ]

    public override EvaluationResult Evaluate(EvaluationContext context)
    {
      DayOfWeek d = context.TimeCurrent.DayOfWeek;
      var r = new EvaluationResult((d == DayOfWeek.Saturday) || (d == DayOfWeek.Sunday));
      DateTime dt = context.TimeCurrent.Date.Add(TimeSpan.FromDays(6 - TariffExtensions.WeekDays[d]));
      r.TimeStart = dt;
      if (r)
        r.TimeMax = dt.AddDays(2);
      else
        r.TimeNext = dt;

      return r;
    }

    #endregion

    public override string ToString()
    {
      return "Выходные дни";
    }
  }

  /// <summary>
  /// Повторяющиеся интервалы
  /// </summary>
  [DataContract]
  public class RepeatIntervalCondition : Condition
  {
    [DataMember]
    private TimeSpan _start;
    [DataMember]
    private TimeSpan _duration;

    public RepeatIntervalCondition()
      : this(TimeSpan.Zero, Interval.Infinite)
    {
      //
    }

    public RepeatIntervalCondition(TimeSpan s, TimeSpan d)
    {
      _start = s;
      _duration = d;
    }

    #region [ Condition ]

    public override EvaluationResult Evaluate(EvaluationContext context)
    {
      DateTime dcs = context.TimeCurrent;
      DateTime dce = dcs + context.Duration;
      DateTime dts = context.GetLimitationBaseTime() + GetStart(context.RepeatIndex);
      TimeSpan d = (_duration == Interval.Infinite) ? context.TimeEnd - dts : _duration;
      DateTime dte = dts + d;

      bool b = false;
      if (dts < context.TimeEnd)
      {
        if (dcs < dts)
          b = dce > dts;
        else
          b = (dcs < dte);
      }

      var r = new EvaluationResult(b);
      r.TimeStart = dts;
      if (b)
        r.TimeMax = dte;
      else
        r.TimeNext = dts + d;

      return r;
    }

    public override DateTime? GetTimeMax(DateTime timeStart)
    {
      return (_duration == Interval.Infinite) ? DateTime.MaxValue : timeStart + _duration;
    }

    #endregion

    private TimeSpan GetStart(int index)
    {
      if (index < 0)
        throw new InvalidOperationException("Invalid index");

      return _start + TimeSpan.FromSeconds(_duration.TotalSeconds * index);
    }

    public override string ToString()
    {
      return ToString(0);
    }

    public string ToString(int index)
    {
      TimeSpan ts = GetStart(index);
      string s = (_duration == Interval.Infinite) ? Double.PositiveInfinity.ToString() : _duration.ToString();
      //string r = (_repeat > 1) ? String.Format(" x{0}", (_repeat == Interval.Unlimited) ? Double.PositiveInfinity.ToString() : _repeat.ToString()) : String.Empty;

      return String.Format("{0} - {1}", ts, s);
    }
  }
}