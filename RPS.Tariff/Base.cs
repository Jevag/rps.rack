﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace rps.Data
{
  /// <summary>
  /// Способ расчета по интервалу
  /// </summary>
  [DataContract]
  public enum IntervalProcessingMode : int
  {
    [EnumMember]
    Inherit = 0,
    [EnumMember]
    Maximum = 1,
    [EnumMember]
    Exact = 2,
    [EnumMember]
    Integer = 3
  }

  /// <summary>
  /// Способ выбора времени в начале
  /// </summary>
  [DataContract]
  public enum InitialMode : int
  {
    [EnumMember]
    Inherit = 0,
    [EnumMember]
    Override = 1
  }

  /// <summary>
  /// Способ выбора защитного интервала
  /// </summary>
  [DataContract]
  public enum ProtectionMode : int
  {
    [EnumMember]
    Inherit = 0,
    [EnumMember]
    Override = 1
  }

  /// <summary>
  /// Способ перехода между правилами
  /// </summary>
  [DataContract]
  public enum SwitchMode : int
  {
    [EnumMember]
    Inherit = 0,
    [EnumMember]
    PreviousPriority = 1,
    [EnumMember]
    Match = 2,
    [EnumMember]
    NextPriority = 3
  }

  /// <summary>
  /// Интервал
  /// </summary>
  [DataContract]
  public class Interval : ICloneable
  {
    #region [ const ]

    public const int Unlimited = Int32.MaxValue;

    #endregion

    [DataMember]
    public Guid ID;
    [DataMember]
    public TimeSpan Start;
    [DataMember]
    public TimeSpan Duration;
    [DataMember]
    public decimal Amount;
    [DataMember]
    public TimeSpan Period; //must be <= Duration
    [DataMember]
    public IntervalProcessingMode ProcessingMode;
    [DataMember]
    public TimeSpan Protection;
    [DataMember]
    public ProtectionMode ProtectionMode;
    [DataMember]
    public string Description;

    #region [ properties ]

    public TimeSpan End
    {
      get { return (Duration == Infinite) ? Infinite : (Start + Duration); }
    }

    #endregion

    #region [ static ]

    public static TimeSpan Infinite = TimeSpan.MaxValue;
    
    #endregion
    
    public Interval()
      : this(Guid.NewGuid())
    {
      //
    }

    public Interval(Guid id)
    {
      ID = id;
      Start = TimeSpan.Zero;
      Duration = TimeSpan.Zero;
      Amount = 0;
      Period = TimeSpan.Zero;
      ProcessingMode = IntervalProcessingMode.Inherit;
      Protection = TimeSpan.Zero;
      ProtectionMode = ProtectionMode.Inherit;
      Description = String.Empty;
    }

    public Interval(Interval interval)
	  {
      ID = interval.ID;
      Start = interval.Start;
      Duration = interval.Duration;
      Amount = interval.Amount;
      Period = interval.Period;
      ProcessingMode = interval.ProcessingMode;
      Protection = interval.Protection;
      ProtectionMode = interval.ProtectionMode;
      Description = interval.Description;
	  }

    #region [ ICloneable ]

    public object Clone()
    {
      return new Interval(this);
    }

    #endregion

    public override string ToString()
    {
      string s = (Duration == Interval.Infinite) ? Double.PositiveInfinity.ToString() : End.ToString();
      string d = String.IsNullOrWhiteSpace(Description) ? String.Empty : String.Format(" ({0})", Description);

      return String.Format("{0} - {1}, {2:C}/{3}{4}", Start, s, Amount, Period, d);
    }
  }

  /// <summary>
  /// Правило
  /// </summary>
  [DataContract]
  public class Rule : ICloneable
  {
    [DataMember]
    public Guid ID;
    [DataMember]
    public string Name;
    [DataMember]
    public List<Interval> Intervals; //must not intersect
    [DataMember]
    public InitialMode InitialMode;
    [DataMember]
    public TimeSpan Initial;
    [DataMember]
    public decimal InitialAmount;
    [DataMember]
    public IntervalProcessingMode ProcessingMode;
    [DataMember]
    public TimeSpan Protection;
    [DataMember]
    public ProtectionMode ProtectionMode;
    [DataMember]
    public ICondition Match;
    [DataMember]
    public SwitchMode SwitchMode;
    [DataMember]
    public string Description;

    public Rule()
      : this(Guid.NewGuid())
    {
      //
    }

    public Rule(Guid id)
    {
      ID = id;
      Name = String.Empty;
      Intervals = new List<Interval>();
      InitialMode = InitialMode.Inherit;
      Initial = TimeSpan.Zero;
      InitialAmount = 0;
      ProcessingMode = IntervalProcessingMode.Inherit;
      Protection = TimeSpan.Zero;
      ProtectionMode = ProtectionMode.Inherit;
      Match = Condition.Never;
      SwitchMode = SwitchMode.Inherit;
      Description = String.Empty;
    }

    public Rule(Rule rule)
    {
      ID = rule.ID;
      Name = rule.Name;
      Intervals = rule.Intervals.Select(i => (Interval)i.Clone()).ToList();
      InitialMode = rule.InitialMode;
      Initial = rule.Initial;
      InitialAmount = rule.InitialAmount;
      ProcessingMode = rule.ProcessingMode;
      Protection = rule.Protection;
      Match = rule.Match;
      SwitchMode = rule.SwitchMode;
      Description = rule.Description;
    }

    #region [ ICloneable ]

    public object Clone()
    {
      return new Rule(this);
    }

    #endregion

    public override string ToString()
    {
      string d = String.IsNullOrWhiteSpace(Description) ? String.Empty : String.Format(" ({0})", Description);

      return String.Format("{0} : {1}{2}", Name, Match, d);
    }
  }
}