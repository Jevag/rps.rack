﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace rps.Data
{
  public static class TariffExtensions
  {
    internal static Dictionary<DayOfWeek, int> WeekDays = new Dictionary<DayOfWeek, int>()
    {
      { DayOfWeek.Monday, 1 },
      { DayOfWeek.Tuesday, 2 },
      { DayOfWeek.Wednesday, 3 },
      { DayOfWeek.Thursday, 4 },
      { DayOfWeek.Friday, 5 },
      { DayOfWeek.Saturday, 6 },
      { DayOfWeek.Sunday, 7 }
    };

    public static ICondition Add(this ICondition condition, ICondition newCondition)
    {
      MultipleCondition mc = condition as MultipleCondition;
      if (mc == null)
      {
        mc = new MultipleCondition();
        mc.Append(condition);
      }

      mc.Append(newCondition);

      return mc;
    }

    public static ICondition Concat(this ICondition condition1, ICondition condition2)
    {
      MultipleCondition mc = new MultipleCondition();
      mc.Append(condition1);
      mc.Append(condition2);

      return mc;
    }

    public static ICondition Concat(this IEnumerable<ICondition> conditions)
    {
      MultipleCondition mc = new MultipleCondition();
      foreach (var v in conditions)
        mc.Append(v);

      return mc;
    }

    public static ICondition Weekend(this ICondition condition)
    {
      return condition.Add(new WeekendCondition());
    }

    public static ICondition Workday(this ICondition condition)
    {
      return condition.Add(new WorkdayCondition());
    }

    public static ICondition Any(this ICondition condition)
    {
      MultipleCondition mc = condition as MultipleCondition;
      if (mc != null)
        mc.Any = true;

      return condition;
    }

    public static ICondition Mode(this ICondition condition, SwitchMode mode)
    {
      condition.Mode = mode;

      return condition;
    }

    public static Limitation Bind(this Limitation limitation, Rule rule)
    {
      limitation.Scope = LimitationScope.Rule;
      limitation.RuleID = rule.ID;

      return limitation;
    }

    public static Limitation Bind(this Limitation limitation, Rule rule, Interval interval)
    {
      limitation.Scope = LimitationScope.Interval;
      limitation.RuleID = rule.ID;
      limitation.IntervalID = interval.ID;

      return limitation;
    }

    public static DateTime GetLimitationBaseTime(this EvaluationContext e)
    {
      DateTime dt = DateTime.MaxValue;
      switch (e.Limitation.Scope)
      {
        case LimitationScope.Global:
          dt = e.TimeStart;
          break;
        case LimitationScope.Rule:
          dt = e.TimeRule;
          break;
        case LimitationScope.Interval:
          dt = e.TimeCurrent;
          break;
      }

      return dt;
    }

    public static SwitchMode GetLimitationSwitchMode(this EvaluationContext e, SwitchMode m)
    {
      if (m == SwitchMode.Inherit)
      {
        Rule rule = e.Tariff.Rules.Find(r => r.ID == e.Limitation.RuleID);

        return ((rule == null) || (rule.SwitchMode == SwitchMode.Inherit)) ? e.Tariff.SwitchMode : rule.SwitchMode;
      }

      return m;
    }

    public static string GetString(this DayOfWeek w)
    {
      string s = CultureInfo.CurrentUICulture.DateTimeFormat.DayNames[(int)w];
      if (String.IsNullOrEmpty(s))
        return s;

      StringBuilder sb = new StringBuilder(s.Length);
      sb.Append(Char.ToUpper(s[0]));
      sb.Append(s, 1, s.Length - 1);

      return sb.ToString();
    }

    internal static string GetString(this DateTime? dt)
    {
      return dt.HasValue ? dt.ToString() : "*";
    }

    public static string GetString(this IEnumerable items, string itemSeparator)
    {
      StringBuilder sb = new StringBuilder(256);
      foreach (object item in items)
      {
        if (sb.Length > 0)
        {
          sb.Append(itemSeparator);
          sb.Append(' ');
        }

        string s = String.Empty;
        if (item != null)
          s = item.ToString();

        sb.Append(s);
      }

      return sb.ToString();
    }

    public static DateTime TrimMilliseconds(this DateTime dt)
    {
      if (dt.Millisecond == 0)
        return dt;

      return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, 0);
    }
  }
}