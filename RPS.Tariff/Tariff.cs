﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace rps.Data
{
  /// <summary>
  /// Тариф
  /// </summary>
  [DataContract]
  public class Tariff : ICloneable
  {
    [DataMember]
    private Guid _id;
    [DataMember]
    private string _name;
    [DataMember]
    private string _description;
    [DataMember]
    private TimeSpan _freeTimeAtStart;
    [DataMember]
    private TimeSpan _freeTimeAfterPayment;
    [DataMember]
    public IntervalProcessingMode _processingMode;
    [DataMember]
    public TimeSpan _initial;
    [DataMember]
    public decimal _initialAmount;
    [DataMember]
    public TimeSpan _protection;
    [DataMember]
    public SwitchMode _switchMode;
    [DataMember]
    private List<Rule> _rules;
    [DataMember]
    private List<Limitation> _limitations;

    #region [ properties ]

    public Guid ID
    {
      get { return _id; }
      set { _id = value; }
    }

    public string Name
    {
      get { return _name; }
      set { _name = value; }
    }

    public string Description
    {
      get { return _description; }
      set { _description = value; }
    }

    public TimeSpan FreeTimeAtStart
    {
      get { return _freeTimeAtStart; }
      set { _freeTimeAtStart = value; }
    }

    public TimeSpan FreeTimeAfterPayment
    {
      get { return _freeTimeAfterPayment; }
      set { _freeTimeAfterPayment = value; }
    }

    public IntervalProcessingMode ProcessingMode
    {
      get { return _processingMode; }
      set { _processingMode = value; }
    }

    public TimeSpan Initial
    {
      get { return _initial; }
      set { _initial = value; }
    }

    public decimal InitialAmount
    {
      get { return _initialAmount; }
      set { _initialAmount = value; }
    }

    public TimeSpan Protection
    {
      get { return _protection; }
      set { _protection = value; }
    }

    public SwitchMode SwitchMode
    {
      get { return _switchMode; }
      set { _switchMode = value; }
    }

    public List<Rule> Rules
    {
      get { return _rules; }
    }

    public List<Limitation> Limitations
    {
      get { return _limitations; }
    }

    #endregion

    public Tariff()
      : this(Guid.NewGuid())
    {
      //
    }

    public Tariff(Guid id)
    {
      _name = String.Empty;
      _description = String.Empty;
      _freeTimeAtStart = TimeSpan.Zero;
      _freeTimeAfterPayment = TimeSpan.Zero;
      _processingMode = IntervalProcessingMode.Maximum;
      _initial = TimeSpan.Zero;
      _initialAmount = 0;
      _protection = TimeSpan.Zero;
      _switchMode = SwitchMode.PreviousPriority;
      _rules = new List<Rule>();
      _limitations = new List<Limitation>();
    }

    public Tariff(Tariff tariff)
    {
      _id = tariff.ID;
      _name = tariff.Name;
      _description = tariff.Description;
      _freeTimeAtStart = tariff.FreeTimeAtStart;
      _freeTimeAfterPayment = tariff.FreeTimeAfterPayment;
      _processingMode = tariff.ProcessingMode;
      _initial = tariff.Initial;
      _initialAmount = tariff.InitialAmount;
      _protection = tariff.Protection;
      _switchMode = tariff.SwitchMode;
      _rules = tariff.Rules.Select(r => (Rule)r.Clone()).ToList();
      _limitations = tariff.Limitations.Select(l => (Limitation)l.Clone()).ToList();
    }

    #region [ ICloneable ]

    public object Clone()
    {
      return new Tariff(this);
    }

    #endregion

    public override string ToString()
    {
      return _name;
    }
  }
}