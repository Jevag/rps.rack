﻿using System.Linq;
using System.Diagnostics;
using System.Collections.ObjectModel;
using CommonClassLibrary;
using System;

namespace Communications
{
    public class OperatorTalk
    {
        private string serverURL, deviceId, RealServerURL;
        bool run = false;
        private bool answered = false;
        Log log;
        public bool Answered
        {
            get
            {
                return answered;
            }
        }
        public bool Run
        {
            get
            {
                return run;
            }
        }

        //Thread StatusThread;

        public string ServerURL
        {
            get
            {
                return serverURL;
            }
            set
            {
                if (serverURL != value)
                {
                    Stop();
                    serverURL = value;
                }
            }
        }

        public string DeviceId
        {
            get
            {
                return deviceId;
            }
            set
            {
                if (deviceId != value)
                {
                    Stop();
                    deviceId = value;
                }
            }
        }

        public void Stop()
        {
            Process[] pl = Process.GetProcessesByName("chrome");
            foreach (Process p in pl)
            {
                try
                {
                    p.Kill();
                }
                catch { }
            }
            pl = Process.GetProcessesByName("GoogleChromePortable");
            foreach (Process p in pl)
            {
                try
                {
                    p.Kill();
                }
                catch { }
            }
        }


        public OperatorTalk(string ServerURL, string DeviceId, Log log)
        {
            this.log = log;
            serverURL = ServerURL;
            deviceId = DeviceId;
        }
        bool UnableStartGoogleChromeLogged = false;
        public void Init()
        {
            try
            {
                if ((serverURL != null) && (serverURL != "") && (deviceId != null))
                {
                    RealServerURL = serverURL;
                    if (Process.GetProcesses().Count(x => x.ProcessName == "GoogleChromePortable") == 0)
                    {
                        Process pr = new Process();
                        pr.StartInfo.FileName = @"c:\GoogleChromePortable\GoogleChromePortable.exe";
                        if (RealServerURL.IndexOf("http://") >= 0)
                            RealServerURL.Replace("http://", "https://");
                        else
                        if (RealServerURL.IndexOf("https://") < 0)
                            RealServerURL = "https://" + serverURL;
                        //pr.StartInfo.Arguments = "--silent-launch --use-fake-ui-for-media-stream " + serverURL + "/station.html?" + deviceId.ToUpper();
                        pr.StartInfo.Arguments = "--use-fake-ui-for-media-stream " + RealServerURL + "/station.html?" + deviceId.ToUpper();
                        pr.StartInfo.UseShellExecute = false;
                        pr.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
                        pr.Start();
                        pr.WaitForInputIdle();
                    }
                }
            }
            catch
            {
                if (!UnableStartGoogleChromeLogged)
                {
                    if (log != null)
                        log.ToLog(2, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " OperatoeTalk: Init: " + "Unable start GoogleChromePortable.");
                    UnableStartGoogleChromeLogged = true;
                }
            }
        }

        public void Call()
        {
            Init();
            answered = false;
            run = true;
        }

        public void Answer()
        {
            answered = true;
        }
        public void HangUp()
        {
            answered = false;
            run = false;
        }
    }
}
