﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace Communications
{
    public class SinglePassage
    {
        private string serverURL, deviceId;
        bool run = false;

        public string ServerURL
        {
            get { return serverURL; }
            set
            {
                if (serverURL != value)
                {
                    //Stop();
                    serverURL = value;
                }
            }
        }

        public string DeviceId
        {
            get { return deviceId; }
            set
            {
                if (deviceId != value)
                {
                    //Stop();
                    deviceId = value;
                }
            }
        }

        public SinglePassage(string ServerURL, string DeviceId)
        {
            serverURL = ServerURL;
            deviceId = DeviceId;
            /*
            StatusThread = new Thread(PoolThread);
            StatusThread.Name = "CallOperatorThread";
            StatusThread.IsBackground = true;
            StatusThread.Start();
            */
        }




    }
}
