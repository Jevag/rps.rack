﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Communications.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Communications.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not supported commnad.
        /// </summary>
        public static string CardDispenserCommandError {
            get {
                return ResourceManager.GetString("CardDispenserCommandError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ошибка взаимодейсвтия с диспенсером карт. Проверьте подключение..
        /// </summary>
        public static string CardDispenserCommunicationsError {
            get {
                return ResourceManager.GetString("CardDispenserCommunicationsError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to нет карт.
        /// </summary>
        public static string CardDispenserEmpty {
            get {
                return ResourceManager.GetString("CardDispenserEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Операция не завершена.
        /// </summary>
        public static string CardDispenserFailed {
            get {
                return ResourceManager.GetString("CardDispenserFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to карта в губах.
        /// </summary>
        public static string CardDispenserInLips {
            get {
                return ResourceManager.GetString("CardDispenserInLips", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to карта под ридером.
        /// </summary>
        public static string CardDispenserInReader {
            get {
                return ResourceManager.GetString("CardDispenserInReader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to карта застряла.
        /// </summary>
        public static string CardDispenserJam {
            get {
                return ResourceManager.GetString("CardDispenserJam", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to нет карты в тракте.
        /// </summary>
        public static string CardDispenserNotInAPath {
            get {
                return ResourceManager.GetString("CardDispenserNotInAPath", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Операция успешно завершена.
        /// </summary>
        public static string CardDispenserSuccessfully {
            get {
                return ResourceManager.GetString("CardDispenserSuccessfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to неопределен.
        /// </summary>
        public static string CardDispenserUnknown {
            get {
                return ResourceManager.GetString("CardDispenserUnknown", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Номер карты {0}.
        /// </summary>
        public static string CardId {
            get {
                return ResourceManager.GetString("CardId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ошибка взаимодействия с ридерм карт. Проверьте подключение..
        /// </summary>
        public static string CardReaderCommunicationsError {
            get {
                return ResourceManager.GetString("CardReaderCommunicationsError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ошибка взаимодействия с банкнотоприемником. Проверьте подключение..
        /// </summary>
        public static string CashAcceptorCommunicationError {
            get {
                return ResourceManager.GetString("CashAcceptorCommunicationError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Статус банкнотоприемника изменился с {0} на {1}.
        /// </summary>
        public static string CashAcceptorStatusChanged {
            get {
                return ResourceManager.GetString("CashAcceptorStatusChanged", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сообщение повреждено.
        /// </summary>
        public static string CashDispenserBCCError {
            get {
                return ResourceManager.GetString("CashDispenserBCCError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ошибка взаимодействия с диспенсером банкнот. Проверьте подключение..
        /// </summary>
        public static string CashDispenserCommunicationError {
            get {
                return ResourceManager.GetString("CashDispenserCommunicationError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Кассовый чек.
        /// </summary>
        public static string CashReceipt {
            get {
                return ResourceManager.GetString("CashReceipt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ошибка взаимодействия с рециклером банкнот. Проверьте подключение..
        /// </summary>
        public static string CashRecyclerCommunicationError {
            get {
                return ResourceManager.GetString("CashRecyclerCommunicationError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ВЫДАНО СДАЧИ:   {0}.
        /// </summary>
        public static string ChangesIssued {
            get {
                return ResourceManager.GetString("ChangesIssued", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ошибка закрытия смены:.
        /// </summary>
        public static string CloseShiftError {
            get {
                return ResourceManager.GetString("CloseShiftError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to № док..
        /// </summary>
        public static string DocNo {
            get {
                return ResourceManager.GetString("DocNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ошибка инкасации №: {0} {1}.
        /// </summary>
        public static string EncashmentError {
            get {
                return ResourceManager.GetString("EncashmentError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Время въезда:  {0}.
        /// </summary>
        public static string EnterTime {
            get {
                return ResourceManager.GetString("EnterTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выезд до.
        /// </summary>
        public static string ExitBefore {
            get {
                return ResourceManager.GetString("ExitBefore", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Part of returned data may be corrupt..
        /// </summary>
        public static string HID_0x62_0x81 {
            get {
                return ResourceManager.GetString("HID_0x62_0x81", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to End of file reached before writing all Lc bytes..
        /// </summary>
        public static string HID_0x62_0x82 {
            get {
                return ResourceManager.GetString("HID_0x62_0x82", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Requested key slot does not exist..
        /// </summary>
        public static string HID_0x62_0x83 {
            get {
                return ResourceManager.GetString("HID_0x62_0x83", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No further information given (warning)..
        /// </summary>
        public static string HID_0x63_0x00 {
            get {
                return ResourceManager.GetString("HID_0x63_0x00", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Loading/updating is not allowed..
        /// </summary>
        public static string HID_0x63_0x81 {
            get {
                return ResourceManager.GetString("HID_0x63_0x81", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Card key not supported..
        /// </summary>
        public static string HID_0x63_0x82 {
            get {
                return ResourceManager.GetString("HID_0x63_0x82", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reader key not supported..
        /// </summary>
        public static string HID_0x63_0x83 {
            get {
                return ResourceManager.GetString("HID_0x63_0x83", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Plaintext transmission not supported..
        /// </summary>
        public static string HID_0x63_0x84 {
            get {
                return ResourceManager.GetString("HID_0x63_0x84", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Secured transmission not supported..
        /// </summary>
        public static string HID_0x63_0x85 {
            get {
                return ResourceManager.GetString("HID_0x63_0x85", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Volatile memory is not available..
        /// </summary>
        public static string HID_0x63_0x86 {
            get {
                return ResourceManager.GetString("HID_0x63_0x86", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Non-volatile memory is not available..
        /// </summary>
        public static string HID_0x63_0x87 {
            get {
                return ResourceManager.GetString("HID_0x63_0x87", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Key number not valid..
        /// </summary>
        public static string HID_0x63_0x88 {
            get {
                return ResourceManager.GetString("HID_0x63_0x88", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Key length is not correct..
        /// </summary>
        public static string HID_0x63_0x89 {
            get {
                return ResourceManager.GetString("HID_0x63_0x89", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Card Execution Error..
        /// </summary>
        public static string HID_0x64_0x00 {
            get {
                return ResourceManager.GetString("HID_0x64_0x00", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Memory failure (unsuccessful writing)..
        /// </summary>
        public static string HID_0x65_0x81 {
            get {
                return ResourceManager.GetString("HID_0x65_0x81", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Command incompatible..
        /// </summary>
        public static string HID_0x69_0x81 {
            get {
                return ResourceManager.GetString("HID_0x69_0x81", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Security status not satisfied..
        /// </summary>
        public static string HID_0x69_0x82 {
            get {
                return ResourceManager.GetString("HID_0x69_0x82", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Authentication cannot be done..
        /// </summary>
        public static string HID_0x69_0x83 {
            get {
                return ResourceManager.GetString("HID_0x69_0x83", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reference key not useable..
        /// </summary>
        public static string HID_0x69_0x84 {
            get {
                return ResourceManager.GetString("HID_0x69_0x84", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Command not allowed..
        /// </summary>
        public static string HID_0x69_0x86 {
            get {
                return ResourceManager.GetString("HID_0x69_0x86", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Key number not valid..
        /// </summary>
        public static string HID_0x69_0x88 {
            get {
                return ResourceManager.GetString("HID_0x69_0x88", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Function not supported..
        /// </summary>
        public static string HID_0x6A_0x81 {
            get {
                return ResourceManager.GetString("HID_0x6A_0x81", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File not found or addressed block or byte does not exist..
        /// </summary>
        public static string HID_0x6A_0x82 {
            get {
                return ResourceManager.GetString("HID_0x6A_0x82", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wrong parameter P1 or P2..
        /// </summary>
        public static string HID_0x6B_0x00 {
            get {
                return ResourceManager.GetString("HID_0x6B_0x00", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to More data available than requested; {0} returns available data size..
        /// </summary>
        public static string HID_0x6C {
            get {
                return ResourceManager.GetString("HID_0x6C", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error 0x{0} (refer to the MIFARE DESFire data sheet)..
        /// </summary>
        public static string HID_0x91 {
            get {
                return ResourceManager.GetString("HID_0x91", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reader not exists. Check driver and device..
        /// </summary>
        public static string HIDCommunicationError {
            get {
                return ResourceManager.GetString("HIDCommunicationError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in Authenticate on .
        /// </summary>
        public static string HIDErrorAuthenticate {
            get {
                return ResourceManager.GetString("HIDErrorAuthenticate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in GetIdCard on.
        /// </summary>
        public static string HIDErrorGetCardId {
            get {
                return ResourceManager.GetString("HIDErrorGetCardId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in LoadKey on .
        /// </summary>
        public static string HIDErrorLoadKey {
            get {
                return ResourceManager.GetString("HIDErrorLoadKey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in Read1 on .
        /// </summary>
        public static string HIDErrorRead1 {
            get {
                return ResourceManager.GetString("HIDErrorRead1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in Read1 - not Authenticated on .
        /// </summary>
        public static string HIDErrorRead1A {
            get {
                return ResourceManager.GetString("HIDErrorRead1A", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in Write1 on .
        /// </summary>
        public static string HIDErrorWrite1 {
            get {
                return ResourceManager.GetString("HIDErrorWrite1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in Write1 - not Authenticated on.
        /// </summary>
        public static string HIDErrorWrite1A {
            get {
                return ResourceManager.GetString("HIDErrorWrite1A", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable connect to Card. Error: .
        /// </summary>
        public static string HIDUnableConnectToCard {
            get {
                return ResourceManager.GetString("HIDUnableConnectToCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable connect to reader .
        /// </summary>
        public static string HIDUnableConnectToReader {
            get {
                return ResourceManager.GetString("HIDUnableConnectToReader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable connect to reader . Empty readerName..
        /// </summary>
        public static string HIDUnableConnectToReaderEmpty {
            get {
                return ResourceManager.GetString("HIDUnableConnectToReaderEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ПРИНЯТО:  {0}.
        /// </summary>
        public static string InCash {
            get {
                return ResourceManager.GetString("InCash", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ИНН.
        /// </summary>
        public static string INN {
            get {
                return ResourceManager.GetString("INN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to не отвечает.
        /// </summary>
        public static string NotResponse {
            get {
                return ResourceManager.GetString("NotResponse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Открытие смены.
        /// </summary>
        public static string OpenShift {
            get {
                return ResourceManager.GetString("OpenShift", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Не удалось открыть смену ОШИБКА:.
        /// </summary>
        public static string OpenShiftError {
            get {
                return ResourceManager.GetString("OpenShiftError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Terminal ID: .
        /// </summary>
        public static string OpName {
            get {
                return ResourceManager.GetString("OpName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ОПЛАЧЕНО:.
        /// </summary>
        public static string Paid {
            get {
                return ResourceManager.GetString("Paid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Оплата за парковку.
        /// </summary>
        public static string PaymentForParking {
            get {
                return ResourceManager.GetString("PaymentForParking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Время оплаты.
        /// </summary>
        public static string PaymentTime {
            get {
                return ResourceManager.GetString("PaymentTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ошибка печати чека ОШИБКА:.
        /// </summary>
        public static string PrintChequeError {
            get {
                return ResourceManager.GetString("PrintChequeError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to принтер не включен или не установлено соединение с ним.
        /// </summary>
        public static string PrinterNotConnect {
            get {
                return ResourceManager.GetString("PrinterNotConnect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ККМ.
        /// </summary>
        public static string SerNo {
            get {
                return ResourceManager.GetString("SerNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BCC in Slave er.
        /// </summary>
        public static string SlaveBCCError {
            get {
                return ResourceManager.GetString("SlaveBCCError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Таймаут при запросе.
        /// </summary>
        public static string TimeOut {
            get {
                return ResourceManager.GetString("TimeOut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Включая НДС ({0}%) -  {1}.
        /// </summary>
        public static string VAT {
            get {
                return ResourceManager.GetString("VAT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to waiting.
        /// </summary>
        public static string Waiting {
            get {
                return ResourceManager.GetString("Waiting", resourceCulture);
            }
        }
    }
}
