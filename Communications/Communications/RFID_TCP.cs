﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Communications
{
    public class RFID_TCP
    {
        public struct Antenna
        {
            public byte Number;
            public byte Status;
            public byte RSSI;
            public int PhaseAngle;
        }

        public struct RFIDInfo
        {
            public byte AType; //Тип антенны
            public string PC; //У нас 3000
            public string SNR; //Самое важное!!!
            public List<Antenna> AntList;
        }

        public string Host { get; set; }
        public Int32 Port { get; set; }

        public byte[] RecBuff;

        //Доступность антенн!!!
        public bool A1Enabled { get; set; }
        public bool A2Enabled { get; set; }
        public bool A3Enabled { get; set; }
        public bool A4Enabled { get; set; }

        public List<RFIDInfo> RFIDList { get; set; }

        byte CRC16_1 = 0x00;
        byte CRC16_2 = 0x00;

        public RFID_TCP(string ip, Int32 prt)
        {
            Host = ip;
            Port = prt;
        }

        TcpClient client;

        public string Status { get; set; }



        public byte AntennasByte()
        {
            byte a = 0x00;
            if (A1Enabled) a += 0x01;
            if (A2Enabled) a += 0x02;
            if (A3Enabled) a += 0x04;
            if (A4Enabled) a += 0x08;
            return a;
        }

        public async Task<bool> SendRequest()
        {
            try
            {
                TcpClient client = new TcpClient();
                IAsyncResult ar = client.BeginConnect(Host, Port, null, null);

                WaitHandle wh;
                wh = ar.AsyncWaitHandle;

                if (!ar.AsyncWaitHandle.WaitOne(TimeSpan.FromMilliseconds(300), false))
                {
                    Status = "не отвечает";
                    return false;
                }
                //Debug.Print("Connected");
                byte Antennas = AntennasByte();

                ushort crc16 = CRC16(new byte[] { 0x02, 0x00, 0x0A, 0xFF, 0xB0, 0x01, 0x10, Antennas });

                byte[] crc_arr = BitConverter.GetBytes(crc16);

                CRC16_1 = crc_arr[0];
                CRC16_2 = crc_arr[1];

                //Debug.Print(CRC16_1.ToString("X"));
                //Debug.Print(CRC16_2.ToString("X"));


                byte[] SendCommand = new byte[] { 0x02, 0x00, 0x0A, 0xFF, 0xB0, 0x01, 0x10, Antennas, CRC16_1, CRC16_2 };

                NetworkStream stream = client.GetStream();
                await stream.WriteAsync(SendCommand, 0, SendCommand.Length);
                RecBuff = new byte[256];
                Int32 bytes = await stream.ReadAsync(RecBuff, 0, RecBuff.Length);
                stream.Close();
                ParseData();
                Status = "работает";
                return true;
            }
            catch (Exception ex) { return false; }
        }


        ushort CRC16(byte[] Data)
        {
            ushort CRC_POLYNOM = 0x8408;
            ushort CRC_PRESET = 0xFFFF;
            ushort crc = CRC_PRESET;

            for (int i = 0; i < Data.Length; i++)
            {
                crc ^= Data[i];

                //for (j = 0; j < 8; j++) { if (crc & 0x0001) crc = (crc >> 1) ^ CRC_POLYNOM; else crc = (crc >> 1); }

                for (int j = 0; j < 8; j++)
                {
                    if ((crc & 0x0001) > 0)
                    {
                        crc = (ushort)((crc >> 1) ^ CRC_POLYNOM);
                    }
                    else
                    {
                        crc = (ushort)(crc >> 1);
                    }
                }
            }
            return crc;
        }

        public void ParseData()
        {
            try
            {

                if (RecBuff[0] != 0x02) //Занято похоже
                {
                    Status = "не отвечает";
                    return;
                }
                if (RecBuff[5] != 0x00)
                {
                    //02 00 08 00 B0 84 BC 1D   Reader: RF-Warning
                    if (RecBuff[5] == 0x84)
                    {
                        Status = "несоответствие антенн";
                        return;
                    }
                    //02 00 08 00 B0 01 19 CE   Reader: No Transponder in Reader Field
                    if (RecBuff[5] == 0x01)
                    {
                        Status = "нет меток";
                        return;
                    }
                    Status = "unknown";
                    return;
                }

                //byte 7 = datasets

                byte datasets = RecBuff[6];

                //Find Last Byte

                int LastByte = 0;
                for (int i = 255; i > 0; i--)
                {
                    if (RecBuff[i] != 0x00)
                    {
                        LastByte = i;
                        break;
                    }
                }
                //Вычленяем дату
                RFIDList = new List<RFIDInfo>();

                int datacount = LastByte - 9; //2 ,байта контрольной суммы + 7 байт сначала

                //int partcount = datacount / datasets; //Кол-во байт в одной посылке

                //byte[] oneanswer = new byte[partcount];
                int k = 6;
                for (int i = 0; i < datasets; i++)
                {
                    RFIDInfo RInfo = new RFIDInfo();
                    RInfo.AntList = new List<Antenna>();
                    byte ant_count = 0x00;
                    //100 хватит...
                    string num = "";
                    for (int j = 0; j < 19; j++) //Наверное можно выбросить этот кусок, т.к. куски могут быть неравными...
                    {
                        byte PC1 = 0x00;
                        byte PC2 = 0x00;


                        k += 1;
                        if (j == 1)
                        {
                            RInfo.AType = RecBuff[k]; //0x84                        
                        }
                        //00-0E
                        if (j == 4)
                        {
                            PC1 = RecBuff[k];
                        }
                        if (j == 5)
                        {
                            PC2 = RecBuff[k];
                        }
                        RInfo.PC = PC1.ToString() + PC2.ToString();
                        //12 байт номер
                        if (j > 5 && j <= 17)
                        {
                            num += RecBuff[k].ToString("X2");
                        }
                        if (j == 17)
                        {
                            RInfo.SNR = num;
                        }
                        //Следующий байт - кол-во антенн!
                        if (j == 18)
                        {
                            ant_count = RecBuff[k];
                        }

                        //01 00 28 06 F8 00 00 02 00 29 00 28 00 00
                        //по 7 байт на антенну
                    }
                    //Антеннки пошли
                    for (int a = 0; a < ant_count; a++)
                    {
                        Antenna antenna = new Antenna();
                        for (int b = 0; b < 7; b++)
                        {
                            k = k + 1;
                            if (b == 0)
                            {
                                antenna.Number = RecBuff[k];
                            }
                            if (b == 1)
                            {
                                antenna.Status = RecBuff[k];
                            }
                            if (b == 2)
                            {
                                antenna.RSSI = RecBuff[k];
                            }
                            if (b == 4)
                            {
                                antenna.PhaseAngle = BitConverter.ToInt16(new byte[] { RecBuff[k - 1], RecBuff[k] }, 0);
                            }
                        }
                        RInfo.AntList.Add(antenna);
                    }
                    RFIDList.Add(RInfo);
                }
                Status = "работает";
            }
            catch (Exception ex) { }
        }
    }
}
