﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Communications
{
    public class TabloUDP
    {
        public string Host { get; set; }
        public string Port { get; set; }

        public int ColorIndex { get; set; }

        public int CurrentValue {get;set;}

        public string Status { get; set; }

        private static ushort CRC16(byte[] data)
        {
            ushort wCRC = 0;
            for (int i = 0; i < data.Length; i++)
            {
                wCRC ^= (ushort)(data[i] << 8);
                for (int j = 0; j < 8; j++)
                {
                    if ((wCRC & 0x8000) != 0)
                        wCRC = (ushort)((wCRC << 1) ^ 0x1021);
                    else
                        wCRC <<= 1;
                }
            }
            return wCRC;
        }

        public bool UDPPing()
        {
            UdpClient client = new UdpClient();

            IAsyncResult ar = client.Client.BeginConnect(Host, Convert.ToInt32(Port), null, null);
            WaitHandle wh;
            wh = ar.AsyncWaitHandle;
            if (!ar.AsyncWaitHandle.WaitOne(TimeSpan.FromMilliseconds(200), false))
            {
                Status = "не отвечает";
                return false;
            }
            Status = "Ping Ok";
            return true;
        }
        /*
        public void SendPing()
        {
            try
            {
                //Пинг с отдачей...
                Ping pingSender = new Ping();
                //pingSender.PingCompleted += PingSender_PingCompleted;
                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = Encoding.ASCII.GetBytes(data);

                // Wait 10 seconds for a reply.
                int timeout = 2000;
                PingReply reply = pingSender.Send(Host, timeout, buffer);
                if (reply.Status == IPStatus.Success)
                {
                    Status = "Ping Ok";
                }
                else
                {
                    Status = "не отвечает";
                }
            }
            catch (Exception ex)
            {
                Status = "не отвечает";
            }
        }
        */


        /*0xFE,0x0F,0x01,0x01，0x01，0x24，0x01，0x02，0x03，0x04，0x
05，0x06，0x07，0x08，0xFF,0x61，0x00 */

        //1 байт - всегда 0xFE
        //2 байт - длина посылки, исключая 2 байта crc
        //3 байт -адрес кому вещать, если 0, то всем, а так с 0 до 255
        //4 байт - отвечает за контент, 1 если вывод цифр, 2 если яркость, 3 время, итд смотреть в протоколе
        //5,6 байт
        //контент 3 цифры...5,6,7
        //8 байт - фиксировано 0xFF
        //9,10 байт - контрольная сумма
        public void SendChar(string chars, string color)
        {
            try
            {

                using (UdpClient sender = new UdpClient())
                {
                    // Создаем endPoint по информации об удаленном хосте
                    IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(Host), Convert.ToInt32(Port));
                    //byte[] tmp = new byte[] {0xFE,0x0F,0x01,0x01,0x01,0x24,0x01,0x02,0x03,
                    ////    0x04,0x05,0x06,0x07,0x08,0xFF,0x61,0x00};
                    byte[] tmp = new byte[17];

                    tmp[0] = 0xFE;
                    tmp[1] = 0x0F;
                    tmp[2] = 0x01;

                    tmp[3] = 0x01; //Команда
                    tmp[4] = 0x01;
                    tmp[5] = 0x24; //Длина посылки

                    tmp[6] = 0x01;
                    tmp[7] = 0x02;
                    tmp[8] = 0x03;
                    tmp[9] = 0x04;

                    byte ch1 = Convert.ToByte(color.Substring(0, 1));

                    byte ch2 = Convert.ToByte(chars.Substring(0, 1));
                    byte ch3 = Convert.ToByte(chars.Substring(1, 1));
                    byte ch4 = Convert.ToByte(chars.Substring(2, 1));

                    tmp[10] = ch1;
                    tmp[11] = ch2;
                    tmp[12] = ch3;
                    tmp[13] = ch4;

                    tmp[14] = 0xFF;
                    ushort crc = CRC16(new byte[] { tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7], tmp[8], tmp[9], tmp[10], tmp[11], tmp[12], tmp[13], tmp[14] });
                    byte[] crc_bytes = BitConverter.GetBytes(crc);
                    tmp[15] = crc_bytes[1];
                    tmp[16] = crc_bytes[0];

                    sender.Send(tmp, tmp.Length, endPoint);
                }
            }
            catch (Exception ex)
            { }
        }

        //Пока не делаем...
        public void SendBrightness()
        {

        }

    }
}
