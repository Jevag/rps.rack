﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Google.Protobuf;

namespace Slave3Testing
{
    public class SlaveV3
    {
        //private const string getinfo = "\xc0\x82\x03\x00\x23"; //C0 82 03 00 23



        private string status;                          // статус Slave
        private string command;

        private int txSize = 0;
        private int rxSize = 0;
        private int Length = 0;
        private byte[] tx = new byte[256];              // передача в ридер 
        private byte[] rx = new byte[256];              // прием из ридер 

        SerialPort _serialPort;
        public string Status { get { return status; } }             // статус Slave контроллера

        public struct SlaveInfoS
        {
            public string MemorySize;
            public string ID;
            public string Version;
            public string mName;
        }

        public SlaveInfoS SlaveInfo;

        public struct DiskretInput
        {
            public bool D1; //1
            public bool D2;
            public bool D3;
            public bool D4;
        }
        //структуры портов A и B
        public DiskretInput PortA_In;
        public DiskretInput PortB_In;

        public DiskretInput PortA_Out;
        public DiskretInput PortB_Out;


        public struct RelaysInOut
        {
            public bool R1;
            public bool R2;
            public bool R3;
            public bool R4;
            public bool R5;
            public bool R6;
            public bool R7;
            public bool R8;
            public bool R9;
            public bool R10;
        }

        public RelaysInOut RelaysStateIn;
        public RelaysInOut RelaysStateOut;

        public RelaysInOut PowerState;
        public RelaysInOut PowerErrors;

        public struct Climate
        {
            public float LocalT;
            public float OutsideSensorT;
            public float GigrometerT;
            public float Humidity;

            public bool HeaterStatus;
            public bool FanStatus;
            public bool AutoMode;
        }

        public Climate ClimateState;

        /*
          Первый флоат – Локальная температура(33 B3 96 43), 301.400,
Второй флоат – Температура внешнего датчика(33 A7 96 43), 301.30,
Третий флоат – Температура гигрометра(52 68 96 43), 300.815
Четверный флоат – Влажность относительная в процентах(3B 0C 19 42), 38.3 %

Статусный байт ST: 
•	ST[0] – состояние нагревателя(0 – выключен, 1 – включен)
•	ST[1] - состояние вентилятора(0 – выключен, 1 – включен)
•	ST[2] – режим работы(0 – ручное управление нагревателем и вентилятором, 1 – автоматическое)
         */

        /*
      static void Do_Crc8(uint8_t b, uint8_t *crc) {
for (uint8_t i = 0; i < 8; b = b >> 1, i++)
     if ((b ^ *crc) & 1)
         *crc = ((*crc ^ 0x18) >> 1) | 0x80;
     else
         *crc = (*crc >> 1) & ~0x80;
}
      */


        public static byte Do_Crc8(byte b, byte crc)
        {
            for (byte i = 0; i < 8; b = (byte)(b >> 1), i++)
            {
                if (((b ^ crc) & 1) == 1)
                    crc = (byte)(((crc ^ 0x18) >> 1) | 0x80);
                else
                    crc = (byte)((crc >> 1) & ~0x80);
            }
            return crc;
        }

        public static byte CalcCRC(params byte[] arg)
        {

            byte init = 0x00;

            byte crc = Do_Crc8(arg[0], init);

            byte twobytebefore = arg[1];
            Debug.Print(SingleByteToBitString(twobytebefore));

            int twobyteafter_i = twobytebefore & 0x7f;

            byte twobyteafter = Convert.ToByte(twobyteafter_i);

            Debug.Print(SingleByteToBitString(twobyteafter));

            crc = Do_Crc8(twobyteafter, crc);

            if (arg.Length > 2)
            {
                for (int i = 2; i < arg.Length; i++)
                {
                    crc = Do_Crc8(arg[i], crc);
                }
            }
            return crc;
        }

        public void TestCRC()
        {
            //C0 82 08 02 07 03 C3
            //C0 82 03 00 
            //byte ret = ComputeChecksum(1, 2, 3, 4,5,6,7,8,9);

            //Debug.Print(SingleByteToString(ret));

            //Пробуем
            byte c1 = Do_Crc8(0x31, 0x00);

            Debug.Print("'1' " + SingleByteToString(c1));

            byte c2 = Do_Crc8(0x32, c1);

            Debug.Print("'12' " + SingleByteToString(c2));

            byte c3 = Do_Crc8(0x33, c2);

            Debug.Print("'123' " + SingleByteToString(c3));

            byte c4 = Do_Crc8(0x34, c3);

            Debug.Print("'1234' " + SingleByteToString(c4));

            byte c5 = Do_Crc8(0x35, c4);

            Debug.Print("'12345' " + SingleByteToString(c5));

            byte c6 = Do_Crc8(0x36, c5);

            Debug.Print("'123456' " + SingleByteToString(c6));

            byte c7 = Do_Crc8(0x37, c6);

            Debug.Print("'1234567' " + SingleByteToString(c7));

            byte c8 = Do_Crc8(0x38, c7);

            Debug.Print("'12345678' " + SingleByteToString(c8));

            byte c9 = Do_Crc8(0x39, c8);

            Debug.Print("'123456789' " + SingleByteToString(c9));

            //C0 82 03 00 
            byte[] tst1 = new byte[] {0xc0, 0x82, 0x03, 0x00 };
            Debug.Print(SingleByteToString(CalcCRC(tst1)));

            //C0 82 06 00 
            /*
            byte[] tst2 = new byte[] { 0xc0, 0x82, 0x06, 0x00 };
            Debug.Print(SingleByteToString(CalcCRC(tst2)));

            //C0 81 06 02 00 00
            byte[] tst3 = new byte[] { 0xc0, 0x81, 0x06, 0x02,0x00,0x00 };
            Debug.Print(SingleByteToString(CalcCRC(tst3)));
            */

            //byte[] microtest = new byte[] { 0x31, 0x32, 0x33 };
            //Debug.Print(SingleByteToString(CalcCRC(microtest)));

            //C0 82 08 02 07 03 ======= C3
            byte[] tst4 = new byte[] { 0xc0, 0x82, 0x08, 0x02, 0x07, 0x03 };
            Debug.Print(SingleByteToString(CalcCRC(tst4)));
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string ByteArrayToStringX(byte[] ba)
        {
            return BitConverter.ToString(ba);
        }

        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }

        public static string SingleByteToString(byte ba)
        {
            byte[] b = new byte[1];
            b[0] = ba;

            return BitConverter.ToString(b);
        }

        public static string ByteArrayEncode(byte[] data)
        {
            char[] characters = data.Select(b => (char)b).ToArray();
            return new string(characters);
        }

        public static string SingleByteToBitString(byte ba)
        {
            return Convert.ToString(ba, 2).PadLeft(8, '0');
        }

        public static bool GetBit(byte b, int bitNumber) //Гетбит... (надобно еще сетбит...)
        {
            return (b & (1 << bitNumber)) != 0;
        }

        public static byte[] BytesStuffing(byte[] arg)
        {
            string mystr = ByteArrayToStringX(arg);

            mystr = mystr.Substring(3, mystr.Length - 3);

            Debug.Print(mystr);

            //Ищем в полученном байты

            //DB на DB + DD 'этот сначала
            mystr = mystr.Replace("-DB", "-DB-DD");

            //С0 на DB + DC
            mystr = mystr.Replace("-C0", "-DB-DC");

            mystr = mystr.Replace("-", "");

            mystr = "C0" + mystr;
            Debug.Print(mystr);
            return StringToByteArray(mystr);
        }

        public SlaveV3()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 256000;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 200;
            _serialPort.ReadTimeout = 200;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
            _serialPort.DataReceived += _serialPort_DataReceived;
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int bytescount = 0;
            //throw new NotImplementedException();
            switch (command)
            {
                case "info":
                    Debug.Print(_serialPort.BytesToRead.ToString());
                    bytescount = _serialPort.BytesToRead;

                    byte[] infobytes = new byte[bytescount];
                    _serialPort.Read(infobytes, 0, bytescount);

                    SlaveInfo = new SlaveInfoS();

                    byte[] slaveinfo_bytememory = new byte[2];
                    for (int i = 0; i < 2; i++)
                    {
                        slaveinfo_bytememory[i] = infobytes[i];
                    }                    
                    SlaveInfo.MemorySize = ByteArrayToString(slaveinfo_bytememory);

                    byte[] slaveinfo_id = new byte[12];
                    for (int i = 0; i < 12; i++)
                    {
                        slaveinfo_id[i] = infobytes[i+2];
                    }
                    SlaveInfo.ID= ByteArrayToString(slaveinfo_id);

                    byte[] slaveinfo_ver = new byte[4];
                    for (int i = 0; i < 4; i++)
                    {
                        slaveinfo_ver[i] = infobytes[i + 14];
                    }
                    SlaveInfo.Version = Encoding.Default.GetString(slaveinfo_ver);

                    byte[] slaveinfo_arm = new byte[9];
                    for (int i = 0; i < 9; i++)
                    {
                        slaveinfo_arm[i] = infobytes[i + 22];
                    }
                    SlaveInfo.mName = Encoding.Default.GetString(slaveinfo_arm); //ByteArrayEncode(slaveinfo_arm);

                    Debug.Print(SlaveInfo.ID);
                    Debug.Print(SlaveInfo.MemorySize);
                    Debug.Print(SlaveInfo.Version);
                    Debug.Print(SlaveInfo.mName);

                    /*
                     a)	2 байта – размер памяти (little-endian) (80 00)
    b)	12 байт – уникальный ID (34FF67064D50323628221143)
    c)	4 байта – версия или порядковый номер устройства (01 00 00 00)
    d)	Строка до 14 байт – текстовое название устройства (Slave-arm)

                     */
                    break;
                case "getdiskret_ab_in":
                    //Debug.Print(_serialPort.BytesToRead.ToString());
                    bytescount = _serialPort.BytesToRead;

                    byte[] infobytes2 = new byte[bytescount];
                    _serialPort.Read(infobytes2, 0, bytescount);

                    byte portAb = infobytes2[4];
                    byte portBb = infobytes2[5];

                    SetBits(portAb, portBb,0);

                    //Debug.Print(PortB_In.D2.ToString() + " " + PortB_In.D1.ToString());
                    /*
                    for (int i = 0; i < bytescount; i++)
                    {
                        Debug.Print(SingleByteToString(infobytes2[i]));
                        Debug.Print(SingleByteToBitString(infobytes2[i]));
                    }
                    */
                    break;
                case "getdiskret_ab_out":
                    //Debug.Print(_serialPort.BytesToRead.ToString());
                    bytescount = _serialPort.BytesToRead;

                    byte[] infobytes3 = new byte[bytescount];
                    _serialPort.Read(infobytes3, 0, bytescount);

                    byte portAb2 = infobytes3[4];
                    byte portBb2 = infobytes3[5];

                    SetBits(portAb2, portBb2,1);

                    Debug.Print(PortB_Out.D2.ToString() + " " + PortB_Out.D1.ToString());
                    
                    for (int i = 0; i < bytescount; i++)
                    {
                        Debug.Print(SingleByteToString(infobytes3[i]));
                        Debug.Print(SingleByteToBitString(infobytes3[i]));
                    }                   
                    break;
                case "write_diskret_ab":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] writedbytes = new byte[bytescount];
                    _serialPort.Read(writedbytes, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(writedbytes));
                    break;

                case "setdiskret_1":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] setdbytes1 = new byte[bytescount];
                    _serialPort.Read(setdbytes1, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(setdbytes1)); //byte[] slaveinfo_arm = new byte[9];
                    break;

                case "setdiskret_0":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] setdbytes0 = new byte[bytescount];
                    _serialPort.Read(setdbytes0, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(setdbytes0));
                    break;
                case "getrelays_in":
                    bytescount = _serialPort.BytesToRead;

                    byte[] infobytes4 = new byte[bytescount];
                    _serialPort.Read(infobytes4, 0, bytescount);

                    RelaysStateIn = new RelaysInOut();

                    byte RelaysByte1 = infobytes4[4]; //2 бита
                    byte RelaysByte2 = infobytes4[5]; //весь байт

                    if (GetBit(RelaysByte1, 1)) { RelaysStateIn.R1 = true; }
                    if (GetBit(RelaysByte1, 0)) { RelaysStateIn.R2 = true; }

                    if (GetBit(RelaysByte2, 7)) { RelaysStateIn.R3 = true; }
                    if (GetBit(RelaysByte2, 6)) { RelaysStateIn.R4 = true; }
                    if (GetBit(RelaysByte2, 5)) { RelaysStateIn.R5 = true; }
                    if (GetBit(RelaysByte2, 4)) { RelaysStateIn.R6 = true; }
                    if (GetBit(RelaysByte2, 3)) { RelaysStateIn.R7 = true; }
                    if (GetBit(RelaysByte2, 2)) { RelaysStateIn.R8 = true; }
                    if (GetBit(RelaysByte2, 1)) { RelaysStateIn.R9 = true; }
                    if (GetBit(RelaysByte2, 0)) { RelaysStateIn.R10 = true; }

                    Debug.Print("Relays:");

                    Debug.Print(GetBit(RelaysByte2, 0).ToString());
                    Debug.Print(GetBit(RelaysByte2, 1).ToString());
                    Debug.Print(GetBit(RelaysByte2, 2).ToString());
                    Debug.Print(GetBit(RelaysByte2, 3).ToString());
                    Debug.Print(GetBit(RelaysByte2, 4).ToString());
                    Debug.Print(GetBit(RelaysByte2, 5).ToString());
                    Debug.Print(GetBit(RelaysByte2, 6).ToString());
                    Debug.Print(GetBit(RelaysByte2, 7).ToString());

                    Debug.Print(SingleByteToBitString(RelaysByte1));
                    Debug.Print(SingleByteToBitString(RelaysByte2));
                    break;

                case "getrelays_out":
                    bytescount = _serialPort.BytesToRead;

                    byte[] infobytes5 = new byte[bytescount];
                    _serialPort.Read(infobytes5, 0, bytescount);

                    RelaysStateIn = new RelaysInOut();

                    byte RelaysByte1o = infobytes5[4]; //2 бита
                    byte RelaysByte2o = infobytes5[5]; //весь байт

                    if (GetBit(RelaysByte1o, 1)) { RelaysStateOut.R1 = true; }
                    if (GetBit(RelaysByte1o, 0)) { RelaysStateOut.R2 = true; }

                    if (GetBit(RelaysByte2o, 7)) { RelaysStateOut.R3 = true; }
                    if (GetBit(RelaysByte2o, 6)) { RelaysStateOut.R4 = true; }
                    if (GetBit(RelaysByte2o, 5)) { RelaysStateOut.R5 = true; }
                    if (GetBit(RelaysByte2o, 4)) { RelaysStateOut.R6 = true; }
                    if (GetBit(RelaysByte2o, 3)) { RelaysStateOut.R7 = true; }
                    if (GetBit(RelaysByte2o, 2)) { RelaysStateOut.R8 = true; }
                    if (GetBit(RelaysByte2o, 1)) { RelaysStateOut.R9 = true; }
                    if (GetBit(RelaysByte2o, 0)) { RelaysStateOut.R10 = true; }
                    /*
                    Debug.Print("Relays:");

                    Debug.Print(GetBit(RelaysByte2, 0).ToString());
                    Debug.Print(GetBit(RelaysByte2, 1).ToString());
                    Debug.Print(GetBit(RelaysByte2, 2).ToString());
                    Debug.Print(GetBit(RelaysByte2, 3).ToString());
                    Debug.Print(GetBit(RelaysByte2, 4).ToString());
                    Debug.Print(GetBit(RelaysByte2, 5).ToString());
                    Debug.Print(GetBit(RelaysByte2, 6).ToString());
                    Debug.Print(GetBit(RelaysByte2, 7).ToString());
                    */
                    Debug.Print(SingleByteToBitString(RelaysByte1o));
                    Debug.Print(SingleByteToBitString(RelaysByte2o));
                    break;
                case "write_diskret_relay":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] writedbytes2 = new byte[bytescount];
                    _serialPort.Read(writedbytes2, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(writedbytes2));
                    break;

                case "setrelay_1":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] setrbytes1 = new byte[bytescount];
                    _serialPort.Read(setrbytes1, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(setrbytes1)); //byte[] slaveinfo_arm = new byte[9];
                    break;
                case "setrelay_0":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] setrbytes2 = new byte[bytescount];
                    _serialPort.Read(setrbytes2, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(setrbytes2)); //byte[] slaveinfo_arm = new byte[9];
                    break;
                case "getpower_out":
                    bytescount = _serialPort.BytesToRead;

                    byte[] infobytes6 = new byte[bytescount];
                    _serialPort.Read(infobytes6, 0, bytescount);

                    RelaysStateIn = new RelaysInOut();

                    byte PowerByte1 = infobytes6[4]; //2 бита
                    byte PowerByte2 = infobytes6[5]; //весь байт

                    if (GetBit(PowerByte1, 1)) { PowerState.R1 = true; }
                    if (GetBit(PowerByte1, 0)) { PowerState.R2 = true; }

                    if (GetBit(PowerByte2, 7)) { PowerState.R3 = true; }
                    if (GetBit(PowerByte2, 6)) { PowerState.R4 = true; }
                    if (GetBit(PowerByte2, 5)) { PowerState.R5 = true; }
                    if (GetBit(PowerByte2, 4)) { PowerState.R6 = true; }
                    if (GetBit(PowerByte2, 3)) { PowerState.R7 = true; }
                    if (GetBit(PowerByte2, 2)) { PowerState.R8 = true; }
                    if (GetBit(PowerByte2, 1)) { PowerState.R9 = true; }
                    if (GetBit(PowerByte2, 0)) { PowerState.R10 = true; }
                    /*
                    Debug.Print("Powers:");

                    Debug.Print(GetBit(RelaysByte2, 0).ToString());
                    Debug.Print(GetBit(RelaysByte2, 1).ToString());
                    Debug.Print(GetBit(RelaysByte2, 2).ToString());
                    Debug.Print(GetBit(RelaysByte2, 3).ToString());
                    Debug.Print(GetBit(RelaysByte2, 4).ToString());
                    Debug.Print(GetBit(RelaysByte2, 5).ToString());
                    Debug.Print(GetBit(RelaysByte2, 6).ToString());
                    Debug.Print(GetBit(RelaysByte2, 7).ToString());
                    */
                    Debug.Print(SingleByteToBitString(PowerByte1));
                    Debug.Print(SingleByteToBitString(PowerByte2));
                    break;
                case "getpower_err":
                    bytescount = _serialPort.BytesToRead;

                    byte[] infobytes7 = new byte[bytescount];
                    _serialPort.Read(infobytes7, 0, bytescount);

                    RelaysStateIn = new RelaysInOut();

                    byte PowerByte1e = infobytes7[4]; //2 бита
                    byte PowerByte2e = infobytes7[5]; //весь байт

                    if (GetBit(PowerByte1e, 1)) { PowerErrors.R1 = true; }
                    if (GetBit(PowerByte1e, 0)) { PowerErrors.R2 = true; }

                    if (GetBit(PowerByte2e, 7)) { PowerErrors.R3 = true; }
                    if (GetBit(PowerByte2e, 6)) { PowerErrors.R4 = true; }
                    if (GetBit(PowerByte2e, 5)) { PowerErrors.R5 = true; }
                    if (GetBit(PowerByte2e, 4)) { PowerErrors.R6 = true; }
                    if (GetBit(PowerByte2e, 3)) { PowerErrors.R7 = true; }
                    if (GetBit(PowerByte2e, 2)) { PowerErrors.R8 = true; }
                    if (GetBit(PowerByte2e, 1)) { PowerErrors.R9 = true; }
                    if (GetBit(PowerByte2e, 0)) { PowerErrors.R10 = true; }
                    /*
                    Debug.Print("Powers:");

                    Debug.Print(GetBit(RelaysByte2, 0).ToString());
                    Debug.Print(GetBit(RelaysByte2, 1).ToString());
                    Debug.Print(GetBit(RelaysByte2, 2).ToString());
                    Debug.Print(GetBit(RelaysByte2, 3).ToString());
                    Debug.Print(GetBit(RelaysByte2, 4).ToString());
                    Debug.Print(GetBit(RelaysByte2, 5).ToString());
                    Debug.Print(GetBit(RelaysByte2, 6).ToString());
                    Debug.Print(GetBit(RelaysByte2, 7).ToString());
                    */
                    Debug.Print(SingleByteToBitString(PowerByte1e));
                    Debug.Print(SingleByteToBitString(PowerByte2e));
                    break;

                case "write_diskret_power":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] writedbytes3 = new byte[bytescount];
                    _serialPort.Read(writedbytes3, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(writedbytes3));
                    break;

                case "setpower_1":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] setpbytes1 = new byte[bytescount];
                    _serialPort.Read(setpbytes1, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(setpbytes1)); //byte[] slaveinfo_arm = new byte[9];
                    break;
                case "setpower_0":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] setpbytes2 = new byte[bytescount];
                    _serialPort.Read(setpbytes2, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(setpbytes2)); //byte[] slaveinfo_arm = new byte[9];
                    break;
                case "getclimate": //флоаты
                    /*
                    Ответ
C0 81 16 11 33 B3 96 43 33 A7 96 43 52 68 96 43 3B 0C 19 42 00 83
В полезной нагрузке 4 флоата по 32 бита(little - endian) и 1 байт статус. Температура в Кельвинах
 Первый флоат – Локальная температура(33 B3 96 43), 301.400,
Второй флоат – Температура внешнего датчика(33 A7 96 43), 301.30,
Третий флоат – Температура гигрометра(52 68 96 43), 300.815
Четверный флоат – Влажность относительная в процентах(3B 0C 19 42), 38.3 %

Статусный байт ST: 
•	ST[0] – состояние нагревателя(0 – выключен, 1 – включен)
•	ST[1] - состояние вентилятора(0 – выключен, 1 – включен)
•	ST[2] – режим работы(0 – ручное управление нагревателем и вентилятором, 1 – автоматическое)
*/
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] climatebytes = new byte[bytescount];
                    _serialPort.Read(climatebytes, 0, bytescount);

                    Debug.Print(ByteArrayToStringX(climatebytes));

                    ClimateState = new Climate();

                    byte[] firstfloat = new byte[4];
                    for (int i = 0; i < 4; i++)
                    {
                        firstfloat[i] = climatebytes[i + 4];
                    }
                    float f1 = BitConverter.ToSingle(firstfloat, 0);
                    ClimateState.LocalT = f1 - 273;
                    Debug.Print(ClimateState.LocalT.ToString());

                    byte[] secondfloat = new byte[4];
                    for (int i = 0; i < 4; i++)
                    {
                        secondfloat[i] = climatebytes[i + 8];
                    }
                    float f2 = BitConverter.ToSingle(secondfloat, 0);
                    ClimateState.OutsideSensorT = f2 - 273;
                    Debug.Print(ClimateState.OutsideSensorT.ToString());

                    byte[] thirdfloat = new byte[4];
                    for (int i = 0; i < 4; i++)
                    {
                        thirdfloat[i] = climatebytes[i + 12];
                    }
                    float f3 = BitConverter.ToSingle(thirdfloat, 0);
                    ClimateState.GigrometerT = f3 - 273;
                    Debug.Print(ClimateState.GigrometerT.ToString());

                    byte[] fourthfloat = new byte[4];
                    for (int i = 0; i < 4; i++)
                    {
                        fourthfloat[i] = climatebytes[i + 16];
                    }
                    float f4 = BitConverter.ToSingle(fourthfloat, 0);
                    ClimateState.Humidity = f3;
                    Debug.Print(ClimateState.Humidity.ToString());

                    byte statebyte = climatebytes[20];
                    Debug.Print(SingleByteToBitString(statebyte));

                    ClimateState.HeaterStatus= GetBit(statebyte, 0);
                    ClimateState.FanStatus = GetBit(statebyte, 1);
                    ClimateState.AutoMode = GetBit(statebyte, 2);
                    break;
                case "setclimate":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] setclimate = new byte[bytescount];
                    _serialPort.Read(setclimate, 0, bytescount);
                    Debug.Print(Encoding.Default.GetString(setclimate)); //byte[] slaveinfo_arm = new byte[9];
                    break;
                case "getwiegand":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] wiegandbytes = new byte[bytescount];
                    _serialPort.Read(wiegandbytes, 0, bytescount);

                    Debug.Print(ByteArrayToStringX(wiegandbytes));

                    //Debug.Print(Encoding.Default.GetString(wiegandbytes)); //byte[] slaveinfo_arm = new byte[9];
                    break;

                case "getall":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] getallbytes = new byte[bytescount];

                    _serialPort.Read(getallbytes, 0, bytescount);
                    Debug.Print(ByteArrayToStringX(getallbytes));

                    int databytesize = bytescount - 5;

                    byte[] databytes = new byte[databytesize];

                    for (int i = 4; i < bytescount - 1; i++)
                    {
                        databytes[i - 4] = getallbytes[i];
                    }

                    Debug.Print(ByteArrayToStringX(databytes));

                    ResponseAll resp;
                    resp = ResponseAll.Parser.ParseFrom(databytes);

                    //входы а и б
                    byte[] AportIn = BitConverter.GetBytes(resp.PORTAIDR);
                    byte[] BportIn = BitConverter.GetBytes(resp.PORTBIDR);

                    Debug.Print(SingleByteToBitString(AportIn[0]));

                    SetBits(AportIn[0], BportIn[0], 0);

                    Debug.Print(PortA_In.D1.ToString());
                    Debug.Print(PortA_In.D2.ToString());
                    Debug.Print(PortA_In.D3.ToString());
                    Debug.Print(PortA_In.D4.ToString());

                    Debug.Print(PortB_In.D1.ToString());
                    Debug.Print(PortB_In.D2.ToString());
                    Debug.Print(PortB_In.D3.ToString());
                    Debug.Print(PortB_In.D4.ToString());

                    //выходы а и б
                    byte[] AportOut = BitConverter.GetBytes(resp.PORTAODR);
                    byte[] BportOut = BitConverter.GetBytes(resp.PORTBODR);

                    SetBits(AportOut[0], BportOut[0], 1);

                    Debug.Print(PortA_Out.D1.ToString());
                    Debug.Print(PortA_Out.D2.ToString());
                    Debug.Print(PortA_Out.D3.ToString());
                    Debug.Print(PortA_Out.D4.ToString());

                    Debug.Print(PortB_Out.D1.ToString());
                    Debug.Print(PortB_Out.D2.ToString());
                    Debug.Print(PortB_Out.D3.ToString());
                    Debug.Print(PortB_Out.D4.ToString());

                    //релюхи
                   
                    byte[] RelaysOut = BitConverter.GetBytes(resp.RELAYSODR);

                    RelaysStateOut = new RelaysInOut();

                    byte RelayOut1 = RelaysOut[0]; //Весь байт
                    byte RelayOut2 = RelaysOut[1]; //2 бита

                    Debug.Print(SingleByteToBitString(RelayOut1));
                    Debug.Print(SingleByteToBitString(RelayOut2));

                   
                   if (GetBit(RelayOut2, 1)) { RelaysStateOut.R1 = true; }
                   if (GetBit(RelayOut2, 0)) { RelaysStateOut.R2 = true; }

                    
                    if (GetBit(RelayOut1, 7)) { RelaysStateOut.R3 = true; }
                    
                    if (GetBit(RelayOut1, 6)) { RelaysStateOut.R4 = true; }
                    if (GetBit(RelayOut1, 5)) { RelaysStateOut.R5 = true; }
                    if (GetBit(RelayOut1, 4)) { RelaysStateOut.R6 = true; }
                    if (GetBit(RelayOut1, 3)) { RelaysStateOut.R7 = true; }
                    if (GetBit(RelayOut1, 2)) { RelaysStateOut.R8 = true; }
                    if (GetBit(RelayOut1, 1)) { RelaysStateOut.R9 = true; }
                    if (GetBit(RelayOut1, 0)) { RelaysStateOut.R10 = true; }
                    
                    //виганды попозжа, когда сделает...

                    Debug.Print(RelaysStateOut.R1.ToString());
                    Debug.Print(RelaysStateOut.R2.ToString());
                    Debug.Print(RelaysStateOut.R3.ToString());
                    Debug.Print(RelaysStateOut.R4.ToString());
                    Debug.Print(RelaysStateOut.R5.ToString());
                    Debug.Print(RelaysStateOut.R6.ToString());
                    Debug.Print(RelaysStateOut.R7.ToString());
                    Debug.Print(RelaysStateOut.R8.ToString());
                    Debug.Print(RelaysStateOut.R9.ToString());
                    Debug.Print(RelaysStateOut.R10.ToString());


                    byte[] RelaysIn = BitConverter.GetBytes(resp.RELAYSIDR);

                    RelaysStateIn = new RelaysInOut();

                    byte RelayIn1 = RelaysIn[0]; //Весь байт
                    byte RelayIn2 = RelaysIn[1]; //2 бита

                    Debug.Print(SingleByteToBitString(RelayIn1));
                    Debug.Print(SingleByteToBitString(RelayIn2));


                    if (GetBit(RelayIn2, 1)) { RelaysStateIn.R1 = true; }
                    if (GetBit(RelayIn2, 0)) { RelaysStateIn.R2 = true; }


                    if (GetBit(RelayIn1, 7)) { RelaysStateIn.R3 = true; }

                    if (GetBit(RelayIn1, 6)) { RelaysStateIn.R4 = true; }
                    if (GetBit(RelayIn1, 5)) { RelaysStateIn.R5 = true; }
                    if (GetBit(RelayIn1, 4)) { RelaysStateIn.R6 = true; }
                    if (GetBit(RelayIn1, 3)) { RelaysStateIn.R7 = true; }
                    if (GetBit(RelayIn1, 2)) { RelaysStateIn.R8 = true; }
                    if (GetBit(RelayIn1, 1)) { RelaysStateIn.R9 = true; }
                    if (GetBit(RelayIn1, 0)) { RelaysStateIn.R10 = true; }

                    //виганды попозжа, когда сделает...

                    Debug.Print(RelaysStateIn.R1.ToString());
                    Debug.Print(RelaysStateIn.R2.ToString());
                    Debug.Print(RelaysStateIn.R3.ToString());
                    Debug.Print(RelaysStateIn.R4.ToString());
                    Debug.Print(RelaysStateIn.R5.ToString());
                    Debug.Print(RelaysStateIn.R6.ToString());
                    Debug.Print(RelaysStateIn.R7.ToString());
                    Debug.Print(RelaysStateIn.R8.ToString());
                    Debug.Print(RelaysStateIn.R9.ToString());
                    Debug.Print(RelaysStateIn.R10.ToString());
                    break;

                case "blasting":
                    bytescount = _serialPort.BytesToRead;
                    Debug.Print(bytescount.ToString());
                    byte[] pulsebytes = new byte[bytescount];

                    _serialPort.Read(pulsebytes, 0, bytescount);
                    Debug.Print(ByteArrayToStringX(pulsebytes));

                    break;
                default:
                    break;
            }
        }

        public void SetBits(byte pA, byte pB, byte InOut) //0 or 1
        {
            if (InOut == 0)
            {
                PortA_In = new DiskretInput();
                if (GetBit(pA, 0)) { PortA_In.D1 = true; }
                if (GetBit(pA, 1)) { PortA_In.D2 = true; }
                if (GetBit(pA, 2)) { PortA_In.D3 = true; }
                if (GetBit(pA, 3)) { PortA_In.D4 = true; }

                PortB_In = new DiskretInput();
                if (GetBit(pB, 0)) { PortB_In.D1 = true; }
                if (GetBit(pB, 1)) { PortB_In.D2 = true; }
                if (GetBit(pB, 2)) { PortB_In.D3 = true; }
                if (GetBit(pB, 3)) { PortB_In.D4 = true; }
            }
            else if (InOut == 1)
            {
                PortA_Out = new DiskretInput();
                if (GetBit(pA, 0)) { PortA_Out.D1 = true; }
                if (GetBit(pA, 1)) { PortA_Out.D2 = true; }
                if (GetBit(pA, 2)) { PortA_Out.D3 = true; }
                if (GetBit(pA, 3)) { PortA_Out.D4 = true; }

                PortB_Out = new DiskretInput();
                if (GetBit(pB, 0)) { PortB_Out.D1 = true; }
                if (GetBit(pB, 1)) { PortB_Out.D2 = true; }
                if (GetBit(pB, 2)) { PortB_Out.D3 = true; }
                if (GetBit(pB, 3)) { PortB_Out.D4 = true; }
            }
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
               // try
               // {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    //GetStatus();
                    //_serialPort.DiscardInBuffer();
                    //_serialPort.DiscardOutBuffer();
                    return true;
              //  }
              //  catch (Exception e)
              //  {
              //      status = "не отвечает";
              //      Console.WriteLine("Возникло исключение: " + e.ToString());
              //      return false;
             //   }
            }
            else
            {
                return true;
            }
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    Console.WriteLine("Возникло исключение: " + e.ToString());
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public void GetSlaveInfo()                      // команда в Slave чтения SignatureSlave
        {
            command = "info";
            if (_serialPort.IsOpen)
            {
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x03, 0x00, 0x23 };

                _serialPort.Write(bytes,0,5);
            }
        }

        public void SetBootLoaderMode() //в конце
        {

        }

        /*
         Команда PORTS_IDR (0x06)
Описание
Считывание состояние входов PORTA, B
Запрос
C0 82 06 00 DC
Ответ
C0 81 06 02 00 00 14
2 байта данных. Младший байт – PORTA, старший – PORTB. Значащие в этих байтах – младшие 4 бита, отвечающие за Px0 – Px3 (x = A or B).

         */

        //получаем состояние дискретов  а и б
        public void GetDiskretABIn()
        {
            /*
             Команда PORTS_IDR (0x06)
Описание
Считывание состояние входов PORTA, B
Запрос
C0 82 06 00 DC
Ответ
C0 81 06 02 00 00 14
2 байта данных. Младший байт – PORTA, старший – PORTB. Значащие в этих байтах – младшие 4 бита, отвечающие за Px0 – Px3 (x = A or B).

             */
            command = "getdiskret_ab_in";
            if (_serialPort.IsOpen)
            {
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x06, 0x00, 0xdc };

                _serialPort.Write(bytes, 0, 5);
            }

        }

        public void GetDiskretABOut()
        {
            /*
             Считывание состояние выходов PORTA и PORTB
Запрос
C0 82 07 00 18
Ответ
C0 81 07 02 08 0E F2
2 байта данных (08 0E). Младший байт – PORTA (08), старший байт – PORTB (0E). Значащие в этих байтах – младшие 4 бита, отвечающие за Px0 – Px3 (x = A or B).

             */
            command = "getdiskret_ab_out";
            if (_serialPort.IsOpen)
            {
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x07, 0x00, 0x18 };

                _serialPort.Write(bytes, 0, 5);
            }

        }

        //Устанавливает в 1 вывод порта A,B
        public void SetDiskretTo1(int diskret, int port)
        {
            /*
            Описание
Установить дискретные выходы PORTA, B в единицу.
Запрос
C0 82 09 02 07 03 4C
2 байта данных. Младший байт – состояние PORTA (07), старший – PORTB (03). Значащие в этих байтах – младшие 4 бита, отвечающие за Px0 – Px3 (x = A or B).
Ответ
PORTS_SET в случае успешности
ERR – во всех остальных


             */
            
            if (_serialPort.IsOpen)
            {
                command = "setdiskret_1";

                byte managebyte = 0x00;

                byte aport = 0x00;
                byte bport = 0x00;

                if (diskret == 1)
                {
                    managebyte = 0x01;
                }
                else if (diskret == 2)
                {
                    managebyte = 0x02;
                }
                else if (diskret == 3)
                {
                    managebyte = 0x04;
                }
                else if (diskret == 4)
                {
                    managebyte = 0x08;
                }
                else
                {
                    managebyte = 0x01;
                }

                if (port == 0)
                {
                    aport = managebyte;
                }
                else
                {
                    bport = managebyte;
                }

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x09, 0x02, aport,bport};

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc= new byte[] { 0xC0, 0x82, 0x09, 0x02, aport, bport,crc };

                byte[] bytesafter =  BytesStuffing(byteswithcrc);

                //Debug.Print(ByteArrayToString(NewBytes));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        //Устанавливает в 0 вывод порта A,B
        public void SetDiskretTo0(int diskret, int port)
        {
            /*
            Описание
Установить дискретные выходы PORTA, B в нули.
Запрос
C0 82 0A 02 07 03 C4
2 байта данных. Младший байт – состояние PORTA (07), старший – PORTB (03). Значащие в этих байтах – младшие 4 бита, отвечающие за Px0 – Px3 (x = A or B).
Ответ
PORTS_RESET в случае успешности
ERR – во всех остальных



             */
            if (_serialPort.IsOpen)
            {
                command = "setdiskret_0";

                byte managebyte = 0x00;

                byte aport = 0x00;
                byte bport = 0x00;

                if (diskret == 1)
                {
                    managebyte = 0x01;
                }
                else if (diskret == 2)
                {
                    managebyte = 0x02;
                }
                else if (diskret == 3)
                {
                    managebyte = 0x04;
                }
                else if (diskret == 4)
                {
                    managebyte = 0x08;
                }
                else
                {
                    managebyte = 0x01;
                }

                if (port == 0)
                {
                    aport = managebyte;
                }
                else
                {
                    bport = managebyte;
                }

                //C0 82 0A 02 07 03 C4

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x0a, 0x02, aport, bport };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x0a, 0x02, aport, bport, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                //Debug.Print(ByteArrayToString(NewBytes));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }


        //Запись в дискрет порта A,B (пока не будем использовать)
        public void WriteToDiskretAB(string maska,string maskb)
        {
            /*
           Описание
Запись данных в выходные порты (дискретные выходы) PORTA, B
Запрос
C0 82 08 02 07 03 C3
2 байта данных. Младший байт – состояние PORTA (07), старший – PORTB (03). Значащие в этих байтах – младшие 4 бита, отвечающие за Px0 – Px3 (x = A or B).
             */
            
            if (_serialPort.IsOpen)
            {
                command = "write_diskret_ab";
                byte aport = Convert.ToByte("0000"+maska, 2);
                byte bport = Convert.ToByte("0000" + maskb, 2);



                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x08, 0x02, aport, bport };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x08, 0x02, aport, bport, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        public void GetRelaysIn()
        {
            /*
             Команда RELAYS_IDR (0x0C)
Описание
Чтение дополнительных дискретных входов
Запрос
C0 82 0C 00 3B
Ответ
C0 81 0C 02 02 09 02
2 байта данных 02 09. Значение на дискретных входах: 0 – разомкнут, 1 – замкнут. Значащими битами являются 10 бит (10_0000_1001).


             */
            
            if (_serialPort.IsOpen)
            {
                command = "getrelays_in";
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x0c, 0x00, 0x3b };

                _serialPort.Write(bytes, 0, 5);
            }

        }

        public void GetRelaysOut()
        {
            /*
             Команда RELAYS_ODRR (0x0D)
Описание
Чтение дополнительных дискретных выходов.
Запрос
C0 82 0D 00 FF
Ответ
C0 81 0D 02 02 C2 67
2 байта данных 02 C2. Состояние дополнительных дискретных выходов: 0 – разомкнут, 1 – замкнут. Значащими битами являются 10 бит (10_1100_0010) 



             */

            if (_serialPort.IsOpen)
            {
                command = "getrelays_out";
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x0d, 0x00, 0xff };

                _serialPort.Write(bytes, 0, 5);
            }

        }

        //Устанавливает номерное реле вывода в 1
        public void SetRelayTo1(int diskret)
        {
            /*
Команда RELAYS_SET (0x0F)
Описание
Установить дискретные выходы в ЕДИНИЦУ.
Запрос
C0 82 0F 02 03 2A C1
2 байта данных (03 2А). Перевести указанные дискретные выходы в ЕДИНИЦУ. Значащими битами являются 10 бит (11_0010_1010)
Ответ
RELAYS_SET в случае успешности
ERR – во всех остальных
             */

            if (_serialPort.IsOpen)
            {
                command = "setrelay_1";

                //byte managebyte = 0x00;

                byte byte1 = 0x00;
                byte byte2 = 0x00;

                if (diskret == 1)
                {
                    byte1 = 0x02; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 2)
                {
                    byte1 = 0x01; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 3)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x80;
                }
                else if (diskret == 4)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x40;
                }
                else if (diskret == 5)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x20;
                }
                else if (diskret == 6)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x10;
                }
                else if (diskret == 7)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x08;
                }
                else if (diskret == 8)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x04;
                }
                else if (diskret == 9)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x02;
                }
                else if (diskret == 10)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x01;
                }

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x0f, 0x02, byte1, byte2 };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x0f, 0x02, byte1, byte2, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                //Debug.Print(ByteArrayToString(NewBytes));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        //Устанавливает номерное реле вывода в 1
        public void SetRelayTo0(int diskret)
        {
            /*
Команда RELAYS_RESET (0x10)
Описание
Установить дискретные выходы в НУЛИ.

Запрос
C0 82 10 02 03 2A 63
2 байта данных (03 2А). Перевести указанные дискретные выходы в НОЛЬ. Значащими битами являются 10 бит (11_0010_1010)
Ответ
RELAYS_RESET в случае успешности
ERR – во всех остальных

             */

            if (_serialPort.IsOpen)
            {
                command = "setrelay_0";

                //byte managebyte = 0x00;

                byte byte1 = 0x00;
                byte byte2 = 0x00;

                if (diskret == 1)
                {
                    byte1 = 0x02; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 2)
                {
                    byte1 = 0x01; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 3)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x80;
                }
                else if (diskret == 4)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x40;
                }
                else if (diskret == 5)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x20;
                }
                else if (diskret == 6)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x10;
                }
                else if (diskret == 7)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x08;
                }
                else if (diskret == 8)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x04;
                }
                else if (diskret == 9)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x02;
                }
                else if (diskret == 10)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x01;
                }

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x10, 0x02, byte1, byte2 };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x10, 0x02, byte1, byte2, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                //Debug.Print(ByteArrayToString(NewBytes));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        //Запись в дискрет порта A,B (пока не будем использовать)
        public void WriteToDiskretRelay(string mask)
        {
            /*
           Команда RELAYS_ODRW (0x0E)
Описание
Установить дискретные выходы в соответствующее состояние.
Запрос
C0 82 0E 02 03 2A 4E
2 байта данных (03 2А). Перевести дискретные выходы в указанное состояние: 0 – разомкнуты, 1 – замкнуты. Значащими битами являются 10 бит (11_0010_1010)
Ответ
RELAYS_ODRW в случае успешности
ERR – во всех остальных

             */

            if (_serialPort.IsOpen)
            {
                command = "write_diskret_relay";

                string mask1 = mask.Substring(0, 2);
                string mask2 = mask.Substring(2, 8);

                byte byte1 = Convert.ToByte("000000" + mask1, 2);
                byte byte2 = Convert.ToByte(mask2, 2);



                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x0e, 0x02, byte1, byte2 };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x0e, 0x02, byte1, byte2, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        public void GetPowerErrors()
        {
            /*
            Команда POWERS_IDR (0x11)
Описание
Считать значение ошибок силовых выходов.
Запрос
C0 82 11 00 5E
Ответ
C0 81 11 02 00 00 AA
2 байта, всегда нули, не реализовано.



             */

            if (_serialPort.IsOpen)
            {
                command = "getpower_err";
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x11, 0x00, 0x5e };

                _serialPort.Write(bytes, 0, 5);
            }
        }

        public void GetPowerOut()
        {
            /*
             Команда POWERS_ODRR (0x12)
Описание
Считать состояние силовых выходов.
Запрос
C0 82 12 00 0B
Ответ
C0 81 12 02 02 A7 9F
2 байта 02 A7. Состояние силовых выходов: 0 – отключен, 1 – включен. Значащими битами являются 10 бит (10_1010_0111)


             */

            if (_serialPort.IsOpen)
            {
                command = "getpower_out";
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x12, 0x00, 0x0b };

                _serialPort.Write(bytes, 0, 5);
            }

        }


        //Устанавливает силовой разъем в 1
        public void SetPowerTo1(int diskret)
        {
            /*
Команда POWERS_SET (0x14)
Описание
Установить Силовые выходы в включенное состояние.
Запрос
C0 82 14 02 02 A7 D8
2 байта данных (02 А7). Перевести силовые выходы в включенное состояние. Значащими битами являются 10 бит (10_1010_0111).
Ответ
POWERS_SET в случае успешности
ERR – во всех остальных

             */

            if (_serialPort.IsOpen)
            {
                command = "setpower_1";

                //byte managebyte = 0x00;

                byte byte1 = 0x00;
                byte byte2 = 0x00;

                if (diskret == 1)
                {
                    byte1 = 0x02; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 2)
                {
                    byte1 = 0x01; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 3)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x80;
                }
                else if (diskret == 4)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x40;
                }
                else if (diskret == 5)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x20;
                }
                else if (diskret == 6)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x10;
                }
                else if (diskret == 7)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x08;
                }
                else if (diskret == 8)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x04;
                }
                else if (diskret == 9)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x02;
                }
                else if (diskret == 10)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x01;
                }

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x14, 0x02, byte1, byte2 };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x14, 0x02, byte1, byte2, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                //Debug.Print(ByteArrayToString(NewBytes));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        //Устанавливает силовой разъем в 0
        public void SetPowerTo0(int diskret)
        {
            /*
Команда POWERS_RESET (0x15)
Описание
Установить Силовые выходы в отключенное состояние.
Запрос
C0 82 15 02 02 A7 57
2 байта данных (02 А7). Перевести силовые выходы в отключенное состояние. Значащими битами являются 10 бит (10_1010_0111).
Ответ
POWERS_RESET в случае успешности
ERR – во всех остальных


             */

            if (_serialPort.IsOpen)
            {
                command = "setpower_1";

                //byte managebyte = 0x00;

                byte byte1 = 0x00;
                byte byte2 = 0x00;

                if (diskret == 1)
                {
                    byte1 = 0x02; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 2)
                {
                    byte1 = 0x01; //справа налево...
                    byte2 = 0x00;
                }
                else if (diskret == 3)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x80;
                }
                else if (diskret == 4)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x40;
                }
                else if (diskret == 5)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x20;
                }
                else if (diskret == 6)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x10;
                }
                else if (diskret == 7)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x08;
                }
                else if (diskret == 8)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x04;
                }
                else if (diskret == 9)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x02;
                }
                else if (diskret == 10)
                {
                    byte1 = 0x00; //справа налево...
                    byte2 = 0x01;
                }

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x15, 0x02, byte1, byte2 };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x15, 0x02, byte1, byte2, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        //Запись в дискрет порта A,B (пока не будем использовать)
        public void WriteToDiskretPower(string mask)
        {
            /*
           Команда POWERS_ODRW (0x13)
Описание
Установить Силовые выходы в соответствующее состояние.
Запрос
C0 82 13 02 02 A7 5E
2 байта данных (02 А7). Перевести силовые выходы в указанное состояние: 0 – отключен, 1 – включен. Значащими битами являются 10 бит (10_1010_0111).
Ответ
POWERS_ODRW в случае успешности
ERR – во всех остальных


             */

            if (_serialPort.IsOpen)
            {
                command = "write_diskret_power";

                string mask1 = mask.Substring(0, 2);
                string mask2 = mask.Substring(2, 8);

                byte byte1 = Convert.ToByte("000000" + mask1, 2);
                byte byte2 = Convert.ToByte(mask2, 2);



                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x13, 0x02, byte1, byte2 };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x13, 0x02, byte1, byte2, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        //Slave Functions:
        public void GetClimate()
        {
            /*
             Команда CLIMATE_GET (0x16)
Описание
Запрос состояние контроллера климата. Считывание температур, влажности, состояние печки, вентилятора…
Запрос
C0 82 16 00 30
Ответ
C0 81 16 11 33 B3 96 43 33 A7 96 43 52 68 96 43 3B 0C 19 42 00 83
В полезной нагрузке 4 флоата по 32 бита (little-endian) и 1 байт статус. Температура в Кельвинах
Первый флоат – Локальная температура (33 B3 96 43), 301.400,
Второй флоат – Температура внешнего датчика (33 A7 96 43), 301.30,
Третий флоат – Температура гигрометра (52 68 96 43), 300.815
Четверный флоат – Влажность относительная в процентах (3B 0C 19 42), 38.3%

Статусный байт ST: 
•	ST[0] – состояние нагревателя (0 – выключен, 1 – включен)
•	ST[1]  - состояние вентилятора (0 – выключен, 1 – включен)
•	ST[2] – режим работы (0 – ручное управление нагревателем и вентилятором, 1 – автоматическое)


             */

            if (_serialPort.IsOpen)
            {
                command = "getclimate";
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x16, 0x00, 0x30 };

                _serialPort.Write(bytes, 0, 5);
            }

        }

        //public bool HeaterStatus;
        //public bool FanStatus;
        //public bool AutoMode;

        //Устанавливает силовой разъем в 1
        public void SetClimate(bool HeaterStatus, bool FanStatus, bool AutoMode)
        {
            /*
Команда CLIMATE_SET (0x17)
Описание
Установить режим работы климатики: ручное, автоматическое.
Запрос
C0 82 17 01 03 33
1 байт полезной информации ST:
•	ST[0] – состояние нагревателя (0 – выключен, 1 – включен)
•	ST[1] – состояние вентилятора (0 – выключен, 1 – включен)
•	ST[2] – режим работы (0 – ручное управление нагревателем и вентилятором, 1 – автоматическое)
ST[0], ST[1] работают только при ST[2] == 0. В автоматическом режиме изменение ST[0:1] ни на что не влияет.


             */

            if (_serialPort.IsOpen)
            {
                command = "setclimate";

                //byte managebyte = 0x00;

                byte climatbyte = 0x00;
                //byte byte2 = 0x00;
                if (HeaterStatus)
                    climatbyte |= 1;
                if (FanStatus)
                    climatbyte |= 2;
                if (AutoMode)
                    climatbyte |= 4;

                Debug.Print(SingleByteToBitString(climatbyte));

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x17, 0x01, climatbyte};

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x17, 0x01, climatbyte, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                //Debug.Print(ByteArrayToString(NewBytes));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }


        public void GetWiegand()
        {
            /*
             Команда WIEGAND (0x0B)
Описание
Чтение данных из Виганда.
Запрос
C0 82 0B 00 55
Ответ
C0 81 0B 06 01 1A 8F CD CF 80 68
             */

            if (_serialPort.IsOpen)
            {
                command = "getwiegand";
                byte[] bytes = new byte[] { 0xC0, 0x82, 0x0b, 0x00, 0x55 };

                _serialPort.Write(bytes, 0, 5);
            }

        }

        //crc


        //Устанавливает силовой разъем в 1
        public void GetAllParameters()
        {

            if (_serialPort.IsOpen)
            {
                command = "getall";

                //byte managebyte = 0x00;

                //byte climatbyte = 0xff;

                //Debug.Print(SingleByteToBitString(climatbyte));

                byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x50, 0x01, 0xFF };

                byte crc = CalcCRC(bytesbefore);

                Debug.Print("CRC byte:");

                Debug.Print(SingleByteToString(crc));

                byte[] byteswithcrc = new byte[] { 0xC0, 0x82, 0x50, 0x01, 0xFF, crc };

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                //Debug.Print(ByteArrayToString(NewBytes));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }

        }

        public void SetPulseCommand(int diskret, uint diskretnum, uint width, uint delay) //дискрет 0=а 1=б 2=реле. номера соответственно 1-4 или 1-10
        {
            if (_serialPort.IsOpen)
            {
                command = "blasting";

                Pulse pulse= new Pulse();
                pulse.Delay = delay;
                if (diskret == 0)
                {
                    pulse.Pin = diskretnum;
                }
                else if (diskret == 1)
                {
                    pulse.Pin = diskretnum + 4;
                }
                else if (diskretnum == 2)
                {
                    pulse.Pin = diskretnum + 8;
                }
                pulse.Width = 1000;


                int structlen = pulse.CalculateSize();

                Debug.Print(structlen.ToString());

                byte[] pulsebytes;

                //CodedOutputStream output = new CodedOutputStream(pulsebytes);

                using (var output = new MemoryStream())
                {
                    pulse.WriteTo(output);
                    pulsebytes = output.ToArray();
                }


                Debug.Print(ByteArrayToStringX(pulsebytes));

                int beforecount = structlen + 4;
                byte[] bytesbefore = new byte[beforecount];

                bytesbefore[0] = 0xc0;
                bytesbefore[1] = 0x82;
                bytesbefore[2] = 0x51;
                bytesbefore[3] = Convert.ToByte(structlen);

                for (int i = 4; i < beforecount; i++)
                {
                    bytesbefore[i] = pulsebytes[i - 4];
                }

                //C0 82 17 01 
                Debug.Print(ByteArrayToStringX(bytesbefore));

                // byte[] bytesbefore = new byte[] { 0xC0, 0x82, 0x17, 0x01, climatbyte };

                byte crc = CalcCRC(bytesbefore);

                byte[] byteswithcrc = new byte[beforecount + 1];

                for (int i = 0; i < beforecount; i++)
                {
                    byteswithcrc[i] = bytesbefore[i];
                }
                byteswithcrc[beforecount] = crc;

                byte[] bytesafter = BytesStuffing(byteswithcrc);

                Debug.Print(ByteArrayToStringX(bytesbefore));

                _serialPort.Write(bytesafter, 0, bytesafter.Length);
            }
        }

    }
}
