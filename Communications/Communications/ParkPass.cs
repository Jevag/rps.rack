﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public delegate void LogChanged(object sender, TxRxLogEventArgs e);

    public class TxRxLogEventArgs : EventArgs
    {
        private string EventInfo;

        public TxRxLogEventArgs(string Text)
        {
            EventInfo = Text;
        }

        public string GetInfo()
        {
            return EventInfo;
        }
    }

    public class ParkPass
    {
        public event LogChanged log_changed;

        protected virtual void OnLogChanged(TxRxLogEventArgs e)
        {
            log_changed?.Invoke(this, e);
        }

        public bool IsOpen
        {
            get
            {
                return _serialPort.IsOpen;
            }
        }


        public string Result
        {
            get { return RxData; }
        }

        public string Status
        {
            get { return status; }
        }

        public SerialPort _serialPort;
        private string RxData;
        private string status;

        public byte[] CardNumber;

        public Int16 VendorID;
        public Int32 ParkingID;
        public Int32 EnterTime;

        public Byte[] rackpack;

        public byte ExitOrPere;

        public Int32 Debt;

        public byte ExitCode;

        //public string LogStuff;

        //public string OldLogStuff;

        public ParkPass()
        {
            // Create a new SerialPort object with default settings.
            _serialPort = new SerialPort();

            // Allow the user to set the appropriate properties.
            _serialPort.BaudRate = 115200;
            _serialPort.Parity = Parity.None;
            _serialPort.DataBits = 8;
            _serialPort.StopBits = StopBits.One;
            _serialPort.Handshake = Handshake.None;
            //_serialPort.DataReceived += _serialPort_DataReceived;
            RxData = "";
            // Set the read/write timeouts
            //   _serialPort.ReadTimeout = 100;
            //   _serialPort.WriteTimeout = 100;
            CardNumber = new byte[8];
            rackpack = new byte[48];
            status = "OK";
        }

        /*
        public void ComRead()
        {
            int bytescount = _serialPort.BytesToRead;
            Debug.Print("BeforePack: " + bytescount.ToString());

            byte[] InArray = new byte[bytescount];
            _serialPort.Read(InArray, 0, bytescount);
            //string t = _serialPort.ReadExisting();
            //Debug.Print(t);
            //Debug.Print(Helpers.ByteArrayToString(InArray));
            //Debug.Print(Helpers.ByteArrayEncode(InArray));

            bytescount = _serialPort.BytesToRead;
            Debug.Print("AfterPack: " + bytescount.ToString());
        }
        */

        //Первый пакет=162

        /*
        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
           // try
          //  {

                //throw new NotImplementedException();
           int bytescount = _serialPort.BytesToRead;

            Debug.Print(bytescount.ToString());

                if (bytescount == 128) //Ленина пакет!
                {
                    byte[] InArray = new byte[128];
                    _serialPort.Read(InArray, 0, 128);

                    if (InArray[0] == 0) //Въезд
                    {
                        CardNumber = new byte[8];
                        for (int i = 0; i < 8; i++)
                        {
                            CardNumber[i] = InArray[i + 1];
                        }

                        OnLogChanged(new TxRxLogEventArgs("RX: Въезд Номер карты: " + ByteArrayToString(CardNumber)));
                    }
                    else //переезд либо выезд
                    {
                        CardNumber = new byte[8];
                        for (int i = 0; i < 8; i++)
                        {
                            CardNumber[i] = InArray[i + 1];
                        }
                        rackpack = new byte[48];
                        for (int i = 0; i < 48; i++)
                        {
                            rackpack[i] = InArray[i + 9];
                        }

                        //string str = System.Text.Encoding.Default.GetString(CardNumber);
                        //LogStuff += "RX: Выезд/переезд Номер карты: " + str + Environment.NewLine;
                        OnLogChanged(new TxRxLogEventArgs("RX: Выезд/переезд Номер карты: " + ByteArrayToString(CardNumber)));


                        //string str2 = System.Text.Encoding.Default.GetString(rackpack);
                        //LogStuff += "RX: Выезд/переезд Данные: " + str + Environment.NewLine;
                        OnLogChanged(new TxRxLogEventArgs("RX: Выезд/переезд Данные: " + ByteArrayToString(rackpack)));
                    }

                }
                else //stuff
                {
                    //byte[] ArrayStuff = new byte[bytescount];
                    string t= _serialPort.ReadExisting();
                    //LogStuff += "RX: " + t + Environment.NewLine;
                    OnLogChanged(new TxRxLogEventArgs("RX: " + t));
                }
       //     }
       //     catch (Exception ex)
       //     {
       //         status += " + ошибка";
       //     }
        }
        */

        void PreInit()
        {
            status = "OK";
            VendorID = 1;
            ParkingID = 1;
            EnterTime = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            Debt = 0;
            //ExitCode = 1;
        }

        public void SendEntrancePack(byte [] rpack)
        {
            PreInit();
            //ID парковки
            byte[] EntrancePack = new byte[59];

            ExitOrPere = 0;

            byte[] vid = BitConverter.GetBytes(VendorID);
            vid = vid.Reverse().ToArray();
            byte[] pid = BitConverter.GetBytes(ParkingID);
            pid = pid.Reverse().ToArray();
            byte[] utime = BitConverter.GetBytes(EnterTime);
            utime = utime.Reverse().ToArray();
            //byte[] rackpack = new byte[48];

            //Random rnd = new Random();
            //rackpack = new Byte[48];
            //rnd.NextBytes(rackpack);

            for (int i = 0; i < 2; i++)
            {
                EntrancePack[i] = vid[i];
            }

            EntrancePack[2] = ExitOrPere;

            for (int i = 0; i < 4; i++)
            {
                EntrancePack[i + 3] = pid[i];
            }
            for (int i = 0; i < 4; i++)
            {
                EntrancePack[i + 7] = utime[i];
            }
            for (int i = 0; i < 48; i++)
            {
                EntrancePack[i + 11] = rpack[i];
            }

            byte[] outpack = new byte[128];

            for (int i = 0; i < 59; i++)
            {
                outpack[i] = EntrancePack[i];
            }

            for (int i = 59; i < 128; i++)
            {
                outpack[i] = 0;
            }

            _serialPort.Write(outpack, 0, 128);

            //LogStuff += "TX: Въезд: " + System.Text.Encoding.Default.GetString(EntrancePack) + Environment.NewLine;
            //OnLogChanged(new TxRxLogEventArgs("TX: Въезд: " + DateTime.Now.ToString("H:mm:ss:fff") + Environment.NewLine + Helpers.ByteArrayToString(outpack)));
        }

        public void SendPerePack(byte [] rpack, byte ext)
        {
            PreInit();

            byte[] vid = BitConverter.GetBytes(VendorID);
            vid = vid.Reverse().ToArray();
            ExitOrPere = 1;

            byte[] PerePack = new byte[52];

            for (int i = 0; i < 2; i++)
            {
                PerePack[i] = vid[i];
            }

            PerePack[2] = ExitOrPere;

            PerePack[3] = ext;

            for (int i = 0; i < 48; i++)
            {
                PerePack[i + 4] = rpack[i];
            }

            byte[] outpack = new byte[128];

            for (int i = 0; i < 52; i++)
            {
                outpack[i] = PerePack[i];
            }

            for (int i = 52; i < 128; i++)
            {
                outpack[i] = 0;
            }

            _serialPort.Write(outpack, 0, 128);

        }

        public void SendExitPack(byte ext)
        {
            PreInit();
            //ID парковки
            byte[] ExitPack = new byte[16];

            byte[] vid = BitConverter.GetBytes(VendorID);
            vid = vid.Reverse().ToArray();
            byte[] pid = BitConverter.GetBytes(ParkingID);
            pid = pid.Reverse().ToArray();
            byte[] utime = BitConverter.GetBytes(EnterTime);
            utime = utime.Reverse().ToArray();
            byte[] dbt = BitConverter.GetBytes(Debt);
            dbt = dbt.Reverse().ToArray();
            ExitOrPere = 2;
            //byte[] rackpack = new byte[48];

            for (int i = 0; i < 2; i++)
            {
                ExitPack[i] = vid[i];
            }
            ExitPack[2] = ExitOrPere;

            for (int i = 0; i < 4; i++)
            {
                ExitPack[i + 3] = pid[i];
            }
            for (int i = 0; i < 4; i++)
            {
                ExitPack[i + 7] = utime[i];
            }

            for (int i = 0; i < 4; i++)
            {
                ExitPack[i + 11] = dbt[i];
            }
            ExitPack[15] = ext;

            byte[] outpack = new byte[128];

            for (int i = 0; i < 16; i++)
            {
                outpack[i] = ExitPack[i];
            }

            for (int i = 16; i < 128; i++)
            {
                outpack[i] = 0;
            }

            //LogStuff += "TX: Выезд: " + System.Text.Encoding.Default.GetString(ExitPack) + Environment.NewLine;

            //OnLogChanged(new TxRxLogEventArgs("TX: Выезд: " + DateTime.Now.ToString("H:mm:ss:fff") + Environment.NewLine + Helpers.ByteArrayToString(outpack)));
            _serialPort.Write(outpack, 0, 128);
            //_serialPort.Write(ExitPack, 0, 16);
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    //status = "ожидание";
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    return true;
                }
                catch (Exception e)
                {
                    // _CardDispenserStatus.CommunicationsErrorStatus = "Возникло исключение: " + e.ToString();
                    return false;
                }
            }
            else return false;
        }



    }
}
