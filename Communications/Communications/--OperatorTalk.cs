﻿using System;
//using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Communications
{
    public class OperatorTalk
    {
        private string serverURL, deviceId;
        bool run = false;
        private bool answered = false;

        public bool Answered { get { return answered; } }
        public bool Run { get { return run; } }

        public Collection<string> ToLog = new Collection<string>();

        //Thread StatusThread;

        public string ServerURL
        {
            get { return serverURL; }
            set
            {
                if (serverURL != value)
                {
                    Stop();
                    serverURL = value;
                }
            }
        }

        public string DeviceId
        {
            get { return deviceId; }
            set
            {
                if (deviceId != value)
                {
                    Stop();
                    deviceId = value;
                }
            }
        }

        public void Stop()
        {
            Process[] pl = Process.GetProcessesByName("chrome");
            foreach (Process p in pl)
            {
                try
                {
                    p.Kill();
                }
                catch { }
            }
            pl = Process.GetProcessesByName("GoogleChromePortable");
            foreach (Process p in pl)
            {
                try
                {
                    p.Kill();
                }
                catch { }
            }
        }


        public OperatorTalk(string ServerURL, string DeviceId)
        {
            serverURL = ServerURL;
            deviceId = DeviceId;
            /*
            StatusThread = new Thread(PoolThread);
            StatusThread.Name = "CallOperatorThread";
            StatusThread.IsBackground = true;
            StatusThread.Start();
            */
        }

        public void Init()
        {
            if ((serverURL != null) && (deviceId != null))
            {
                if (Process.GetProcesses().Count(x => x.ProcessName == "GoogleChromePortable") == 0)
                {
                    Process pr = new Process();
                    pr.StartInfo.FileName = @"c:\GoogleChromePortable\GoogleChromePortable.exe";
                    //pr.StartInfo.Arguments = "--silent-launch --use-fake-ui-for-media-stream " + serverURL + "/station.html?" + deviceId.ToUpper();
                    pr.StartInfo.Arguments = "--use-fake-ui-for-media-stream " + serverURL + "/station.html?" + deviceId.ToUpper();

                    pr.Start();
                    pr.WaitForInputIdle();
                }
            }
        }

        public void Call()
        {
            Init();
            answered = false;
            run = true;
        }

        public void Answer()
        {
            answered = true;
        }
        public void HangUp()
        {
            run = false;
        }
    }
}

