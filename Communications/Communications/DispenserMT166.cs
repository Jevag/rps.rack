﻿/* Класс реализует обмен с Диспенсером карт MT166
 * Использование класса:
 * Подклбчить сборку Communications в References
 * Включить сборку с побмощью using Communications;
 * Описать переменные
 *        DispenserMT166 CardDispenser;
 *        DispenserMT166.Status CardDispenserStatus;
 *        
 * Создать экзепляры
 *           CardDispenser = new DispenserMT166();
 *           CardDispenserStatus = new DispenserMT166.Status();
 *           
 * Структура CardDispenserStatus - состояние диспенсера в соответствии с документацией +
 * состояние Ошибки после последней операции
 *           CardTimeOut - ???????
 *           CardError - ошибка размещения карты в диспенсере
 *           CardAccepting - продвижение карты в состояние PreSendPos
 *           CardDispensing - продвижение карты в состояние DespensePos
 *           CardShortage - мало карт в Боксе (порядка 10 или меньше)
 *           CardPreSendPos - карта перед считывателем
 *           CardDespensePos - карта в Губах
 *           CardEmpty - нет карт в Боксе и в приемнике (периодически глючит - при отсутствии карт говорит, что есть)
 *           
 *           CommunicationsError - ошибка при взаимодействии с Диспенсером
 *           CommunicationsErrorStatus - описание ошибки при взаимодействии с Диспенсером
 * 
 * При CommunicationsError = true 
 * в CommunicationsErrorStatus - текстовое описание ошибки
 * 
 * CardDispenserStatus обновляется при запросе GetStatus, 
 * кроме полей CommunicationsError и CommunicationsErrorStatus
 * 
 * Поля CommunicationsError и CommunicationsErrorStatus обновляются при любом запросе к диспенсеру
 * 
 * Методы (ворвращают true в случае корректного выполнения и false в случае ошибки):
 * Все метады связанные с движением карты имеют необязательный параметр TimeOut - это время максимальное ожидания исполнения операции механикой
 * По умолчанию 5 сек. 
 * Если операция завершается ранее - метод возврощает результат операции. 
 * Если операция завершается позже - метод возвращает ошибку CommunicationsError с описанием
 * 
 * bool Open(string ComPortName); // Открывает соединение с диспенсером карт по указанному порту
 * bool Close(); // Закрывает соединение
 * bool GetVersion(ref string Version); // Получние версии софта диспенсера
 * bool GetStatus(); // Получение текущего статуса (Статус в структуре CardDispenserStatus)
 * bool SendFromBoxToReadPosition(int TimeOut = 5); // Выполняет запрос "Подать карту из Бокса под считыватель"
 * bool SendCardToBezelAndHold(int TimeOut = 5); // Выполняет запрос "Выдать карту в губы"
 * bool SendCardToBezelAndNotHold(int TimeOut = 5); // Выполняет запрос "Выбросить карту" (не задерживая в губах)
 * bool SendCardToRecycleBox(int TimeOut = 5); // Выполняет запрос "Забрать карту в корзину"
 * bool RecycleToReadPosition(int TimeOut = 5); // Выполняет запрос "Вернуть карту из губ под считыватель" (не заботает в текущей версии Диспенсера)
 * 
 * При работе - с открытым соединением всегда сначала необходимо запросить статус диспенсера
 * Только по результатам запроса сатуса можно производить дальнейшие действия
*/
using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Text;
using System.Threading;
//using System.Windows.Forms;

namespace Communications
{
    public class DispenserMT166
    {
        byte ETX = 0x03;
        byte NAK = 0x15;

        struct Result
        {
            internal int Length;
            internal byte CW;
            internal byte CP;
            internal byte[] DP;
        }

        public struct DispensStatus                // статус , получаемый от диспенсера в ответ на GetStatus
        {
            public bool CardTimeOut;
            public bool CardError;
            public bool CardAccepting;
            public bool CardDispensing;
            public bool CardShortage;
            public bool CardPreSendPos;
            public bool CardDespensePos;
            public bool CardEmpty;
            public bool CommunicationsError;
            public string CommunicationsErrorStatus;
            public override string ToString()
            {
                string res = "";
                res += "CardTimeOut=" + CardTimeOut.ToString() + Environment.NewLine;
                res += "CardError=" + CardError.ToString() + Environment.NewLine;
                res += "CardAccepting=" + CardAccepting.ToString() + Environment.NewLine;
                res += "CardDispensing=" + CardDispensing.ToString() + Environment.NewLine;
                res += "CardShortage=" + CardShortage.ToString() + Environment.NewLine;
                res += "CardPreSendPos=" + CardPreSendPos.ToString() + Environment.NewLine;
                res += "CardDespensePos=" + CardDespensePos.ToString() + Environment.NewLine;
                res += "CardEmpty=" + CardEmpty.ToString() + Environment.NewLine;
                res += "CommunicationsError=" + CommunicationsError.ToString() + Environment.NewLine;
                res += "CommunicationsErrorStatus=" + CommunicationsErrorStatus + Environment.NewLine;
                return res;
            }
        }

        DispensStatus _CardDispenserStatus;

        public string Res202Value = "";

        public DispensStatus CardDispenserStatus
        {
            get
            {
                return _CardDispenserStatus;
            }
        }

        public bool IsOutPut = false;

        public bool IsOpen
        {
            get
            {
                return _serialPort.IsOpen;
            }
        }

        SerialPort _serialPort;
        byte[] DespenserRes;


        private int rxSize = 0;
        private int Length = 0;
        private byte[] rx = new byte[256];              // прием из Dispens 

        private string status;                          // статус Dispens текст
        private string statusCard;                      // статус карт есть/мало/нет
        private string version;

        public string Status { get { return status; } }             // статус Dispens Текст
        public string StatusCard { get { return statusCard; } }             // статус карт есть/мало/нет
        public string Version { get { return version; } }

        public byte[] Rx { get { return rx; } }                     // прием из Dispens

        public DispenserMT166()
        {
            _CardDispenserStatus = new DispensStatus();
            DespenserRes = new byte[100];

            // Create a new SerialPort object with default settings.
            _serialPort = new SerialPort();

            // Allow the user to set the appropriate properties.
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = System.IO.Ports.Parity.None;
            _serialPort.DataBits = 8;
            _serialPort.StopBits = System.IO.Ports.StopBits.One;
            _serialPort.Handshake = System.IO.Ports.Handshake.None;

            // Set the read/write timeouts
            _serialPort.ReadTimeout = 100;
            _serialPort.WriteTimeout = 100;

        }
        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    return true;
                }
                catch (Exception e)
                {
                    _CardDispenserStatus.CommunicationsErrorStatus = "Возникло исключение: " + e.ToString();
                    status = "не отвечает";
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    return true;
                }
                catch (Exception e)
                {
                    _CardDispenserStatus.CommunicationsErrorStatus = "Возникло исключение: " + e.ToString();
                    return false;
                }
            }
            else return true;
        }

        string AddBCC(string Req)
        {
            byte[] bstr = Encoding.ASCII.GetBytes(Req);
            int BCC = 0;
            foreach (byte b in bstr)
            {
                BCC = BCC ^ b;
            }
            Req = String.Concat(Req, ((char)BCC).ToString());
            return Req;
        }

        //bool CheckBCC(byte[] bstr, int charLRC)
        //{
        //    int BCC = 0;
        //    foreach (byte b in bstr)
        //    {
        //        BCC = BCC ^ b;
        //    }
        //    bool check = (BCC == charLRC);
        //    return check;
        //}
        private bool CheckBCC()         // может проверить STX ETX ????
        {
            byte BCC = 0;
            Length = (rx[1] << 8) + rx[2];
            for (int i = 0; i < Length + 4; i++)
            {
                BCC = (byte)(BCC ^ rx[i]);
            }
            if (BCC == rx[Length + 4]) return true;

            else return false;
        }

        //bool GetBytesFromPort(ref byte[] bstr)
        //{
        //    byte[] Msg = new byte[100];
        //    int BCC;
        //    bool check;
        //    try
        //    {
        //        Msg[0] = (byte)_serialPort.ReadByte();
        //        if (Msg[0] == NAK)
        //        {
        //            _CardDispenserStatus.CommunicationsErrorStatus = "Неподдкрживаемая команда" ;
        //            _CardDispenserStatus.CommunicationsError = true;
        //            return false;
        //        }
        //        else
        //        {
        //            _CardDispenserStatus.CommunicationsErrorStatus = "";
        //            _CardDispenserStatus.CommunicationsError = false;
        //        }
        //        Msg[1] = (byte)_serialPort.ReadByte();
        //        Msg[2] = (byte)_serialPort.ReadByte();
        //        int Length = (Msg[1] << 8) + Msg[2];
        //        for (int i = 3; i <= Length + 3; i++)
        //            Msg[i] = (byte)_serialPort.ReadByte();

        //        BCC = _serialPort.ReadByte();
        //    }
        //    catch (Exception e)
        //    {
        //        _CardDispenserStatus.CommunicationsErrorStatus = "Возникло исключение: " + e.ToString();
        //        _CardDispenserStatus.CommunicationsError = true;
        //        return false;
        //    }
        //    check = CheckBCC(Msg, BCC);

        //    if (check)
        //    {
        //        int i = Array.IndexOf(Msg, ETX, 3);
        //        Array.Copy(Msg, 1, bstr, 0, i);
        //        return true;
        //    }
        //    else
        //    {
        //        bstr = Encoding.ASCII.GetBytes("");
        //        return false;
        //    }
        //}

        Result ParseResult(byte[] res)
        {
            Result Res = new Result();
            Res.Length = (res[0] << 8) + res[1];    //length
            Res.CW = res[2];                        //command
            Res.CP = res[3];                        //command parameter
            Res.DP = new byte[Res.Length - 2];      //data
            Array.Copy(res, 4, Res.DP, 0, Res.Length - 2);
            return Res;
        }

        Result SendAndGetResult(string Req, int TimeOut)
        {
            Result Res = new Result();
            //int i = 0;
            //int t = TimeOut*1000/10;
            try
            {
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
                rxSize = 0;
                status = "ожидание";
               
                _serialPort.Write(Req);
                //while ((_serialPort.BytesToRead == 0) && (i < t))
                //{
                //    Thread.Sleep(10);
                //    i++;
                //}
                //if (_serialPort.BytesToRead != 0)
                //{
                //    if (GetBytesFromPort(ref DespenserRes))
                //    {
                //        Res = ParseResult(DespenserRes);
                //    }
                //}
                //else
                //{
                //    _CardDispenserStatus.CommunicationsErrorStatus = "Таймаут при запросе";
                //    _CardDispenserStatus.CommunicationsError = true;
                //}
                _CardDispenserStatus.CommunicationsError = false;
            }
            catch (Exception e)
            {
                _CardDispenserStatus.CommunicationsErrorStatus = "Возникло исключение: " + e.ToString();
                _CardDispenserStatus.CommunicationsError = true;
            }
            return Res;
        }

        //--------------------------------------

        public bool GetVersion()
        {
            string Req = "\x02\x00\x02\x30\x30\x03";
            Req = AddBCC(Req);
            Result Res = SendAndGetResult(Req,1 );
            int OperStatus;
            if (!_CardDispenserStatus.CommunicationsError) 
            //{
            //    OperStatus = Res.DP[0];
            //    if (OperStatus == 0x59)
            //    {
            //        for (int i = 2; i < Res.DP.Length; i++)
            //        {
            //            Version += (char)Res.DP[i];
            //        }
                    return true;
            //    }
            //    else if (OperStatus == 0x4E)
            //    {
            //        Version = "Unknown";
            //        return false;
            //    }
            //    else
            //    {
            //        Version = "Unknown";
            //        return false;
            //    }
            //}
            else
            {
                //Version = "Unknown";
                return false;
            }
        }

        public bool SendFromBoxToReadPosition(int TimeOut = 5)  //"выдать под ридер"
        {
            string Req = "\x02\x00\x02\x31\x30\x03";
            Req = AddBCC(Req);
            Result Res = SendAndGetResult(Req, TimeOut);
            int OperStatus;
            if (!_CardDispenserStatus.CommunicationsError)
            {
                //OperStatus = Res.DP[0];
                //if (OperStatus == 0x59)
                //{
                    return true;
                //}
                //else if (OperStatus == 0x4E)
                //{
                //    return false;
                //}
                //else
                //{
                //    return false;
                //}
            }
            else
            {
                return false;
            }
        }
        public bool SendCardToBezelAndHold(int TimeOut = 5)  //"выдать карту"
        {
            string Req = "\x02\x00\x02\x31\x31\x03";
            Req = AddBCC(Req);
            Result Res = SendAndGetResult(Req, TimeOut);
            int OperStatus;
            if (!_CardDispenserStatus.CommunicationsError)
            {
                //OperStatus = Res.DP[0];
                //if (OperStatus == 0x59)
                //{
                    return true;
                //}
                //else if (OperStatus == 0x4E)
                //{
                //    return false;
                //}
                //else
                //{
                //    return false;
                //}
            }
            else
            {
                return false;
            }

        }

        public bool SendFromBezelToReadPosition(int TimeOut = 5)  //"втянуть карту"
        {
            string Req = "\x02\x00\x02\x33\x32\x03";
            Req = AddBCC(Req);
            Result Res = SendAndGetResult(Req, TimeOut);
            int OperStatus;
            if (!_CardDispenserStatus.CommunicationsError)
            {
                //OperStatus = Res.DP[0];
                //if (OperStatus == 0x59)
                //{
                return true;
                //}
                //else if (OperStatus == 0x4E)
                //{
                //    return false;
                //}
                //else
                //{
                //    return false;
                //}
            }
            else
            {
                return false;
            }

        }
        public bool SendCardToBezelAndNotHold(int TimeOut = 5)
        {
            string Req = "\x02\x00\x02\x31\x32\x03";
            Req = AddBCC(Req);
            Result Res = SendAndGetResult(Req, TimeOut);
            int OperStatus;
            if (!_CardDispenserStatus.CommunicationsError)
            {
                //OperStatus = Res.DP[0];
                //if (OperStatus == 0x59)
                //{
                    return true;
                //}
                //else if (OperStatus == 0x4E)
                //{
                //    return false;
                //}
                //else
                //{
                //    return false;
                //}
            }
            else
            {
                return false;
            }

        }

        public bool GetStatus()   //"GetStatus"
        {
            string Req = "\x02\x00\x02\x32\x30\x03";
            Req = AddBCC(Req);
            Result Res = SendAndGetResult(Req, 1);
            if (!_CardDispenserStatus.CommunicationsError)
            {
                //_CardDispenserStatus.CardTimeOut = ((Res.DP[0] & 1) == 1);
                //_CardDispenserStatus.CardError = ((Res.DP[0] & 2) == 2);
                //_CardDispenserStatus.CardAccepting = ((Res.DP[0] & 4) == 4);
                //_CardDispenserStatus.CardDispensing = ((Res.DP[0] & 8) == 8);
                //_CardDispenserStatus.CardShortage = ((Res.DP[0] & 16) == 16);
                //_CardDispenserStatus.CardPreSendPos = ((Res.DP[0] & 32) == 32);
                //_CardDispenserStatus.CardDespensePos = ((Res.DP[0] & 64) == 64);
                //_CardDispenserStatus.CardEmpty = ((Res.DP[0] & 128) == 128);
                return true;
            }
            else
            {
                _CardDispenserStatus.CardTimeOut = false;
                _CardDispenserStatus.CardError = false;
                _CardDispenserStatus.CardAccepting = false;
                _CardDispenserStatus.CardDispensing = false;
                _CardDispenserStatus.CardShortage = false;
                _CardDispenserStatus.CardPreSendPos = false;
                _CardDispenserStatus.CardDespensePos = false;
                _CardDispenserStatus.CardEmpty = false;
                return false;
            }
        }

        public bool SendCardToRecycleBox(int TimeOut = 5)   //"карту в отказник"
        {
            string Req = "\x02\x00\x02\x33\x30\x03";
            Req = AddBCC(Req);
            Result Res = SendAndGetResult(Req, TimeOut);
            int OperStatus;
            if (!_CardDispenserStatus.CommunicationsError)
            {
                //OperStatus = Res.DP[0];
                //if (OperStatus == 0x59)
                //{
                    return true;
                //}
                //else if (OperStatus == 0x4E)
                //{
                //    return false;
                //}
                //else
                //{
                //    return false;
                //}
            }
            else
            {
                return false;
            }

        }
        public bool RecycleToReadPosition(int TimeOut = 5)  // "опять под ридер"
        {
            string Req = "\x02\x00\x02\x33\x31\x03";
            Req = AddBCC(Req);
            Result Res = SendAndGetResult(Req, TimeOut);
            int OperStatus;
            if (!_CardDispenserStatus.CommunicationsError)
            {
                //OperStatus = Res.DP[0];
                //if (OperStatus == 0x59)
                //{
                    return true;
                //}
                //else if (OperStatus == 0x4E)
                //{
                //    return false;
                //}
                //else
                //{
                //    return false;
                //}
            }
            else
            {
                return false;
            }
        }

        public bool Wait1()                        // ожидание ответа
        {
            try
            {
                bool bitReady = false;

                if (_serialPort.BytesToRead > 0)
                {
                    int _rxSize = _serialPort.BytesToRead;
                    _serialPort.Read(
                        rx,
                        rxSize,
                        _serialPort.BytesToRead
                        );
                    rxSize += _rxSize;
                    if (rxSize > 7 && rxSize >= (rx[1] << 8) + rx[2] + 5 && CheckBCC()) // "ответ"
                    {
                        if (rx[0] == 0x02)      // STX
                        {
                            switch (rx[3])
                            {
                                case 0x30:  // GetVersion
                                    int OperStatus;
                                    if (!_CardDispenserStatus.CommunicationsError)
                                    {
                                        OperStatus = rx[5];
                                        if (OperStatus == 0x59)
                                        {
                                            version = " ";
                                            for (int i = 0; i < Length - 3; i++)
                                            {
                                                version += (char)rx[i + 6];
                                            }
                                        }
                                        else if (OperStatus == 0x4E)
                                        {
                                            version = "Unknown";
                                        }
                                        else
                                        {
                                            version = "Unknown";
                                        }
                                    }
                                    else
                                    {
                                        version = "Unknown";
                                    }
                                    status = "неопределен";
                                    break;

                                case 0x32:  // GetStatus
                                    if (!_CardDispenserStatus.CommunicationsError)
                                    {

                                        _CardDispenserStatus.CardTimeOut = ((rx[5] & 1) == 1);
                                        _CardDispenserStatus.CardError = ((rx[5] & 2) == 2);            // - ошибка размещения карты в диспенсере
                                        _CardDispenserStatus.CardAccepting = ((rx[5] & 4) == 4);        // - продвижение карты в состояние PreSendPos
                                        _CardDispenserStatus.CardDispensing = ((rx[5] & 8) == 8);       // - продвижение карты в состояние DespensePos
                                        _CardDispenserStatus.CardShortage = ((rx[5] & 16) == 16);       // - мало карт в Боксе (порядка 10 или меньше)
                                        _CardDispenserStatus.CardPreSendPos = ((rx[5] & 32) == 32);     // - карта перед считывателем
                                        _CardDispenserStatus.CardDespensePos = ((rx[5] & 64) == 64);    // - карта в Губах
                                        _CardDispenserStatus.CardEmpty = ((rx[5] & 128) == 128);        // - нет карт в Боксе и в приемнике
                                        status = "неопределен";

                                        if (rxSize == 10 && IsOutPut) //Прошивка 202?????
                                        {
                                            bitReady = ((rx[7] & 1) == 1);
                                            /*
                                            bool bit2 = ((rx[7] & 2) == 2);            
                                            bool bit3 = ((rx[7] & 4) == 4);        
                                            bool bit4 = ((rx[7] & 8) == 8);       
                                            bool bit5 = ((rx[7] & 16) == 16);       
                                            bool bit6 = ((rx[7] & 32) == 32);     
                                            bool bit7 = ((rx[7] & 64) == 64);    
                                            bool bit8 = ((rx[7] & 128) == 128);
                                            Res202Value = bit1.ToString() + " " + bit2.ToString() + " " + bit3.ToString() + " "
                                                + bit4.ToString() + " " + bit5.ToString() + " " + bit6.ToString() + " "
                                                + bit7.ToString() + " " + bit8.ToString();
                                                */
                                            if (_CardDispenserStatus.CardPreSendPos && bitReady) status = "карта под ридером";

                                        }
                                        else
                                        {
                                            if (_CardDispenserStatus.CardPreSendPos) status = "карта под ридером";
                                        }

                                        if (_CardDispenserStatus.CardDespensePos) status = "карта в губах";
                                        //if (_CardDispenserStatus.CardDespensePos) status = "карта под ридером";

                                        if (!_CardDispenserStatus.CardAccepting && !_CardDispenserStatus.CardDispensing &&
                                            !_CardDispenserStatus.CardPreSendPos && !_CardDispenserStatus.CardDespensePos) status = "нет карты в тракте";
                                        if (!_CardDispenserStatus.CardAccepting && !_CardDispenserStatus.CardDispensing && 
                                            _CardDispenserStatus.CardEmpty && !_CardDispenserStatus.CardPreSendPos && !_CardDispenserStatus.CardDespensePos)
                                            //status = "нет карты в тракте";
                                            status = "нет карт";
                                        if (!_CardDispenserStatus.CardEmpty && _CardDispenserStatus.CardShortage) statusCard = "мало карт";
                                        if (_CardDispenserStatus.CardEmpty) statusCard = "нет карт";
                                        if (!_CardDispenserStatus.CardEmpty && !_CardDispenserStatus.CardShortage) statusCard = "есть карты";
                                        if (_CardDispenserStatus.CardError) status = "карта застряла";
                                    }
                                    else
                                    {
                                        _CardDispenserStatus.CardTimeOut = false;
                                        _CardDispenserStatus.CardError = false;
                                        _CardDispenserStatus.CardAccepting = false;
                                        _CardDispenserStatus.CardDispensing = false;
                                        _CardDispenserStatus.CardShortage = false;
                                        _CardDispenserStatus.CardPreSendPos = false;
                                        _CardDispenserStatus.CardDespensePos = false;
                                        _CardDispenserStatus.CardEmpty = false;
                                    }

                                    break;

                                default:
                                    OperStatus = rx[5];
                                    if (OperStatus == 0x59)
                                    {
                                        //status = "Operate successfully";
                                        status = "неопределен";
                                    }
                                    else 
                                    {
                                        //status = "Operate failed";
                                        status = "неопределен";
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            status = "неопределен";     // ошибка в ответе, может status = "не отвечает";
                        }
                    }
                    else
                    {
                        status = "ожидание";
                    }
                }
                else
                {
                    status = "ожидание";
                }
                return true;
            }
            catch (Exception e)
            {
                status = "не отвечает";
                //Console.WriteLine("Exception5: " + e.Message);
                return false;
            }
            
        }
    }
}
