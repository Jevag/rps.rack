﻿//Сканер Barcode RT200

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;


namespace Communications
{
    public class RT200Barcode
    {
        SerialPort _serialPort;
        private string status;                          // статус сканера Barcode
        public string Status { get { return status; } }             // статус сканера Barcode
        private int txSize = 0;
        private int rxSize = 0;
        private int Length = 0;
        private byte[] tx = new byte[256];              // передача в сканер 
        private byte[] rx = new byte[256];              // прием из сканера 
        private bool booli;
        public string QRid { get { return qrid; } }             //  QRcode
        private string qrid;                          //  QRcode

        public RT200Barcode()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 115200;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 200;
            _serialPort.ReadTimeout = 200;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";

                    return false;
                }
            }
            else
            {
                //GetStatus();
                return true;
            }
        }


        public bool StartDecoding()
        {
            tx[0] = 0x32; tx[1] = 0x75; tx[2] = 0x01;
            txSize = 3;
            try
            {
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
                _serialPort.Write(tx, 0, txSize);
                rxSize = 0;
                qrid = "";
                status = "ожидание";
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception3: " + e.Message);
                return false;
            }
        }

        public bool StopDecoding()
        {
            tx[0] = 0x32; tx[1] = 0x75; tx[2] = 0x02;
            txSize = 3;
            try
            {
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
                _serialPort.Write(tx, 0, txSize);
                rxSize = 0;
                qrid = "";
                status = "ожидание";
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception3: " + e.Message);
                return false;
            }
        }




        //public bool Connect()
        //{
        //    tx[0] = (byte)'C'; tx[1] = (byte)'M'; tx[2] = (byte)'D';
        //    tx[3] = 0x00; tx[4] = 0x01;                         // длинна команды/данных
        //    tx[5] = 0x52;  //tx[5] = (byte) 'R';                // поле команды/данных
        //    if (BccTx()) return true;
        //    else return false;
        //}

        //public bool Disconnect()
        //{
        //    tx[0] = (byte)'C'; tx[1] = (byte) 'M';tx[2] = (byte)'D';
        //    tx[3] = 0x00; tx[4] = 0x02;
        //    tx[5] = 0x44; tx[6] = 0x43; //tx[5] = (byte) 'D';tx[6] = (byte)'C';
        //    if (BccTx()) return true;
        //    else return false;
        //}

        //public bool HandshakeCMD()
        //{
        //    tx[0] = (byte)'C'; tx[1] = (byte)'M'; tx[2] = (byte)'D';
        //    tx[3] = 0x00; tx[4] = 0x02;
        //    tx[5] = 0x59;  //tx[5] = (byte) 'Y';
        //    if (BccTx()) return true;
        //    else return false;
        //}

        //public bool GetHardwareInformation()
        //{
        //    tx[0] = (byte)'C'; tx[1] = (byte)'M'; tx[2] = (byte)'D';
        //    tx[3] = 0x00; tx[4] = 0x05;
        //    //tx[5:9] = ”85505”;
        //    if (BccTx()) return true;
        //    else return false;
        //}






        //private bool BccTx()
        //{
        //    txSize = (tx[3] << 8) + tx[4] + 7;
        //    tx[txSize - 1] = 0;                     // CRC low
        //    tx[txSize - 2] = 0;                     // CRC high
        //    for (int i = 0; i < txSize - 1; i++)
        //    {
        //        tx[txSize - 1] = (byte)(tx[txSize - 1] ^ tx[i]);
        //    }

        //    try
        //    {
        //        _serialPort.DiscardInBuffer();
        //        _serialPort.DiscardOutBuffer();
        //        _serialPort.Write(tx, 0, txSize);
        //        rxSize = 0;
        //        status = "ожидание";
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Exception3: " + e.Message);
        //        return false;
        //    }
        //}


        public bool Wait()
        {
            try
            {
                if (_serialPort.BytesToRead > 0)
                {
                    int _rxSize = _serialPort.BytesToRead;
                    _serialPort.Read(
                        rx,
                        rxSize,
                        _serialPort.BytesToRead
                        );
                    rxSize += _rxSize;
                }
                else
                {

                }
                //if (rxSize > 4 && _rxSize != 0)     // был еще прием, возможно - в процессе
                //{
                //    status = "ожидание";
                //}
                //else
                if (rxSize > 4) // "ответ"
                    {
                        switch (tx[2])
                        {
                            case 0x01:  //StartDecoding
                                if (tx[0] == 0x32 && tx[1] == 0x75 && tx[2] == 0x01 && rxSize > 35) // "QRcode "
                                {
                                    for (int i = 0; i < 36; i++)
                                    {
                                        qrid += (char)rx[i];   // QRcode
                                    }

                                    status = "QRid";
                                    break;
                                }
                                if (tx[0] == 0x32 && tx[1] == 0x75 && tx[2] == 0x01 && rxSize > 0) // "QRidInProcess "
                                {
                                    status = "QRidInProcess";
                                    break;
                                }
                                //else status = "неопределен";
                                break;

                            case 0x02:  //StopDecoding
                                if (tx[0] == 0x32 && tx[1] == 0x75 && tx[2] == 0x02)
                                if (rx[0] == 0x33 && rx[1] == 0x75 && rx[2] == 0x02 && rx[3] == 0x00) status = "StopDecoding";
                                
                                //else status = "неопределен";
                                break;
                            default:
                                //status = "неопределен";
                                break;
                        }
                    
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                status = "не отвечает";
                
                return false;
            }


            //return booli;
            //return true;
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    //GetStatus();
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    return false;
                }
            }
            else
            {
                //GetStatus();
                return true;
            }
        }
    }
}
