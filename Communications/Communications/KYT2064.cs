﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class KYT2064
    {
        SerialPort COM;

        System.Windows.Forms.Timer Waiting;

        private const byte STX = 0x02;
        private const byte ETX = 0x03;
        private const byte ENQ = 0x05;
        private const byte ACK = 0x06;
        private const byte NAK = 0x15;
        private const byte CAN = 0x18;

        private byte lastbyte=0x00;

        public string CommandName = "";

        public struct DeviceBitStatus
        {
            public bool b10;
            public bool b11;
            public bool b12;
            public bool b14;
            public bool b15;
            public bool b16;
            public bool b20;
            public bool b21;
            public bool b22;
            public bool b23;
            public bool b24;
            public bool b25;
            public bool b26;

            public string b10s;
            public string b11s;
            public string b12s;
            public string b14s;
            public string b15s;
            public string b16s;
            public string b20s;
            public string b21s;
            public string b22s;
            public string b23s;
            public string b24s;
            public string b25s;
            public string b26s;
        }

        public DeviceBitStatus CurrentDeviceState;

        public const string b10_name = "Bin Status:";
        public const string b10_0 = "Bin Empty";
        public const string b10_1 = "Error Bin Full";

        public const string b11_name = "Move Sensor:";
        public const string b11_0 = "Move Sensor Non-Detection";
        public const string b11_1 = "Move Sensor Detection";

        public const string b12_name = "Finish Sensor:";
        public const string b12_0 = "Finish Sensor Non-Detection";
        public const string b12_1 = "Finish Sensor Detection";

        public const string b14_name = "Stack#1 Finish Sensor:";
        public const string b14_0 = "Stack#1 Finish Sensor Non-Detection";
        public const string b14_1 = "Stack#1 Finish Sensor Detection";

        public const string b15_name = "Stack#2 Finish Sensor:";
        public const string b15_0 = "Stack#2 Finish Sensor Non-Detection";
        public const string b15_1 = "Stack#2 Finish Sensor Detection";

        public const string b16_name = "Ready Status:";
        public const string b16_0 = "Ready";
        public const string b16_1 = "Busy (Inhibit)";

        public const string b20_name = "Stack #1 Empty Status:";
        public const string b20_0 = "Stack #1 Good";
        public const string b20_1 = "Stack #1 Empty";

        public const string b21_name = "Stack #2 Empty Status:";
        public const string b21_0 = "Stack #2 Good";
        public const string b21_1 = "Stack #2 Empty";

        public const string b22_name = "Stack #1 Warning Status:";
        public const string b22_0 = "Stack #1 Good";
        public const string b22_1 = "Stack #1 Warning";

        public const string b23_name = "Stack #2 Warning Status:";
        public const string b23_0 = "Stack #2 Good";
        public const string b23_1 = "Stack #2 Warning";

        public const string b24_name = "Motor #1 Status:";
        public const string b24_0 = "Motor #1 Good";
        public const string b24_1 = "Motor #1 Error";

        public const string b25_name = "Motor #2 Status:";
        public const string b25_0 = "Motor #2 Good";
        public const string b25_1 = "Motor #2 Error";

        public const string b26_name = "Motor #3 Status:";
        public const string b26_0 = "Motor #3 Good";
        public const string b26_1 = "Motor #3 Error";

        public string CurrentVersion = "";

        public string Status = "";

        public string CardStatus = "";
        public string BoxStatus = "";

        public bool IsError = false;

        public void DisplayStatus()
        {
            if (CurrentDeviceState.b10 == false)
            {
                CurrentDeviceState.b10s = b10_name + " " + b10_0;
            }
            else if (CurrentDeviceState.b10 == true)
            {
                CurrentDeviceState.b10s = b10_name + " " + b10_1;
            }

            if (CurrentDeviceState.b11 == false)
            {
                CurrentDeviceState.b11s = b11_name + " " + b11_0;
            }
            else if (CurrentDeviceState.b11 == true)
            {
                CurrentDeviceState.b11s = b11_name + " " + b11_1;
            }

            if (CurrentDeviceState.b12 == false)
            {
                CurrentDeviceState.b12s = b12_name + " " + b12_0;
            }
            else if (CurrentDeviceState.b12 == true)
            {
                CurrentDeviceState.b12s = b12_name + " " + b12_1;
            }
            if (CurrentDeviceState.b14 == false)
            {
                CurrentDeviceState.b14s = b14_name + " " + b14_0;
            }
            else if (CurrentDeviceState.b14 == true)
            {
                CurrentDeviceState.b14s = b14_name + " " + b14_1;
            }

            if (CurrentDeviceState.b15 == false)
            {
                CurrentDeviceState.b15s = b15_name + " " + b15_0;
            }
            else if (CurrentDeviceState.b15 == true)
            {
                CurrentDeviceState.b15s = b15_name + " " + b15_1;
            }

            if (CurrentDeviceState.b16 == false)
            {
                CurrentDeviceState.b16s = b16_name + " " + b16_0;
            }
            else if (CurrentDeviceState.b16 == true)
            {
                CurrentDeviceState.b16s = b16_name + " " + b16_1;
            }

            if (CurrentDeviceState.b20 == false)
            {
                CurrentDeviceState.b20s = b20_name + " " + b20_0;
            }
            else if (CurrentDeviceState.b20 == true)
            {
                CurrentDeviceState.b20s = b20_name + " " + b20_1;
            }

            if (CurrentDeviceState.b21 == false)
            {
                CurrentDeviceState.b21s = b21_name + " " + b21_0;
            }
            else if (CurrentDeviceState.b21 == true)
            {
                CurrentDeviceState.b21s = b21_name + " " + b21_1;
            }

            if (CurrentDeviceState.b22 == false)
            {
                CurrentDeviceState.b22s = b22_name + " " + b22_0;
            }
            else if (CurrentDeviceState.b22 == true)
            {
                CurrentDeviceState.b22s = b22_name + " " + b22_1;
            }

            if (CurrentDeviceState.b23 == false)
            {
                CurrentDeviceState.b23s = b23_name + " " + b23_0;
            }
            else if (CurrentDeviceState.b23 == true)
            {
                CurrentDeviceState.b23s = b23_name + " " + b23_1;
            }

            if (CurrentDeviceState.b24 == false)
            {
                CurrentDeviceState.b24s = b24_name + " " + b24_0;
            }
            else if (CurrentDeviceState.b24 == true)
            {
                CurrentDeviceState.b24s = b24_name + " " + b24_1;
            }

            if (CurrentDeviceState.b25 == false)
            {
                CurrentDeviceState.b25s = b25_name + " " + b25_0;
            }
            else if (CurrentDeviceState.b25 == true)
            {
                CurrentDeviceState.b25s = b25_name + " " + b25_1;
            }

            if (CurrentDeviceState.b26 == false)
            {
                CurrentDeviceState.b26s = b26_name + " " + b26_0;
            }
            else if (CurrentDeviceState.b26 == true)
            {
                CurrentDeviceState.b26s = b26_name + " " + b26_1;
            }

            Status = CurrentDeviceState.b10s + Environment.NewLine;
            Status += CurrentDeviceState.b11s + Environment.NewLine;
            Status += CurrentDeviceState.b12s + Environment.NewLine;
            Status += CurrentDeviceState.b14s + Environment.NewLine;
            Status += CurrentDeviceState.b15s + Environment.NewLine;
            Status += CurrentDeviceState.b16s + Environment.NewLine;
            Status += CurrentDeviceState.b20s + Environment.NewLine;
            Status += CurrentDeviceState.b21s + Environment.NewLine;
            Status += CurrentDeviceState.b22s + Environment.NewLine;
            Status += CurrentDeviceState.b23s + Environment.NewLine;
            Status += CurrentDeviceState.b24s + Environment.NewLine;
            Status += CurrentDeviceState.b25s + Environment.NewLine;
            Status += CurrentDeviceState.b26s + Environment.NewLine;

            CardStatus = "есть карты";

            if (CurrentDeviceState.b20 && CurrentDeviceState.b21)
            {
                CardStatus = "нет карт";
            }
            if (CurrentDeviceState.b20 && CurrentDeviceState.b23)
            {
                CardStatus = "мало карт";
            }
            if (CurrentDeviceState.b21 && CurrentDeviceState.b22)
            {
                CardStatus = "мало карт";
            }
            if (CurrentDeviceState.b22 && CurrentDeviceState.b23)
            {
                CardStatus = "мало карт";
            }

            if (CurrentDeviceState.b10)
            {
                BoxStatus = "корзина переполнена";
            }
            else
            {
                BoxStatus = "корзина ок";
            }
            
            /*
            if (CurrentDeviceState.b24 || CurrentDeviceState.b25 || CurrentDeviceState.b26)
            {
                IsError = true;
            }
            */
        }

        public bool GetBit(byte b, int bitNumber)
        {
            //black magic goes here
            var bit = (b & (1 << bitNumber)) != 0;
            return (bool)bit;
        }

        //public methods
        public bool Open(string Port)
        {
            if (!COM.IsOpen)
            {
                try
                {
                    COM.PortName = Port;
                    COM.Open();
                    return true;
                }
                catch (Exception e)
                {
                    // _CardDispenserStatus.CommunicationsErrorStatus = "Возникло исключение: " + e.ToString();
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        public bool Close()
        {
            if (COM.IsOpen)
            {
                try
                {
                    COM.Close();
                    return true;
                }
                catch (Exception e)
                {
                    // _CardDispenserStatus.CommunicationsErrorStatus = "Возникло исключение: " + e.ToString();
                    return false;
                }
            }
            else return false;
        }

        public bool IsOpen
        {
            get
            {
                return COM.IsOpen;
            }
        }

        byte LRC(byte[] bytes)
        {
            int LRC = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                LRC ^= bytes[i];
            }
            return (byte)LRC;
        }

        void cmd(byte b)
        {
            //IsError = true;
            Waiting.Start();

            try
            {
                byte[] s_byte = new byte[4] { STX, b, ETX, LRC(new byte[3] { STX, b, ETX }) };
                COM.Write(s_byte, 0, 4);
            }
            catch (Exception ex)
            {
                IsError = true;
            }
        }

        //Constructor
        public KYT2064()
        {
            // Create a new SerialPort object with default settings.
            COM = new SerialPort();

            // Allow the user to set the appropriate properties.
            COM.BaudRate = 9600;
            COM.Parity = Parity.None;
            COM.DataBits = 8;
            COM.StopBits = StopBits.One;
            COM.Handshake = Handshake.None;
            COM.DataReceived += COM_DataReceived;

            Waiting = new System.Windows.Forms.Timer();
            Waiting.Interval = 3000;
            Waiting.Tick += Waiting_Tick;
        }

        private void Waiting_Tick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            IsError = true;
            Status = "не отвечает";
        }

        private void COM_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                //throw new NotImplementedException();           
                int count = COM.BytesToRead;
                //Debug.Print(Convert.ToString(count));
                byte[] r_bytes = new byte[count];
                COM.Read(r_bytes, 0, count);
                if (count == 1)
                {
                    Debug.Print("--1b");

                    Debug.Print(Convert.ToString(r_bytes[0]));

                    if (r_bytes[0] == ACK)
                    {

                        GetStatus();
                        //IsError = false;
                    }
                }
                else if (count == 6)
                {
                    //1-Ack
                    //2-Stx
                    //3,4 -need
                    byte b1 = r_bytes[2];
                    byte b2 = r_bytes[3];
                    ReadDeviceState(b1, b2);

                    SendAck();
                    IsError = false;
                    Waiting.Stop();
                    /*
                    switch (CommandName)
                    {
                        case "clear":
                            SendAck();
                            break;
                        case "statusrequest":
                            SendAck();
                            break;
                        case "stacker1out":
                        default:
                            break;
                    }
                    */
                }
            }
            catch (Exception exx) { }
        }

        void ReadDeviceState(byte ans1,byte ans2)
        {
            CurrentDeviceState.b10 = GetBit(ans1, 0);
            CurrentDeviceState.b11 = GetBit(ans1, 1);
            CurrentDeviceState.b12 = GetBit(ans1, 2);
            CurrentDeviceState.b14 = GetBit(ans1, 4);
            CurrentDeviceState.b15 = GetBit(ans1, 5);
            CurrentDeviceState.b16 = GetBit(ans1, 6);
            CurrentDeviceState.b20 = GetBit(ans2, 0);
            CurrentDeviceState.b21 = GetBit(ans2, 1);
            CurrentDeviceState.b22 = GetBit(ans2, 2);
            CurrentDeviceState.b23 = GetBit(ans2, 3);
            CurrentDeviceState.b24 = GetBit(ans2, 4);
            CurrentDeviceState.b25 = GetBit(ans2, 5);
            CurrentDeviceState.b26 = GetBit(ans2, 6);
            DisplayStatus();
        }

        void SendAck()
        {
            COM.Write(new byte[] { ACK }, 0, 1);
        }

        void SendNak()
        {
            COM.Write(new byte[] { NAK }, 0, 1);
        }

        public void SendENQ()
        {
            COM.Write(new byte[] { ENQ }, 0, 1);
        }

        #region Commands

        public void Clear()
        {
            cmd(0x30);
            lastbyte = 0x30;
            CommandName = "clear";
        }

        public void GetStatus()
        {
            cmd(0x31);
            lastbyte = 0x31;
            CommandName = "statusrequest";
        }

        public void Stacker1Out()
        {
            cmd(0x40);
            lastbyte = 0x40;
            CommandName = "stacker1out";
        }

        public void Stacker2Out()
        {
            cmd(0x41);
            lastbyte = 0x41;
            CommandName = "stacker2out";
        }

        public void AutoIssue()
        {
            cmd(0x42);
            lastbyte = 0x42;
            CommandName = "autoissue";
        }

        public void Capture()
        {
            cmd(0x43);
            lastbyte = 0x43;
            CommandName = "capture";
        }

        public void FeedIn()
        {
            cmd(0x44);
            lastbyte = 0x44;
            CommandName = "feedin";
        }

        public void FeedOut()
        {
            cmd(0x45);
            lastbyte = 0x45;
            CommandName = "feedout";
        }

        public void Stop()
        {
            cmd(0x46);
            lastbyte = 0x46;
            CommandName = "stop";
        }

        public void Stack1Wait()
        {
            cmd(0x47);
            lastbyte = 0x47;
            CommandName = "stack1wait";
        }

        public void Stack2Wait()
        {
            cmd(0x48);
            lastbyte = 0x48;
            CommandName = "stack2wait";
        }

        public void AutoWait()
        {
            cmd(0x49);
            lastbyte = 0x49;
            CommandName = "autowait";
        }

        public void FeedOutSol()
        {
            cmd(0x4a);
            lastbyte = 0x4a;
            CommandName = "feedoutsol";
        }

        public void Set9600()
        {
            cmd(0x50);
            lastbyte = 0x50;
            CommandName = "set9600";
        }

        public void Set19200()
        {
            cmd(0x51);
            lastbyte = 0x51;
            CommandName = "set19200";
        }


        public void GetVersion()
        {
            cmd(0x60);
            lastbyte = 0x60;
            CommandName = "version";
        }

        #endregion
    }
}
