﻿////
//
//  каждая тетрада в comDiskret (команда в дискрет) - это 4 выхода на один порт (одна плата на разъеме Slave)
//  каждая тетрада в statDiskret (статус дискрета) - это 4 входа на той же плате (порту)
//  0 на выходе - контакты реле разомкнуты, 1 - замкнуты
//  0 на входе - напряжения на входе нет, 1 - есть
//
////
//
//
//
//
//
////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Globalization;


namespace Communications
{
    public class Slave
    {
        private int comDiskret;             // команда в дискрет шлаг.вниз=1, шлаг.вверх=2, светофор GRIN=4. светофор RED=8
        //private string slavePort1 = " ";          // порт 1 дискрет
        //private string slavePort2 = " ";          // порт 2 дискрет
        //private string slavePort3 = " ";          // порт 3 дискрет
        //private string slavePort4 = " ";          // порт 4 дискрет
// дискретный порт из ряда (соответствует мануалу на микросхему и обозначению на плате контроллера):
//      - пробел для неиспользуемых портов   
//A
//B
//C
//D     - не использовать под дискрет, там СОМ для связи с РС
//E
//F
//H
//J
//K

        private int txSize = 0;
        private int rxSize = 0;
        private int Length = 0;
        private byte[] tx = new byte[256];              // передача в ридер 
        private byte[] rx = new byte[256];              // прием из ридер 
        
        private string status;                          // статус Slave

        // реальный статус дискретных устройств пишем в statDiskret  
        private int statDiskret;            // статус loopA=1, ИК=2, loopB=4, кнопка "НАЖМИТЕ"=8,  
        // кнопка "ВЫЗОВ"=16, датчик верхней двери=32,  датчик нижней двери=64, ...=128

        SerialPort _serialPort;

        public int StatDiskret { get { return statDiskret; } }      
        public string Status { get { return status; } }             // статус Slave контроллера

        public int ComDiskret { set { comDiskret = value; } }       //команда в дискрет шлаг.вниз=1, шлаг.вверх=2, светофор GRIN=4. светофор RED=8
        //public string SlavePort1 { get { return slavePort1; } set { slavePort1 = value; } }       // порт 1 дискрет
        //public string SlavePort2 { get { return slavePort2; } set { slavePort2 = value; } }       // порт 2 дискрет
        //public string SlavePort3 { get { return slavePort3; } set { slavePort3 = value; } }       // порт 3 дискрет
        //public string SlavePort4 { get { return slavePort4; } set { slavePort4 = value; } }       // порт 4 дискрет
        public string SlavePort1 { get; set; }
        public string SlavePort2 { get; set; }
        public string SlavePort3 { get; set; }
        public string SlavePort4 { get; set; }

        private string signatureSlave;                          // SignatureSlave
        public string SignatureSlave { get { return signatureSlave; } }           // строка SignatureSlave контроллера  

        private string versionSlave = "";                          // версии ПО Slave
        public string VersionSlave { get { return versionSlave; } }            // строка версии ПО Slave контроллера 

        public Slave()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 115200;
            _serialPort.Parity = Parity.None;
            _serialPort.WriteTimeout = 200;
            _serialPort.ReadTimeout = 200;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
        }

        public bool Open(string Port)
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.PortName = Port;
                    _serialPort.Open();
                    //GetStatus();
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    Console.WriteLine("Возникло исключение: " + e.ToString());
                    return false;
                }
            }
            else
            {
                //GetStatus();
                return true;
            }
        }

        public bool Diskret()                      // команда в Slave выдачи дискретных сигналов на выход и запрос состояния дискретных входов
        {
                tx[1] = 0x00; tx[2] = 0x0A;
                tx[3] = 0x31; tx[4] = 0x30;
                // формирование команды
                //tx[5] = Convert.ToByte(slavePort1);
                //tx[6] = Convert.ToByte(slavePort2);
                //tx[7] = Convert.ToByte(slavePort3);
                //tx[8] = Convert.ToByte(slavePort4);
                byte[] 
                bytes = Encoding.ASCII.GetBytes(SlavePort1);
                tx[5] = bytes[0];
                bytes = Encoding.ASCII.GetBytes(SlavePort2);
                tx[6] = bytes[0];
                bytes = Encoding.ASCII.GetBytes(SlavePort3);
                tx[7] = bytes[0];
                bytes = Encoding.ASCII.GetBytes(SlavePort4);
                tx[8] = bytes[0];

                for (int i = 0; i < 4; i++)
                {
                    int a = (comDiskret & (15 << (i * 4))) >> (i * 4);  // выбор тетрады
                    tx[i + 9] = (byte) a;
                }

                if (BccTx()) return true;
                else return false;
        }

        public bool SignatureSlaveRead()                      // команда в Slave чтения SignatureSlave
        {
            tx[1] = 0x00; tx[2] = 0x02;
            tx[3] = 0x32; tx[4] = 0x30;
            if (BccTx()) return true;
            else return false;
        }

        public bool VersionSlaveRead()                      // команда в Slave чтения версии ПО
        {
            tx[1] = 0x00; tx[2] = 0x02;
            tx[3] = 0x37; tx[4] = 0x30;
            if (BccTx()) return true;
            else return false;
        }

        public bool Wait1()                        // ожидание ответа
        {
            try
            {
                if (_serialPort.BytesToRead > 0)
                {
                    int _rxSize = _serialPort.BytesToRead;
                    _serialPort.Read(
                        rx,
                        rxSize,
                        _serialPort.BytesToRead
                        );
                    rxSize += _rxSize;
                    if(rxSize >= 3 & rx[0] == 3 & rx[1] == 3 & rx[2] ==3)
                    {
                        status = "BCC in Slave er";
                    }
                    else
                    {
                        if (rxSize > 7 && rxSize >= (rx[1] << 8) + rx[2] + 5) // "ответ"
                        {
                            if (CheckBCC())
                            {
                                switch (rx[3])
                                {
                                    case 0x31:  //Diskret
                                        if (rx[4] == 0x30) status = "Diskret ok";
                                        //if (rx[5] == 0x4E) status = "нет карты";
                                        else status = "Diskret er"; // + rx[4].ToString("X2");

                                        statDiskret = 0;
                                        int a;
                                        for (int i = 0; i < 4; i++)
                                        {
                                            //if (rx[i + 5] != 0x39)          // в поле адреса - ошибка (плата не подключена, например)
                                            //{
                                            a = rx[i + 9] << (i * 4);
                                            //}
                                            //else a = 0;
                                            statDiskret += a;
                                        }
                                        //slavePort1 = Convert.ToString(tx[5]);
                                        //slavePort2 = Convert.ToString(tx[6]);
                                        //slavePort3 = Convert.ToString(tx[7]);
                                        //slavePort4 = Convert.ToString(tx[8]);
                                        SlavePort1 = Convert.ToString(tx[5]);
                                        SlavePort2 = Convert.ToString(tx[6]);
                                        SlavePort3 = Convert.ToString(tx[7]);
                                        SlavePort4 = Convert.ToString(tx[8]);
                                        break;

                                    case 0x32:  //чтения SignatureSlave
                                        signatureSlave = "";
                                        if (rx[4] == 0x30)
                                        {
                                            status = "SignatureSlave ok";
                                            for(int i = 0; i < 11; i++)
                                            {
                                                signatureSlave += rx[5 + i].ToString("X2");
                                                //signatureSlave += String.Format("{0:X2} ", rx[5 + i]);
                                            }
                                        }
                                        else status = "SignatureSlave er"; // + rx[4].ToString("X2");

                                       break;

                                    case 0x37:  //чтения версии ПО Slave
                                        versionSlave = "";
                                        if (rx[4] == 0x30)
                                        {
                                            status = "VersionSlave ok";
                                            for (int i = 0; i < 11; i++)
                                            {
                                                versionSlave += (char) rx[5 + i];
                                            }
                                        }
                                        else status = "VersionSlave er"; // + rx[4].ToString("X2");

                                        break;

                                    default:
                                        status = "неопределен";
                                        break;
                                }

                            }
                            else status = "BCC er";
                        }
                        else
                        {
                            status = "ожидание";
                        }
                    }
                    
                }
                else
                {
                    status = "ожидание";
                }
                return true;
            }
            catch (Exception e)
            {
                status = "не отвечает";
                Console.WriteLine("Exception1: " + e.Message);
                return false;
            }
        }

        public bool WaitDiskret()
        {
            if (Wait1())
            {

                return true;
            }
            else return false;
        }

        private bool BccTx()
        {
            txSize = (tx[1] << 8) + tx[2] + 5;
            tx[0] = 0x02;
            tx[txSize - 2] = 0x03;
            tx[txSize - 1] = 0;
            for (int i = 0; i < txSize - 1; i++)
            {
                tx[txSize - 1] = (byte)(tx[txSize - 1] ^ tx[i]);
            }

            try
            {
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
                _serialPort.Write(tx, 0, txSize);
                rxSize = 0;
                status = "ожидание";
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception2: " + e.Message);
                return false;
            }
        }

        private bool CheckBCC()
        {
            byte BCC = 0;
            Length = (rx[1] << 8) + rx[2];
            for (int i = 0; i < Length + 4; i++)
            {
                BCC = (byte)(BCC ^ rx[i]);
            }
            if (BCC == rx[Length + 4]) return true;

            else return false;
        }

        public bool Close()
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Close();
                    //GetStatus();
                    return true;
                }
                catch (Exception e)
                {
                    status = "не отвечает";
                    Console.WriteLine("Возникло исключение: " + e.ToString());
                    return false;
                }
            }
            else
            {
                //GetStatus();
                return true;
            }
        }

    }

    /*// пример работы с SignatureSlave
                        //private string SignatureSlave;          // SignatureSlave текстовый 
                        //private DateTime LastSignatureTime = new DateTime(2000, 1, 1, 0, 0, 0, 0);     //  время последнего чтения SignatureSlave
                        //private int SignatureTime = 2;          // период опроса SignatureSlave в секундах
     
    // выбор команды                   
    DateTime dt = DateTime.Now;
                        tt = dt - LastSignatureTime;
                        int ttt = (int)tt.TotalSeconds;
                        if (ttt > SignatureTime)
                        {
                            // готовим для чтения SignatureSlave
                            if (Slave.SignatureSlaveRead())
                            {
                                LastSignatureTime = dt;
                            }
                            else
                            {
                                labelStatSlave.Text = "не отвечает";
                                labelComSlave.Text = nameSlave + "  не отвечает #2" + statSlave;
                                statSlave = "не отвечает";
                            }
                        }
                        else
                        {
                        if (Slave.Diskret())
                            {
                            }
                        else
                            {
                                labelStatSlave.Text = "не отвечает";
                                labelComSlave.Text = nameSlave + "  не отвечает #2" + statSlave;
                                statSlave = "не отвечает";
                            }
                        }

    //ожидание ответа
    if (statSlave == "SignatureSlave ok")
                            {
                                SignatureSlave = Slave.SignatureSlave;
                                // СОХРАНИТЬ В БАЗЕ

                                labelSignatureSlave.Text = "SignatureSlave - " + SignatureSlave;
                            }


    отправка команды
                    case 2:
                        // выбор команды

                        //private string SignatureSlave;          // SignatureSlave текстовый 
                        //private DateTime LastSignatureTime = new DateTime(2000, 1, 1, 0, 0, 0, 0);     //  время последнего чтения SignatureSlave
                        //private int SignatureTime = 2....;          // период опроса SignatureSlave в секундах
                        DateTime dt = DateTime.Now;
                        tt = dt - LastSignatureTime;
                        int ttt = (int)tt.TotalSeconds;
                        if (ttt > SignatureTime)
                        {
                            // готовим для чтения SignatureSlave
                            if (Slave.SignatureSlaveRead())
                            {
                                etapSlave = 3;              //ожидание ответа
                                                            
                                //LastSignatureTime = dt;
                            }
                            else
                            {
                                etapSlave = 5;
                                labelStatSlave.Text = "не отвечает";
                                labelComSlave.Text = nameSlave + "  не отвечает #22" + statSlave;
                                statSlave = "не отвечает";
                            }
                        }

                        else if(versionSlave == "")
                        {
                            if (Slave.VersionSlaveRead())
                            {
                                etapSlave = 3;              //ожидание ответа
                                timer_diskret = time_diskret;
                                versionSlave = " ";
                                
                            }
                            else
                            {
                                etapSlave = 5;
                                labelStatSlave.Text = "не отвечает";
                                labelComSlave.Text = nameSlave + "  не отвечает #27" + statSlave;
                                statSlave = "не отвечает";
                            }
                        }
                        else
                        {
                            // готовим для передачи дискрета
                            Slave.SlavePort1 = comboBoxPortDiscret1.Text;
                            Slave.SlavePort2 = comboBoxPortDiscret2.Text;
                            Slave.SlavePort3 = " ";
                            Slave.SlavePort4 = " ";
                            Slave.ComDiskret = comDiskret;
                            try
                            {
                                if (Slave.Diskret())
                                {
                                    etapSlave = 3;              //ожидание ответа
                                                                //timer_diskret = time_diskret;
                                }
                                else
                                {
                                    etapSlave = 5;
                                    labelStatSlave.Text = "не отвечает";
                                    labelComSlave.Text = nameSlave + "  не отвечает #21" + statSlave;
                                    statSlave = "не отвечает";
                                }
                            }
                            catch(Exception ee)
                            {
                                ToLog("Exception Slave.Diskret " + ee);
                            }
                        }

                        break;
                    #endregion
                    case 3:         //ожидание ответа
                        if (Slave.Wait1())
                        {

                            statSlave = Slave.Status;
                            if (statSlave == "Diskret ok")
                            {
                                int _StatDiskret = Slave.StatDiskret;
                                // проверка инверсии
                                if (!checkBoxLoopA.Checked) _StatDiskret = _StatDiskret ^ 1;
                                if (!checkBoxLoopB.Checked) _StatDiskret = _StatDiskret ^ 4;
                                if (checkBoxIR.Checked) _StatDiskret = _StatDiskret ^ 2;
                                if (!checkBoxPUSH.Checked) _StatDiskret = _StatDiskret ^ 8;
                                if (InvertCall == 1) _StatDiskret = _StatDiskret ^ 16;

                                statDiskret = _StatDiskret;
                                statDiskretN = statDiskret;
                                labelStatSlave.Text = "Diskret ok";
                                labelComSlave.Text = nameSlave + " #31" + statSlave;
                                // готовим для передачи дискрета
                                //Slave.SlavePort1 = comboBoxPortDiscret1.Text;
                                //Slave.SlavePort2 = comboBoxPortDiscret2.Text;
                                //Slave.SlavePort3 = " ";
                                //Slave.SlavePort4 = " ";
                                //Slave.ComDiskret = comDiskret;
                                //if (Slave.Diskret())
                                //{
                                etapSlave = 2;
                                timer_diskret = time_diskret;
                                timer_slave = time_slave;           // таймаут для statSlave = "не отвечает";
                                //}
                                //else
                                //{
                                //    etapSlave = 3;
                                //    labelStatSlave.Text = "не отвечает";
                                //    labelComSlave.Text = nameSlave + "  не отвечает #2" + statSlave;
                                //}
                            }

                            if (statSlave == "ожидание" & timer_diskret == 0)
                            {
                                // готовим для передачи дискрета
                                //Slave.SlavePort1 = comboBoxPortDiscret1.Text;
                                //Slave.SlavePort2 = comboBoxPortDiscret2.Text;
                                //Slave.SlavePort3 = " ";
                                //Slave.SlavePort4 = " ";
                                //Slave.ComDiskret = comDiskret;
                                //if (Slave.Diskret())
                                //{
                                //    etapSlave = 2;
                                //    timer_diskret = time_diskret;
                                //}
                                //else
                                //{
                                //    etapSlave = 3;
                                //    labelStatSlave.Text = "не отвечает";
                                //    labelComSlave.Text = nameSlave + "  не отвечает #2" + statSlave;
                                //}
                                labelStatSlave.Text = "не отвечает";
                                labelComSlave.Text = nameSlave + "  не отвечает #30" + statSlave;
                                if (timer_slave == 0) statSlave = "не отвечает";
                                etapSlave = 5;
                            }

                            if ((statSlave == "BCC er" | statSlave == "BCC in Slave er") & timer_diskret != 0)
                            {
                                // готовим для передачи дискрета
                                //Slave.SlavePort1 = comboBoxPortDiscret1.Text;
                                //Slave.SlavePort2 = comboBoxPortDiscret2.Text;
                                //Slave.SlavePort3 = " ";
                                //Slave.SlavePort4 = " ";
                                //Slave.ComDiskret = comDiskret;
                                //if (Slave.Diskret())
                                //{
                                etapSlave = 2;
                                //    //timer_diskret = time_diskret;
                                //}
                                //else
                                //{
                                //    etapSlave = 3;
                                //    labelStatSlave.Text = "BCC er";
                                //    labelComSlave.Text = nameSlave + "  BCC er #2" + statSlave;
                                //}
                                labelStatSlave.Text = "BCC er";
                                labelComSlave.Text = nameSlave + "  BCC er" + statSlave;
                            }

                            if (statSlave == "ожидание" & timer_diskret != 0)
                            {
                                timer_diskret--;
                                statSlave = statSlave2;
                            }

                            if (statSlave == "SignatureSlave ok")
                            {
                                SignatureSlave = Slave.SignatureSlave;
                                // СОХРАНИТЬ В БАЗЕ
                                if (DisposNewEntities())
                                {
                                    DeviceModel = db.DeviceModel.FirstOrDefault();
                                    DeviceModel.SlaveCode = SignatureSlave;
                                    SaveChanges("");

                                    LastSignatureTime = DateTime.Now;

                                    labelSignatureSlave.Text = "SignatureSlave - " + SignatureSlave;
                                    etapSlave = 2;
                                    timer_diskret = time_diskret;
                                    timer_slave = time_slave;
                                }
                            }

                            if (statSlave == "VersionSlave ok")
                            {
                                versionSlave = Slave.VersionSlave;
                                // СОХРАНИТЬ В БАЗЕ
                                
                                labelVersionSlave.Text = "VersionSlave - " + versionSlave;
                                etapSlave = 2;
                                timer_diskret = time_diskret;
                                timer_slave = time_slave;
                            }

                            if (statSlave == " ")
                            { }
                            }
                        else
                        {
                            etapSlave = 5;
                            labelStatSlave.Text = "не отвечает";
                            labelComSlave.Text = nameSlave + "  не отвечает #3" + statSlave;
                            statSlave = "не отвечает";
                        }

                        break;
    */
}
