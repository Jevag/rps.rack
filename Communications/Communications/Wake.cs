﻿using System;

namespace Slave3Testing
{


	public static class Wake
	{
		/*
		 *  Константы протокола Wake
		 */
		const byte FEND = 0xC0;    //Frame END
		const byte FESC = 0xDB;   //Frame ESCape
		const byte TFEND = 0xDC;    //Transposed Frame END
		const byte TFESC = 0xDD;    //Transposed Frame ESCape

		/*
		 * Настройки параметров протокола 
		 */
		static byte CRC_INIT = 0xDE;     //Innitial CRC value
		static byte FRAME = 20;          //максимальная длина пакета
		static byte SYSTEM_BYTE_QTY = 5; // Количество системных байт FEND + ADDR + NBT + CMD + CRC = 5

		/*
		 *  RX process states: 
		 */
		const byte WAIT_FEND = 1;      //ожидание приема FEND
		const byte WAIT_ADDR = 2;      //ожидание приема адреса
		const byte WAIT_CMD = 3;      //ожидание приема команды
		const byte WAIT_NBT = 4;      //ожидание приема количества байт в пакете
		const byte WAIT_DATA = 5;      //прием данных
		const byte WAIT_CRC = 6;      //ожидание окончания приема CRC
		const byte WAIT_CARR = 7;      //ожидание несущей
																	 /*
																		*  TX process states: 
																		*/
		const byte SEND_IDLE = 1;     //состояние бездействия
		const byte SEND_ADDR = 2;     //передача адреса
		const byte SEND_CMD = 3;     //передача команды
		const byte SEND_NBT = 4;     //передача количества байт в пакете
		const byte SEND_DATA = 5;     //передача данных
		const byte SEND_CRC = 6;     //передача CRC
		const byte SEND_END = 7;     //окончание передачи пакета
																 /*
																	*  Коды универсальных команд:
																	*/
		public const byte CMD_NOP = 0;    //нет операции
		public const byte CMD_ERR = 1;    //ошибка приема пакета
		public const byte CMD_ECHO = 2;    //передать эхо
		public const byte CMD_INFO = 3;    //передать информацию об устройстве

		/*
		 * Коды ошибок
		 */
		public const byte ERR_NO = 0x00;   //no error
		public const byte ERR_TX = 0x01;   //Rx/Tx error
		public const byte ERR_BU = 0x02;   //device busy error
		public const byte ERR_RE = 0x03;   //device not ready error
		public const byte ERR_PA = 0x04;   //parameters value error
		public const byte ERR_NR = 0x05;   //no replay
		public const byte ERR_NC = 0x06;   //no carrier

		//----------------------------- Адреса приема и передачи: ------------------------

		static byte PC_RX_ADDR = 1;   //адрес при приеме данных от PC
		static byte PC_TX_ADDR = 0;   //адрес, передаваемый в PC

		//------------------------------- Переменные: --------------------------------

		static byte Pc_Rx_Sta;        //состояние процесса приема пакета
		static byte Pc_Rx_Pre;        //предыдущий принятый байт
		static byte Pc_Rx_Add;        //адрес, с которым сравнивается принятый
		static byte Pc_Rx_Cmd;        //принятая команда
		static byte Pc_Rx_Nbt;        //принятое количество байт в пакете
		public static byte[] Pc_Rx_Dat = new byte[FRAME]; //массив принятых данных
		static byte Pc_Rx_Crc;        //контрольная сумма принимаемого пакета
		static byte Pc_Rx_Ptr;        //указатель на массив принимаемых данных
		static byte Rx_Data_Byte;   // Принятый байт данных
		public static byte[] Temp_Pc_Rx_Dat = new byte[FRAME + SYSTEM_BYTE_QTY]; //Врменный буфер для принятых по DMA данных

		public static byte Command;          //код команды на выполнение

		static byte Pc_Tx_Sta;        //состояние процесса передачи пакета
		static byte Pc_Tx_Pre;        //предыдущий переданный байт
		static byte Pc_Tx_Add;        //адрес, передававемый в пакете
		static byte Pc_Tx_Cmd;        //команда, передаваемая в пакете
		static byte Pc_Tx_Nbt;        //количество байт данных в пакете
		public static byte[] Pc_Tx_Dat = new byte[FRAME]; //массив данных для передачи
		static byte Pc_Tx_Crc;        //контрольная сумма передаваемого пакета
		static byte Pc_Tx_Ptr;        //указатель на массив передаваемых данных
		public static byte[] Temp_Pc_Tx_Dat = new byte[FRAME + SYSTEM_BYTE_QTY]; //Врменный буфер для принятых по DMA данных
		static byte Temp_Dat_Index;   // Текущий индекс временного массива Temp_Pc_Tx_Dat
		static byte Tx_Data_Byte;

		//------------------------ Инициализация UART: -------------------------------

		public static void Pc_Init_Port()
		{
			Pc_Rx_Add = PC_RX_ADDR; //адрес на прием
			Pc_Tx_Add = PC_TX_ADDR; //адрес на передачу
			Pc_Rx_Sta = WAIT_FEND;  //ожидание пакета
			Pc_Tx_Sta = SEND_IDLE;  //ничего пока не передаем
			Command = CMD_NOP;      //нет команды на выполнение
		}

		//--------------------- Вычисление контрольной суммы -------------------------
		public static byte Do_Crc8(byte b, byte crc)
		{
			for (byte i = 0; i < 8; b = (byte)(b >> 1), i++)
			{
				if (((b ^ crc) & 1) == 1)
					crc = (byte)(((crc ^ 0x18) >> 1) | 0x80);
				else
					crc = (byte)((crc >> 1) & ~0x80);
			}
			return crc;
		}

		//------------------- Прерывание UART после приема байта: --------------------
		public static void PC_Rx()
		{
			byte error_flags = 0;

			if (error_flags == 1)                     //если обнаружены ошибки при приеме байта
			{
				Pc_Rx_Sta = WAIT_FEND;            //ожидание нового пакета
				Command = CMD_ERR;                //рапортуем об ошибке
				return;
			}

			if (Rx_Data_Byte == FEND)               //если обнаружено начало фрейма,
			{
				Pc_Rx_Pre = Rx_Data_Byte;            //то сохранение пре-байта,
				Pc_Rx_Crc = CRC_INIT;             //инициализация CRC,
				Pc_Rx_Sta = WAIT_ADDR;            //сброс указателя данных,
				Pc_Rx_Crc = Do_Crc8(Rx_Data_Byte, Pc_Rx_Crc);   //обновление CRC,
				return;                           //выход
			}

			if (Pc_Rx_Sta == WAIT_FEND)          //-----> если ожидание FEND,
				return;                           //то выход

			byte Pre = Pc_Rx_Pre;               //сохранение старого пре-байта
			Pc_Rx_Pre = Rx_Data_Byte;              //обновление пре-байта
			if (Pre == FESC)                     //если пре-байт равен FESC,
			{
				if (Rx_Data_Byte == TFESC)            //а байт данных равен TFESC,
					Rx_Data_Byte = FESC;               //то заменить его на FESC
				else if (Rx_Data_Byte == TFEND)       //если байт данных равен TFEND,
					Rx_Data_Byte = FEND;          //то заменить его на FEND
				else
				{
					Pc_Rx_Sta = WAIT_FEND;     //для всех других значений байта данных,
					Command = CMD_ERR;         //следующего за FESC, ошибка
					return;
				}
			}
			else
			{
				if (Rx_Data_Byte == FESC)             //если байт данных равен FESC, он просто
					return;                         //запоминается в пре-байте
			}

			switch (Pc_Rx_Sta)
			{
				case WAIT_ADDR:                     //-----> ожидание приема адреса
					{
						if ((Rx_Data_Byte & 0x80) != 0)            //если бит 7 данных не равен нулю, то это адрес
						{
							Rx_Data_Byte = (byte)(Rx_Data_Byte & 0x7F); //обнуляем бит 7, получаем истинный адрес
							if (Rx_Data_Byte == 0 || Rx_Data_Byte == Pc_Rx_Add) //если нулевой или верный адрес,
							{
								Pc_Rx_Crc = Do_Crc8(Rx_Data_Byte, Pc_Rx_Crc); //то обновление CRC и
								Pc_Rx_Sta = WAIT_CMD;       //переходим к приему команды
								break;
							}
							Pc_Rx_Sta = WAIT_FEND;        //адрес не совпал, ожидание нового пакета
							break;
						}                               //если бит 7 данных равен нулю, то
						Pc_Rx_Sta = WAIT_CMD;           //сразу переходим к приему команды
						goto case WAIT_CMD;
					}

				case WAIT_CMD:                      //-----> ожидание приема команды
					{
						if ((Rx_Data_Byte & 0x80) == 1)            //проверка бита 7 данных
						{
							Pc_Rx_Sta = WAIT_FEND;        //если бит 7 не равен нулю,
							Command = CMD_ERR;            //то ошибка
							break;
						}
						Pc_Rx_Cmd = Rx_Data_Byte;          //сохранение команды
						Pc_Rx_Crc = Do_Crc8(Rx_Data_Byte, Pc_Rx_Crc); //обновление CRC
						Pc_Rx_Sta = WAIT_NBT;           //переходим к приему количества байт
						break;
					}
				case WAIT_NBT:                      //-----> ожидание приема количества байт
					{
						if (Rx_Data_Byte > FRAME)           //если количество байт > FRAME,
						{
							Pc_Rx_Sta = WAIT_FEND;
							Command = CMD_ERR;            //то ошибка
							break;
						}
						Pc_Rx_Nbt = Rx_Data_Byte;
						Pc_Rx_Crc = Do_Crc8(Rx_Data_Byte, Pc_Rx_Crc); //обновление CRC
						Pc_Rx_Ptr = 0;                  //обнуляем указатель данных
						Pc_Rx_Sta = WAIT_DATA;          //переходим к приему данных
						break;
					}
				case WAIT_DATA:                     //-----> ожидание приема данных
					{
						if (Pc_Rx_Ptr < Pc_Rx_Nbt)       //если не все данные приняты,
						{
							Pc_Rx_Dat[Pc_Rx_Ptr++] = Rx_Data_Byte; //то сохранение байта данных,
							Pc_Rx_Crc = Do_Crc8(Rx_Data_Byte, Pc_Rx_Crc);  //обновление CRC
							break;
						}
						if (Rx_Data_Byte != Pc_Rx_Crc)      //если приняты все данные, то проверка CRC
						{
							Pc_Rx_Sta = WAIT_FEND;        //если CRC не совпадает,
							Command = CMD_ERR;            //то ошибка
							break;
						}
						Pc_Rx_Sta = WAIT_FEND;          //прием пакета завершен,
						Command = Pc_Rx_Cmd;            //загрузка команды на выполнение
						if (RX_Complete != null)
						{
							RX_Complete();
						}
						break;
					}
			}
		}

		//------------------ Прерывание UART после передачи байта: -------------------
		public static void Tx_PC()
		{
			if (Pc_Tx_Pre == FEND)                 //если производится стаффинг,
			{
				Tx_Data_Byte = TFEND;                //передача TFEND вместо FEND
				Pc_Tx_Pre = Tx_Data_Byte;
				Temp_Pc_Tx_Dat[Temp_Dat_Index++] = Tx_Data_Byte;
				return;
			}
			if (Pc_Tx_Pre == FESC)                 //если производится стаффинг,
			{
				Tx_Data_Byte = TFESC;                //передача TFESC вместо FESC
				Pc_Tx_Pre = Tx_Data_Byte;
				Temp_Pc_Tx_Dat[Temp_Dat_Index++] = Tx_Data_Byte;
				return;
			}

			switch (Pc_Tx_Sta)
			{
				case SEND_ADDR:                               //-----> передача адреса
					{
						if (Pc_Tx_Add != 0)                        //если адрес не равен нулю,
						{
							Tx_Data_Byte = (byte)(Pc_Tx_Add | 0x80); //то он передается (бит 7 равен единице)
							Pc_Tx_Sta = SEND_CMD;
							break;
						}
						else
							Pc_Tx_Sta = SEND_CMD;                    //иначе сразу передаем команду
						goto case SEND_CMD;
					}
				case SEND_CMD:                                 //-----> передача команды
					{
						Tx_Data_Byte = (byte)(Pc_Tx_Cmd & 0x7F);
						Pc_Tx_Sta = SEND_NBT;
						break;
					}
				case SEND_NBT:                                 //-----> передача количества байт
					{
						Tx_Data_Byte = Pc_Tx_Nbt;
						Pc_Tx_Sta = SEND_DATA;
						Pc_Tx_Ptr = 0;                            //обнуление указателя данных для передачи
						break;
					}
				case SEND_DATA:                               //-----> передача данных
					{
						if (Pc_Tx_Ptr < Pc_Tx_Nbt)
							Tx_Data_Byte = Pc_Tx_Dat[Pc_Tx_Ptr++];
						else
						{
							Tx_Data_Byte = Pc_Tx_Crc;                //передача CRC
							Pc_Tx_Sta = SEND_CRC;
						}
						break;
					}
				default:
					{
						TX_Wpacket();
						Pc_Tx_Sta = SEND_IDLE;          //передача пакета завершена
						return;
					}
			}

			if (Pc_Tx_Sta == SEND_CMD)
			{
				Pc_Tx_Crc = Do_Crc8(Pc_Tx_Add, Pc_Tx_Crc);
			}
			else
			{
				Pc_Tx_Crc = Do_Crc8(Tx_Data_Byte, Pc_Tx_Crc);     //обновление CRC
			}
			Pc_Tx_Pre = Tx_Data_Byte;              //сохранение пре-байта
			if (Tx_Data_Byte == FEND || Tx_Data_Byte == FESC)
			{
				Tx_Data_Byte = FESC;                 //передача FESC, если нужен стаффинг
			}
			Temp_Pc_Tx_Dat[Temp_Dat_Index++] = Tx_Data_Byte;
		}

		//------------------------- Передача пакета ----------------------------------
		public static void Pc_Tx_Frame()
		{
			Tx_Data_Byte = FEND;
			Pc_Tx_Crc = CRC_INIT;                              //инициализация CRC,
			Pc_Tx_Crc = Do_Crc8(Tx_Data_Byte, Pc_Tx_Crc);      //обновление CRC
			Pc_Tx_Sta = SEND_ADDR;
			Pc_Tx_Pre = TFEND;
			Temp_Pc_Tx_Dat[Temp_Dat_Index++] = Tx_Data_Byte;
			for (int i = 0; i < Pc_Tx_Nbt + SYSTEM_BYTE_QTY; i++)
			{
				Tx_PC();
			}
		}

		// -----------------------ОБертка для передачи бувера чере протокол-----------
		public static void SendWpacket(byte address, byte CMD, byte Nbt, byte[] customData)
		{
			{
				Temp_Dat_Index = 0;
				Pc_Tx_Add = address;
				Pc_Tx_Cmd = CMD;
				Pc_Tx_Nbt = Nbt;
				for (int i = 0; i < Pc_Tx_Nbt; i++)
				{
					Pc_Tx_Dat[i] = customData[i];
				}
				Pc_Tx_Frame();
			}
		}

		// ------------------Обетка для приема сообщения через протокол----------------
		public static void ReciveWpacket()
		{
			for (int i = 0; i < Temp_Pc_Rx_Dat.Length; i++)
			{
				Rx_Data_Byte = Temp_Pc_Rx_Dat[i];
				PC_Rx();
			}
		}

		//------------------------------------------------------------------------------
		public static event Action RX_Complete;
		public static event Action TX_Wpacket;
	}
}
