﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RPS.ServerLicencee.Share
{
    public class AesCryptoData
    {
        public void GenerateKeys()
        {
            AesCryptoServiceProvider crypto = new AesCryptoServiceProvider();
            crypto.GenerateIV();
            crypto.GenerateKey();
            string iv = GeenrateByteToString("iv", crypto.IV);
            string key = GeenrateByteToString("iv", crypto.Key);
        }
        public string GeenrateByteToString(string nameper, byte[] data)
        {
            StringBuilder str = new StringBuilder();
            str.Append("byte[] " + nameper + "=new byte[] {");
            bool isfirst = true;
            foreach (byte bt in data)
            {
                if (isfirst) { str.Append(bt.ToString()); isfirst = false; }
                else { str.Append(","); str.Append(bt.ToString()); }
            }
            str.Append("};");
            return str.ToString();
        }

        private static byte[] iv = new byte[] { 83, 32, 249, 116, 165, 75, 78, 122, 209, 117, 112, 227, 16, 131, 17, 138 };
        private static byte[] key = new byte[] { 54, 214, 126, 48, 126, 201, 116, 159, 32, 236, 38, 90, 168, 80, 39, 170, 31, 197, 182, 31, 41, 62, 140, 151, 164, 11, 125, 155, 103, 193, 239, 104 };


        private static AesCryptoServiceProvider service = null;

        protected static AesCryptoServiceProvider Service
        {
            get
            {
                if (service == null)
                {
                    service = new AesCryptoServiceProvider();
                    service.IV = iv;
                    service.Key = key;
                }
                return service;
            }
        }

        public static byte[] EncyptString(string data)
        {
            return Encrypt(UnicodeEncoding.Unicode.GetBytes(data));
        }

        public static string DecryptData(byte[] data)
        {
            var bts = Decrypt(data);
            return UnicodeEncoding.Unicode.GetString(bts);
        }

        public static Byte[] Encrypt(Byte[] input)
        {
            return Transform(Service.CreateEncryptor(), input, Service.BlockSize);
        }

        public static Byte[] Decrypt(Byte[] input)
        {
            return Transform(Service.CreateDecryptor(), input, Service.BlockSize);
        }

        private static Byte[] Transform(ICryptoTransform cryptoTransform, Byte[] input, Int32 blockSize)
        {
            if (input.Length > blockSize)
            {
                Byte[] ret1 = new Byte[((input.Length - 1) / blockSize) * blockSize];

                Int32 inputPos = 0;
                Int32 ret1Length = 0;
                for (inputPos = 0; inputPos < input.Length - blockSize; inputPos += blockSize)
                {
                    ret1Length += cryptoTransform.TransformBlock(input, inputPos, blockSize, ret1, ret1Length);
                }

                Byte[] ret2 = cryptoTransform.TransformFinalBlock(input, inputPos, input.Length - inputPos);

                Byte[] ret = new Byte[ret1Length + ret2.Length];
                Array.Copy(ret1, 0, ret, 0, ret1Length);
                Array.Copy(ret2, 0, ret, ret1Length, ret2.Length);
                return ret;
            }
            else
            {
                return cryptoTransform.TransformFinalBlock(input, 0, input.Length);
            }

        }
    }
}
