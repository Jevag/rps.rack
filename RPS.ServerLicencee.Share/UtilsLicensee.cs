﻿using Rps.Crypto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RPS.ServerLicencee.Share
{
    public class UtilsLicensee
    {
        public InfoKeys GetKeys(Guid objectid,SqlConnection connectdbm)
        {
            InfoKeys coninfo = null;
            DataTable tbl = new DataTable();
            string zap = "select top 1 PrivKey,PubKey,ExpireSet from [Object] where Id=@obj";
            using (SqlDataAdapter adap = new SqlDataAdapter(zap, connectdbm))
            {
                adap.SelectCommand.Parameters.AddWithValue("@obj", objectid);
                adap.Fill(tbl);
                if (tbl != null && tbl.Rows != null && tbl.Rows.Count > 0)
                {
                    DataRow row = tbl.Rows[0];
                    try
                    {
                        coninfo = new InfoKeys();
                        var bts = (byte[])row["PrivKey"];
                        coninfo.PrivKey = AesCryptoData.DecryptData(bts);
                        var bts1 = (byte[])row["PubKey"];
                        coninfo.PublicKey = AesCryptoData.DecryptData(bts1);
                        coninfo.ToDate = System.Convert.ToDateTime(row["ExpireSet"]);
                    }
                    catch(Exception ex)
                    {

                    }
                }
            }
            return coninfo;
        }

        public void SaveKeys(Guid objectid, InfoKeys keys,SqlConnection connectdbm)
        {
            string zap = "update [Object] set PrivKey=@prkey,PubKey=@pubkey where Id=@objid";
            using (SqlCommand command = new SqlCommand(zap, connectdbm))
            {
                command.Parameters.Add("@prkey", AesCryptoData.EncyptString(keys.PrivKey));
                command.Parameters.Add("@pubkey", AesCryptoData.EncyptString(keys.PublicKey));
                command.Parameters.Add("@objid", objectid);
                command.ExecuteNonQuery();
            }
        }

        public InfoConnectBase GetConnectInfo(Guid objectid, SqlConnection connectdbm)
        {
            CryptoDecrypto decrypto = new CryptoDecrypto();
            InfoConnectBase coninfo = null;
            DataTable tbl = new DataTable();
            string zap = "select top 1 DB3Host,DB3Name,DB3Login,DB3Password from [Object] where Id=@obj";
            using (SqlDataAdapter adap = new SqlDataAdapter(zap, connectdbm))
            {
                adap.SelectCommand.Parameters.Add("@obj", objectid);
                adap.Fill(tbl);
                if(tbl!=null && tbl.Rows!=null && tbl.Rows.Count>0)
                {
                    DataRow row = tbl.Rows[0];
                    try
                    {
                        coninfo = new InfoConnectBase();
                        coninfo.Db = System.Convert.ToString(row["DB3Name"]);
                        coninfo.Host = System.Convert.ToString(row["DB3Host"]);
                        coninfo.Password = decrypto.Decrypto(System.Convert.ToString(row["DB3Password"]));

                        coninfo.User = System.Convert.ToString(row["DB3Login"]);
                    }
                    catch { }
                }
            }
            return coninfo;
        }

        public void GenerateLicensee(Guid objectid, SqlConnection connectdbm)
        {
            var coninfo = GetConnectInfo(objectid, connectdbm);
            //Test from 2 level
            //coninfo.Host = "mc.r-p-s.ru,14330";
            if (coninfo!=null && coninfo.IsFull())
            {
                string constr = "Data Source=" + coninfo.Host + "; Initial Catalog=" + coninfo.Db + "; User Id=" + coninfo.User + "; Password=" + coninfo.Password;
                var keys = GetKeys(objectid, connectdbm);

                if (keys != null && keys.IsFull())
                {
                    if (keys.ToDate <= DateTime.Today)
                    {
                        keys.ToDate = DateTime.Today.AddDays(15);
                    }
                }
                else
                {
                    if(keys==null)
                    { keys = new InfoKeys(); }
                    RSACryptoServiceProvider rsaGenKeys = new RSACryptoServiceProvider();
                    keys.PrivKey = rsaGenKeys.ToXmlString(true);
                    keys.PublicKey = rsaGenKeys.ToXmlString(false);
                    if (keys.ToDate <= DateTime.Today)
                    {
                        keys.ToDate = DateTime.Today.AddDays(15);
                    }
                    SaveKeys(objectid, keys, connectdbm);
                }

                using (SqlConnection connect = new SqlConnection(constr))
                {
                    connect.Open();
                    string sel = "select SlaveCode from DeviceModel";

                    using (SqlDataAdapter adap = new SqlDataAdapter(sel, connect))
                    {
                        DataTable tbl = new DataTable();
                        adap.Fill(tbl);
                        foreach (DataRow row in tbl.Rows)
                        {
                            object objslave = row["SlaveCode"];
                            if (objslave != null && objslave != System.DBNull.Value)
                            {
                                string code = Convert.ToString(objslave);
                                byte[] data = GenerateLicensee(keys.ToDate, code, keys.PublicKey, keys.PrivKey);
                                SaveLicense(data, code, connect);
                            }
                        }
                    }
                }

                string upddate = "update [Object] set ExpireGet=@dt where Id=@objid";
                using (SqlCommand comm = new SqlCommand(upddate, connectdbm))
                {
                    comm.Parameters.Add("@dt", keys.ToDate);
                    comm.Parameters.Add("@objid", objectid);
                    comm.ExecuteNonQuery();
                }
            }
        }

        public byte[] GenerateLicensee(DateTime todate,string code,string pubkey, string privkey)
        {
            RSACryptoServiceProvider rsaserver = new RSACryptoServiceProvider();
            rsaserver.FromXmlString(privkey);
            SHA1Managed Sha = new SHA1Managed();

            byte[] hashed = Sha.ComputeHash(Encoding.UTF8.GetBytes(code));
            byte[] signature = rsaserver.SignHash(hashed, CryptoConfig.MapNameToOID("SHA1"));

            LicenseeData licen = new LicenseeData();
            licen.PublicKey = pubkey;
            licen.Signature = signature;
            licen.ToDate = todate;

            byte[] result = null;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter format = new BinaryFormatter();
                format.Serialize(ms, licen);
                ms.Position = 0;
                result = ms.GetBuffer();
            }
            return result;
        }

        public  void SaveLicense(byte[] data, string slavecode, SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(slavecode) && !string.IsNullOrWhiteSpace(slavecode))
            {
                string selfind = "select Top 1 SlaveCode from License where SlaveCode='" + slavecode + "'";
                string update = "update License set Data=@dat where SlaveCode='" + slavecode + "'";
                string ins = "insert into License(SlaveCode,Data) values('" + slavecode + "',@dat)";
                bool isfind = false;
                using (SqlCommand comm = new SqlCommand(selfind, connect))
                {
                    var code = comm.ExecuteScalar();
                    if (code != null && code != System.DBNull.Value)
                    {
                        string codestr = code.ToString();
                        if (!string.IsNullOrEmpty(codestr) && !string.IsNullOrWhiteSpace(codestr) && codestr.Equals(slavecode))
                        {
                            isfind = true;
                        }
                    }
                }
                if (isfind)
                {
                    using (SqlCommand comm = new SqlCommand(update, connect))
                    {
                        SqlParameter pr = new SqlParameter("@dat", data);
                        comm.Parameters.Add(pr);
                        comm.ExecuteNonQuery();
                    }
                }
                else
                {
                    using (SqlCommand comm = new SqlCommand(ins, connect))
                    {
                        SqlParameter pr = new SqlParameter("@dat", data);
                        comm.Parameters.Add(pr);
                        comm.ExecuteNonQuery();
                    }
                }
            }
        }
    }

    public class InfoConnectBase
    {
        public string Host { get; set; }
        public string Db { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public bool IsFull()
        {
            bool result = false;

            if(!string.IsNullOrEmpty(Host) && !string.IsNullOrWhiteSpace(Host)
                && !string.IsNullOrEmpty(Db) && !string.IsNullOrWhiteSpace(Db)
                && !string.IsNullOrEmpty(User) && !string.IsNullOrWhiteSpace(User)
                && !string.IsNullOrEmpty(Password) && !string.IsNullOrWhiteSpace(Password))
            {
                result = true;
            }
            return result;
        }

    }

    public class InfoKeys
    {
        public string PrivKey { get; set; }
        public string PublicKey { get; set; }
        public DateTime ToDate { get; set; }

        public bool IsFull()
        {
            bool result = false;

            if (!string.IsNullOrEmpty(PrivKey) && !string.IsNullOrWhiteSpace(PrivKey)
                && !string.IsNullOrEmpty(PublicKey) && !string.IsNullOrWhiteSpace(PublicKey))
            {
                result = true;
            }

            return result;
        }
    }
}
