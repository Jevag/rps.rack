﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RPS.ServerLicencee.Share
{
    [Serializable]
    public class LicenseeData
    {
        public string PublicKey { get; set; }
        public byte[] Signature { get; set; }
        public DateTime ToDate { get; set; }

        public static byte[] Serializable(LicenseeData data)
        {
            byte[] result = null;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter format = new BinaryFormatter();
                format.Serialize(ms, data);
                ms.Position = 0;
                result = ms.GetBuffer();
            }
            return result;
        }

        public static LicenseeData DeSerializable(byte[] data)
        {
            LicenseeData result = null;
            using (MemoryStream ms = new MemoryStream(data))
            {
                ms.Position = 0;
                BinaryFormatter format = new BinaryFormatter();
                result = format.Deserialize(ms) as LicenseeData;
            }
            return result;
        }

        public static LicenseeData GenerateLicensee(DateTime todate,string NumberDevice, string filenamepublic,string filenameprivate)
        {
            string privkey = System.IO.File.ReadAllText(filenameprivate);
            string pubkey = System.IO.File.ReadAllText(filenamepublic);

            RSACryptoServiceProvider rsaserver = new RSACryptoServiceProvider();
            rsaserver.FromXmlString(privkey);
            SHA1Managed Sha = new SHA1Managed();

            byte[] hashed = Sha.ComputeHash(Encoding.UTF8.GetBytes(NumberDevice));
            byte[] signature = rsaserver.SignHash(hashed, CryptoConfig.MapNameToOID("SHA1"));

            LicenseeData licen = new LicenseeData();
            licen.PublicKey = pubkey;
            licen.Signature = signature;
            licen.ToDate = todate;

            return licen;
        }

        public static void GenerateKeys(string fileprivate,string filepublic)
        {
            RSACryptoServiceProvider rsaGenKeys = new RSACryptoServiceProvider();
            string privateXml = rsaGenKeys.ToXmlString(true);
            string publicXml = rsaGenKeys.ToXmlString(false);
            System.IO.File.WriteAllText(filepublic, publicXml);
            System.IO.File.WriteAllText(fileprivate, privateXml);
        }

        public static void GenerateLisenseForBase(DateTime todate,string fileprivate,string filepublic,SqlConnection connect)
        {
            //Выбираем все устрайства 
            string sel = "select SlaveCode from DeviceModel";

            using (SqlDataAdapter adap = new SqlDataAdapter(sel, connect))
            {
                DataTable tbl = new DataTable();
                adap.Fill(tbl);
                foreach(DataRow row in tbl.Rows)
                {
                    object objslave = row["SlaveCode"];
                    if(objslave!=null && objslave!=System.DBNull.Value)
                    {
                        string code = Convert.ToString(objslave);
                        LicenseeData data = GenerateLicensee(todate, code, filepublic, fileprivate);
                        SaveLicense(data, code, connect);
                    }
                }
            }
        }

        public static void SaveLicense(LicenseeData data, string slavecode, SqlConnection connect)
        {
            if (!string.IsNullOrEmpty(slavecode) && !string.IsNullOrWhiteSpace(slavecode))
            {
                string selfind = "select Top 1 SlaveCode from License where SlaveCode='" + slavecode + "'";
                string update = "update License set Data=@dat where SlaveCode='" + slavecode + "'";
                string ins = "insert into License(SlaveCode,Data) values('" + slavecode + "',@dat)";
                bool isfind = false;
                using (SqlCommand comm = new SqlCommand(selfind, connect))
                {
                    var code = comm.ExecuteScalar();
                    if (code != null && code != System.DBNull.Value)
                    {
                        string codestr = code.ToString();
                        if (!string.IsNullOrEmpty(codestr) && !string.IsNullOrWhiteSpace(codestr) && codestr.Equals(slavecode))
                        {
                            isfind = true;
                        }
                    }
                }
                byte[] dat = LicenseeData.Serializable(data);
                if (isfind)
                {
                    using (SqlCommand comm = new SqlCommand(update, connect))
                    {
                        SqlParameter pr = new SqlParameter("@dat", dat);
                        comm.Parameters.Add(pr);
                        comm.ExecuteNonQuery();
                    }
                }
                else
                {
                    using (SqlCommand comm = new SqlCommand(ins, connect))
                    {
                        SqlParameter pr = new SqlParameter("@dat", dat);
                        comm.Parameters.Add(pr);
                        comm.ExecuteNonQuery();
                    }
                }
            }
        }

    }
}
