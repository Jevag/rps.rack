﻿// для работы Калькулятора Тарифных Сеток необходимы
// данные с парковочной карты DateCard
// признак выбора клиентом оплаты абонемента для касс ToAbonement
// зона после для стоек ZoneAfter

using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using rps.Data;
//using System.Linq.Expressions;
using EntityFramework.Extensions;
using EntityFramework.Batch;
using EntityFramework.Future;
using EntityFramework;
using RPS_Shared;
using System.IO;


namespace CalculatorTS
{
    public class ToTariffPlan
    {
        private string logPath = @"Log.txt";
        public string LogPath { set { logPath = value; } }
        private int DeviceType;
        private Entities db;
        private Guid? TarifiTPgu;
        private Guid TarifiTSgu;
        private DateTime datetime0 = new DateTime(2000, 1, 1, 0, 0, 0, 0);  // НУЛЕВОЕ время
        private DateTime DateExitEstimated_;
        private TimeSpan tt;                // преобразование datetime и int
        private int ttt;
        private int countTpChang;           // число смен ТП
        const int MaxTpChang = 10;          // максимальное число смен ТП на всякий случай
        private decimal Amount_;
        public decimal Amount
        {
            get { return Amount_; }
        }


        public enum TimeType : int
        {
            Minutes = 1,
            Hours = 2,
            Days = 3,
            Weeeks = 4,
            Months = 5
        }

        public struct DateCard
        {
            //public int? CardId;
            public int ParkingEnterTime;
            public int LastRecountTime;
            public byte TSidFC;
            public byte TPidFC;
            public byte ZoneidFC;
            public byte ClientGroupidFC;
            public int SumOnCard;
            public int LastPaymentTime;
            public int Nulltime1;
            public int Nulltime2;
            public int Nulltime3;
            public int TVP;
            public byte TKVP;
            public byte ClientTypidFC;
            public int DateSaveCard;
        };
        private DateCard _DateCard;
        private DateCard NewDateCard;           //результат для нового пересчета и записи на карту
        public DateTime DateExitEstimated { get { return DateExitEstimated_; } }
        public DateCard DateCards
        {
            get { return NewDateCard; }
            set
            {
                _DateCard = value;
                TarifiTS = db.TariffScheduleModel.FirstOrDefault(x => x.IdFC == _DateCard.TSidFC);
                TarifiTP = db.Tariffs.FirstOrDefault(x => x.IdFC == _DateCard.TPidFC);
                TarifiTSgu = TarifiTS.Id;
                TarifiTPgu = TarifiTP.Id;
            }
        }

        //private Guid? _ToAbonement;
        //public Guid? ToAbonement { set { _ToAbonement = value; } get { return _ToAbonement; } }            // Выбор клиентом оплаты абонемента для касс

        private byte _ZoneAfter;
        public byte ZoneAfter { set { _ZoneAfter = value; } }            // Зона после для стоек

        private TariffScheduleModel TarifiTS;
        private Tariffs TarifiTP;
        public Guid? TarifPlanId { get { return TarifiTPgu; } }


        public ToTariffPlan()
        {
            try
            {
                db = new Entities();
                var TypeD = db.DeviceModel.FirstOrDefault();
                DeviceType = TypeD.Type;
            }
            catch (Exception e)
            {
                ToLog("Исключение в BigCalculator при создании соединения с БД ToTariffPlan(). " + e.ToString());
                //return false;
            }
           
        }

/*        public int TariffPlanForwardSchedulesCount()
        {
            try
            {
                db = new Entities();
            }
            catch (Exception e)
            {
                return 0;
            }

            NewDateCard = _DateCard;
            if (DeviceType == 2)
            {
                var c = db.TariffPlanTariffScheduleModel.Count(x => x.TariffScheduleId == TarifiTSgu && (x.TariffPlanId == TarifiTPgu || x.TariffPlanNextId == TarifiTPgu) && x.TypeId == 3 && !x.C_IsDeleted);
                return c;
            }
            else if (DeviceType == 1)
            {
                var c = db.TariffPlanTariffScheduleModel.Count(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanId == TarifiTPgu && x.TypeId == 4 && !x.C_IsDeleted);
                return c;
            }
            else return 0;

        }*/

/*        public List<TariffPlanTariffScheduleModel> TariffPlanForwardSchedules()
        {
            if (DeviceType == 2)
            {
                return db.TariffPlanTariffScheduleModel.Where(x => x.TariffScheduleId == TarifiTSgu && (x.TariffPlanId == TarifiTPgu || x.TariffPlanNextId == TarifiTPgu) && x.TypeId == 3 && !x.C_IsDeleted).OrderBy(x => x.ConditionAbonementPrice).ToList();
            }
            else if (DeviceType == 1)
            {
                return db.TariffPlanTariffScheduleModel.Where(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanId == TarifiTPgu && x.TypeId == 4 && !x.C_IsDeleted).ToList();
            }
            else return null;

        }*/


        public bool BigCalculator()
        {
            Amount_ = 0;
            try
            {
                db = new Entities();
            }
            catch (Exception e)
            {
                ToLog("Исключение в BigCalculator при создании соединения с БД . " + e.ToString());
                return false;
            }

            try
            {
                NewDateCard = _DateCard;

                tt = DateTime.Now - datetime0;      //текущее время TimeSpan
                ttt = (int)tt.TotalSeconds;

                // проверка условий 
                for (countTpChang = 0; countTpChang <= MaxTpChang; countTpChang++)
                {
                    if (CheckConditions())
                    {
                        _DateCard = NewDateCard;
                    }
                    else
                    {
                        break;
                    }
                }

                // последний пересчет калькулятора без условий и переходов
                var tariff = db.Tariffs.FirstOrDefault(x => x.IdFC == _DateCard.TPidFC);
                if (tariff != null)
                {
                    var tt0 = tariff.ToTariff();
                    var calc = new Calculator();
                    DateTime dt1 = datetime0 + TimeSpan.FromSeconds(_DateCard.LastRecountTime);
                    DateTime dt2 = DateTime.Now; //datetime0 + tt; //datetime0 + TimeSpan.FromSeconds(ttt);
                    var z = calc.Calculate(tt0, dt1, dt2);
                    Amount_ = Amount_ + z.Amount;
                    NewDateCard.SumOnCard = _DateCard.SumOnCard /*- (int)z.Amount*/;
                    DateExitEstimated_ = z.DateExitEstimated;
                    _DateCard = NewDateCard;
                }
                else
                {

                    return false;
                }

                //NewDateCard.LastRecountTime = ttt;

                return true;
            }
            catch (Exception e)
            {
                ToLog("Исключение в BigCalculator при расчете. " + e.ToString());
                return false;
            }

        }

        public bool CheckConditions()
        {
            // Условие окончания времени
            // Находим меньшее из времен _DateCard.Nulltime
            #region // Проверка Nulltime1 Время обнуления TVP Текущее время за период
            if (_DateCard.Nulltime1 != 0 && _DateCard.Nulltime1 <= ttt &&
                !(_DateCard.Nulltime2 != 0 && _DateCard.Nulltime1 > _DateCard.Nulltime2) &&
                !(_DateCard.Nulltime3 != 0 && _DateCard.Nulltime1 > _DateCard.Nulltime3))
            {
                if (_DateCard.Nulltime1 <= _DateCard.LastRecountTime) //
                {   // Время обнуления TVP закончился до въезда и еще не пересчитан - МЕНЯЕМ ТП и просто калькулятор
                    NewDateCard.Nulltime1 = 0;
                    NewDateCard.TVP = 0;
                    //новый ТП на карту
                    var TarifiTPN = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanNextId == TarifiTPgu && x.TypeId == 1 && !x.C_IsDeleted);
                    if (TarifiTPN != null)
                    {
                        TarifiTPgu = TarifiTPN.TariffPlanId;
                        var TarifiTPidN = db.Tariffs.FirstOrDefault(x => x.Id == TarifiTPgu && !x.C_IsDeleted);
                        if (TarifiTPidN != null)
                            NewDateCard.TPidFC = (byte)TarifiTPidN.IdFC;
                    }
                }
                else
                { // Время обнуления TVP закончился после въезда - калькулятор до момента окончания Время обнуления TVP, затем МЕНЯЕМ ТП и просто калькулятор
                    var tariff = db.Tariffs.FirstOrDefault(x => x.IdFC == _DateCard.TPidFC && !x.C_IsDeleted);
                    if (tariff != null)
                    {
                        var tt0 = tariff.ToTariff();
                        var calc = new Calculator();
                        DateTime dt1 = datetime0 + TimeSpan.FromSeconds(_DateCard.LastRecountTime);
                        DateTime dt2 = datetime0 + TimeSpan.FromSeconds(_DateCard.Nulltime1);
                        var z = calc.Calculate(tt0, dt1, dt2);
                        //Amount_ = Amount_ + z.Amount;
                        NewDateCard.SumOnCard = _DateCard.SumOnCard - (int)z.Amount;
                    }
                    else
                    {

                        return false;
                    }
                    NewDateCard.LastRecountTime = _DateCard.Nulltime1;
                    NewDateCard.Nulltime1 = 0;
                    NewDateCard.TVP = 0;
                    //новый ТП на карту
                    var TarifiTPN = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanNextId == TarifiTPgu && x.TypeId == 1 && !x.C_IsDeleted);
                    if (TarifiTPN != null)
                    {
                        TarifiTPgu = TarifiTPN.TariffPlanId;
                        var TarifiTPidN = db.Tariffs.FirstOrDefault(x => x.Id == TarifiTPgu && !x.C_IsDeleted);
                        if (TarifiTPidN != null)
                            NewDateCard.TPidFC = (byte)TarifiTPidN.IdFC;
                    }
                }
                return true;
            }
            #endregion

            #region // Проверка Nulltime2 Вермя обнуления TKVP Текущее количество въездов за период
            if (_DateCard.Nulltime2 != 0 && _DateCard.Nulltime2 <= ttt &&
                !(_DateCard.Nulltime1 != 0 && _DateCard.Nulltime2 > _DateCard.Nulltime1) &&
                !(_DateCard.Nulltime3 != 0 && _DateCard.Nulltime2 > _DateCard.Nulltime3))
            {
                if (_DateCard.Nulltime2 <= _DateCard.LastRecountTime) //
                {   // Время обнуления TVP закончился до въезда и еще не пересчитан - МЕНЯЕМ ТП и просто калькулятор
                    NewDateCard.Nulltime2 = 0;
                    NewDateCard.TKVP = 0;
                    //новый ТП на карту
                    var TarifiTPN = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanNextId == TarifiTPgu && x.TypeId == 2 && !x.C_IsDeleted);
                    if (TarifiTPN != null)
                    {
                        TarifiTPgu = TarifiTPN.TariffPlanId;
                        var TarifiTPidN = db.Tariffs.FirstOrDefault(x => x.Id == TarifiTPgu && !x.C_IsDeleted);
                        if (TarifiTPidN != null)
                            NewDateCard.TPidFC = (byte)TarifiTPidN.IdFC;
                    }
                }
                else
                { // Время обнуления TVP закончился после въезда - калькулятор до момента окончания Время обнуления TVP, затем МЕНЯЕМ ТП и просто калькулятор
                    var tariff = db.Tariffs.FirstOrDefault(x => x.IdFC == _DateCard.TPidFC && !x.C_IsDeleted);
                    if (tariff != null)
                    {
                        var tt0 = tariff.ToTariff();
                        var calc = new Calculator();
                        DateTime dt1 = datetime0 + TimeSpan.FromSeconds(_DateCard.LastRecountTime);
                        DateTime dt2 = datetime0 + TimeSpan.FromSeconds(_DateCard.Nulltime2);
                        var z = calc.Calculate(tt0, dt1, dt2);
                        //Amount_ = Amount_ + z.Amount;
                        NewDateCard.SumOnCard = _DateCard.SumOnCard - (int)z.Amount;
                    }
                    else
                    {

                        return false;
                    }
                    NewDateCard.LastRecountTime = _DateCard.Nulltime2;
                    NewDateCard.Nulltime2 = 0;
                    NewDateCard.TKVP = 0;
                    //новый ТП на карту
                    var TarifiTPN = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanNextId == TarifiTPgu && x.TypeId == 2 && !x.C_IsDeleted);
                    if (TarifiTPN != null)
                    {
                        TarifiTPgu = TarifiTPN.TariffPlanId;
                        var TarifiTPidN = db.Tariffs.FirstOrDefault(x => x.Id == TarifiTPgu && !x.C_IsDeleted);
                        if (TarifiTPidN != null)
                            NewDateCard.TPidFC = (byte)TarifiTPidN.IdFC;
                    }
                }
                return true;
            }
            #endregion

            #region // Проверка Nulltime3 Окончание Абонемента
            if (_DateCard.Nulltime3 != 0 && _DateCard.Nulltime3 <= ttt &&
                !(_DateCard.Nulltime2 != 0 && _DateCard.Nulltime3 > _DateCard.Nulltime2) &&
                !(_DateCard.Nulltime1 != 0 && _DateCard.Nulltime2 > _DateCard.Nulltime1))
            {
                if (_DateCard.Nulltime3 <= _DateCard.LastRecountTime) //(_DateCard.Nulltime3 <= _DateCard.ParkingEnterTime)   ???
                {   // абонемент закончился до въезда и еще не пересчитан - МЕНЯЕМ ТП и просто калькулятор
                    NewDateCard.Nulltime3 = 0;

                    //новый ТП на карту
                    // Begin ASmirnov 09.12.2015 Замена тарифов при обратном переходе 
                    /*var TarifiTPN = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu & x.TariffPlanId == TarifiTPgu & x.TypeId == 7);
                    TarifiTPgu = TarifiTPN.TariffPlanNextId;*/
                    var TarifiTPN = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanNextId == TarifiTPgu && x.TypeId == 3 && !x.C_IsDeleted);
                    if (TarifiTPN != null)
                    {
                        TarifiTPgu = TarifiTPN.TariffPlanId;
                        // End ASmirnov 09.12.2015 Замена тарифов при обратном переходе 
                        var TarifiTPidN = db.Tariffs.FirstOrDefault(x => x.Id == TarifiTPgu && !x.C_IsDeleted);
                        if (TarifiTPidN != null)
                            NewDateCard.TPidFC = (byte)TarifiTPidN.IdFC;
                    }
                }
                else
                { // абонемент закончился после въезда - калькулятор до момента окончания абонемента, затем МЕНЯЕМ ТП и просто калькулятор
                    var tariff = db.Tariffs.FirstOrDefault(x => x.IdFC == _DateCard.TPidFC && !x.C_IsDeleted);
                    if (tariff != null)
                    {
                        var tt0 = tariff.ToTariff();
                        var calc = new Calculator();
                        DateTime dt1 = datetime0 + TimeSpan.FromSeconds(_DateCard.LastRecountTime);
                        DateTime dt2 = datetime0 + TimeSpan.FromSeconds(_DateCard.Nulltime3);
                        var z = calc.Calculate(tt0, dt1, dt2);
                        //Amount_ = Amount_ + z.Amount;
                        NewDateCard.SumOnCard = _DateCard.SumOnCard - (int)z.Amount;
                    }
                    else
                    {

                        return false;
                    }
                    NewDateCard.LastRecountTime = _DateCard.Nulltime3;
                    NewDateCard.Nulltime3 = 0;

                    //новый ТП на карту
                    // Begin ASmirnov 09.12.2015 Замена тарифов при обратном переходе 
                    /*var TarifiTPN = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu & x.TariffPlanId == TarifiTPgu & x.TypeId == 7);
                    TarifiTPgu = TarifiTPN.TariffPlanNextId;*/
                    var TarifiTPN = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanNextId == TarifiTPgu && x.TypeId == 3 && !x.C_IsDeleted);
                    if (TarifiTPN != null)
                    {
                        TarifiTPgu = TarifiTPN.TariffPlanId;
                        // End ASmirnov 09.12.2015 Замена тарифов при обратном переходе 
                        var TarifiTPidN = db.Tariffs.FirstOrDefault(x => x.Id == TarifiTPgu && !x.C_IsDeleted);
                        if (TarifiTPidN != null)
                            NewDateCard.TPidFC = (byte)TarifiTPidN.IdFC;
                    }
                }
                return true;
            }
            #endregion

            #region  // проверка перехода по зоне для стоек
            if (DeviceType == 1)
            {
                var TarifZ = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanId == TarifiTPgu && x.TypeId == 4 && !x.C_IsDeleted);
                //Guid zoneA = (Guid)TarifZ.ConditionZoneAfter;
                if (TarifZ != null && TarifZ.ZoneModel.IdFC == _ZoneAfter)       //калькулятор, затем МЕНЯЕМ ТП и просто калькулятор
                { // пересчет
                    var tariff = db.Tariffs.FirstOrDefault(x => x.IdFC == _DateCard.TPidFC && !x.C_IsDeleted);
                    if (tariff != null)
                    {
                        var tt0 = tariff.ToTariff();
                        var calc = new Calculator();
                        DateTime dt1 = datetime0 + TimeSpan.FromSeconds(_DateCard.LastRecountTime);
                        DateTime dt2 = DateTime.Now; //datetime0 + tt; //datetime0 + TimeSpan.FromSeconds(ttt);
                        var z = calc.Calculate(tt0, dt1, dt2);
                        //Amount_ = Amount_ + z.Amount;
                        NewDateCard.SumOnCard = _DateCard.SumOnCard - (int)z.Amount;
                    }
                    else
                    {

                        return false;
                    }

                    NewDateCard.LastRecountTime = ttt;

                    //новый ТП на карту
                    var TarifiTPN = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanId == TarifiTPgu && x.TypeId == 3 && !x.C_IsDeleted);
                    if (TarifiTPN != null)
                    {
                        TarifiTPgu = TarifiTPN.TariffPlanNextId;
                        var TarifiTPidN = db.Tariffs.FirstOrDefault(x => x.Id == TarifiTPgu && !x.C_IsDeleted);
                        if (TarifiTPidN != null)
                            NewDateCard.TPidFC = (byte)TarifiTPidN.IdFC;
                    }

                    return true;
                }
            }
            #endregion

            // Другие условия

            #region // Проверка перехода по TVP Текущее время за период     Обнулить ли TVP???
            var TarifTVP = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanId == TarifiTPgu && x.TypeId == 1 && !x.C_IsDeleted);
            if(TarifTVP != null)
            {
                int ConditionParkingTimeSec = 0;
                if (TarifTVP.ConditionParkingTime != null)
                    ConditionParkingTimeSec = (int)TarifTVP.ConditionParkingTime;                    //время в ConditionParkingTimeTypeId

                TimeToSec((TimeType)TarifTVP.ConditionParkingTimeTypeId, ConditionParkingTimeSec);                        // преобразуем ConditionTime в секунды

                if (TarifTVP != null && ConditionParkingTimeSec <= NewDateCard.TVP)       //калькулятор, затем МЕНЯЕМ ТП и просто калькулятор
                { // пересчет
                    var tariff = db.Tariffs.FirstOrDefault(x => x.IdFC == _DateCard.TPidFC && !x.C_IsDeleted);
                    if (tariff != null)
                    {
                        var tt0 = tariff.ToTariff();
                        var calc = new Calculator();
                        DateTime dt1 = datetime0 + TimeSpan.FromSeconds(_DateCard.LastRecountTime);
                        DateTime dt2 = DateTime.Now; //datetime0 + tt; //datetime0 + TimeSpan.FromSeconds(ttt);
                        var z = calc.Calculate(tt0, dt1, dt2);
                        //Amount_ = Amount_ + z.Amount;
                        NewDateCard.SumOnCard = _DateCard.SumOnCard - (int)z.Amount;
                    }
                    else
                    {

                        return false;
                    }

                    NewDateCard.LastRecountTime = ttt;

                    //новый ТП на карту
                    var TarifiTPN = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanId == TarifiTPgu && x.TypeId == 1 && !x.C_IsDeleted);
                    if (TarifiTPN != null)
                    {
                        TarifiTPgu = TarifiTPN.TariffPlanNextId;
                        var TarifiTPidN = db.Tariffs.FirstOrDefault(x => x.Id == TarifiTPgu && !x.C_IsDeleted);
                        if (TarifiTPidN != null)
                            NewDateCard.TPidFC = (byte)TarifiTPidN.IdFC;
                    }

                    return true;
                }
            }
            #endregion

            #region // Проверка перехода по TKVP Текущее количество въездов за период       Обнулить ли TKVP???
            var TarifTKVP = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanId == TarifiTPgu && x.TypeId == 2 && !x.C_IsDeleted);

            if (TarifTKVP != null && TarifTKVP.ConditionEC <= NewDateCard.TKVP)       //калькулятор, затем МЕНЯЕМ ТП и просто калькулятор
            { // пересчет
                var tariff = db.Tariffs.FirstOrDefault(x => x.IdFC == _DateCard.TPidFC && !x.C_IsDeleted);
                if (tariff != null)
                {
                    var tt0 = tariff.ToTariff();
                    var calc = new Calculator();
                    DateTime dt1 = datetime0 + TimeSpan.FromSeconds(_DateCard.LastRecountTime);
                    DateTime dt2 = DateTime.Now; //datetime0 + tt; //datetime0 + TimeSpan.FromSeconds(ttt);
                    var z = calc.Calculate(tt0, dt1, dt2);
                    //Amount_ = Amount_ + z.Amount;
                    NewDateCard.SumOnCard = _DateCard.SumOnCard - (int)z.Amount;
                }
                else
                {

                    return false;
                }

                NewDateCard.LastRecountTime = ttt;

                //новый ТП на карту
                var TarifiTPN = db.TariffPlanTariffScheduleModel.FirstOrDefault(x => x.TariffScheduleId == TarifiTSgu && x.TariffPlanId == TarifiTPgu && x.TypeId == 2 && !x.C_IsDeleted);
                if (TarifiTPN != null)
                {
                    TarifiTPgu = TarifiTPN.TariffPlanNextId;
                    var TarifiTPidN = db.Tariffs.FirstOrDefault(x => x.Id == TarifiTPgu && !x.C_IsDeleted);
                    if (TarifiTPidN != null)
                        NewDateCard.TPidFC = (byte)TarifiTPidN.IdFC;
                }

                return true;
            }
            #endregion

            return false;
        }


        public void ToLog(string What) // запись в лог строки
        {
            try
            {
                StreamWriter sw = new StreamWriter(logPath, true);
                DateTime d = DateTime.Now;
                string ds = d.ToString("yyyy/MM/dd HH:mm:ss.fff");
                //sw.WriteLine(new string('-', 20));
                sw.WriteLine(ds + "  " + What);
                //Close the file
                sw.Close();
            }
            catch (Exception e)
            {

            }
        }

        private void TimeToSec(TimeType TTimeTypeId, int ConditionTime) // преобразуем ConditionTime в секунды
        {
            int ts = 0;
            DateTime NewDate;
            switch (TTimeTypeId)
            {
                case TimeType.Minutes:
                    NewDate = DateTime.Now.AddMinutes(ConditionTime);
                    ts = (int)((NewDate - DateTime.Now).TotalSeconds);
                    break;
                case TimeType.Hours:
                        NewDate = DateTime.Now.AddHours(ConditionTime);
                        ts = (int)((NewDate - DateTime.Now).TotalSeconds);
                    break;
                case TimeType.Days:
                        NewDate = DateTime.Now.AddDays(ConditionTime);
                        ts = (int)((NewDate - DateTime.Now).TotalSeconds);
                    break;
                case TimeType.Weeeks:
                        NewDate = DateTime.Now.AddDays(7 * ConditionTime);
                        ts = (int)((NewDate - DateTime.Now).TotalSeconds);
                    break;
                case TimeType.Months:
                        NewDate = DateTime.Now.AddMonths((int)ConditionTime);
                        ts = (int)((NewDate - DateTime.Now).TotalSeconds);
                    break;
                default:
                    throw new ApplicationException(String.Format("Тип временного интервала {0} не поддерживается", tt));
                    //ToLog("Тип временного интервала {0} не поддерживается   "+ tt.ToString());
                    break;
            }
            ConditionTime = ts;
        }


    }
}

